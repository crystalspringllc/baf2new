//
//  IconTitleSubtitleView.m
//  Baf2
//
//  Created by Askar Mustafin on 6/7/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "IconTitleSubtitleView.h"
#import "UIView+ConcisePureLayout.h"

@implementation IconTitleSubtitleView

- (id)init {
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"IconTitleSubtitleView"
                                                      owner:self
                                                    options:nil];
    for (id view in nibViews) {
        if ([view isKindOfClass:[UIView class]]) {
            return view;
        }
    }
    return nil;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.iconImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self aa_setHeight:81];

    UIImage *image = [[UIImage imageNamed:@"ic_settings_black"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.infoButton setImage:image forState:UIControlStateNormal];
    [self.infoButton setImageEdgeInsets:UIEdgeInsetsMake(10,10,10,10)];

}

#pragma mark - config actions

- (IBAction)clickInfoButton:(id)sender {
    if (self.onInfoButtonClick) {
        self.onInfoButtonClick();
    }
}

@end
