//
//  MultilineLabel.m
//  Myth
//
//  Created by Almas Adilbek on 6/24/13.
//
//

#import "MultilineLabel.h"

@implementation MultilineLabel

- (id)initWithTitle:(NSString *)title width:(float)width
{
    self = [super initWithFrame:CGRectMake(0, 0, width, 1)];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.numberOfLines = 0;
        self.lineBreakMode = NSLineBreakByWordWrapping;
        self.font = [UIFont systemFontOfSize:14];
        
        [self setText:title];
    }
    return self;
}

-(void)setText:(NSString *)text {
    [super setText:text];
    [self sizeToFit];
}

@end
