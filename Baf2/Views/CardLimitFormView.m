//
//  CardLimitFormView.m
//  Baf2
//
//  Created by Shyngys Kassymov on 12.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "CardLimitFormView.h"
#import "CardLimit.h"
#import "MainButton.h"

@implementation CardLimitFormView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.nameLabel.text = NSLocalizedString(@"limit_on_withdraw_cash_in_atms", nil);
    self.amountTitle.text = NSLocalizedString(@"limit_sum", nil);
    self.fromTitle.text = NSLocalizedString(@"limit_installed", nil);
    self.toTitle.text = NSLocalizedString(@"limit_expired", nil);
    self.submitButton.title = NSLocalizedString(@"set", nil);
    
    self.detailView.layer.cornerRadius = 6;
    self.detailView.layer.masksToBounds = true;
    
    self.amountLabel.textColor = [UIColor appBrownColor];
    self.fromLabel.textColor = [UIColor appBrownColor];
    self.toLabel.textColor = [UIColor appBrownColor];
    
    self.textFieldContainerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    self.textFieldContainerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.textFieldContainerView.layer.borderWidth = 0.5;
    self.textFieldContainerView.layer.masksToBounds = true;
    
    self.textField.keyboardType = UIKeyboardTypeDecimalPad;
    
    [self.submitButton addTarget:self action:@selector(tapSubmit) forControlEvents:UIControlEventTouchUpInside];
    
//    NSMutableAttributedString *text = [NSMutableAttributedString new];
////    NSAttributedString *startText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"attention", nil) attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:14], NSForegroundColorAttributeName: [UIColor appBrownColor]}];
////    [text appendAttributedString:startText];
//    NSAttributedString *endText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"limit_warning", nil) attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14], NSForegroundColorAttributeName: [UIColor appBrownColor]}];
//    [text appendAttributedString:endText];

    self.warningLabel.textColor = [UIColor appBrownColor];
    self.warningLabel.text = NSLocalizedString(@"limit_warning", nil);
}

- (void)layoutSubviews {
    [super layoutSubviews];    
    self.nameLabel.preferredMaxLayoutWidth = self.nameLabel.frame.size.width;
}


#pragma mark - Actions

- (void)tapSubmit {
    [self.textField resignFirstResponder];
    
    if (self.didTapSubmitButton) {
        self.didTapSubmitButton(self.textField.text);
    }
}


#pragma mark - Methods

- (void)setCardLimit:(CardLimit *)limit {
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"dd.MM.yyyy"];
    
    self.nameLabel.text = limit.name;
    
    self.amountLabel.text = [NSString stringWithFormat:@"%@ %@", [limit.amount decimalFormatString], limit.currency];
    self.fromLabel.text = [df stringFromDate:[limit activityFromDate]];
    self.toLabel.text = [df stringFromDate:[limit activityToDate]];
    
    self.textField.text = [limit.amount stringValue];
    
    self.rangeLabel.text = [NSString stringWithFormat:@"от %@ %@ до %@ %@",
                    [limit.minAmount decimalFormatString],
                    limit.currency,
                    [limit.maxAmount decimalFormatString],
                    limit.currency];
}

@end
