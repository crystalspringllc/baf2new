//
//  TemplateView.m
//  Baf2
//
//  Created by Shyngys Kassymov on 28.07.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "TemplateView.h"

@implementation TemplateView {
    BOOL didSetup;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup {
    if (!didSetup) {
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOpacity = 0.15;
        self.layer.shadowRadius = 3;
        self.layer.shadowOffset = CGSizeZero;
        self.layer.cornerRadius = 3;
        
        didSetup = true;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    [self.layer setShouldRasterize:true];
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

@end
