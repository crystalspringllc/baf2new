//
//  AAActionSheetView.h
//  Myth
//
//  Created by Almas Adilbek on 6/24/13.
//
//

#import <UIKit/UIKit.h>

#define kActionSheetSidePadding 20

@interface AAActionSheetView : UIView {
    UILabel *titleLabel;
    UIView *backgroundView;
}

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *backgroundView;

- (id)initWithTitle:(NSString *)title;

-(void)setTitle:(NSString *)title;
-(void)setContentView;
-(void)setContentView:(UIView *)view;
-(void)insertPrepareContentSubview:(UIView *)subview;
-(void)insertPrepareContentSubview:(UIView *)subview withMargin:(int)margin;

-(void)show;
-(void)hide;

-(float)contentViewWidth;

// ----

-(void)insertSeparator;
-(void)insertSeparatorWithMargin:(int)margin;

@end
