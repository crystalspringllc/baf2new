//
//  NewsAddImageCollectionCell.m
//  Baf2
//
//  Created by nmaksut on 18.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NewsAddImageCollectionCell.h"

@implementation NewsAddImageCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.deleteButton addTarget:self action:@selector(delete) forControlEvents:UIControlEventTouchUpInside];
}

- (void)delete {
    if (_deleteBlock) self.deleteBlock();
}

@end
