//
//  FavoriteView.m
//  BAF
//
//  Created by Almas Adilbek on 8/13/14.
//
//

#import "FavoriteView.h"

@implementation FavoriteView {
    UIImageView *bgView;
}

-(id)init {
    self = [super initWithFrame:CGRectMake(0, 0, 12, 12)];
    if(self) {
        [self setFavorited:NO];
    }
    return self;
}

- (void)toggle {
    [self setFavorited:!self.isFavorited];
}

- (void)setFavorited:(BOOL)favorited
{
    self.isFavorited = favorited;

    if (!bgView) {
        bgView = [[UIImageView alloc] initWithFrame:self.bounds];
        [self addSubview:bgView];
    }
    UIImage *image = [UIImage imageNamed:favorited?@"icon-favorite-on":@"icon-favorite-off"];
    bgView.image = image;
}


@end
