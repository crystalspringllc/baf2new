//
//  ProfilePasswordTableViewCell.m
//  Baf2
//
//  Created by Shyngys Kassymov on 20.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ProfilePasswordTableViewCell.h"
#import <JVFloatLabeledTextField.h>

@implementation ProfilePasswordTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.passwordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.passwordTextField.secureTextEntry = true;
    [self.passwordTextField addTarget:self action:@selector(didChangeText:) forControlEvents:UIControlEventEditingChanged];
    
    self.repeatPasswordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.repeatPasswordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.repeatPasswordTextField.secureTextEntry = true;
    [self.repeatPasswordTextField addTarget:self action:@selector(didChangeText:) forControlEvents:UIControlEventEditingChanged];
}

- (void)didChangeText:(JVFloatLabeledTextField *)textField {
    if (self.didChangeText) {
        self.didChangeText(textField);
    }
}

@end
