//
//  ProductRowView.m
//  BAF
//
//  Created by Almas Adilbek on 8/13/14.
//
//

#import "ProductRowView.h"
#import "FavoriteView.h"
#import "M13BadgeView.h"
#import "HorizontalPushView.h"

#define kPaddingLeft 10

@implementation ProductRowView {
    BOOL hasSideLines;
}

- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle {
    return [self initWithTitle:title subtitle:subtitle iconName:nil];
}

- (id)initWithTitle:(NSString *)title
           subtitle:(NSString *)subtitle
           iconName:(NSString *)iconName
{
    return [self initWithTitle:title subtitle:subtitle icon:iconName?[UIImage imageWithName:iconName]: nil];
}

- (id)initWithTitle:(NSString *)title
           subtitle:(NSString *)subtitle
               icon:(id)icon
{
    return [self initWithTitle:title subtitle:subtitle icon:icon warningMessage:nil];
}

- (id)initWithTitle:(NSString *)title
           subtitle:(NSString *)subtitle
               icon:(id)icon
     warningMessage:(NSString *)warningMessage
{
    return [self initWithTitle:title subtitle:subtitle icon:icon warningMessage:warningMessage showFavorite:false];
}

- (id)initWithTitle:(NSString *)title
           subtitle:(NSString *)subtitle
               icon:(id)icon
     warningMessage:(NSString *)warningMessage
       showFavorite:(BOOL)showFavorite
{
    self = [super initWithFrame:CGRectMake(0, 0, [ASize screenWidth] - 16, 50)];
    if (self)
    {
        self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

        if (showFavorite) {
            // Favorites icon
            favoriteView = [[FavoriteView alloc] init];
            favoriteView.backgroundColor = [UIColor redColor];
            favoriteView.x = self.width - 40;
            favoriteView.y = (CGFloat) ((self.height - favoriteView.height) * 0.5);
            [self addSubview:favoriteView];
        }
        
        // Icon
        if(icon)
        {
            iconView = [[UIView alloc] init];
            if([icon isKindOfClass:[UIImage class]])
            {
                UIImageView *iconImageView = [[UIImageView alloc] initWithImage:icon];
                iconImageView.contentMode = UIViewContentModeScaleAspectFit;
                iconImageView.tintColor = [UIColor fromRGB:0x444444];
                if(iconImageView.width > 32) {
                    iconImageView.width = iconImageView.height = 32;
                }
                iconView.frame = iconImageView.bounds;
                [iconView addSubview:iconImageView];
            }
            else if([icon isKindOfClass:[UIView class]])
            {
                UIView *view = (UIView *)icon;
                if(view.width > 32) {
                    view.width = view.height = 32;
                }
                iconView.frame = view.bounds;
                [iconView addSubview:view];
            } else if ([icon isKindOfClass:[NSString class]]) {
                NSURL *url = [NSURL URLWithString:icon];
                UIImageView *iconImageView = [[UIImageView alloc] init];
                iconImageView.contentMode = UIViewContentModeScaleAspectFit;
                iconImageView.width = iconImageView.height = 32;
                [iconImageView sd_setImageWithURL:url];
                iconView.frame = iconImageView.bounds;
                [iconView addSubview:iconImageView];
            }
            iconView.x = kPaddingLeft;
            iconView.y = (CGFloat) ((self.height - iconView.height) * 0.5);
            [self addSubview:iconView];
        }
        
        // Title
        CGFloat titleLabelX = 74;
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelX, 8, self.width - titleLabelX - 40, 1)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:14];
        titleLabel.textColor = [UIColor fromRGB:0x444444];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.x = icon ? iconView.right + kPaddingLeft : kPaddingLeft;

        titleLabel.y = 8;
        titleLabel.text = title;
        [titleLabel sizeToFit];
        [self addSubview:titleLabel];
        
        // Subtitle
        subtitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        subtitleLabel.backgroundColor = [UIColor clearColor];
        subtitleLabel.font = [UIFont systemFontOfSize:14];
        subtitleLabel.textColor = [UIColor fromRGB:0x444444];
        subtitleLabel.x = titleLabel.x;
        subtitleLabel.y = titleLabel.bottom + 2;
        subtitleLabel.text = subtitle;
        [subtitleLabel sizeToFit];
        [self addSubview:subtitleLabel];
        self.height = subtitleLabel.bottom + 6 + 10;
        
        if (warningMessage) {
            warningLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            warningLabel.width = [ASize screenWidth] - subtitleLabel.x - 40;
            warningLabel.backgroundColor = [UIColor clearColor];
            warningLabel.font = [UIFont systemFontOfSize:14];
            warningLabel.textColor = [UIColor redColor];
            warningLabel.numberOfLines = 0;
            warningLabel.lineBreakMode = NSLineBreakByWordWrapping;
            warningLabel.x = subtitleLabel.x;
            warningLabel.y = subtitleLabel.bottom + 2;
            warningLabel.text = warningMessage;
            [warningLabel sizeToFit];
            [self addSubview:warningLabel];
            self.height = warningLabel.bottom + 6 + 10;
        }
        
        self.leftOffset = iconView.x;
        
        iconView.y = (CGFloat) ((self.height - iconView.height) * 0.5);
        
        // Trigger button
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button.frame = self.bounds;
        [self.button addTarget:self action:@selector(tapped) forControlEvents:UIControlEventTouchUpInside];
        
        self.highlightBg = [[UIView alloc] init];
        self.highlightBg.frame = self.bounds;
        [self.highlightBg setBackgroundColor:[UIColor fromRGB:0xeeeeee]];
        [self.button setImage:[UIImage imageWithView:self.highlightBg] forState:UIControlStateHighlighted];
        
        [self insertSubview:self.button atIndex:0];
        
        iconView.userInteractionEnabled = false;
    }
    return self;
}

- (void)setFavorited:(BOOL)favorited {
    [favoriteView setFavorited:favorited];
}

- (void)showDisclosure {
    UIImageView *icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_disclosure_indicator"]];
    icon.x = self.width - icon.width - 12;
    icon.y = (CGFloat) ((self.height - icon.height) * 0.5);
    [self addSubview:icon];
}

- (void)alignLeftLabels {
    titleLabel.x = subtitleLabel.x = kPaddingLeft + 4;
}

- (void)hideFavorite {
    if(favoriteView) {
        [favoriteView removeFromSuperview];
    }
}

- (void)tapped {
    [self.delegate productRowViewTapped:self];
}

- (void)addCardNumberWithText:(NSString *)cardNumber {
    if (!self.cardNumberLabel) {
        self.cardNumberLabel = [[UILabel alloc] init];
        [self addSubview:self.cardNumberLabel];
    }
    
    self.cardNumberLabel.font = [UIFont systemFontOfSize:13];
    self.cardNumberLabel.textColor = [UIColor grayColor];
    self.cardNumberLabel.text = cardNumber;
    [self.cardNumberLabel sizeToFit];
    self.cardNumberLabel.y = titleLabel.bottom + 5;
    self.cardNumberLabel.x = titleLabel.x;
    subtitleLabel.y = self.cardNumberLabel.bottom + 5;
    
    self.height = subtitleLabel.bottom + 5 + 10;
    iconView.y = (CGFloat) ((self.height - iconView.height) * 0.5);
    self.button.frame = CGRectMake(0,0,self.width,self.height);
    self.highlightBg.frame = CGRectMake(0,0,self.width,self.height);
    [self.button setImage:[UIImage imageWithView:self.highlightBg] forState:UIControlStateHighlighted];
    
}

- (void)addTagWithText:(NSString *)text color:(UIColor *)color {
    if (!self.horizontalPushView) {
        self.horizontalPushView = [[HorizontalPushView alloc] initWithFrame:CGRectMake(0,0,self.width,1)];
        self.horizontalPushView.leftIndent = 0;
        self.horizontalPushView.topIndent = 0;
        [self addSubview:self.horizontalPushView];

        self.horizontalPushView.y = subtitleLabel.bottom + 5;
        self.horizontalPushView.x = subtitleLabel.x;
    }

    M13BadgeView *badgeView = [[M13BadgeView alloc] init];
    badgeView.text = text;
    badgeView.textColor = [UIColor whiteColor];
    badgeView.badgeBackgroundColor = color;
    badgeView.font = [UIFont boldSystemFontOfSize:10];
    badgeView.cornerRadius = 4;
    badgeView.height = 20;
    badgeView.tag = 1;
    badgeView.userInteractionEnabled = false;
    [self.horizontalPushView pushView:badgeView];
    self.horizontalPushView.userInteractionEnabled = false;

    self.height = self.horizontalPushView.bottom + 5 + 10;
    
    iconView.y = (CGFloat) ((self.height - iconView.height) * 0.5);

    self.button.frame = CGRectMake(0,0,self.width,self.height);
    self.highlightBg.frame = CGRectMake(0,0,self.width,self.height);
    [self.button setImage:[UIImage imageWithView:self.highlightBg] forState:UIControlStateHighlighted];

}

#pragma mark - Getters/Setters

- (void)setTitleColor:(UIColor *)titleColor {
    _titleColor = titleColor;
    
    titleLabel.textColor = titleColor;
}

- (void)setSubtitleColor:(UIColor *)subtitleColor {
    _subtitleColor = subtitleColor;
    
    subtitleLabel.textColor = subtitleColor;
}

- (void)setWarningColor:(UIColor *)warningColor {
    _warningColor = warningColor;
    
    warningLabel.textColor = warningColor;
}

- (void)setTitleFont:(UIFont *)titleFont {
    _titleFont = titleFont;
    
    titleLabel.font = titleFont;
    [titleLabel sizeToFit];
}

- (void)setSubtitleFont:(UIFont *)subtitleFont {
    _subtitleFont = subtitleFont;
    
    subtitleLabel.font = subtitleFont;
    [subtitleLabel sizeToFit];
}

- (void)setWarningFont:(UIFont *)warningFont {
    _warningFont = warningFont;
    
    warningLabel.font = warningFont;
    [warningLabel sizeToFit];
}

/**
 * Draw hierarchy lines
 *
 */

- (void)drawChildLine {
    hasSideLines = true;
    
    UIView *horizontalLine = [[UIView alloc] init];
    horizontalLine.width = 1;
    horizontalLine.height = self.height;
    horizontalLine.x = (CGFloat) (iconView ? (iconView.width + kPaddingLeft)/2.0 : 10);
    horizontalLine.backgroundColor = [UIColor fromRGB:0x444444];
    [self addSubview:horizontalLine];

    UIView *pointerLine = [[UIView alloc] init];
    pointerLine.width = 10;
    pointerLine.height = 1;
    pointerLine.x = horizontalLine.right;
    pointerLine.centerY = self.centerY;
    pointerLine.backgroundColor = [UIColor fromRGB:0x444444];
    [self addSubview:pointerLine];
    
    [self updateHorizontalAlignmentOfViews];

}

- (void)drawLastChildLine {
    hasSideLines = true;
    
    UIView *horizontalLine = [[UIView alloc] init];
    horizontalLine.width = 1;
    horizontalLine.height = self.height / 2;
    horizontalLine.x = (CGFloat) (iconView ? (iconView.width + kPaddingLeft)/2.0 : 10);
    horizontalLine.backgroundColor = [UIColor fromRGB:0x444444];
    [self addSubview:horizontalLine];

    UIView *pointerLine = [[UIView alloc] init];
    pointerLine.width = 11;
    pointerLine.height = 1;
    pointerLine.x = horizontalLine.right - 1;
    pointerLine.centerY = self.centerY;
    pointerLine.backgroundColor = [UIColor fromRGB:0x444444];
    [self addSubview:pointerLine];
    
    [self updateHorizontalAlignmentOfViews];
}

- (void)updateHorizontalAlignmentOfViews {
    if (favoriteView) {
        favoriteView.x = hasSideLines ? (iconView ? iconView.width : 10) : kPaddingLeft;
        iconView.x = favoriteView.right;
    } else {
        iconView.x = hasSideLines ? (iconView ? iconView.width : 10) : kPaddingLeft;
    }
    
    if (favoriteView) {
        titleLabel.x = iconView ? iconView.right + kPaddingLeft : (favoriteView ? favoriteView.right : kPaddingLeft);
    } else {
        titleLabel.x = iconView ? iconView.right + kPaddingLeft : kPaddingLeft;
    }
    
    subtitleLabel.x = titleLabel.x;
    warningLabel.x = subtitleLabel.x;
    self.horizontalPushView.x = subtitleLabel.x;
    self.cardNumberLabel.x = titleLabel.x;
    self.leftOffset = iconView.x;
}

@end
