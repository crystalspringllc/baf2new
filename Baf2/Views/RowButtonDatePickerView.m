//
//  RowButtonDatePickerView.m
//  BAF
//
//  Created by Almas Adilbek on 10/30/14.
//
//

#import "RowButtonDatePickerView.h"
#import "AADatePicker.h"

@implementation RowButtonDatePickerView {
    AADatePicker *_datePicker;
}

- (id)initWithTitle:(NSString *)title
{
    __weak RowButtonDatePickerView *wSelf = self;
    self = [super initWithIcon:@"btn_birthday_normal" title:title onTap:^(RowButtonView *rowButtonView) {
        [wSelf tapped];
    }];
    if(self) {
        self.date = [NSDate date];
        self.dateFormat = @"dd.MM.yyyy";
    }
    return self;
}

- (NSString *)value {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:self.dateFormat];
    return [df stringFromDate:self.date];
}

- (BOOL)validate {
    return [self value].length != 0;
}

- (float )dateAsTimestamp {
    return (float) [self.date timeIntervalSince1970];
}


- (void)tapped {
    _datePicker = [[AADatePicker alloc] init];
    _datePicker.tag = 1;
    _datePicker.delegate = self;
    [_datePicker showWithDate:self.date];
}

#pragma mark -
#pragma mark AADatePicker

- (void)aaDatePicker:(AADatePicker *)datePicker dateChanged:(NSDate *)date
{
    self.date = date;

    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:self.dateFormat];
    [self setTitle:[df stringFromDate:date]];

    if (self.onDateChanged) {
        self.onDateChanged();
    }
}

- (void)aaDatePickerDoneTapped:(AADatePicker *)datePicker {
    if (self.onDateChanged) {
        self.onDateChanged();
    }
}

@end
