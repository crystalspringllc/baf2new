//
//  AAccordionView.m
//  BAF
//
//  Created by Almas Adilbek on 8/13/14.
//
//

#import "AAccordionView.h"

@implementation AAccordionView {
    NSMutableArray *headers;
}

- (id)init {
    return [self initWithWidth:[ASize screenWidth] - 16];
}

- (id)initWithWidth:(CGFloat)width {
    return [self initWithWidth:width headerHeight:44];
}

- (id)initWithWidth:(CGFloat)width headerHeight:(CGFloat)headerHeight {
    self = [super initWithFrame:CGRectMake(8, 48, width, headerHeight)];
    if(self) {
        headers = [[NSMutableArray alloc] init];

        // Default param values
        self.revealDuration = 0.25;
    }
    return self;
}

#pragma mark - Adding headers

- (void)addHeaderWithTitle:(NSString *)title icon:(NSString *)iconName contentView:(UIView *)view {
    [self addHeaderWithTitle:title icon:iconName contentView:view marginTop:0];
}

- (void)addHeaderWithTitle:(NSString *)title icon:(NSString *)iconName contentView:(UIView *)view marginTop:(CGFloat)marginTop {
    [self addHeaderWithTitle:title icon:iconName contentView:view marginTop:marginTop isOpen:NO];
}

- (void)addHeaderWithTitle:(NSString *)title icon:(NSString *)iconName contentView:(UIView *)view marginTop:(CGFloat)marginTop isOpen:(BOOL)isOpen {
    [self addHeaderWithTitle:title icon:iconName contentView:view detailColor:[UIColor clearColor] marginTop:marginTop isOpen:isOpen];
}

- (void)addHeaderWithTitle:(NSString *)title icon:(NSString *)iconName contentView:(UIView *)view detailColor:(UIColor *)color marginTop:(CGFloat)marginTop isOpen:(BOOL)isOpen {
    AAccordionHeaderView *header = [[AAccordionHeaderView alloc] initWithTitle:title icon:iconName width:[ASize screenWidth] - 16 detailColor:color];
    header.delegate = self;
    header.index = headers.count;
    header.revealDuration = self.revealDuration;
    [header setContentView:view];
    
    if(headers.count > 0) {
        UIView *lastHeader = [headers lastObject];
        if(lastHeader) {
            header.y = lastHeader.bottom + 8;
        }
    }
    
    header.y += marginTop;
    
    [self addSubview:header];
    [headers addObject:header];
    
    self.height = header.bottom;
    
    if(isOpen) [header toggleHeader];
}

#pragma mark - Overrides

-(void)setRevealDuration:(CGFloat)revealDuration {
    _revealDuration = revealDuration;
    for(AAccordionHeaderView *header in headers) {
        header.revealDuration = revealDuration;
    }
}

#pragma mark - AAccordionHeaderView

-(void)accordionHeaderView:(AAccordionHeaderView *)headerView opened:(BOOL)opened
{
    self.userInteractionEnabled = NO;

    CGFloat offset = headerView.contentView.height;
    if(opened) {
        self.height += offset;
    } else {
        self.height -= offset;
    }

    [UIView animateWithDuration:_revealDuration animations:^{
        for (AAccordionHeaderView *header in headers) {
            if (header.index > headerView.index) {
                if (opened) {
                    header.y += offset;
                } else {
                    header.y -= offset;
                }
            }
        }
    } completion:^(BOOL finished) {
        self.userInteractionEnabled = YES;
        [self.delegate aacordionView:self header:headerView didOpen:opened];
    }];
}

#pragma mark - dealloc

- (void)dealloc {
    headers = nil;
}

@end
