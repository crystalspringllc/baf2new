//
// Created by Almas Adilbek on 9/7/14.
// Copyright (c) 2014 Almas Adilbek. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TitleInputTextField : UIView<UITextFieldDelegate> {

}

@property (nonatomic, copy) void (^textFieldDidBeginEditingBlock)(UITextField *textField);
@property(nonatomic, strong) UITextField *textField;

-(id)initWithTitle:(NSString *)title;
-(id)initWithWidth:(CGFloat)width;
-(id)initWithTitle:(NSString *)title width:(CGFloat)width height:(CGFloat)height;
-(id)initWithWidth:(CGFloat)width height:(CGFloat)height;

-(NSString *)value;

-(void)setValue:(NSString *)value;
-(void)setPlaceholder:(NSString *)placeholder;
-(void)enabled:(BOOL)enabled;
-(void)setSecurity;

-(void)setRightText:(NSString *)text;
-(void)setRightView:(UIView *)view;
-(void)setRightView:(UIView *)view rightPadding:(CGFloat)padding;

-(void)focus;

@end