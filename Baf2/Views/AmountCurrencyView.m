//
//  AmountCurrencyView.m
//  Baf2
//
//  Created by Askar Mustafin on 4/14/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "AmountCurrencyView.h"

@implementation AmountCurrencyView

- (id)init {
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"AmountCurrency"
                                                      owner:self
                                                    options:nil];
    for (id view in nibViews) {
        if ([view isKindOfClass:[UIView class]]) {
            return view;
        }
    }
    return nil;
}


@end
