//
//  NewsTableCell.m
//  Baf2
//
//  Created by nmaksut on 16.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NewsTableCell.h"
#import "NSString+HTML.h"
#import "User.h"

@implementation NewsTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.likesButton setImage:[UIImage imageNamed:@"icon-like-empty"] forState:UIControlStateNormal];
    [self.likesButton setImage:[UIImage imageNamed:@"icon-like-filled"] forState:UIControlStateSelected];

    self.likesButton.enabled = [User sharedInstance].loggedIn;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    self.innerView.backgroundColor = [UIColor clearColor];
    if (selected) {
        self.innerView.backgroundColor = [[UIColor fromRGB:0xeeeeee] colorWithAlphaComponent:0.9];
    } else {
        self.innerView.backgroundColor = [UIColor clearColor];
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    self.innerView.backgroundColor = [UIColor clearColor];
    if (highlighted) {
        self.innerView.backgroundColor = [[UIColor fromRGB:0xeeeeee] colorWithAlphaComponent:0.9];
    } else {
        self.innerView.backgroundColor = [UIColor clearColor];
    }
}

- (void)setNews:(News *)news {
    if (news.images.count) {
        self.imageView.hidden = NO;
        [self.newsImageView sd_setImageWithURL:[NSURL URLWithString:news.images.firstObject]];
        self.imageViewHeight.constant = 180;
        self.underImageViewConstraint.constant = 18;
    } else {
        self.imageView.hidden = YES;
        self.imageViewHeight.constant = 0;
        self.underImageViewConstraint.constant = 0;
    }
    
    self.avatarImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:news.src.avaUrl] placeholderImage:[UIImage imageNamed:@"icon_profile_placeholder"]];
    
    NSString *msg = news.info.message;
    self.contentLabel.text = [msg stringByConvertingHTMLToPlainText];

    self.usernameLabel.text = news.src.nick;
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    self.commentsLabel.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ (%lu)", NSLocalizedString(@"comments", nil),(unsigned long)news.comments.count] attributes:underlineAttribute];
    self.commentsLabel.textColor = [UIColor fromRGB:0x005780];

    [self.likesButton setTitle:[NSString stringWithFormat:@"%@ ", news.likes.stringValue] forState:UIControlStateNormal];
    self.likesButton.selected = news.like;
    self.dateLabel.text = news.formattedDateString;    
}

- (IBAction)clickLikeButton:(id)sender {
    if (self.tapLikeButton) {
        self.tapLikeButton(self);
    }
}

@end
