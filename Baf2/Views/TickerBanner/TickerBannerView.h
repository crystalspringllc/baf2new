//
//  TickerBannerView.h
//  Baf2
//
//  Created by Askar Mustafin on 7/18/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ticker.h"

@interface TickerBannerView : UIView
@property (weak, nonatomic) IBOutlet UILabel *sumLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) Ticker *ticker;
@property (weak, nonatomic) IBOutlet UILabel *persentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;

+ (instancetype)configuredView;

- (void)setDarkStyle;
- (void)setLightStyle;

@end
