//
//  TickerBannerView.m
//  Baf2
//
//  Created by Askar Mustafin on 7/18/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "TickerBannerView.h"
#import "ChameleonMacros.h"


@implementation TickerBannerView {
    BOOL istickerStyleLight;
}

+ (instancetype)configuredView {
    TickerBannerView *tickerView = [[NSBundle mainBundle] loadNibNamed:@"TickerBanner" owner:self options:nil].firstObject;
    return tickerView;
}

- (void)setTicker:(Ticker *)ticker {
    _ticker = ticker;

    if (self.ticker != nil) {
        self.titleLabel.text = ticker.name ? : @"АО БАНК АСТАНЫ";
        self.sumLabel.text = [ticker.last_price decimalFormatString] ? : @"";
        self.persentLabel.text = [[ticker.difference decimalFormatString] stringByAppendingString:@"%"] ? : @"";

        if (istickerStyleLight) {

            self.persentLabel.font = [UIFont boldSystemFontOfSize:14];
            self.arrowImageView.hidden = NO;
            self.sumLabel.textColor = [UIColor blackColor];
            self.titleLabel.textColor = [UIColor grayColor];
            if (self.ticker.difference.doubleValue > 0) {
                self.arrowImageView.image = [UIImage imageNamed:@"up_ticker"];
                self.persentLabel.textColor = [UIColor flatGreenColor];
            } else if (self.ticker.difference.doubleValue < 0) {
                self.arrowImageView.image = [UIImage imageNamed:@"down_ticker"];
                self.persentLabel.textColor = [UIColor flatRedColor];
            } else {
                self.arrowImageView.hidden = YES;
                self.persentLabel.textColor = [UIColor grayColor];
            }
            self.persentLabel.font = [UIFont boldSystemFontOfSize:14];

        } else {

            self.arrowImageView.hidden = NO;
            self.sumLabel.textColor = [UIColor whiteColor];
            self.titleLabel.textColor = [UIColor whiteColor];
            if (self.ticker.difference.doubleValue > 0) {
                self.arrowImageView.image = [UIImage imageNamed:@"up_ticker"];
                self.persentLabel.textColor = [UIColor greenColor];
            } else if (self.ticker.difference.doubleValue < 0) {
                self.arrowImageView.image = [UIImage imageNamed:@"down_ticker"];
                self.persentLabel.textColor = [UIColor redColor];
            } else {
                self.arrowImageView.hidden = YES;
                self.persentLabel.textColor = [UIColor whiteColor];
            }

        }

    } else {
        self.titleLabel.text = @"АО БАНК АСТАНЫ";
        self.sumLabel.text = @"";
        self.persentLabel.text = @"";
    }
}

- (void)setDarkStyle {
    istickerStyleLight = NO;
}

- (void)setLightStyle {
    istickerStyleLight = YES;
}

@end
