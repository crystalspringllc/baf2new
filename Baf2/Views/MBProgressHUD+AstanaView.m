//
//  MBProgressHUD+AstanaView.m
//  Baf2
//
//  Created by Shyngys Kassymov on 30.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "MBProgressHUD+AstanaView.h"
#import "AstanaView.h"

@implementation MBProgressHUD (AstanaView)

+ (instancetype)showAstanaHUDWithTitle:(NSString *)title animated:(BOOL)animated {
    [MBProgressHUD hideAllHUDsForView:((AppDelegate *)[UIApplication sharedApplication].delegate).window animated:true];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:((AppDelegate *)[UIApplication sharedApplication].delegate).window animated:animated];
    hud.minSize = CGSizeMake(120, 120);
    hud.dimBackground = true;
    hud.detailsLabelText = title;
    hud.detailsLabelFont = [UIFont systemFontOfSize:16];
    hud.mode = MBProgressHUDModeCustomView;
    hud.animationType = MBProgressHUDAnimationFade;
    AstanaView *astanaView = [[AstanaView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [astanaView animateView];
    hud.customView = astanaView;
    
    return hud;
}

+ (instancetype)showAstanaHUDWithoutMaskWithTitle:(NSString *)title animated:(BOOL)animated {
    [MBProgressHUD hideAllHUDsForView:((AppDelegate *)[UIApplication sharedApplication].delegate).window animated:true];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:((AppDelegate *)[UIApplication sharedApplication].delegate).window animated:animated];
    hud.minSize = CGSizeMake(120, 120);
    hud.dimBackground = false;
    hud.detailsLabelText = title;
    hud.detailsLabelFont = [UIFont systemFontOfSize:16];
    hud.mode = MBProgressHUDModeCustomView;
    hud.animationType = MBProgressHUDAnimationFade;
    AstanaView *astanaView = [[AstanaView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [astanaView animateView];
    hud.customView = astanaView;
    hud.userInteractionEnabled = false;
    
    return hud;
}

+ (instancetype)showAstanaHUDAddedTo:(UIView *)view withTitle:(NSString *)title animated:(BOOL)animated {
    [MBProgressHUD hideAllHUDsForView:view animated:true];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:animated];
    hud.minSize = CGSizeMake(120, 120);
    hud.dimBackground = true;
    hud.detailsLabelText = title;
    hud.detailsLabelFont = [UIFont systemFontOfSize:16];
    hud.mode = MBProgressHUDModeCustomView;
    hud.animationType = MBProgressHUDAnimationFade;
    AstanaView *astanaView = [[AstanaView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [astanaView animateView];
    hud.customView = astanaView;
    
    return hud;
}

+ (void)hideAstanaHUDAnimated:(BOOL)animated {
    [MBProgressHUD hideHUDForView:((AppDelegate *)[UIApplication sharedApplication].delegate).window animated:animated];
}

+ (void)hideAstanaHUDForView:(UIView *)view animated:(BOOL)animated {
    [MBProgressHUD hideHUDForView:view animated:animated];
}

@end
