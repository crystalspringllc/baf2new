//
//  NoConnectionView.m
//  Kiwi.kz
//
//  Created by Almas Adilbek on 6/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NoConnectionView.h"
#import "FUIButton.h"
#import "Connection.h"
#import "ApiClient.h"

#define kConTag 20
#define kRefreshButtonTag 21
#define kMessageLabelTag 22
#define kIconViewTag 23

@interface NoConnectionView()
-(void)configUI;
-(UIView *)con;
-(UILabel *)messageLabel;
-(UIImageView *)iconView;

-(void)refreshTapped:(UIButton *)sender;
@end

@implementation NoConnectionView {
    FUIButton *refreshButton;
}

@synthesize delegate = _delegate;

- (id)init
{
    CGSize size = [[UIScreen mainScreen] bounds].size;
    self = [super initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    if (self) {
        [self configUI];
    }
    return self;
}

-(void)refreshTapped:(UIButton *)sender
{
    if([Connection isReachable]) {
        [self hide];
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [ApiClient cancelAllOperations];
    }
}

-(void)setIcon:(NSString *)iconName andMessage:(NSString *)message
{
    if([self iconView]) {
        [[self iconView] removeFromSuperview];
    }
    CGSize size = [[UIScreen mainScreen] bounds].size;
    UIView *con = [self con];
    
    // Icon View
    UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageWithName:iconName]];
    iconView.tag = kIconViewTag;
    [con addSubview:iconView];
    CGRect f = iconView.frame;
    f.origin.x = (size.width - f.size.width) * 0.5;
    iconView.frame = f;
    
    // Message Label
    if(![self messageLabel]) {
        int padding = 50;
        UILabel *t = [[UILabel alloc] initWithFrame:CGRectMake(padding, iconView.frame.origin.y + iconView.frame.size.height + 5, size.width - padding * 2, 1)];
        t.tag = kMessageLabelTag;
        t.backgroundColor = [UIColor clearColor];
        t.textColor = [UIColor whiteColor];
        t.font = [UIFont systemFontOfSize:16];
        t.textAlignment = NSTextAlignmentCenter;
        [t setNumberOfLines:0];
        t.lineBreakMode = NSLineBreakByWordWrapping;
        [con addSubview:t];
    }
    
    UILabel *t = [self messageLabel];
    t.text = message;
    [t sizeToFit];
    
    f = t.frame;
    f.origin.x = (size.width - f.size.width) * 0.5;
    t.frame = f;
    
    // Refresh Button
    f = refreshButton.frame;
    f.origin.y = t.bottom + 40;
    refreshButton.frame = f;
    
    float conH = refreshButton.bottom;
    
    f = con.frame;
    f.origin.y = (size.height - conH) * 0.5;
    con.frame = f;
}

-(void)show {
    self.alpha = 0;
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
    
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 1;
    }];
}

-(void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        NSLog(@"finished");
        [self removeFromSuperview];
    }];
}

#pragma mark -

-(UIView *)con {
    return (UIView *)[self viewWithTag:kConTag];
}
-(UIImageView *)iconView {
    return (UIImageView *)[[self con] viewWithTag:kIconViewTag];
}
-(UILabel *)messageLabel {
    return (UILabel *)[[self con] viewWithTag:kMessageLabelTag];
}

-(void)configUI
{
    UIView *bg = [[UIView alloc] initWithFrame:self.bounds];
    bg.backgroundColor = [UIColor fromRGB:0x000e18];
    bg.alpha = 0.9;
    [self addSubview:bg];
    
    CGSize size = [[UIScreen mainScreen] bounds].size;
    UIView *con = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, 300)];
    con.tag = kConTag;
    con.userInteractionEnabled = YES;
    
    float buttonWidth = 200;
    refreshButton = [FUIButton buttonWithType:UIButtonTypeCustom];
    [refreshButton addTarget:self action:@selector(refreshTapped:) forControlEvents:UIControlEventTouchUpInside];
    refreshButton.frame = CGRectMake(([ASize screenWidth] - buttonWidth) * 0.5, 0, buttonWidth, 40);
    refreshButton.buttonColor = [UIColor fromRGB:0x4db3d1];
    refreshButton.shadowColor = [UIColor fromRGB:0x4193ab];
    refreshButton.shadowHeight = 3.0f;
    refreshButton.cornerRadius = 6.0f;
    //loginButton.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    [refreshButton setTitleColor:[UIColor fromRGB:0xECF0F1] forState:UIControlStateNormal];
    [refreshButton setTitleColor:[UIColor fromRGB:0xECF0F1] forState:UIControlStateHighlighted];
    refreshButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    refreshButton.titleLabel.shadowOffset = CGSizeMake(0, -1);
    [refreshButton setTitleShadowColor:[UIColor fromRGB:0x16a085] forState:UIControlStateNormal];
    [refreshButton setTitle:NSLocalizedString(@"update", nil) forState:UIControlStateNormal];
    [con addSubview:refreshButton];

    [self addSubview:con];
}

@end
