//
//  RowButtonDatePickerView.h
//  BAF
//
//  Created by Almas Adilbek on 10/30/14.
//
//



#import "RowButtonView.h"
#import "AADatePicker.h"

@interface RowButtonDatePickerView : RowButtonView <AADatePickerDelegate>

@property(nonatomic, strong) NSDate *date;
@property(nonatomic, copy) NSString *dateFormat;

- (id)initWithTitle:(NSString *)title;
- (NSString *)value;
- (BOOL)validate;
- (float)dateAsTimestamp;

@property (nonatomic, copy) void (^onDateChanged)();

@end
