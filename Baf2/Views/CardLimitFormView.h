//
//  CardLimitFormView.h
//  Baf2
//
//  Created by Shyngys Kassymov on 12.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CardLimit, MainButton;

@interface CardLimitFormView : UIView

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;

@property (nonatomic, weak) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UILabel *amountTitle;
@property (nonatomic, weak) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *fromTitle;
@property (nonatomic, weak) IBOutlet UILabel *fromLabel;
@property (weak, nonatomic) IBOutlet UILabel *toTitle;
@property (nonatomic, weak) IBOutlet UILabel *toLabel;

@property (nonatomic, weak) IBOutlet UIView *textFieldContainerView;
@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, weak) IBOutlet UILabel *textFieldDetailLabel;

@property (nonatomic, weak) IBOutlet UILabel *rangeLabel;

@property (nonatomic, weak) IBOutlet UILabel *warningLabel;

@property (nonatomic, weak) IBOutlet MainButton *submitButton;

@property (nonatomic, copy) void (^didTapSubmitButton)(NSString *newAmount);


#pragma mark - Methods

- (void)setCardLimit:(CardLimit *)limit;

@end
