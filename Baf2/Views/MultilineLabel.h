//
//  MultilineLabel.h
//  Myth
//
//  Created by Almas Adilbek on 6/24/13.
//
//

#import <UIKit/UIKit.h>

@interface MultilineLabel : UILabel

- (id)initWithTitle:(NSString *)title width:(float)width;

@end
