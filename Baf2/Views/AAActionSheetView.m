//
//  AAActionSheetView.m
//  Myth
//
//  Created by Almas Adilbek on 6/24/13.
//
//

#import "AAActionSheetView.h"

@interface AAActionSheetView()
-(void)updateUI;
-(UIWindow *)appDelegateWindow;
@end

@implementation AAActionSheetView {
    UIView *dimView;
    UIView *contentView;
}

@synthesize titleLabel, backgroundView;

- (id)initWithTitle:(NSString *)title
{
    CGSize size = [UIScreen mainScreen].bounds.size;
    self = [super initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    if (self)
    {
        // Dim
        dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0;
        [self addSubview:dimView];
        
        // Sheet view
        self.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, size.height, size.width, 1)];
        backgroundView.backgroundColor = [UIColor colorWithRed:247 / 255.f green:247 / 255.f blue:247 / 255.f alpha:1];
        [self addSubview:backgroundView];
        
        // Sheed view top line
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, 1)];
        line.backgroundColor = [UIColor whiteColor];
        [backgroundView addSubview:line];
        
        // Close button
        int closeSize = 30;
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [closeButton addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
        closeButton.frame = CGRectMake(size.width - closeSize - 2, 2, closeSize, closeSize);
        [closeButton setBackgroundImage:[UIImage imageNamed:@"icon-sheet-close.png"] forState:UIControlStateNormal];
        closeButton.showsTouchWhenHighlighted = YES;
        [backgroundView addSubview:closeButton];
        
        [self setTitle:title];
        
    }
    return self;
}

-(void)show
{
    [[self appDelegateWindow] addSubview:self];
    
    CGRect f = backgroundView.frame;
    f.origin.y = [[UIScreen mainScreen] bounds].size.height - f.size.height;
    
    [UIView animateWithDuration:0.2 animations:^{
        dimView.alpha = 0.4;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.25 animations:^{
            backgroundView.frame = f;
        } completion:^(BOOL finished) {
            //
        }];
    }];
}

-(void)hide
{
    CGRect f = backgroundView.frame;
    f.origin.y = [[UIScreen mainScreen] bounds].size.height;
    
    [UIView animateWithDuration:0.2 animations:^{
        backgroundView.frame = f;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            dimView.alpha = 0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];
}

-(void)setTitle:(NSString *)title
{
    if(!titleLabel) {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 10, 1, 1)];
        titleLabel.userInteractionEnabled = NO;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.font = [UIFont boldSystemFontOfSize:15];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.shadowColor = [UIColor whiteColor];
        titleLabel.shadowOffset = CGSizeMake(0, 1);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [backgroundView addSubview:titleLabel];
    }
    
    float w = [UIScreen mainScreen].bounds.size.width;
    
    CGRect f = titleLabel.frame;
    f.size.width = (CGFloat) (w * 0.8);
    titleLabel.frame = f;
    
    titleLabel.text = title;
    [titleLabel sizeToFit];
    
    f = titleLabel.frame;
    f.origin.x = (CGFloat) ((w - f.size.width) * 0.5);
    titleLabel.frame = f;
    
    [self updateUI];
}

-(void)setContentView:(UIView *)view
{
    if(contentView) {
        [contentView removeFromSuperview];
        contentView = nil;
    }
    if(view) {
        contentView = view;
        [backgroundView addSubview:contentView];
    }
    [self updateUI];
}

-(void)setContentView
{
    if(![contentView isDescendantOfView:backgroundView]) {
        [backgroundView addSubview:contentView];
    }
    [self updateUI];
}

-(void)insertPrepareContentSubview:(UIView *)subview
{
    [self insertPrepareContentSubview:subview withMargin:15];
}

-(void)insertPrepareContentSubview:(UIView *)subview withMargin:(int)margin
{
    if(!contentView) {
        contentView = [[UIView alloc] initWithFrame:CGRectMake(kActionSheetSidePadding, 0, [self contentViewWidth], 0)];
    }
    
    CGRect f = contentView.frame;
    CGRect vf = subview.frame;
    vf.origin.y = f.size.height + margin;
    subview.frame = vf;
    [contentView addSubview:subview];
    
    f.size.height = vf.origin.y + vf.size.height;
    contentView.frame = f;
}

-(void)insertSeparator {
    [self insertSeparatorWithMargin:15];
}

-(void)insertSeparatorWithMargin:(int)margin {
    UIImageView *line = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"line-separator-light-gray.png"]];
    CGRect f = line.frame;
    f.origin.x = -kActionSheetSidePadding;
    f.size.width = backgroundView.bounds.size.width;
    line.frame = f;
    
    [self insertPrepareContentSubview:line withMargin:margin];
}

-(float)contentViewWidth {
    return [[UIScreen mainScreen] bounds].size.width - 2 * kActionSheetSidePadding;
}

#pragma mark -

-(void)updateUI
{
    CGRect bgFrame = backgroundView.frame;
    
    if(contentView)
    {
        CGRect cf = contentView.frame;
        
        if(titleLabel) {
            cf.origin.y = titleLabel.frame.origin.y + titleLabel.bounds.size.height;
        } else {
            cf.origin.y = 20;
        }
        contentView.frame = cf;
        
        bgFrame.size.height = cf.origin.y + cf.size.height + 20;
    }
    else if(titleLabel)
    {
        bgFrame.size.height = titleLabel.frame.origin.y + titleLabel.bounds.size.height + 20;
    }
    backgroundView.frame = bgFrame;
}

-(UIWindow *)appDelegateWindow {
    return [[[UIApplication sharedApplication] delegate] window];
}

@end
