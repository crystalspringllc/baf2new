//
//  NumpadKeyboardView.m
//  BAF
//
//  Created by Almas Adilbek on 11/20/14.
//
//

#import "NumpadKeyboardView.h"

#define kUserUseTouchID @"useTouchID"
#define kUseTouchIDAsked @"useTouchIDAsked"

@implementation NumpadKeyboardView

-(id)init {
    self = [super initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 1)];
    if(self) {

        CGFloat paddingTop = 15;
//        CGFloat padding = [ASize screen35:8 higherIphone:20 ipad:25];
        CGFloat padding = [ASize iphone4:8 iphone5:10 iphone6:15 iphone6Plus:15 ipad:20];
        CGFloat buttonDimension = [ASize iphone4:65 iphone5:65 iphone6:75 iphone6Plus:80 ipad:80];
        NSUInteger index = 0;
        CGFloat startX = (CGFloat) (([ASize screenWidth] - (3 * buttonDimension + 2 * padding)) * 0.5);

        for(NSInteger i = 0; i<3; ++i)
        {
            ++index;
            UIButton *button = [self buttonWithDimension:buttonDimension index:index];
            button.x = startX + i * (buttonDimension + padding);
            button.y = paddingTop;
            [self addSubview:button];
        }

        for(NSInteger i = 0; i<3; ++i)
        {
            ++index;
            UIButton *button = [self buttonWithDimension:buttonDimension index:index];
            button.x = startX + i * (buttonDimension + padding);
            button.y = paddingTop + buttonDimension + padding;
            [self addSubview:button];
        }

        for(NSInteger i = 0; i<3; ++i)
        {
            ++index;
            UIButton *button = [self buttonWithDimension:buttonDimension index:index];
            button.x = startX + i * (buttonDimension + padding);
            button.y = paddingTop + 2 * (buttonDimension + padding);
            [self addSubview:button];
        }

        // Delete Button
        UIButton *deleteButton = [self buttonWithDimension:buttonDimension index:99];
        deleteButton.x = [ASize screenWidth] - startX - buttonDimension;
        deleteButton.y = paddingTop + 3 * (buttonDimension + padding);
        [self addSubview:deleteButton];

        // 0 Button
        UIButton *zeroButton = [self buttonWithDimension:buttonDimension index:0];
        zeroButton.x = deleteButton.x - buttonDimension - padding;
        zeroButton.y = deleteButton.y;
        [self addSubview:zeroButton];

//        //TouchID Button
//        UIButton *touchIdButton = [self buttonWithDimension:buttonDimension index:98];
//        touchIdButton.x = zeroButton.x - buttonDimension - padding;
//        touchIdButton.y = zeroButton.y;
//        [self addSubview:touchIdButton];
//
        
        self.height  = zeroButton.bottom + paddingTop;

    }
    return self;
}

- (UIButton *)buttonWithDimension:(CGFloat)dimension index:(NSUInteger)index
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = index;
    button.width = dimension;
    button.height = dimension;
    [button setBackgroundImage:[UIImage imageNamed:@"btn_numpad_normal.png"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"btn_numpad_pressed.png"] forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor fromRGB:0xeeeeee] forState:UIControlStateHighlighted];
    [button.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:[ASize iphone4:26 iphone5:26 iphone6:28 iphone6Plus:32]]];
    if(index == 99) {
        [button setTitle:@"←" forState:UIControlStateNormal];
    } else if(index == 98){
        [button setImage:[[UIImage imageNamed:@"icon_touchID"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        [button.imageView setTintColor:[UIColor whiteColor]];
    } else {
        [button setTitle:[NSString stringWithFormat:@"%i", (int)index] forState:UIControlStateNormal];
    }

    return button;
}

- (void)pressed:(UIButton *)button {
    if(button.tag == 99) {
        [self.delegate numpadKeyboardViewDeleteTapped];
    } else if(button.tag == 98){
        [self.delegate numpadKeyboardViewTouchIdTapped];
    } else {
        [self.delegate numpadKeyboardViewTappedNumber:button.tag];
    }
}

@end
