//
//  AARoundButton.h
//  Toppy
//
//  Created by Almas Adilbek on 4/13/15.
//  Copyright (c) 2015 Toppy Inc. All rights reserved.
//



@interface AARoundButton : UIButton

@property(nonatomic, assign) CGFloat sidePadding;
@property(nonatomic, assign) CGFloat borderWidth;
@property(nonatomic, assign) CGFloat cornerRadius;
@property(nonatomic, strong) UIColor *borderColor;
@property(nonatomic, strong) UIColor *highlighedBorderColor;
@property(nonatomic, strong) UIColor *fillColor;
@property(nonatomic, assign) CGFloat iconTitleSpacing;

@property(nonatomic, assign) CGFloat onTapAlpha;
@property(nonatomic, assign, getter=isTransparentOnTap) BOOL transparentOnTap;

- (id)initWithHeight:(CGFloat)height;

- (void)setTitle:(NSString *)title;
- (void)setTitleColor:(UIColor *)color;
- (void)setFont:(UIFont *)font;
- (void)setIcon:(NSString *)iconName;
- (void)setButtonEnabled:(BOOL)enabled;
- (void)setTitleAndBorderColor:(UIColor *)color;
- (void)notRounded;

- (void)startLoading;
- (void)stopLoading;

@end
