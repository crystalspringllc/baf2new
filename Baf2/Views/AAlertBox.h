//
//  AAlertBox.h
//  BAF
//
//  Created by Almas Adilbek on 6/9/15.
//
//

typedef NS_ENUM(NSInteger, AlertBoxStyle) {
    AlertBoxStyleWarning = 0,
    AlertBoxStyleInfo = 1,
    AlertBoxStyleData = 2
};

@interface AAlertBox : UIView

@property(nonatomic, assign) AlertBoxStyle style;

- (void)setMessage:(NSString *)message;
- (void)setMessage:(NSString *)message marginTop:(CGFloat)marginTop;

- (void)startLoading;
- (void)startLoadingWithTitle:(NSString *)title;
- (void)stopLoading;

@end
