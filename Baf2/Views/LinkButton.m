//
//  LinkButton.m
//  Myth
//
//  Created by Almas Adilbek on 12/12/12.
//
//

#import "LinkButton.h"
#import "UIColor+Extensions.h"

@interface LinkButton()
@end

@implementation LinkButton

@synthesize linkButton;

-(id)initWithLabel:(NSString *)label {
    self = [super init];
    if (self) {
        self.linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
        linkButton.frame = CGRectMake(0, 0, 1, 1);
        [linkButton setTitle:label.length>0?label:@"                                      " forState:UIControlStateNormal];
        [linkButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
        [linkButton setTitleColor:[UIColor fromRGB:0x146888] forState:UIControlStateNormal];
        [linkButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [linkButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:linkButton];

        NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
        linkButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:label attributes:underlineAttribute];

        // Default
        [self setTitleFont:[UIFont systemFontOfSize:14] textColor:[UIColor fromRGB:0x58648e]];
    }
    return self;
}

- (void)onClick:(OnClick)onClick {
    onClickBlock = onClick;
}

- (void)setTitleFont:(UIFont *)font {
    [self setTitleFont:font textColor:nil];
}

- (void)setTitleFont:(UIFont *)font textColor:(UIColor *)color {
    linkButton.titleLabel.font = font;
    if(color) [linkButton setTitleColor:color forState:UIControlStateNormal];
    [self adjustButton];
}

- (void)buttonTapped:(UIButton *)button {
    if(onClickBlock) onClickBlock(self);
}

- (void)setButtonLabel:(NSString *)label {
    [linkButton setTitle:label forState:UIControlStateNormal];
    [self adjustButton];
}

- (UILabel *)linkLabel {
    return linkButton.titleLabel;
}

- (void)adjustButton {
    CGSize stringSize = [linkButton.titleLabel.text boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:@{
                                                NSFontAttributeName : linkButton.titleLabel.font
                                                }
                                      context:nil].size;
    
    CGSize size = CGSizeMake(ceilf(stringSize.width), ceilf(stringSize.height));
    
    CGRect f = linkButton.frame;
    f.size.width = size.width + 8;
    f.size.height = size.height + 6;
    linkButton.frame = f;
    
    f = self.frame;
    f.size = linkButton.bounds.size;
    self.frame = f;
}

#pragma mark -

- (void)dealloc {
    onClickBlock = nil;
}

@end