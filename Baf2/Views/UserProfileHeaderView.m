//
//  UserProfileHeaderView.m
//  Baf2
//
//  Created by Shyngys Kassymov on 20.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "UserProfileHeaderView.h"
#import <UIActionSheet+Blocks.h>
#import <UIImageView+WebCache.h>
#import <TOCropViewController.h>
#import "MBProgressHUD+AstanaView.h"
#import <GPUImage.h>

@interface UserProfileHeaderView () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, TOCropViewControllerDelegate>
@property (nonatomic, strong) UIImagePickerController *imagePickerController;
@end

@implementation UserProfileHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];

    [self.avatarChangeButton addTarget:self action:@selector(showChangeAvatarSheet) forControlEvents:UIControlEventTouchUpInside];
    self.avatarChangeButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.blurAlphaView.alpha = 0;
    self.avatarSpinner.hidden = true;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.avatarChangeButton.layer.cornerRadius = self.avatarChangeButton.frame.size.width/2.0;
    self.avatarChangeButton.layer.masksToBounds = true;
    self.avatarChangeButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)setAvatarImageFromUrlString:(NSString *)urlString {
    __weak UserProfileHeaderView *weakSelf = self;

    self.avatarSpinner.hidden = false;
    [self.avatarSpinner startAnimating];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.375;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [self.avatarChangeButton setBackgroundImage:[UIImage imageNamed:@"img_placeholder"] forState:UIControlStateNormal];
    self.avatarChangeButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:urlString] options:0 progress:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        if (image) {
            [weakSelf.avatarChangeButton setBackgroundImage:image forState:UIControlStateNormal];
            weakSelf.avatarChangeButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
            [weakSelf.avatarChangeButton.imageView.layer addAnimation:transition forKey:nil];
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if (image) {
                GPUImageGaussianBlurFilter *gaussianBlur = [[GPUImageGaussianBlurFilter alloc] init];
                gaussianBlur.blurRadiusInPixels = 5;
                UIImage *blurredImage = [gaussianBlur imageByFilteringImage:image];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf.avatarSpinner stopAnimating];
                    weakSelf.avatarSpinner.hidden = true;
                    
                    weakSelf.blurImageView.image = blurredImage;
                    weakSelf.blurAlphaView.alpha = 1;
                    
                    [weakSelf.blurImageView.layer addAnimation:transition forKey:nil];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf.avatarSpinner stopAnimating];
                    weakSelf.avatarSpinner.hidden = true;
                    
                    weakSelf.blurAlphaView.alpha = 0;
                });
            }
        });
    }];
}

#pragma mark - Actions

- (void)showChangeAvatarSheet {
    //TEMP
//    self.imagePickerController = [[UIImagePickerController alloc] init];
//    self.imagePickerController.delegate = self;
//    self.imagePickerController.navigationBar.tintColor = [UIColor fromRGB:0x444444];
//    [self.imagePickerController.navigationBar setTitleTextAttributes:
//     @{NSForegroundColorAttributeName:[UIColor fromRGB:0x444444]}];
//    self.imagePickerController.navigationBar.barStyle = UIBarStyleBlack;
//    self.imagePickerController.navigationBar.translucent = false;
//
//    __weak UserProfileHeaderView *weakSelf = self;
//    __block UIImagePickerControllerSourceType *sourceType = NULL;
//    [UIActionSheet showInView:self
//                    withTitle:nil
//            cancelButtonTitle:NSLocalizedString(@"cancel", nil)
//       destructiveButtonTitle:nil
//            otherButtonTitles:@[NSLocalizedString(@"choose_from_album", nil), NSLocalizedString(@"make_a_photo", nil)]
//                     tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
//
//                         if (buttonIndex != 2) {
//                             if (buttonIndex == 0) {
//                                 sourceType = (UIImagePickerControllerSourceType *) UIImagePickerControllerSourceTypePhotoLibrary;
//                             } else if (buttonIndex == 1) {
//                                 if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
//                                     sourceType = (UIImagePickerControllerSourceType *) UIImagePickerControllerSourceTypeCamera;
//                                 } else {
//                                     [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"device_not_support_camera", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
//                                     return;
//                                 }
//                             }
//
//                             weakSelf.imagePickerController.sourceType = (UIImagePickerControllerSourceType) sourceType;
//                             weakSelf.imagePickerController.popoverPresentationController.sourceView = weakSelf.avatarChangeButton;
//                             [weakSelf.parentNavigationController presentViewController:weakSelf.imagePickerController animated:YES completion:nil];
//                         }
//                     }];
}

- (void)cropPickedImage {
    if (self.pickedImage) {
        TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:self.pickedImage];
        cropViewController.delegate = self;
        cropViewController.defaultAspectRatio = TOCropViewControllerAspectRatioSquare;
        [self.parentNavigationController presentViewController:cropViewController animated:YES completion:nil];
    }
}

#pragma makr - TOCropViewControllerDelegate

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    
    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
    
    __weak UserProfileHeaderView *weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *blurredImage = [image blurredImageWithRadius:7];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideAstanaHUDAnimated:true];
            
            weakSelf.blurImageView.image = blurredImage;
            
            [weakSelf.avatarChangeButton setBackgroundImage:image forState:UIControlStateNormal];
            weakSelf.avatarChangeButton.imageView.contentMode = UIViewContentModeScaleAspectFill;

            if (weakSelf.didCropImage) {
                weakSelf.didCropImage(image);
            }
            
            [cropViewController dismissViewControllerAnimated:NO completion:nil];
        });
    });
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    __weak UserProfileHeaderView *wSelf = self;
    
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    NSData *imgData = UIImageJPEGRepresentation(image, 1.0);
    NSInteger imgSizeInMB = imgData.length / 1024 / 1024;
    
    if (imgSizeInMB > 1) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"picture_must_not_be_with_size", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        
        return;
    }
    
    self.pickedImage = image;
    [picker dismissViewControllerAnimated:YES completion:^{
//        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [wSelf cropPickedImage];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:true completion:nil];
    
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

@end
