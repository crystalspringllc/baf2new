//
//  BAFChatPhotoItem.m
//  Baf2
//
//  Created by Shyngys Kassymov on 19.07.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "BAFChatPhotoItem.h"
#import "JSQMessagesMediaPlaceholderView.h"
#import <UIImageView+WebCache.h>

@interface BAFChatPhotoItem ()

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) JSQMessagesMediaPlaceholderView *activityIndicator;

@end

@implementation BAFChatPhotoItem

- (instancetype)initWithURL:(NSString *)url {
    self = [super init];
    
    CGSize size = self.mediaViewDisplaySize;
    
    self.imageView = [UIImageView new];
    self.imageView.frame = CGRectMake(0, 0, size.width, size.height);
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.clipsToBounds = true;
    self.imageView.layer.cornerRadius = 20;
    
    self.activityIndicator = [JSQMessagesMediaPlaceholderView viewWithActivityIndicator];
    self.activityIndicator.frame = CGRectMake(0, 0, size.width, size.height);
    [self.imageView addSubview:self.activityIndicator];
    
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [self.activityIndicator removeFromSuperview];
    }];
    
    return self;
}

- (UIView *)mediaView {
    return self.imageView;
}

@end
