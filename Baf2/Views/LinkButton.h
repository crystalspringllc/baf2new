//
//  LinkButton.h
//  Myth
//
//  Created by Almas Adilbek on 12/12/12.
//
//

#import <UIKit/UIKit.h>

typedef void (^OnClick)(id _self);

@interface LinkButton : UIView {
    OnClick onClickBlock;
    UIButton *linkButton;
}

@property (nonatomic, strong) IBOutlet UIButton *linkButton;

-(id)initWithLabel:(NSString *)label;

-(void)setButtonLabel:(NSString *)label;
-(UILabel *)linkLabel;

-(void)setTitleFont:(UIFont *)font;
-(void)setTitleFont:(UIFont *)font textColor:(UIColor *)color;

-(void)onClick:(OnClick)onClick;

@end