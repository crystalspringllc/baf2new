//
// Created by Askar Mustafin on 2/22/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class M13Checkbox;

@interface TwoCheckBoxesSegmentedControl : UIView

@property (nonatomic, strong) NSArray *items;

//Only for reading
@property (nonatomic, assign) NSInteger selectedItemIndex;

- (void)toggleCheckbox:(NSInteger)checkboxIndex;
- (void)uncheckedItems;
- (BOOL)segmentItemsChecked;

@end
