//
//  ObjectsMapView.m
//  BAF
//
//  Created by Almas Adilbek on 5/19/15.
//
//

#import "ObjectsMapView.h"
#import "AARoundButton.h"
#import "UIButton+Tap.h"
#import "UIView+AAPureLayout.h"
#import "ContactsBranchInfoView.h"
#import "Atm.h"
#import "OCMapView.h"
#import "OCMapViewSampleHelpAnnotation.h"
#import "CityObject.h"
#import "KzCityDetector.h"
#import "LocationHelper.h"
#import "BankBranch.h"
#import "ContactsSQLiteManager.h"
#import "ActionSheetListPopupViewController.h"
#import "STPopupController+Extensions.h"

@interface ObjectsMapView ()
@end

@implementation ObjectsMapView {
    ContactsBranchInfoView *infoView;

    NSArray *atms;
    NSArray *branches;
    UIControl *tapView;

    UIButton *buttonCity;
    AARoundButton *buttonFilter;
}

- (id)initWithHeight:(CGFloat)height
{
    self = [super initWithFrame:CGRectMake(0, 0, [ASize screenWidth], height)];
    if(self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        [self aa_autoSetDimensions];

        self.mv = [[OCMapView alloc] initWithFrame:self.bounds];
        self.mv.minimumAnnotationCountPerCluster = 20;
        self.mv.delegate = self;
        self.mv.translatesAutoresizingMaskIntoConstraints = NO;
        self.mv.clusteringEnabled = YES;
        self.mv.showsUserLocation = YES;
        [self addSubview:self.mv];

        [self.mv autoPinEdgeToSuperviewEdge:ALEdgeTop];
        [self.mv autoPinEdgeToSuperviewEdge:ALEdgeLeft];
        [self.mv autoPinEdgeToSuperviewEdge:ALEdgeRight];
        [self.mv autoSetDimension:ALDimensionHeight toSize:height];

        // Buttons
        CGFloat arrowSize = 14;
        
        buttonCity = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, (CGFloat) ([ASize screenWidth] * 0.5), 44)];
        buttonCity.titleLabel.font = [UIFont helveticaNeue:15];
        buttonCity.titleLabel.numberOfLines = 2;
        buttonCity.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        buttonCity.titleLabel.textAlignment = NSTextAlignmentCenter;
        [buttonCity tap:self selector:@selector(cityTapped)];
        [buttonCity addTarget:self action:@selector(buttonTapDown) forControlEvents:UIControlEventTouchDown];
        [buttonCity addTarget:self action:@selector(buttonTapUp) forControlEvents:UIControlEventTouchUpOutside];
        [buttonCity addTarget:self action:@selector(buttonTapUp) forControlEvents:UIControlEventTouchUpInside];
        [buttonCity setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.65]];
        [buttonCity setTitle:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity.name forState:UIControlStateNormal];
        [self.mv addSubview:buttonCity];
        
        UIImageView *mapImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-map-white-small.png"]];
        CGRect frame = mapImageView.frame;
        frame.origin.x = 5;
        frame.origin.y = (buttonCity.height - frame.size.height)/2.0;
        mapImageView.frame = frame;
        [buttonCity addSubview:mapImageView];
        [buttonCity setTitleEdgeInsets:UIEdgeInsetsMake(0.0, mapImageView.right + 5, 0.0, 2 * 5 + arrowSize)];
        
        UIImageView *cityArrowImageView = [UIImageView new];
        cityArrowImageView.frame = CGRectMake(buttonCity.width - 5 - arrowSize, (buttonCity.height - arrowSize)/2.0, arrowSize, arrowSize);
        cityArrowImageView.contentMode = UIViewContentModeScaleAspectFit;
        cityArrowImageView.image = [UIImage imageNamed:@"down4"];
        [buttonCity addSubview:cityArrowImageView];
        
        buttonFilter = [[AARoundButton alloc] initWithFrame:buttonCity.bounds];
        [buttonFilter setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, arrowSize)];
        buttonFilter.titleLabel.numberOfLines = 2;
        buttonFilter.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        buttonFilter.width -= 1;
        buttonFilter.x = buttonCity.right + 1;
        [buttonFilter tap:self selector:@selector(mapFilterTapped)];
        [buttonFilter notRounded];
        buttonFilter.borderWidth = 0;
        buttonFilter.onTapAlpha = 0.8;//buttonCity.onTapAlpha;
        buttonFilter.fillColor = [[UIColor blackColor] colorWithAlphaComponent:0.65];//buttonCity.fillColor;
        [buttonFilter setFont:buttonCity.titleLabel.font];
        [buttonFilter setTitle:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms ? NSLocalizedString(@"atms", nil) : NSLocalizedString(@"branches", nil)];
        [self.mv addSubview:buttonFilter];
        
        UIImageView *filterArrowImageView = [UIImageView new];
        filterArrowImageView.frame = CGRectMake(buttonCity.width - 5 - arrowSize, (buttonCity.height - arrowSize)/2.0, arrowSize, arrowSize);
        filterArrowImageView.contentMode = UIViewContentModeScaleAspectFit;
        filterArrowImageView.image = [UIImage imageNamed:@"down4"];
        [buttonFilter addSubview:filterArrowImageView];
        
        // Tap overlay
        tapView = [[UIControl alloc] initWithFrame:CGRectMake(0, 44, self.width, self.height - 44)];
        [tapView addTarget:self action:@selector(tapSelf) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:tapView];

        // Get current city name.
        if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity) {
            [self loadCity:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity];
        } else {
            if ([LocationHelper myLocationCoordinate].latitude != 0 && [LocationHelper myLocationCoordinate].longitude != 0) {
                CityObject *city = [KzCityDetector cityOfLocation:[[CLLocation alloc] initWithLatitude:[LocationHelper myLocationCoordinate].latitude longitude:[LocationHelper myLocationCoordinate].longitude]];
                [self loadCity:city];
            } else {
                [self loadCity:[[KzCityDetector cities] firstObject]];
            }
        }
    }
    return self;
}

- (void)tapSelf
{
    if([self.delegate respondsToSelector:@selector(objectsMapViewOpenFullscreenMap)]) {
        [self.delegate objectsMapViewOpenFullscreenMap];
    }
}

- (void)loadObjectsWithFocus:(BOOL)needFocus
{
    [self removeAllAnnotations];

    NSMutableSet *annotationsToAdd = [[NSMutableSet alloc] init];

    switch ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType) {
        case MapObjectsTypeAtms:
        {
            atms = [[ContactsSQLiteManager sharedContactsSQLiteManager] loadAtms:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity.cityId];
            for(Atm *atm in atms) {
                OCMapViewSampleHelpAnnotation *annotation = [[OCMapViewSampleHelpAnnotation alloc] initWithCoordinate:atm.location];
                annotation.title = atm.bankName;
                annotation.subtitle = atm.address;
                [annotationsToAdd addObject:annotation];
            }
        };
            break;
        case MapObjectsTypeBranches:
        {
            branches = [[ContactsSQLiteManager sharedContactsSQLiteManager] loadBranches:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity.cityId];
            if (branches.count == 0) {
                if (_fullscreen) {
                    [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"no_branch_in_region", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                }
            } else {
                for(BankBranch *branch in branches) {
                    OCMapViewSampleHelpAnnotation *annotation = [[OCMapViewSampleHelpAnnotation alloc] initWithCoordinate:branch.location];
                    annotation.title = branch.title;
                    annotation.subtitle = branch.address;
                    [annotationsToAdd addObject:annotation];
                }
            }
        };
            break;
    }

    [self.mv addAnnotations:[annotationsToAdd allObjects]];

    if (needFocus) {
        CityObject *city = [KzCityDetector cityOfLocation:[[CLLocation alloc] initWithLatitude:[LocationHelper myLocationCoordinate].latitude longitude:[LocationHelper myLocationCoordinate].longitude]];
        
        if ([city.name isEqualToString:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity.name] && [LocationHelper myLocationCoordinate].latitude != 0 && [LocationHelper myLocationCoordinate].longitude != 0) {
            MKCoordinateRegion mapRegion;
            mapRegion.center = [LocationHelper myLocationCoordinate];
            mapRegion.span.latitudeDelta = 0.009;
            mapRegion.span.longitudeDelta = 0.019;
            
            [self.mv setRegion:mapRegion animated:YES];
        } else {
            [self focusCity:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity];
        }
    }
}

- (void)removeAllAnnotations
{
    NSMutableArray *locs = [[NSMutableArray alloc] init];
    for (id <MKAnnotation> annot in self.mv.annotations) {
        if (![annot isKindOfClass:[ MKUserLocation class]]) {
            [locs addObject:annot];
        }
    }
    [self.mv removeAnnotations:locs];
}

- (void)setFullscreen:(BOOL)fullscreen {
    _fullscreen = fullscreen;
    if(_fullscreen) {
        if(tapView) {
            [tapView removeFromSuperview];
            tapView = nil;
        }
    }
}

- (void)loadCity:(CityObject *)city
{
    [ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity = city;

    [buttonCity setTitle:city.name forState:UIControlStateNormal];
    [buttonFilter setTitle:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms ? NSLocalizedString(@"atms", nil) : NSLocalizedString(@"branches", nil)];
    [self loadObjectsWithFocus:YES];
}

#pragma mark -
#pragma mark Actions

- (void)mapFilterTapped
{
    __weak ObjectsMapView *weakSelf = self;
    
    ActionSheetListPopupViewController *mapActionSheet = [ActionSheetListPopupViewController new];
    
    mapActionSheet.checkmarkDisabledSections = [NSMutableIndexSet indexSetWithIndex:2];
    
    mapActionSheet.numberOfRowsInSection = ^NSInteger(NSInteger section) {
        return 2;
    };
    
    mapActionSheet.rowForIndexPath = ^ActionSheetListRowObject*(NSIndexPath *indexPath) {
        if (indexPath.row == 0) {
            ActionSheetListRowObject *rowObject = [[ActionSheetListRowObject alloc] init];
            rowObject.title = NSLocalizedString(@"atms", nil);
            rowObject.imageName = @"btn_atm_normal.png";
            rowObject.keepObject = @(MapObjectsTypeAtms);
            
            return rowObject;
        }
        
        ActionSheetListRowObject *rowObject = [[ActionSheetListRowObject alloc] init];
        rowObject.title = NSLocalizedString(@"branches", nil);
        rowObject.imageName = @"btn_branches_normal.png";
        rowObject.keepObject = @(MapObjectsTypeBranches);
        
        return rowObject;
    };
    mapActionSheet.title = NSLocalizedString(@"show", nil);
    mapActionSheet.selectedIndexPath = [NSIndexPath indexPathForRow:([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms ? 0 : 1) inSection:0];
    
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:mapActionSheet];
    popupController.dismissOnBackgroundTap = true;
    popupController.style = STPopupStyleBottomSheet;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [popupController presentInViewController:self.parentVC];
    
    mapActionSheet.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
        [ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType = (MapObjectsType) [selectedObject integerValue];
        [buttonFilter setTitle:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms ? NSLocalizedString(@"atms", nil) : NSLocalizedString(@"branches", nil)];
        [weakSelf loadObjectsWithFocus:NO];
        
        if ([weakSelf.delegate respondsToSelector:@selector(didChangeMapFilter)]) {
            [weakSelf.delegate didChangeMapFilter];
        }
    };
}

- (void)cityTapped
{
    __weak ObjectsMapView *weakSelf = self;
    
    ActionSheetListPopupViewController *cityActionSheet = [ActionSheetListPopupViewController new];
    
    cityActionSheet.checkmarkDisabledSections = [NSMutableIndexSet indexSetWithIndex:2];
    
    NSArray *cities = [KzCityDetector cities];
    cityActionSheet.numberOfRowsInSection = ^NSInteger(NSInteger section) {
        return cities.count;
    };
    
    cityActionSheet.rowForIndexPath = ^ActionSheetListRowObject*(NSIndexPath *indexPath) {
        CityObject *city = cities[indexPath.row];
        
        ActionSheetListRowObject *rowObject = [[ActionSheetListRowObject alloc] init];
        rowObject.title = city.name;
        rowObject.keepObject = city;
        
        return rowObject;
    };
    cityActionSheet.title = NSLocalizedString(@"choose_city", nil);

    for (int i = 0; i < cities.count; i++) {
        CityObject *city = cities[i];
        
        if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity && [[ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity.cityId isEqual:city.cityId]) {
            cityActionSheet.selectedIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
            break;
        }
    }
    
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:cityActionSheet];
    popupController.dismissOnBackgroundTap = true;
    popupController.style = STPopupStyleBottomSheet;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [popupController presentInViewController:self.parentVC];
    
    cityActionSheet.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
        [ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity = selectedObject;
        [weakSelf loadCity:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity];
        
        if ([weakSelf.delegate respondsToSelector:@selector(didChangeMapFilter)]) {
            [weakSelf.delegate didChangeMapFilter];
        }
    };
}

- (void)focusCity:(CityObject *)city {
    MKCoordinateRegion mapRegion;
    mapRegion.center = city.coordinate;
    mapRegion.span.latitudeDelta = 0.009;
    mapRegion.span.longitudeDelta = 0.019;
    
    [self.mv setRegion:mapRegion animated:YES];
}

- (void)updateTitles {
    [buttonCity setTitle:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity.name forState:UIControlStateNormal];
    [buttonFilter setTitle:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms ? NSLocalizedString(@"atms", nil) : NSLocalizedString(@"branches", nil)];
    
    if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity) {
        [self loadCity:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity];
    } else {
        if ([LocationHelper myLocationCoordinate].latitude != 0 && [LocationHelper myLocationCoordinate].longitude != 0) {
            CityObject *city = [KzCityDetector cityOfLocation:[[CLLocation alloc] initWithLatitude:[LocationHelper myLocationCoordinate].latitude longitude:[LocationHelper myLocationCoordinate].longitude]];
            [self loadCity:city];
        } else {
            [self loadCity:[[KzCityDetector cities] firstObject]];
        }
    }
}

#pragma mark -
#pragma mark MKMapView

- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *annotationView;

    // if it's a cluster
    if ([annotation isKindOfClass:[OCAnnotation class]])
    {
        OCAnnotation *clusterAnnotation = (OCAnnotation *)annotation;

        annotationView = [aMapView dequeueReusableAnnotationViewWithIdentifier:@"ClusterView"];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"ClusterView"];
            annotationView.canShowCallout = YES;
//            annotationView.centerOffset = CGPointMake(0, -20);
        }

        // set its image
        UILabel *numberLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        numberLabel.backgroundColor = [UIColor clearColor];
        numberLabel.font = [UIFont boldSystemFontOfSize:12];
        numberLabel.textColor = [UIColor whiteColor];
        numberLabel.text = [NSString stringWithFormat:@"%zd", [clusterAnnotation.annotationsInCluster count]];
        [numberLabel sizeToFit];

        CGFloat clusterSize = MAX(numberLabel.width + 12, 30);
        UIView *clusterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, clusterSize , clusterSize)];
        clusterView.layer.borderColor = [UIColor whiteColor].CGColor;
        clusterView.layer.borderWidth = 2;
        clusterView.layer.cornerRadius = (CGFloat) (clusterSize * 0.5);
        clusterView.clipsToBounds = YES;
        clusterView.opaque = NO;
        clusterView.backgroundColor = [UIColor darkGreen];

        numberLabel.center = clusterView.middlePoint;
        [clusterView addSubview:numberLabel];

        annotationView.image = [UIImage imageWithView:clusterView];
    }
    // If it's a single annotation
    else if([annotation isKindOfClass:[OCMapViewSampleHelpAnnotation class]])
    {
        OCMapViewSampleHelpAnnotation *singleAnnotation = (OCMapViewSampleHelpAnnotation *)annotation;
        annotationView = [aMapView dequeueReusableAnnotationViewWithIdentifier:@"singleAnnotationView"];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:singleAnnotation reuseIdentifier:@"singleAnnotationView"];
            annotationView.canShowCallout = YES;
            annotationView.centerOffset = CGPointMake(0, -22);
        }
        if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms) {
            annotationView.image = [UIImage imageNamed:@"btn_pin_normal"];
        } else {
            annotationView.image = [UIImage imageNamed:@"pin-baf.png"];
        }
    }
    else if (annotation == self.mv.userLocation)
    {
        return nil;
    }
    // Error
    else{
        annotationView = (MKPinAnnotationView *)[aMapView dequeueReusableAnnotationViewWithIdentifier:@"errorAnnotationView"];
        if (!annotationView) {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"errorAnnotationView"];
            annotationView.canShowCallout = NO;
            ((MKPinAnnotationView *)annotationView).pinColor = MKPinAnnotationColorRed;
        }
    }

    return annotationView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if([view.annotation isKindOfClass:[OCMapViewSampleHelpAnnotation class]]) {
        if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms) {
            view.image = [UIImage imageNamed:@"btn_pin_pressed"];
            
            [self deselectAllOtherAnnotations:view];
        }
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if([view.annotation isKindOfClass:[OCMapViewSampleHelpAnnotation class]]) {
        if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms) {
            view.image = [UIImage imageNamed:@"btn_pin_normal"];
        }
    }
}

- (void)mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)animated {
    [self.mv doClustering];
}

#pragma mark - Helpers

- (void)deselectAllOtherAnnotations:(MKAnnotationView *)view {
    NSArray *selectedAnnotations = self.mv.selectedAnnotations;
    for (id<MKAnnotation> annotation in selectedAnnotations) {
        if ([annotation isKindOfClass:[OCMapViewSampleHelpAnnotation class]]) {
            if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms) {
                if (annotation != view.annotation) {
                    [self.mv deselectAnnotation:annotation animated:YES];
                }
            }
        }
    }
}

#pragma mark  - City Button Actions

- (void)buttonTapDown {
    buttonCity.alpha = 0.8;
}

- (void)buttonTapUp {
    buttonCity.alpha = 1;
}

@end
