//
//  ProductRowView.h
//  BAF
//
//  Created by Almas Adilbek on 8/13/14.
//
//

@class FavoriteView;

@protocol ProductRowViewDelegate;
@class HorizontalPushView;

@interface ProductRowView : UIView {
    UIView *iconView;
    FavoriteView *favoriteView;
    UILabel *titleLabel;
    UILabel *subtitleLabel;
    UILabel *warningLabel;
}

@property(nonatomic, assign) id<ProductRowViewDelegate> delegate;
@property(nonatomic, strong) id data;
@property (nonatomic, strong) HorizontalPushView *horizontalPushView;
@property (nonatomic, strong) UILabel *cardNumberLabel;

@property(nonatomic, strong) UIButton *button;
@property(nonatomic, strong) UIView *highlightBg;

- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle;
- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle iconName:(NSString *)iconName;
- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle icon:(id)icon;
- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle date:(NSString *)date;
- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle icon:(id)icon warningMessage:(NSString *)warningMessage;
- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle icon:(id)icon warningMessage:(NSString *)warningMessage showFavorite:(BOOL)showFavorite;

-(void)setFavorited:(BOOL)favorited;
-(void)showDisclosure;
-(void)alignLeftLabels;
-(void)hideFavorite;

- (void)addTagWithText:(NSString *)text color:(UIColor *)color;
- (void)addCardNumberWithText:(NSString *)cardNumber;

//Draw the hieranchy lines
- (void)drawChildLine;
- (void)drawLastChildLine;

@property (nonatomic, strong) UIColor *titleColor;
@property (nonatomic, strong) UIColor *subtitleColor;
@property (nonatomic, strong) UIColor *warningColor;

@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic, strong) UIFont *subtitleFont;
@property (nonatomic, strong) UIFont *warningFont;

@property (nonatomic) CGFloat leftOffset;

@end

@protocol ProductRowViewDelegate<NSObject>
-(void)productRowViewTapped:(ProductRowView *)rowView;
@end
