//
//  NewsImageCollectionCell.h
//  Baf2
//
//  Created by nmaksut on 17.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsImageCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end
