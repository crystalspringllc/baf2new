//
//  AccountDetailHeaderView.h
//  Baf2
//
//  Created by Askar Mustafin on 4/11/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CurrentAccount;
@class Account;
@class ChoosePeriodControlView;
@class Deposit;
@class AccountStatementsResponse;
@class Card;

@interface AccountDetailHeaderView : UIView

@property (nonatomic, weak) Account *account;

@property (nonatomic, copy) void (^onClickTransferMultiCurrencyButton)();
@property (nonatomic, copy) void (^onChargeDeposit)(Deposit *deposit);
@property (nonatomic, copy) void (^onClickChoosePeriod)();
@property (nonatomic, copy) void (^onSegmentValueChanged)(NSInteger selectedIndex);
@property (nonatomic, copy) void (^onAdditionalInfoTapped)();
@property (nonatomic, copy) void (^onFavouriteTapped)();
@property (nonatomic, copy) void (^onBlockCardTapped)();
@property (nonatomic, copy) void (^onAccountAliasEditTapped)();
@property (nonatomic, copy) void (^onManageLimitsTapped)();
@property (nonatomic, copy) void (^onSmsBankingTapped)();
@property (nonatomic, copy) void (^onShareRequisitesTapped)(Account *account);
@property (nonatomic, copy) void (^onChargeCard)(Card *card);
@property (nonatomic, copy) void (^onConfigCashBackTapped)();


@property (nonatomic, weak) AccountStatementsResponse *accountStatementsResponse;

- (void)setChoosePeriodText:(NSDate *)fromDate toDate:(NSDate *)toDate;

@property (nonatomic, weak) UIViewController *parentVC;

@end
