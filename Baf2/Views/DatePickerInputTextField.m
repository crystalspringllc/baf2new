//
//  DatePickerInputTextField.m
//  BAF
//
//  Created by Almas Adilbek on 11/1/14.
//
//

#import "DatePickerInputTextField.h"
#import "AADatePickerView.h"
#import "NSString+Ext.h"

@implementation DatePickerInputTextField {
}

- (void)awakeFromNib {
    [super awakeFromNib];

    self.date = [NSDate date];
    self.dateFormat = @"dd.MM.yyyy";

    self.datePickerView = [[AADatePickerView alloc] init];
    self.datePickerView.delegate = self;
    self.textField.inputView = self.datePickerView;
//    [self setIcon:@"icon_birthday"];
}

-(id)initWithTitle:(NSString *)title {
    return [self initWithTitle:title frame:CGRectMake(0, 0, [ASize screenWidth], 44)];
}

-(id)initWithTitle:(NSString *)title frame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.date = [NSDate date];
        self.dateFormat = @"dd.MM.yyyy";
        
        self.datePickerView = [[AADatePickerView alloc] init];
        self.datePickerView.delegate = self;
        [self.datePickerView setTitle:title];
        self.textField.inputView = self.datePickerView;
        [self setIcon:@"icon_birthday"];
    }
    return self;
}

- (NSString *)value {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:self.dateFormat];
    return [df stringFromDate:self.date];
}

#pragma mark -
#pragma mark AADatePickerView

- (void)datePickerViewDidChange:(NSString *)dateString {
    [self setValue:dateString];
}

- (NSDate *)getDateFromText {
    return [self.textField.text dateWithDateFormat:self.dateFormat];
}

@end
