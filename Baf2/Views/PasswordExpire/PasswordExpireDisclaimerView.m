//
// Created by Shyngys Kassymov on 15.03.17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "PasswordExpireDisclaimerView.h"
#import "StatusLabel.h"
#import "MainButton.h"

@interface PasswordExpireDisclaimerView ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) StatusLabel *disclaimerLabel;
@property (nonatomic, strong) MainButton *notNowButton;
@property (nonatomic, strong) MainButton *postponeButton;
@property (nonatomic, strong) MainButton *changePasswordButton;

@property (nonatomic) NSInteger graces;
@property (nonatomic) NSInteger passwordChangePeriod;

@end

@implementation PasswordExpireDisclaimerView {
    BOOL didSetup;
}

- (instancetype)initWithGraces:(NSInteger)graces
          passwordChangePeriod:(NSInteger)passwordChangePeriod {
    self = [super init];

    if (self) {
        self.graces = graces;
        self.passwordChangePeriod = passwordChangePeriod != 0 ? passwordChangePeriod : 60;

        [self setup];
    }

    return self;
}

- (void)setup {
    if (!didSetup) {
        [self prepareTitleLabel];
        [self prepareDisclaimerLabel];
        [self prepareButtons];

        didSetup = true;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];

    self.titleLabel.preferredMaxLayoutWidth = self.titleLabel.frame.size.width;
}

#pragma mark - UI

- (void)prepareTitleLabel {
    self.titleLabel = [UILabel newAutoLayoutView];
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    if (self.graces > 0) {
        self.titleLabel.text = [NSString stringWithFormat:@"Срок действия Вашего пароля истек, осталось входов: %ld.", (long)self.graces];
    } else {
        self.titleLabel.text = [NSString stringWithFormat:@"Срок действия Вашего пароля истек."];
    }
    [self addSubview:self.titleLabel];
    [self.titleLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:8];
    [self.titleLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:8];
    [self.titleLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:8];
}

- (void)prepareDisclaimerLabel {
    self.disclaimerLabel = [[StatusLabel alloc] initWithType:Warning];
    self.disclaimerLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.disclaimerLabel.edgeInsets = UIEdgeInsetsMake(8, 8, 8, 8);
    self.disclaimerLabel.text = [NSString stringWithFormat:@"Переодичность смены пароля составляет: %ld дней.", (long)self.passwordChangePeriod];
    [self addSubview:self.disclaimerLabel];
    [self.disclaimerLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.titleLabel withOffset:8];
    [self.disclaimerLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:8];
    [self.disclaimerLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:8];
}

- (void)prepareButtons {
    self.notNowButton = [self createButton:@"Не сейчас"];
    [self.notNowButton addTarget:self action:@selector(onNotNowButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.notNowButton];
    [self.notNowButton autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.disclaimerLabel withOffset:20];
    [self.notNowButton autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:8];
    [self.notNowButton autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:8];

    self.postponeButton = [self createButton:[NSString stringWithFormat:@"Напомнить через %ld дней", (long)self.passwordChangePeriod]];
    [self.postponeButton addTarget:self action:@selector(onPostponeButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.postponeButton];
    [self.postponeButton autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.notNowButton withOffset:8];
    [self.postponeButton autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:8];
    [self.postponeButton autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:8];

    self.changePasswordButton = [self createButton:@"Сменить пароль"];
    [self.changePasswordButton addTarget:self action:@selector(onChangePasswordButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.changePasswordButton];
    [self.changePasswordButton autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.postponeButton withOffset:8];
    [self.changePasswordButton autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:8];
    [self.changePasswordButton autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:8];
    [self.changePasswordButton autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20];
}

- (MainButton *)createButton:(NSString *)title {
    MainButton *button = [MainButton newAutoLayoutView];
    button.translatesAutoresizingMaskIntoConstraints = false;
    button.mainButtonStyle = MainButtonStyleOrange;
    button.title = title;
    [button autoSetDimension:ALDimensionHeight toSize:45];
    return button;
}

#pragma mark - Actions

- (void)onNotNowButtonTapped {
    if (self.didTapNotNow) {
        self.didTapNotNow();
    }
}

- (void)onPostponeButtonTapped {
    if (self.didTapPostpone) {
        self.didTapPostpone();
    }
}

- (void)onChangePasswordButtonTapped {
    if (self.didTapChangePassword) {
        self.didTapChangePassword();
    }
}

@end