//
// Created by Shyngys Kassymov on 15.03.17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PasswordExpireDisclaimerView : UIView

@property (nonatomic, copy) void (^didTapNotNow)();
@property (nonatomic, copy) void (^didTapPostpone)();
@property (nonatomic, copy) void (^didTapChangePassword)();

- (instancetype)initWithGraces:(NSInteger)graces
          passwordChangePeriod:(NSInteger)passwordChangePeriod;

@end