//
// Created by Shyngys Kassymov on 15.03.17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "PasswordExpireView.h"
#import "NSString+Ext.h"

#define VERTICAL_INSET 20
#define VERTICAL_INNER_INSET 8
#define HORIZONTAL_INSET 20

@implementation PasswordExpireView {
    BOOL didSetup;
    BOOL hasError;
}

- (instancetype)init {
    self = [super init];

    if (self) {
        [self setup];
    }

    return self;
}

- (void)setup {
    if (!didSetup) {
        [self configDisclaimerLabel];
        [self configPasswordDisclaimerLabel];
        [self configErrorLabel];
        [self configTextFields];
        [self configureButton];

        [self updatePasswordDisclaimer];

        didSetup = true;
    }
}

#pragma mark - UI

- (void)configDisclaimerLabel {
    _disclaimerLabel = [[StatusLabel alloc] initWithType:Warning];
    self.disclaimerLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.disclaimerLabel.edgeInsets = UIEdgeInsetsMake(8, 8, 8, 8);
    [self addSubview:self.disclaimerLabel];
    _disclaimerLabelTopConstraint = [self.disclaimerLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:VERTICAL_INSET];
    [self.disclaimerLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:HORIZONTAL_INSET];
    [self.disclaimerLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:HORIZONTAL_INSET];
}

- (void)configPasswordDisclaimerLabel {
    _passwordDisclaimerLabel = [[StatusLabel alloc] initWithType:Info];
    self.passwordDisclaimerLabel.backgroundColor = [UIColor clearColor];
    self.passwordDisclaimerLabel.textColor = [UIColor blackColor];
    self.passwordDisclaimerLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.passwordDisclaimerLabel.edgeInsets = UIEdgeInsetsMake(8, 8, 8, 8);
    self.passwordDisclaimerLabel.hidden = true;
    [self addSubview:self.passwordDisclaimerLabel];
    _passwordDisclaimerLabelTopConstraint = [self.passwordDisclaimerLabel autoPinEdge:ALEdgeTop
                                                                               toEdge:ALEdgeBottom
                                                                               ofView:self.disclaimerLabel
                                                                           withOffset:VERTICAL_INNER_INSET];
    [self.passwordDisclaimerLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:HORIZONTAL_INSET];
    [self.passwordDisclaimerLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:HORIZONTAL_INSET];
}

- (void)configErrorLabel {
    _errorLabel = [[StatusLabel alloc] initWithType:Error];
    self.errorLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.errorLabel.edgeInsets = UIEdgeInsetsMake(8, 8, 8, 8);
    self.errorLabel.hidden = true;
    [self addSubview:self.errorLabel];
    _errorLabelTopConstraint =[self.errorLabel autoPinEdge:ALEdgeTop
                                                        toEdge:ALEdgeBottom
                                                        ofView:self.passwordDisclaimerLabel
                                                    withOffset:VERTICAL_INNER_INSET];
    [self.errorLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:HORIZONTAL_INSET];
    [self.errorLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:HORIZONTAL_INSET];
}

- (void)configTextFields {
    // old
    _oldPasswordTextField = [FloatingTextField new];
    [self configureTextField:self.oldPasswordTextField
                 placeholder:@"Текуший пароль"];
    [self setCommonLayoutConstraints:self.oldPasswordTextField];
    [self.oldPasswordTextField autoPinEdge:ALEdgeTop
                                    toEdge:ALEdgeBottom
                                    ofView:self.errorLabel
                                withOffset:VERTICAL_INNER_INSET];


    // new
    _updatedPasswordTextField = [FloatingTextField new];
    [self configureTextField:self.updatedPasswordTextField
                 placeholder:@"Новый пароль"];
    [self setCommonLayoutConstraints:self.updatedPasswordTextField];
    [self.updatedPasswordTextField autoPinEdge:ALEdgeTop
                                        toEdge:ALEdgeBottom
                                        ofView:self.oldPasswordTextField
                                    withOffset:VERTICAL_INNER_INSET];

    // repeat new
    _repeatUpdatedPasswordTextField = [FloatingTextField new];
    [self configureTextField:self.repeatUpdatedPasswordTextField
                 placeholder:@"Введите новый пароль еще раз"];
    [self setCommonLayoutConstraints:self.repeatUpdatedPasswordTextField];
    [self.repeatUpdatedPasswordTextField autoPinEdge:ALEdgeTop
                                              toEdge:ALEdgeBottom
                                              ofView:self.updatedPasswordTextField
                                          withOffset:VERTICAL_INNER_INSET];
}

- (void)configureTextField:(FloatingTextField *)textField placeholder:(NSString *)placeholder {
    textField.separatorHidden = false;
    textField.placeholder = placeholder;
    textField.translatesAutoresizingMaskIntoConstraints = false;
    textField.secureTextEntry = true;
    [self addSubview:textField];
}

- (void)setCommonLayoutConstraints:(JVFloatLabeledTextField *)textField {
    [textField autoSetDimension:ALDimensionHeight toSize:50];
    [textField autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:HORIZONTAL_INSET];
    [textField autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:HORIZONTAL_INSET];
}

- (void)configureButton {
    _submitButton = [MainButton newAutoLayoutView];
    self.submitButton.mainButtonStyle = MainButtonStyleOrange;
    self.submitButton.title = @"Обновить";
    [self.submitButton addTarget:self action:@selector(onSubmitButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.submitButton];
    [self.submitButton autoSetDimension:ALDimensionHeight toSize:45];
    [self.submitButton autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.repeatUpdatedPasswordTextField withOffset:VERTICAL_INSET];
    [self.submitButton autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:HORIZONTAL_INSET];
    [self.submitButton autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:HORIZONTAL_INSET];
    [self.submitButton autoPinEdgeToSuperviewEdge:ALEdgeBottom
                                                          withInset:VERTICAL_INSET];
}

#pragma mark - Actions

- (void)onSubmitButtonTapped {
    [self endEditing:true];

    [self updatePasswordDisclaimer];

    if (!hasError && self.didTapSubmit) {
        self.didTapSubmit();
    }
}

#pragma mark - Methods

- (void)updatePasswordDisclaimer {
    NSMutableAttributedString *passwordDisclaimerText = [NSMutableAttributedString new];

    hasError = false;

    // password length
    UIColor *passwordLengthColor = [UIColor greenColor];
    if (![self checkPasswordLength]) {
        hasError = true;

        passwordLengthColor = [UIColor redColor];
    }
    NSDictionary *passwordLengthAttributes = @{
            NSFontAttributeName: [UIFont systemFontOfSize:14],
            NSForegroundColorAttributeName: passwordLengthColor
    };
    NSAttributedString *passwordLengthText = [[NSAttributedString alloc] initWithString:@"• не менее 8 символов\n"
                                                                             attributes:passwordLengthAttributes];
    [passwordDisclaimerText appendAttributedString:passwordLengthText];

    // empty spaces
    UIColor *emptySpacesColor = [UIColor greenColor];
    if (![self checkNoEmptySpaces]) {
        hasError = true;

        emptySpacesColor = [UIColor redColor];
    }
    NSDictionary *emptySpacesAttributes = @{
            NSFontAttributeName: [UIFont systemFontOfSize:14],
            NSForegroundColorAttributeName: emptySpacesColor
    };
    NSAttributedString *emptySpacesText = [[NSAttributedString alloc] initWithString:@"• без пробелов\n"
                                                                          attributes:emptySpacesAttributes];
    [passwordDisclaimerText appendAttributedString:emptySpacesText];

    // one digit
    UIColor *oneDigitColor = [UIColor greenColor];
    if (![self checkAtLeastOneDigit]) {
        hasError = true;

        oneDigitColor = [UIColor redColor];
    }
    NSDictionary *oneDigitAttributes = @{
            NSFontAttributeName: [UIFont systemFontOfSize:14],
            NSForegroundColorAttributeName: oneDigitColor
    };
    NSAttributedString *oneDigitText = [[NSAttributedString alloc] initWithString:@"• цифры (0-9)\n"
                                                                       attributes:oneDigitAttributes];
    [passwordDisclaimerText appendAttributedString:oneDigitText];

    // only latin characters
    UIColor *onlyLatinColor = [UIColor greenColor];
    if (![self checkOnlyLatinLetters]) {
        hasError = true;

        onlyLatinColor = [UIColor redColor];
    }
    NSDictionary *onlyLatinAttributes = @{
            NSFontAttributeName: [UIFont systemFontOfSize:14],
            NSForegroundColorAttributeName: onlyLatinColor
    };
    NSAttributedString *onlyLatinText = [[NSAttributedString alloc] initWithString:@"• строчные и прописные буквы на латинице, не использовать буквы других алфавитов\n"
                                                                        attributes:onlyLatinAttributes];
    [passwordDisclaimerText appendAttributedString:onlyLatinText];

    // one special character
    UIColor *oneSpecialCharacterColor = [UIColor greenColor];
    if (![self checkSpecialCharacter]) {
        hasError = true;

        oneSpecialCharacterColor = [UIColor redColor];
    }
    NSDictionary *oneSpecialCharacterAttributes = @{
            NSFontAttributeName: [UIFont systemFontOfSize:14],
            NSForegroundColorAttributeName: oneSpecialCharacterColor
    };
    NSAttributedString *oneSpecialCharacterText = [[NSAttributedString alloc] initWithString:@"• как минимум один специальный символ !@#$%^&amp;*?_~+=.\n"
                                                                                  attributes:oneSpecialCharacterAttributes];
    [passwordDisclaimerText appendAttributedString:oneSpecialCharacterText];

    // check if two password matches
    UIColor *twoPasswordMatchesColor = [UIColor greenColor];
    if (![self.updatedPasswordText isEqualToString:self.repeatedUpdatedPasswordText]) {
        hasError = true;

        twoPasswordMatchesColor = [UIColor redColor];
    }
    NSDictionary *twoPasswordMatchesAttributes = @{
            NSFontAttributeName: [UIFont systemFontOfSize:14],
            NSForegroundColorAttributeName: twoPasswordMatchesColor
    };
    NSAttributedString *twoPasswordMatchesText = [[NSAttributedString alloc] initWithString:@"• подтвержденный пароль должен совпадать"
                                                                                 attributes:twoPasswordMatchesAttributes];
    [passwordDisclaimerText appendAttributedString:twoPasswordMatchesText];

    [self setAttributedPasswordDisclaimerText:passwordDisclaimerText];
}

#pragma mark - Helpers

- (BOOL)checkPasswordLength {
    return self.updatedPasswordText.length >= 8;
}

- (BOOL)checkNoEmptySpaces {
    NSString *searchedString = self.updatedPasswordText;
    NSRange searchedRange = NSMakeRange(0, [searchedString length]);
    NSString *pattern = @".*\\s+.*";
    NSError *error = nil;

    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    NSArray *matches = [regex matchesInString:searchedString options:0 range:searchedRange];

    return matches.count == 0;
}

- (BOOL)checkAtLeastOneDigit {
    NSString *searchedString = self.updatedPasswordText;
    NSRange searchedRange = NSMakeRange(0, [searchedString length]);
    NSString *pattern = @"\\d";
    NSError *error = nil;

    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    NSArray *matches = [regex matchesInString:searchedString options:0 range:searchedRange];

    return matches.count > 0;
}

- (BOOL)checkOnlyLatinLetters {
    NSString *searchedString = self.updatedPasswordText;
    NSRange searchedRange = NSMakeRange(0, [searchedString length]);
    NSString *pattern = @"[\\p{L}&&[^a-zA-Z]]";
    NSError *error = nil;

    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    NSArray *matches = [regex matchesInString:searchedString options:0 range:searchedRange];

    return matches.count == 0;
}

- (BOOL)checkSpecialCharacter {
    NSString *searchedString = self.updatedPasswordText;
    NSRange searchedRange = NSMakeRange(0, [searchedString length]);
    NSString *pattern = @"[!@#$%^&*?_~+=.]";
    NSError *error = nil;

    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    NSArray *matches = [regex matchesInString:searchedString options:0 range:searchedRange];

    return matches.count > 0;
}

#pragma mark - Getters/Setters

- (NSString *)disclaimerText {
    return self.disclaimerLabel.text;
}

- (void)setDisclaimerText:(NSString *)disclaimerText {
    self.disclaimerLabel.hidden = disclaimerText ? false : true;
    self.disclaimerLabel.text = disclaimerText;
    self.disclaimerLabelTopConstraint.constant = disclaimerText ? VERTICAL_INNER_INSET : 0;
}

- (NSString *)errorText {
    return self.errorLabel.text;
}

- (void)setErrorText:(NSString *)errorText {
    self.errorLabel.hidden = errorText ? false : true;

    if (errorText) {
        [self.errorLabel setHTMLText:errorText];

        self.errorLabel.transform = CGAffineTransformMakeTranslation(20, 0);
        [UIView animateWithDuration:0.4
                              delay:0.0
             usingSpringWithDamping:0.2
              initialSpringVelocity:1.0
                            options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    self.errorLabel.transform = CGAffineTransformIdentity;
                }        completion:nil];
    } else {
        self.errorLabel.text = nil;
    }

    self.errorLabelTopConstraint.constant = errorText ? VERTICAL_INNER_INSET : 0;
}

- (void)setAttributedPasswordDisclaimerText:(NSAttributedString *)attributedErrorText {
    self.passwordDisclaimerLabel.hidden = attributedErrorText ? false : true;
    self.passwordDisclaimerLabel.attributedText = attributedErrorText;
    self.passwordDisclaimerLabelTopConstraint.constant = attributedErrorText ? VERTICAL_INNER_INSET : 0;

    if (hasError) {
        self.passwordDisclaimerLabel.transform = CGAffineTransformMakeTranslation(20, 0);
        [UIView animateWithDuration:0.4
                              delay:0.0
             usingSpringWithDamping:0.2
              initialSpringVelocity:1.0
                            options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    self.passwordDisclaimerLabel.transform = CGAffineTransformIdentity;
                }        completion:nil];
    }
}

- (NSString *)oldPasswordText {
    return self.oldPasswordTextField.text;
}

- (NSString *)updatedPasswordText {
    return self.updatedPasswordTextField.text;
}

- (NSString *)repeatedUpdatedPasswordText {
    return self.repeatUpdatedPasswordTextField.text;
}

@end