//
// Created by Shyngys Kassymov on 15.03.17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StatusLabel.h"
#import "FloatingTextField.h"
#import "MainButton.h"

@interface PasswordExpireView : UIView

@property (strong, nonatomic, readonly) StatusLabel *disclaimerLabel;
@property (strong, nonatomic, readonly) NSLayoutConstraint *disclaimerLabelTopConstraint;
@property (strong, nonatomic, readonly) StatusLabel *passwordDisclaimerLabel;
@property (strong, nonatomic, readonly) NSLayoutConstraint *passwordDisclaimerLabelTopConstraint;
@property (strong, nonatomic, readonly) StatusLabel *errorLabel;
@property (strong, nonatomic, readonly) NSLayoutConstraint *errorLabelTopConstraint;
@property (strong, nonatomic, readonly) FloatingTextField *oldPasswordTextField;
@property (strong, nonatomic, readonly) FloatingTextField *updatedPasswordTextField;
@property (strong, nonatomic, readonly) FloatingTextField *repeatUpdatedPasswordTextField;
@property (strong, nonatomic, readonly) MainButton *submitButton;

@property (nonatomic, copy) NSString *disclaimerText;
@property (nonatomic, copy) NSString *errorText;
@property (nonatomic, copy) NSString *oldPasswordText;
@property (nonatomic, copy) NSString *updatedPasswordText;
@property (nonatomic, copy) NSString *repeatedUpdatedPasswordText;

@property (nonatomic, copy) void (^didTapSubmit)();

@end