//
//  ExchangeRatesView.m
//  BAF
//
//  Created by Almas Adilbek on 10/6/15.
//
//


#import "ExchangeRatesView.h"
#import "UIView+AAPureLayout.h"
#import "ExchangeRatesApi.h"
#import "UIView+Ext.h"
#import "ExchangeRate.h"
#import "NSString+Ext.h"
#import "NSDate+Ext.h"

@interface ExchangeRatesView()
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIView *ratesView;
@property (weak, nonatomic) IBOutlet UILabel *updateLabel;
@property (weak, nonatomic) IBOutlet UILabel *updateLabel1;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mainLoader;
@property (weak, nonatomic) IBOutlet UILabel *mainLoaderMesssageLabel;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@end

@implementation ExchangeRatesView {

    UIActivityIndicatorView *loader;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.translatesAutoresizingMaskIntoConstraints = NO;

    self.backgroundColor = [UIColor whiteColor];
    self.layer.borderColor = [UIColor fromRGB:0xA4C6CF].CGColor;
    self.layer.borderWidth = 1;

    self.ratesView.backgroundColor = [UIColor clearColor];
    self.loaderView.backgroundColor = [UIColor clearColor];
    
    self.refreshButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.mainLoaderMesssageLabel.text = NSLocalizedString(@"load_exchanges_rates", nil);
    
    [self loadExchangeRates:YES];
}

- (void)loadExchangeRates:(BOOL)isMainLoader {
    if (isMainLoader) {
        [self showMainLoader];
    }

    NSURLSessionDataTask *task = [ExchangeRatesApi getCourses:^(id response) {
        [self stopActiveLoader];
        [self.refreshButton visible];
        [self exchangeRatesLoaded:response];

    } failure:^(NSString *code, NSString *message) {
        [self stopActiveLoader];
        [self.refreshButton visible];
        self.mainLoaderMesssageLabel.text = message;
    }];
    [self.tasks addObject:task];

}

- (IBAction)tapRefresh:(UIButton *)sender
{
    sender.hidden = YES;

    loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loader.hidesWhenStopped = YES;
    loader.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:loader];
    [loader aa_centerWithView:sender];
    [loader startAnimating];

    [self loadExchangeRates:NO];
}

#pragma mark -

- (void)exchangeRatesLoaded:(NSArray *)response
{
    // Clear content of ratesView
    [self.ratesView removeSubviews];

    CGFloat spacing = 6;
    UIView *lastView = nil;

    for(ExchangeRate *rate in response)
    {
        UILabel *currencyLabel = [UILabel newAutoLayoutView];
        currencyLabel.backgroundColor = [UIColor clearColor];
        currencyLabel.font = [UIFont systemFontOfSize:14];
        currencyLabel.textColor = [UIColor blackColor];
        currencyLabel.text = rate.currency;
        [_ratesView addSubview:currencyLabel];
        [currencyLabel aa_superviewLeft:22];

        if(lastView) {
            [currencyLabel aa_pinUnderView:lastView offset:spacing];
        } else {
            [currencyLabel aa_superviewTop:0];
        }

        UILabel *sellLabel = [UILabel newAutoLayoutView];
        sellLabel.backgroundColor = [UIColor clearColor];
        sellLabel.font = [UIFont systemFontOfSize:14];
        sellLabel.textColor = [UIColor blackColor];
        sellLabel.text = [rate getSellDecimal];
        [_ratesView addSubview:sellLabel];
        [sellLabel aa_verticalAlignWithView:currencyLabel];
        [sellLabel aa_superviewRight:19];

        UILabel *buyLabel = [UILabel newAutoLayoutView];
        buyLabel.backgroundColor = [UIColor clearColor];
        buyLabel.font = [UIFont systemFontOfSize:14];
        buyLabel.textColor = [UIColor blackColor];
        buyLabel.text = [rate getBuyDecimal];
        [_ratesView addSubview:buyLabel];
        [buyLabel aa_verticalAlignWithView:currencyLabel];
        [buyLabel aa_superviewRight:113];

        UIView * line = [UIView newAutoLayoutView];
        [line setBackgroundColor:[UIColor fromRGB:0xeeeeee]];
        [_ratesView addSubview:line];
        [line aa_setHeight:1];
        [line aa_pinUnderView:currencyLabel offset:spacing];
        [line aa_sameLeftEdge:currencyLabel];
        [line aa_sameRightEdge:sellLabel];

        lastView = line;
    }

    if(lastView) {
        [lastView aa_superviewBottom:0];
    }

    // Set updated date.
    NSString *updatedDateString = nil;
    if(response.count > 0) {
        updatedDateString = [(ExchangeRate *)[response firstObject] updated];
    }
    if(updatedDateString) {
        //Oct 6, 2015 4:20:39 PM
        NSDate *updatedDate = [updatedDateString dateWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateString = [updatedDate stringWithDateFormat:@"dd.MM.yyyy HH:mm:ss"];
        self.updateLabel1.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"update_time", nil) ,dateString];
    }
}

#pragma mark -
#pragma mark Helper

- (void)stopActiveLoader {
    [self stopMainLoader];
    [self stopLoader];
}

- (void)showMainLoader {
    self.ratesView.hidden = true;
    [self.loaderView visible];
    [self.mainLoader startAnimating];

    self.mainLoaderMesssageLabel.text = NSLocalizedString(@"load_exchanges_rates", nil);
}

- (void)stopMainLoader {
    self.ratesView.hidden = false;
    if(!self.loaderView.hidden) {
        [self.loaderView invisible];
        [self.mainLoader stopAnimating];
    }
}

- (void)stopLoader {
    if(loader) {
        [loader stopAnimating];
        [loader removeFromSuperview];
    }
}


@end
