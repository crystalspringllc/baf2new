//
//  ContactsObjectInfoView.h
//  BAF
//
//  Created by Almas Adilbek on 5/18/15.
//
//

static NSInteger const contactsObjectInfoViewLeftInset = 15;

@interface ContactsObjectInfoView : UIView {
    UILabel *titleLabel;
    UILabel *addressLabel;

    NSLayoutConstraint *viewBottomConstraint;
    NSLayoutConstraint *viewWidthConstraint;
}

@property(nonatomic, strong) UIFont *textFont;
@property(nonatomic, strong) UIColor *textColor;

- (void)setTitle:(NSString *)text;
- (void)setAddress:(NSString *)text;

@end
