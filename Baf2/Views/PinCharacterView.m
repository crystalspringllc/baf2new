//
//  PinCharacterView.m
//  BAF
//
//  Created by Almas Adilbek on 11/19/14.
//
//

#import "PinCharacterView.h"
#import "FrameAccessor.h"

@implementation PinCharacterView {
    UIView *dotView;
}

-(id)init {
    self = [super initWithFrame:CGRectMake(0, 0, 35, 44)];
    if(self) {
        UIView *bgView = [[UIView alloc] initWithFrame:self.bounds];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.alpha = 0.4;
        bgView.layer.cornerRadius = 6;
        bgView.layer.masksToBounds = YES;
        [self addSubview:bgView];

        dotView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 12, 12)];
        dotView.backgroundColor = [UIColor whiteColor];
        dotView.center = self.middlePoint;
        dotView.alpha = 0;
        dotView.layer.cornerRadius = (CGFloat) (dotView.width * 0.5);
        dotView.layer.masksToBounds = YES;
        [self addSubview:dotView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected {
    _selected = selected;
    if(_selected) {
        [UIView animateWithDuration:0.15 animations:^{
            dotView.alpha = 1;
        }];
    } else {
        [UIView animateWithDuration:0.15 animations:^{
            dotView.alpha = 0;
        }];
    }
}

@end
