//
//  ConfirmEmailAndPhoneNumberCell.m
//  Baf2
//
//  Created by Shyngys Kassymov on 08.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ConfirmEmailAndPhoneNumberCell.h"
#import "MainButton.h"

@implementation ConfirmEmailAndPhoneNumberCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    
    self.confirmButton.mainButtonStyle = MainButtonStyleOrange;
    self.confirmButton.title = NSLocalizedString(@"accept_phone_email", nil);
    [self.confirmButton addTarget:self action:@selector(tapConfirmButton) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Actions

- (void)tapConfirmButton {
    if (self.didTapConfirmButton) {
        self.didTapConfirmButton();
    }
}

@end
