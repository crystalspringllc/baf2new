//
//  AmountCurrencyView.h
//  Baf2
//
//  Created by Askar Mustafin on 4/14/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AmountCurrencyView : UIView
@property (weak, nonatomic) IBOutlet UILabel *currencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@end
