//
//  DepositsListView.m
//  BAF
//
//  Created by Almas Adilbek on 8/14/14.
//
//

#import "DepositsListView.h"
#import "ViewsListView.h"
#import "Deposit.h"
#import "UIImage+Icon.h"

#define kPadding 10
#define kListPadding 10

@implementation DepositsListView

- (id)initWithDepositGroup:(NSArray *)depositGroups {

    self = [super initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 1)];
    if (self) {

        self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

        NSMutableArray * rowViews = [NSMutableArray array];
        
        NSMutableIndexSet *indexesWithoutLines = [NSMutableIndexSet new];
        NSMutableIndexSet *indexesWithShorLines = [NSMutableIndexSet new];
        NSMutableDictionary *leftOffsets = [NSMutableDictionary new];
        
        int i = 0;
        for (DepositGroup *depositGroup in depositGroups) {
            ProductRowView *rowView = [[ProductRowView alloc] initWithTitle:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"deposit", nil), depositGroup.product]
                                                                   subtitle:[NSString stringWithFormat:@"%@ %@", depositGroup.product, depositGroup.deal]
                                                                       icon:[UIImage imageNamed:@"icn_deposit"]];
            rowView.delegate = self;
            rowView.data = depositGroup;
            [rowView hideFavorite];
            rowView.button.userInteractionEnabled = false;
            [rowViews addObject:rowView];

            [indexesWithoutLines addIndex:(NSUInteger) i];
            
            i++;
            
            for (Deposit *deposit in depositGroup.deposits) {
                ProductRowView *rowView1 = [[ProductRowView alloc] initWithTitle:deposit.alias
                                                                       subtitle:[deposit getBalanceWithCurrency]
                                                                           icon:[UIImage currencyIconImage:deposit.currency]];
                rowView1.delegate = self;
                rowView1.data = deposit;
                rowView1.subtitleFont = [UIFont boldSystemFontOfSize:14];
                [rowView1 showDisclosure];
                if (deposit.isFavorite) [rowView1 setFavorited:YES];
                [rowViews addObject:rowView1];


                if ([deposit isEqual:depositGroup.deposits.lastObject]) {
                    [rowView1 drawLastChildLine];
                } else {
                    [rowView1 drawChildLine];

                    [indexesWithShorLines addIndex:(NSUInteger) i];
                    leftOffsets[@(i)] = @(rowView1.leftOffset);
                }

                i++;
            }
        }
        
        ViewsListView *listView = [ViewsListView new];
        listView.indexesWithoutLines = indexesWithoutLines;
        listView.indexesWithShortLines = indexesWithShorLines;
        listView.leftOffsetForShortLines = leftOffsets;
        listView = [listView viewsListWithViews:rowViews];
        [self addSubview:listView];

        self.height = listView.bottom;

    }
    return self;

}

- (void)productRowViewTapped:(ProductRowView *)rowView {
    [self.delegate depositsListViewProductRowViewDidTapped:rowView];
}


@end
