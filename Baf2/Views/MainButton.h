
//
// Created by Askar Mustafin on 4/5/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FUIButton.h"

typedef NS_ENUM(NSInteger, MainButtonStyle) {
    MainButtonStyleOrange,
    MainButtonStyleWhite,
    MainButtonStyleDisable,
    MainButtonStyleRed
};

@interface MainButton : FUIButton

@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) MainButtonStyle mainButtonStyle;

@end
