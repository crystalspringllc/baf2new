//
// Created by Mustafin Askar on 28/09/15.
//

#import <Foundation/Foundation.h>

@class Invoice;
@class Contract;


@interface AlsekoInfoView : UIView

- (id)initWithInvoice:(Invoice *)invoice contract:(Contract *)contract;

@end