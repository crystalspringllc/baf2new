//
// Created by Mustafin Askar on 16/09/15.
//

#import "AlsekoTotalPaySumView.h"
#import "MainButton.h"

@interface AlsekoTotalPaySumView ()
@property (nonatomic, strong) UILabel *sumLabel;
@property (nonatomic, strong) MainButton *goToPayButton;
@property (nonatomic, strong) MainButton *saveInvoiceButton;
@property (nonatomic, strong) MainButton *rejectInvoiceButton;;
@property (nonatomic, assign) BOOL isRejectInvoice;
@end

@implementation AlsekoTotalPaySumView {}

- (id)initWithTotalPaySum:(double)paySum {
    self = [super initForAutoLayout];
    if (self) {
        self.paySum = paySum;
        [self configUI];
    }
    return self;
}

#pragma mark - setter / getter

- (void)setPaySum:(double)paySum {
    _paySum = paySum;
    self.sumLabel.text = [[@(self.paySum) decimalFormatString] stringByAppendingString:@" KZT"];
}

#pragma mark - config actions

- (void)clickSaveButton {
    if (self.onSaveButtonClick) {
        self.onSaveButtonClick();
    }
}

- (void)clickGoToPayButton {
    if (self.onGoToPayButtonClick) {
        self.onGoToPayButtonClick();
    }
}

- (void)clickRejectButton {
    if (self.onRejectButtonClick) {
        self.onRejectButtonClick(self.isRejectInvoice);
    }
}

#pragma mark - config ui

- (void)configUI {
    self.backgroundColor = [UIColor whiteColor];

    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.textColor = [UIColor grayColor];
    titleLabel.font = [UIFont systemFontOfSize:13];
    titleLabel.text = NSLocalizedString(@"bill_total_sum", nil);
    [self addSubview:titleLabel];

    self.sumLabel = [[UILabel alloc] init];
    self.sumLabel.font = [UIFont boldSystemFontOfSize:15];
    self.sumLabel.text = [[@(self.paySum) decimalFormatString] stringByAppendingString:@" KZT"];
    [self addSubview:self.sumLabel];

    self.goToPayButton = [[MainButton alloc] init];
    self.goToPayButton.mainButtonStyle = MainButtonStyleOrange;
    [self.goToPayButton setTitle:NSLocalizedString(@"jump_to_payment", nil)];
    [self.goToPayButton addTarget:self action:@selector(clickGoToPayButton) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.goToPayButton];

    self.saveInvoiceButton = [[MainButton alloc] init];
    self.saveInvoiceButton.mainButtonStyle = MainButtonStyleWhite;
    [self.saveInvoiceButton setTitle:NSLocalizedString(@"save", nil)];
    [self.saveInvoiceButton addTarget:self action:@selector(clickSaveButton) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.saveInvoiceButton];

    self.rejectInvoiceButton = [[MainButton alloc] init];
    self.rejectInvoiceButton.mainButtonStyle = MainButtonStyleWhite;
    [self.rejectInvoiceButton setTitle:NSLocalizedString(@"already_payed", nil)];
    [self.rejectInvoiceButton addTarget:self action:@selector(clickRejectButton) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.rejectInvoiceButton];


    //autolayout code
    [self autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];

    [self.sumLabel autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:15];
    [self.sumLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];

    [titleLabel autoPinEdge:ALEdgeRight toEdge:ALEdgeLeft ofView:self.sumLabel withOffset:-10];
    [titleLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self.sumLabel];

    [self.goToPayButton autoSetDimension:ALDimensionWidth toSize:160];
    [self.goToPayButton autoSetDimension:ALDimensionHeight toSize:44];
    [self.goToPayButton autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.sumLabel withOffset:30];
    [self.goToPayButton autoAlignAxisToSuperviewAxis:ALAxisVertical];

    [self.saveInvoiceButton autoSetDimension:ALDimensionWidth toSize:160];
    [self.saveInvoiceButton autoSetDimension:ALDimensionHeight toSize:44];
    [self.saveInvoiceButton autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.goToPayButton withOffset:10];
    [self.saveInvoiceButton autoAlignAxisToSuperviewAxis:ALAxisVertical];

    [self.rejectInvoiceButton autoSetDimension:ALDimensionHeight toSize:44];
    [self.rejectInvoiceButton autoSetDimension:ALDimensionWidth toSize:160];
    [self.rejectInvoiceButton autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.saveInvoiceButton withOffset:10];
    [self.rejectInvoiceButton autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [self.rejectInvoiceButton autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20];

}

#pragma mark - setters:
#pragma mark - public
- (void)setInvoiceHistoryState:(NSString *)invoiceHistoryState {
    if ([invoiceHistoryState isEqualToString:@"expired"]) {
        [self setRejectEnable];
    } else if ([invoiceHistoryState isEqualToString:@"ready"]) {
        [self setRejectEnable];
    } else if ([invoiceHistoryState isEqualToString:@"inprogress"]) {
        [self setAllButtonsDisabled];
    } else if ([invoiceHistoryState isEqualToString:@"payed"]) {
        [self setAllButtonsDisabled];
    } else if ([invoiceHistoryState isEqualToString:@"payed1"]) {
        [self setAllButtonsDisabled];
    } else if ([invoiceHistoryState isEqualToString:@"payed2"]) {
        [self setAllButtonsDisabled];
    } else if ([invoiceHistoryState isEqualToString:@"rejected"]) {
        [self setCancelRejectEnable];
    } else {
        [self setRejectEnable];
    }
}

#pragma mark - private
- (void)setRejectEnable {
    self.goToPayButton.enabled = YES;
    self.saveInvoiceButton.enabled = YES;
    self.rejectInvoiceButton.enabled = YES;
    [self.rejectInvoiceButton setTitle:NSLocalizedString(@"already_payed", nil) forState:UIControlStateNormal];

    self.isRejectInvoice = NO;
}

- (void)setCancelRejectEnable {
    self.goToPayButton.enabled = NO;
    self.saveInvoiceButton.enabled = NO;
    self.rejectInvoiceButton.enabled = YES;
    [self.rejectInvoiceButton setTitle:NSLocalizedString(@"cancel_cancellation", nil) forState:UIControlStateNormal];

    self.isRejectInvoice = YES;
}

- (void)setAllButtonsDisabled {
    self.goToPayButton.enabled = NO;
    self.saveInvoiceButton.enabled = NO;
    self.rejectInvoiceButton.enabled = NO;
    [self.rejectInvoiceButton setTitle:NSLocalizedString(@"already_payed", nil) forState:UIControlStateNormal];

    self.isRejectInvoice = NO;
}

#pragma mark - dealloc

- (void)dealloc {}

@end