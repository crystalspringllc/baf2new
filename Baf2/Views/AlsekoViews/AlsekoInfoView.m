//
// Created by Mustafin Askar on 28/09/15.
//

#import "AlsekoInfoView.h"
#import "Invoice.h"
#import "Contract.h"
#import "NSDate+Ext.h"
#import "Param.h"


@interface AlsekoInfoView() {}
@property (nonatomic, strong) Invoice *invoice;
@property (nonatomic, strong) Contract *contract;
@end

@implementation AlsekoInfoView {}

- (id)initWithInvoice:(Invoice *)invoice contract:(Contract *)contract {
    self = [super initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 1)];
    if (self) {
        self.invoice = invoice;
        self.contract = contract;
        [self configUI];
    }
    return self;
}

#pragma mark - config ui

- (void)configUI {
    self.backgroundColor = [UIColor whiteColor];

    UIImageView *alsekoIcon = [[UIImageView alloc] init];
    alsekoIcon.height = 30;
    alsekoIcon.width = 30;
    alsekoIcon.x = 15;
    alsekoIcon.y = 10;
    alsekoIcon.contentMode = UIViewContentModeScaleAspectFit;
    [alsekoIcon sd_setImageWithURL:[[NSURL alloc] initWithString:self.contract.provider.logo]
                  placeholderImage:[UIImage imageNamed:@"icon-icon-placeholder"]];
    [self addSubview:alsekoIcon];

    UILabel *titleNameLabel = [[UILabel alloc] init];
    titleNameLabel.font = [UIFont systemFontOfSize:14];
    titleNameLabel.text = self.contract.provider.name ? : @"";
    [titleNameLabel sizeToFit];
    titleNameLabel.x = alsekoIcon.right + 15;
    titleNameLabel.y = 10;
    [self addSubview:titleNameLabel];

    UILabel *invoiceIdLabel = [[UILabel alloc] init];
    invoiceIdLabel.font = [UIFont systemFontOfSize:13];
    invoiceIdLabel.text = self.invoice.account;
    [invoiceIdLabel sizeToFit];
    invoiceIdLabel.x = alsekoIcon.right + 15;
    invoiceIdLabel.y = titleNameLabel.bottom + 3;
    [self addSubview:invoiceIdLabel];

    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, invoiceIdLabel.bottom + 3, self.width, 1)];
    line.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3];
    [self addSubview:line];

    UILabel *addressLabel = [self getLabelWithTitle:NSLocalizedString(@"address", nil) andText:self.invoice.address];
    addressLabel.x = 15;
    addressLabel.y = alsekoIcon.bottom + 20;
    [self addSubview:addressLabel];

    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0, addressLabel.bottom + 3, self.width, 1)];
    line2.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3];
    [self addSubview:line2];

    UILabel *personalAccountLabel = [self getLabelWithTitle:NSLocalizedString(@"personal_account", nil) andText:self.contract.contract];
    personalAccountLabel.x = 15;
    personalAccountLabel.y = addressLabel.bottom + 10;
    [self addSubview:personalAccountLabel];

    UIView *line3 = [[UIView alloc] initWithFrame:CGRectMake(0, personalAccountLabel.bottom + 3, self.width, 1)];
    line3.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3];
    [self addSubview:line3];

    UILabel *formDateLabel = [self getLabelWithTitle:NSLocalizedString(@"formation_date", nil) andText:[self.invoice.formedDate stringWithDateFormat:@"dd.MM.yyyy"]];
    formDateLabel.x = 15;
    formDateLabel.y = personalAccountLabel.bottom + 10;
    [self addSubview:formDateLabel];

    UIView *line4 = [[UIView alloc] initWithFrame:CGRectMake(0, formDateLabel.bottom + 3, self.width, 1)];
    line4.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3];
    [self addSubview:line4];

    CGFloat yParamPos = formDateLabel.bottom;
    for (Param *param in self.invoice.params) {
        int parValInt = [param.parValue intValue];
        NSString *parValString = [NSString stringWithFormat:@"%i", parValInt];
        UILabel *paramLabel = [self getLabelWithTitle:[NSString stringWithFormat:@"%@: ",param.parName] andText:parValString];
        paramLabel.x = 15;
        paramLabel.y = yParamPos + 10;
        [self addSubview:paramLabel];

        yParamPos = paramLabel.bottom;

        UIView *line5 = [[UIView alloc] initWithFrame:CGRectMake(0, yParamPos + 3, self.width, 1)];
        line5.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3];
        [self addSubview:line5];
    }

    self.height = yParamPos + 20;

}

#pragma mark - attributed label

- (UILabel *)getLabelWithTitle:(NSString *)title
                       andText:(NSString *)text
{
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.width = [ASize screenWidth] - 30;
    titleLabel.numberOfLines = 0;
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    if (title) {
        titleLabel.text = [title stringByAppendingString:text ? : @""];
    }
    [titleLabel sizeToFit];
    return titleLabel;
}

@end