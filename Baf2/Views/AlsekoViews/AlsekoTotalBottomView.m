//
// Created by Mustafin Askar on 21/09/15.
//

#import <PureLayout/ALView+PureLayout.h>
#import <ChameleonFramework/ChameleonMacros.h>
#import "AlsekoTotalBottomView.h"
#import "M13Checkbox.h"
#import "Service.h"
#import "NSNumber+Ext.h"


@interface AlsekoTotalBottomView() {}
@property (nonatomic, strong) Service *service;
@property (nonatomic, strong) NSString *isIncludeDebtTitle;
@property (nonatomic, strong) UILabel *debtSummLabel;
//@property (nonatomic, strong) UILabel *totalForServiceTitleLabel;
//@property (nonatomic, strong) UILabel *totalForServiceValueLabel;
@end

@implementation AlsekoTotalBottomView {}

- (id)initWithService:(Service *)service {
    self = [super initForAutoLayout];
    if (self) {
        self.service = service;

        [self configUI];

//        if ([self.service.fixSum intValue] > 0) {
//            self.totalSum = [self.service.fixSum doubleValue];
//        }
    }
    return self;
}


#pragma mark - config action

//- (void)setTotalSum:(double)totalSum {
//    _totalSum = totalSum;
//    self.totalForServiceValueLabel.text =  [NSString stringWithFormat:@"%@ KZT", [@(_totalSum) numberFormattedText]];
//}

- (void)isIncludeDebtCheckboxAction:(M13Checkbox *)isIncludeDebtCheckboxAction {
    if (self.onIncludeDebtCheckboxValueChanged) {
        self.onIncludeDebtCheckboxValueChanged();
    }

}

#pragma mark - config ui

- (void)configUI {
    self.backgroundColor = [UIColor headerBgColor];

    [self autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];

    //UILabel *
    if (![self.service.debtSumm isEqualToNumber:@0]) {

        if (self.service.debtSumm) {
            if ([self.service.debtSumm intValue] > 0) {
                self.isIncludeDebtTitle = NSLocalizedString(@"include_overpayment", nil);
            } else {
                self.isIncludeDebtTitle = NSLocalizedString(@"consider_debt", nil);
            }
        }
        self.isIncludeDebtCheckbox = [[M13Checkbox alloc] initWithTitle:self.isIncludeDebtTitle];
        self.isIncludeDebtCheckbox.titleLabel.font = [UIFont systemFontOfSize:14];
        self.isIncludeDebtCheckbox.checkHeight = 18;
        self.isIncludeDebtCheckbox.checkColor = [UIColor flatBlueColor];
        self.isIncludeDebtCheckbox.strokeColor = [UIColor flatBlueColor];
        [self.isIncludeDebtCheckbox autoSetDimension:ALDimensionWidth toSize:150];
        self.isIncludeDebtCheckbox.checkAlignment = M13CheckboxAlignmentLeft;
        [self addSubview:self.isIncludeDebtCheckbox];


        self.debtSummLabel = [[UILabel alloc] init];
        [self addSubview:self.debtSummLabel];


        [self.isIncludeDebtCheckbox autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:15];
        [self.isIncludeDebtCheckbox autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
        [self.debtSummLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
        [self.debtSummLabel autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:15];
        [self.isIncludeDebtCheckbox autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:15];
        [self.debtSummLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:15];

    }


    //self.totalForServiceTitleLabel = [[UILabel alloc] init];
    //self.totalForServiceTitleLabel.font = [UIFont systemFontOfSize:13];
    //self.totalForServiceTitleLabel.textColor = [UIColor lightGrayColor];
    //self.totalForServiceValueLabel = [[UILabel alloc] init];
    //[self addSubview:self.totalForServiceTitleLabel];
    //[self addSubview:self.totalForServiceValueLabel];
    //[self.totalForServiceTitleLabel autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:15];
    //[self.totalForServiceValueLabel autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:15];
    //[self.totalForServiceTitleLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20];
    //[self.totalForServiceValueLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20];

//    if (![self.service.debtSumm isEqualToNumber:@0]) {
//        [self.totalForServiceTitleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.isIncludeDebtCheckbox withOffset:20];
//        [self.totalForServiceValueLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.debtSummLabel withOffset:20];
//    } else {
        //[self.totalForServiceTitleLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
//        [self.totalForServiceValueLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
//    }


    //Apply data to views
    //self.totalForServiceTitleLabel.text = @"Итого за сервис:";

    self.isIncludeDebtCheckbox.checkState = self.service.includeDebt ? M13CheckboxStateChecked : M13CheckboxStateUnchecked;
    [self.isIncludeDebtCheckbox addTarget:self action:@selector(isIncludeDebtCheckboxAction:) forControlEvents:UIControlEventValueChanged];

    self.debtSummLabel.text = [NSString stringWithFormat:@"%@ KZT", [@(fabs([self.service.debtSumm doubleValue])) decimalFormatString]];

    //initial total sum with respect to debt
   // [self setTotalSum:[self.service.paySum doubleValue]];



}

@end