//
// Created by Mustafin Askar on 16/09/15.
//

#import <PureLayout/ALView+PureLayout.h>
#import "AlsekoFormedAndExpireDatesView.h"
#import "Invoice.h"
#import "NSDate+Ext.h"
#import "UnderlinedTitleButton.h"

@interface AlsekoFormedAndExpireDatesView() {}
@property (nonatomic, strong) Invoice *invoice;
@property (nonatomic, strong) NSString *invoiceState;
@property(nonatomic, strong) NSLayoutConstraint *bottomConstraint;
@property(nonatomic, strong) UILabel *expiresDateLabel;
@end

@implementation AlsekoFormedAndExpireDatesView {}


- (id)initWithInvoice:(Invoice *)invoice invoiceState:(NSString *)invoiceState {
    self = [super initForAutoLayout];
    if (self) {
        self.invoiceState = invoiceState;
        self.invoice = invoice;
        [self configUI];
    }
    return self;
}

#pragma mark - config actions

- (void)openHistoryAction {
    if (self.onOpenHistoryButtonTap) {
        self.onOpenHistoryButtonTap();
    }
}

- (void)setOnOpenHistoryButtonTap:(void (^)())onOpenHistoryButtonTap {
    _onOpenHistoryButtonTap = onOpenHistoryButtonTap;
    UIButton *openHistoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [openHistoryButton autoSetDimension:ALDimensionHeight toSize:40];
    [openHistoryButton autoSetDimension:ALDimensionWidth toSize:40];
    [openHistoryButton setImage:[UIImage imageNamed:@"icon-statement-green"] forState:UIControlStateNormal];
    [openHistoryButton addTarget:self action:@selector(openHistoryAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:openHistoryButton];
    [openHistoryButton autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
    [openHistoryButton autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:13];

    UnderlinedTitleButton *openHistoryLink = [UnderlinedTitleButton buttonWithType:UIButtonTypeSystem];
    [openHistoryLink setTitle:@"История оплаты" forState:UIControlStateNormal];
    openHistoryLink.titleLabel.textAlignment = NSTextAlignmentLeft;
    [openHistoryLink addTarget:self action:@selector(openHistoryAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:openHistoryLink];
    [openHistoryLink autoSetDimension:ALDimensionHeight toSize:20];
    [openHistoryLink autoSetDimension:ALDimensionWidth toSize:120];
    [openHistoryLink autoPinEdge:ALEdgeTop toEdge:ALAttributeBottom ofView:self.expiresDateLabel withOffset:5];
    [openHistoryLink autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:15];
    [NSLayoutConstraint deactivateConstraints:@[self.bottomConstraint]];
    self.bottomConstraint = [openHistoryLink autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:10];

}

#pragma mark - config ui

- (void)configUI {
    self.backgroundColor = [UIColor whiteColor];

    UILabel *formedDateLabel = [[UILabel alloc] init];
    formedDateLabel.font = [UIFont systemFontOfSize:15];
    formedDateLabel.text = [self.invoice getLongFormedDateString];
    [self addSubview:formedDateLabel];

    self.expiresDateLabel = [[UILabel alloc] init];
    self.expiresDateLabel.font = [UIFont systemFontOfSize:13];
    self.expiresDateLabel.textColor = [UIColor lightGrayColor];
    NSString *expireDateString = [self.invoice.expireDate stringWithDateFormat:@"dd.MM.yyyy"];
    int daysLeftInt = (int)[NSDate daysBetweenDate:[NSDate date] andDate:self.invoice.expireDate];
    NSString *daysLeft;
    if (daysLeftInt > 0) {
        daysLeft = [NSString stringWithFormat:@"(%@ %i %@)", NSLocalizedString(@"left", nil), daysLeftInt,[NSDate dayPrefixFromDayNum:daysLeftInt]];
    } else {
        daysLeft = @"";
    }
    self.expiresDateLabel.text = [NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"credit_info_pay_due", nil), expireDateString, daysLeft];
    [self addSubview:self.expiresDateLabel];

    UILabel *stateLabel = [[UILabel alloc] init];
    stateLabel.font = [UIFont systemFontOfSize:13];
    stateLabel.text = [NSString stringWithFormat:@"(%@)", [self getMessageWithState:self.invoiceState]];
    [self addSubview:stateLabel];

    //autolayout code
    [formedDateLabel autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:15];
    [formedDateLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
    [self.expiresDateLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:formedDateLabel withOffset:5];
    [self.expiresDateLabel autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:15];
    self.bottomConstraint = [self.expiresDateLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:10];
    [stateLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:formedDateLabel withOffset:10];
    [stateLabel autoConstrainAttribute:ALAttributeBottom toAttribute:ALAttributeBottom ofView:formedDateLabel];

    [self autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];
}

#pragma mark - helper

- (NSString *)getMessageWithState:(NSString *)state {

    if ([state isEqualToString:@"expired"]) {
        return NSLocalizedString(@"expired", nil);
    } else if ([state isEqualToString:@"ready"]) {
        return NSLocalizedString(@"ready", nil);
    } else if ([state isEqualToString:@"inprogress"]) {
        return NSLocalizedString(@"inprogress", nil);
    } else if ([state isEqualToString:@"payed"]) {
        return NSLocalizedString(@"payed1", nil);
    } else if ([state isEqualToString:@"payed1"]) {
        return NSLocalizedString(@"payed1", nil);
    } else if ([state isEqualToString:@"payed2"]) {
        return NSLocalizedString(@"payed1", nil);
    } else if ([state isEqualToString:@"rejected"]) {
        return NSLocalizedString(@"rejected", nil);
    } else {
        return NSLocalizedString(@"unknown_status", nil);
    }
}

#pragma mark -

- (void)dealloc {}

@end