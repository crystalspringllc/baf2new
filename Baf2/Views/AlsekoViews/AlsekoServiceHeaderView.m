//
// Created by Mustafin Askar on 12/09/15.
//

#import <PureLayout/ALView+PureLayout.h>
#import "AlsekoServiceHeaderView.h"
#import "SuperTextFieldView.h"
#import "AlsekoConstants.h"
#import "M13Checkbox.h"
#import "Service.h"
#import "NSNumber+Ext.h"
#import "SuperTextFieldView+Styles.h"

@interface AlsekoServiceHeaderView() {}
@property (nonatomic, strong) Service *service;
@property (nonatomic, strong) NSString *titleString;
@property (nonatomic, strong) M13Checkbox *isIncludeSwitch;
@property (nonatomic, strong) UIButton *expandButton;

@property (nonatomic, strong) UIView *disclaimerView;
@property (nonatomic, strong) UILabel *disclaimerLabel;
@end

@implementation AlsekoServiceHeaderView {}


- (id)initWithService:(Service *)service {
    self = [super initForAutoLayout];
    if (self) {
        self.service = service;

        self.titleString = service.serviceName;
        [self configUI];
        [self applyData];

    }
    return self;
}

#pragma mark - animations

- (void)animateExpandButtonImage:(BOOL)isUp {
    UIImage *iconDown = [UIImage imageNamed:@"icon-open-accordion"];
    if (!isUp) {
        iconDown = [iconDown imageRotatedByDegrees:180];
    }
    [self.expandButton setImage:iconDown forState:UIControlStateNormal];
}

#pragma mark -
#pragma mark - config actions

- (void)isIncludeSwitchAction:(M13Checkbox *)isIncludeSwitch {
    if (self.onIncludeInBillValueChanged) {
        self.onIncludeInBillValueChanged(isIncludeSwitch.checkState == M13CheckboxStateChecked);
    }
}

- (void)expandButtonAction {
    if (self.onExpandButtonClick) {
        self.onExpandButtonClick();
    }
}

- (void)changePayStateWithAutoCalc:(BOOL)autocalc {
    if (autocalc) {
        if (self.service.isCounterService) {
            [self.paySumTextField enabled:NO];
            [self.paySumTextField disabledStyle];
        } else {
            [self.paySumTextField enabled:YES];
            [self.paySumTextField defaultStyle];
        }
    } else {
        [self.paySumTextField enabled:YES];
        [self.paySumTextField defaultStyle];
    }
}


- (void)setPaySumTextFieldValue:(NSNumber *)paySumTextFieldValue {
    self.paySumTextField.value = [paySumTextFieldValue decimalFormatString];
}


#pragma mark - apply data

- (void)applyData {

    //Включить в квитанцию
    if (self.service.includeInBill) {
        self.isIncludeSwitch.checkState = M13CheckboxStateChecked;
    } else {
        self.isIncludeSwitch.checkState = M13CheckboxStateUnchecked;
    }


    //"Использовать авторасчет"
    [self changePayStateWithAutoCalc:self.service.autocalc];
}

#pragma mark - config ui

- (void)configUI {
    __weak AlsekoServiceHeaderView *wSelf = self;


    self.backgroundColor = [UIColor whiteColor];

    [self autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];

    self.isIncludeSwitch = [[M13Checkbox alloc] initWithTitle:self.titleString];
    self.isIncludeSwitch.titleLabel.font = [UIFont systemFontOfSize:13];
    self.isIncludeSwitch.titleLabel.numberOfLines = 0;
    self.isIncludeSwitch.checkAlignment = M13CheckboxAlignmentLeft;
    self.isIncludeSwitch.checkColor = [UIColor bafBlueColor];
    self.isIncludeSwitch.strokeColor = [UIColor bafBlueColor];

    [self.isIncludeSwitch addTarget:self action:@selector(isIncludeSwitchAction:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:self.isIncludeSwitch];

    self.expandButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.expandButton setImage:[UIImage imageNamed:@"icon-open-accordion"] forState:UIControlStateNormal];
    [self.expandButton addTarget:self action:@selector(expandButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.expandButton];

    self.paySumTextField = [[SuperTextFieldView alloc] init];
    [self.paySumTextField setPostfix:@"KZT"];
    self.paySumTextField.textField.textAlignment = NSTextAlignmentRight;
    [self.paySumTextField makeNumberKeyboard];
    [self.paySumTextField onValueChanged:^(NSString *value) {
        if (wSelf.onPaysumTextFieldValueChaged) {

            NSString *val = [value stringByReplacingOccurrencesOfString:@"," withString:@"."];
            val = [val stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSNumber *paySumValue = @([val doubleValue]);
            wSelf.onPaysumTextFieldValueChaged(paySumValue);

        }
    }];
    [self addSubview:self.paySumTextField];

    UILabel *inputTitle = [[UILabel alloc] init];
    inputTitle.font = kInputTitleFont;
    inputTitle.textColor = kAlsekoInputTitleLabelColor;
    inputTitle.text = NSLocalizedString(@"payment_amount", nil);
    [self addSubview:inputTitle];

    self.disclaimerView = [[UIView alloc] init];
    [self.disclaimerView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:self.disclaimerView];
    
    self.disclaimerLabel = [[UILabel alloc] init];
    [self.disclaimerLabel setTextColor:[UIColor lightGrayColor]];
    [self.disclaimerLabel setTextAlignment:NSTextAlignmentCenter];
    [self.disclaimerLabel setFont:[UIFont systemFontOfSize:14]];
    [self.disclaimerLabel setText:@"РЕДАКТИРОВАНИЕ ЗАПРЕЩЕНО: \nТарифы не были переданы поставщиком услуги. По вопросам расчета стоимости обратитесь к поставщику услуги."];
    [self.disclaimerLabel setNumberOfLines:4];
    [self.disclaimerView addSubview:self.disclaimerLabel];

    //
    //AUTOLAYOUT
    [self.isIncludeSwitch autoSetDimension:ALDimensionWidth toSize:250];
    [self.isIncludeSwitch autoSetDimension:ALDimensionHeight toSize:40];
    [self.isIncludeSwitch autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:5];
    [self.isIncludeSwitch autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:15];
    [self.expandButton autoSetDimension:ALDimensionWidth toSize:kExpandButtonWidth];
    [self.expandButton autoSetDimension:ALDimensionHeight toSize:kExpandButtonHeight];
    [self.expandButton autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
    [self.expandButton autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:10];
    [self.paySumTextField autoSetDimension:ALDimensionWidth toSize:kPaySumTextFieldWidth];
    [self.paySumTextField autoSetDimension:ALDimensionHeight toSize:kPaySumTextFieldHeight];
    [self.paySumTextField autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:10];
    [self.paySumTextField autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.isIncludeSwitch withOffset:20];
    [inputTitle autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.paySumTextField withOffset:5];
    [inputTitle autoPinEdge:ALEdgeRight toEdge:ALEdgeLeft ofView:self.paySumTextField withOffset:-10];
    
    //Для disclaimer-a "Редактирование запрещено"
    if(self.service.isCounterService && (self.service.tariff == nil || [self.service.tariff integerValue] == 0) && self.service.tariff2 == nil){
        [self.expandButton setEnabled:NO];
        [self.disclaimerView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.paySumTextField];
        [self.disclaimerView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
        [self.disclaimerView autoPinEdgeToSuperviewEdge:ALEdgeRight];
        [self.disclaimerView autoSetDimension:ALDimensionHeight toSize:120];
        [self.disclaimerView autoPinEdgeToSuperviewEdge:ALEdgeBottom];
        
        [self.disclaimerLabel autoPinEdgeToSuperviewEdge:ALEdgeTop];
        [self.disclaimerLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft];
        [self.disclaimerLabel autoPinEdgeToSuperviewEdge:ALEdgeRight];
        [self.disclaimerLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom];//SUPERVIEW BOTTOM
    }else{
        [self.expandButton setEnabled:YES];
        [self.paySumTextField autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:15]; //SUPERVIEW BOTTOM
    }

}

#pragma mark - get

- (NSString *)getPayTextWithoutSpaces {
    NSString *withoutSpaces = [self.paySumTextField.value stringByReplacingOccurrencesOfString:@" " withString:@""];
    withoutSpaces = [withoutSpaces stringByReplacingOccurrencesOfString:@"," withString:@"."];
    return withoutSpaces;
}

- (NSNumber *)getPaySumNumber {
    NSString *withoutSpaces = [self.paySumTextField.value stringByReplacingOccurrencesOfString:@" " withString:@""];
    withoutSpaces = [withoutSpaces stringByReplacingOccurrencesOfString:@"," withString:@"."];
    /**
     *
     *Если галочка не выбрана для сервиса paySum = 0
     *
     */
    if (self.isIncludeSwitch.checkState != M13CheckboxStateChecked) {
        return @(0);
    }
    return @([withoutSpaces doubleValue]);
}


@end
