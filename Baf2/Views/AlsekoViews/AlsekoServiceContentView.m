//
// Created by Mustafin Askar on 14/09/15.
//

#import <M13Checkbox/M13Checkbox.h>
#import "AlsekoServiceContentView.h"
#import "SuperTextFieldView.h"
#import "Service.h"
#import "NSDate+Ext.h"
#import "NSString+Ext.h"

#define kSignificativeViewTitleFont [UIFont systemFontOfSize:12]
#define kInnerElementWidth ([ASize screenWidth]/3) - 10

@interface  AlsekoServiceContentView() {}
@property (nonatomic, strong) Service *service;
@property (nonatomic, strong) SuperTextFieldView *lastCountTextField;
@property (nonatomic, strong) SuperTextFieldView *fixCountTextField;
@property (nonatomic, strong) SuperTextFieldView *prevCountTextField;
@property (nonatomic, strong) UIButton *runningAvegateDateButton;
@property (nonatomic, strong) AADatePicker *datePicker;
@end

@implementation AlsekoServiceContentView {}

- (id)initWithService:(Service *)service {
    self = [super initForAutoLayout];
    if (self) {
        self.service = service;
        [self configUI];
    }
    return self;
}

#pragma mark - config setters and getters

- (BOOL)getAutoCalcBool {
    return self.useAutoCountCheckbox.checkState == M13CheckboxStateChecked;
}

- (NSDate *)getLastCountDate {
    NSDate *lastCountDate = [self.runningAvegateDateButton.titleLabel.text dateWithDateFormat:@"dd.MM.yyyy"];
    return lastCountDate;
}

- (NSString *)getFixCount {
    return [@([self.fixCountTextField doubleValue]) decimalFormatString];
}

- (NSString *)getLastCount {
    return [@([self.lastCountTextField doubleValue]) decimalFormatString];
}

- (NSString *)getPrevCount{
    return [@([self.prevCountTextField doubleValue]) decimalFormatString];
}

- (NSNumber *)getLastCountNumber {
    return @([self.lastCountTextField doubleValue]);
}

- (NSNumber *)getFixCountNumber {
    return @([self.fixCountTextField doubleValue]);
}

- (NSNumber *)getPrevCountNumber{
    return @([self.prevCountTextField doubleValue]);
}

#pragma mark - config action and callbacks

- (void)selectLastCountDate {
    NSDate *initialDate = [self.runningAvegateDateButton.titleLabel.text dateWithDateFormat:@"dd.MM.yyyy"];

    if (!self.datePicker) {
        self.datePicker = [[AADatePicker alloc] init];
        self.datePicker.delegate = self;
    }
    [self.datePicker showWithDate:initialDate];

}

- (void)setOnCountTextFieldsValueChanged:(void (^)(double, NSDate *))onCountTextFieldsValueChanged {
    _onCountTextFieldsValueChanged = onCountTextFieldsValueChanged;
    [self lastCountTextFieldValueChanged];
    [self countTextFieldValueChanged];
}

- (void)useAutoContCheckboxAction:(M13Checkbox *)checkbox {    

    if (checkbox.checkState == M13CheckboxStateChecked) {
        [self.prevCountTextField enabled:NO];
    }else{
        [self.prevCountTextField enabled:YES];
    }
    
    self.service.autocalc = checkbox.checkState == M13CheckboxStateChecked;

    if (self.onUseAutoCountValueChanged) {
        self.onUseAutoCountValueChanged(checkbox.checkState == M13CheckboxStateChecked);
    }
}

- (void)lastCountTextFieldValueChanged {
    double lastCount = self.lastCountTextField.doubleValue;
    double prevCount = self.prevCountTextField.doubleValue;

    if (lastCount > 0 || prevCount > 0) {
        double calculatedFixSum = [Service calculateFixCountWithLastCount:lastCount andPrevCount:prevCount];
        self.fixCountTextField.value = [@(calculatedFixSum) decimalFormatString];
    }
}

- (void)fixSumTextFieldsValueChanged {
    double fixSum = self.fixCountTextField.doubleValue;
    double prevCount = self.prevCountTextField.doubleValue;
    double calculatedLastCount = [Service calculateLastCountWithFixCount:fixSum andPrevCount:prevCount];
    self.lastCountTextField.value = [NSString stringWithFormat:@"%i", (int)calculatedLastCount];
}

- (void)prevCountTextFieldValueChanged {
    double prevCount = self.prevCountTextField.doubleValue;
    double lastCount = self.lastCountTextField.doubleValue;

    if (lastCount > 0 || prevCount > 0) {
        double calculatedFixSum = [Service calculateFixCountWithLastCount:lastCount andPrevCount:prevCount];
        self.fixCountTextField.value = [@(calculatedFixSum) decimalFormatString];
    }
}

- (void)countTextFieldValueChanged {
    if (self.onCountTextFieldsValueChanged) {
        double fixSum = self.fixCountTextField.doubleValue;
        self.onCountTextFieldsValueChanged(fixSum, [self getLastCountDate]);
    }
}

#pragma mark - AADatePicker delegate

- (void)aaDatePicker:(AADatePicker *)datePicker dateChanged:(NSDate *)date {
// do nothing
}

- (void)aaDatePickerDoneTapped:(AADatePicker *)datePicker {
    [self.runningAvegateDateButton setTitle:[datePicker.date stringWithDateFormat:@"dd.MM.yyyy"] forState:UIControlStateNormal];

    if (self.onCountTextFieldsValueChanged) {
        double fixSum = self.fixCountTextField.doubleValue;
        self.onCountTextFieldsValueChanged(fixSum, datePicker.date);
    }
}

#pragma mark - config ui

- (void)configUI {

    __weak AlsekoServiceContentView *wSelf = self;

    self.backgroundColor = [UIColor headerBgColor];

    [self autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];

    //Использовать авторасчет
    self.useAutoCountCheckbox = [[M13Checkbox alloc] initWithTitle:NSLocalizedString(@"use_autocompute", nil)];
    self.useAutoCountCheckbox.checkAlignment = M13CheckboxAlignmentLeft;
    self.useAutoCountCheckbox.titleLabel.font = [UIFont systemFontOfSize:14];
    self.useAutoCountCheckbox.checkHeight = 18;
    self.useAutoCountCheckbox.checkColor = [UIColor bafBlueColor];
    self.useAutoCountCheckbox.strokeColor = [UIColor bafBlueColor];
    [self.useAutoCountCheckbox addTarget:self action:@selector(useAutoContCheckboxAction:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:self.useAutoCountCheckbox];
    [self.useAutoCountCheckbox autoSetDimension:ALDimensionHeight toSize:20];
    [self.useAutoCountCheckbox autoSetDimension:ALDimensionWidth toSize:250];
    [self.useAutoCountCheckbox autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:15];
    [self.useAutoCountCheckbox autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:15];

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Закологки показателей (Текущие, Предыдущие, Обьем)
    UIView *significativeView = [[UIView alloc] initForAutoLayout];
    significativeView.backgroundColor = [UIColor headerBgColor];
    [self addSubview:significativeView];
    [significativeView autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];
    [significativeView autoSetDimension:ALDimensionHeight toSize:44];
    [significativeView autoPinEdgeToSuperviewEdge:ALEdgeLeading];
    [significativeView autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
    [significativeView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.useAutoCountCheckbox withOffset:20];

    UIView *firstColumn = [[UIView alloc] init];
    UIView *secondColumn = [[UIView alloc] init];
    UIView *thirdColumn = [[UIView alloc] init];
    [significativeView addSubview:firstColumn];
    [significativeView addSubview:secondColumn];
    [significativeView addSubview:thirdColumn];

    [firstColumn autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]/3];
    [firstColumn autoPinEdgeToSuperviewEdge:ALEdgeLeading];
    [firstColumn autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [secondColumn autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]/3];
    [secondColumn autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:firstColumn];
    [secondColumn autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [thirdColumn autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]/3];
    [thirdColumn autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [thirdColumn autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:secondColumn];


    UILabel *runningAverageTitle = [[UILabel alloc] init];
    runningAverageTitle.font = kSignificativeViewTitleFont;
    runningAverageTitle.numberOfLines = 0;
    runningAverageTitle.lineBreakMode = NSLineBreakByWordWrapping;
    runningAverageTitle.textAlignment = NSTextAlignmentCenter;
    runningAverageTitle.text = NSLocalizedString(@"current_index", nil);
    [firstColumn addSubview:runningAverageTitle];
    [runningAverageTitle autoSetDimension:ALDimensionWidth toSize:kInnerElementWidth];
    [runningAverageTitle autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [runningAverageTitle autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];

    UILabel *previousAverageTitle = [[UILabel alloc] init];
    previousAverageTitle.font = kSignificativeViewTitleFont;
    previousAverageTitle.numberOfLines = 0;
    previousAverageTitle.lineBreakMode = NSLineBreakByWordWrapping;
    previousAverageTitle.textAlignment = NSTextAlignmentCenter;
    previousAverageTitle.text = NSLocalizedString(@"prev_index", nil);
    [secondColumn addSubview:previousAverageTitle];
    [previousAverageTitle autoSetDimension:ALDimensionWidth toSize:kInnerElementWidth];
    [previousAverageTitle autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [previousAverageTitle autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];

    //fixCount / Обьем потреб.
    UILabel *consumptionValueTitle = [[UILabel alloc] init];
    consumptionValueTitle.font = kSignificativeViewTitleFont;
    consumptionValueTitle.numberOfLines = 0;
    consumptionValueTitle.lineBreakMode = NSLineBreakByWordWrapping;
    consumptionValueTitle.textAlignment = NSTextAlignmentCenter;
    consumptionValueTitle.text = NSLocalizedString(@"consumpion_valume", nil);
    [thirdColumn addSubview:consumptionValueTitle];
    [consumptionValueTitle autoSetDimension:ALDimensionWidth toSize:kInnerElementWidth - 30];
    [consumptionValueTitle autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [consumptionValueTitle autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    UIView *downContent = [[UIView alloc] init];
    [self addSubview:downContent];
    [downContent autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:significativeView];
    [downContent autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];
    [downContent autoPinEdgeToSuperviewEdge:ALEdgeLeading];
    [downContent autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
    [downContent autoPinEdgeToSuperviewEdge:ALEdgeBottom];

    UIView *firstDownContentColumn = [[UIView alloc] init];
    [downContent addSubview:firstDownContentColumn];
    UIView *secondDownContentColumn = [[UIView alloc] init];
    [downContent addSubview:secondDownContentColumn];
    UIView *thirdDownContentColumn = [[UIView alloc] init];
    [downContent addSubview:thirdDownContentColumn];


    [firstDownContentColumn autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]/3];
    [firstDownContentColumn autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [firstDownContentColumn autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    [firstDownContentColumn autoPinEdgeToSuperviewEdge:ALEdgeLeading];

    [secondDownContentColumn autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]/3];
    [secondDownContentColumn autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [secondDownContentColumn autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    [secondDownContentColumn autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:firstDownContentColumn];

    [thirdDownContentColumn autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]/3];
    [thirdDownContentColumn autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [thirdDownContentColumn autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    [thirdDownContentColumn autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:secondDownContentColumn];
    [thirdDownContentColumn autoPinEdgeToSuperviewEdge:ALEdgeTrailing];



    ////lastCount / Текущие показатели
    self.lastCountTextField = [[SuperTextFieldView alloc] init];
    self.lastCountTextField.backgroundColor = [UIColor whiteColor];
    [self.lastCountTextField makeNumberKeyboard];
    [self.lastCountTextField onValueChange:(SuperTextFieldViewValueChangeBlock) ^{
            [wSelf lastCountTextFieldValueChanged];
        }];
    [self.lastCountTextField onValueChanged:^(NSString *value) {
        [wSelf countTextFieldValueChanged];
    }];
    [firstDownContentColumn addSubview:self.lastCountTextField];
    [self.lastCountTextField autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
    [self.lastCountTextField autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [self.lastCountTextField autoSetDimension:ALDimensionWidth toSize:kInnerElementWidth];
    [self.lastCountTextField autoSetDimension:ALDimensionHeight toSize:30];

    //lastCountDate
    self.runningAvegateDateButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.runningAvegateDateButton.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.runningAvegateDateButton setTitle:[self.service.lastCountDate stringWithDateFormat:@"dd.MM.yyyy"]
                                   forState:UIControlStateNormal];
    [self.runningAvegateDateButton addTarget:self action:@selector(selectLastCountDate) forControlEvents:UIControlEventTouchUpInside];
    [firstDownContentColumn addSubview:self.runningAvegateDateButton];
    [self.runningAvegateDateButton autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [self.runningAvegateDateButton autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.lastCountTextField withOffset:20];
    [self.runningAvegateDateButton autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20];// SUPERVIEW BOTTOM



////////////////////////////////////////////////////////////
    //fixedCount ()
    self.fixCountTextField = [[SuperTextFieldView alloc] init];
    self.fixCountTextField.backgroundColor = [UIColor whiteColor];
    [self.fixCountTextField makeNumberKeyboard];
    [self.fixCountTextField onValueChange:(SuperTextFieldViewValueChangeBlock) ^{
            [wSelf fixSumTextFieldsValueChanged];
        }];
    [self.fixCountTextField onValueChanged:^(NSString *value) {
        [wSelf countTextFieldValueChanged];
    }];
    [thirdDownContentColumn addSubview:self.fixCountTextField];
    [self.fixCountTextField autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [self.fixCountTextField autoSetDimension:ALDimensionWidth toSize:kInnerElementWidth];
    [self.fixCountTextField autoSetDimension:ALDimensionHeight toSize:30];
    [self.fixCountTextField autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];

////////////////////////////////////////////////////////////
    
    //prevCount / Предыдущие показатели
    //    self.prevCountLabel = [UILabel newAutoLayoutView];
    //    self.prevCountLabel.font = kSignificativeViewTitleFont;
    //    [secondDownContentColumn addSubview:self.prevCountLabel];
    //    [self.prevCountLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
    //    [self.prevCountLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
    //    [self.prevCountLabel autoSetDimension:ALDimensionHeight toSize:30];
    self.prevCountTextField = [SuperTextFieldView newAutoLayoutView];
    self.prevCountTextField.backgroundColor = [UIColor whiteColor];
    [self.prevCountTextField makeNumberKeyboard];
    [self.prevCountTextField hideClearButton];
    [self.prevCountTextField enabled:NO];

    [self.prevCountTextField onValueChange:^(NSString *value) {
        [wSelf prevCountTextFieldValueChanged];
    }];
    [self.prevCountTextField onValueChanged:^(NSString *value) {
        [wSelf prevCountTextFieldValueChanged];
    }];
    [secondDownContentColumn addSubview:self.prevCountTextField];
    [self.prevCountTextField autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
    [self.prevCountTextField autoAlignAxisToSuperviewAxis:ALAxisVertical];
   // [self.prevCountTextField autoAlignAxis:ALEdgeRight toSameAxisOfView:self.lastCountTextField withOffset:10];
   // [self.prevCountTextField autoAlignAxis:ALEdgeLeft toSameAxisOfView:self.fixCountTextField withOffset:10];
    [self.prevCountTextField autoSetDimension:ALDimensionHeight toSize:30];
////////////////////////////////////////////////////////////
    
    //prevCountDate
    UILabel *previousAverageDateLabel = [[UILabel alloc] init];
    previousAverageDateLabel.font = kSignificativeViewTitleFont;
    previousAverageDateLabel.text = [self.service.prevCountDate stringWithDateFormat:@"dd.MM.yyyy"];
    [secondDownContentColumn addSubview:previousAverageDateLabel];
    [previousAverageDateLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [previousAverageDateLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.prevCountTextField withOffset:15];
    [previousAverageDateLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20];


    //measure
    UILabel *consumptionValueDenomination = [[UILabel alloc] init];
    consumptionValueDenomination.font = kSignificativeViewTitleFont;
    consumptionValueDenomination.text = self.service.measure;
    [thirdDownContentColumn addSubview:consumptionValueDenomination];
    [consumptionValueDenomination autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [consumptionValueDenomination autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.fixCountTextField withOffset:10];
    [consumptionValueDenomination autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20];


    //
    // Setting data from self.service
    //
    if (self.service.isCounterService) {
        self.useAutoCountCheckbox.checkState = self.service.autocalc ? M13CheckboxStateChecked : M13CheckboxStateUnchecked;
    } else {
        self.useAutoCountCheckbox.hidden = YES;
    }
    
    self.lastCountTextField.value = [self.service.lastCount stringValue];
    self.prevCountTextField.value = [self.service.prevCount stringValue];
    self.fixCountTextField.value = [self.service.fixCount stringValue];
    
//    if(self.service.isCounterService && (self.service.tariff == nil || [self.service.tariff integerValue] == 0) && self.service.tariff2 == nil)
//    {
//        [self.lastCountTextField enabled:NO];
//        [self.prevCountTextField enabled:NO];
//        [self.fixCountTextField enabled:NO];
//        [self.useAutoCountCheckbox setEnabled:NO];
//    }

}

@end
