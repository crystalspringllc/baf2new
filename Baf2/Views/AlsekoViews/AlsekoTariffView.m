//
// Created by Mustafin Askar on 21/09/15.
//

#import "AlsekoTariffView.h"
#import "ServiceTariff2.h"


#define kShowTariffTitle @"Показать тарификацию"
#define kHideTariffTitle @"Скрыть тарификацию"

@interface AlsekoTariffView() {}

@property (nonatomic, strong) id serviceTariff;

@property (nonatomic, strong) NSString *measure;

@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) NSLayoutConstraint *bottomConstrain;
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) UILabel *maxLabel;
@property (nonatomic, strong) UILabel *middleLabel;
@property (nonatomic, strong) UILabel *minLabel;
@end

@implementation AlsekoTariffView {
    BOOL isExpand;
}

- (id)initWithServiceTariff:(id)serviceTariff measure:(NSString *)measure {
    self = [super initForAutoLayout];
    if (self) {
        self.serviceTariff = serviceTariff;
        self.measure = measure;
        [self configUI];
    }
    return self;
}

#pragma mark - setters

- (void)setMinThresholdLabelText:(double)minThreshold minLim:(double)minLim {
    if ([self.serviceTariff isKindOfClass:[ServiceTariff2 class]]) {
        ServiceTariff2 *tarif = (ServiceTariff2 *) self.serviceTariff;

        NSString *minThresholdString = [NSString stringWithFormat:@"%i", (int)minThreshold];

        self.minLabel.text = [NSString stringWithFormat:@"1 ур. - до %i кВт·ч\n"
                                                                "%@ кВт·ч x %@тг.",
                        (int) minLim,
                                                        minThresholdString,
                                                        [tarif.minTariffValue decimalFormatString]];
    }
}

- (void)setMiddleThresholdLabelText:(double)middleThreshold minLim:(double)minLim middleLim:(double)middleLim {

    if ([self.serviceTariff isKindOfClass:[ServiceTariff2 class]]) {

        ServiceTariff2 *tarif = (ServiceTariff2 *) self.serviceTariff;

        NSString *middleThresholdString = [NSString stringWithFormat:@"%i", (int) middleThreshold];

        self.middleLabel.text = [NSString stringWithFormat:@"2 ур. - %i - %i кВт·ч\n"
                                                                   "%@ кВт·ч x %@тг.",
                        (int)minLim,
                        (int)middleLim,
                                                           middleThresholdString,
                                                           [tarif.middleTariffValue decimalFormatString]];
    }
}

- (void)setMaxThresholdLabelText:(double)maxThreshold middleLim:(double)middleLim {

    if ([self.serviceTariff isKindOfClass:[ServiceTariff2 class]]) {

        ServiceTariff2 *tarif = (ServiceTariff2 *) self.serviceTariff;

        NSString *maxThresholdString = [NSString stringWithFormat:@"%i", (int) maxThreshold];

        self.maxLabel.text = [NSString stringWithFormat:@"3 ур. - свыше %i кВт·ч\n"
                                                                "%@ кВт·ч x %@тг.",
                        (int)middleLim,
                                                        maxThresholdString,
                                                        [tarif.maxTariffValue decimalFormatString]];
    }
}

#pragma mark - config ui

- (void)configUI {
    self.backgroundColor = [UIColor headerBgColor];

    [self autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];

    self.button = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.button setTitle:kShowTariffTitle forState:UIControlStateNormal];
    [self.button addTarget:self action:@selector(showContent) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.button];

    [self.button autoSetDimension:ALDimensionHeight toSize:44];
    [self.button autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];
    [self.button autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [self.button autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:5];
    self.bottomConstrain = [self.button autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:5];


    //apply data to the view
    if ([self.serviceTariff isKindOfClass:[ServiceTariff2 class]]) {
        self.contentView = [self buildContentWithServiceTariff2];
    } else if ([self.serviceTariff isKindOfClass:[NSNumber class]]) {
        self.contentView = [self buildContentWithServiceTariff];
    }

    self.contentView.alpha = 0;
    [self addSubview:self.contentView];

}

- (UIView *)buildContentWithServiceTariff {

    NSNumber *tarifNumber = (NSNumber *) self.serviceTariff;

    UIView *view = [[UIView alloc] init];
    [view autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];

    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor grayColor];
    label.text = [NSString stringWithFormat:@"%@ %@", [tarifNumber stringValue], self.measure ? self.measure : @""];
    [view addSubview:label];

    [label autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:15];
    [label autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
    [label autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:10];

    return view;
}

- (UIView *)buildContentWithServiceTariff2 {
    ServiceTariff2 *tarif = (ServiceTariff2 *) self.serviceTariff;


    UIView *view = [[UIView alloc] init];
    [view autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];

    self.minLabel = [[UILabel alloc] init];
    self.minLabel.textColor = [UIColor grayColor];
    self.minLabel.numberOfLines = 0;
    self.minLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.minLabel.text = [NSString stringWithFormat:@"1 ур. - до %@ кВт·ч\n"
            "%@ кВт·ч x %@тг.", [tarif.minTariffThreshold stringValue], [tarif.minTariffThreshold stringValue], [tarif.minTariffValue decimalFormatString]];
    [view addSubview:self.minLabel];
    [self.minLabel autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:15];
    [self.minLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];


    self.middleLabel = [[UILabel alloc] init];
    self.middleLabel.textColor = [UIColor grayColor];
    self.middleLabel.numberOfLines = 0;
    self.middleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.middleLabel.text = [NSString stringWithFormat:@"2 ур. - %@ - %@ кВт·ч\n"
            "%@ кВт·ч x %@тг.", [tarif.minTariffThreshold stringValue], [tarif.middleTariffThreshold stringValue], [tarif.middleTariffThreshold stringValue], [tarif.middleTariffValue decimalFormatString]];
    [view addSubview:self.middleLabel];
    [self.middleLabel autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:15];
    [self.middleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.minLabel withOffset:10];


    self.maxLabel = [[UILabel alloc] init];
    self.maxLabel.textColor = [UIColor grayColor];
    self.maxLabel.numberOfLines = 0;
    self.maxLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.maxLabel.text = [NSString stringWithFormat:@"3 ур. - свыше %@ кВт·ч\n"
            "%@ кВт·ч x %@тг.", [tarif.middleTariffThreshold stringValue], @"0", [tarif.maxTariffValue decimalFormatString]];
    [view addSubview:self.maxLabel];
    [self.maxLabel autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:15];
    [self.maxLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.middleLabel withOffset:10];
    [self.maxLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:10];

    return view;
}

#pragma mark - animations

- (void)showContent {
    [self.bottomConstrain autoRemove];
    self.bottomConstrain = nil;

    [self layoutIfNeeded];

    if (!isExpand) {
        [self.button setTitle:kHideTariffTitle forState:UIControlStateNormal];
        isExpand = YES;

        //add content View and set constraints

        [self.contentView autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [self.contentView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.button];
        self.bottomConstrain = [self.contentView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:10]; //SUPERVIEW BOTTOM

        [UIView animateWithDuration:0.2 animations:^{
            [self layoutIfNeeded];
        } completion:^(BOOL finished) {
            self.contentView.alpha = 1;
        }];

    } else {
        isExpand = NO;
        [self.button setTitle:kShowTariffTitle forState:UIControlStateNormal];

        self.bottomConstrain = [self.button autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
        self.contentView.alpha = 0;

        [UIView animateWithDuration:0.2 animations:^{
            [self layoutIfNeeded];
        }];
    }
}

@end