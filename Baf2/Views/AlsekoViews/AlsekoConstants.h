#define kLeftInset                           15
#define kExpandButtonWidth                   30
#define kExpandButtonHeight                  30
#define kTitleLabelWidth                     250
#define kHorizonatalSpaceBetweenElements     10
#define kTopInset                            10
#define kVerticalSpaceBetweenElements        20
#define kPaySumTextFieldWidth                200
#define kPaySumTextFieldHeight               30
#define kInputTitleFont                      [UIFont systemFontOfSize:14]
#define kAlsekoInputTitleLabelColor          [UIColor lightGrayColor]
#define kTitleLabelFont                      [UIFont systemFontOfSize:14]
#define kAlsekoGreenLabelColor               [UIColor fromRGB:0x359C76]