//
// Created by Mustafin Askar on 11/09/15.
//

#import <M13Checkbox/M13Checkbox.h>
#import "AlsekoServiceView.h"
#import "AlsekoServiceHeaderView.h"
#import "AlsekoServiceContentView.h"
#import "Service.h"
#import "AlsekoTotalBottomView.h"
#import "AlsekoTariffView.h"
#import "Param.h"
#import "NSDate+Ext.h"
#import "ServiceTariff2.h"
#import "SuperTextFieldView.h"

@interface AlsekoServiceView () {}
@property (nonatomic, strong) Service *service;
@property (nonatomic, strong) Param *numberOfLivings;
@property (nonatomic, strong) NSLayoutConstraint *tempConstrain;
@end

@implementation AlsekoServiceView {
    BOOL isExpanded;
}

- (id)initWithService:(Service *)service andNumberOfLivings:(Param *)numberOfLivings {
    self = [super init];
    if (self) {
        self.service = service;
        self.numberOfLivings = numberOfLivings;
        self.isIncludeBill = self.service.includeInBill;

        [self configUI];
        [self configActions];


        [self calculateTotalSumWithFixCount:[self.service.fixCount doubleValue] lastCountDate:self.service.lastCountDate];

    }
    return self;
}

#pragma mark - config actions

- (void)configActions {
    __weak AlsekoServiceView *wSelf = self;

    //Расщирить вьшку сервиса
    self.alsekoServiceHeaderView.onExpandButtonClick = ^{
        [wSelf expand];
    };

    //Включить квитанцию
    self.alsekoServiceHeaderView.onIncludeInBillValueChanged = ^(BOOL switchState) {

        wSelf.isIncludeBill = switchState;

        if (wSelf.onIncludeInBillValueChanged) {
            wSelf.onIncludeInBillValueChanged(switchState, wSelf);
        }

    };

    //Оплачиваю textfield value chages
    self.alsekoServiceHeaderView.onPaysumTextFieldValueChaged = ^(NSNumber *changedPaySumValue) {
        [wSelf calculateTotalSumWithChangePaySumValue:changedPaySumValue];
    };


    //Показать тариф
    self.alsekoTariffView.onTarifInfoButtonClick = ^{
        if (wSelf.onTarifInfoViewShow) {
            wSelf.onTarifInfoViewShow();
        }
    };

    //Учесть долг
    self.alsekoTotalBottomView.onIncludeDebtCheckboxValueChanged = ^{

        if (wSelf.alsekoServiceContentView.useAutoCountCheckbox.checkState == M13CheckboxStateChecked) {

            double fixCount = [[wSelf.alsekoServiceContentView getFixCount] doubleValue];
            NSDate *lastCountDate = [wSelf.alsekoServiceContentView getLastCountDate];
            [wSelf calculateTotalSumWithFixCount:fixCount lastCountDate:lastCountDate];

        } else {

            double paySum = [[wSelf.alsekoServiceHeaderView getPayTextWithoutSpaces] doubleValue];
            [wSelf calculateTotalSumWithChangePaySumValue:@(paySum)];

        }

    };

    //Включилося/выключился автоподсчет
    self.alsekoServiceContentView.onUseAutoCountValueChanged = ^(BOOL useAutoCount) {
        [wSelf changeHeaderPayTextFieldState];
        if (useAutoCount) {
            double fixCount = [[wSelf.alsekoServiceContentView getFixCount] doubleValue];
            NSDate *lastCountDate = [wSelf.alsekoServiceContentView getLastCountDate];
            [wSelf calculateTotalSumWithFixCount:fixCount lastCountDate:lastCountDate];
        } else {
            double paySum = [[wSelf.alsekoServiceHeaderView getPayTextWithoutSpaces] doubleValue];
            [wSelf calculateTotalSumWithChangePaySumValue:@(paySum)];
        }
    };

    //Изменения значения поле текущие показатели, обьем потребления и текущей даты
    self.alsekoServiceContentView.onCountTextFieldsValueChanged = ^(double fixCount, NSDate *lastCountDate) {
        if (wSelf.alsekoServiceContentView.useAutoCountCheckbox.checkState == M13CheckboxStateChecked) {
            [wSelf calculateTotalSumWithFixCount:fixCount lastCountDate:lastCountDate];
        }
    };
}

- (void)changeHeaderPayTextFieldState {
    if (self.service.autocalc) {
//        double fixCount = [[self.alsekoServiceContentView getFixCount] doubleValue];
        //[self calculateTotalSumWithFixCount:fixCount lastCountDate:self.alsekoServiceContentView.getLastCountDate];
    }
    [self.alsekoServiceHeaderView changePayStateWithAutoCalc:self.service.autocalc];
}


/**
*
*   CALCULATION METHODS
*
*
*
*
*/

- (void)calculateTotalSumWithChangePaySumValue:(NSNumber *)changedPaySumValue {

    if (self.alsekoTotalBottomView.isIncludeDebtCheckbox.checkState == M13CheckboxStateChecked) {
        if(!self.service.isCounterService){
            double d = [changedPaySumValue doubleValue] - [self.service.debtSumm doubleValue];
            [self.alsekoServiceHeaderView setPaySumTextFieldValue:@(d < 0 ? 0 : d)];
        }else{
            double d = [changedPaySumValue doubleValue];// - [self.service.debtSumm doubleValue];
            [self.alsekoServiceHeaderView setPaySumTextFieldValue:@(d < 0 ? 0 : d)];
        }
    } else {
        if(!self.service.isCounterService){
            double d = [changedPaySumValue doubleValue] + [self.service.debtSumm doubleValue];
            [self.alsekoServiceHeaderView setPaySumTextFieldValue:@(d)];
        }else{
            double d = [changedPaySumValue doubleValue];// + [self.service.debtSumm doubleValue];
            [self.alsekoServiceHeaderView setPaySumTextFieldValue:@(d)];
        }

    }

    self.totalSum = @([[self.alsekoServiceHeaderView getPayTextWithoutSpaces] doubleValue]);

    if (self.onTotalSumValueChanged) {
        self.onTotalSumValueChanged();
    }

}

//ВЫЗЫВАЕТСЯ В САМОМ НАЧАЛЕ
//ИЗМЕНЕНИЯ ISINCLUDEDEBT IF AUTOCOUNT ON
//ИЗМЕНЕНИЯ IF AUTOCALC ON
//ИЗМЕНЕНИЯ COUNTTEXTFIELD
- (void)calculateTotalSumWithFixCount:(double)fixCount lastCountDate:(NSDate *)lastCountDate {
    double totalSumVal = 0;

    if (self.service.isCounterService) {
        if (self.service.autocalc) {
            if ([self.service.tariff2.maxTariffValue intValue] > 0) {


                totalSumVal = [self.service calcTotalSumWithFixCount:fixCount numberLivings:[self.numberOfLivings.parValue integerValue] lastDate:lastCountDate threshholdValues:^(double min, double middle, double max, double minLim, double middleLim) {
                    [self.alsekoTariffView setMinThresholdLabelText:min minLim:minLim];
                    [self.alsekoTariffView setMiddleThresholdLabelText:middle minLim:minLim middleLim:middleLim];
                    [self.alsekoTariffView setMaxThresholdLabelText:max middleLim:middleLim];
                }];

                self.totalSum = @(totalSumVal);

                if (self.alsekoTotalBottomView.isIncludeDebtCheckbox.checkState == M13CheckboxStateChecked) {

                        //Переплата & Долг
                        double d = [self.totalSum doubleValue] - [self.service.debtSumm doubleValue];
                        [self.alsekoServiceHeaderView setPaySumTextFieldValue:@(d < 0 ? 0 : d)];

                } else {
                    if (self.alsekoServiceContentView.useAutoCountCheckbox.checkState == M13CheckboxStateChecked){
                        double d = [self.totalSum doubleValue];
                        [self.alsekoServiceHeaderView setPaySumTextFieldValue:@(d < 0 ? 0 : d)];
                    }else{
                        double d = [[self.alsekoServiceHeaderView getPayTextWithoutSpaces] doubleValue];
                        [self.alsekoServiceHeaderView setPaySumTextFieldValue:@(d < 0 ? 0 : d)];
                    }
                }

            } else if ([self.service.tariff intValue] >= 0) {

                //также как и если isCounterService = YES кроме расчета с тарифом

                totalSumVal = fixCount * [self.service.tariff doubleValue];
                self.totalSum = @(totalSumVal);


                if (self.alsekoTotalBottomView.isIncludeDebtCheckbox.checkState == M13CheckboxStateChecked) {

                    //Переплата & Долг
                    double d = [self.totalSum doubleValue] - [self.service.debtSumm doubleValue];
                    [self.alsekoServiceHeaderView setPaySumTextFieldValue:@(d < 0 ? 0 : d)];

                } else {

                    double d = [self.totalSum doubleValue];
                    [self.alsekoServiceHeaderView setPaySumTextFieldValue:@(d < 0 ? 0 : d)];

                }
            }else{
                double d = [self.service.paySum doubleValue];
                [self.alsekoServiceHeaderView setPaySumTextFieldValue:@(d < 0 ? 0 : d)];
            }

        } else {
            [self.alsekoServiceHeaderView setPaySumTextFieldValue:self.service.paySum];
            //[self.alsekoServiceHeaderView setPaySumTextFieldValue:self.service.fixSum];

        }
    } else {

            [self.alsekoServiceHeaderView setPaySumTextFieldValue:self.service.paySum];

    }

    //Для disclaimer-a "Редактирование запрещено"
    if(self.service.isCounterService && (self.service.tariff == nil || [self.service.tariff integerValue] == 0) && self.service.tariff2 == nil)
    {
        [self.alsekoServiceHeaderView setPaySumTextFieldValue:self.service.paySum];
//        [self.alsekoServiceHeaderView.paySumTextField.textField setTextColor:[UIColor lightGrayColor]];
//        [self.alsekoServiceHeaderView.paySumTextField setPostfix:@"KZT" textColor:[UIColor lightGrayColor]];
    }
    
    //SET ON TOTAL
    self.totalSum = @([[self.alsekoServiceHeaderView getPayTextWithoutSpaces] doubleValue]);
    
    if (self.onTotalSumValueChanged) {
        self.onTotalSumValueChanged();
    }
}

#pragma mark - config ui

- (void)configUI {

    self.backgroundColor = [UIColor whiteColor];
    [self autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];


    //Header view:
    self.alsekoServiceHeaderView = [[AlsekoServiceHeaderView alloc] initWithService:self.service];
    [self addSubview:self.alsekoServiceHeaderView];
    [self.alsekoServiceHeaderView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
    self.tempConstrain = [self.alsekoServiceHeaderView autoPinEdgeToSuperviewEdge:ALEdgeBottom];


    //Content view
    self.alsekoServiceContentView = [[AlsekoServiceContentView alloc] initWithService:self.service];
    self.alsekoServiceContentView.alpha = 0;
    [self addSubview:self.alsekoServiceContentView];


    //Tarif view
    self.alsekoTariffView = [[AlsekoTariffView alloc] initWithServiceTariff:self.service.tariff2 ?: self.service.tariff measure:self.service.measure];
    self.alsekoTariffView.alpha = 0;
    [self addSubview:self.alsekoTariffView];


    //Bottom view
    self.alsekoTotalBottomView = [[AlsekoTotalBottomView alloc] initWithService:self.service];
    self.alsekoTotalBottomView.alpha = 0;
    [self addSubview:self.alsekoTotalBottomView];


}

#pragma mark - animations

/***
*   Add ContentView and then expand it. Like accordion
*/
- (void)expand {

    [self.tempConstrain autoRemove];
    self.tempConstrain = nil;

    [self layoutIfNeeded];

    if (!isExpanded) {

        if (self.service.isCounterService) {

            [self.alsekoServiceContentView autoAlignAxisToSuperviewAxis:ALAxisVertical];
            [self.alsekoServiceContentView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.alsekoServiceHeaderView];
            [self.alsekoTariffView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.alsekoServiceContentView withOffset:0];
            [self.alsekoTariffView autoPinEdgeToSuperviewEdge:ALEdgeLeading];
            [self.alsekoTotalBottomView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.alsekoTariffView withOffset:0];
            self.tempConstrain = [self.alsekoTotalBottomView autoPinEdgeToSuperviewEdge:ALEdgeBottom];

            [UIView animateWithDuration:0.3 animations:^{
                self.alsekoServiceContentView.alpha = 1;
                self.alsekoTariffView.alpha = 1;
                self.alsekoTotalBottomView.alpha = 1;
            } completion:^(BOOL finished) {
                [self layoutIfNeeded];
            }];

        } else {
            [self.alsekoTariffView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.alsekoServiceHeaderView withOffset:0];
            [self.alsekoTariffView autoPinEdgeToSuperviewEdge:ALEdgeLeading];
            [self.alsekoTotalBottomView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.alsekoTariffView withOffset:0];
            [self.alsekoTotalBottomView autoAlignAxisToSuperviewAxis:ALAxisVertical];
            self.tempConstrain = [self.alsekoTotalBottomView autoPinEdgeToSuperviewEdge:ALEdgeBottom];

            [UIView animateWithDuration:0.3 animations:^{
                self.alsekoTariffView.alpha = 1;
                self.alsekoTotalBottomView.alpha = 1;
            } completion:^(BOOL finished) {
                [self layoutIfNeeded];
            }];
        }

        [self.alsekoServiceHeaderView animateExpandButtonImage:NO];
        isExpanded = YES;

    } else {

        self.tempConstrain = [self.alsekoServiceHeaderView autoPinEdgeToSuperviewEdge:ALEdgeBottom];

        [UIView animateWithDuration:0.3 animations:^{
            self.alsekoServiceContentView.alpha = 0;
            self.alsekoTariffView.alpha = 0;
            self.alsekoTotalBottomView.alpha = 0;
        } completion:^(BOOL finished) {
            [self layoutIfNeeded];
        }];

        [self.alsekoServiceHeaderView animateExpandButtonImage:YES];
        isExpanded = NO;

    }
}


#pragma mark -


- (NSNumber *)getServiceId {
    return self.service.serviceId;
}

/**
*
*   Compose NSDictionary to be saved
*
*/
- (NSDictionary *)getDataToBeSaved {
    NSMutableDictionary *dataToSave = [[NSMutableDictionary alloc] init];

    if (self.service.tariff2 || [self.alsekoServiceContentView getLastCount]) {
        dataToSave[@"lastCountDate"] = [[self.alsekoServiceContentView getLastCountDate] stringWithDateFormat:@"yyyyMMddHHmmss"];
        dataToSave[@"lastCount"] = [self.alsekoServiceContentView getLastCount];
        dataToSave[@"fixCount"] = [self.alsekoServiceContentView getFixCount];
    }

    dataToSave[@"paySum"] = [self.alsekoServiceHeaderView getPayTextWithoutSpaces];

    BOOL isIncludeDebt;
    if (![self.service.debtSumm isEqualToNumber:@0]) {
         isIncludeDebt = self.alsekoTotalBottomView.isIncludeDebtCheckbox.checkState == M13CheckboxStateChecked;
    } else {
        isIncludeDebt = self.service.includeDebt;
    }


    dataToSave[@"includeDebt"] = @(isIncludeDebt);
    dataToSave[@"includeInBill"] = @(self.isIncludeBill);
    dataToSave[@"autocalc"] = @(self.alsekoServiceContentView.getAutoCalcBool);

    return dataToSave;
}

- (Service *)getServiceDataToBeSaved {
    if (self.service.tariff2 || [self.alsekoServiceContentView getLastCount]) {
        self.service.lastCountDate = [self.alsekoServiceContentView getLastCountDate];
        self.service.lastCount = [self.alsekoServiceContentView getLastCountNumber];
        self.service.fixCount = [self.alsekoServiceContentView getFixCountNumber];
        self.service.prevCount = [self.alsekoServiceContentView getPrevCountNumber];
    }

    self.service.paySum = [self.alsekoServiceHeaderView getPaySumNumber];

    BOOL isIncludeDebt;
    if (![self.service.debtSumm isEqualToNumber:@0]) {
        isIncludeDebt = self.alsekoTotalBottomView.isIncludeDebtCheckbox.checkState == M13CheckboxStateChecked;
    } else {
        isIncludeDebt = self.service.includeDebt;
    }



    self.service.includeDebt = isIncludeDebt;
    self.service.includeInBill = self.isIncludeBill;
    self.service.autocalc = self.alsekoServiceContentView.getAutoCalcBool;

    return self.service;
}


- (void)dealloc {}

@end
