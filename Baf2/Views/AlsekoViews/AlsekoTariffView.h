//
// Created by Mustafin Askar on 21/09/15.
//

#import <Foundation/Foundation.h>

@class ServiceTariff2;


@interface AlsekoTariffView : UIView


/**
*   @function Initialize with serviceTarriff and measure
*   @param serviceTariff can receive ServiceTariff2 type
*       or NSNumber for single tariffs
*   @param measure - receive service.measure value
*   @return id
*/
- (id)initWithServiceTariff:(id)serviceTariff measure:(NSString *)measure;


/**
*   @function set min threshlods
*
*/
- (void)setMinThresholdLabelText:(double)minThreshold minLim:(double)minLim;

/**
*   @function set middle threshlods
*
*/
- (void)setMiddleThresholdLabelText:(double)middleThreshold minLim:(double)minLim middleLim:(double)middleLim;


/**
*   @function set max threshlods
*
*/
- (void)setMaxThresholdLabelText:(double)maxThreshold middleLim:(double)middleLim;


/**
*   block call when tarif info button tapped
*
*/
@property (nonatomic, copy) void (^onTarifInfoButtonClick)();


@end