//
// Created by Mustafin Askar on 12/09/15.
//

#import <Foundation/Foundation.h>

@class M13Checkbox;
@class SuperTextFieldView;
@class Service;

@interface AlsekoServiceHeaderView : UIView


@property (nonatomic, strong) SuperTextFieldView *paySumTextField;


/**
*  This block calls when includeInBill checkbox state changed
*
*  All actions send to the AlsekoServiceView class,
*  where will business logic occurs
*
*/
@property (nonatomic, copy) void (^onIncludeInBillValueChanged)(BOOL switchState);


/**
*  This block calls when expand button click
*/
@property (nonatomic, copy) void (^onExpandButtonClick)();


/**
*
* On pay sum textfield value changed
*
*/
@property (nonatomic, copy) void (^onPaysumTextFieldValueChaged)(NSNumber *changedPaySumValue);


/**
*
*
*/
- (id)initWithService:(Service *)service;


/**
*
*
*/
- (void)animateExpandButtonImage:(BOOL)isUp;


/**
*  Public method to enable/disable paySumTextField
*/
- (void)changePayStateWithAutoCalc:(BOOL)autocalc;


/**
*
*
*/
- (void)setPaySumTextFieldValue:(NSNumber *)paySumTextFieldValue;

#pragma mark -

- (NSString *)getPayTextWithoutSpaces;
- (NSNumber *)getPaySumNumber;

@end