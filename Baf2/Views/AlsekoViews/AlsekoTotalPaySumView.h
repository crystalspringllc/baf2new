//
// Created by Mustafin Askar on 16/09/15.
//

#import <Foundation/Foundation.h>


@interface AlsekoTotalPaySumView : UIView

- (id)initWithTotalPaySum:(double)paySum;

- (void)setInvoiceHistoryState:(NSString *)invoiceHistoryState;

@property (nonatomic, assign) double paySum;
@property (nonatomic, copy) void (^onGoToPayButtonClick)();
@property (nonatomic, copy) void (^onSaveButtonClick)();
@property (nonatomic, copy) void (^onRejectButtonClick)(BOOL isRejectInvoice);

@end