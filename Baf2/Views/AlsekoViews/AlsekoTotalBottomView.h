//
// Created by Mustafin Askar on 21/09/15.
//

#import <Foundation/Foundation.h>

@class M13Checkbox;
@class Service;


@interface AlsekoTotalBottomView : UIView


@property (nonatomic, strong) M13Checkbox *isIncludeDebtCheckbox;

/**
*   @function Initialize with service instance
*   @param service - loaded service
*   @return id
*/
- (id)initWithService:(Service *)service;

/**
*   @block Consider debt or overpayment
*
*/
@property (nonatomic, copy) void (^onIncludeDebtCheckboxValueChanged)();

@end