//
// Created by Mustafin Askar on 14/09/15.
//

#import <Foundation/Foundation.h>
#import "AADatePicker.h"

@class Service;
@class SuperTextFieldView;


@interface AlsekoServiceContentView : UIView <AADatePickerDelegate>

@property (nonatomic, strong) M13Checkbox *useAutoCountCheckbox;

/**
*
*   This block calls when useAutoCalc checkbox calue is changed
*
*/
@property (nonatomic, copy) void (^onUseAutoCountValueChanged)(BOOL useAutoCount);


/**
*
*   This block calls when count textfield's value changed
*
*/
@property (nonatomic, copy) void (^onCountTextFieldsValueChanged)(double fixSum, NSDate *lastCountDate);


/**
*
*  Initialize with service
*
*/
- (id)initWithService:(Service *)service;


/**
*
*  Set value to lastCount, prevCount and fixSum textfields and labels
*
*/
//- (void)setLastCount:(double)lastCount prevCount:(double)prevCount fixSum:(double)fixSum;



#pragma mark - get values for saveInvoice method

- (BOOL)getAutoCalcBool;
- (NSDate *)getLastCountDate;
- (NSString *)getFixCount;
- (NSString *)getLastCount;
- (NSString *)getPrevCount;

- (NSNumber *)getLastCountNumber;
- (NSNumber *)getFixCountNumber;
- (NSNumber *)getPrevCountNumber;
@end
