//
// Created by Mustafin Askar on 11/09/15.
//

#import <Foundation/Foundation.h>
#import <PureLayout/PureLayout.h>

@class Service;
@class AlsekoServiceHeaderView;
@class AlsekoServiceContentView;
@class AlsekoTariffView;
@class AlsekoTotalBottomView;
@class Param;


@interface AlsekoServiceView : UIView


/**
*   Initialize with service
*
*/
- (id)initWithService:(Service *)service andNumberOfLivings:(Param *)numberOfLivings;


/**
*  SERVICE PAY SUM. USE FOR TOTAL SUM
*
*/
@property (nonatomic, strong) NSNumber *totalSum;


/**
*   Include Service in bill
*
*/
@property (nonatomic, assign) BOOL isIncludeBill;


/**
*   AlsekoServiceHeaderView
*   View contains Checkbox for isInclude in bill flag
*   and textfield for entering sum that user whants to pay
*
*/
@property (nonatomic, strong) AlsekoServiceHeaderView *alsekoServiceHeaderView;


/**
*   AlsekoServiceContentView
*
*/
@property (nonatomic, strong) AlsekoServiceContentView *alsekoServiceContentView;


/**
*    AlsekoTariffView
*
*/
@property (nonatomic, strong) AlsekoTariffView *alsekoTariffView;


/**
*   AlsekoTotalBottomView
*
*/
@property (nonatomic, strong) AlsekoTotalBottomView *alsekoTotalBottomView;


/**
*   Block calls when isIncludeInBill checkbox triggered
*   Sends checkbox state
*/
@property (nonatomic, copy) void (^onIncludeInBillValueChanged)(BOOL switchState, AlsekoServiceView *alsekoServiceView);


/**
*   Block calls when tarif view show
*
*/
@property (nonatomic, copy) void (^onTarifInfoViewShow)();


/**
*
*   Need calc total sum in AlsekoReceiptViewController
*
*/
@property (nonatomic, copy) void (^onTotalSumValueChanged)();



/**
*   Getting service id
*
*/
- (NSNumber *)getServiceId;

/**
*   Compose data that going to save
*/
- (NSDictionary *)getDataToBeSaved;
- (Service *)getServiceDataToBeSaved;



@end
