//
// Created by Mustafin Askar on 16/09/15.
//

#import <Foundation/Foundation.h>

@class Invoice;
@class InvoiceHistory;


@interface AlsekoFormedAndExpireDatesView : UIView

- (id)initWithInvoice:(Invoice *)invoice invoiceState:(NSString *)invoiceState;

@property (nonatomic, copy) void (^onOpenHistoryButtonTap)();

@end