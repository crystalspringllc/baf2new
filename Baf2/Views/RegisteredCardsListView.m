//
//  RegisteredCardsListView.m
//  BAF
//
//  Created by Almas Adilbek on 8/22/14.
//
//

#import <AFNetworking/UIKit+AFNetworking.h>
#import "RegisteredCardsListView.h"
#import "ViewsListView.h"
#import "CardsApi.h"
#import "ExtCard.h"

#define kPadding 10
#define kListPadding 10

@implementation RegisteredCardsListView

- (id)initWithCards:(NSArray *)cards title:(NSString *)title {
    self = [super initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 1)];
    if (self) {

        self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

        NSMutableArray * rowViews = [NSMutableArray array];
        for (ExtCard *card in cards) {

            UIImageView *icon = [[UIImageView alloc] init];
            icon.width = 20;
            icon.height = 20;
            [icon setImageWithURL:[[NSURL alloc] initWithString:card.logo] placeholderImage:[UIImage imageNamed:@"icon-icon-placeholder"]];

            ProductRowView *rowView = [[ProductRowView alloc] initWithTitle:card.alias subtitle:card.descriptionText icon:icon warningMessage:[self getStatusMessageOfCard:card]];

            rowView.delegate = self;
            rowView.data = card;
            [rowView showDisclosure];
            if (card.isFavorite) [rowView setFavorited:YES];
            [rowViews addObject:rowView];
        }
        
        ViewsListView *listView = [ViewsListView new];
        listView = [listView viewsListWithViews:rowViews];
        [self addSubview:listView];

        self.height = listView.bottom;

    }
    return self;
}

- (NSString *)getStatusMessageOfCard:(ExtCard *)registeredCard {
    if (registeredCard.epayApproved) {
        return nil;
    }
    if ([registeredCard.status isEqual:kStatusEpayActivationFailed]) {
        return NSLocalizedString(@"finish_registration", nil);
    } else if([registeredCard.status isEqual:kStatusCardExpired]) {
        return NSLocalizedString(@"card_expired", nil);
    } else {
        return NSLocalizedString(@"enter_activation_codee", nil);
    }
}

- (void)productRowViewTapped:(ProductRowView *)rowView {
    [self.delegate registeredCardsListViewProductRowViewDidTapped:rowView];
}

@end
