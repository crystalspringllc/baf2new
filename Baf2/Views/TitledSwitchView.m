//
//  TitledSwitchView.m
//  Baf2
//
//  Created by Askar Mustafin on 3/28/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//


#import <ChameleonFramework/ChameleonMacros.h>
#import "UIView+ConcisePureLayout.h"
#import "TitledSwitchView.h"

@implementation TitledSwitchView {
    BOOL didConfig; // flag is used to not call configUI mutliple times when creating through xib
}

- (id)init {
    self = [[NSBundle mainBundle] loadNibNamed:@"TitledSwitchView" owner:self options:nil][0];
    if (self) {
        [self configUI];
        didConfig = true;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configUI];
    didConfig = true;
}

#pragma mark - config actions

- (void)materialSwitchValueDidChanged:(UISwitch *)materialSwitch {
    if (self.onSwitchValueChanged) {
        self.onSwitchValueChanged(materialSwitch.isOn);
    }
}

#pragma mark - config ui

- (void)configUI {
    if (!didConfig) {
        self.titleLabel.textColor = [UIColor flatSkyBlueColorDark];
        self.titleLabel.font = [UIFont systemFontOfSize:13];

        self.materialSwitch = [[UISwitch alloc] init];
        self.materialSwitch.backgroundColor = [UIColor clearColor];

        self.materialSwitch.onTintColor = [UIColor flatSkyBlueColorDark];
        //self.materialSwitch.tintColor = [UIColor fromRGB:0x1F987C];


        [self.aSwitch addSubview:self.materialSwitch];
        self.aSwitch.backgroundColor = [UIColor clearColor];
        [self.materialSwitch aa_superviewFillWithInset:0];
        [self.materialSwitch addTarget:self action:@selector(materialSwitchValueDidChanged:) forControlEvents:UIControlEventValueChanged];
    }
}

#pragma mark - setters

- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = _title;
}

- (void)setIsOn:(BOOL)isOn {
    [self.materialSwitch setOn:isOn animated:YES];
}

#pragma mark - getters 

- (BOOL)isOn {
    return self.materialSwitch.isOn;
}

@end
