//
//  NewsTableCell.h
//  Baf2
//
//  Created by nmaksut on 16.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"

@interface NewsTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel     *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel     *commentsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIButton *likesButton;
@property (weak, nonatomic) IBOutlet UIView *innerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *underImageViewConstraint;
@property (nonatomic, copy) void (^tapLikeButton)(NewsTableCell *selfCell);

- (void)setNews:(News *)news;

@end
