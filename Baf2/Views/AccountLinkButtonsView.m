//
//  AccountLinkButtonsView.m
//  Baf2
//
//  Created by Shyngys Kassymov on 23.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "AccountLinkButtonsView.h"

@implementation AccountLinkButtonsView

- (id)init {
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"AccountLinkButtonsView"
                                                      owner:self
                                                    options:nil];
    for (id view in nibViews) {
        if ([view isKindOfClass:[UIView class]]) {
            return view;
        }
    }
    return nil;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.leftButton setTitle:NSLocalizedString(@"go_to_payments", nil) forState:UIControlStateNormal];
    [self.rightButton setTitle:NSLocalizedString(@"go_to_translations", nil) forState:UIControlStateNormal];
}

@end
