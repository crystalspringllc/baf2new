//
//  UnderlinedTitleButton.m
//  Baf2
//
//  Created by Shyngys Kassymov on 12.07.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "UnderlinedTitleButton.h"

@implementation UnderlinedTitleButton

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setTitle:[self titleForState:UIControlStateNormal] forState:UIControlStateNormal];
}

- (void)setTitle:(nullable NSString *)title forState:(UIControlState)state {
    UIColor *stateColor = [self titleColorForState:state];
    UIFont *font = self.titleLabel.font;
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName: font, NSForegroundColorAttributeName: stateColor, NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
    [super setAttributedTitle:attributedTitle forState:state];
}

- (void)setAttributedTitle:(NSAttributedString *)title forState:(UIControlState)state {
    UIColor *stateColor = [self titleColorForState:state];
    UIFont *font = self.titleLabel.font;
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title.string attributes:@{NSFontAttributeName: font, NSForegroundColorAttributeName: stateColor, NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
    [super setAttributedTitle:attributedTitle forState:state];
}

@end
