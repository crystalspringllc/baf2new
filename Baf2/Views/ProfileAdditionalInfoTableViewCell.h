//
//  ProfileAdditionalInfoTableViewCell.h
//  Baf2
//
//  Created by Shyngys Kassymov on 20.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JVFloatLabeledTextField;

@interface ProfileAdditionalInfoTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet JVFloatLabeledTextField *lastnameTextField;
@property (nonatomic, weak) IBOutlet JVFloatLabeledTextField *firstnameTextField;
@property (nonatomic, weak) IBOutlet JVFloatLabeledTextField *middlenameTextField;
@property (nonatomic, weak) IBOutlet JVFloatLabeledTextField *birthdateTextField;

@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, copy) void (^didChangeText)(JVFloatLabeledTextField *field);

@end
