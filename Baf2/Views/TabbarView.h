//
//  TabbarView.h
//  BAF
//
//  Created by Almas Adilbek on 10/21/14.
//
//

#import "TabView.h"

@protocol TabbarViewDelegate;

@interface TabbarView : UIView <TabViewDelegate>

@property(nonatomic, weak) id <TabbarViewDelegate> delegate;
@property(nonatomic, assign) NSInteger selectedIndex;

+ (CGFloat)tabbarHeight;

@end

@protocol TabbarViewDelegate<NSObject>
-(void)tabbarViewTappedAtIndex:(NSInteger)index;
@end
