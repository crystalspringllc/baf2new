//
//  TitleWithView.m
//  AstanaKzNationalControl
//
//  Created by Almas Adilbek on 5/13/13.
//  Copyright (c) 2013 Astana.kz. All rights reserved.

#define kViewTag 10
//

#import "TitleWithView.h"
#import "UIColor+Extensions.h"
#import <FrameAccessor.h>

@implementation TitleWithView

- (id)initWithWidth:(CGFloat)width title:(NSString *)title {
    return [self initWithFrame:CGRectMake(0, 0, width, 1) title:title];
}

- (id)initWithFrame:(CGRect)frame title:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {

        _viewPaddingTop = 5;

        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.numberOfLines = 0;
        _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _titleLabel.font = [UIFont systemFontOfSize:15];
        _titleLabel.textColor = [UIColor fromRGB:0x323232];
        [self addSubview:_titleLabel];

        [self setTitle:title];
    }
    return self;
}

-(void)insertView:(UIView *)_view
{
    UIView *oldView = [self viewWithTag:kViewTag];
    if(oldView) {
        [oldView removeFromSuperview];
    }
    _view.tag = kViewTag;
    CGRect f = _view.frame;
    f.origin.y = _titleLabel.bottom + _viewPaddingTop;
    _view.frame = f;
    [self addSubview:_view];
    
    f = self.frame;
    f.size.height = _view.frame.origin.y + _view.frame.size.height;
    self.frame = f;
}

- (void)setTitle:(NSString *)title
{
    _titleLabel.width = self.width;
    _titleLabel.text = title;
    [_titleLabel sizeToFit];

    UIView *view = [self viewWithTag:kViewTag];
    if(view) {
        view.y = _titleLabel.bottom + _viewPaddingTop;
    }
}


#pragma mark -
#pragma mark Predefined views

-(void)setLabelWithText:(NSString *)_text
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,  _titleLabel.bottom + _viewPaddingTop, self.bounds.size.width, 1)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:17];
    label.textColor = [UIColor fromRGB:0x333333];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.text = _text;
    [label sizeToFit];
    
    [self insertView:label];
}

@end
