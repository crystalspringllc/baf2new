//
//  StatusLabel.m
//  Baf2
//
//  Created by Shyngys Kassymov on 15.02.17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "StatusLabel.h"

@implementation StatusLabel

- (instancetype)initWithType:(StatusType)statusType {
    self = [super initWithFrame:CGRectZero];
    
    if (self) {
        self.statusType = statusType;
        self.font = [UIFont systemFontOfSize:14];
        self.numberOfLines = 0;
        self.lineBreakMode = NSLineBreakByWordWrapping;
        self.layer.cornerRadius = 3;
        self.layer.masksToBounds = true;
    }
    
    return self;
}

- (void)drawTextInRect:(CGRect)rect {
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInsets)];
}
    
- (CGSize)intrinsicContentSize {
    CGSize size = [super intrinsicContentSize];
    size.width  += self.edgeInsets.left + self.edgeInsets.right;
    size.height += self.edgeInsets.top + self.edgeInsets.bottom;
    return size;
}
    
- (void)layoutSubviews {
    [super layoutSubviews];
    self.preferredMaxLayoutWidth = self.frame.size.width;
}

#pragma mark - Methods

- (void)setHTMLText:(NSString *)html {
    NSError *err = nil;
    NSMutableAttributedString *string =  [[NSMutableAttributedString alloc] initWithData:[html dataUsingEncoding:NSUnicodeStringEncoding]
                                                                                 options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                                                      documentAttributes:nil
                                                                                   error:&err];
    CTFontRef systemFont = CTFontCreateWithName((__bridge CFStringRef)[UIFont systemFontOfSize:14].fontName, [UIFont systemFontOfSize:14].pointSize, NULL);
    if (systemFont) {
        [string addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)systemFont range:NSMakeRange(0, string.length)];
        CFRelease(systemFont);
        
    }
    [self setText:string afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        [mutableAttributedString addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[UIColor fromRGB:0x4c4c4c].CGColor range:NSMakeRange(0, string.length)];
        
        return mutableAttributedString;
    }];
    
    if (err) {
        NSLog(@"Unable to parse label text: %@", err);
    }
}

#pragma mark - Setters

- (void)setStatusType:(StatusType)statusType {
    _statusType = statusType;
    
    switch (statusType) {
        case Warning:
        {
            self.backgroundColor = [UIColor appDisclaimerYellowColor];
            self.textColor = [UIColor appBrownColor];
        }
        break;
        case Error:
        {
            self.backgroundColor = [UIColor fromRGB:0xF2DEDE];
            self.textColor = [UIColor fromRGB:0xA00000];
        }
        break;
        case Info:
        {
            self.backgroundColor = [UIColor appBlueColor];
            self.textColor = [UIColor whiteColor];
        }
        break;
        default:
        break;
    }
}

@end
