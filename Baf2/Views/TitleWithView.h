//
//  TitleWithView.h
//  AstanaKzNationalControl
//
//  Created by Almas Adilbek on 5/13/13.
//  Copyright (c) 2013 Astana.kz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleWithView : UIView {

}

@property(nonatomic, strong) UILabel *titleLabel;
@property(nonatomic, assign) CGFloat viewPaddingTop;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title;
- (id)initWithWidth:(CGFloat)width title:(NSString *)title;

-(void)insertView:(UIView *)_view;
- (void)setTitle:(NSString *)title;

// Predefined views
-(void)setLabelWithText:(NSString *)_text;

@end
