//
// Created by Almas Adilbek on 9/7/14.
// Copyright (c) 2014 Almas Adilbek. All rights reserved.
//

#import "TitleInputTextField.h"
#import "UIFont+Custom.h"
#import "Version.h"

#define kTfPadding 20
#define kHeaderPaddingTop 17
#define kHeaderPaddingBottom 5

@implementation TitleInputTextField {

}
- (id)initWithTitle:(NSString *)title {
    return [self initWithTitle:title width:[ASize screenWidth] height:40];
}

- (id)initWithWidth:(CGFloat)width
{
    return [self initWithWidth:width height:40];
}

- (id)initWithWidth:(CGFloat)width height:(CGFloat)height {
    return [self initWithTitle:nil width:width height:height];
}

- (id)initWithTitle:(NSString *)title width:(CGFloat)width height:(CGFloat)height
{
    self = [super initWithFrame:CGRectMake(0, 0, width, height)];
    if(self)
    {
        // Bg
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 1)];
        bgView.backgroundColor = [UIColor headerBgColor];
        [self addSubview:bgView];
        
        // Title
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kTfPadding, kHeaderPaddingTop, self.width, 1)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont helveticaNeue:14];
        titleLabel.textColor = [UIColor headerTitleColor];
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.numberOfLines = 0;
        titleLabel.text = title;
        [titleLabel sizeToFit];
        [self addSubview:titleLabel];

        // Field white bg
        UIView *fieldBg = [[UIView alloc] initWithFrame:CGRectMake(0, titleLabel.bottom + kHeaderPaddingBottom, width, 50)];
        fieldBg.backgroundColor = [UIColor whiteColor];
        [self addSubview:fieldBg];

        // Header bottom line
        UIView * line1 = [[UIView alloc] initWithFrame:CGRectMake(0, fieldBg.y, self.width, 0.5)];
        [line1 setBackgroundColor:[UIColor fromRGB:0xD9D9D9]];
        [self addSubview:line1];

        self.textField = [[UITextField alloc] initWithFrame:CGRectMake(kTfPadding, fieldBg.y + 5, self.width - 2 * kTfPadding, self.height)];
        _textField.font = [UIFont helveticaNeue:16];
        _textField.textColor = [UIColor fromRGB:0x7D857C];
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.backgroundColor = [UIColor whiteColor];
        _textField.delegate = self;
        [self addSubview:_textField];

        bgView.height = _textField.bottom;

        UIView * line2 = [[UIView alloc] initWithFrame:CGRectMake(0, fieldBg.bottom, self.width, 0.5)];
        [line2 setBackgroundColor:[UIColor fromRGB:0xE3E3E1]];
        [self addSubview:line2];

        self.height = fieldBg.bottom;
    }
    return self;
}

- (NSString *)value {
    if(_textField) {
        return [_textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    return @"";
}

- (void)setValue:(NSString *)value {
    if(_textField) {
        _textField.text = value;
    }
}

- (void)setPlaceholder:(NSString *)placeholder {
    if(_textField) {
        _textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: [UIColor fromRGB:0xB6BBB6]}];
    }
}


- (void)enabled:(BOOL)enabled {
    if(_textField) {
        _textField.enabled = enabled;
    }
}

- (void)setSecurity {
    if(_textField) {
        _textField.secureTextEntry = YES;
    }
}

- (void)setRightText:(NSString *)text
{
    if(_textField) {
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.font = _textField.font;
        textLabel.textColor = _textField.textColor;
        textLabel.text = text;
        [textLabel sizeToFit];
        textLabel.x = self.width - kTfPadding - textLabel.width;
        textLabel.centerY = _textField.centerY;
        [self addSubview:textLabel];

        _textField.width = self.width - 3 * kTfPadding - textLabel.width;
    }
}


- (void)setRightView:(UIView *)view {
    [self setRightView:view rightPadding:15];
}

- (void)setRightView:(UIView *)view rightPadding:(CGFloat)padding {
    view.x = self.width - view.width - padding;
    view.centerY = _textField.centerY;
    [self addSubview:view];
    _textField.width = self.width - _textField.x - (self.width - view.x);
}


- (void)focus {
    if(_textField) {
        [_textField becomeFirstResponder];
    }
}


#pragma mark - uitextfield delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (self.textFieldDidBeginEditingBlock) {
        self.textFieldDidBeginEditingBlock(textField);
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
}


@end