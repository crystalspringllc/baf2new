//
//  TitledSwitchView.h
//  Baf2
//
//  Created by Askar Mustafin on 3/28/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TitledSwitchView : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *aSwitch;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) BOOL isOn;

@property (nonatomic, strong) UISwitch *materialSwitch;

@property (nonatomic, copy) void (^onSwitchValueChanged)(BOOL isON);

@end
