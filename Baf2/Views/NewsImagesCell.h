//
//  NewsImagesCell.h
//  Baf2
//
//  Created by nmaksut on 17.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"

typedef void(^PhotoSelectionBlock)(NSInteger index);

@interface NewsImagesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic, strong) News *news;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionHeightConstraint;

@property (nonatomic, strong) PhotoSelectionBlock selectionBlock;


@end
