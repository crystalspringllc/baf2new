//
//  HeaderTitleView.h
//  BAF
//
//  Created by Almas Adilbek on 10/27/14.
//
//

#import <UIKit/UIKit.h>

@interface HeaderTitleView : UIView

- (id)initWithTitle:(NSString *)title;
+ (CGFloat)headerHeight;

@end
