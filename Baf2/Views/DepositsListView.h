//
//  DepositsListView.h
//  BAF
//
//  Created by Almas Adilbek on 8/14/14.
//
//



#import "ProductRowView.h"
#import "DepositGroup.h"

@protocol DepositsListViewDelegate;

@interface DepositsListView : UIView <ProductRowViewDelegate>

@property(nonatomic, assign) id<DepositsListViewDelegate> delegate;

- (id)initWithDepositGroup:(NSArray *)depositGroups;

@end

@protocol DepositsListViewDelegate;

@protocol DepositsListViewDelegate<NSObject>
- (void)depositsListViewProductRowViewDidTapped:(ProductRowView *)rowView;
@end