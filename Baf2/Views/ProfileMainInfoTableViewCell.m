//
//  ProfileMainInfoTableViewCell.m
//  Baf2
//
//  Created by Shyngys Kassymov on 20.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ProfileMainInfoTableViewCell.h"
#import <JVFloatLabeledTextField.h>

@implementation ProfileMainInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.nicknameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.nicknameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.nicknameTextField addTarget:self action:@selector(didChangeText:) forControlEvents:UIControlEventEditingChanged];
    
    self.phoneTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.phoneTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.phoneTextField.keyboardType = UIKeyboardTypePhonePad;
    [self.phoneTextField addTarget:self action:@selector(didChangeText:) forControlEvents:UIControlEventEditingChanged];
    
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    [self.emailTextField addTarget:self action:@selector(didChangeText:) forControlEvents:UIControlEventEditingChanged];
}

- (void)didChangeText:(JVFloatLabeledTextField *)textField {
    if (self.didChangeText) {
        self.didChangeText(textField);
    }
}

@end
