//
//  BankCardsListView.h
//  BAF
//
//  Created by Almas Adilbek on 10/2/14.
//
//

#import <UIKit/UIKit.h>
#import "ProductRowView.h"

@protocol BankCardsListViewDelegate;


@interface BankCardsListView : UIView <ProductRowViewDelegate>

@property(nonatomic, weak) id <BankCardsListViewDelegate> delegate;

- (id)initWithBankCardAccounts:(NSArray *)cardAccounts title:(NSString *)title;

@end


@protocol BankCardsListViewDelegate<NSObject>
-(void)bankCardsListViewCardTapped:(ProductRowView *)rowView;
@end
