//
//  TabView.h
//  BAF
//
//  Created by Almas Adilbek on 10/21/14.
//
//


@protocol TabViewDelegate;

@interface TabView : UIView

@property(nonatomic, weak) id <TabViewDelegate> delegate;
@property(nonatomic, assign, getter=isSelected) BOOL selected;

- (id)initWithIcon:(NSString *)iconName title:(NSString *)title;
+ (CGFloat)tabWidth;

@end


@protocol TabViewDelegate<NSObject>
-(void)tabView:(TabView *)tabView selectedTabWithTag:(NSInteger)tabTag;
@end
