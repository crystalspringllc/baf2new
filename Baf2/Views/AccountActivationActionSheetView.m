//
//  AccountActivationActionSheetView.m
//  Myth
//
//  Created by Almas Adilbek on 6/24/13.
//
//

#import "AccountActivationActionSheetView.h"

@implementation AccountActivationActionSheetView {

}

-(void)setContentView
{
    UILabel *disclamerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [self contentViewWidth], 1)];
    disclamerLabel.backgroundColor = [UIColor clearColor];
    disclamerLabel.numberOfLines = 0;
    disclamerLabel.lineBreakMode = NSLineBreakByWordWrapping;
    disclamerLabel.font = [UIFont systemFontOfSize:12];
    disclamerLabel.textColor = [UIColor darkGrayColor];
    disclamerLabel.text = NSLocalizedString(@"if_email_or_phonenumber_is_incorrect", nil);
    [disclamerLabel sizeToFit];
    
    [self insertSeparator];
    [self insertPrepareContentSubview:disclamerLabel];
    [super setContentView];
}

@end