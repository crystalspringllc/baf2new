//
//  FloatingTextField.m
//  Baf2
//
//  Created by nmaksut on 31.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "FloatingTextField.h"
#import "ChameleonMacros.h"

@implementation FloatingTextField {
    // Blocks.
    FloatingTextFieldViewOnValueChangedBlock _onValueChangedBlock;
    FloatingTextFieldViewValueChangeBlock _onValueChangeBlock;
    
    // Mix.
    NSString *previousValue;
    UIControl *tapControl;
    UILabel *messageLabel;

    UIView *separatorView;
    UIView *separatorAnimatableView;
    NSArray *separatorAnimatableViewConatraints;
}

- (id)init {
    self = [super init];
    if(self) {
        [self initView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self initView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    [self initVars];
    [self configUI];
}

- (void)initVars
{
    self.maxCharacters = NSIntegerMax;
    self.delegate = self;
    _placeholderColor = [UIColor fromRGB:0xC2C6C2];
    self.tintColor = [UIColor flatSkyBlueColorDark];
    _textFieldIconSpacing = 8;
    _textFieldRightViewSpacing = 8;
    
    self.separatorNormalColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0];
    self.separatorActiveColor = [UIColor flatSkyBlueColorDark];
}

- (void)configUI {
    self.floatingLabelActiveTextColor = [UIColor flatSkyBlueColorDark];
    
    separatorView = [UIView newAutoLayoutView];
    separatorView.backgroundColor = self.separatorNormalColor;
    [self addSubview:separatorView];
    [separatorView autoSetDimension:ALDimensionHeight toSize:1];
    [separatorView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero excludingEdge:ALEdgeTop];
    separatorView.hidden = true;
    
    separatorAnimatableView = [UIView newAutoLayoutView];
    separatorAnimatableView.backgroundColor = self.separatorNormalColor;
    [self addSubview:separatorAnimatableView];
    NSLayoutConstraint *c1 = [separatorAnimatableView autoSetDimension:ALDimensionHeight toSize:1];
    NSLayoutConstraint *c2 = [separatorAnimatableView autoSetDimension:ALDimensionWidth toSize:0];
    NSLayoutConstraint *c3 = [separatorAnimatableView autoAlignAxisToSuperviewMarginAxis:ALAxisVertical];
    NSLayoutConstraint *c4 = [separatorAnimatableView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
    separatorAnimatableView.hidden = true;
    separatorAnimatableViewConatraints = @[c1, c2, c3, c4];
    
    [self addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBeginEditing:) name:UITextFieldTextDidBeginEditingNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEndEditing:) name:UITextFieldTextDidEndEditingNotification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -
#pragma mark Actions

- (void)tap {
    // Inherit in subclasses.
}

#pragma mark -
#pragma mark TextField Methods

- (NSString *)value {
    return [self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (CGFloat)floatValue {
    NSString *stringValue = [[self value] stringByReplacingOccurrencesOfString:@"," withString:@"."];
    return [stringValue floatValue];
}

- (double)doubleValue {
    NSString *stringValue = [[self value] stringByReplacingOccurrencesOfString:@"," withString:@"."];
    return [stringValue doubleValue];
}

- (void)setValue:(NSString *)value {
    self.text = value ?: @"";
}

- (void)setPlaceholder:(NSString *)placeholder {
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: self.placeholderColor}];
}

- (void)clear {
    self.text = @"";
}

- (void)enabled:(BOOL)enabled {
    self.userInteractionEnabled = enabled;
    self.enabled = enabled;
    if(tapControl) {
        tapControl.enabled = enabled;
    }
}

- (void)focus {
    [self becomeFirstResponder];
}

- (void)hideClearButton {
    self.clearButtonMode = UITextFieldViewModeNever;
}

- (BOOL)isset {
    return [self value].length > 0;
}

#pragma mark -
#pragma mark UITextField Delegates

- (void)textFieldDidChange:(id)textFieldDidChange
{
    // Check if entered text field length is more that max allowed length.
    NSString *text = [self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if(text.length > self.maxCharacters) {
        text = [text substringToIndex:self.maxCharacters];
        self.text = text;
        return;
    }
    
    // Call value change block
    if (_onValueChangeBlock) {
        _onValueChangeBlock(text);
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    previousValue = textField.text;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if(_onValueChangedBlock && previousValue && ![previousValue isEqual:textField.text]) _onValueChangedBlock(self.value);
    if ([self.superTextFieldDelegate respondsToSelector:@selector(superTextFieldDidFinishEditing:)]) {
        [self.superTextFieldDelegate superTextFieldDidFinishEditing:textField];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.mask) {
        NSString *changedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if(range.length == 1 && // Only do for single deletes
           string.length < range.length &&
           [[textField.text substringWithRange:range] rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]].location == NSNotFound)
        {
            // Something was deleted.  Delete past the previous number
            NSInteger location = changedString.length-1;
            if(location > 0)
            {
                for(; location > 0; location--)
                {
                    if(isdigit([changedString characterAtIndex:location]))
                    {
                        break;
                    }
                }
                changedString = [changedString substringToIndex:location];
            }
        }
        
        textField.text = [self filteredPhoneStringFromString:changedString filter:self.mask];
        
        return NO;
    }
    
    if(self.numeric || self.decimalNumeric)
    {
        NSString *fulltext = [textField.text stringByAppendingString:string];
        NSString *charactersSetString = @"0123456789";
        
        // For decimal keyboard, allow "dot" and "comma" characters.
        if(self.decimalNumeric) {
            charactersSetString = [charactersSetString stringByAppendingString:@".,"];
        }
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:charactersSetString];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:fulltext];
        
        // If typed character is out of Set, ignore it.
        BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        if(!stringIsValid) {
            return NO;
        }
        
        if(self.decimalNumeric)
        {
            NSString *currentText = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            // Change the "," (appears in other locale keyboards, such as russian) key of "."
            fulltext = [fulltext stringByReplacingOccurrencesOfString:@"," withString:@"."];
            
            // Check the statements of decimal value.
            if([fulltext isEqualToString:@"."]) {
                textField.text = @"0.";
                return NO;
            }
            
            if([fulltext rangeOfString:@".."].location != NSNotFound) {
                textField.text = [fulltext stringByReplacingOccurrencesOfString:@".." withString:@"."];
                return NO;
            }
            
            // If second dot is typed, ignore it.
            NSArray *dots = [fulltext componentsSeparatedByString:@"."];
            if(dots.count > 2) {
                textField.text = currentText;
                return NO;
            }
            
            // If first character is zero and second character is > 0, replace first with second. 05 -> 5;
            if(fulltext.length == 2) {
                if([[fulltext substringToIndex:1] isEqualToString:@"0"] && ![fulltext isEqualToString:@"0."]) {
                    textField.text = [fulltext substringWithRange:NSMakeRange(1, 1)];
                    return NO;
                }
            }
        }
    }
    
    // Check the max characters typed.
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= _maxCharacters || returnKey;
}

// New Methods

- (void)didBeginEditing:(NSNotification *)notification {
    FloatingTextField *textField = notification.object;
    
    if (textField == self && !self.separatorHidden) {
        [separatorAnimatableViewConatraints autoRemoveConstraints];
        
        separatorAnimatableView.backgroundColor = self.separatorNormalColor;
        NSLayoutConstraint *c1 = [separatorAnimatableView autoSetDimension:ALDimensionHeight toSize:1];
        NSLayoutConstraint *c2 = [separatorAnimatableView autoSetDimension:ALDimensionWidth toSize:0];
        NSLayoutConstraint *c3 = [separatorAnimatableView autoAlignAxisToSuperviewMarginAxis:ALAxisVertical];
        NSLayoutConstraint *c4 = [separatorAnimatableView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
        separatorAnimatableViewConatraints = @[c1, c2, c3, c4];
        
        [self layoutSubviews];
        
        [separatorAnimatableViewConatraints autoRemoveConstraints];
        
        separatorAnimatableView.backgroundColor = self.separatorActiveColor;
        c1 = [separatorAnimatableView autoSetDimension:ALDimensionHeight toSize:2];
        NSArray *c2array =  [separatorAnimatableView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero excludingEdge:ALEdgeTop];
        NSMutableArray *constraints = [NSMutableArray arrayWithArray:c2array];
        [constraints addObject:c1];
        separatorAnimatableViewConatraints = constraints;
        
        __weak FloatingTextField *weakSelf = self;
        [UIView animateWithDuration:0.375 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            [weakSelf layoutSubviews];
        } completion:nil];
    }
}

- (void)didEndEditing:(NSNotification *)notification {
    FloatingTextField *textField = notification.object;
    
    if (textField == self && !self.separatorHidden) {
        [separatorAnimatableViewConatraints autoRemoveConstraints];
        
        separatorAnimatableView.backgroundColor = self.separatorActiveColor;
        NSLayoutConstraint *c1 = [separatorAnimatableView autoSetDimension:ALDimensionHeight toSize:2];
        NSArray *c2array = [separatorAnimatableView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero excludingEdge:ALEdgeTop];
        NSMutableArray *constraints = [NSMutableArray arrayWithArray:c2array];
        [constraints addObject:c1];
        separatorAnimatableViewConatraints = constraints;
        
        [self layoutSubviews];
        
        [separatorAnimatableViewConatraints autoRemoveConstraints];
        
        c1 = [separatorAnimatableView autoSetDimension:ALDimensionHeight toSize:1];
        NSLayoutConstraint *c2 = [separatorAnimatableView autoSetDimension:ALDimensionWidth toSize:0];
        NSLayoutConstraint *c3 = [separatorAnimatableView autoAlignAxisToSuperviewMarginAxis:ALAxisVertical];
        NSLayoutConstraint *c4 = [separatorAnimatableView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
        separatorAnimatableViewConatraints = @[c1, c2, c3, c4];
        
        __weak FloatingTextField *weakSelf = self;
        [UIView animateWithDuration:0.375 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            [weakSelf layoutSubviews];
        } completion:nil];
    }
}

#pragma mark - Mask

- (NSMutableString *)filteredPhoneStringFromString:(NSString *)string filter:(NSString *)filter {
    NSUInteger onOriginal = 0, onFilter = 0, onOutput = 0;
    char outputString[([filter length])];
    BOOL done = NO;
    
    while (onFilter < [filter length] && !done) {
        char filterChar = [filter characterAtIndex:onFilter];
        char originalChar = onOriginal >= string.length ? '\0' : [string characterAtIndex:onOriginal];
        switch (filterChar) {
            case '#':
                if(originalChar=='\0')
                {
                    // We have no more input numbers for the filter.  We're done.
                    done = YES;
                    break;
                }
                if(isdigit(originalChar))
                {
                    outputString[onOutput] = originalChar;
                    onOriginal++;
                    onFilter++;
                    onOutput++;
                }
                else
                {
                    onOriginal++;
                }
                break;
            default:
                // Any other character will automatically be inserted for the user as they type (spaces, - etc..) or deleted as they delete if there are more numbers to come.
                outputString[onOutput] = filterChar;
                onOutput++;
                onFilter++;
                if(originalChar == filterChar)
                    onOriginal++;
                break;
        }
    }
    outputString[onOutput] = '\0'; // Cap the output string
    return [NSString stringWithUTF8String:outputString].mutableCopy;
}

#pragma mark - Getters/Setters

- (void)setSeparatorHidden:(BOOL)separatorHidden {
    _separatorHidden = separatorHidden;
    
    separatorView.hidden = separatorHidden;
    separatorAnimatableView.hidden = separatorHidden;
}

#pragma mark -
#pragma mark Blocks

- (void)onValueChanged:(FloatingTextFieldViewOnValueChangedBlock)block {
    _onValueChangedBlock = block;
}

- (void)onValueChange:(FloatingTextFieldViewValueChangeBlock)block {
    _onValueChangeBlock = block;
}


@end
