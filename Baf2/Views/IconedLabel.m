//
//  IconedLabel.m
//  Baf2
//
//  Created by Askar Mustafin on 4/11/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "IconedLabel.h"

@interface IconedLabel() {}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftIconLonstrain;

@end

@implementation IconedLabel

- (id)init {
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"IconedLabel"
                                                      owner:self
                                                    options:nil];
    for (id view in nibViews) {
        if ([view isKindOfClass:[UIView class]]) {
            return view;
        }
    }
    return nil;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setLeftIndent:(CGFloat)leftIndent {
    _leftIndent = leftIndent;
    self.leftIconLonstrain.constant = leftIndent;
}

@end