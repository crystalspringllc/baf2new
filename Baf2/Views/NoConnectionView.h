//
//  NoConnectionView.h
//  Kiwi.kz
//
//  Created by Almas Adilbek on 6/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NoConnectionViewDelegate;

@interface NoConnectionView : UIView {}

@property (nonatomic, weak) id<NoConnectionViewDelegate> delegate;

-(void)setIcon:(NSString *)iconName andMessage:(NSString *)message;

-(void)show;
-(void)hide;

@end


@protocol NoConnectionViewDelegate

-(void)noConnectionViewInternetBecameAvailable:(NoConnectionView *)noConnectionView;

@end
