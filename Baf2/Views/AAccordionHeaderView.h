//
//  AAccordionHeaderView.h
//  BAF
//
//  Created by Almas Adilbek on 8/12/14.
//
//

@class AAccordionHeaderView;

@protocol AccordionHeaderViewDelegate;
@protocol AccordionHeaderViewDelegate<NSObject>
-(void)accordionHeaderView:(AAccordionHeaderView *)headerView opened:(BOOL)opened;
@end


@interface AAccordionHeaderView : UIView

@property(nonatomic, assign) NSUInteger index;
@property(nonatomic, assign) BOOL isOpen;
@property(nonatomic, assign) CGFloat revealDuration;
@property(nonatomic, assign) id <AccordionHeaderViewDelegate> delegate;
@property(nonatomic, strong) UIView *contentView;

- (id)initWithTitle:(NSString *)title icon:(NSString *)iconName;
- (id)initWithTitle:(NSString *)title icon:(NSString *)iconName width:(CGFloat)width;
- (id)initWithTitle:(NSString *)title icon:(NSString *)iconName width:(CGFloat)width detailColor:(UIColor *)color;

- (void)setContentView:(UIView *)view;
- (void)toggleHeader;

@end
