//
//  HeaderTitleView.m
//  BAF
//
//  Created by Almas Adilbek on 10/27/14.
//
//

#import "HeaderTitleView.h"

@implementation HeaderTitleView

- (id)initWithTitle:(NSString *)title {
    self = [super initWithFrame:CGRectMake(0, 0, kScreenWidth, 1)];
    if(self) {
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.width, 1)];
        bgView.backgroundColor = [UIColor fromRGB:0xF4F4F4];
        bgView.tag = 100;
        [self addSubview:bgView];

        UIView * line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.width, 1)];
        [line setBackgroundColor:[UIColor fromRGB:0xE3E3E3]];
        [self addSubview:line];

        UIView * line2 = [[UIView alloc] initWithFrame:CGRectMake(0, line.bottom, self.width, 1)];
        [line2 setBackgroundColor:[UIColor whiteColor]];
        line2.alpha = 0.5;
        [self addSubview:line2];

        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kViewSidePadding, 15, self.width - 2 * kViewSidePadding, 1)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont helveticaNeue:14];
        titleLabel.textColor = [UIColor appGrayColor];
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.numberOfLines = 0;
        titleLabel.text = title;
        [titleLabel sizeToFit];
        [self addSubview:titleLabel];

        bgView.height = titleLabel.bottom + 9;

        UIView * line3 = [[UIView alloc] initWithFrame:CGRectMake(0, bgView.height - 1, self.width, 1)];
        [line3 setBackgroundColor:[UIColor fromRGB:0xC8C7CC]];
        [self addSubview:line3];

        self.height = bgView.height;
    }
    return self;
}

+ (CGFloat)headerHeight {
    return 40;
}

@end
