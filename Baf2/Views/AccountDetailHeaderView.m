//
//  AccountDetailHeaderView.m
//  Baf2
//
//  Created by Askar Mustafin on 4/11/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <ChameleonFramework/ChameleonMacros.h>
#import "AccountDetailHeaderView.h"
#import "UIView+ConcisePureLayout.h"
#import "Loan.h"
#import "Deposit.h"
#import "Current.h"
#import "Card.h"
#import "CardAccount.h"
#import "ChoosePeriodControlView.h"
#import "NSDate+Ext.h"
#import "AMStackView.h"
#import "IconTitleSubtitleView.h"
#import "AccountLinkButtonsView.h"
#import "BlockSumAndTotalBalanceView.h"
#import "MainButton.h"
#import "STPopupController+Extensions.h"
#import "ActionSheetListPopupViewController.h"
#import "UIImage+Icon.h"
#import "AccountStatementsResponse.h"
#import "PaymentsViewController.h"
#import "TransfersQuickViewController.h"
#import "NSDateFormatter+Ext.h"
#import "AmountCurrencyView.h"
#import "TransferViewController.h"

#define kDateFormat_ddMMyyyyHHmm @"dd.MM.yyyy HH:mm"

@interface AccountDetailHeaderView ()
@property (nonatomic, strong) ChoosePeriodControlView *periodView;
@property(nonatomic, strong) UISegmentedControl *segmentedControl;
@end

@implementation AccountDetailHeaderView {
    AMStackView *stackView;
    UIView *segmentedControlContainer;
    NSInteger selectedSegmentIndex;
}

- (id)initForAutoLayout {
    self = [super initForAutoLayout];
    if (self) {
        [self aa_setWidth:[ASize screenWidth]];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

#pragma mark - config actions

- (void)clickChargeCardButton {
    if (self.onChargeCard) {
        self.onChargeCard(self.account);
    }
}

- (void)clickChargeDepositButton {
    if (self.onChargeDeposit) {
        self.onChargeDeposit(self.account);
    }
}

- (void)clickTransferMultiCurrencyButton {
    if (self.onClickTransferMultiCurrencyButton) {
        self.onClickTransferMultiCurrencyButton();
    }
}

- (void)onValueChanged:(UISegmentedControl *)segmentedControl {
    selectedSegmentIndex = segmentedControl.selectedSegmentIndex;
    
    //[self composeInterfaceForCardAccount:self.account];
    
    if (self.onSegmentValueChanged) {
        self.onSegmentValueChanged(selectedSegmentIndex);
    }
}

- (void)chooseDatePeriod {
    if (self.onClickChoosePeriod) {
        self.onClickChoosePeriod();
    }
}

- (void)openPayments {
    [self.parentVC.navigationController pushViewController:[[PaymentsViewController alloc] init] animated:true];
}

- (void)openTransfers {
   // [self.parentVC.navigationController pushViewController:[TransfersQuickViewController createVC] animated:true];
    [self.parentVC.navigationController pushViewController:[TransferViewController createVC] animated:true];
}

#pragma mark - set Account or Card

- (void)setAccount:(id)account {
    _account = account;
    
    [self composeInterfaceForCardAccount:account];
}

#pragma mark - config ui

- (void)composeInterfaceForCardAccount:(CardAccount *)cardAccount {
    [self composeInterfaceForAccount:cardAccount];
}

- (void)composeInterfaceForAccount:(Account *)account {
    if (stackView) {
        [stackView removeFromSuperview];
    }
    
    __weak AccountDetailHeaderView *wSelf = self;

    self.backgroundColor = [UIColor yellowColor];

    stackView = [[AMStackView alloc] init];
    stackView.backgroundColor = [UIColor whiteColor];
    [self addSubview:stackView];
    [stackView aa_superviewFillWithInset:0];

    
    IconTitleSubtitleView *cardAliasView = [[IconTitleSubtitleView alloc] init];
    NSDate *updatedDate = [NSDate formattedDateFromString:account.updated];
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:kDateFormat_ddMMyyyyHHmm];
    if (updatedDate) {
        cardAliasView.updatedDateLabel.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"datas_on", nil),[df stringFromDate:updatedDate]];
    } else {
        cardAliasView.updatedDateLabel.text = [NSString stringWithFormat:@"%@ -", NSLocalizedString(@"datas_on", nil)];
    }
    cardAliasView.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    
    if (account.accountType == AccountTypeLoan || account.accountType == AccountTypeDeposit || account.accountType == AccountTypeAccount) {
        cardAliasView.iconImageView.image = [UIImage currencyIconImage:account.currency];
    } else {
        [cardAliasView.iconImageView sd_setImageWithURL:[NSURL URLWithString:self.account.logo] placeholderImage:[UIImage currencyIconImage:account.currency]];
    }
    
    cardAliasView.titleLabel.text = account.alias;
    
    cardAliasView.subTitleLabel.text = account.iban;
    if ([account isKindOfClass:[Loan class]]) {
        cardAliasView.subTitleLabel.text = account.key;
    }
    
    cardAliasView.onInfoButtonClick = ^() {
        ActionSheetListPopupViewController *actionSheetVC = [ActionSheetListPopupViewController new];
        actionSheetVC.numberOfRowsInSection = ^NSInteger(NSInteger section) {
            if (wSelf.account.accountType == AccountTypeCard) {
                if (wSelf.account.isBlockStole) {
                    return 6;
                } else {
                    return 7;
                }
            }
            return 3;
        };
        actionSheetVC.rowForIndexPath = ^ActionSheetListRowObject*(NSIndexPath *indexPath) {
            ActionSheetListRowObject *row = [ActionSheetListRowObject new];
            row.tintColor = [UIColor fromRGB:0x444444];

            NSArray *options;
            NSArray *images;
            [wSelf getAccountInfoDataSource:wSelf options:&options images:&images];

            row.title = options[(NSUInteger) indexPath.row];
            row.keepObject = options[(NSUInteger) indexPath.row];
            row.imageName = images[(NSUInteger) indexPath.row];
            return row;
        };
        actionSheetVC.checkmarkDisabledSections = [NSMutableIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 2)];
        actionSheetVC.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
            if (wSelf.account.accountType == AccountTypeCard) {
                if (wSelf.account.isIslamic){
                    if (selectedIndexPath.row == 0) {
                        // show additional info
                        if (wSelf.onAdditionalInfoTapped) {
                            wSelf.onAdditionalInfoTapped();
                        }
                    } else if (selectedIndexPath.row == 1) {
                        // edit alias
                        if (wSelf.onAccountAliasEditTapped) {
                            wSelf.onAccountAliasEditTapped();
                        }
                    } else if (selectedIndexPath.row == 2){
                        //cashback configuration
                        if (wSelf.onConfigCashBackTapped) {
                            wSelf.onConfigCashBackTapped();
                        }
                    } else if (selectedIndexPath.row == 3) {
                        // set/unset favourite
                        if (wSelf.onManageLimitsTapped) {
                            wSelf.onManageLimitsTapped();
                        }
                    } else if (selectedIndexPath.row == 4) {
                        if (wSelf.onSmsBankingTapped) {
                            wSelf.onSmsBankingTapped();
                        }
                        
                    } else if (selectedIndexPath.row == 5) {
                        if (wSelf.onShareRequisitesTapped) {
                            wSelf.onShareRequisitesTapped(wSelf.account);
                        }
                        
                    } else if (selectedIndexPath.row == 6) {
                        // set/unset favourite
                        if (wSelf.onFavouriteTapped) {
                            wSelf.onFavouriteTapped();
                        }
                    } else if (selectedIndexPath.row == 7) {
                        // block card
                        if (wSelf.onBlockCardTapped) {
                            wSelf.onBlockCardTapped();
                        }
                    }
                }else{
                    if (selectedIndexPath.row == 0) {
                        // show additional info
                        if (wSelf.onAdditionalInfoTapped) {
                            wSelf.onAdditionalInfoTapped();
                        }
                    } else if (selectedIndexPath.row == 1) {
                        // edit alias
                        if (wSelf.onAccountAliasEditTapped) {
                            wSelf.onAccountAliasEditTapped();
                        }
                    } else if (selectedIndexPath.row == 2) {
                        // set/unset favourite
                        if (wSelf.onManageLimitsTapped) {
                            wSelf.onManageLimitsTapped();
                        }
                    } else if (selectedIndexPath.row == 3) {
                        if (wSelf.onSmsBankingTapped) {
                            wSelf.onSmsBankingTapped();
                        }
                        
                    } else if (selectedIndexPath.row == 4) {
                        if (wSelf.onShareRequisitesTapped) {
                            wSelf.onShareRequisitesTapped(wSelf.account);
                        }
                        
                    } else if (selectedIndexPath.row == 5) {
                        // set/unset favourite
                        if (wSelf.onFavouriteTapped) {
                            wSelf.onFavouriteTapped();
                        }
                    } else if (selectedIndexPath.row == 6) {
                        // block card
                        if (wSelf.onBlockCardTapped) {
                            wSelf.onBlockCardTapped();
                        }
                    }
                }
                
            } else {
                if (selectedIndexPath.row == 0) {
                    // show additional info
                    if (wSelf.onAdditionalInfoTapped) {
                        wSelf.onAdditionalInfoTapped();
                    }
                } else if (selectedIndexPath.row == 1) {
                    // edit alias
                    if (wSelf.onAccountAliasEditTapped) {
                        wSelf.onAccountAliasEditTapped();
                    }
                }
//                else if (selectedIndexPath.row == 2) {
//                    // set/unset favourite
//                    if (wSelf.onFavouriteTapped) {
//                        wSelf.onFavouriteTapped();
//                    }
//                }
                else if (selectedIndexPath.row == 2) {
                    if (wSelf.onShareRequisitesTapped) {
                        wSelf.onShareRequisitesTapped(wSelf.account);
                    }
                }
            }
        };
        actionSheetVC.title = NSLocalizedString(@"choose_action", nil);
        
        STPopupController *vc = [[STPopupController alloc] initWithRootViewController:actionSheetVC];
        vc.dismissOnBackgroundTap = true;
        vc.style = STPopupStyleBottomSheet;
        vc.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
        [vc presentInViewController:self.parentVC];
    };
    [stackView pushView:cardAliasView marginTop:8 sideMargin:0 textAlignment:AMStackViewTextAlignmentDefault];

    if ([account isKindOfClass:[Card class]]) {
        UILabel *cardNumberLabel = [[UILabel alloc] initForAutoLayout];
        cardNumberLabel.font = [UIFont systemFontOfSize:15];
        cardNumberLabel.textColor = [UIColor grayColor];
        cardNumberLabel.text = [account number];
        cardNumberLabel.numberOfLines = 0;
        [cardNumberLabel aa_setWidth:[ASize screenWidth] - 30];
        [stackView pushView:cardNumberLabel marginTop:0 sideMargin:57 textAlignment:AMStackViewTextAlignmentLeft];
    }

    if (account.isWarning) {
        UILabel *statusDescr = [[UILabel alloc] initForAutoLayout];
        statusDescr.font = [UIFont boldSystemFontOfSize:13];
        statusDescr.textColor = [UIColor flatOrangeColor];
        statusDescr.text = account.statusDescr;
        statusDescr.numberOfLines = 0;
        [statusDescr aa_setWidth:[ASize screenWidth] - 30];
        [stackView pushView:statusDescr marginTop:10 sideMargin:15 textAlignment:AMStackViewTextAlignmentLeft];
    }

    if (account.balance) {

        UILabel *availableSumTitle = [[UILabel alloc] initForAutoLayout];
        availableSumTitle.font = [UIFont systemFontOfSize:13];
        availableSumTitle.text = NSLocalizedString(@"available_balance", nil);
        availableSumTitle.textColor = [UIColor fromRGB:0x444444];
        [stackView pushView:availableSumTitle marginTop:15 sideMargin:15 textAlignment:AMStackViewTextAlignmentCenter];

        if ([account isKindOfClass:[Card class]]) {
            UILabel *bySubtractingBlockedSumTitle = [[UILabel alloc] initForAutoLayout];
            bySubtractingBlockedSumTitle.font = [UIFont systemFontOfSize:11];
            bySubtractingBlockedSumTitle.text = NSLocalizedString(@"without_blocked_sum", nil);
            bySubtractingBlockedSumTitle.textColor = [[UIColor fromRGB:0x444444] colorWithAlphaComponent:0.8];
            [stackView pushView:bySubtractingBlockedSumTitle marginTop:0 sideMargin:15 textAlignment:AMStackViewTextAlignmentCenter];
        }

        UILabel *availableSumValue = [[UILabel alloc] initForAutoLayout];

        availableSumValue.textColor = [UIColor fromRGB:0x444444];
        availableSumValue.attributedText = [self styleAttributedText:[account.balance decimalFormatString]
                                                      withSecondText:[@" " stringByAppendingString:account.currency]
                                                            fontSize:32
                                                            thinFont:NO];
        [stackView pushView:availableSumValue marginTop:3 sideMargin:15 textAlignment:AMStackViewTextAlignmentCenter];
        
        if ([account isKindOfClass:[Loan class]]) {
            availableSumTitle.text = NSLocalizedString(@"rest_of_principal_debt", nil);
            NSString *totalPricipal = [((Loan *) account).totalPrincipal decimalFormatString];
            availableSumValue.attributedText = [self styleAttributedText:totalPricipal withSecondText:[@" " stringByAppendingString:account.currency] fontSize:32];


            UILabel *totalCreditSumTitle = [[UILabel alloc] initForAutoLayout];
            totalCreditSumTitle.textColor = [UIColor fromRGB:0x444444];
            totalCreditSumTitle.font = [UIFont systemFontOfSize:13];
            totalCreditSumTitle.text = @"Общая сумма кредита";
            [stackView pushView:totalCreditSumTitle marginTop:10 sideMargin:0 textAlignment:AMStackViewTextAlignmentCenter];

            UILabel *totalCreditSum = [[UILabel alloc] initForAutoLayout];
            totalCreditSum.textColor = [UIColor fromRGB:0x444444];
            totalCreditSum.font = [UIFont boldSystemFontOfSize:17];
            totalCreditSum.text = [account getBalanceWithCurrency];
            [stackView pushView:totalCreditSum marginTop:3 sideMargin:0 textAlignment:AMStackViewTextAlignmentCenter];


            UIView *space = [[UIView alloc] initWithFrame:CGRectZero];
            [stackView pushView:space marginTop:30 sideMargin:15 textAlignment:AMStackViewTextAlignmentCenter];
        }


    }

    AMStackView *stackForButtons = [[AMStackView alloc] init];
    [stackForButtons aa_setHeight:40];
    [stackForButtons aa_setWidth:285];

    if (self.account.isMulty) {
        UILabel *leftByValues = [[UILabel alloc] initForAutoLayout];
        leftByValues.font = [UIFont systemFontOfSize:12];
        leftByValues.text = NSLocalizedString(@"balances_in_cur", nil);
        leftByValues.textColor = [[UIColor fromRGB:0x444444] colorWithAlphaComponent:0.8];
        [stackView pushView:leftByValues marginTop:15 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];



        AmountCurrencyView *kzt = [[AmountCurrencyView alloc] init];
        kzt.amountLabel.text = [account.balanceKzt decimalFormatString];
        kzt.currencyLabel.text = @"KZT";
        [stackView pushView:kzt marginTop:5 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];

        AmountCurrencyView *usd = [[AmountCurrencyView alloc] init];
        usd.amountLabel.text = [account.balanceUsd decimalFormatString];
        usd.currencyLabel.text = @"USD";
        [stackView pushView:usd marginTop:5 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];


        AmountCurrencyView *eur = [[AmountCurrencyView alloc] init];
        eur.amountLabel.text = [account.balanceEur decimalFormatString];
        eur.currencyLabel.text = @"EUR";
        [stackView pushView:eur marginTop:5 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];


        AmountCurrencyView *gbp = [[AmountCurrencyView alloc] init];
        gbp.amountLabel.text = [account.balanceGbp decimalFormatString];
        gbp.currencyLabel.text = @"GBP";
        [stackView pushView:gbp marginTop:5 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];


        AmountCurrencyView *rub = [[AmountCurrencyView alloc] init];
        rub.amountLabel.text = [account.balanceRub decimalFormatString];
        rub.currencyLabel.text = @"RUB";
        [stackView pushView:rub marginTop:5 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];

        if ([account isKindOfClass:[Card class]]) {
            Card *card = account;
            if (card.isAvia) {
                UILabel *aviaBonusTitle = [[UILabel alloc] initForAutoLayout];
                aviaBonusTitle.font = [UIFont systemFontOfSize:12];
                aviaBonusTitle.text = @"Накоплено авиа миль:";
                aviaBonusTitle.textColor = [[UIColor fromRGB:0x444444] colorWithAlphaComponent:0.8];
                [stackView pushView:aviaBonusTitle marginTop:15 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];

                UILabel *aviaBonusLabel = [[UILabel alloc] initForAutoLayout];
                aviaBonusLabel.font = [UIFont systemFontOfSize:16];
                aviaBonusLabel.textColor = [UIColor fromRGB:0x444444];
                aviaBonusLabel.attributedText = [self styleAttributedText:[card.aviaBonus decimalFormatString] withSecondText:@"MIL" fontSize:20 thinFont:YES];
                [stackView pushView:aviaBonusLabel marginTop:5 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];
            }
        }

        MainButton *convertButton = [[MainButton alloc] init];
        convertButton.mainButtonStyle = MainButtonStyleOrange;
        if (self.account.isWarning) {
            convertButton.mainButtonStyle = MainButtonStyleDisable;
        }
        convertButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        convertButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        convertButton.title = NSLocalizedString(@"translation_between_currencies", nil);
        [convertButton addTarget:self action:@selector(clickTransferMultiCurrencyButton) forControlEvents:UIControlEventTouchUpInside];
        [convertButton aa_setHeight:40];
        [convertButton aa_setWidth:140];

        [stackForButtons pushHorizontallyView:convertButton];
        [stackView pushView:stackForButtons marginTop:20 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];
    }


    if ([account isKindOfClass:[Card class]]) {
        Card *card = account;

        MainButton *chargeCardButton = [[MainButton alloc] init];
        chargeCardButton.mainButtonStyle = MainButtonStyleOrange;
        if (self.account.isWarning) {
            chargeCardButton.mainButtonStyle = MainButtonStyleDisable;
        }
        chargeCardButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        chargeCardButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        chargeCardButton.title = NSLocalizedString(@"add_card", nil);
        [chargeCardButton addTarget:self action:@selector(clickChargeCardButton) forControlEvents:UIControlEventTouchUpInside];
        [chargeCardButton aa_setHeight:40];
        [chargeCardButton aa_setWidth:140];

        [stackForButtons pushHorizontallyView:chargeCardButton];
        if (stackForButtons.itemsCount == 1) {
            [stackView pushView:stackForButtons marginTop:20 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];
        }

        if (card.hasCreditLimit || card.isCredit) {

            //creditlimit
            NSString *creditLimitAmountText = [NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"credit_info_limit", nil), [card.creditLimit decimalFormatString], card.currency];
            NSString *nextPaymentText = [NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"credit_info_next_payment", nil), [card.nextPaymentAmount decimalFormatString], card.currency];

            UILabel *creditLimitAmount = [[UILabel alloc] init];
            UILabel *nextPaymentAmount = [[UILabel alloc] init];
            creditLimitAmount.font = [UIFont systemFontOfSize:13];
            creditLimitAmount.textColor = [UIColor fromRGB:0x444444];
            nextPaymentAmount.font = [UIFont systemFontOfSize:13];
            nextPaymentAmount.textColor = [UIColor fromRGB:0x444444];
            creditLimitAmount.text = creditLimitAmountText;
            nextPaymentAmount.text = nextPaymentText;
            [stackView pushView:creditLimitAmount marginTop:10 sideMargin:10 textAlignment:AMStackViewTextAlignmentRight];
            [stackView pushView:nextPaymentAmount marginTop:2 sideMargin:10 textAlignment:AMStackViewTextAlignmentRight];

            NSDate *nextPaymentDate = [[NSDateFormatter baf2DateFormatter] dateFromString:card.nextPaymentDate];
            NSString *nextPaymentDateString = [df stringFromDate:nextPaymentDate];
            NSString *nextPaymentDateText = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"credit_info_pay_due", nil), nextPaymentDateString];
            if (nextPaymentDateString) {
                UILabel *payDueDate = [[UILabel alloc] init];
                payDueDate.font = [UIFont systemFontOfSize:13];
                payDueDate.textColor = [UIColor fromRGB:0x444444];
                payDueDate.text = nextPaymentDateText;
                [stackView pushView:payDueDate marginTop:2 sideMargin:10 textAlignment:AMStackViewTextAlignmentRight];
            }
        }
    }
    
    NSNumber *cashBack = self.accountStatementsResponse.cashBack;
    NSString *cashBackCurrency = self.accountStatementsResponse.cashBackCurrency;
    if (cashBack && cashBackCurrency && cashBackCurrency.length > 0) {
        UILabel *cashBackTitle = [[UILabel alloc] initForAutoLayout];
        cashBackTitle.font = [UIFont systemFontOfSize:15];
        cashBackTitle.text = @"Cashback";
        cashBackTitle.textColor = [UIColor fromRGB:0x444444];
        [stackView pushView:cashBackTitle marginTop:15 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];
        
        UILabel *cashBackValue = [[UILabel alloc] initForAutoLayout];
        cashBackValue.textColor = [UIColor fromRGB:0x444444];
        cashBackValue.attributedText = [self styleAttributedText:[cashBack decimalFormatString] withSecondText:[@" " stringByAppendingString:cashBackCurrency] fontSize:16];
        [stackView pushView:cashBackValue marginTop:0 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];
    }
    
    if (self.account.accountType == AccountTypeDeposit) {
        MainButton *convertButton = [[MainButton alloc] init];
        convertButton.mainButtonStyle = MainButtonStyleOrange;
        convertButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        convertButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        convertButton.title = NSLocalizedString(@"add_deposit", nil);
        [convertButton addTarget:self action:@selector(clickChargeDepositButton) forControlEvents:UIControlEventTouchUpInside];
        [convertButton aa_setHeight:40];
        [convertButton aa_setWidth:180];
        [stackView pushView:convertButton marginTop:10 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];
    }

    if ([self.account.block doubleValue] > 0) {

        //Заблокировано / Общий остаток
        UIView *line = [[UIView alloc] init];
        line.backgroundColor = [[UIColor fromRGB:0x444444] colorWithAlphaComponent:0.1];
        [line aa_setHeight:0.5];
        [stackView pushView:line marginTop:15 marginBottom:10 marginLeft:0 marginRight:0];


        BlockSumAndTotalBalanceView *blockSumAndTotalBalanceView = [[BlockSumAndTotalBalanceView alloc] init];
        blockSumAndTotalBalanceView.blockedSumLabel.attributedText = [self styleAttributedText:[account.block decimalFormatString] withSecondText:account.currency fontSize:20 thinFont:YES];
        [blockSumAndTotalBalanceView hideTotalSumLabel];
        [stackView pushView:blockSumAndTotalBalanceView marginTop:5 sideMargin:15 textAlignment:AMStackViewTextAlignmentRight];

    }
    
    if ([self.account isKindOfClass:[Card class]]) {
        AccountLinkButtonsView *accountLinkButtonsView = [AccountLinkButtonsView new];
        [accountLinkButtonsView.leftButton addTarget:self action:@selector(openPayments) forControlEvents:UIControlEventTouchUpInside];
        [accountLinkButtonsView.rightButton addTarget:self action:@selector(openTransfers) forControlEvents:UIControlEventTouchUpInside];
        
        CGFloat marginTop = [self.account.block doubleValue] > 0 ? 0 : 20;
        [stackView pushView:accountLinkButtonsView marginTop:marginTop sideMargin:0 textAlignment:AMStackViewTextAlignmentDefault];
    }
    
    if (self.account.accountType != AccountTypeLoan) {
        segmentedControlContainer = [[UIView alloc] init];
        segmentedControlContainer.backgroundColor = [UIColor whiteColor];

        NSArray *segmentOptions;

        if ([account isKindOfClass:[Deposit class]] || [account isKindOfClass:[Current class]]) {
            segmentOptions = @[NSLocalizedString(@"statement", nil)];
        } else {
            segmentOptions = @[NSLocalizedString(@"statement", nil), NSLocalizedString(@"in_block", nil)];
        }


        self.segmentedControl = [[UISegmentedControl alloc] initWithItems:segmentOptions];
        self.segmentedControl.selectedSegmentIndex = selectedSegmentIndex;
        self.segmentedControl.tintColor = [UIColor fromRGB:0x444444];
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        NSDictionary *attributes = @{NSFontAttributeName : font};
        [self.segmentedControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
        [self.segmentedControl addTarget:self action:@selector(onValueChanged:) forControlEvents:UIControlEventValueChanged];
        [segmentedControlContainer addSubview:self.segmentedControl];
        [self.segmentedControl aa_superviewTop:10];
        [self.segmentedControl aa_superviewBottom:10];
        [self.segmentedControl aa_superviewLeft:15];
        [self.segmentedControl aa_superviewRight:15];


        if ([self.account isKindOfClass:[Card class]]) {
            [stackView pushView:segmentedControlContainer marginTop:0 sideMargin:0 textAlignment:AMStackViewTextAlignmentDefault];
        } else {
            CGFloat marginTop = [self.account.block doubleValue] > 0 ? 0 : 20;
            [stackView pushView:segmentedControlContainer marginTop:marginTop sideMargin:0 textAlignment:AMStackViewTextAlignmentDefault];
        }

        if (selectedSegmentIndex == 0) {
            self.periodView = [[ChoosePeriodControlView alloc] init];
            self.periodView.onChoosePeriodButtonClick = ^{
                [wSelf chooseDatePeriod];
            };
            [stackView pushView:self.periodView marginTop:0 sideMargin:0 textAlignment:AMStackViewTextAlignmentDefault];
        }
    }

    UIView *line = [[UIView alloc] init];
    line.backgroundColor = [[UIColor fromRGB:0x444444] colorWithAlphaComponent:0.3];
    [line aa_setHeight:1];
    [stackView pushView:line marginTop:0 sideMargin:0 textAlignment:AMStackViewTextAlignmentDefault];
}

- (void)getAccountInfoDataSource:(AccountDetailHeaderView *)wSelf options:(NSArray **)options images:(NSArray **)images {
    NSString *favouriteText = wSelf.account.isFavorite ? NSLocalizedString(@"do_not_show_in_overview", nil) : NSLocalizedString(@"show_in_overview", nil);
    NSString *favouriteImageName = wSelf.account.isFavorite ? @"christmas_star" : @"christmas_star_filled";
    NSString *blockedText = [wSelf.account isBlockedIBFL] ? @"Заблокировать/Разблокировать" : NSLocalizedString(@"block", nil);

    if (wSelf.account.accountType == AccountTypeCard) {

        if (wSelf.account.isBlockStole) {
            (*options) = @[
                           
                           NSLocalizedString(@"additional_info", nil),
                           NSLocalizedString(@"edit", nil),
                           NSLocalizedString(@"limit_control", nil),
                           NSLocalizedString(@"manage_sms_banking", nil),
                           NSLocalizedString(@"share_requisite", nil),
                           favouriteText
                           ];
            
            (*images) = @[
                          @"info_popup",
                          @"edit",
                          @"speedometer",
                          @"smsbanking",
                          @"icon_share",
                          favouriteImageName
                          ];
        } else {
            
            if (wSelf.account.isIslamic){
                (*options) = @[
                               
                               NSLocalizedString(@"additional_info", nil),
                               NSLocalizedString(@"edit", nil),
                               NSLocalizedString(@"config_charity", nil),
                               NSLocalizedString(@"limit_control", nil),
                               NSLocalizedString(@"manage_sms_banking", nil),
                               NSLocalizedString(@"share_requisite", nil),
                               favouriteText,
                               blockedText
                               ];
                
                (*images) = @[
                              @"info_popup",
                              @"edit",
                              @"cashback_3",
                              @"speedometer",
                              @"smsbanking",
                              @"icon_share",
                              favouriteImageName,
                              @"lock"
                              ];
            }else{
                (*options) = @[
                               
                               NSLocalizedString(@"additional_info", nil),
                               NSLocalizedString(@"edit", nil),
                               NSLocalizedString(@"limit_control", nil),
                               NSLocalizedString(@"manage_sms_banking", nil),
                               NSLocalizedString(@"share_requisite", nil),
                               favouriteText,
                               blockedText
                               ];
                
                (*images) = @[
                              @"info_popup",
                              @"edit",
                              @"speedometer",
                              @"smsbanking",
                              @"icon_share",
                              favouriteImageName,
                              @"lock"
                              ];
            }
        }

    } else {

        (*options) = @[
                NSLocalizedString(@"additional_info", nil),
                NSLocalizedString(@"edit", nil),
                NSLocalizedString(@"share_requisite", nil)
        ];

        (*images) = @[
                @"info_popup",
                @"edit",
                @"icon_share"
        ];
    }
}

#pragma mark -

- (NSMutableAttributedString *)styleAttributedText:(NSString *)text withSecondText:(NSString *)secondText fontSize:(CGFloat)fontSize {
    return [self styleAttributedText:text withSecondText:secondText fontSize:fontSize thinFont:false];
}

- (NSMutableAttributedString *)styleAttributedText:(NSString *)text withSecondText:(NSString *)secondText fontSize:(CGFloat)fontSize thinFont:(BOOL)thinFont {
    UIFont *font = thinFont ? [UIFont helveticaNeueThin:fontSize] : [UIFont systemFontOfSize:fontSize];
    return [self styleAttributedText:text withTextFont:font andSecondText:secondText withSecondTextFont:[UIFont helveticaNeueThin:15]];
}

- (NSMutableAttributedString *)styleAttributedText:(NSString *)text withTextFont:(UIFont *)textFont andSecondText:(NSString *)secondText withSecondTextFont:(UIFont *)secondTextFont {
    NSDictionary *firstFont = @{NSFontAttributeName : textFont};
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:text attributes:firstFont];

    NSDictionary *secondFont = @{NSFontAttributeName : secondTextFont};
    NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc]initWithString:[@" " stringByAppendingString:secondText] attributes:secondFont];

    [aAttrString appendAttributedString:vAttrString];

    return aAttrString;
}

#pragma mark - set

- (void)setChoosePeriodText:(NSDate *)fromDate toDate:(NSDate *)toDate {
    self.periodView.choosenPeriodLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@", NSLocalizedString(@"from_s", nil), fromDate.ddMMyyyy ? fromDate.ddMMyyyy : @"" , NSLocalizedString(@"until_po", nil), toDate.ddMMyyyy ? toDate.ddMMyyyy : @""];
}

- (void)setAccountStatementsResponse:(AccountStatementsResponse *)accountStatementsResponse {
    _accountStatementsResponse = accountStatementsResponse;
    [self composeInterfaceForCardAccount:self.account];

    if (self.segmentedControl.superview != nil) {
        if (self.accountStatementsResponse.aviaStatements.count > 0) {
            NSString *title = NSLocalizedString(@"compesation_miles", nil);
            NSUInteger index = self.segmentedControl.numberOfSegments;
            [self.segmentedControl insertSegmentWithTitle:title atIndex:index animated:YES];
        }
    }
}

@end
