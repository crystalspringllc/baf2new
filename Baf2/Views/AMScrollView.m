//
//  AMScrollView.m
//  ACB
//
//  Created by Mustafin Askar on 11/09/2015.
//
//

#import "AMScrollView.h"
#import <PureLayout/PureLayout.h>
#import <HMSegmentedControl/HMSegmentedControl.h>

@interface AMScrollView() {}
@property (nonatomic, strong) NSMutableArray *items;
@end

@implementation AMScrollView

- (void)pushView:(UIView *)view marginTop:(CGFloat)marginTop centered:(BOOL)centered {
    [self pushView:view marginTop:marginTop centered:centered addSeparator:NO];
}

- (void)pushView:(UIView *)view marginTop:(CGFloat)marginTop centered:(BOOL)centered addSeparator:(BOOL)separator {

    if (!self.items) {
        self.items = [[NSMutableArray alloc] init];
        if (self.headerView) {
            [self.items addObject:self.headerView];
        }
    }
    
    UIView *lastView = [self.items lastObject];
    [lastView removeConstraint:self.lastViewBottom];
    [self.lastViewBottom autoRemove];
    self.lastViewBottom = nil;

    [self.contentView addSubview:view];
    
    if (centered) {
        [view autoAlignAxisToSuperviewAxis:ALAxisVertical];
    }


    [view autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:lastView withOffset:marginTop];
    self.lastViewBottom = [view autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:marginTop];
    

    [self.items addObject:view];
}


/**
*  Remove pushed views into scrollview instead of headerview and hmsegmentedcontrol
*
*/
- (void)removeAllPushsedViews {

    NSMutableArray *itemsToremove = [[NSMutableArray alloc] init];
    for (UIView *view in self.items) {
        if ([view isEqual:self.headerView] ) {

        } else if ([view isKindOfClass:[HMSegmentedControl class]]) {

        } else {
            [view removeFromSuperview];
            [itemsToremove addObject:view];
        }
    }
    for (UIView *view in itemsToremove) {
        [self.items removeObject:view];
    }


    UIView *lastView = [self.items lastObject];
    self.lastViewBottom = [lastView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:2];
}


@end
