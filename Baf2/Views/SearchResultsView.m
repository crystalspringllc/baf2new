//
//  SearchResultsView.m
//  Myth
//
//  Created by Almas Adilbek on 10/10/13.
//
//

#import "SearchResultsView.h"
#import "RequestsCache.h"
#import "ContractsAPIOLD.h"
#import "SideMenuContractTableCell.h"
#import "ProviderCategory.h"
#import "SearchResultIconTitleTableCell.h"
#import "User.h"

#define kMyContractsKey @"myContracts"
#define kProvidersKey @"providers"
#define kProviderCategories @"providerCategories"

@implementation SearchResultsView {
    NSMutableDictionary *resultsDic;
    
    NSArray *myContracts;
    NSMutableArray *resultMyContracts;
    
    NSArray *providerCategories;
    NSMutableArray *resultProviders;
    NSMutableArray *resultProviderCategories;
    
    UILabel *noResultLabel;
}

@synthesize delegate;

- (id)init
{
    return [self initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeightWithoutStatusBarAndNavigationBar])];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {

        self.backgroundColor = [UIColor whiteColor];

        list = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.width, self.height) style:UITableViewStylePlain];
        list.delegate = self;
        list.dataSource = self;
        list.backgroundColor = [UIColor fromRGB:0xF4F4F4];
        list.separatorColor = [UIColor clearColor];
        list.rowHeight = 50;
        [self addSubview:list];

        resultsDic = [[NSMutableDictionary alloc] init];

    }
    return self;
}

-(void)search:(NSString *)text
{
    text = [text lowercaseString];
    // Fetch
    if(!myContracts && [[User sharedInstance] isLoggedIn])
    {
        id response = [RequestsCache getResponseWithPath:[RequestsCache userPathWithPath:kGetContractsPath]];
        if(response) {
            myContracts = [[NSArray alloc] initWithArray:[ContractsAPIOLD responseToContracts:response]];
        }
    }
    if(!providerCategories)
    {
        id response = [RequestsCache getResponseWithPath:kProvidersListPath];
        if(response) {
            providerCategories = [[NSArray alloc] initWithArray:[ProviderCategory responseToProviderCategories:response]];
        }
    }
    
    // Search contracts
    if(resultMyContracts) {
        resultMyContracts = nil;
    }
    if(myContracts) {
        for(Contract *contract in myContracts) {
            if([[contract.alias lowercaseString] rangeOfString:text].location != NSNotFound) {
                if(!resultMyContracts) {
                    resultMyContracts = [[NSMutableArray alloc] init];
                }
                [resultMyContracts addObject:contract];
            }
        }
    }
    [self setArray:resultMyContracts forKey:kMyContractsKey];
    
    // Search providers
    if(resultProviders) {
        resultProviders = nil;
    }
    if(resultProviderCategories) {
        resultProviderCategories = nil;
    }
    if(providerCategories) {
        for(ProviderCategory *pc in providerCategories) {
            for (PaymentProvider *provider in pc.paymentProviders) {
                if([[provider.name lowercaseString] rangeOfString:text].location != NSNotFound) {
                    if(!resultProviders) {
                        resultProviders = [[NSMutableArray alloc] init];
                    }
                    [resultProviders addObject:provider];
                }
            }
            
            if([[pc.categoryName lowercaseString] rangeOfString:text].location != NSNotFound) {
                if(!resultProviderCategories) {
                    resultProviderCategories = [[NSMutableArray alloc] init];
                }
                [resultProviderCategories addObject:pc];
            }
        }
    }
    [self setArray:resultProviders forKey:kProvidersKey];
    [self setArray:resultProviderCategories forKey:kProviderCategories];
    
    [list reloadData];
    
    if([[resultsDic allKeys] count] > 0) {
        if(noResultLabel) {
            [noResultLabel removeFromSuperview];
            noResultLabel = nil;
        }
    } else {
        if(!noResultLabel) {
            noResultLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, list.width, 100)];
            noResultLabel.backgroundColor = [UIColor clearColor];
            noResultLabel.font = [UIFont helveticaNeue:15];
            noResultLabel.textColor = [UIColor grayColor];
            noResultLabel.lineBreakMode = NSLineBreakByWordWrapping;
            noResultLabel.numberOfLines = 0;
            noResultLabel.textAlignment = NSTextAlignmentCenter;
            noResultLabel.text = NSLocalizedString(@"nothing_was_found", nil);
            [self addSubview:noResultLabel];
        }
    }
}

-(void)searchOnlyProviderCategories:(NSString *)text {
    text = [text lowercaseString];

    if(!providerCategories)
    {
        id response = [RequestsCache getResponseWithPath:kProvidersListPath];
        if(response) {
            providerCategories = [[NSArray alloc] initWithArray:[ProviderCategory responseToProviderCategories:response]];
        }
    }


    // Search providers
    if(resultProviders) {
        resultProviders = nil;
    }
    if(resultProviderCategories) {
        resultProviderCategories = nil;
    }
    if(providerCategories) {
        for(ProviderCategory *pc in providerCategories) {
            for (PaymentProvider *provider in pc.paymentProviders) {
                if([[provider.name lowercaseString] rangeOfString:text].location != NSNotFound) {
                    if(!resultProviders) {
                        resultProviders = [[NSMutableArray alloc] init];
                    }
                    [resultProviders addObject:provider];
                }
            }

//            if([[pc.categoryName lowercaseString] rangeOfString:text].location != NSNotFound) {
//                if(!resultProviderCategories) {
//                    resultProviderCategories = [[NSMutableArray alloc] init];
//                }
//                [resultProviderCategories addObject:pc];
//            }
        }
    }
    [self setArray:resultProviders forKey:kProvidersKey];
    [self setArray:resultProviderCategories forKey:kProviderCategories];

    [list reloadData];

    if([[resultsDic allKeys] count] > 0) {
        if(noResultLabel) {
            [noResultLabel removeFromSuperview];
            noResultLabel = nil;
        }
    } else {
        if(!noResultLabel) {
            noResultLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, list.width, 100)];
            noResultLabel.backgroundColor = [UIColor clearColor];
            noResultLabel.font = [UIFont helveticaNeue:15];
            noResultLabel.textColor = [UIColor grayColor];
            noResultLabel.lineBreakMode = NSLineBreakByWordWrapping;
            noResultLabel.numberOfLines = 0;
            noResultLabel.textAlignment = NSTextAlignmentCenter;
            noResultLabel.text = NSLocalizedString(@"nothing_was_found", nil);
            [self addSubview:noResultLabel];
        }
    }
}

#pragma mark -

-(void)setArray:(id)arr forKey:(NSString *)key {
    if(arr) resultsDic[key] = arr;
    else [resultsDic removeObjectForKey:key];
}

#pragma mark -
#pragma mark UIScrollView

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.delegate respondsToSelector:@selector(searchResultsViewHideKeyboard)]) {
        [self.delegate searchResultsViewHideKeyboard];
    }
}

#pragma mark -
#pragma mark UITableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[resultsDic allKeys] count];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *keys = [resultsDic allKeys];
    NSArray *results = resultsDic[keys[section]];
    if(results) return [results count];
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *title = @"";
    
    NSArray *keys = [resultsDic allKeys];
    NSString *key = keys[section];
    
    if([key isEqual:kMyContractsKey]) title = NSLocalizedString(@"my_contracts", nil);
    else if([key isEqual:kProvidersKey]) title = NSLocalizedString(@"service_providers", nil);
    else if([key isEqual:kProviderCategories]) title = NSLocalizedString(@"provider_categories", nil);
    
    if(![title isEqual:@""])
    {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 40)];
        headerView.backgroundColor = [UIColor fromRGB:0xF4F4F4];
        
        CGRect tf = headerView.frame;
        tf.size.width = 260;
        tf.origin.x = 10;
        tf.size.height = 40;
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:tf];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.contentMode = UIViewContentModeCenter;
        titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
        titleLabel.textColor = [UIColor fromRGB:0x819DAC];
        titleLabel.font = [UIFont boldHelveticaNeue:14];
        titleLabel.text = title;
        [headerView addSubview:titleLabel];
        
        return headerView;
    }
    
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [resultsDic allKeys][indexPath.section];
    
    if([key isEqual:kMyContractsKey]) {
        return [SideMenuContractTableCell cellHeight];
    } else if([key isEqual:kProvidersKey]) {
        PaymentProvider *provider = [resultsDic[key] objectAtIndex:indexPath.row];
        return [SearchResultIconTitleTableCell height:provider.name];
    } else if([key isEqual:kProviderCategories]) {
        ProviderCategory *category = [resultsDic[key] objectAtIndex:indexPath.row];
        return [SearchResultIconTitleTableCell height:category.categoryName];
    }
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = nil;
    
    NSString *key = [resultsDic allKeys][indexPath.section];
    
    if([key isEqual:kMyContractsKey])
    {
        CellIdentifier = @"SideMenuContractTableCell";
        SideMenuContractTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[SideMenuContractTableCell alloc] initWithReuseIdentifier:CellIdentifier];
        }
        
        Contract *contract = (Contract *)[resultsDic[key] objectAtIndex:indexPath.row];
        [cell setData:contract];
        
        return cell;
    }
    else if([key isEqual:kProvidersKey])
    {
        CellIdentifier = @"providerTableCell";
        SearchResultIconTitleTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[SearchResultIconTitleTableCell alloc] initWithReuseIdentifier:CellIdentifier];
        }
        
        PaymentProvider *provider = [resultsDic[key] objectAtIndex:indexPath.row];
        [cell setIcon:provider.logo];
        [cell setTitle:provider.name];
        
        return cell;
    }
    else if([key isEqual:kProviderCategories])
    {
        CellIdentifier = @"providerCategoryTableCell";
        SearchResultIconTitleTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[SearchResultIconTitleTableCell alloc] initWithReuseIdentifier:CellIdentifier];
        }
        
        ProviderCategory *category = [resultsDic[key] objectAtIndex:indexPath.row];
        [cell setIcon:category.categoryLogo];
        [cell setTitle:category.categoryName];
        
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if(![Connection isReachableM]) {
//        [tableView deselectRowAtIndexPath:indexPath animated:YES];
//        return;
//    }
    
    NSString *key = [resultsDic allKeys][indexPath.section];

    if([key isEqual:kMyContractsKey])
    {
        Contract *contract = (Contract *)[resultsDic[key] objectAtIndex:indexPath.row];
        if(contract) {
            if ([self.delegate respondsToSelector:@selector(searchResultsView:contractTapped:)]) {
                [self.delegate searchResultsView:self contractTapped:contract];
            }
        }
    }
    else if([key isEqual:kProvidersKey])
    {
        PaymentProvider *provider = [resultsDic[key] objectAtIndex:indexPath.row];
        if(provider) {
            if ([self.delegate respondsToSelector:@selector(searchResultsView:providerTapped:)]) {
                [self.delegate searchResultsView:self providerTapped:provider];
            }
        }
    }
    else if([key isEqual:kProviderCategories])
    {
        ProviderCategory *category = [resultsDic[key] objectAtIndex:indexPath.row];
        if(category) {
            if ([self.delegate respondsToSelector:@selector(searchResultsView:providerCategoryTapped:)]) {
                [self.delegate searchResultsView:self providerCategoryTapped:category];
            }
        }
    }
}

- (UITableView *)getList {
    return list;
}

#pragma mark -

-(void)dealloc {
    list = nil;
}

@end
