//
//  PinCharacterView.h
//  BAF
//
//  Created by Almas Adilbek on 11/19/14.
//
//


#import <UIKit/UIKit.h>

@interface PinCharacterView : UIView

@property(nonatomic, assign, getter=isSelected) BOOL selected;

@end
