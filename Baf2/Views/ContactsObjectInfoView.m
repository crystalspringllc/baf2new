//
//  ContactsObjectInfoView.m
//  BAF
//
//  Created by Almas Adilbek on 5/18/15.
//
//

#import <PureLayout/ALView+PureLayout.h>
#import "ContactsObjectInfoView.h"
#import "UIView+AAPureLayout.h"

@interface ContactsObjectInfoView ()

@end

@implementation ContactsObjectInfoView {
}

- (id)init {
    self = [super init];
    if(self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        viewWidthConstraint = [self autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];

        self.textFont = [UIFont helveticaNeue:15];
        self.textColor = [UIColor darkGrayColor];

        titleLabel = [UILabel newAutoLayoutView];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont boldHelveticaNeue:16];
        titleLabel.textColor = [UIColor blackColor];
        [self addSubview:titleLabel];

        [titleLabel aa_superviewTop:contactsObjectInfoViewLeftInset];
        [titleLabel aa_superviewLeft:contactsObjectInfoViewLeftInset];
        [titleLabel aa_superviewRight:contactsObjectInfoViewLeftInset];

        // Address
        addressLabel = [UILabel newAutoLayoutView];
        addressLabel.backgroundColor = [UIColor clearColor];
        addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        addressLabel.numberOfLines = 0;
        addressLabel.font = self.textFont;
        addressLabel.textColor = self.textColor;
        [self addSubview:addressLabel];

        [addressLabel aa_pinUnderView:titleLabel offset:10];
        [addressLabel aa_superviewLeft:contactsObjectInfoViewLeftInset];
        [addressLabel aa_superviewRight:contactsObjectInfoViewLeftInset];

        viewBottomConstraint = [addressLabel aa_superviewBottom:15];
    }
    return self;
}

- (void)setTitle:(NSString *)text {
    titleLabel.text = text;
}

- (void)setAddress:(NSString *)text {
    addressLabel.text = text;
}


@end
