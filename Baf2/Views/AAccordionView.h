//
//  AAccordionView.h
//  BAF
//
//  Created by Almas Adilbek on 8/13/14.
//
//

#import "AAccordionHeaderView.h"

@protocol AAccordionViewDelegate;

@interface AAccordionView : UIView <AccordionHeaderViewDelegate>

@property(nonatomic, weak) id<AAccordionViewDelegate> delegate;
@property(nonatomic, assign) CGFloat revealDuration;

- (id)initWithWidth:(CGFloat)width;

// Default header height : 44
- (id)initWithWidth:(CGFloat)width headerHeight:(CGFloat)headerHeight;

- (void)addHeaderWithTitle:(NSString *)title icon:(NSString *)iconName contentView:(UIView *)view;
- (void)addHeaderWithTitle:(NSString *)title icon:(NSString *)iconName contentView:(UIView *)view marginTop:(CGFloat)marginTop;
- (void)addHeaderWithTitle:(NSString *)title icon:(NSString *)iconName contentView:(UIView *)view marginTop:(CGFloat)marginTop isOpen:(BOOL)isOpen;
- (void)addHeaderWithTitle:(NSString *)title icon:(NSString *)iconName contentView:(UIView *)view detailColor:(UIColor *)color marginTop:(CGFloat)marginTop isOpen:(BOOL)isOpen;

@end

@protocol AAccordionViewDelegate<NSObject>

-(void)aacordionView:(AAccordionView *)aAccordionView header:(AAccordionHeaderView *)aAccordionHeaderView didOpen:(BOOL)didOpen;

@end
