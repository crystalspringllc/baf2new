//
//  ProfileAdditionalInfoTableViewCell.m
//  Baf2
//
//  Created by Shyngys Kassymov on 20.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ProfileAdditionalInfoTableViewCell.h"
#import <JVFloatLabeledTextField.h>

@implementation ProfileAdditionalInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.firstnameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.firstnameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.firstnameTextField addTarget:self action:@selector(didChangeText:) forControlEvents:UIControlEventEditingChanged];

    self.lastnameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.lastnameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.lastnameTextField addTarget:self action:@selector(didChangeText:) forControlEvents:UIControlEventEditingChanged];
    
    self.middlenameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.middlenameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.middlenameTextField addTarget:self action:@selector(didChangeText:) forControlEvents:UIControlEventEditingChanged];
    
    self.birthdateTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.birthdateTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.birthdateTextField addTarget:self action:@selector(didChangeText:) forControlEvents:UIControlEventEditingChanged];
    
    self.datePicker = [UIDatePicker new];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.backgroundColor = [UIColor whiteColor];
    [self.datePicker addTarget:self action:@selector(datePickerDidChange:) forControlEvents:UIControlEventValueChanged];

    self.birthdateTextField.inputView = self.datePicker;

}

- (void)datePickerDidChange:(UIDatePicker *)dp {
    self.birthdateTextField.text = [[self dateFormatter] stringFromDate:dp.date];
    [self didChangeText:self.birthdateTextField];
}

- (void)didChangeText:(JVFloatLabeledTextField *)textField {
    if (self.didChangeText) {
        self.didChangeText(textField);
    }
}

- (NSDateFormatter *)dateFormatter {
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"dd.MM.yyyy"];
    
    return df;
}

@end
