//
//  ObjectsMapView.h
//  BAF
//
//  Created by Almas Adilbek on 5/19/15.
//
//



#import <MapKit/MapKit.h>

@class CityObject, OCMapView;

@protocol ObjectsMapViewDelegate<NSObject>
@optional
- (void)objectsMapViewOpenFullscreenMap;
- (void)didChangeMapFilter;
@end

@interface ObjectsMapView : UIView <UIGestureRecognizerDelegate, MKMapViewDelegate>

@property (nonatomic, weak) UIViewController *parentVC;
@property(nonatomic, strong) OCMapView *mv;
@property(nonatomic, weak) id <ObjectsMapViewDelegate> delegate;
@property(nonatomic, assign, getter=isFullscreen) BOOL fullscreen;

- (id)initWithHeight:(CGFloat)height;

- (void)loadCity:(CityObject *)city;
- (void)focusCity:(CityObject *)object;
- (void)updateTitles;

@end
