//
//  BlockSumAndTotalBalanceView.h
//  Baf2
//
//  Created by Askar Mustafin on 6/7/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlockSumAndTotalBalanceView : UIView

@property (weak, nonatomic) IBOutlet UILabel *blockedSumTitle;
@property (weak, nonatomic) IBOutlet UILabel *blockedSumLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalSumTitle;
@property (weak, nonatomic) IBOutlet UILabel *totalSumLabel;

- (void)hideBlockedSumLabel;
- (void)hideTotalSumLabel;

@end
