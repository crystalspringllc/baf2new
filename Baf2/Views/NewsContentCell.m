//
//  NewsContentCell.m
//  Baf2
//
//  Created by nmaksut on 17.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NewsContentCell.h"
#import "NSString+HTML.h"

@implementation NewsContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.textView.backgroundColor = [UIColor clearColor];
    self.textView.editable = NO;
    self.textView.scrollEnabled = NO;
}

- (void)setNews:(News *)news {
    NSString *msg = news.info.message;
    self.textView.text = [msg stringByConvertingHTMLToPlainText];
}

@end
