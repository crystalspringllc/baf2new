//
// Created by Askar Mustafin on 6/6/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "UIView+ConcisePureLayout.h"
#import "AMStackView.h"
#import "UIView+AAPureLayout.h"


@interface AMStackView() {}

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSLayoutConstraint *lastViewBottom;


@property (nonatomic, strong) NSLayoutConstraint *lastViewTop;

@end


@implementation AMStackView {}

- (int)itemsCount {
    return self.items.count;
}

/**
 * Pushes views in a horizontal stack maner.
 * Method tested only for 1 or 2 views inside
 *
 * @param view - view will pushed
 */
- (void)pushHorizontallyView:(UIView *)view {
    if (!self.items) {
        self.items = [[NSMutableArray alloc] init];
    }

    UIView *lastView = [self.items lastObject];


    [lastView removeConstraint:self.lastViewBottom];
    [self.lastViewBottom autoRemove];
    self.lastViewBottom = nil;

    [lastView removeConstraint:self.lastViewTop];
    [self.lastViewTop autoRemove];
    self.lastViewTop = nil;

    [self addSubview:view];
    [view aa_centerVertical];


    if (lastView) {
        [lastView aa_superviewLeft:10];
        [view aa_pinHorizontalAfterView:lastView offset:5];
    } else {
        self.lastViewTop = [view aa_superviewLeft:145];
    }

    self.lastViewBottom = [view aa_superviewRight:0];
    [self.items addObject:view];
}

- (void)pushView:(UIView *)view
       marginTop:(CGFloat)marginTop
      sideMargin:(CGFloat)sideMargin
   textAlignment:(AMStackViewTextAlignment)textAlignment {

    if (!self.items) {
        self.items = [[NSMutableArray alloc] init];
    }

    UIView *lastView = [self.items lastObject];
//    if (lastView) {
        [lastView removeConstraint:self.lastViewBottom];
        [self.lastViewBottom autoRemove];
        self.lastViewBottom = nil;
//    }


    [self addSubview:view];

    if (textAlignment == AMStackViewTextAlignmentCenter) {
        [view aa_centerHorizontal];
    } else if (textAlignment == AMStackViewTextAlignmentLeft) {
        [view aa_superviewLeft:sideMargin];
    } else if (textAlignment == AMStackViewTextAlignmentRight) {
        [view aa_superviewRight:sideMargin];
    } else if (textAlignment == AMStackViewTextAlignmentDefault) {
        [view aa_superviewLeft:sideMargin];
        [view aa_superviewRight:sideMargin];
    }


    if (!lastView) {
        [view aa_superviewTop:marginTop];
    } else {
        [view aa_pinUnderView:lastView offset:marginTop];
    }


    self.lastViewBottom = [view aa_superviewBottom:marginTop];


    [self.items addObject:view];
}

- (void)pushView:(UIView *)view marginTop:(CGFloat)marginTop marginBottom:(CGFloat)marginBottom marginLeft:(CGFloat)marginLeft marginRight:(CGFloat)marginRight {
    if (!self.items) {
        self.items = [[NSMutableArray alloc] init];
    }

    UIView *lastView = [self.items lastObject];
    [lastView removeConstraint:self.lastViewBottom];
    [self.lastViewBottom autoRemove];
    self.lastViewBottom = nil;
    [self addSubview:view];

    [view aa_superviewLeft:marginLeft];
    [view aa_superviewRight:marginRight];

    if (!lastView) {
        [view aa_superviewTop:marginTop];
    } else {
        [view aa_pinUnderView:lastView offset:marginTop];
    }

    self.lastViewBottom = [view aa_superviewBottom:marginTop];

    [self.items addObject:view];
}

- (void)removeAllPushsedViews {
    NSMutableArray *itemsToRemove = [[NSMutableArray alloc] init];
    for (UIView *view in self.items) {
        [view removeFromSuperview];
        [itemsToRemove addObject:view];
    }
    for (UIView *view in itemsToRemove) {
        [self.items removeObject:view];
    }
}


@end