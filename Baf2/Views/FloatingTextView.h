//
//  FloatingTextView.h
//  Baf2
//
//  Created by nmaksut on 19.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <JVFloatLabeledTextField/JVFloatLabeledTextField.h>
#import "JVFloatLabeledTextView.h"

@interface FloatingTextView : JVFloatLabeledTextView

@end
