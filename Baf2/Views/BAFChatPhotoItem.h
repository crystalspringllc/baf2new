//
//  BAFChatPhotoItem.h
//  Baf2
//
//  Created by Shyngys Kassymov on 19.07.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <JSQPhotoMediaItem.h>

@interface BAFChatPhotoItem : JSQPhotoMediaItem

- (instancetype)initWithURL:(NSString *)url;

@end
