//
//  OpportunitiesCollectionCell.h
//  Baf2
//
//  Created by nmaksut on 27.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpportunitiesCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
