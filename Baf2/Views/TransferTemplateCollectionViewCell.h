//
//  TransferTemplateCollectionViewCell.h
//  Baf2
//
//  Created by Shyngys Kassymov on 30.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransferTemplateCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cardImageView;

@property (weak, nonatomic) IBOutlet UILabel *requestorAccountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *requestorAccountLabel;

@property (weak, nonatomic) IBOutlet UILabel *destinationAccountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *destinationAccountLabel;

@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (nonatomic, copy) void (^didTapDeleteButton)();

@end
