//
//  AstanaView.m
//  Baf2
//
//  Created by Shyngys Kassymov on 30.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "AstanaView.h"

#define TRIANGLES_SPACING 0.05

@interface AstanaView ()
@property NSInteger refreshAnimationIteration;
@end

@implementation AstanaView

- (instancetype)init {
    self = [super init];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup {
    [self addAstanaLogoView];
}

- (void)addAstanaLogoView {
    /*
     
     |
     | Assuming that AstanaLogoView will have square frame
     |
     |         /\
     |        / 8\
     |       /____\
     |      /\    /\
     |     / 5\ 7/ 6\
     |    /____\/____\
     |   /\    /\    /\
     |  / 1\ 3/  \4 / 2\
     | /____\/    \/____\
     |
     | <----> - triangle width (== triangle height)
     
     */
    
    CGFloat sizeOfTriangle = self.frame.size.width / 3.0 - 2 * TRIANGLES_SPACING;
    CGFloat horizontalTrianglesSpacing = 2 * TRIANGLES_SPACING / 4.0;
    
    // Add triangles
    UIView *triangle1 = [[UIView alloc] initWithFrame:CGRectMake(0, 2 * sizeOfTriangle + 2 * TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
    triangle1.backgroundColor = [UIColor colorWithRed:12/255.0 green:74/255.0 blue:110/255.0 alpha:1.0];
    triangle1.layer.mask = [self createTriangleMask:YES];
    [self addSubview:triangle1];
    
    UIView *triangle3 = [[UIView alloc] initWithFrame:CGRectMake(sizeOfTriangle/2.0 + horizontalTrianglesSpacing, 2 * sizeOfTriangle + 2 * TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
    triangle3.backgroundColor = [UIColor colorWithRed:20/255.0 green:117/255.0 blue:136/255.0 alpha:1.0];
    triangle3.layer.mask = [self createTriangleMask:NO];
    [self addSubview:triangle3];
    
    UIView *triangle5 = [[UIView alloc] initWithFrame:CGRectMake(sizeOfTriangle/2.0 + horizontalTrianglesSpacing, sizeOfTriangle + TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
    triangle5.backgroundColor = [UIColor colorWithRed:26/255.0 green:144/255.0 blue:128/255.0 alpha:1.0];
    triangle5.layer.mask = [self createTriangleMask:YES];
    [self addSubview:triangle5];
    
    UIView *triangle7 = [[UIView alloc] initWithFrame:CGRectMake(sizeOfTriangle + 2 * horizontalTrianglesSpacing, sizeOfTriangle + TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
    triangle7.backgroundColor = [UIColor colorWithRed:152/255.0 green:204/255.0 blue:70/255.0 alpha:1.0];
    triangle7.layer.mask = [self createTriangleMask:NO];
    [self addSubview:triangle7];
    
    UIView *triangle8 = [[UIView alloc] initWithFrame:CGRectMake(sizeOfTriangle + 2 * horizontalTrianglesSpacing, 0, sizeOfTriangle, sizeOfTriangle)];
    triangle8.backgroundColor = [UIColor colorWithRed:201/255.0 green:221/255.0 blue:87/255.0 alpha:1.0];
    triangle8.layer.mask = [self createTriangleMask:YES];
    [self addSubview:triangle8];
    
    UIView *triangle6 = [[UIView alloc] initWithFrame:CGRectMake(1.5 * sizeOfTriangle + 3 * horizontalTrianglesSpacing, sizeOfTriangle + TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
    triangle6.backgroundColor = [UIColor colorWithRed:82/255.0 green:170/255.0 blue:108/255.0 alpha:1.0];
    triangle6.layer.mask = [self createTriangleMask:YES];
    [self addSubview:triangle6];
    
    UIView *triangle4 = [[UIView alloc] initWithFrame:CGRectMake(1.5 * sizeOfTriangle + 3 * horizontalTrianglesSpacing, 2 * sizeOfTriangle + 2 * TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
    triangle4.backgroundColor = [UIColor colorWithRed:41/255.0 green:144/255.0 blue:131/255.0 alpha:1.0];
    triangle4.layer.mask = [self createTriangleMask:NO];
    [self addSubview:triangle4];
    
    UIView *triangle2 = [[UIView alloc] initWithFrame:CGRectMake(2 * sizeOfTriangle + 4 * horizontalTrianglesSpacing, 2 * sizeOfTriangle + 2 * TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
    triangle2.backgroundColor = [UIColor colorWithRed:18/255.0 green:119/255.0 blue:111/255.0 alpha:1.0];
    triangle2.layer.mask = [self createTriangleMask:YES];
    [self addSubview:triangle2];
}

- (void)animateView {
    if (self.refreshAnimationIteration == self.subviews.count) {
        self.refreshAnimationIteration = 0;
    }
    
    UIView *currentView = self.subviews[self.refreshAnimationIteration];
    currentView.alpha = 0.5;
    
    [UIView animateWithDuration:0.25 animations:^{
        currentView.alpha = 1;
    } completion:^(BOOL finished) {
    }];
    
    self.refreshAnimationIteration++;
    [self performSelector:@selector(animateView) withObject:nil afterDelay:0.1 inModes:@[NSRunLoopCommonModes]];
}

#pragma mark - Helpers

- (CAShapeLayer *)createTriangleMask:(BOOL)isUp {
    CGFloat sizeOfTriangle = self.frame.size.width / 3.0 - 2 * TRIANGLES_SPACING;
    
    if (isUp) {
        // Mask path to form up triangle
        UIBezierPath *upTrianglePath = [UIBezierPath new];
        [upTrianglePath moveToPoint:(CGPoint){0, sizeOfTriangle}];
        [upTrianglePath addLineToPoint:(CGPoint){sizeOfTriangle/2.0, 0}];
        [upTrianglePath addLineToPoint:(CGPoint){sizeOfTriangle, sizeOfTriangle}];
        [upTrianglePath addLineToPoint:(CGPoint){0, sizeOfTriangle}];
        
        CAShapeLayer *upTriangleShapeMask = [CAShapeLayer new];
        upTriangleShapeMask.frame = CGRectMake(0, 0, sizeOfTriangle, sizeOfTriangle);
        upTriangleShapeMask.path = upTrianglePath.CGPath;
        
        return upTriangleShapeMask;
    }
    
    // Mask path to form down triangle
    UIBezierPath *downTrianglePath = [UIBezierPath new];
    [downTrianglePath moveToPoint:(CGPoint){0, 0}];
    [downTrianglePath addLineToPoint:(CGPoint){sizeOfTriangle, 0}];
    [downTrianglePath addLineToPoint:(CGPoint){sizeOfTriangle/2.0, sizeOfTriangle}];
    [downTrianglePath addLineToPoint:(CGPoint){0, 0}];
    
    CAShapeLayer *downTriangleShapeMask = [CAShapeLayer new];
    downTriangleShapeMask.frame = CGRectMake(0, 0, sizeOfTriangle, sizeOfTriangle);
    downTriangleShapeMask.path = downTrianglePath.CGPath;
    
    return downTriangleShapeMask;
}

@end
