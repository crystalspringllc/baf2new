//
// Created by Askar Mustafin on 2/22/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "TwoCheckBoxesSegmentedControl.h"
#import "M13Checkbox.h"
#import "ChameleonMacros.h"


@interface TwoCheckBoxesSegmentedControl ()
@property(nonatomic, strong) M13Checkbox *checkbox1;
@property(nonatomic, strong) M13Checkbox *checkbox2;
@end

@implementation TwoCheckBoxesSegmentedControl {

}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor whiteColor];
}

- (void)setItems:(NSArray *)items {
    _items = items;

    if (!self.checkbox1 && !self.checkbox2) {

        self.checkbox1 = [[M13Checkbox alloc] initWithTitle:self.items[0]];
        self.checkbox1.checkAlignment = M13CheckboxAlignmentLeft;
        self.checkbox1.checkColor = [UIColor flatOrangeColor];
        self.checkbox1.strokeColor = [UIColor flatOrangeColor];
        self.checkbox1.titleLabel.font = [UIFont systemFontOfSize:13];
        self.checkbox1.height = 30;
        self.checkbox1.width = self.width/2 - 30;
        self.checkbox1.x = 10;
        self.checkbox1.y = 5;
        [self addSubview:self.checkbox1];

        self.checkbox2 = [[M13Checkbox alloc] initWithTitle:self.items[1]];
        self.checkbox2.checkAlignment = M13CheckboxAlignmentLeft;
        self.checkbox2.checkColor = [UIColor flatOrangeColor];
        self.checkbox2.strokeColor = [UIColor flatOrangeColor];
        self.checkbox2.titleLabel.font = [UIFont systemFontOfSize:13];
        self.checkbox2.height = 30;
        self.checkbox2.width = self.width/2 - 30;
        self.checkbox2.x = self.checkbox1.right + 10;
        self.checkbox2.y = 5;
        [self addSubview:self.checkbox2];

        self.checkbox1.tag = 0;
        self.checkbox2.tag = 1;
        [self.checkbox1 addTarget:self action:@selector(checkboxValueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.checkbox2 addTarget:self action:@selector(checkboxValueChanged:) forControlEvents:UIControlEventValueChanged];


        self.checkbox1.checkState = M13CheckboxStateChecked;
        self.checkbox2.checkState = M13CheckboxStateUnchecked;

        _selectedItemIndex = 0;

    } else {

        self.checkbox1.titleLabel.text = items[0];
        self.checkbox2.titleLabel.text = items[1];

    }
}

- (void)setSelectedItemIndex:(NSInteger)selectedItemIndex {
    _selectedItemIndex = selectedItemIndex;
}

- (void)checkboxValueChanged:(M13Checkbox *)checkbox {
    [self toggleCheckbox:checkbox.tag];
}

- (void)uncheckedItems
{
    [self.checkbox1 setCheckState:M13CheckboxStateUnchecked];
    [self.checkbox2 setCheckState:M13CheckboxStateUnchecked];
}

- (BOOL)segmentItemsChecked
{
    if(self.checkbox1.checkState == M13CheckboxStateChecked || self.checkbox2.checkState == M13CheckboxStateChecked)
    {
        return YES;
    }else{
        return NO;
    }
    return nil;
}

- (void)toggleCheckbox:(NSInteger)checkboxIndex {
    if (checkboxIndex == 0) {
        _selectedItemIndex = 0;
        [self.checkbox1 setCheckState:M13CheckboxStateChecked];
        [self.checkbox2 setCheckState:M13CheckboxStateUnchecked];
    } else {
        _selectedItemIndex = 1;
        [self.checkbox1 setCheckState:M13CheckboxStateUnchecked];
        [self.checkbox2 setCheckState:M13CheckboxStateChecked];
    }

    NSLog(@"selecteg index : %i", self.selectedItemIndex);
}

@end
