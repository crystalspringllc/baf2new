//
//  NewsImagesCell.m
//  Baf2
//
//  Created by nmaksut on 17.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NewsImagesCell.h"
#import "NewsImageCollectionCell.h"
#import "MWPhotoBrowser.h"

@interface NewsImagesCell()

@end

@implementation NewsImagesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.collectionView.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
