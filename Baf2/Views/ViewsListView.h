//
//  ViewsListView.h
//  BAF
//
//  Created by Almas Adilbek on 8/14/14.
//
//



@interface ViewsListView : UIView

@property (nonatomic, strong) NSMutableIndexSet *indexesWithoutLines;
@property (nonatomic, strong) NSMutableIndexSet *indexesWithShortLines;
@property (nonatomic, strong) NSDictionary *leftOffsetForShortLines; // index: offset

- (id)viewsListWithViews:(NSArray *)views;

@end
