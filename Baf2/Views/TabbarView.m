//
//  TabbarView.m
//  BAF
//
//  Created by Almas Adilbek on 10/21/14.
//
//

#import "TabbarView.h"

@implementation TabbarView {
    NSMutableArray *tabs;
}

-(id)init {
    self = [super initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [TabbarView tabbarHeight])];
    if(self) {
        self.backgroundColor = [UIColor whiteColor];
        self.selectedIndex = -1;

        UIImageView *shadow = [[UIImageView alloc] initWithImage:[UIImage imageWithName:@"TabbarShadow"]];
        shadow.y = -shadow.height;
        shadow.width = self.width;
        [self addSubview:shadow];

        NSInteger tabsCount = 5;
        CGFloat tabWidth = [TabView tabWidth];
        CGFloat spacing = ([ASize screenWidth] - tabsCount * tabWidth) / (tabsCount + 1);

        tabs = [[NSMutableArray alloc] init];

        TabView *tab1 = [[TabView alloc] initWithIcon:@"btn_home" title:NSLocalizedString(@"main_uppercase", nil)];
        tab1.x = spacing;

        TabView *tab2 = [[TabView alloc] initWithIcon:@"btn_operation" title:NSLocalizedString(@"ОПЕРАЦИИ", nil)];
        tab2.x = tab1.right + spacing;

        TabView *tab3 = [[TabView alloc] initWithIcon:@"btn_pay" title:NSLocalizedString(@"ОПЛАТИТЬ", nil)];
        tab3.x = tab2.right + spacing;

        TabView *tab4 = [[TabView alloc] initWithIcon:@"btn_accounts_cards" title:NSLocalizedString(@"cards_and_accounts_uppercase", nil)];
        tab4.x = tab3.right + spacing;

        TabView *tab5 = [[TabView alloc] initWithIcon:@"btn_more" title:NSLocalizedString(@"МЕНЮ", nil)];
        tab5.x = tab4.right + spacing;

        [tabs addObject:tab1];
        [tabs addObject:tab2];
        [tabs addObject:tab3];
        [tabs addObject:tab4];
        [tabs addObject:tab5];

        NSInteger index = 0;
        for(TabView *tab in tabs) {
            ++index;
            tab.delegate = self;
            tab.tag = index;
            tab.centerY = self.centerY;
            [self addSubview:tab];
        }
    }
    return self;
}

#pragma mark -
#pragma mark Methods

+ (CGFloat)tabbarHeight {
    return 54;
}

#pragma mark -
#pragma mark TabView

-(void)tabView:(TabView *)tabView selectedTabWithTag:(NSInteger)tabTag
{
    [tabView setSelected:YES];

    self.selectedIndex = tabTag - 1;
    [self unselectOtherTabs];
    [self.delegate tabbarViewTappedAtIndex:_selectedIndex];
}

#pragma mark -
#pragma mark Helper

- (void)unselectOtherTabs {
    for(TabView *tab in tabs) {
        if(tab.tag - 1 != _selectedIndex) tab.selected = NO;
    }
}

@end