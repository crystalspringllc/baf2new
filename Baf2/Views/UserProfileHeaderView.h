//
//  UserProfileHeaderView.h
//  Baf2
//
//  Created by Shyngys Kassymov on 20.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileHeaderView : UIView

@property (nonatomic, weak) IBOutlet UIImageView *blurImageView;
@property (nonatomic, weak) IBOutlet UIView *blurAlphaView;
@property (nonatomic, weak) IBOutlet UIButton *avatarChangeButton;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *avatarSpinner;

@property (nonatomic, weak) UINavigationController *parentNavigationController;

@property (nonatomic, strong) UIImage *pickedImage;
@property (nonatomic, copy) void (^didCropImage)(UIImage *resultImage);

- (void)setAvatarImageFromUrlString:(NSString *)urlString;

@end
