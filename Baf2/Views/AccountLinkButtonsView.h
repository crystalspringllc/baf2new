//
//  AccountLinkButtonsView.h
//  Baf2
//
//  Created by Shyngys Kassymov on 23.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountLinkButtonsView : UIView

@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;

@end
