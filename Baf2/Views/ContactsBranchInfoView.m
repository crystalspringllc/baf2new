//
//  ContactsBranchInfoView.m
//  BAF
//
//  Created by Almas Adilbek on 5/18/15.
//
//

#import <PureLayout/ALView+PureLayout.h>
#import "ContactsBranchInfoView.h"
#import "UIView+AAPureLayout.h"
#import "TTTAttributedLabel.h"
#import "NSLayoutConstraint+PureLayout.h"

@implementation ContactsBranchInfoView {
    TTTAttributedLabel *telLabel;
    TTTAttributedLabel *emailLabel;
    TTTAttributedLabel *scheduleLabel;
    TTTAttributedLabel *faxLabel;
}

- (id)initWithWidth:(CGFloat)width {
    self = [super init];
    if(self) {

        [viewWidthConstraint autoRemove];
        viewWidthConstraint = [self autoSetDimension:ALDimensionWidth toSize:width];

        CGFloat contentWidth = width - 2 * contactsObjectInfoViewLeftInset;

        // Tel
        telLabel = [TTTAttributedLabel newAutoLayoutView];
        telLabel.backgroundColor = [UIColor clearColor];
        telLabel.lineBreakMode = NSLineBreakByWordWrapping;
        telLabel.numberOfLines = 0;
        telLabel.font = self.textFont;
        telLabel.textColor = self.textColor;
        [self addSubview:telLabel];
        [telLabel aa_pinUnderView:addressLabel offset:10];
        [telLabel aa_superviewLeft:contactsObjectInfoViewLeftInset];
        [telLabel autoSetDimension:ALDimensionWidth toSize:contentWidth];

        // Fax
        faxLabel = [TTTAttributedLabel newAutoLayoutView];
        faxLabel.backgroundColor = [UIColor clearColor];
        faxLabel.lineBreakMode = NSLineBreakByWordWrapping;
        faxLabel.numberOfLines = 0;
        faxLabel.font = self.textFont;
        faxLabel.textColor = self.textColor;
        [self addSubview:faxLabel];
        [faxLabel aa_pinUnderView:telLabel offset:10];
        [faxLabel aa_superviewLeft:contactsObjectInfoViewLeftInset];
        [faxLabel autoSetDimension:ALDimensionWidth toSize:contentWidth];

        // Email
        emailLabel = [TTTAttributedLabel newAutoLayoutView];
        emailLabel.backgroundColor = [UIColor clearColor];
        emailLabel.lineBreakMode = NSLineBreakByWordWrapping;
        emailLabel.numberOfLines = 0;
        emailLabel.font = self.textFont;
        emailLabel.textColor = self.textColor;
        [self addSubview:emailLabel];
        [emailLabel aa_pinUnderView:faxLabel offset:10];
        [emailLabel aa_superviewLeft:contactsObjectInfoViewLeftInset];
        [emailLabel autoSetDimension:ALDimensionWidth toSize:contentWidth];

        // Schedule
        scheduleLabel = [TTTAttributedLabel newAutoLayoutView];
        scheduleLabel.backgroundColor = [UIColor clearColor];
        scheduleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        scheduleLabel.numberOfLines = 0;
        scheduleLabel.font = self.textFont;
        scheduleLabel.textColor = self.textColor;
        [self addSubview:scheduleLabel];
        [scheduleLabel aa_pinUnderView:emailLabel offset:10];
        [scheduleLabel aa_superviewLeft:contactsObjectInfoViewLeftInset];
        [scheduleLabel autoSetDimension:ALDimensionWidth toSize:contentWidth];

        // Remove previous constraint
        [viewBottomConstraint autoRemove];

        viewBottomConstraint = [scheduleLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:15];
    }
    return self;
}

- (void)setTel:(NSString *)tel
{
    telLabel.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"phone", nil), tel];
    NSRange range = [telLabel.text rangeOfString:tel];
    [telLabel addLinkToURL:[[NSURL alloc] initWithString:[NSString stringWithFormat:@"tel:%@", tel]] withRange:range];
}

- (void)setFax:(NSString *)tel
{
    faxLabel.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"fax", nil),tel];
    NSRange range = [faxLabel.text rangeOfString:tel];
    [faxLabel addLinkToURL:[[NSURL alloc] initWithString:[NSString stringWithFormat:@"tel:%@", tel]] withRange:range];
}


- (void)setEmail:(NSString *)email
{
    emailLabel.text = [NSString stringWithFormat:@"E-mail: %@", email];
    NSRange range = [emailLabel.text rangeOfString:email];
    [emailLabel addLinkToURL:[[NSURL alloc] initWithString:[NSString stringWithFormat:@"mailto:%@", email]] withRange:range];
}

- (void)setSchedule:(NSString *)text
{
    scheduleLabel.text = text;
}

@end
