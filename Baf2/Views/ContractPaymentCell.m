//
//  ContractPaymentCell.m
//  Baf2
//
//  Created by Askar Mustafin on 4/25/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <AFNetworking/UIImageView+AFNetworking.h>
#import "ContractPaymentCell.h"


@implementation ContractPaymentCell

- (void)setContract:(Contract *)contract {
    _contract = contract;


    [self.icon setImageWithURL:[[NSURL alloc] initWithString:_contract.provider.logo] placeholderImage:nil];
    self.title.text = _contract.alias;
    self.subtitle.text = _contract.provider.categoryProviderName;

}

@end
