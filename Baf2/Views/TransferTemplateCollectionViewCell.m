//
//  TransferTemplateCollectionViewCell.m
//  Baf2
//
//  Created by Shyngys Kassymov on 30.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "TransferTemplateCollectionViewCell.h"

@implementation TransferTemplateCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.deleteButton addTarget:self action:@selector(deleteButtonTapped) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Actions

- (void)deleteButtonTapped {
    if (self.didTapDeleteButton) {
        self.didTapDeleteButton();
    }
}

@end
