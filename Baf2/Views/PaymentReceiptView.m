//
//  PaymentReceiptView.m
//  Baf2
//
//  Created by Shyngys Kassymov on 01.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "PaymentReceiptView.h"
#import "PaymentReceiptRowView.h"

@interface PaymentReceiptView ()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic, strong) UIStackView *contentView;

@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (nonatomic, strong) CAShapeLayer *shapeLayer;

@property (weak, nonatomic) IBOutlet UIView *zigzagView;

@end

@implementation PaymentReceiptView {
    BOOL didSetup;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setup];
    
    self.titleLabel.text = NSLocalizedString(@"authorization_service_tells_abount_successfull_card_withdraw", nil);
    self.amountTitleLabel.text = NSLocalizedString(@"amount", nil);
}

- (void)setup {
    if (!didSetup) {
        // shadow
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOffset = CGSizeZero;
        self.layer.shadowRadius = 3;
        self.layer.shadowOpacity = 0.25;
        
        // stack view
        self.contentView = [UIStackView newAutoLayoutView];
        self.contentView.distribution = UIStackViewDistributionFill;
        self.contentView.alignment = UIStackViewAlignmentFill;
        self.contentView.axis = UILayoutConstraintAxisVertical;
        self.contentView.spacing = 10;
        [self.containerView addSubview:self.contentView];
        [self.contentView autoPinEdgesToSuperviewEdges];
        
        // dashed line
        self.separatorView.backgroundColor = [UIColor clearColor];
        
        UIColor *fill = [UIColor fromRGB:0x979797];
        
        self.shapeLayer = [CAShapeLayer layer];
        self.shapeLayer.strokeStart = 0.0;
        self.shapeLayer.strokeColor = fill.CGColor;
        self.shapeLayer.lineWidth = 1;
        self.shapeLayer.lineJoin = kCALineJoinMiter;
        self.shapeLayer.lineDashPattern = [NSArray arrayWithObjects:[NSNumber numberWithInt:2],[NSNumber numberWithInt:2], nil];
        self.shapeLayer.lineDashPhase = 0;
        [self.separatorView.layer addSublayer:self.shapeLayer];
        
        // zigzag
        self.zigzagView.backgroundColor = [UIColor colorWithPatternImage:[UIImage scaleImage:[UIImage imageNamed:@"Rectangle"] maxWidth:10 maxHeight:5]];
        
        didSetup = true;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // shadow
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    
    // dashed line
    UIBezierPath *path = [UIBezierPath bezierPath];
    CGPoint separatorFrameOrigin = self.separatorView.bounds.origin;
    CGPoint separatorFrameEnd = CGPointMake(separatorFrameOrigin.x + self.separatorView.bounds.size.width, separatorFrameOrigin.y);
    [path moveToPoint:separatorFrameOrigin];
    [path addLineToPoint:separatorFrameEnd];
    [path stroke];
    
    CGFloat dashPattern[] = {2, 2};
    [path setLineDash:dashPattern count:2 phase:0];
    
    self.shapeLayer.path = path.CGPath;

    // labels
    self.titleLabel.preferredMaxLayoutWidth = self.titleLabel.frame.size.width;
}

#pragma mark - Methods

- (void)addReceiptRowWithTitle:(NSString *)title value:(NSString *)value {
    PaymentReceiptRowView *receiptRowView = [[NSBundle mainBundle] loadNibNamed:@"PaymentReceiptRowView" owner:self options:nil].firstObject;
    receiptRowView.titleLabel.text = title;
    receiptRowView.valueLabel.text = value;
    [self.contentView addArrangedSubview:receiptRowView];
}

@end
