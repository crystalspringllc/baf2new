//
// Created by Askar Mustafin on 6/6/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, AMStackViewTextAlignment) {
    AMStackViewTextAlignmentDefault,
    AMStackViewTextAlignmentCenter,
    AMStackViewTextAlignmentLeft,
    AMStackViewTextAlignmentRight
};


@interface AMStackView : UIView

@property (nonatomic, readonly) int itemsCount;

/**
 * Push view
 *
 * @param view
 * @param marginTop
 * @param sideMargin
 * @param textAligment
 *
 */
- (void)pushView:(UIView *)view marginTop:(CGFloat)marginTop sideMargin:(CGFloat)sideMargin textAlignment:(AMStackViewTextAlignment)textAlignment;


- (void)pushView:(UIView *)view marginTop:(CGFloat)marginTop marginBottom:(CGFloat)marginBottom marginLeft:(CGFloat)marginLeft marginRight:(CGFloat)marginRight;


/**
 *  Remove pushed views
 *
 */
- (void)removeAllPushsedViews;

/**
 * Pushes views in a horizontal stack maner.
 * Method tested only for 1 or 2 views inside
 * Used only for this particular case
 *
 * @param view - view will pushed
 */
- (void)pushHorizontallyView:(UIView *)view;

@end