//
//  NewsContentCell.h
//  Baf2
//
//  Created by nmaksut on 17.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"

@interface NewsContentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *textView;

- (void)setNews:(News *)news;
@end
