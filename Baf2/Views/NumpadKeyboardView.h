//
//  NumpadKeyboardView.h
//  BAF
//
//  Created by Almas Adilbek on 11/20/14.
//
//

#import <UIKit/UIKit.h>

@protocol NumpadKeyboardViewDelegate<NSObject>
- (void)numpadKeyboardViewTappedNumber:(NSInteger)number;
- (void)numpadKeyboardViewDeleteTapped;
- (void)numpadKeyboardViewTouchIdTapped;
@end

@interface NumpadKeyboardView : UIView

@property (nonatomic, weak) id <NumpadKeyboardViewDelegate> delegate;

@end
