//
//  SearchResultsView.h
//  Myth
//
//  Created by Almas Adilbek on 10/10/13.
//
//

#import <UIKit/UIKit.h>

@class Contract;
@class PaymentProvider;
@class ProviderCategory;

@protocol SearchResultsViewDelegate;

@interface SearchResultsView : UIView<UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate> {
    UITableView *list;
}

@property (nonatomic, weak) id<SearchResultsViewDelegate> delegate;

-(void)search:(NSString *)text;
-(void)searchOnlyProviderCategories:(NSString *)text;
- (UITableView *)getList;

@end

@protocol SearchResultsViewDelegate <NSObject>

@optional
-(void)searchResultsView:(SearchResultsView *)_searchResultsView contractTapped:(Contract *)contract;
-(void)searchResultsView:(SearchResultsView *)_searchResultsView providerTapped:(PaymentProvider *)provider;
-(void)searchResultsView:(SearchResultsView *)_searchResultsView providerCategoryTapped:(ProviderCategory *)category;
-(void)searchResultsViewHideKeyboard;
-(void)searchResultsViewDidHide;

@end
