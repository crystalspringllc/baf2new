//
// Created by Almas Adilbek on 8/22/14.
//

#import "ConnectAccountsMessageBoxView.h"
#import "MainButton.h"

#define kPadding 12

@implementation ConnectAccountsMessageBoxView {

}

-(id)init {
    self = [super initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 150)];
    if(self) {
        self.backgroundColor = [UIColor whiteColor];

//        UIImageView *bgView = [[UIImageView alloc] initWithFrame:self.bounds];
//        bgView.image = [[UIImage imageWithName:@"img_white_bg"] stretchableImageWithLeftCapWidth:15 topCapHeight:15];
//        [self addSubview:bgView];

        CGFloat contentWidth = self.width - 2 * kPadding;

        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kPadding, 15, contentWidth, 1)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:16];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.numberOfLines = 0;
        titleLabel.text = NSLocalizedString(@"you_client_our_bank", nil);
        [titleLabel sizeToFit];
        [self addSubview:titleLabel];

        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabel.x, titleLabel.bottom + 15, contentWidth, 1)];
        messageLabel.backgroundColor = [UIColor clearColor];
        messageLabel.font = [UIFont systemFontOfSize:14];
        messageLabel.textColor = [UIColor fromRGB:0x333333];
        messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        messageLabel.numberOfLines = 0;
        messageLabel.text = NSLocalizedString(@"for_get_information", nil);
        [messageLabel sizeToFit];
        [self addSubview:messageLabel];

        MainButton *connectButton = [[MainButton alloc] initWithFrame:CGRectMake(kPadding, messageLabel.bottom + 15, contentWidth, 44)];
        [connectButton addTarget:self action:@selector(connectTapped) forControlEvents:UIControlEventTouchUpInside];
        connectButton.mainButtonStyle = MainButtonStyleOrange;
        connectButton.frame = CGRectMake(kPadding, messageLabel.bottom + 15, contentWidth, 44);
        [connectButton setTitle:NSLocalizedString(@"connecti_accounts", nil)];
        [self addSubview:connectButton];

        self.height = connectButton.bottom + kPadding;

        UIView * line = [[UIView alloc] initWithFrame:CGRectMake(0, self.height - 1, [ASize screenWidth], 1)];
        [line setBackgroundColor:[UIColor fromRGB:0xdddddd]];
        [self addSubview:line];

        self.layer.shadowRadius = 3;
        self.layer.shadowOpacity = 0.2;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(0, 2);
    }
    return self;
}

- (void)connectTapped {
    [self.delegate connectAccountsMessageBoxViewConnectTapped];
}

@end