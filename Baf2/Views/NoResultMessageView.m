//
//  NoVideoMessageView.m
//  Kiwi.kz
//
//  Created by Almas Adilbek on 11/3/12.
//
//

#import "NoResultMessageView.h"

#define kTextTag 10

@interface NoResultMessageView()
-(void)baseInit;
@end

@implementation NoResultMessageView

@synthesize iconView;

-(id)init {
    self = [super initWithFrame:CGRectMake(0, 50, [ASize screenWidth], 1)];
    if(self) {
        [self baseInit];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self baseInit];
    }
    return self;
}

-(void)baseInit
{
    self.backgroundColor = [UIColor clearColor];
    
    int w = self.width;
//    int iconSize = 65;
//    iconView = [[UIImageView alloc] initWithFrame:CGRectMake((w - iconSize) * 0.5, 0, iconSize, iconSize)];
//    [self addSubview:iconView];
    
    int sidePadding = 65;
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(sidePadding, 0, w - sidePadding * 2, 1)];
    textLabel.tag = kTextTag;
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.font = [UIFont systemFontOfSize:15];
    textLabel.textColor = [UIColor darkGrayColor];
    textLabel.shadowColor = [UIColor whiteColor];
    textLabel.shadowOffset = CGSizeMake(0, 1);
    textLabel.numberOfLines = 0;
    textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self addSubview:textLabel];
}

-(void)setText:(NSString *)text {
    UILabel *t = (UILabel *)[self viewWithTag:kTextTag];
    t.text = text;
    [t sizeToFit];
    
    CGRect f = t.frame;
    f.origin.x = (self.width - f.size.width) * 0.5;
    t.frame = f;
    
    self.height = t.bottom;
}

@end
