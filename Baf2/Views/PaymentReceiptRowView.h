//
//  PaymentReceiptRowView.h
//  Baf2
//
//  Created by Shyngys Kassymov on 01.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentReceiptRowView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

@end
