//
// Created by Askar Mustafin on 4/5/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <ChameleonFramework/ChameleonMacros.h>
#import "FUIButton.h"
#import "MainButton.h"

@implementation MainButton {}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setMainButtonStyle:MainButtonStyleOrange];
}

- (void)setMainButtonStyle:(MainButtonStyle)mainButtonStyle {
    if (mainButtonStyle == MainButtonStyleOrange) {
        self.userInteractionEnabled = YES;
        self.buttonColor = [UIColor mainButtonColor];
        self.shadowColor = [UIColor mainButtonColor];
        self.highlightedColor = [UIColor mainButtonShadowColor];
        self.shadowHeight = 2.0f;
        self.cornerRadius = 3.0f;
        self.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    } else if (mainButtonStyle == MainButtonStyleWhite) {
        self.userInteractionEnabled = YES;
        self.buttonColor = [UIColor flatWhiteColor];
        self.shadowColor = [UIColor flatWhiteColor];
        self.highlightedColor = [UIColor flatWhiteColorDark];
        self.shadowHeight = 2.0f;
        self.cornerRadius = 3.0f;
        self.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [self setTitleColor:[UIColor fromRGB:0x575757] forState:UIControlStateNormal];
        [self setTitleColor:[UIColor fromRGB:0x575757] forState:UIControlStateHighlighted];
    } else if (mainButtonStyle == MainButtonStyleDisable) {
        self.userInteractionEnabled = NO;
        self.buttonColor = [UIColor fromRGB:0xFCB37A];
        self.shadowColor = [UIColor fromRGB:0xFCB37A];
        self.shadowHeight = 2.0f;
        self.cornerRadius = 3.0f;
        self.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    } else if (mainButtonStyle == MainButtonStyleRed) {
        self.userInteractionEnabled = YES;
        self.buttonColor = [UIColor flatRedColor];
        self.shadowColor = [UIColor flatRedColor];
        self.highlightedColor = [UIColor flatRedColorDark];
        self.shadowHeight = 2.0f;
        self.cornerRadius = 3.0f;
        self.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    }
}

- (void)setTitle:(NSString *)title {
    [self setTitle:[title uppercaseString] forState:UIControlStateNormal];
}

- (void)setTitle:(NSString *)title forState:(UIControlState)state {
    title = [title uppercaseString];
    [super setTitle:title forState:state];
}

@end