//
//  ContractPaymentCell.h
//  Baf2
//
//  Created by Askar Mustafin on 4/25/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contract.h"

@interface ContractPaymentCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *subtitle;

@property (nonatomic, weak) Contract *contract;

@end