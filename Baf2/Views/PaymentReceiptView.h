//
//  PaymentReceiptView.h
//  Baf2
//
//  Created by Shyngys Kassymov on 01.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentReceiptView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *amountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountValueLabel;

#pragma mark - Methods

- (void)addReceiptRowWithTitle:(NSString *)title value:(NSString *)value;

@end
