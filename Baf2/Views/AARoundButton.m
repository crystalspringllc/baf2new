//
//  AARoundButton.m
//  Toppy
//
//  Created by Almas Adilbek on 4/13/15.
//  Copyright (c) 2015 Toppy Inc. All rights reserved.
//

#import "AARoundButton.h"

@implementation AARoundButton {
    BOOL isFixedWidth;
    UIImageView *iconView;
    UIActivityIndicatorView *loader;

    NSString *buttonTitle;
}

- (id)init {
    self = [self initWithHeight:44];
    if(self) {

    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        [self config];
    }
    return self;
}

- (void)setTransparentOnTap:(BOOL)transparentOnTap
{
    _transparentOnTap = transparentOnTap;
    [self applyParamsForLabel];
}

- (id)initWithHeight:(CGFloat)height
{
    self = [self initWithFrame:CGRectMake(0, 0, 0, height)];
    if(self) {

    }
    return self;
}

- (void)setTitle:(NSString *)title
{
    buttonTitle = title;
    [self setTitle:title forState:UIControlStateNormal];
    [self applyBounds];
}

- (void)setTitleColor:(UIColor *)color {
    [self setTitleColor:color forState:UIControlStateNormal];
    [self setTitleColor:color forState:UIControlStateHighlighted];
}

- (void)setFont:(UIFont *)font
{
    [self.titleLabel setFont:font];
    [self applyBounds];
}

- (void)setIcon:(NSString *)iconName
{
    if(iconView) {
        [iconView removeFromSuperview];
    }
    iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:iconName]];
    iconView.centerY = self.middleY;
    [self addSubview:iconView];

    [self applyBounds];
    [self layoutSubviews];
}

- (void)setButtonEnabled:(BOOL)enabled {
    self.enabled = enabled;
    if(!enabled) self.alpha = 0.5;
    else self.alpha = 1;
}

- (void)setTitleAndBorderColor:(UIColor *)color {
    [self setBorderColor:color];
    [self setTitleColor:color];
}

- (void)notRounded
{
    self.layer.cornerRadius = 0;
    self.layer.masksToBounds = NO;
}

- (void)startLoading
{
    if(!loader)
    {
        if(iconView) {
            iconView.hidden = YES;
        }
        [self setTitle:@"" forState:UIControlStateNormal];

        loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        loader.hidesWhenStopped = YES;
        loader.center = self.middlePoint;
        [self addSubview:loader];
        [loader startAnimating];
    }
}

- (void)stopLoading {
    if(loader) {
        [loader stopAnimating];
        [loader removeFromSuperview];
        loader = nil;

        if(iconView) {
            iconView.hidden = NO;
        }
        [self setTitle:buttonTitle forState:UIControlStateNormal];
    }
}

#pragma mark -

- (void)config
{
    [self addTarget:self action:@selector(buttonTapDown) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(buttonTapUp) forControlEvents:UIControlEventTouchUpOutside];
    [self addTarget:self action:@selector(buttonTapUp) forControlEvents:UIControlEventTouchUpInside];

    self.backgroundColor = [UIColor clearColor];

    // Check if initialized with initWithFrame
    if(self.frame.size.width != 0) isFixedWidth = YES;

    _transparentOnTap = YES;
    _onTapAlpha = 0.6;
    _sidePadding = 55;
    _borderColor = [UIColor whiteColor];
    _borderWidth = 2.0f;
    _cornerRadius = (CGFloat) (self.height * 0.5);
    _iconTitleSpacing = 4;

    self.layer.masksToBounds = YES;
    self.layer.borderColor = _borderColor.CGColor;
    self.layer.borderWidth = _borderWidth;

    [self applyCornerRadius];
    [self applyParamsForLabel];
    [self applyBounds];
}

- (void)applyParamsForLabel
{
    [self setTitleColor:_borderColor forState:UIControlStateNormal];
    if(!self.isTransparentOnTap) {
        [self setTitleColor:_highlighedBorderColor forState:UIControlStateHighlighted];
    } else {
        [self setTitleColor:_borderColor forState:UIControlStateHighlighted];
    }
    [self applyBounds];
}

- (void)applyCornerRadius {
    if(self.frame.size.width > 0 && self.frame.size.height > 0) {
        self.layer.cornerRadius = _cornerRadius;
    }
}

- (void)applyBounds {
    if(!isFixedWidth) {
        CGRect f = self.frame;
        f.size.width = [self labelWidth] + 2 * _sidePadding;
        if(iconView) f.size.width += iconView.width + _iconTitleSpacing;
        self.frame = f;
    }
}

#pragma mark -
#pragma mark Actions

- (void)buttonTapDown
{
    if(_transparentOnTap) {
        self.alpha = _onTapAlpha;
    } else {
        self.layer.borderColor = _highlighedBorderColor.CGColor;
    }
}

- (void)buttonTapUp
{
    self.layer.borderColor = _borderColor.CGColor;
    self.alpha = 1;
}

#pragma mark -
#pragma mark Methods

- (void)setBorderWidth:(CGFloat)borderWidth
{
    _borderWidth = borderWidth;
    self.layer.borderWidth = _borderWidth;
}

- (void)setBorderColor:(UIColor *)borderColor
{
    _borderColor = borderColor;
    self.layer.borderColor = borderColor.CGColor;
    [self applyParamsForLabel];
}

- (void)setHighlighedBorderColor:(UIColor *)highlighedBorderColor
{
    _highlighedBorderColor = highlighedBorderColor;
    [self applyParamsForLabel];
}

- (void)setFillColor:(UIColor *)fillColor
{
    _fillColor = fillColor;
    self.backgroundColor = fillColor;
}

- (void)setIconTitleSpacing:(CGFloat)iconTitleSpacing
{
    _iconTitleSpacing = iconTitleSpacing;
    [self applyBounds];
    [self layoutSubviews];
}


- (void)setCornerRadius:(CGFloat)cornerRadius
{
    _cornerRadius = cornerRadius;
    [self applyCornerRadius];
}

- (void)setSidePadding:(CGFloat)sidePadding
{
    _sidePadding = sidePadding;
    isFixedWidth = NO;
    [self applyBounds];
}

- (void)setEnabled:(BOOL)enabled {

}

#pragma mark -
#pragma mark Helper

- (CGFloat)labelWidth
{
    if(self.titleLabel.text.length == 0) return 0;

    NSDictionary *attributes = @{NSFontAttributeName:self.titleLabel.font};
    CGRect rect = [self.titleLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributes
                                      context:nil];

    return rect.size.width;
}

#pragma mark -
#pragma mark System

- (void)layoutSubviews
{
    [super layoutSubviews];
    if(iconView) {
        CGFloat w = iconView.frame.size.width + _iconTitleSpacing + [self labelWidth];
        CGFloat padding = (CGFloat) ((self.frame.size.width - w) * 0.5);
        iconView.x = padding;
        self.titleLabel.x = padding + iconView.frame.size.width + _iconTitleSpacing;
    }
}

@end
