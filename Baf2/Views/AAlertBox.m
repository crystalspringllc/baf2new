//
//  AAlertBox.m
//  BAF
//
//  Created by Almas Adilbek on 6/9/15.
//
//

#import <PureLayout/ALView+PureLayout.h>
#import "AAlertBox.h"
#import "UIView+AAPureLayout.h"

@interface AAlertBox ()
@property(nonatomic, strong) UILabel *messageLabel;
@end

@implementation AAlertBox {
    UIView *backgroundView;

    CGFloat contentInset;
    UIActivityIndicatorView *loader;
    UILabel *loadingTitleLabel;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        contentInset = 10;
        [self configUI];
    }
    return self;
}

- (void)setStyle:(AlertBoxStyle)style
{
    _style = style;
    switch (style) {
        case AlertBoxStyleWarning:
            [self setBackgroundColor:[UIColor fromRGB:0xffecec] borderColor:[UIColor fromRGB:0xe4bebe]];
            break;
        case AlertBoxStyleInfo:
            [self setBackgroundColor:[UIColor fromRGB:0xFFFDDD] borderColor:[UIColor fromRGB:0xd7cf48]];
            break;
        case AlertBoxStyleData:
            [self setBackgroundColor:[UIColor fromRGB:0xf1f1f1] borderColor:[UIColor fromRGB:0xd2d2d2]];
            break;
    }
}

- (void)setMessage:(NSString *)message {
    [self setMessage:message marginTop:contentInset];
}

- (void)setMessage:(NSString *)message marginTop:(CGFloat)marginTop
{
    contentInset = marginTop;
    if(!_messageLabel) {
        self.messageLabel = [UILabel newAutoLayoutView];
        _messageLabel.backgroundColor = [UIColor clearColor];
        _messageLabel.font = [UIFont systemFontOfSize:12];
        _messageLabel.textColor = [UIColor blackColor];
        _messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _messageLabel.numberOfLines = 0;
        [backgroundView addSubview:_messageLabel];

        [_messageLabel aa_insideSuperview:contentInset];
    }

    NSError* error;
    message = [message stringByAppendingString:@"<style>*{font-family: 'Helvetica';font-size: 13px;}</style>"];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithData:[message dataUsingEncoding:NSUTF8StringEncoding]
                                                                             options:@{NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute : @(NSUTF8StringEncoding)}
                                                                  documentAttributes:nil error:&error];

    _messageLabel.attributedText = str;
}

#pragma mark -
#pragma mark Loading

- (void)startLoading {
    [self startLoadingWithTitle:nil];
}

- (void)startLoadingWithTitle:(NSString *)title
{
    if(!loader) {
        if(_messageLabel) {
            [_messageLabel removeFromSuperview];
            _messageLabel = nil;
        }

        loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        loader.translatesAutoresizingMaskIntoConstraints = YES;
        loader.hidesWhenStopped = YES;
        [backgroundView addSubview:loader];

        if(title) {
            [loader aa_superviewLeft:contentInset];
            [loader aa_superviewTop:contentInset];
            [loader aa_superviewBottom:contentInset];
        } else {
            [loader aa_insideSuperview:contentInset];
        }
        [loader startAnimating];

        if(title) {
            loadingTitleLabel = [UILabel newAutoLayoutView];
            loadingTitleLabel.font = [UIFont systemFontOfSize:14];
            loadingTitleLabel.textColor = [UIColor blackColor];
            [backgroundView addSubview:loadingTitleLabel];

            [loadingTitleLabel aa_pinHorizontalAfterView:loader offset:10];
            [loadingTitleLabel aa_superviewRight:contentInset];
            [loadingTitleLabel aa_verticalAlignWithView:loader];

            loadingTitleLabel.text = title;
        }
    }
}

- (void)stopLoading
{
    if(loader) {
        [loader stopAnimating];
        [loader removeFromSuperview];
        loader = nil;

        [loadingTitleLabel removeFromSuperview];
        loadingTitleLabel = nil;
    }
}

#pragma mark -
#pragma mark Helper

- (void)setBackgroundColor:(UIColor *)bgColor borderColor:(UIColor *)borderColor
{
    backgroundView.backgroundColor = bgColor;
    backgroundView.layer.borderColor = borderColor.CGColor;
}

- (void)setBackgroundColorWithBorder:(UIColor *)bgColor
{
    backgroundView.backgroundColor = bgColor;
    backgroundView.layer.borderColor = [bgColor darkerColor].CGColor;
}

- (void)configUI
{
    backgroundView = [UIView newAutoLayoutView];
    backgroundView.layer.borderWidth = 1;
    backgroundView.layer.cornerRadius = 4;
    backgroundView.layer.masksToBounds = YES;
    [self addSubview:backgroundView];
    [backgroundView aa_insideSuperview:0];

    self.style = AlertBoxStyleInfo;
}

@end
