//
// Created by Almas Adilbek on 8/29/14.
//

#import <Foundation/Foundation.h>


@interface NSString (Hashing)
- (NSString *)md5;
@end

@interface NSData (Hashing)
- (NSString*)md5;
@end