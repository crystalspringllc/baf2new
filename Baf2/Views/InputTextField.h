//
// Created by Askar Mustafin on 9/7/14.
// Copyright (c) 2014 Askar Mustafin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OCMaskedTextFieldView;

typedef void (^InputTextFieldOnValueChangedBlock)(void);
typedef void (^InputTextFieldOnValueChangeBlock)(void);

@interface InputTextField : UIView <UITextFieldDelegate> {}

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) OCMaskedTextFieldView *maskedTextFieldView;
@property (nonatomic, strong) UIView * line;

@property (nonatomic, copy) void (^textFieldDidBeginEditingBlock)(UITextField *textField);
@property (nonatomic, assign, getter=isLoading) BOOL loading;
@property (nonatomic, assign) NSUInteger maxCharacters;

- (NSString *)value;
- (void)clear;
- (void)focus;

- (void)setValue:(NSString *)value;
- (void)setPlaceholder:(NSString *)placeholder;
- (void)enabled:(BOOL)enabled;

// Icon
- (void)setIcon:(NSString *)iconName;

// Right stuff
- (void)setRightText:(NSString *)text;
- (void)setRightView:(UIView *)view;
- (void)setRightView:(UIView *)view rightPadding:(CGFloat)padding;

// Prefix
- (void)setPrefix:(NSString *)prefixText;

// Keyboard
- (void)setEmailKeyboard;
- (void)setPhoneNumberKeyboard;
- (void)setPasswordField;

// Loader
- (void)startLoading;
- (void)stopLoading;

// Blocks
- (void)onValueChanged:(InputTextFieldOnValueChangedBlock)block;
- (void)onValueChange:(InputTextFieldOnValueChangeBlock)block;

//Mask
- (void)setPhoneMask;
- (void)setMaskString:(NSString *)maskString;

- (void)setAligment:(NSTextAlignment)alignment;

@end
