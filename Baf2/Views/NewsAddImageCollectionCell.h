//
//  NewsAddImageCollectionCell.h
//  Baf2
//
//  Created by nmaksut on 18.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DeleteBlock)();

@interface NewsAddImageCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *addImageView;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong, nonatomic) DeleteBlock deleteBlock;

@end
