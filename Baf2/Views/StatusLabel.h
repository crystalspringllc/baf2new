//
//  StatusLabel.h
//  Baf2
//
//  Created by Shyngys Kassymov on 15.02.17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel.h>

typedef enum StatusType: NSInteger {
    Warning,
    Error,
    Info
} StatusType;

@interface StatusLabel : TTTAttributedLabel

@property (nonatomic) UIEdgeInsets edgeInsets;
@property (nonatomic) StatusType statusType;
    
- (instancetype)initWithType:(StatusType)statusType;
- (void)setHTMLText:(NSString *)html;
    
@end
