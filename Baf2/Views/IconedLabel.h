//
//  IconedLabel.h
//  Baf2
//
//  Created by Askar Mustafin on 4/11/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IconedLabel : UIView
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (nonatomic, assign) CGFloat leftIndent;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loaderView;

@end