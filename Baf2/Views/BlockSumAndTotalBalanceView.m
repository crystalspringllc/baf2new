//
//  BlockSumAndTotalBalanceView.m
//  Baf2
//
//  Created by Askar Mustafin on 6/7/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "UIView+ConcisePureLayout.h"
#import "BlockSumAndTotalBalanceView.h"

@interface BlockSumAndTotalBalanceView()
@property (weak, nonatomic) IBOutlet UIView *blockedSumContainerView;
@property (weak, nonatomic) IBOutlet UIView *totalSumContainerLabel;
@end

@implementation BlockSumAndTotalBalanceView


- (id)init {
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"BlockSumAndTotalBalanceView"
                                                      owner:self
                                                    options:nil];
    for (id view in nibViews) {
        if ([view isKindOfClass:[UIView class]]) {
            return view;
        }
    }
    return nil;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.totalSumTitle.text = NSLocalizedString(@"blocked", nil);
    self.totalSumTitle.text = NSLocalizedString(@"total_sum", nil);
}



#pragma mark - config actions

- (void)hideBlockedSumLabel {
    [self.blockedSumContainerView removeFromSuperview];
    [self.blockedSumContainerView aa_superviewLeft:0];
}

- (void)hideTotalSumLabel {
    [self.totalSumContainerLabel removeFromSuperview];
    [self.blockedSumContainerView aa_superviewRight:0];
}

@end
