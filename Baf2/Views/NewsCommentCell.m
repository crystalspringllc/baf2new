//
//  NewsCommentCell.m
//  Baf2
//
//  Created by nmaksut on 17.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NewsCommentCell.h"
#import "NSString+HTML.h"
#import "User.h"

@implementation NewsCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.textView.editable = NO;
    self.textView.scrollEnabled = NO;
    
    [self.likesButton setImage:[UIImage imageNamed:@"icn_like_norm"] forState:UIControlStateNormal];
    [self.likesButton setImage:[UIImage imageNamed:@"icn_like_tab"] forState:UIControlStateSelected];
    [self.likesButton addTarget:self action:@selector(likeTap) forControlEvents:UIControlEventTouchUpInside];
    
    self.trashButton.tintColor = [UIColor colorWithRed:0 green:152/255.0 blue:152/255.0 alpha:1.0];
    [self.trashButton addTarget:self action:@selector(trashButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [self setTrashButtonHidden:true];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.textView.layer.cornerRadius = 4;
    self.textView.layer.masksToBounds = YES;
}

- (void)setComment:(Comment *)comment {
    if (comment.isAdminPost) self.contentView.backgroundColor = [[UIColor fromRGB:0xFFFAF5] colorWithAlphaComponent:0.5];
    else self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5] ;
    
    self.avatarImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:comment.src.avaUrl] placeholderImage:[UIImage imageNamed:@"icon_profile_placeholder"]];
    
    self.usernameLabel.text = comment.src.nick;
    self.textView.editable  = NO;
    self.dateLabel.text     = comment.formattedDateString;
    
    self.likesButton.selected = comment.like;
    [self.likesButton setTitle:[NSString stringWithFormat:@"%@ ", comment.likes] forState:UIControlStateNormal];
    
    NSString *msg = comment.info.message;
    
    self.textView.text = [msg stringByConvertingHTMLToPlainText];

    if ([User sharedInstance].userID == [comment.src.commentSrcId integerValue]) {
        [self setTrashButtonHidden:false];
    }
}

- (void)setTrashButtonHidden:(BOOL)hidden {
    if (hidden) {
        self.trashButtonWidthConstraint.constant = 0;
        self.trashButtonLeftConstraint.constant = 0;
    } else {
        self.trashButtonWidthConstraint.constant = 18;
        self.trashButtonLeftConstraint.constant = 16;
    }
    [self layoutIfNeeded];
}

#pragma mark - Actions

- (void)likeTap {
    if (_likeBlock) self.likeBlock();
}

- (void)trashButtonTapped {
    if (self.didTapTrashButton) {
        self.didTapTrashButton();
    }
}

@end
