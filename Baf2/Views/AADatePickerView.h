//
//  AADatePickerView.h
//  WashMe
//
//  Created by Almas Adilbek on 10/26/14.
//  Copyright (c) 2014 Almas Adilbek. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AADatePickerViewDelegate;

@interface AADatePickerView : UIView

@property(nonatomic, weak) id <AADatePickerViewDelegate> delegate;
@property(nonatomic, strong) UIDatePicker *datePicker;

- (void)setTitle:(NSString *)title;
- (void)setDatePickerDate:(NSDate *)date;

- (void)show;
- (void)hide;

@end

@protocol AADatePickerViewDelegate<NSObject>
- (void)datePickerViewDidChange:(NSString *)dateString;
@end
