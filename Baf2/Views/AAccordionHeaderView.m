//
//  AAccordionHeaderView.m
//  BAF
//
//  Created by Almas Adilbek on 8/12/14.
//
//

#import "AAccordionHeaderView.h"
#import "Hashing.h"

#define kHeaderHeight 44
#define kPadding 10

@implementation AAccordionHeaderView {
    UIImageView *arrowView;
    UIImageView *iconView;
    NSString *identifier;
}

- (id)initWithTitle:(NSString *)title icon:(NSString *)iconName {
    return [self initWithTitle:title icon:iconName width:[ASize screenWidth]];
}

- (id)initWithTitle:(NSString *)title icon:(NSString *)iconName width:(CGFloat)width {
    return [self initWithTitle:title icon:iconName width:width detailColor:[UIColor clearColor]];
}

- (id)initWithTitle:(NSString *)title icon:(NSString *)iconName width:(CGFloat)width detailColor:(UIColor *)color {
    self = [super initWithFrame:CGRectMake(0, 0, width, kHeaderHeight)];
    if (self) {
        // Init vars
        
        identifier = [[title stringByAppendingString:iconName ? : @""] md5];
        self.revealDuration = 0.25;
        
        // Config
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 4;
        self.layer.shadowColor = [UIColor fromRGB:0x444444].CGColor;
        self.layer.shadowOffset = CGSizeMake(0,0);
        self.layer.shadowOpacity = 0.3;
        
        UIView *headerView = [[UIView alloc] initWithFrame:self.bounds];
        headerView.backgroundColor = [UIColor whiteColor];
        [self addSubview:headerView];
        
        UIView *titleContanerView = [[UIView alloc] init];
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont systemFontOfSize:12];
        titleLabel.textColor = [UIColor fromRGB:0x444444];
        titleLabel.text = [title uppercaseString];
        [titleLabel sizeToFit];
        [titleContanerView addSubview:titleLabel];
        titleContanerView.width = titleLabel.width + 24;
        titleContanerView.height = titleLabel.height + 5;
        titleContanerView.x = 12;
        titleContanerView.y = (CGFloat) ((headerView.height - titleContanerView.height) * 0.5);
        titleLabel.x = 0;
        titleLabel.y = (CGFloat) ((titleContanerView.height - titleLabel.height) * 0.5);
        titleContanerView.backgroundColor = [UIColor clearColor];
        
        [self addSubview:titleContanerView];
        
        // Add detail color view
        UIView *detailColorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 3, kHeaderHeight)];
        detailColorView.backgroundColor = color;
        [self addSubview:detailColorView];
        
        // Open close button
        arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"expand_arrow_green"]];
        arrowView.frame = CGRectMake(0, 0, 12, 12);
        arrowView.x = self.width - arrowView.width - kPadding;
        arrowView.y = (CGFloat) ((self.height - arrowView.height) * 0.5);
        [self addSubview:arrowView];
        
        // Trigger button
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = self.bounds;
        [button addTarget:self action:@selector(onHeaderTap) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
    }
    return self;
}

/**
*  Set content view
*  @param view
*/
- (void)setContentView:(UIView *)view
{
    _contentView = view;
    _contentView.y = kHeaderHeight;
    [self addSubview:_contentView];

    // Get state
    self.isOpen = [[NSUserDefaults standardUserDefaults] boolForKey:identifier];
    if (_isOpen) {
        [self openCloseContent:NO];
    } else {
        self.contentView.alpha = 0;
    }
}

-(void)toggleHeader {
    [self onHeaderTap];
}

- (void)onHeaderTap {
    self.isOpen = !self.isOpen;
    [self openCloseContent:YES];
}

- (void)setIsOpen:(BOOL)isOpen {
    _isOpen = isOpen;
}

-(void)openCloseContent:(BOOL)animated
{
    if(_contentView)
    {
        self.userInteractionEnabled = NO;
        if(_isOpen) {
            if(animated) {
                [UIView animateWithDuration:_revealDuration animations:^{
                    self.height = _contentView.bottom;
                    self.contentView.alpha = 1;

                } completion:^(BOOL finished) {
                    self.userInteractionEnabled = YES;
                }];
            } else {
                self.height = _contentView.bottom;
                self.userInteractionEnabled = YES;
            }

        } else {

            if(animated) {
                [UIView animateWithDuration:_revealDuration animations:^{
                    self.height = kHeaderHeight;

                    self.contentView.alpha = 0;
                } completion:^(BOOL finished) {
                    self.userInteractionEnabled = YES;
                }];
            } else {
                self.height = kHeaderHeight;
                self.userInteractionEnabled = YES;
            }
        }
        arrowView.transform = CGAffineTransformMakeRotation(self.isOpen?((CGFloat) M_PI_2 * 2):0);
        [self.delegate accordionHeaderView:self opened:self.isOpen];

        // Save accordion open/closed state
        [[NSUserDefaults standardUserDefaults] setBool:self.isOpen forKey:identifier];


    }
}

@end
