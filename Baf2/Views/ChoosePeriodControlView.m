//
//  ChoosePeriodControlView.m
//  Baf2
//
//  Created by Askar Mustafin on 4/11/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ChoosePeriodControlView.h"
#import "UIButton+BackgroudForState.h"

@implementation ChoosePeriodControlView

- (id)init {
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"ChoosePeriodView"
                                                      owner:self
                                                    options:nil];
    for (id view in nibViews) {
        if ([view isKindOfClass:[UIView class]]) {
            return view;
        }
    }
    return nil;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.choosePeriodButton setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.3] forState:UIControlStateHighlighted];
    
    self.titleLabel.text = NSLocalizedString(@"period", nil);
    self.choosenPeriodLabel.text = @"Последние 3 дня";
}

#pragma mark - config actions

- (IBAction)clickChoosePerionButton:(id)sender {
    if (self.onChoosePeriodButtonClick) {
        self.onChoosePeriodButtonClick();
    }
}

@end
