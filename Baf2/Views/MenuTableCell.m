//
//  MenuTableCell.m
//  Baf2
//
//  Created by nmaksut on 04.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "MenuTableCell.h"

@interface MenuTableCell()
@end

@implementation MenuTableCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.backgroundColor = [UIColor fromRGB:0x364048];
    self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    self.textLabel.textColor = [UIColor whiteColor];

    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor fromRGB:0x364048];
    [self setSelectedBackgroundView:bgColorView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    UIColor *c = self.leftColorView.backgroundColor;
    [self.leftColorView setBackgroundColor:c];
    
    
    if (self.coloredBg) {
        self.backgroundColor = [UIColor fromRGB:0x364048];
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    UIColor *c = self.leftColorView.backgroundColor;
    [self.leftColorView setBackgroundColor:c];
    
    if (self.coloredBg) {
        self.backgroundColor = [UIColor fromRGB:0x364048];
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
}

@end












