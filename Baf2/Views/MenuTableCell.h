//
//  MenuTableCell.h
//  Baf2
//
//  Created by nmaksut on 04.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *leftColorView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *innerView;
@property (assign, nonatomic) BOOL coloredBg;

@end
