//
// Created by Almas Adilbek on 8/22/14.
//

#import <Foundation/Foundation.h>

@protocol ConnectAccountsMessageBoxViewDelegate;

@interface ConnectAccountsMessageBoxView : UIView

@property(nonatomic, weak) id <ConnectAccountsMessageBoxViewDelegate> delegate;

@end


@protocol ConnectAccountsMessageBoxViewDelegate<NSObject>
-(void)connectAccountsMessageBoxViewConnectTapped;
@end