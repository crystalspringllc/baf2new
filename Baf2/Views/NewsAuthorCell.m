//
//  NewsAuthorCell.m
//  Baf2
//
//  Created by nmaksut on 17.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NewsAuthorCell.h"
#import "User.h"


@implementation NewsAuthorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.likesButton setImage:[UIImage imageNamed:@"icon-like-empty"] forState:UIControlStateNormal];
    [self.likesButton setImage:[UIImage imageNamed:@"icon-like-filled"] forState:UIControlStateSelected];
    
    [self.likesButton addTarget:self action:@selector(likeTap) forControlEvents:UIControlEventTouchUpInside];
    [self.shareButton addTarget:self action:@selector(shareTap) forControlEvents:UIControlEventTouchUpInside];
    
    if ([User sharedInstance].loggedIn) {
        self.likesButton.enabled = YES;
    } else {
        self.likesButton.enabled = NO;
    }
}

- (void)setNews:(News *)news {
    self.avatarImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:news.src.avaUrl] placeholderImage:[UIImage imageNamed:@"icon_profile_placeholder"]];
    
    self.usernameLabel.text = news.src.nick;
    self.commentsLabel.text = [NSString stringWithFormat:@"Комментарии(%lu)", (unsigned long)news.comments.count];
    [self.likesButton setTitle:[NSString stringWithFormat:@"%@ ", news.likes.stringValue] forState:UIControlStateNormal];
    self.likesButton.selected = news.like;
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    self.commentsLabel.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" Комментарии(%lu)", (unsigned long)news.comments.count] attributes:underlineAttribute];
    self.commentsLabel.textColor = [UIColor fromRGB:0x005780];
    self.dateLabel.text = news.formattedDateString;
    
}

- (void)likeTap {
    if (_likeBlock) self.likeBlock();
}

- (void)shareTap {
    if (_shareBlock) self.shareBlock();
}

@end
