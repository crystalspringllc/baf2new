//
//  NoVideoMessageView.h
//  Kiwi.kz
//
//  Created by Almas Adilbek on 11/3/12.
//
//

#import <UIKit/UIKit.h>

@interface NoResultMessageView : UIView {
    UIImageView *iconView;
}

@property (nonatomic, strong) UIImageView *iconView;

-(void)setText:(NSString *)text;

@end
