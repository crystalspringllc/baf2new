//
//  BankOfferTableViewCell.h
//  Baf2
//
//  Created by Shyngys Kassymov on 19.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankOfferTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *detailColorView;
@property (weak, nonatomic) IBOutlet UIImageView *detailImageView;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;

- (void)setTitle:(NSString *)title description:(NSString *)description;

@end
