//
//  LastOperationCell.h
//  Baf2
//
//  Created by Shyngys Kassymov on 22.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OperationHistory;

@interface LastOperationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UILabel *dateStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *mainTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

- (void)setOperationHistory:(OperationHistory *)operationHistory;

@end
