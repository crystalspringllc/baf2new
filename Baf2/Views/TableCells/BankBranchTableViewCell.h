//
//  BankBranchTableViewCell.h
//  BAF
//
//  Created by Shyngys Kassymov on 24.07.15.
//
//

#import <UIKit/UIKit.h>

@interface BankBranchTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *branchTitleLabel, *branchAddressLabel, *branchPhoneLabel, *branchWorkTimeLabel, *branchEmailAddressLabel, *branchDistanceLabel;

@end
