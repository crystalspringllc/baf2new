//
//  SearchResultIconTitleTableCell.h
//  Myth
//
//  Created by Almas Adilbek on 11/6/12.
//
//

#import <UIKit/UIKit.h>

@interface SearchResultIconTitleTableCell : UITableViewCell {
    UILabel *titleLabel;
    UIImageView *iconView;
}

@property (nonatomic, strong) UILabel *titleLabel;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

-(void)setIcon:(NSString *)iconUrl;
-(void)setTitle:(NSString *)title;

+(float)height:(NSString *)title;

@end
