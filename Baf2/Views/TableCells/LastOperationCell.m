//
//  LastOperationCell.m
//  Baf2
//
//  Created by Shyngys Kassymov on 22.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "LastOperationCell.h"
#import "OperationHistory.h"
#import <Chameleon.h>

@implementation LastOperationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.dateStatusLabel.textColor = [UIColor darkGrayColor];
    self.mainTextLabel.textColor = [UIColor appBlueColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    UIColor *color = self.detailView.backgroundColor;
    [super setSelected:selected animated:animated];

    self.detailView.backgroundColor = color;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    UIColor *color = self.detailView.backgroundColor;
    [super setHighlighted:highlighted animated:animated];
    
    self.detailView.backgroundColor = color;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //self.mainTextLabel.preferredMaxLayoutWidth = self.mainTextLabel.frame.size.width;
}

#pragma mark - Methods

- (void)setOperationHistory:(OperationHistory *)operationHistory {
    self.detailView.backgroundColor = [self colorForStatus:[OperationHistory statusFromString:operationHistory.status]];
    
    self.dateStatusLabel.text = [NSString stringWithFormat:@"%@ (%@)", [operationHistory dateNice], operationHistory.prettyStatus];
    self.mainTextLabel.text = operationHistory.text;

    NSMutableAttributedString *amountText = [[NSMutableAttributedString alloc]
                                             initWithString:[NSString stringWithFormat:@"%@ %@ ", [operationHistory.amount decimalFormatString], operationHistory.currency]
                                             attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14], NSForegroundColorAttributeName: [UIColor blackColor]}];
    double commission = operationHistory.amountWithCommission.doubleValue - operationHistory.amount.doubleValue;
    NSAttributedString *commissionText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"( %@ %@ %@ )", NSLocalizedString(@"commission", nil), [@(commission) decimalFormatString], operationHistory.commissionCurrency] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12], NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    [amountText appendAttributedString:commissionText];


    self.amountLabel.attributedText = amountText;
}

#pragma mark - Helpers

- (UIColor *)colorForStatus:(StatusType)status {
    switch (status) {
        case StatusTypeError:
            return [UIColor flatOrangeColor];
        case StatusTypeComplete:
            return [UIColor flatGreenColor];
        case StatusTypeInProgress:
            return [UIColor flatGrayColor];
        default:
            break;
    }
    
    return [UIColor flatGreenColor];
}

@end
