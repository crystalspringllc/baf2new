//
//  BankBranchTableViewCell.m
//  BAF
//
//  Created by Shyngys Kassymov on 24.07.15.
//
//

#import "BankBranchTableViewCell.h"

#define BB_CELL_MARGIN 15

@implementation BankBranchTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        CGFloat cellWidth = [ASize screenWidth];

        // title
        self.branchTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(BB_CELL_MARGIN, 7, cellWidth - 2 * BB_CELL_MARGIN, 20)];
        self.branchTitleLabel.numberOfLines = 0;
        self.branchTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.branchTitleLabel.backgroundColor = [UIColor clearColor];
        self.branchTitleLabel.font = [UIFont helveticaNeue:14];
        self.branchTitleLabel.textColor = [UIColor blackColor];
        [self addSubview:self.branchTitleLabel];
        
        // address
        self.branchAddressLabel = [[UILabel alloc] initWithFrame:CGRectMake(BB_CELL_MARGIN, self.branchTitleLabel.bottom + 7, cellWidth - 2 * BB_CELL_MARGIN, 20)];
        self.branchAddressLabel.numberOfLines = 0;
        self.branchAddressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.branchAddressLabel.backgroundColor = [UIColor clearColor];
        self.branchAddressLabel.font = [UIFont helveticaNeue:14];
        self.branchAddressLabel.textColor = [UIColor darkGrayColor];
        [self addSubview:self.branchAddressLabel];
        
        // phone
        self.branchPhoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(BB_CELL_MARGIN, self.branchAddressLabel.bottom + BB_CELL_MARGIN/3.0, cellWidth - 2 * BB_CELL_MARGIN, 20)];
        self.branchPhoneLabel.numberOfLines = 0;
        self.branchPhoneLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.branchPhoneLabel.backgroundColor = [UIColor clearColor];
        self.branchPhoneLabel.font = [UIFont helveticaNeue:14];
        self.branchPhoneLabel.textColor = [UIColor darkGrayColor];
        [self addSubview:self.branchPhoneLabel];
        
        // work time
        self.branchWorkTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(BB_CELL_MARGIN, self.branchPhoneLabel.bottom + BB_CELL_MARGIN/3.0, cellWidth - 2 * BB_CELL_MARGIN, 20)];
        self.branchWorkTimeLabel.numberOfLines = 0;
        self.branchWorkTimeLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.branchWorkTimeLabel.backgroundColor = [UIColor clearColor];
        self.branchWorkTimeLabel.font = [UIFont helveticaNeue:14];
        self.branchWorkTimeLabel.textColor = [UIColor darkGrayColor];
        [self addSubview:self.branchWorkTimeLabel];
        
        // email address
        self.branchEmailAddressLabel = [[UILabel alloc] initWithFrame:CGRectMake(BB_CELL_MARGIN, self.branchWorkTimeLabel.bottom + BB_CELL_MARGIN/3.0, cellWidth - 2 * BB_CELL_MARGIN, 20)];
        self.branchEmailAddressLabel.numberOfLines = 0;
        self.branchEmailAddressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.branchEmailAddressLabel.backgroundColor = [UIColor clearColor];
        self.branchEmailAddressLabel.font = [UIFont helveticaNeue:14];
        self.branchEmailAddressLabel.textColor = [UIColor darkGrayColor];
        [self addSubview:self.branchEmailAddressLabel];
        
        // distance
        self.branchDistanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(BB_CELL_MARGIN, self.branchEmailAddressLabel.bottom + BB_CELL_MARGIN/3.0, cellWidth - 2 * BB_CELL_MARGIN, 20)];
        self.branchDistanceLabel.numberOfLines = 0;
        self.branchDistanceLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.branchDistanceLabel.backgroundColor = [UIColor clearColor];
        self.branchDistanceLabel.font = [UIFont helveticaNeue:14];
        self.branchDistanceLabel.textColor = [UIColor darkGrayColor];
        self.branchDistanceLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:self.branchDistanceLabel];
    }
    
    return self;
}

@end
