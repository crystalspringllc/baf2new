//
//  AASimpleTableViewCell.h
//  BAF
//
//  Created by Almas Adilbek on 2/27/15.
//
//



@interface AASimpleTableViewCell : UITableViewCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

- (void)setTitle:(NSString *)title;
- (void)setSubtitle:(NSString *)subtitle;

- (void)setTitleTextColor:(UIColor *)color;
- (void)setSubtitleTextColor:(UIColor *)color;

+ (CGFloat)cellHeightWithTitle:(NSString *)title;
+ (CGFloat)cellHeightWithTitle:(NSString *)title subtitle:(NSString *)subtitle;

@end