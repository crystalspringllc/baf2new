//
//  BankDetailTableViewCell.h
//  Baf2
//
//  Created by Shyngys Kassymov on 13.07.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *beneficiaryBankLabel;
@property (weak, nonatomic) IBOutlet UITextView *beneficiaryBankTextView;
@property (weak, nonatomic) IBOutlet UILabel *correspondentBankLabel;
@property (weak, nonatomic) IBOutlet UITextView *correspondentBankTextView;

@end
