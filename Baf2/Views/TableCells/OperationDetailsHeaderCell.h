//
//  OperationDetailsHeaderCell.h
//  BAF
//
//  Created by Almas Adilbek on 1/23/15.
//
//



@interface OperationDetailsHeaderCell : UITableViewCell

@property(nonatomic, assign) CGFloat contentPadding;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;
- (void)setTitle:(NSString *)title subtitle:(NSString *)subtitle;

+ (CGFloat)cellHeightWithTitle:(NSString *)titleText subtitle:(NSString *)subtitleText;

@end
