//
//  TransfersFormDisclaimerCell.m
//  Baf2
//
//  Created by Shyngys Kassymov on 31.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "TransfersFormDisclaimerCell.h"

@implementation TransfersFormDisclaimerCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if ([self.mainTextLabel isKindOfClass:[TTTAttributedLabel class]]) {
        self.mainTextLabel.delegate = self;
    }
    self.mainTextLabel.preferredMaxLayoutWidth = self.mainTextLabel.frame.size.width;
    self.leftDescriptionTextLabel.preferredMaxLayoutWidth = self.leftDescriptionTextLabel.frame.size.width;
}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    if ([url.absoluteString containsString:@"tel:"]) {
        NSString *phoneNumber = [url.absoluteString substringFromIndex:4];
        [UIHelper callPhone:phoneNumber showSheetInView:((AppDelegate *)[UIApplication sharedApplication].delegate).window];
    }
    
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithPhoneNumber:(NSString *)phoneNumber {
    [UIHelper callPhone:phoneNumber showSheetInView:((AppDelegate *)[UIApplication sharedApplication].delegate).window];
}

#pragma mark - Methods

- (void)setHTML:(NSString *)html {
    if ([html containsString:@"<"]) {
        NSError *err = nil;
        NSMutableAttributedString *string =  [[NSMutableAttributedString alloc] initWithData:[html dataUsingEncoding:NSUnicodeStringEncoding]
                                                                              options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                                                   documentAttributes:nil
                                                                                error:&err];
        CTFontRef systemFont = CTFontCreateWithName((__bridge CFStringRef)[UIFont systemFontOfSize:14].fontName, [UIFont systemFontOfSize:14].pointSize, NULL);
        if (systemFont) {
            [string addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)systemFont range:NSMakeRange(0, string.length)];
            CFRelease(systemFont);

        }
        [self.mainTextLabel setText:string afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
            [mutableAttributedString addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[UIColor fromRGB:0x4c4c4c].CGColor range:NSMakeRange(0, string.length)];

            return mutableAttributedString;
        }];

        if (err) {
            NSLog(@"Unable to parse label text: %@", err);
        }
    } else {
        self.mainTextLabel.text = html;
    }
}

@end
