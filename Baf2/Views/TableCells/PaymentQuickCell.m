//
//  PaymentQuickCell.m
//  Baf2
//
//  Created by Askar Mustafin on 4/12/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "PaymentQuickCell.h"
#import "Contract.h"
#import "ContractPaymentQuickCell.h"

@interface PaymentQuickCell () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@end

@implementation PaymentQuickCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.textFieldValues = [NSMutableDictionary new];
}

- (void)setContracts:(NSArray *)contracts {
    _contracts = contracts;
    
    [self.collectionView reloadData];
}

- (void)setTextFieldValues:(NSMutableDictionary *)textFieldValues {
    _textFieldValues = textFieldValues;
    
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.contracts.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ContractPaymentQuickCell";
    
    ContractPaymentQuickCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Contract *contract = self.contracts[(NSUInteger) indexPath.item];

    if ([contract.provider.infoRequestType intValue] == 2) {
        cell.paymentAmountField.hidden = YES;
        cell.fieldContainerView.layer.borderColor = [UIColor clearColor].CGColor;
    } else {
        cell.paymentAmountField.hidden = NO;
        cell.fieldContainerView.layer.borderColor = cell.nextButton.backgroundColor.CGColor;
    }


    cell.contract = contract;
    cell.paymentAmountField.text = nil;
    for (int i = 0; i < self.textFieldValues.allKeys.count; i++) {
        NSString *key = self.textFieldValues.allKeys[(NSUInteger) i];
        if ([key hasSuffix:[NSString stringWithFormat:@"%ld%ld", (long)indexPath.section, (long)indexPath.item]]) {
            cell.paymentAmountField.text = self.textFieldValues[key];
            break;
        }
    }
    __weak PaymentQuickCell *weakSelf = self;
    cell.onTextFieldEndEditing = ^(UITextField *textField) {
        if (weakSelf.initialAmountChanged) {
            weakSelf.initialAmountChanged(textField, indexPath);
        }
    };


    cell.onNextTapped = ^(Contract *_contract, NSNumber *inputAmount) {
        if (weakSelf.payButtonTapped) {
            weakSelf.payButtonTapped(_contract, indexPath, inputAmount);
        }
    };
    
    return cell;
}

- (void)dealloc {
    NSLog(@"PaymentQuickCell Dealloc");
}

@end
