//
//  WholeTextFieldTableViewCell.h
//  Baf2
//
//  Created by Shyngys Kassymov on 26.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JVFloatLabeledTextField;

@interface WholeTextFieldTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *textField;

@end
