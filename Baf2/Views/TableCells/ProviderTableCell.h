//
//  ProviderTableCell.h
//  Myth
//
//  Created by Almas Adilbek on 11/6/12.
//
//

#import <UIKit/UIKit.h>

@interface ProviderTableCell : UITableViewCell {
    UILabel *titleLabel;
}

@property (nonatomic, strong) UILabel *titleLabel;

-(void)setIcon:(NSString *)iconUrl;
-(void)setTitle:(NSString *)title;

+(float)height:(NSString *)title;

@end
