//
//  OperationDetailsHeaderCell.m
//  BAF
//
//  Created by Almas Adilbek on 1/23/15.
//
//

#import "OperationDetailsHeaderCell.h"

#define kPaddingTop 10
#define kTitleSubtitleSpacing 5

#define kTitleLabelFont [UIFont boldSystemFontOfSize:16]
#define kSubTitleLabelFont [UIFont systemFontOfSize:14]

static CGFloat _contentPaddingStatic = 15;

@interface OperationDetailsHeaderCell ()
@property(nonatomic, strong) UILabel *titleLabel;
@property(nonatomic, strong) UILabel *subtitleLabel;
@end

@implementation OperationDetailsHeaderCell {
}

-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if(self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        _contentPadding = _contentPaddingStatic;

        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_contentPaddingStatic, kPaddingTop, [[self class] contentWidth], 1)];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = kTitleLabelFont;
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _titleLabel.numberOfLines = 0;
        [self.contentView addSubview:_titleLabel];

        self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_contentPaddingStatic, 0, [[self class] contentWidth], 1)];
        _subtitleLabel.backgroundColor = [UIColor clearColor];
        _subtitleLabel.font = kSubTitleLabelFont;
        _subtitleLabel.textColor = [UIColor blackColor];
        _subtitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _subtitleLabel.numberOfLines = 0;
        [self.contentView addSubview:_subtitleLabel];
    }
    return self;
}

#pragma mark -

- (void)setTitle:(NSString *)title subtitle:(NSString *)subtitle
{
    _titleLabel.text = title;
    _subtitleLabel.text = subtitle;

    [_titleLabel sizeToFit];
    [_subtitleLabel sizeToFit];

    _subtitleLabel.y = _titleLabel.bottom + kTitleSubtitleSpacing;
}

+ (CGFloat)cellHeightWithTitle:(NSString *)titleText subtitle:(NSString *)subtitleText
{
    NSDictionary *attributes1 = @{NSFontAttributeName:kTitleLabelFont};

    CGRect rect1 = [titleText boundingRectWithSize:CGSizeMake([OperationDetailsHeaderCell contentWidth], CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributes1
                                      context:nil];

    NSDictionary *attributes2 = @{NSFontAttributeName:kSubTitleLabelFont};

    CGRect rect2 = [subtitleText boundingRectWithSize:CGSizeMake([OperationDetailsHeaderCell contentWidth], CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributes2
                                      context:nil];

    return 2 * kPaddingTop + rect1.size.height + rect2.size.height + kTitleSubtitleSpacing;
}

+ (CGFloat)contentWidth {
    return [ASize screenWidth] - 2 * _contentPaddingStatic;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if(self.imageView.image) {
        self.imageView.frame = CGRectMake(10, 10, 40, 40);
        self.textLabel.x = self.detailTextLabel.x = self.imageView.right + 10;
    }
}

@end
