//
//  OnlineDepositProfitInfoCell.h
//  Baf2
//
//  Created by Askar Mustafin on 11/6/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnlineDepositProfitInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *termLabel;
@property (weak, nonatomic) IBOutlet UILabel *kztLabel;
@property (weak, nonatomic) IBOutlet UILabel *eurLabel;
@property (weak, nonatomic) IBOutlet UILabel *usdLabel;
@property (weak, nonatomic) IBOutlet UILabel *rubLabel;
@end
