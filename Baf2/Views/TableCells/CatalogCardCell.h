//
//  CatalogCardCell.h
//  BAF
//
//  Created by Almas Adilbek on 4/1/15.
//
//



@interface CatalogCardCell : UITableViewCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

- (void)setCardImageUrl:(NSString *)cardUrl title:(NSString *)title;

@end
