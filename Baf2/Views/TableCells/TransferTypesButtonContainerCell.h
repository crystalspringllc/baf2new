//
//  TransferTypesButtonContainerCell.h
//  Baf2
//
//  Created by Askar Mustafin on 9/15/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransferTypesButtonContainerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *amongAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *toThirdPartiesButton;
@property (weak, nonatomic) IBOutlet UIButton *amongBanksTransfer;
@property (weak, nonatomic) IBOutlet UIButton *fromCardToCardButton;
@property (weak, nonatomic) IBOutlet UIButton *fromExternalCardToAccountButton;

@property (nonatomic, copy) void (^onAmongAccountClick)();
@property (nonatomic, copy) void (^onThirdPartiesClick)();
@property (nonatomic, copy) void (^onAmongBanksTransferClick)();
@property (nonatomic, copy) void (^onFromCardToCardClick)();
@property (nonatomic, copy) void (^onFromExternalCardToAccountClick)();

@end
