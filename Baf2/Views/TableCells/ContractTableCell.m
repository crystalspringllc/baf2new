//
//  ContractTableCell.m
//  Myth
//
//  Created by Almas Adilbek on 1/21/13.
//
//

#import "ContractTableCell.h"
#import "UIImageView+AFNetworking.h"
#import "UITableViewCell+Ext.h"

#define kIconSize 30

@implementation ContractTableCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [self setupDefaultSelection];
        
        float y = (float) (([ContractTableCell cellHeight] - kIconSize) * 0.5);
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(y, y, kIconSize, kIconSize)];
        [self.contentView addSubview:imageView];
        
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(y + imageView.frame.size.width + 10, 9, 200, 20)];
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.font = [UIFont boldHelveticaNeue:14];
        nameLabel.textColor = [UIColor fromRGB:0x5C5C5C];
        [self.contentView addSubview:nameLabel];
        
        identifierLabel = [[UILabel alloc] initWithFrame:CGRectMake(nameLabel.frame.origin.x, nameLabel.bottom - 2, 200, 15)];
        identifierLabel.backgroundColor = [UIColor clearColor];
        identifierLabel.font = [UIFont helveticaNeue:11];
        identifierLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:identifierLabel];

//        UIView * line = [[UIView alloc] initWithFrame:CGRectMake(0, [ContractTableCell cellHeight] - 1, [ASize screenWidth], 1)];
//        [line setBackgroundColor:[UIColor separatorLineColor]];
//        [self.contentView addSubview:line];

        [self configUI];
    }
    return self;
}

+(CGFloat)cellHeight {
    return 50;
}

-(void)setData:(Contract *)contract
{
    [imageView setImageWithURL:[NSURL URLWithString:contract.provider.logo]];
    nameLabel.text = contract.alias;
    identifierLabel.text = [NSString stringWithFormat:@"%@, %@", contract.provider.providerCode, contract.identifier];
}

-(void)setEnabled:(BOOL)enabled {
    if(enabled) {
        nameLabel.textColor = [UIColor blackColor];
        [self setSelectionStyle:UITableViewCellSelectionStyleBlue];
        self.userInteractionEnabled = YES;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else {
        nameLabel.textColor = [UIColor grayColor];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        self.userInteractionEnabled = NO;
        self.accessoryType = UITableViewCellAccessoryNone;
    }
}

-(void)configUI {

}


@end
