//
//  IconMultilineTitleCell.m
//  BAF
//
//  Created by Almas Adilbek on 4/1/15.
//
//

#import "IconMultilineTitleCell.h"

#define textLabelFont [UIFont systemFontOfSize:15]
#define textLabelWithIconWidth [ASize screenWidth] - 70
#define textLabelWidth [ASize screenWidth] - 50

@implementation IconMultilineTitleCell

-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if(self) {

        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.textLabel.numberOfLines = 0;
        self.textLabel.width = textLabelWidth;
        self.textLabel.font = textLabelFont;
    }
    return self;
}

+ (CGFloat)cellHeightWithTitle:(NSString *)title hasIcon:(BOOL)hasIcon
{
    CGFloat w = textLabelWidth;
    if(hasIcon) {
        w = textLabelWithIconWidth;
    }

    NSDictionary *attributes = @{NSFontAttributeName:textLabelFont};

    CGRect rect = [title boundingRectWithSize:CGSizeMake(w, CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributes
                                      context:nil];

    return rect.size.height + 20;
}

- (void)setTextLabelFont:(UIFont *)font
{
    _textLabelFont = font;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if(self.imageView) {
        self.textLabel.width = textLabelWithIconWidth;
    }
}

@end
