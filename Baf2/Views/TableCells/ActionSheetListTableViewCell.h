//
//  ActionSheetListTableViewCell.h
//  Baf2
//
//  Created by Askar Mustafin on 5/27/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionSheetListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitle2Label;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoImageViewLeftConstraint, *logoImageViewWidthConstraint;

- (void)hideImageView;

@end
