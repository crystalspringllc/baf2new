//
//  BankDetailTableViewCell.m
//  Baf2
//
//  Created by Shyngys Kassymov on 13.07.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "BankDetailTableViewCell.h"

@implementation BankDetailTableViewCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.beneficiaryBankLabel.preferredMaxLayoutWidth = self.beneficiaryBankLabel.frame.size.width;
    self.correspondentBankLabel.preferredMaxLayoutWidth = self.correspondentBankLabel.frame.size.width;
}

@end
