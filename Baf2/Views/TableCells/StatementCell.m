//
//  StatementCell.m
//  Baf2
//
//  Created by Askar Mustafin on 4/11/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <ChameleonFramework/ChameleonMacros.h>
#import "StatementCell.h"
#import "Statement.h"
#import "NSDateFormatter+Ext.h"

@implementation StatementCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //self.descriptionLabel.preferredMaxLayoutWidth = self.descriptionLabel.frame.size.width;
    //self.titleLabel.preferredMaxLayoutWidth = self.titleLabel.frame.size.width;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    UIColor *color = self.leftLineView.backgroundColor;
    [super setSelected:selected animated:animated];
    
    self.leftLineView.backgroundColor = color;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    UIColor *color = self.leftLineView.backgroundColor;
    [super setHighlighted:highlighted animated:animated];
    
    self.leftLineView.backgroundColor = color;
}

#pragma mark - set statement

/**
 * Set Account statement
 */
- (void)setStatement:(Statement *)statement {
    //todo: refactor

    //Описание транзакции
    self.titleLabel.text = statement.descriptionText;

    //Дата транзакции
    NSDate *date = [[NSDateFormatter baf2DateFormatter] dateFromString:statement.date];
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"dd.MM.yyyy"];
    self.dateLabel.text = [df stringFromDate:date];
    self.descriptionLabel.text = statement.alias;

    if (statement.merchant && statement.merchant.length > 0) {
        [self.logImageView sd_setImageWithURL:[NSURL URLWithString:statement.merchant] placeholderImage:[UIImage imageNamed:@"stmt_placeholder"]];
    } else {
        [self.logImageView setImage:[UIImage imageNamed:@"stmt_placeholder"]];
    }

    //Сумма
    if (statement.accountAmount && [statement.accountAmount doubleValue] != 0) {
        //показываем ин валюту
        self.summLabel.text = [NSString stringWithFormat:@"%@ %@",
                        statement.accountAmount.decimalFormatString,
                        statement.accountCurrency];
    } else {
        self.summLabel.text = @"";
    }

    //Операция
    if (statement.accountCurrency && statement.currency && ![statement.accountCurrency isEqualToString:statement.currency]) {
        self.operationLabel.text = [NSString stringWithFormat:@"Операция: %@ %@", [statement.amount decimalFormatString], statement.currency];
    } else {
        self.operationLabel.text = @"";
    }

    //Cashback
    if (statement.cashback && [statement.cashback doubleValue] != 0) {
        self.cashbackLabel.text = [NSString stringWithFormat:@"Cashback: %@ %@", [statement.cashback decimalFormatString], statement.cashbackCurrency];
    } else {
        self.cashbackLabel.text = @"";
    }

    //Комиссия
    if (statement.comission && [statement.comission doubleValue] != 0) {
        self.commissionLabel.text = [NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"comission", nil), [statement.comission decimalFormatString], statement.comissionCurrency];//
    } else {
        self.commissionLabel.text = nil;
    }

    //Цвет

    self.cashbackLabel.textColor = [UIColor appDarkGreenColor];

    if (statement.isCredit) {
        self.summLabel.textColor = [UIColor appDarkGreenColor];
    } else {
        self.summLabel.textColor = [UIColor blackColor];
    }
    if ([statement.amount doubleValue] == 0) {
        self.leftLineView.backgroundColor = [UIColor flatGrayColor];
    } else {
        if (statement.isCredit) {
            self.leftLineView.backgroundColor = [UIColor flatGreenColor];
        } else {
            self.leftLineView.backgroundColor = [UIColor flatOrangeColor];
        }
    }
}

@end
