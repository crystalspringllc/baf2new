//
//  ProviderTableCell.m
//  Myth
//
//  Created by Almas Adilbek on 11/6/12.
//
//

#import "ProviderTableCell.h"
#import "UIImageView+AFNetworking.h"
#import "UITableViewCell+Ext.h"

#define kIconTag 10
#define kTitleWidth [ASize groupTableWidth] - 85
#define kTitleFont [UIFont helveticaNeue:14]

@implementation ProviderTableCell {

    UIImageView *iconView;
}

@synthesize titleLabel;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self setupDefaultSelection];

        iconView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 24, 24)];
        [self.contentView addSubview:iconView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.font = kTitleFont;
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:titleLabel];
    }
    return self;
}

-(void)setIcon:(NSString *)iconUrl {
    UIImage *placeholder = [UIImage imageNamed:@"icon-icon-placeholder"];
    [iconView setImageWithURL:[NSURL URLWithString:iconUrl] placeholderImage:placeholder];
}

-(void)setTitle:(NSString *)title
{
    titleLabel.frame = CGRectMake(iconView.right + 15, 12, kTitleWidth, 1);
    titleLabel.text = title;
    [titleLabel sizeToFit];
}

+(float)height:(NSString *)title {
    CGSize size = [title boundingRectWithSize:CGSizeMake(kTitleWidth, MAXFLOAT)
                                   options:NSStringDrawingUsesLineFragmentOrigin
                                attributes:@{
                                             NSFontAttributeName : kTitleFont
                                             }
                                   context:nil].size;
    
    CGSize labelSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
    return labelSize.height + 24;
}

@end
