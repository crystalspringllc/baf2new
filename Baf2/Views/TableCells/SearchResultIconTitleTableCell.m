//
//  SearchResultIconTitleTableCell.m
//  Myth
//
//  Created by Almas Adilbek on 11/6/12.
//
//

#import "SearchResultIconTitleTableCell.h"
#import "UIImageView+AFNetworking.h"
#import "UITableViewCell+Ext.h"
#import "UITableViewCell+Separator.h"

#define kTitleWidth [ASize groupTableWidth] - 100
#define kTitleFont [UIFont systemFontOfSize:15]

@implementation SearchResultIconTitleTableCell {
    
}

@synthesize titleLabel;


- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupDefaultSelection];
        
        self.backgroundColor = [UIColor whiteColor];
        
        iconView = [[UIImageView alloc] initWithFrame:CGRectMake(9, 8, 26, 26)];
        [self.contentView addSubview:iconView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.font = kTitleFont;
        titleLabel.textColor = [UIColor fromRGB:0x146888];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.highlightedTextColor = [UIColor fromRGB:0x22546D];
        [self.contentView addSubview:titleLabel];
    }
    return self;
}

-(void)setIcon:(NSString *)iconUrl {
    [iconView setImageWithURL:[NSURL URLWithString:iconUrl] placeholderImage:[UIImage imageNamed:@"icon-provider.png"]];
    //[iconView setImageWithURL:[NSURL URLWithString:iconUrl] placeholderImage:nil];
}

-(void)setTitle:(NSString *)title
{
    titleLabel.frame = CGRectMake(iconView.right + 11, 12, kTitleWidth, 1);
    titleLabel.text = title;
    [titleLabel sizeToFit];
    
    float h = [[self class] height:title];
    [self setupSeparatorWithCellHeight:h - 1];
    //[self setupSeparatorWithCellHeight:45];
}

+(float)height:(NSString *)title {
    CGSize size = [title boundingRectWithSize:CGSizeMake(kTitleWidth, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{
                                               NSFontAttributeName : kTitleFont
                                               }
                                     context:nil].size;

    CGSize labelSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
    if (labelSize.height + 20 < 40) {
        return 40;
    } else {
        return labelSize.height + 20;
    }
}

@end
