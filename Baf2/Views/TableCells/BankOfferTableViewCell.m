//
//  BankOfferTableViewCell.m
//  Baf2
//
//  Created by Shyngys Kassymov on 19.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "BankOfferTableViewCell.h"

@implementation BankOfferTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.detailImageView.tintColor = [UIColor fromRGB:0x444444];
    self.mainLabel.preferredMaxLayoutWidth = self.mainLabel.frame.size.width;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Methods

- (void)setTitle:(NSString *)title description:(NSString *)description {
    NSMutableAttributedString *text = [NSMutableAttributedString new];
    
    if (title && title.length > 0) {
        NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14], NSForegroundColorAttributeName: [UIColor blackColor]}];
        [text appendAttributedString:titleString];
    }
    if (description && description.length > 0) {
        NSAttributedString *descriptionString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", description] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12],NSForegroundColorAttributeName: [UIColor fromRGB:0x444444]}];
        [text appendAttributedString:descriptionString];
    }
    
    // set line spacing
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:8];
    [text addAttribute:NSParagraphStyleAttributeName
                 value:style
                 range:NSMakeRange(0, text.length)];
    
    self.mainLabel.attributedText = text;
}

@end
