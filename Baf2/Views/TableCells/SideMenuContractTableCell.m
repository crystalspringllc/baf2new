//
//  SideMenuContractTableCell.m
//  Myth
//
//  Created by Almas Adilbek on 2/4/13.
//
//

#import "SideMenuContractTableCell.h"
#import "UITableViewCell+Separator.h"

@implementation SideMenuContractTableCell

+(float)height {
    return 50;
}

-(void)configUI {
    self.backgroundColor = [UIColor whiteColor];
    
    CGRect f = imageView.frame;
    f.size.width = f.size.height = 24;
    imageView.frame = f;
    
    int x = 44;
    nameLabel.font = [UIFont systemFontOfSize:15];
    f = nameLabel.frame;
    f.origin.x = x;
    f.origin.y = 7;
    nameLabel.frame = f;
    nameLabel.textColor = [UIColor fromRGB:0x146888];
    
    identifierLabel.textColor = [UIColor grayColor];
    f = identifierLabel.frame;
    f.origin.x = x;
    identifierLabel.frame = f;
    
    [self setupSeparatorWithCellHeight:48];
}

@end
