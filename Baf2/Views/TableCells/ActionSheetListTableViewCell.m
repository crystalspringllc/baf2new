//
//  ActionSheetListTableViewCell.m
//  Baf2
//
//  Created by Askar Mustafin on 5/27/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ActionSheetListTableViewCell.h"

@implementation ActionSheetListTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Methods

- (void)hideImageView {
    self.logoImageViewLeftConstraint.constant = 0;
    self.logoImageViewWidthConstraint.constant = 0;
    
    [self.contentView layoutIfNeeded];
}

@end
