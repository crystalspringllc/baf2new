//
//  ContractTableCell.h
//  Myth
//
//  Created by Almas Adilbek on 1/21/13.
//
//

#import <UIKit/UIKit.h>
#import "Contract.h"

@interface ContractTableCell : UITableViewCell {
    UIImageView *imageView;
    UILabel *nameLabel;
    UILabel *identifierLabel;
}

+(CGFloat)cellHeight;

-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier;
-(void)setData:(Contract *)contract;
-(void)setEnabled:(BOOL)enabled;

-(void)configUI;

@end
