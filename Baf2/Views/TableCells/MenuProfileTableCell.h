//
//  MenuProfileTableCell.h
//  Baf2
//
//  Created by Askar Mustafin on 9/14/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuProfileTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *innerView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (assign, nonatomic) BOOL coloredBg;
@property (nonatomic, strong) UIActivityIndicatorView *loader;
@end
