//
//  PaymentQuickCell.h
//  Baf2
//
//  Created by Askar Mustafin on 4/12/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Contract;

@interface PaymentQuickCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *contracts;

@property (nonatomic, copy) void (^initialAmountChanged)(UITextField *textField, NSIndexPath *indexPath);
@property (nonatomic, copy) void (^payButtonTapped)(Contract *contract, NSIndexPath *indexPath, NSNumber *inputAmount);

@property (nonatomic, strong) NSMutableDictionary *textFieldValues;

@end