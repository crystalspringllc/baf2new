//
//  ConratctPaymentTableViewCell.m
//  Baf2
//
//  Created by developer on 05.01.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "ConratctPaymentTableViewCell.h"
#import "Contract.h"

@implementation ConratctPaymentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:NO];

    // Configure the view for the selected state
}

- (void)setContract:(Contract *)contract {
    
    _contract = contract;
    
    [self.contractImageView sd_setImageWithURL:[[NSURL alloc] initWithString:self.contract.provider.logo] placeholderImage:[UIImage imageNamed:@"img_placeholder.png"]];
    self.contractAliasLabel.text = self.contract.alias;
    self.contractNumber.text = self.contract.contract;
}

@end
