//
//  TransferTypesButtonContainerCell.m
//  Baf2
//
//  Created by Askar Mustafin on 9/15/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "TransferTypesButtonContainerCell.h"
#import "User.h"

@implementation TransferTypesButtonContainerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // config buttons
    self.amongAccountButton.layer.cornerRadius = self.amongAccountButton.width / 2;
    self.toThirdPartiesButton.layer.cornerRadius = self.amongAccountButton.width / 2;
    self.amongBanksTransfer.layer.cornerRadius = self.amongAccountButton.width / 2;
    self.fromCardToCardButton.layer.cornerRadius = self.amongAccountButton.width / 2;
    self.fromExternalCardToAccountButton.layer.cornerRadius = self.amongAccountButton.width / 2;
    self.amongAccountButton.layer.shadowColor = [UIColor fromRGB:0x444444].CGColor;
    self.amongAccountButton.layer.shadowOpacity = 0.3;
    self.amongAccountButton.layer.shadowOffset = CGSizeMake(0, 3);
    self.toThirdPartiesButton.layer.shadowColor = [UIColor fromRGB:0x444444].CGColor;
    self.toThirdPartiesButton.layer.shadowOpacity = 0.3;
    self.toThirdPartiesButton.layer.shadowOffset = CGSizeMake(0, 3);
    self.amongBanksTransfer.layer.shadowColor = [UIColor fromRGB:0x444444].CGColor;
    self.amongBanksTransfer.layer.shadowOpacity = 0.3;
    self.amongBanksTransfer.layer.shadowOffset = CGSizeMake(0, 3);
    self.fromCardToCardButton.layer.shadowColor = [UIColor fromRGB:0x444444].CGColor;
    self.fromCardToCardButton.layer.shadowOpacity = 0.3;
    self.fromCardToCardButton.layer.shadowOffset = CGSizeMake(0, 3);
    self.fromExternalCardToAccountButton.layer.shadowColor = [UIColor fromRGB:0x444444].CGColor;
    self.fromExternalCardToAccountButton.layer.shadowOpacity = 0.3;
    self.fromExternalCardToAccountButton.layer.shadowOffset = CGSizeMake(0, 3);

    int imageEdgeInset = 8;
    self.amongAccountButton.imageEdgeInsets = UIEdgeInsetsMake(imageEdgeInset, imageEdgeInset, imageEdgeInset, imageEdgeInset);
    self.toThirdPartiesButton.imageEdgeInsets = UIEdgeInsetsMake(imageEdgeInset, imageEdgeInset, imageEdgeInset, imageEdgeInset);
    self.amongBanksTransfer.imageEdgeInsets = UIEdgeInsetsMake(imageEdgeInset, imageEdgeInset, imageEdgeInset, imageEdgeInset);
    self.fromCardToCardButton.imageEdgeInsets = UIEdgeInsetsMake(imageEdgeInset, imageEdgeInset, imageEdgeInset, imageEdgeInset);
    self.fromExternalCardToAccountButton.imageEdgeInsets = UIEdgeInsetsMake(imageEdgeInset, imageEdgeInset, imageEdgeInset, imageEdgeInset);
}

#pragma mark - config actions

- (IBAction)clickAmongAccountsButton:(id)sender {
    if (self.onAmongAccountClick) {
        self.onAmongAccountClick();
    }
}
- (IBAction)clickToThirdPartiesButton:(id)sender {
    if (self.onThirdPartiesClick) {
        self.onThirdPartiesClick();
    }
}
- (IBAction)clickAmongBanksButton:(id)sender {
    if (self.onAmongBanksTransferClick) {
        self.onAmongBanksTransferClick();
    }
}
- (IBAction)clickFromCardToCardButton:(id)sender {
    if (self.onFromCardToCardClick) {
        self.onFromCardToCardClick();
    }
}

- (IBAction)clickFromExternalCardToAccount:(id)sender {
    if (self.onFromExternalCardToAccountClick) {
        self.onFromExternalCardToAccountClick();
    }
}

#pragma mark -

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
