//
//  DisclaimerTableViewCell.m
//  Baf2
//
//  Created by Shyngys Kassymov on 16.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "DisclaimerTableViewCell.h"
#import "MainButton.h"

@implementation DisclaimerTableViewCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.titleLabel.preferredMaxLayoutWidth = self.titleLabel.frame.size.width;
    
    self.titleLabel.text = NSLocalizedString(@"you_may_automatically", nil);
    
    self.leftButton.title = NSLocalizedString(@"search_accounts_by_iin", nil);
    self.rightButton.title = NSLocalizedString(@"card_registration", nil);
    self.thirdButton.title = NSLocalizedString(@"order_card", nil);
}

@end
