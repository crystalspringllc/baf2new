//
//  AviataStatementCell.h
//  Baf2
//
//  Created by Askar Mustafin on 9/30/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Statement;

#define AviaStatementCellIdentifier @"AviataStatementCell"

@interface AviataStatementCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *operationIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *aliasLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *aviaBonusKztLabel;

@property (nonatomic, strong) Statement *aviaStatement;

@property (nonatomic, copy) void (^onCompensateMileButtonClick)();

@end
