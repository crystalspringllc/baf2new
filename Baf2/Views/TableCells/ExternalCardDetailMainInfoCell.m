//
//  ExternalCardDetailMainInfoCell.m
//  Baf2
//
//  Created by Askar Mustafin on 6/15/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ExternalCardDetailMainInfoCell.h"

@implementation ExternalCardDetailMainInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
