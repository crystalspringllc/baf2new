//
//  KnpCell.m
//  Baf2
//
//  Created by Askar Mustafin on 6/22/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "KnpCell.h"
#import "KnpCatalogViewController.h"
#import "Knp.h"
#import "NSLayoutConstraint+ConcisePureLayout.h"
#import "UIView+ConcisePureLayout.h"
#import "UIButton+BackgroudForState.h"
#import "Account.h"

@interface KnpCell() {}
@property (nonatomic, strong) Knp *selectedKnp;
@end

@implementation KnpCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.knpTargetField.delegate = self;
    self.knpTextField.delegate = self;
    //self.knpTargetField.separatorHidden = NO;
    self.knpTextField.separatorHidden = NO;
    [self.chooseKnpButton setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.6] forState:UIControlStateHighlighted];

    [self makeShortHeight];
}

#pragma mark - config actions

- (IBAction)clickChooseKnpButton:(id)sender {
    if (!self.knps.count) {
        [Toast showToast:@"Выберите счет списания/заполнения и введите сумму"];
        return;
    }
    __weak KnpCell *wSelf = self;

    KnpCatalogViewController *vc = [[UIStoryboard storyboardWithName:@"Catalog" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([KnpCatalogViewController class])];
    vc.isLegal = YES;
    vc.preparedKnpList = self.knps;
    vc.didSelectKnpItem = ^(Knp *knp) {

        wSelf.knpChooseLabel.text = knp.name;
        wSelf.selectedKnp = knp;

        if ([knp.code isEqualToString:@"another"]) {

            [wSelf makeFullHeight];

            if (wSelf.onKnpSelect) {
                wSelf.onKnpSelect(wSelf.knpTextField.text, [NSString stringWithFormat:@"%@:%@",wSelf.knpTextField.text, wSelf.knpInfoLabel.text], wSelf.knpTargetField.text);
            }

        } else {

            if (wSelf.targetAccount && wSelf.sourceAccount) {

                if (wSelf.sourceAccount.accountType == AccountTypeCard) {
                    if ([wSelf.targetAccount.type isEqualToString:@"beneficiary"]) {
                        //jur
                        if (wSelf.targetAccount.isLegal == true) {
                            [wSelf makeHalfHeight];

                            if (wSelf.onKnpSelect) {
                                wSelf.onKnpSelect(knp.code, knp.name, wSelf.knpTargetField.text);
                            }
                        }
                            //fiz
                        else {
                            if ([knp.code isEqualToString:@"119"] || [knp.code isEqualToString:@"343"]) {
                                [wSelf makeShortHeight];

                                if (wSelf.onKnpSelect) {
                                    wSelf.onKnpSelect(knp.code, knp.name, @"");
                                }
                            }
                        }
                    }
                } else {
                    if ([wSelf.targetAccount.type isEqualToString:@"beneficiary"]) {
                        if (wSelf.targetAccount.isLegal == false) {
                            if ([knp.code isEqualToString:@"119"] || [knp.code isEqualToString:@"343"]) {
                                [wSelf makeHalfHeight];

                                if (wSelf.onKnpSelect) {
                                    wSelf.onKnpSelect(knp.code, knp.name, wSelf.knpTargetField.text);
                                }
                            }
                        } else {
                            [wSelf makeHalfHeight];

                            if (wSelf.onKnpSelect) {
                                wSelf.onKnpSelect(knp.code, knp.name, wSelf.knpTargetField.text);
                            }
                        }
                    }
                }
            }
        }

//        if (wSelf.onConstraintUpdate) {
//            [wSelf layoutSubviews];
//            wSelf.onConstraintUpdate();
//        }

    };

    [self.parentVC.navigationController pushViewController:vc animated:true];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

    if ([self.selectedKnp.code isEqualToString:@"another"]) {
        if (self.knpTextField.text.length == 3) {
            for (Knp *k in self.allKnpList) {
                if ([k.code isEqualToString:self.knpTextField.text]) {
                    self.knpInfoLabel.text = k.knpDescription;
                    break;
                }
            }
        }
        if (self.knpTextField.text.length > 0 &&
                self.knpInfoLabel.text.length > 0) {
            if (self.onKnpSelect) {
                self.onKnpSelect(self.knpTextField.text, [NSString stringWithFormat:@"%@:%@",self.knpTextField.text, self.knpInfoLabel.text], self.knpTargetField.text);
            }
        }
    } else {
        if (self.selectedKnp) {
            if (self.onKnpSelect) {
                if (self.knpTargetField.text.length > 0) {
                    self.onKnpSelect(self.selectedKnp.code, self.selectedKnp.name, self.knpTargetField.text);
                } else {
                    self.onKnpSelect(self.selectedKnp.code, self.selectedKnp.name, @"");
                }
            }
        }
    }
}

#define KNPCODEMAXLENGTH 3
#define KNPAPPOIMENTMAXLENGTH 50

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
        replacementString:(NSString *)string {

    if ([textField isEqual:self.knpTextField] ||
            [textField isEqual:self.knpTargetField]) {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;

        NSUInteger newLength = oldLength - rangeLength + replacementLength;

        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;

        if ([textField isEqual:self.knpTextField]) {
            return newLength <= KNPCODEMAXLENGTH || returnKey;
        } else if ([textField isEqual:self.knpTargetField]) {
            return newLength <= KNPAPPOIMENTMAXLENGTH || returnKey;
        }
    }
    return YES;
}

#pragma mark - make full/short height

- (void)makeFullHeight {
    __weak KnpCell *wSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wSelf.knpTargetFieldBottomConstraint autoRemove];
        [wSelf.chooseKnpViewBottomConstraint autoRemove];
        [wSelf.knpCodeBottomConstreint autoRemove];

        wSelf.knpTextField.hidden = NO;
        wSelf.knpInfoLabel.hidden = NO;
        wSelf.knpTargetField.hidden = NO;

        wSelf.chooseKnpViewBottomConstraint = [wSelf.knpChooseContainerView aa_pinAboveView:wSelf.knpTextField offset:4];
        wSelf.knpCodeBottomConstreint = [wSelf.knpTextField aa_pinAboveView:wSelf.knpTargetField offset:4];
        wSelf.knpTargetFieldBottomConstraint = [wSelf.knpTargetField aa_superviewBottom:5];
    });
}

- (void)makeHalfHeight {
    __weak KnpCell *wSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wSelf.knpTargetFieldBottomConstraint autoRemove];
        [wSelf.knpCodeBottomConstreint autoRemove];
        [wSelf.chooseKnpViewBottomConstraint autoRemove];

        wSelf.knpTextField.hidden = YES;
        wSelf.knpInfoLabel.hidden = YES;
        wSelf.knpTargetField.hidden = NO;

        wSelf.chooseKnpViewBottomConstraint = [wSelf.knpChooseContainerView aa_pinAboveView:wSelf.knpTargetField offset:4];
        wSelf.knpTargetFieldBottomConstraint = [wSelf.knpTargetField aa_superviewBottom:5];
    });
}

- (void)makeShortHeight {
    __weak KnpCell *wSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wSelf.knpTargetFieldBottomConstraint autoRemove];
        [wSelf.chooseKnpViewBottomConstraint autoRemove];
        [wSelf.knpCodeBottomConstreint autoRemove];

        wSelf.knpTextField.hidden = YES;
        wSelf.knpInfoLabel.hidden = YES;
        wSelf.knpTargetField.hidden = YES;

        wSelf.chooseKnpViewBottomConstraint = [wSelf.knpChooseContainerView aa_superviewBottom:5];
    });
}

- (void)resetFields {
    [self makeShortHeight];
    self.knpChooseLabel.text = @"Выберите код назначения платежа";
}

- (void)dealloc {
    NSLog(@"KNPCELL dealloc");
}

@end
