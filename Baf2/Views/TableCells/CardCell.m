//
//  CardCell.m
//  Baf2
//
//  Created by Askar Mustafin on 4/11/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//


#import "CardCell.h"
#import "M13BadgeView.h"
#import "Card.h"

@interface CardCell () {}

@property (nonatomic, strong) HorizontalPushView *horizontalPushView;

@end

@implementation CardCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];

    self.tagViewContainer.backgroundColor = [UIColor clearColor];

    self.horizontalPushView = [[HorizontalPushView alloc] init];
    self.horizontalPushView.width = 200;
    [self.tagViewContainer addSubview:self.horizontalPushView];
    
    self.hereWillBeTitle.text = NSLocalizedString(@"here_will_be_shown_your_cards", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark -

- (void)setCard:(Card *)card {
    _card = card;

    self.aliasLabel.text = self.card.alias;
    self.numberLabel.text = self.card.number ? : @"";
    self.balanceLabel.text = [self.card balanceWithCurrency];
    

    [self.cardLogoImageView sd_setImageWithURL:[[NSURL alloc] initWithString:card.logo] placeholderImage:[UIImage imageNamed:@"img_placeholder"]];

    [self.horizontalPushView removeAllSubviews];

    if (self.card.isIslamic) {
        UIView *badgeView = [self createTagViewWithTitle:@"ISLAMIC" color:[UIColor fromRGB:0xFFA500]];
        badgeView.tag = 5;
        [self.horizontalPushView pushView:badgeView];
    }

    if (self.card.isMulty) {
        UIView *badgeView = [self createTagViewWithTitle:@"MULTI" color:[UIColor fromRGB:0x4364E3]];
        badgeView.tag = 5;
        [self.horizontalPushView pushView:badgeView];
    }

    if (self.card.isAvia) {
        UIView *badgeView = [self createTagViewWithTitle:@"AVIA" color:[UIColor fromRGB:0x00b4ff]];
        badgeView.tag = 5;
        [self.horizontalPushView pushView:badgeView];
    }
    
    if (self.card.isVirtual) {
        UIView *badgeView = [self createTagViewWithTitle:@"VIRTUAL" color:[UIColor fromRGB:0x2073ab]];
        badgeView.tag = 5;
        [self.horizontalPushView pushView:badgeView];
    }
    
    if (self.card.isCredit) {
        UIView *badgeView = [self createTagViewWithTitle:@"CREDIT" color:[UIColor fromRGB:0x70bf54]];
        badgeView.tag = 5;
        [self.horizontalPushView pushView:badgeView];
    }
    
    if (self.card.isWarning) {
        UIView *badgeView = [self createTagViewWithTitle:@"BLOCK" color:[UIColor fromRGB:0xC0392B]];
        badgeView.tag = 5;
        [self.horizontalPushView pushView:badgeView];
    }

}

- (UIView *)createTagViewWithTitle:(NSString *)title color:(UIColor *)color {
    M13BadgeView *badgeView = [[M13BadgeView alloc] init];
    badgeView.text = title;
    badgeView.textColor = [UIColor whiteColor];
    badgeView.badgeBackgroundColor = color;
    badgeView.font = [UIFont boldSystemFontOfSize:10];
    badgeView.cornerRadius = 4;
    badgeView.height = 20;
    badgeView.animateChanges = NO;
    return badgeView;
}

@end
