//
//  CatalogCardCell.m
//  BAF
//
//  Created by Almas Adilbek on 4/1/15.
//
//

#import "CatalogCardCell.h"
#import <UIImageView+WebCache.h>

@implementation CatalogCardCell {
    UIImageView *cardView;
    UILabel *titleLabel;
}

-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if(self) {

        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        cardView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 119, 75)];
        [self.contentView addSubview:cardView];

        titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont boldHelveticaNeue:18];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.numberOfLines = 0;
        titleLabel.x = cardView.right + 20;
        titleLabel.y = cardView.y;
        [self.contentView addSubview:titleLabel];

    }
    return self;
}

- (void)setCardImageUrl:(NSString *)cardUrl title:(NSString *)title
{
    UIActivityIndicatorView *loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loader.hidesWhenStopped = YES;
    loader.center = cardView.center;
    [self.contentView addSubview:loader];
    [loader startAnimating];
    
    [cardView sd_setImageWithURL:[[NSURL alloc] initWithString:cardUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [loader stopAnimating];
        [loader removeFromSuperview];
    }];

    titleLabel.width = 130;
    titleLabel.text = title;
    [titleLabel sizeToFit];
}


@end
