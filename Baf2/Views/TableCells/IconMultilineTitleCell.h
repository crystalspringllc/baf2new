//
//  IconMultilineTitleCell.h
//  BAF
//
//  Created by Almas Adilbek on 4/1/15.
//
//



@interface IconMultilineTitleCell : UITableViewCell

@property(nonatomic, strong) UIFont *textLabelFont;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;
+ (CGFloat)cellHeightWithTitle:(NSString *)title hasIcon:(BOOL)hasIcon;

@end
