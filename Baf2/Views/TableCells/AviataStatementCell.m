//
//  AviataStatementCell.m
//  Baf2
//
//  Created by Askar Mustafin on 9/30/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "AviataStatementCell.h"
#import "Statement.h"
#import "MainButton.h"

@interface AviataStatementCell(){}
@property (weak, nonatomic) IBOutlet MainButton *compensateMilesButton;
@end

@implementation AviataStatementCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.compensateMilesButton setMainButtonStyle:MainButtonStyleOrange];
    self.compensateMilesButton.titleLabel.font = [UIFont boldSystemFontOfSize:10];
    [self.compensateMilesButton setTitle:NSLocalizedString(@"compensate_with_miles", nil)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - ib actions

- (IBAction)clickComponsateMiles:(id)sender {
    if (self.onCompensateMileButtonClick) {
        self.onCompensateMileButtonClick();
    }
}

#pragma mark -

- (void)setAviaStatement:(Statement *)statement {
    if (statement) {
        self.operationIdLabel.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"reference_transaction", nil), statement.operationId];
        self.aliasLabel.text = statement.alias;
        self.descriptionLabel.text = statement.descriptionText;
        self.amountLabel.text = [NSString stringWithFormat:@"%@ %@", [statement.amount decimalFormatString], statement.currency];
        self.aviaBonusKztLabel.text = [NSString stringWithFormat:@"%@ KZT", [statement.aviaBonusKzt decimalFormatString]];
        self.compensateMilesButton.hidden = [statement.aviaBonusKzt floatValue] <= 0;
    }
}

@end
