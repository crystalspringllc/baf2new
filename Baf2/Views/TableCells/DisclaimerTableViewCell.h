//
//  DisclaimerTableViewCell.h
//  Baf2
//
//  Created by Shyngys Kassymov on 16.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainButton;

@interface DisclaimerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet MainButton *leftButton;
@property (weak, nonatomic) IBOutlet MainButton *rightButton;
@property (weak, nonatomic) IBOutlet MainButton *thirdButton;

@end
