//
//  MenuProfileTableCell.m
//  Baf2
//
//  Created by Askar Mustafin on 9/14/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "MenuProfileTableCell.h"
#import "UIView+ConcisePureLayout.h"


@implementation MenuProfileTableCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.backgroundColor = [UIColor fromRGB:0x364048];
    self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    self.textLabel.textColor = [UIColor whiteColor];

    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor fromRGB:0x364048];
    [self setSelectedBackgroundView:bgColorView];

    if (!self.loader) {
        self.loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self addSubview:self.loader];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.loader aa_superviewLeft:(CGFloat) (((self.width - 70) - self.loader.width) * 0.5)];
    [self.loader aa_centerVertical];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    if (self.coloredBg) {
        self.backgroundColor = [UIColor fromRGB:0x364048];
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
}


- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    if (self.coloredBg) {
        self.backgroundColor = [UIColor fromRGB:0x364048];
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
}

@end
