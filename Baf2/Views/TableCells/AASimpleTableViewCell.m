//
//  AASimpleTableViewCell.m
//  BAF
//
//  Created by Almas Adilbek on 2/27/15.
//
//

#import "AASimpleTableViewCell.h"
#import "NSString+Ext.h"

#define kCellPadding 15
#define kCellTopPadding 8
#define kCellTitleSubtitleSpacing 3

#define kTitleFont [UIFont systemFontOfSize:13]
#define kSubtitleFont [UIFont boldSystemFontOfSize:14]

@implementation AASimpleTableViewCell {
    UILabel *titleLabel;
    UILabel *subtitleLabel;
}

-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if(self) {

        titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        titleLabel.x = kCellPadding;
        titleLabel.y = kCellTopPadding;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = kTitleFont;
        titleLabel.textColor = [UIColor grayColor];
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.numberOfLines = 0;
        [self addSubview:titleLabel];

        subtitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        subtitleLabel.x = kCellPadding;
        subtitleLabel.backgroundColor = [UIColor clearColor];
        subtitleLabel.font = kSubtitleFont;
        subtitleLabel.textColor = [UIColor blackColor];
        subtitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        subtitleLabel.numberOfLines = 0;
        [self addSubview:subtitleLabel];

    }
    return self;
}

- (void)setTitle:(NSString *)title
{
    titleLabel.width = [AASimpleTableViewCell contentWidth];
    titleLabel.text = title;
    [titleLabel sizeToFit];

    if(subtitleLabel.text.length > 0) {
        subtitleLabel.y = titleLabel.bottom + kCellTitleSubtitleSpacing;
    }
}

- (void)setSubtitle:(NSString *)subtitle
{
    subtitleLabel.width = [AASimpleTableViewCell contentWidth];
    subtitleLabel.text = subtitle;
    [subtitleLabel sizeToFit];

    if(titleLabel.text.length > 0) {
        subtitleLabel.y = titleLabel.bottom + kCellTitleSubtitleSpacing;
    } else {
        subtitleLabel.y = kCellTopPadding;
    }
}

- (void)setTitleTextColor:(UIColor *)color
{
    if(titleLabel) {
        titleLabel.textColor = color;
    }
}

- (void)setSubtitleTextColor:(UIColor *)color
{
    if(subtitleLabel) {
        subtitleLabel.textColor = color;
    }
}


+ (CGFloat)cellHeightWithTitle:(NSString *)title
{
    return [self cellHeightWithTitle:title subtitle:nil];
}

+ (CGFloat)cellHeightWithTitle:(NSString *)title subtitle:(NSString *)subtitle
{
    if (![title isKindOfClass:[NSString class]]) {
        title = [NSString stringWithFormat:@"%@", title];
    }

    if (![subtitle isKindOfClass:[NSString class]]) {
        subtitle = [NSString stringWithFormat:@"%@", subtitle];
    }

    NSDictionary *attributes = @{NSFontAttributeName:kTitleFont};
    CGRect rect = [title boundingRectWithSize:CGSizeMake([self contentWidth], CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributes
                                      context:nil];

    CGFloat h = rect.size.height + 2 * kCellTopPadding;

    if(subtitle && [subtitle trim].length > 0) {
        NSDictionary *attributes2 = @{NSFontAttributeName: kSubtitleFont};
        CGRect rect2 = [subtitle boundingRectWithSize:CGSizeMake([self contentWidth], CGFLOAT_MAX)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:attributes2
                                          context:nil];

        h = rect.size.height + kCellTitleSubtitleSpacing + rect2.size.height + 2 * kCellTopPadding;
    }

    return h;
}

+ (CGFloat)contentWidth {
    return [ASize screenWidth] - 2 * kCellPadding;
}

@end
