//
//  AtmTableViewCell.h
//  BAF
//
//  Created by Shyngys Kassymov on 24.07.15.
//
//

#import <UIKit/UIKit.h>

@interface AtmTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *atmTitleLabel, *atmAddressLabel, *atmDistanceLabel;

@end
