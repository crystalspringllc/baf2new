//
//  KnpCell.h
//  Baf2
//
//  Created by Askar Mustafin on 6/22/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FloatingTextField.h"

@class Account;

@interface KnpCell : UITableViewCell<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIView *knpChooseContainerView;

@property (nonatomic, weak) IBOutlet UILabel *knpChooseLabel;
@property (nonatomic, weak) IBOutlet UIButton *chooseKnpButton;


@property (nonatomic, weak) IBOutlet UILabel *knpInfoLabel;
@property (nonatomic, weak) IBOutlet FloatingTextField *knpTextField;
@property (nonatomic, weak) IBOutlet UITextField *knpTargetField;


@property (nonatomic, strong) NSArray *knps;
@property (nonatomic, strong) NSArray *allKnpList;
@property (nonatomic, weak) UIViewController *parentVC;

@property (nonatomic, copy) void (^onKnpSelect)(NSString *knpCode, NSString *knpName, NSString *clientDesc);
//@property (nonatomic, copy) void (^onConstraintUpdate)();

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *chooseKnpViewBottomConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *knpTargetFieldBottomConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *knpCodeBottomConstreint;

@property (nonatomic, strong) Account *sourceAccount;
@property (nonatomic, strong) Account *targetAccount;

- (void)resetFields;

@end
