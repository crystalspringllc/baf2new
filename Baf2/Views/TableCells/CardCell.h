//
//  CardCell.h
//  Baf2
//
//  Created by Askar Mustafin on 4/11/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HorizontalPushView.h"


@class M13BadgeView;
@class Card;

@interface CardCell : UITableViewCell

@property (nonatomic, strong) Card *card;

@property (weak, nonatomic) IBOutlet UILabel *aliasLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cardLogoImageView;
@property (weak, nonatomic) IBOutlet UIView *tagViewContainer;
@property (weak, nonatomic) IBOutlet UILabel *hereWillBeTitle;

@end
