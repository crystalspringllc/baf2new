//
//  ConratctPaymentTableViewCell.h
//  Baf2
//
//  Created by developer on 05.01.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"

@class Contract;

@interface ConratctPaymentTableViewCell : MGSwipeTableCell <MGSwipeTableCellDelegate>


@property (weak, nonatomic) IBOutlet UIImageView *contractImageView;
@property (weak, nonatomic) IBOutlet UILabel *contractAliasLabel;
@property (weak, nonatomic) IBOutlet UILabel *contractNumber;

@property (nonatomic, weak) Contract *contract;

@end
