//
//  AtmTableViewCell.m
//  BAF
//
//  Created by Shyngys Kassymov on 24.07.15.
//
//

#import "AtmTableViewCell.h"

#define A_CELL_MARGIN 15

@implementation AtmTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        CGFloat cellWidth = [ASize screenWidth];

        // title
        self.atmTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(A_CELL_MARGIN, 7, cellWidth - 2 * A_CELL_MARGIN, 20)];
        self.atmTitleLabel.numberOfLines = 0;
        self.atmTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.atmTitleLabel.backgroundColor = [UIColor clearColor];
        self.atmTitleLabel.font = [UIFont helveticaNeue:14];
        self.atmTitleLabel.textColor = [UIColor blackColor];
        [self addSubview:self.atmTitleLabel];
        
        // address
        self.atmAddressLabel = [[UILabel alloc] initWithFrame:CGRectMake(A_CELL_MARGIN, self.atmTitleLabel.bottom + 7, cellWidth - 2 * A_CELL_MARGIN, 20)];
        self.atmAddressLabel.numberOfLines = 0;
        self.atmAddressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.atmAddressLabel.backgroundColor = [UIColor clearColor];
        self.atmAddressLabel.font = [UIFont helveticaNeue:14];
        self.atmAddressLabel.textColor = [UIColor darkGrayColor];
        [self addSubview:self.atmAddressLabel];
        
        // distance
        self.atmDistanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(A_CELL_MARGIN, self.atmAddressLabel.bottom + A_CELL_MARGIN/3.0, cellWidth - 2 * A_CELL_MARGIN, 20)];
        self.atmDistanceLabel.numberOfLines = 0;
        self.atmDistanceLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.atmDistanceLabel.backgroundColor = [UIColor clearColor];
        self.atmDistanceLabel.font = [UIFont helveticaNeue:14];
        self.atmDistanceLabel.textColor = [UIColor darkGrayColor];
        self.atmDistanceLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:self.atmDistanceLabel];
    }
    
    return self;
}

@end
