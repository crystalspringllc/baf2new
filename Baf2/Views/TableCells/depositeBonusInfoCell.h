//
//  depositeBonusInfoCell.h
//  Baf2
//
//  Created by developer on 23.11.17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface depositeBonusInfoCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UILabel *rightInfoLabel;

@property(nonatomic, strong) IBOutlet UILabel *leftInfoLabel;
@end
