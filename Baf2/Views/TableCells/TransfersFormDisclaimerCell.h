//
//  TransfersFormDisclaimerCell.h
//  Baf2
//
//  Created by Shyngys Kassymov on 31.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel.h>

@interface TransfersFormDisclaimerCell : UITableViewCell <TTTAttributedLabelDelegate>

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *mainTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftDescriptionTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightDescriptionTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

- (void)setHTML:(NSString *)text;

@end
