//
//  ConfirmEmailAndPhoneNumberCell.h
//  Baf2
//
//  Created by Shyngys Kassymov on 08.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainButton;

@interface ConfirmEmailAndPhoneNumberCell : UITableViewCell

@property (weak, nonatomic) IBOutlet MainButton *confirmButton;
@property (nonatomic, copy) void (^didTapConfirmButton)();

@end
