//
//  TabView.m
//  BAF
//
//  Created by Almas Adilbek on 10/21/14.
//
//

#import "TabView.h"

@implementation TabView {
    UIImageView *iconView;
    NSString *tabIconName;
    UILabel *titleLabel;
}

- (id)initWithIcon:(NSString *)iconName title:(NSString *)title
{
    self = [super initWithFrame:CGRectMake(0, 0, [TabView tabWidth], 54)];
    if(self) {
        tabIconName = iconName;

        iconView = [[UIImageView alloc] initWithImage:[UIImage imageWithName:[NSString stringWithFormat:@"%@_normal", iconName]]];
        iconView.centerX = self.centerX;
        iconView.y = 8;
        [self addSubview:iconView];

        titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont helveticaNeue:9];
        titleLabel.textColor = [UIColor fromRGB:0x737C70];
        titleLabel.text = title;
        [titleLabel sizeToFit];
        titleLabel.centerX = self.centerX;
        titleLabel.y = self.height - titleLabel.height - 6;
        [self addSubview:titleLabel];

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(tapped) forControlEvents:UIControlEventTouchUpInside];
        button.frame = self.bounds;
        [self addSubview:button];
    }
    return self;
}

+ (CGFloat)tabWidth {
    return 64;
}

- (void)tapped {
    self.selected = !self.selected;
    [self.delegate tabView:self selectedTabWithTag:self.tag];
}

-(void)setSelected:(BOOL)selected {
    _selected = selected;
    iconView.image = [UIImage imageWithName:[NSString stringWithFormat:@"%@_%@", tabIconName, _selected?@"pressed":@"normal"]];
    titleLabel.textColor = [UIColor fromRGB:_selected?0x379C72:0x737C70];
}


@end
