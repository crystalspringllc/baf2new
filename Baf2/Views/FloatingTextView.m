//
//  FloatingTextView.m
//  Baf2
//
//  Created by nmaksut on 19.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "FloatingTextView.h"

@interface FloatingTextView() <UITextViewDelegate>

@end

@implementation FloatingTextView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self initView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    [self initVars];
}

- (void)initVars
{
    self.delegate = self;
    self.placeholderTextColor = [UIColor fromRGB:0xC2C6C2];
    self.tintColor = [UIColor fromRGB:0x138384];
}


@end
