//
//  ContactsBranchInfoView.h
//  BAF
//
//  Created by Almas Adilbek on 5/18/15.
//
//



#import "ContactsObjectInfoView.h"

@interface ContactsBranchInfoView : ContactsObjectInfoView

- (id)initWithWidth:(CGFloat)width;

- (void)setTel:(NSString *)tel;
- (void)setFax:(NSString *)tel;
- (void)setEmail:(NSString *)email;
- (void)setSchedule:(NSString *)text;

@end
