//
//  NewsCommentCell.h
//  Baf2
//
//  Created by nmaksut on 17.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Comment.h"
#import "NewsAuthorCell.h"

@interface NewsCommentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel     *usernameLabel;
@property (weak, nonatomic) IBOutlet UITextView  *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftOffset;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UIButton *likesButton;
@property (nonatomic, strong) ButtonActionBlock likeBlock;

@property (weak, nonatomic) IBOutlet UIButton *trashButton;
@property (nonatomic, copy) void (^didTapTrashButton)();
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trashButtonWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trashButtonLeftConstraint;


- (void)setComment:(Comment *)comment;
- (void)setTrashButtonHidden:(BOOL)hidden;

@end
