//
// Created by Shyngys Kassymov on 15.12.16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "RatesBannerView.h"
#import "TickerBannerView.h"

#define FLIP_TIME 0.475f
#define TIMER_INTERVAL 3.0f

@interface RatesBannerView ()

@property (nonatomic, strong) UIView *containerView;

@property (nonatomic, strong) NSTimer *flipTimer;
@property NSMutableArray<UIView *> *rateViews;

@property (nonatomic, strong) NSDate *pauseStart;
@property (nonatomic, strong) NSDate *previousFireDate;

@end

@implementation RatesBannerView {
    BOOL didSetup;
}

- (instancetype)init {
    self = [super init];

    if (self) {
        [self setup];
    }

    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];

    if (self) {
        [self setup];
    }

    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];

    if (self) {
        [self setup];
    }

    return self;
}

#pragma mark - Setup

- (void)setup {
    if (!didSetup) {

        // properties
        self.rateViews = [NSMutableArray new];

        // views
        self.containerView = [UIView newAutoLayoutView];
        self.containerView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.containerView];
        [self.containerView autoPinEdgesToSuperviewEdges];

        RateView *usdRateView = [self rateView];
        [usdRateView setRateType:RateTypeUSD buy:@"" sell:@""];

        RateView *eurRateView = [self rateView];
        eurRateView.hidden = true;
        [eurRateView setRateType:RateTypeEUR buy:@"" sell:@""];

        RateView *gbpRateView = [self rateView];
        gbpRateView.hidden = true;
        [gbpRateView setRateType:RateTypeGBP buy:@"" sell:@""];
        
        RateView *rubRateView = [self rateView];
        rubRateView.hidden = true;
        [rubRateView setRateType:RateTypeRUB buy:@"" sell:@""];


//        UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
//        gr.numberOfTapsRequired = 1;
//        gr.numberOfTouchesRequired = 1;
//        [self addGestureRecognizer:gr];

        UILongPressGestureRecognizer *gr1 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(taphold:)];
        [self addGestureRecognizer:gr1];

        didSetup = true;
    }
}

- (void)setTickerBannerView:(TickerBannerView *)tickerBannerView {
    _tickerBannerView = tickerBannerView;
    if (self.tickerBannerView) {
        self.tickerBannerView.translatesAutoresizingMaskIntoConstraints = false;
        self.tickerBannerView.hidden = true;
        [self.containerView addSubview:self.tickerBannerView];
        [self.tickerBannerView autoPinEdgesToSuperviewEdges];
        [self.rateViews addObject:self.tickerBannerView];
    }
}


#pragma mark - Actions

- (void)taphold:(UIGestureRecognizer *)gestureRecognizer {
    if (UIGestureRecognizerStateBegan == gestureRecognizer.state) {
        if (!self.pauseStart) {
            [self pauseTimer];
        }
    }
    if (UIGestureRecognizerStateEnded == gestureRecognizer.state) {
        [self resumeTimer];
        self.pauseStart = nil;
    }
}

#pragma mark - Helpers

- (RateView *)rateView {
    RateView *rateView = [RateView configuredView];
    rateView.translatesAutoresizingMaskIntoConstraints = false;
    [self.containerView addSubview:rateView];
    [rateView autoPinEdgesToSuperviewEdges];

    [self.rateViews addObject:rateView];

    return rateView;
}

- (void)flip {

    UIView *fromView = self.rateViews[0];
    UIView *toView = self.rateViews[1];

    __weak RatesBannerView *weakSelf = self;

    [UIView transitionWithView:self.containerView
                      duration:FLIP_TIME
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        fromView.hidden = true;
                        toView.hidden = false;
                    }
                    completion:^(BOOL finished) {
                        [weakSelf sendFirstToBack];
                    }];

}

- (void)sendFirstToBack {
    RateView *firstView = self.rateViews[0];
    [self.rateViews removeObjectAtIndex:0];
    [self.rateViews addObject:firstView];
}

- (void)startFlipTimer {
    [self.flipTimer invalidate];
    self.flipTimer = [NSTimer timerWithTimeInterval:TIMER_INTERVAL
                                             target:self
                                           selector:@selector(flip)
                                           userInfo:nil
                                            repeats:true];
    [[NSRunLoop mainRunLoop] addTimer:self.flipTimer forMode:NSRunLoopCommonModes];
}

- (void)pauseTimer {
    self.pauseStart = [NSDate dateWithTimeIntervalSinceNow:0];
    self.previousFireDate = [self.flipTimer fireDate];
    [self.flipTimer setFireDate:[NSDate distantFuture]];
}

- (void)resumeTimer {
    float pauseTime = (float) (-1 * [self.pauseStart timeIntervalSinceNow]);
    [self.flipTimer setFireDate:[self.previousFireDate initWithTimeInterval:pauseTime sinceDate:self.previousFireDate]];
}

#pragma mark - Methods

- (void)setRateType:(RateType)rateType buy:(NSString *)buy sell:(NSString *)sell {
    if (self.rateViews) {
        for (RateView *rateView in self.rateViews) {
            
            if([rateView isKindOfClass:[RateView class]] && rateView.rateType) {
                if (rateView.rateType == rateType) {
                    [rateView setRateType:rateType buy:buy sell:sell];
                    break;
                }
            }
        }
    }
}

- (void)applyLightTheme {
    self.backgroundColor = [UIColor clearColor];
    self.containerView.backgroundColor = [UIColor clearColor];

    for (RateView *rateView in self.rateViews) {
        rateView.backgroundColor = [UIColor clearColor];
        rateView.buyTitleLabel.textColor = [UIColor whiteColor];
        rateView.buyLabel.textColor = [UIColor whiteColor];
        rateView.currencymageView.tintColor = [UIColor whiteColor];
        rateView.sellTitleLabel.textColor = [UIColor whiteColor];
        rateView.sellLabel.textColor = [UIColor whiteColor];
    }
}

@end
