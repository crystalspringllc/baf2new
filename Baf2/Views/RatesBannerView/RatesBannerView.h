//
// Created by Shyngys Kassymov on 15.12.16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"

@class TickerBannerView;

@interface RatesBannerView : UIView

- (void)setRateType:(RateType)rateType buy:(NSString *)buy sell:(NSString *)sell;
- (void)startFlipTimer;

- (void)applyLightTheme;

@property (nonatomic, strong) TickerBannerView *tickerBannerView;

@end