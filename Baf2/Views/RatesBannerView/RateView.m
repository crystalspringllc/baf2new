//
// Created by Shyngys Kassymov on 15.12.16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "RateView.h"

@implementation RateView

+ (instancetype)configuredView {
    RateView *rateView = [[NSBundle mainBundle] loadNibNamed:@"RateView" owner:self options:nil].firstObject;
    [rateView setup];
    return rateView;
}

#pragma mark - Setup

- (void)setup {
    self.currencymageView.contentMode = UIViewContentModeScaleAspectFit;
    self.currencymageView.tintColor = [UIColor darkGrayColor];
}

#pragma mark - Helpers

- (UIImage *)currencyImage {
    NSString *format = @"icn_%@";
    NSString *currency = @"usd";

    if (self.rateType == RateTypeEUR) {
        currency = @"eur";
    } else if (self.rateType == RateTypeRUB) {
        currency = @"rub";
    } else if(self.rateType == RateTypeGBP) {
        currency = @"gbp";
    }

    return [UIImage imageNamed:[NSString stringWithFormat:format, currency]];
}

#pragma mark - Methods

- (void)setRateType:(RateType)rateType buy:(NSString *)buy sell:(NSString *)sell {
    _rateType = rateType;

    self.buyLabel.text = buy;
    self.currencymageView.image = [self currencyImage];
    self.sellLabel.text = sell;
}

+ (RateType)rateTypeForCurrency:(NSString *)currency {
    if ([[currency lowercaseString] isEqualToString:@"usd"]) {
        return RateTypeUSD;
    } else if ([[currency lowercaseString] isEqualToString:@"eur"]) {
        return RateTypeEUR;
    } else if([[currency lowercaseString] isEqualToString:@"gbp"]){
        return RateTypeGBP;
    }

    return RateTypeRUB;
}

@end
