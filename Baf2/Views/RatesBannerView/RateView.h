//
// Created by Shyngys Kassymov on 15.12.16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

#define RATE_VIEW_PREFERRED_SIZE CGSizeMake(200, 44)

typedef enum RateType: NSInteger {
    RateTypeUSD = 1,
    RateTypeEUR = 2,
    RateTypeRUB = 3,
    RateTypeGBP = 4
} RateType;

@interface RateView : UIView

@property (nonatomic, weak) IBOutlet UILabel *buyTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *buyLabel;
@property (nonatomic, weak) IBOutlet UIImageView *currencymageView;
@property (nonatomic, weak) IBOutlet UILabel *sellTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *sellLabel;

@property (nonatomic, readonly) RateType rateType;

+ (instancetype)configuredView;
- (void)setRateType:(RateType)rateType buy:(NSString *)buy sell:(NSString *)sell;

+ (RateType)rateTypeForCurrency:(NSString *)currency;

@end
