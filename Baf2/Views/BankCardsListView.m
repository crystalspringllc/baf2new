//
//  BankCardsListView.m
//  BAF
//
//  Created by Almas Adilbek on 10/2/14.
//
//

#import "BankCardsListView.h"
#import "ViewsListView.h"
#import "CardAccount.h"
#import "Card.h"

#define kPadding 10

@implementation BankCardsListView

- (id)initWithBankCardAccounts:(NSArray *)cardAccounts title:(NSString *)title {
    self = [super initWithFrame:CGRectMake(0, 0, [ASize screenWidth] - 16, 1)];
    if(self) {

        self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

        NSMutableArray * rowViews = [NSMutableArray array];

        NSMutableIndexSet *indexesWithoutLines = [NSMutableIndexSet new];
        NSMutableIndexSet *indexesWithShorLines = [NSMutableIndexSet new];
        NSMutableDictionary *leftOffsets = [NSMutableDictionary new];
        
        int i = 0;
        for (CardAccount *cardAccount in cardAccounts)
        {
            ProductRowView *rowView = [[ProductRowView alloc] initWithTitle:cardAccount.alias
                                                                   subtitle:[cardAccount getBalanceWithCurrency]
                                                                       icon:[UIImage imageNamed:@"icn_card_green"]
                                                             warningMessage:nil
                                                               showFavorite:YES];
            rowView.delegate = self;
            rowView.data = cardAccount;
            [rowView showDisclosure];
            [rowView hideFavorite];
            rowView.subtitleFont = [UIFont boldSystemFontOfSize:14];
            [rowViews addObject:rowView];

            if (cardAccount.isWarning) {
                [rowView addTagWithText:@"BLOCK" color:[UIColor fromRGB:0xC0392B]];
            }

            [indexesWithoutLines addIndex:i];
            
            i++;
            
            for (Card *card in cardAccount.cards)
            {
                ProductRowView *cardRowView = [[ProductRowView alloc] initWithTitle:card.alias subtitle:[card getBalanceWithCurrency] icon:card.logo warningMessage:nil showFavorite:true];
                [cardRowView setFavorited:card.isFavorite];
                cardRowView.delegate = self;
                cardRowView.data = card;
                [cardRowView showDisclosure];
                [cardRowView hideFavorite];
                cardRowView.subtitleFont = [UIFont boldSystemFontOfSize:14];
                [rowViews addObject:cardRowView];
                
                if (card.number.length > 0) {
                    [cardRowView addCardNumberWithText:card.number];
                }
                
                if (card.isIslamic) {
                    [cardRowView addTagWithText:@"ISLAMIC" color:[UIColor fromRGB:0xFFA500]];
                }
                if (card.isMulty) {
                    [cardRowView addTagWithText:@"MULTI" color:[UIColor fromRGB:0x4364E3]];
                }
                if (card.isAvia) {
                    [cardRowView addTagWithText:@"AVIA" color:[UIColor fromRGB:0x00b4ff]];
                }
                if (card.isVirtual) {
                    [cardRowView addTagWithText:@"VIRTUAL" color:[UIColor fromRGB:0x2073ab]];
                }
                if (card.isCredit) {
                    [cardRowView addTagWithText:@"CREDIT" color:[UIColor fromRGB:0x70bf54]];
                }
                if (card.isWarning) {
                    [cardRowView addTagWithText:@"BLOCK" color:[UIColor fromRGB:0xC0392B]];
                }

                if ([card isEqual:cardAccount.cards.lastObject]) {
                    [cardRowView drawLastChildLine];
                } else {
                    [cardRowView drawChildLine];
                    
                    [indexesWithShorLines addIndex:i];
                    leftOffsets[@(i)] = @(cardRowView.leftOffset);
                }
                
                i++;
            }
        }

        ViewsListView *listView = [ViewsListView new];
        listView.indexesWithoutLines = indexesWithoutLines;
        listView.indexesWithShortLines = indexesWithShorLines;
        listView.leftOffsetForShortLines = leftOffsets;
        listView = [listView viewsListWithViews:rowViews];
        [self addSubview:listView];

        self.height = listView.bottom;

    }
    return self;
}

- (void)productRowViewTapped:(ProductRowView *)rowView {
    [self.delegate bankCardsListViewCardTapped:rowView];
}

@end
