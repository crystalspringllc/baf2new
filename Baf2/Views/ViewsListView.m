//
//  ViewsListView.m
//  BAF
//
//  Created by Almas Adilbek on 8/14/14.
//
//

#import "ViewsListView.h"

@implementation ViewsListView

- (id)viewsListWithViews:(NSArray *)views {
    ViewsListView *listView = [[ViewsListView alloc] init];
    if(views.count > 0)
    {
        UIView *firstView = views[0];
        listView.width = firstView.width;
        CGFloat y = 0;

        int i = 0;
        for(UIView *view in views)
        {
            view.y = y;
            [listView addSubview:view];
            y = view.bottom;

            if (self.indexesWithoutLines && [self.indexesWithoutLines containsIndex:i]) {
                // nothing for now
            } else if (self.indexesWithShortLines && [self.indexesWithShortLines containsIndex:i]) {
                CGFloat leftOffset = [self.leftOffsetForShortLines[@(i)] floatValue];
                
                UIView * line = [[UIView alloc] initWithFrame:CGRectMake(leftOffset, y - 1, listView.width - leftOffset, 1)];
                [line setBackgroundColor:[UIColor fromRGB:0xF2F2F2]];
                [listView addSubview:line];
            } else {
                UIView * line = [[UIView alloc] initWithFrame:CGRectMake(0, y - 1, listView.width, 1)];
                [line setBackgroundColor:[UIColor fromRGB:0xF2F2F2]];
                [listView addSubview:line];
            }
            
            i++;
        }

        listView.height = y;
    }
    return listView;
}

@end
