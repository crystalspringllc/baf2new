//
//  ProfilePasswordTableViewCell.h
//  Baf2
//
//  Created by Shyngys Kassymov on 20.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JVFloatLabeledTextField;

@interface ProfilePasswordTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet JVFloatLabeledTextField *passwordTextField;
@property (nonatomic, weak) IBOutlet JVFloatLabeledTextField *repeatPasswordTextField;

@property (nonatomic, copy) void (^didChangeText)(JVFloatLabeledTextField *field);

@end
