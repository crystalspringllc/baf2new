//
//  AstanaView.h
//  Baf2
//
//  Created by Shyngys Kassymov on 30.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AstanaView : UIView

- (void)animateView;

@end
