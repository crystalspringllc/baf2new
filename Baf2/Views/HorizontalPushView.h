//
// Created by Askar Mustafin on 3/31/16.
// Copyright (c) 2016 Банк Астаны. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface HorizontalPushView : UIView

@property (nonatomic, assign) CGFloat leftIndent;
@property (nonatomic, assign) CGFloat topIndent;

- (void)pushView:(UIView *)view;
- (void)removeAllSubviews;

@end