//
//  ChoosePeriodControlView.h
//  Baf2
//
//  Created by Askar Mustafin on 4/11/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoosePeriodControlView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *choosenPeriodLabel;
@property (weak, nonatomic) IBOutlet UIButton *choosePeriodButton;
@property (nonatomic, copy) void (^onChoosePeriodButtonClick)();

@end
