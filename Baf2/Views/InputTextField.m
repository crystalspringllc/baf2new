//
// Created by Mustafin Askar on 9/7/14.
// Copyright (c) 2014 Almas Adilbek. All rights reserved.
//

#import "InputTextField.h"
#import "NSString+Ext.h"
#import "UIView+AAPureLayout.h"
#import "UIView+ConcisePureLayout.h"
#import "NSLayoutConstraint+ConcisePureLayout.h"
#import "OCMaskedTextFieldView.h"

#define kTfPadding 10

@interface InputTextField() {}

@property (nonatomic, strong) NSLayoutConstraint *textFieldLeftConstraint;
@property (nonatomic, strong) NSLayoutConstraint *textFieldRightConstraint;
@property (nonatomic, strong) UIImageView *iconView;
@property (nonatomic, strong) UILabel *textLabel;

@end

@implementation InputTextField {
    UIActivityIndicatorView *loader;
    InputTextFieldOnValueChangedBlock _onValueChangedBlock;
    InputTextFieldOnValueChangeBlock _onValueChangeBlock;
    NSString *previousValue;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setup];
}

- (id)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup {
    self.maxCharacters = NSIntegerMax;

    self.textField = [[UITextField alloc] init];
    self.textField.translatesAutoresizingMaskIntoConstraints = false;
    self.textField.delegate = self;
    self.textField.font = [UIFont helveticaNeue:16];
    self.textField.textColor = [UIColor blackColor];
    self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.textField];
    [self.textField aa_superviewTop:0];
    self.textFieldLeftConstraint = [self.textField aa_superviewLeft:10];
    self.textFieldRightConstraint = [self.textField aa_superviewRight:10];
    [self.textField aa_superviewBottom:0];
    
    self.line = [[UIView alloc] init];
    [self.line setBackgroundColor:[UIColor fromRGB:0xE8E8E7]];
    [self addSubview:self.line];
    [self.line aa_setHeight:1];
    [self.line aa_superviewBottom:1];
    [self.line aa_superviewRight:0];
    [self.line aa_superviewLeft:0];
}

#pragma mark - setting mask string

- (void)setPhoneMask {
    NSString *mask = @"+7 (###) ### ## ##";
    [self setMaskString:mask];
}

- (void)setMaskString:(NSString *)maskString {
    if (self.textField) {
        [self.textField removeFromSuperview];
        self.textField = nil;
    }
    if (!self.maskedTextFieldView) {
        self.maskedTextFieldView = [[OCMaskedTextFieldView alloc] initWithFrame:CGRectZero andMask:maskString showMask:YES];
        [self.maskedTextFieldView.maskedTextField aa_superviewFillWithInset:0];
    }
    self.maskedTextFieldView.maskedTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.maskedTextFieldView.maskedTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.maskedTextFieldView.maskedTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self addSubview:self.maskedTextFieldView];

    [self.maskedTextFieldView aa_superviewTop:0];
    self.textFieldLeftConstraint = [self.maskedTextFieldView aa_superviewLeft:10];
    self.textFieldRightConstraint = [self.maskedTextFieldView aa_superviewRight:10];
    [self.maskedTextFieldView aa_superviewBottom:0];
}

#pragma mark - textfield value manipulations


/**
 * Getting the value
 */
- (NSString *)value {
    if(self.textField) {
        return [self.textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    } else if (self.maskedTextFieldView) {
        return [self.maskedTextFieldView getRawInputText];
    }
    return @"";
}

/**
 * Clearing the text
 */
- (void)clear {
    if(_textField) _textField.text = @"";
}

/**
 * Setting the value
 */
- (void)setValue:(NSString *)value {
    if(_textField) {
        _textField.text = value;
    } else if (self.maskedTextFieldView) {
        [self.maskedTextFieldView setRawInput:value];
    }
}

/**
 * Setting placeholder
 */
- (void)setPlaceholder:(NSString *)placeholder {
    if(_textField) {
        _textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: [UIColor fromRGB:0xB8BDB8]}];
    }
}

/**
 * Enabling and disabling
 */
- (void)enabled:(BOOL)enabled {
    if(_textField) {
        _textField.enabled = enabled;
    }
}

/**
 * Focus on field
 */
- (void)focus {
    if(_textField) {
        [_textField becomeFirstResponder];
    }
}

#pragma mark - icon

- (void)setIcon:(NSString *)iconName {
    if (self.iconView) {
        [self.iconView removeFromSuperview];
        self.iconView = nil;
    }
    self.iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:iconName]];
    self.iconView.translatesAutoresizingMaskIntoConstraints  = false;
    self.iconView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.iconView];

    [self.textFieldLeftConstraint aa_autoRemove];
    [self.iconView aa_superviewLeft:0];
    [self.iconView aa_superviewTop:0];
//    [self.iconView aa_pinAfterView:self.textField ? self.textField : self.maskedTextFieldView];
    [self.iconView autoPinEdge:ALEdgeRight toEdge:ALEdgeLeft ofView:self.textField withOffset:-kTfPadding];
    [self.iconView aa_superviewBottom:0];
}

#pragma mark - right view or text

- (void)setRightText:(NSString *)text {
    if (self.textField || self.maskedTextFieldView) {
        self.textLabel = [[UILabel alloc] init];
        self.textLabel.backgroundColor = [UIColor clearColor];
        self.textLabel.font = _textField.font;
        self.textLabel.textColor = _textField.textColor;
        self.textLabel.text = text;
        [self addSubview:self.textLabel];

        self.textFieldRightConstraint.constant = -50;
        [self.textLabel aa_superviewRight:10];
        [self.textLabel aa_superviewTop:0];
        [self.textLabel aa_superviewBottom:0];

    }
}

- (void)setRightView:(UIView *)view {
    [self setRightView:view rightPadding:15];
}

- (void)setRightView:(UIView *)view rightPadding:(CGFloat)padding {
    [self addSubview:view];
    [self.textFieldRightConstraint aa_autoRemove];
    [view aa_right:self.textField offset:padding];
    [view aa_superviewLeft:0];
    [view aa_superviewTop:0];
    [view aa_superviewBottom:0];
}

#pragma mark -
#pragma mark Prefix

- (void)setPrefix:(NSString *)prefix
{
    UILabel *prefixLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    prefixLabel.backgroundColor = [UIColor clearColor];
    prefixLabel.font = _textField.font;
    prefixLabel.textColor = _textField.textColor;
    prefixLabel.text = prefix;
    [prefixLabel sizeToFit];
    prefixLabel.centerY = _textField.centerY;
    [self addSubview:prefixLabel];

    if (self.iconView) {
        prefixLabel.x = self.right + 3;
    } else {
        prefixLabel.x = 8;
    }

    CGFloat x1 = _textField.x;
    _textField.x = prefixLabel.right + 5;
    _textField.width -= (_textField.x - x1);
}

#pragma mark -
#pragma mark Keyboard

- (void)setEmailKeyboard {
    _textField.keyboardType = UIKeyboardTypeEmailAddress;
}

- (void)setPhoneNumberKeyboard {
    _textField.keyboardType = UIKeyboardTypePhonePad;
}

- (void)setPasswordField {
    _textField.secureTextEntry = YES;
}

#pragma mark -
#pragma mark Loader

-(void)startLoading
{
    self.loading = YES;
    self.userInteractionEnabled = NO;

    loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loader.x = self.width - loader.width;
    loader.centerY = self.middleY;
    [self addSubview:loader];
    [loader startAnimating];
}

-(void)stopLoading
{
    self.loading = NO;
    self.userInteractionEnabled = YES;

    if(loader) {
        [loader removeFromSuperview];
        loader = nil;
    }
}

#pragma mark -
#pragma mark Blocks

- (void)onValueChanged:(InputTextFieldOnValueChangedBlock)block {
    _onValueChangedBlock = block;
}

- (void)onValueChange:(InputTextFieldOnValueChangeBlock)block {
    _onValueChangeBlock = block;
}


#pragma mark -
#pragma mark Delegates

- (void)textFieldDidChange:(id)textFieldDidChange {
    // Check if entered text field length is more that max allowed length.
    NSString *text = [_textField.text trim];
    if(text.length > self.maxCharacters) {
        text = [text substringToIndex:self.maxCharacters];
        _textField.text = text;
        return;
    }

    // Call value change block
    if (_onValueChangeBlock) {
        _onValueChangeBlock();
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (self.textFieldDidBeginEditingBlock) {
        self.textFieldDidBeginEditingBlock(textField);
    }
    previousValue = textField.text;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if(_onValueChangedBlock && previousValue && ![previousValue isEqual:textField.text]) _onValueChangedBlock();
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;

    NSUInteger newLength = oldLength - rangeLength + replacementLength;

    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;

    return newLength <= _maxCharacters || returnKey;
}
#pragma mark -

- (void)setAligment:(NSTextAlignment)alignment {
    if(_textField) {
        _textField.textAlignment = alignment;
    } else if (self.maskedTextFieldView) {
        self.maskedTextFieldView.maskedTextField.textAlignment = alignment;
    }
}



@end