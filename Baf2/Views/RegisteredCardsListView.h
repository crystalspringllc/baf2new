//
//  RegisteredCardsListView.h
//  BAF
//
//  Created by Almas Adilbek on 8/22/14.
//
//



#import "ProductRowView.h"

@protocol RegisteredCardsListViewDelegate;

@interface RegisteredCardsListView : UIView <ProductRowViewDelegate>

@property(nonatomic, weak) id<RegisteredCardsListViewDelegate> delegate;
- (id)initWithCards:(NSArray *)cards title:(NSString *)title;

@end


@protocol RegisteredCardsListViewDelegate<NSObject>
- (void)registeredCardsListViewProductRowViewDidTapped:(ProductRowView *)rowView;
@end
