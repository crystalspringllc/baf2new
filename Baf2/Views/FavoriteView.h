//
//  FavoriteView.h
//  BAF
//
//  Created by Almas Adilbek on 8/13/14.
//
//



@interface FavoriteView : UIView

@property(nonatomic, assign) BOOL isFavorited;

-(void)toggle;
-(void)setFavorited:(BOOL)favorited;

@end
