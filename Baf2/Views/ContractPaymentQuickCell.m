//
//  ContractPaymentQuickCell.m
//  Baf2
//
//  Created by Shyngys Kassymov on 14.04.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ContractPaymentQuickCell.h"
#import "Contract.h"
#import "NSString+Ext.h"
#import <UITextField+Blocks.h>

@interface ContractPaymentQuickCell () <UITextFieldDelegate>

@end

@implementation ContractPaymentQuickCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.fieldContainerView.layer.cornerRadius = 3;
    self.fieldContainerView.layer.borderColor = self.nextButton.backgroundColor.CGColor;
    self.fieldContainerView.layer.borderWidth = 1;
    
    __weak ContractPaymentQuickCell *weakSelf = self;
    self.paymentAmountField.shouldEndEditingBlock = ^BOOL(UITextField *textField) {
        if (weakSelf.onTextFieldEndEditing) {
            weakSelf.onTextFieldEndEditing(textField);
        }
        
        return true;
    };
    
    self.nextButton.layer.cornerRadius = 3;
    self.nextButton.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    [self.nextButton addTarget:self action:@selector(goNext:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Actions

- (void)goNext:(id)sender {
    if (self.onNextTapped) {
        self.onNextTapped(self.contract, [self.paymentAmountField.text numberDecimalFormat]);
    }
}

#pragma mark - Getters/Setters

- (void)setContract:(Contract *)contract {
    _contract = contract;
    
    [self.contractImageView sd_setImageWithURL:[[NSURL alloc] initWithString:self.contract.provider.logo] placeholderImage:[UIImage imageNamed:@"img_placeholder.png"]];
    self.contractNameLabel.text = self.contract.alias;
    self.contractNumberLabel.text = self.contract.contract;
}

@end
