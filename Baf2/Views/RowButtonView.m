//
// Created by Almas Adilbek on 9/27/14.
// Copyright (c) 2014 Almas Adilbek. All rights reserved.
//

#import "RowButtonView.h"
#import "UIFont+Custom.h"
#import "ButtonView.h"


@implementation RowButtonView {
    UIView *topLine;
    UILabel *titleLabel;
    BOOL hasIcon;
    RowButtonViewOnTapBlock _buttonViewOnTapBlock;
}

@synthesize titleLabel = _titleLabel, userData = _userData;

#pragma mark -
#pragma mark With selector

- (id)initWithTitle:(NSString *)title target:(id)target selector:(SEL)_selector {
    return [self initWithIcon:nil title:title target:target selector:_selector];
}

- (id)initWithIcon:(NSString *)iconName title:(NSString *)title target:(id)target selector:(SEL)_selector {
    return [self initWithWidth:kContentWidth icon:iconName title:title target:target selector:_selector];
}

- (id)initWithWidth:(CGFloat)width icon:(NSString *)iconName title:(NSString *)title target:(id)target selector:(SEL)_selector {
    return [self initWithWidth:width icon:iconName title:title onTap:nil target:target selector:_selector];
}

#pragma mark -
#pragma mark With block

- (id)initWithTitle:(NSString *)title onTap:(RowButtonViewOnTapBlock)block {
    return [self initWithIcon:nil title:title onTap:block];
}

- (id)initWithIcon:(NSString *)iconName title:(NSString *)title onTap:(RowButtonViewOnTapBlock)block {
    return [self initWithWidth:kContentWidth icon:iconName title:title onTap:block];
}

- (id)initWithWidth:(CGFloat)width icon:(NSString *)iconName title:(NSString *)title onTap:(RowButtonViewOnTapBlock)block {
    return [self initWithWidth:width icon:iconName title:title onTap:block target:nil selector:nil];
}

- (id)initWithWidth:(CGFloat)width icon:(NSString *)iconName title:(NSString *)title onTap:(RowButtonViewOnTapBlock)block target:(id)target selector:(SEL)_selector
{
    self = [super initWithFrame:CGRectMake(0, 0, width, 40)];
    if(self)
    {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        _buttonViewOnTapBlock = block;

        if(!title || (title.length == 0)) {
            title = @" ";
        }

        ButtonView *buttonView;
        if(block) {
            buttonView = [[ButtonView alloc] initWithTarget:self selector:@selector(onTap)];
        } else {
            buttonView = [[ButtonView alloc] initWithTarget:target selector:_selector];
        }
        buttonView.y = 1;
        buttonView.width = self.width + 2 * kViewSidePadding;
        buttonView.x = -kViewSidePadding;
        buttonView.height = self.height - 1;
        [self addSubview:buttonView];

        if(iconName)
        {
            hasIcon = YES;

            UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:iconName]];
            [self addSubview:iconView];
            iconView.centerY = self.centerY;
        }

        CGFloat titlePaddingLeft = [self titlePaddingLeft];

        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(titlePaddingLeft, 0, [self titleWidth], 1)];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont helveticaNeue:15];
        _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _titleLabel.numberOfLines = 0;
        _titleLabel.textColor = [UIColor fromRGB:0x21546D];
//        _titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _titleLabel.text = title;
        [_titleLabel sizeToFit];
        [self addSubview:_titleLabel];

        _titleLabel.centerY = self.middleY;

        self.centerX = (CGFloat) ([ASize screenWidth] * 0.5);
    }
    return self;
}

#pragma mark -
#pragma mark Actions

- (void)onTap {
    if(_buttonViewOnTapBlock) {
        _buttonViewOnTapBlock(self);
    }
}


#pragma mark -
#pragma mark Methods

- (void)removeTopLine {
    [topLine removeFromSuperview];
}

- (void)setTitle:(NSString *)title {
    _titleLabel.width = [self titleWidth];
    _titleLabel.text = title;
    [_titleLabel sizeToFit];
    _titleLabel.centerY = self.middleY;
}

- (BOOL)validate {
    return _titleLabel.text.length != 0;
}


#pragma mark -
#pragma mark Helper

-(CGFloat)titlePaddingLeft {
    return hasIcon?43:kRowButtonViewSidePadding;
}

-(CGFloat)titleWidth {
    return self.width - [self titlePaddingLeft];
}

#pragma mark - Dealloc

- (void)dealloc {
    _buttonViewOnTapBlock = nil;
}


@end