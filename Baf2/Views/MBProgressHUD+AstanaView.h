//
//  MBProgressHUD+AstanaView.h
//  Baf2
//
//  Created by Shyngys Kassymov on 30.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (AstanaView)

+ (instancetype)showAstanaHUDWithTitle:(NSString *)title animated:(BOOL)animated;
+ (instancetype)showAstanaHUDWithoutMaskWithTitle:(NSString *)title animated:(BOOL)animated;
+ (instancetype)showAstanaHUDAddedTo:(UIView *)view withTitle:(NSString *)title animated:(BOOL)animated;
+ (void)hideAstanaHUDAnimated:(BOOL)animated;
+ (void)hideAstanaHUDForView:(UIView *)view animated:(BOOL)animated;

@end
