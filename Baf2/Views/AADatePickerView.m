//
//  AADatePickerView.m
//  WashMe
//
//  Created by Almas Adilbek on 10/26/14.
//  Copyright (c) 2014 Almas Adilbek. All rights reserved.
//

#import "AADatePickerView.h"
#import "ASize.h"
#import <FrameAccessor.h>

#define kDateFormat_ddMMyyyy @"dd.MM.yyyy"

@implementation AADatePickerView {
    UIView *fader;
}

-(id)init
{
    CGSize screenSize = [UIScreen mainScreen].bounds.size;

    self = [super initWithFrame:CGRectMake(0, 0, screenSize.width, 1)];
    if(self) {

        self.backgroundColor = [UIColor whiteColor];

        // Date pickers
        self.datePicker = [[UIDatePicker alloc] init];
        _datePicker.width = self.width;
        _datePicker.datePickerMode = UIDatePickerModeDate;
        [_datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];

        [self addSubview:_datePicker];

        self.height = _datePicker.height;
        self.y = screenSize.height;

    }
    return self;
}

- (void)setTitle:(NSString *)title
{
    CGFloat topPadding = 10;
    CGFloat sidePadding = 15;

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(sidePadding, 0, [ASize screenWidth] - 2 * sidePadding, 1)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.textColor = [UIColor grayColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 0;
    titleLabel.text = title;
    [titleLabel sizeToFit];
    titleLabel.centerX = self.middleX;
    titleLabel.y = topPadding;
    [self addSubview:titleLabel];

    _datePicker.y = titleLabel.bottom + topPadding;
    self.height = _datePicker.bottom;
}

- (void)show
{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    CGSize size = [UIScreen mainScreen].bounds.size;

    fader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    fader.alpha = 0.0;
    fader.backgroundColor = [UIColor blackColor];
    [window addSubview:fader];

    [window addSubview:self];

    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(faderTapped)];
    [fader addGestureRecognizer:ges];

    [UIView animateWithDuration:0.25 animations:^{
        fader.alpha = 0.15;
        self.y = size.height - self.height;
    } completion:^(BOOL finished) {
    }];
}

- (void)hide
{
    CGSize size = [UIScreen mainScreen].bounds.size;

    [UIView animateWithDuration:0.25 animations:^{
        fader.alpha = 0.0;
        self.y = size.height;
    } completion:^(BOOL finished) {
        [fader removeFromSuperview];
        [self removeFromSuperview];
    }];
}

- (void)setDatePickerDate:(NSDate *)date {
    _datePicker.date = date;
}

#pragma mark -
#pragma mark Actions

- (void)datePickerValueChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:kDateFormat_ddMMyyyy];
    [self.delegate datePickerViewDidChange:[df stringFromDate:datePicker.date]];
}

- (void)faderTapped {
    [self hide];
}

@end
