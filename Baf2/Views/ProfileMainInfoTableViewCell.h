//
//  ProfileMainInfoTableViewCell.h
//  Baf2
//
//  Created by Shyngys Kassymov on 20.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JVFloatLabeledTextField;

@interface ProfileMainInfoTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet JVFloatLabeledTextField *nicknameTextField;
@property (nonatomic, weak) IBOutlet JVFloatLabeledTextField *phoneTextField;
@property (nonatomic, weak) IBOutlet JVFloatLabeledTextField *emailTextField;

@property (nonatomic, copy) void (^didChangeText)(JVFloatLabeledTextField *field);

@end
