//
//  PaymentReceiptRowView.m
//  Baf2
//
//  Created by Shyngys Kassymov on 01.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "PaymentReceiptRowView.h"

@implementation PaymentReceiptRowView

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.valueLabel.preferredMaxLayoutWidth = self.valueLabel.frame.size.width;
}

@end
