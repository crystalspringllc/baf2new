//
//  FloatingTextField.h
//  Baf2
//
//  Created by nmaksut on 31.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <JVFloatLabeledTextField/JVFloatLabeledTextField.h>

typedef void (^FloatingTextFieldViewOnValueChangedBlock)(NSString *value);
typedef void (^FloatingTextFieldViewValueChangeBlock)(NSString *value);
typedef void (^DropdownFloatingTextFieldViewOptionChangedBlock)(NSDictionary *option, NSInteger index);

@protocol FloatingTextFieldViewDelegate <NSObject>
@optional
- (void)superTextFieldDidFinishEditing:(UITextField *)textField;
@end


@interface FloatingTextField : JVFloatLabeledTextField <UITextFieldDelegate>

// Params
@property(nonatomic, assign, getter=isTappable) BOOL tappable;
@property(nonatomic, assign, getter=isLoading) BOOL loading;
@property(nonatomic, assign) NSUInteger maxCharacters;
@property(nonatomic, assign) BOOL numeric;
@property(nonatomic, assign) BOOL decimalNumeric;

/**
 By default separatorHidden = true
 */
@property(nonatomic, assign) BOOL separatorHidden;
@property (nonatomic, strong) UIColor *separatorNormalColor;
@property (nonatomic, strong) UIColor *separatorActiveColor;

@property(weak) id<FloatingTextFieldViewDelegate> superTextFieldDelegate;

// Config
@property(nonatomic, strong) UIColor *placeholderColor;
@property(nonatomic, assign) CGFloat textFieldRightViewSpacing;
@property(nonatomic, assign) CGFloat textFieldIconSpacing;

// Mask
@property (nonatomic, strong) NSString *mask;

- (void)initView;

// The initializer methods.
- (void)initVars;
- (void)configUI;

// On self tap, if tappable YES
- (void)tap;

// TextField Methods
- (NSString *)value;
- (CGFloat)floatValue;
- (double)doubleValue;

- (void)setValue:(NSString *)value;
- (void)setPlaceholder:(NSString *)placeholder;
- (void)clear;
- (void)enabled:(BOOL)enabled;
- (void)focus;
- (void)hideClearButton;

- (BOOL)isset;

- (void)setFont:(UIFont *)font;
- (void)setTextColor:(UIColor *)color;

- (void)onValueChanged:(FloatingTextFieldViewOnValueChangedBlock)block;
- (void)onValueChange:(FloatingTextFieldViewValueChangeBlock)block;

@end
