//
// Created by Askar Mustafin on 3/31/16.
// Copyright (c) 2016 Банк Астаны. All rights reserved.
//

#import "HorizontalPushView.h"


@interface HorizontalPushView() {}
@property (nonatomic, strong) NSMutableArray *pushedViews;
@end

@implementation HorizontalPushView {}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)pushView:(UIView *)view {
    if (!self.pushedViews) {
        self.pushedViews = [[NSMutableArray alloc] init];
    }
    [self.pushedViews addObject:view];
    [self reloadViews];
}

- (void)removeAllSubviews {
    [self.pushedViews removeAllObjects];

    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
}

- (void)reloadViews {

    CGFloat xOffset = self.leftIndent;
    CGFloat yOffset = self.topIndent;

    int i = 0;

    for (UIView *view in self.pushedViews) {

        view.x = xOffset;
        view.y = yOffset;


        if (view.tag > 0) {
            [self addSubview:view];
        }

        if (view.right > self.right) {

            yOffset = yOffset + view.height + 5;
            self.height = yOffset + self.topIndent;

            xOffset = self.leftIndent;

            view.x = xOffset;
            view.y = yOffset;

        } else {

            xOffset = view.right + 5;

        }

        if (i == 0) {
            self.height = self.topIndent * 2 + view.height;
        } else {
            self.height = self.topIndent * 2 + view.height + yOffset;
        }


        i++;
    }
}

@end