//
//  ContractPaymentQuickCell.h
//  Baf2
//
//  Created by Shyngys Kassymov on 14.04.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Contract, IconedTextField, MainButton;

@interface ContractPaymentQuickCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *contractImageView;
@property (nonatomic, weak) IBOutlet UILabel *contractNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *contractNumberLabel;
@property (nonatomic, weak) IBOutlet UITextField *paymentAmountField;
@property (nonatomic, weak) IBOutlet UIButton *nextButton;
@property (nonatomic, weak) IBOutlet UIView *fieldContainerView;

@property (nonatomic, weak) Contract *contract;

@property (nonatomic, copy) void (^onTextFieldEndEditing)(UITextField *paymentAmountField);
@property (nonatomic, copy) void (^onNextTapped)(Contract *contract, NSNumber *inputAmount);

@end
