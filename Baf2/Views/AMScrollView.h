//
//  AMScrollView.h
//  ACB
//
//  Created by Mustafin Askar on 11/09/2015.
//
//

#import <UIKit/UIKit.h>

@interface AMScrollView : UIScrollView

/**
*  Push view to scrollview with margintop
*
*/
- (void)pushView:(UIView *)view marginTop:(CGFloat)marginTop centered:(BOOL)centered;

/**
*  Push view to scrollview with margintop
*
*/
- (void)pushView:(UIView *)view marginTop:(CGFloat)marginTop centered:(BOOL)centered addSeparator:(BOOL)separator;

/**
*  Remove pushed views into scrollview instead of headerview and hmsegmentedcontrol
*
*/
- (void)removeAllPushsedViews;

@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *lastViewBottom;
@property (nonatomic, weak) IBOutlet UIView *headerView;

@end
