//
//  DatePickerInputTextField.h
//  BAF
//
//  Created by Almas Adilbek on 11/1/14.
//
//

#import "InputTextField.h"
#import "AADatePickerView.h"

@interface DatePickerInputTextField : InputTextField <AADatePickerViewDelegate>

@property (nonatomic, strong) NSDate *date;
@property (nonatomic, copy) NSString *dateFormat;
@property (nonatomic, strong) AADatePickerView *datePickerView;

- (id)initWithTitle:(NSString *)title;
- (id)initWithTitle:(NSString *)title frame:(CGRect)frame;

- (NSDate *)getDateFromText;

@end
