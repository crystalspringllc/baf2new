//
//  ContractModelView.h
//  Baf2
//
//  Created by Shyngys Kassymov on 05.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contract.h"

@interface ContractModelView : NSObject

+ (instancetype)sharedInstance;

// properties
@property (nonatomic, strong, readonly) NSArray *contracts;

// states
@property (nonatomic, readonly) BOOL loading;
@property (nonatomic) BOOL needsReload;
@property (nonatomic, readonly) BOOL didLoadPaymentTemplates;

// methods
- (void)loadContracts:(void(^)(BOOL success, NSError *error))completion;
- (void)deleteLocalContractWithIdentifier:(NSNumber *)identifier;
- (NSArray *)contractsByCategoryName:(NSString *)categoryName;

@end