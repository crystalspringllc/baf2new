//
//  ContractModelView.m
//  Baf2
//
//  Created by Shyngys Kassymov on 05.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ContractModelView.h"
#import "ContractsApi.h"
#import "NSError+Extensions.h"
#import "NotificationCenterHelper.h"

@implementation ContractModelView

+ (instancetype)sharedInstance {
    static ContractModelView *sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [ContractModelView new];
    });
    
    return sharedInstance;
}

#pragma mark - Methods

- (void)loadContracts:(void (^)(BOOL success, NSError *error))completion {
    if (self.loading) {
        return;
    }
    
    _loading = true;
    
    [ContractsApi getContractsWithSuccess:^(NSArray *contracts) {
        _contracts = contracts;
        _loading = false;
        _didLoadPaymentTemplates = true;
        completion(true, nil);
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentTemplatesNeedsReload object:nil];
    } failure:^(NSString *code, NSString *message) {
        _loading = false;
        NSError *error = [NSError errorFromDomain:@"ContactsApi" message:message code:code];
        completion(false, error);
    }];
}

- (void)deleteLocalContractWithIdentifier:(NSNumber *)identifier {
    NSMutableArray *itemsToDelete = [NSMutableArray new];
    NSMutableArray *contracts = self.contracts.mutableCopy;
    for (int i = 0; i < contracts.count; i++) {
        Contract *contract = contracts[i];
        if ([contract.identifier integerValue] == [identifier integerValue]) {
            [itemsToDelete addObject:contract];
        }
    }
    
    [contracts removeObjectsInArray:itemsToDelete];
    _contracts = contracts;
}

- (NSArray *)contractsByCategoryName:(NSString *)categoryName {
    NSMutableArray *contractsByCategoryName = [NSMutableArray new];
    
    for (Contract *contract in self.contracts) {
        if ([contract.provider.categoryProviderName isEqualToString:categoryName]) {
            [contractsByCategoryName addObject:contract];
        }
    }
    
    return contractsByCategoryName;
}

@end
