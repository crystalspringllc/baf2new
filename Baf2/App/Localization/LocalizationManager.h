//
//  LocalizationManager.h
//  BAFC
//
//  Created by Shyngys Kassymov on 17.11.16.
//  Copyright © 2016 CrystalSpring LLP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Language.h"

#define LANGUAGE_CODE_KK @"kk"
#define LANGUAGE_CODE_RU @"ru"
#define LANGUAGE_CODE_EN @"en"

@interface LocalizationManager : NSObject

@property (nonatomic, strong) NSArray<Language *> *availableLanguages;
@property (nonatomic, strong) Language *currentLanguage;

+ (instancetype)sharedManager;
- (void)changeCurrentLanguageTo:(NSString *)languageCode;

@end
