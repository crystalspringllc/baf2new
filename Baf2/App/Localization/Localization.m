//
//  Localization.m
//  Myth
//
//  Created by Almas Adilbek on 12/13/12.
//
//

#import "Localization.h"
#import "AppUtils.h"

static NSString *_currentLocale;
static NSDictionary *_translations;

@implementation Localization

+(NSString *)currentLocale {
    if(!_currentLocale) {
        _currentLocale = [AppUtils userDefaults:@"lang"];
        if(!_currentLocale) _currentLocale = localeRU;
    }
    return _currentLocale;
}

@end
