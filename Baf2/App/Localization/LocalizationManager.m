//
//  LocalizationManager.m
//  BAFC
//
//  Created by Shyngys Kassymov on 17.11.16.
//  Copyright © 2016 CrystalSpring LLP. All rights reserved.
//

#import "LocalizationManager.h"
#import "NSBundle+Language.h"

#define APP_LANGUAGE_KEY @"AppleLanguages"

@implementation LocalizationManager

+ (instancetype)sharedManager {
    static LocalizationManager *sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [LocalizationManager new];
        [sharedManager setupLanguages];
    });
    
    return sharedManager;
}

- (void)setupLanguages {
    NSMutableArray *availableLanguages = [NSMutableArray new];
    
    /* Kazakh language */
    Language *kkLanguage = [[Language alloc] initWithCode:LANGUAGE_CODE_KK name:@"Қазақша"];
    [availableLanguages addObject:kkLanguage];
    
    /* Russian language */
    Language *ruLanguage = [[Language alloc] initWithCode:LANGUAGE_CODE_RU name:@"Русский"];
    [availableLanguages addObject:ruLanguage];
    
    /* English language */
    Language *enLanguage = [[Language alloc] initWithCode:LANGUAGE_CODE_EN name:@"English"];
    [availableLanguages addObject:enLanguage];
    
    self.availableLanguages = availableLanguages;
    
    NSArray *appLanguages = [[NSUserDefaults standardUserDefaults] objectForKey:APP_LANGUAGE_KEY];
    if (appLanguages && appLanguages.count > 0) {
        BOOL unknownLanguage = true;
        for (Language *language in availableLanguages) {
            if ([language.code isEqualToString:appLanguages[0]]) {
                unknownLanguage = false;
                break;
            }
        }
        
        if (unknownLanguage) {
            [self setDefaultLanguage];
        } else {
            [self updateCurrentLanguage:appLanguages[0]];
        }
    } else {
        [self setDefaultLanguage];
    }
}

- (void)updateCurrentLanguage:(NSString *)languageCode {
    for (Language *language in self.availableLanguages) {
        if ([language.code isEqualToString:languageCode]) {
            self.currentLanguage = language;
            break;
        }
    }
    
    [NSBundle setLanguage:languageCode];
    
    [[NSUserDefaults standardUserDefaults] setObject:@[languageCode] forKey:APP_LANGUAGE_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setDefaultLanguage {
    [self updateCurrentLanguage:LANGUAGE_CODE_RU];
}

#pragma mark - Methods

- (void)changeCurrentLanguageTo:(NSString *)languageCode {
    [self updateCurrentLanguage:languageCode];
}

@end
