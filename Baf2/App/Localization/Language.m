//
//  Language.m
//  BAFC
//
//  Created by Shyngys Kassymov on 17.11.16.
//  Copyright © 2016 CrystalSpring LLP. All rights reserved.
//

#import "Language.h"

@implementation Language

- (instancetype)initWithCode:(NSString *)code name:(NSString *)name {
    self = [super init];
    
    if (self) {
        self.code = code;
        self.name = name;
    }
    
    return self;
}

@end
