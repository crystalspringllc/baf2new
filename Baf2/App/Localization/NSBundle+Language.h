//
//  NSBundle+Language.h
//  BAFC
//
//  Created by Shyngys Kassymov on 22.11.16.
//  Copyright © 2016 CrystalSpring LLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Language)

+ (void)setLanguage:(NSString *)language;

@end
