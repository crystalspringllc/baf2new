//
//  Language.h
//  BAFC
//
//  Created by Shyngys Kassymov on 17.11.16.
//  Copyright © 2016 CrystalSpring LLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Language : NSObject

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *name;

- (instancetype)initWithCode:(NSString *)code name:(NSString *)name;

@end
