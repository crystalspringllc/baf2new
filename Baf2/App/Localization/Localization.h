//
//  Localization.h
//  Myth
//
//  Created by Almas Adilbek on 12/13/12.
//
//

#import <Foundation/Foundation.h>

#define localeRU @"ru"

@interface Localization : NSObject {}

+ (NSString *)currentLocale;

@end