//
//  LocalizationConstants.h
//  Myth
//
//  Created by Almas Adilbek on 12/13/12.
//
//

#ifndef Myth_LocalizationConstants_h
#define Myth_LocalizationConstants_h

#define localeRU @"ru"
#define localeKZ @"kz"
#define localeEN @"en"

#define lnCANCEL @"cancel"
#define lnLOGIN @"login"
#define lnREGISTRATION @"registration"

#endif
