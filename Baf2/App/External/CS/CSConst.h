//
//  Const.h
//  CS
//
//  Created by EArt on 21.07.11.
//  Copyright 2011 CS LLC. All rights reserved.
//

#define C_VERSION @"i_0.91"

#define C_DB_FILE_NAME @"cs.sqlite"
#define C_DEFAULT_LOG_LEVEL 1 
#define C_LOG_LEVEL_DEBUG 3
#define C_LOG_LEVEL_MESSAGE 2
#define C_LOG_LEVEL_CRITICAL 1
#define C_LOG_LEVEL_ERROR 0


extern int const c_LL;

static const int C_LOG_MAX_LENGTH = 102400;

static const UInt8 C_PUBLIC_KEY_ID[] = "kz.crystalspring.publickey\0";
static const UInt8 C_SRV_PUBLIC_KEY_ID[] = "kz.crystalspring.srv.publickey\0";
static const UInt8 C_PRIVATE_KEY_ID[] = "kz.crystalspring.privatekey\0";

static const char C_AES_IV[16] = {0x72, 0xF3, 0x23, 0x96, 0xA5, 0x52, 0x88, 0xE2, 0x42, 0x74, 0x91, 0x02, 0x17, 0x2E, 0x84, 0x19};
static const char C_AES_COMMON_KEY[32] = {0xD8, 0x7F, 0x91, 0x3E, 0xA6, 0x59, 0x5B, 0x74, 0xBA, 0x83, 0xF7, 0x8A, 0x0E, 0x98, 0x23, 0x01, 0xE2, 0x04, 0xCE, 0x47, 0x9A, 0x18, 0x45, 0x65, 0xB1, 0xFA, 0xE4, 0x8F, 0x54, 0xC0, 0x06, 0x22}; 

#define C_SRV_PUBLIC_KEY @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDRNqo59dQQejYwPU4SxHGEVr864HHresvXLlqPVF/0+iQeMhT0u4fHptSyxlLK1Rg521ceLdBywfR4BqmYw7l2I0zaAnvZ4+chdRYnwn3TXVyZQ/KWOqkzSWykYGQYUWNnf7/zq/AMe1e9tuNq5rY5kuv0sQPw3x2sXQp/d1w9AwIDAQAB"

// каталог в ресурсах из которого все каталоги/файлы копируются в Documents:
#define C_ASSETS_DIR @"assets"

// DB vars names:
#define C_VAR_PRIVATE_KEY @"PR_KEY"
#define C_VAR_PUBLIC_KEY @"PB_KEY"
#define C_VAR_SRV_PUBLIC_KEY @"S_PB_KEY"
#define C_VAR_DEVICE_KEY @"DKEY"
#define C_VAR_USER_DIGEST_KEY @"HKEY"
#define C_VAR_SYNC_ID @"SYNC_ID"
#define C_VAR_DEV_TOKEN_ID @"TOKEN_ID"
#define C_VAR_DEV_INFO_FLAG @"DEV_INFO"
#define C_VAR_DEV_TYPE_FLAG @"DEV_TYPE"
#define C_VAR_DEVICE_ID @"DEVICE_ID"

//#define C_VAR_DB_IS_CHANGED @"DB_IS_CHANGED" // name of var in T_VARS = flag of change of any var
#define C_VAR_DEVICE_ID @"DEVICE_ID"
#define C_VAR_INSTALL_DATE @"INSTALL_DATE"
//#define C_VAR_SERVICE_STATE @"SERVICE_STATE"

// типы команд, отправляемых на сервер
#define C_REGISTRATION_PUB_KEY @"REG_PUB_KEY" // данные - публичн ключ der в Base64
#define C_REGISTRATION_DEVICE_KEY @"REG_DKEY" 
#define C_REGISTRATION_DIGEST @"REG_DIGEST" //формат отправки: {"N":"+NotificationId+","D":"+CurrentDate+"}, дата в формате yyyy-MM-dd HH:mm:ss
#define C_INFO_LOG @"INF_LOG" //формат отправки: строка из строк с разделителем ||
#define C_INFO_SHOW_NOTIFICATION @"INF_SH_NTF" //формат отправки: {"N":"+NotificationId+","D":"+CurrentDate+"}, дата в формате yyyy-MM-dd HH:mm:ss
#define C_INFO_SELECT_WIDGET @"INF_SEL_WD" //формат отправки: {"N":"+NotificationId+","D":"+CurrentDate+"}, дата в формате yyyy-MM-dd HH:mm:ss
#define C_INFO_DELETE_WIDGET @"INF_DEL_WD" //формат отправки: {"N":"+NotificationId+","D":"+CurrentDate+"}, дата в формате yyyy-MM-dd HH:mm:ss
#define C_INFO_DEVICE @"DEV_INFO" //формат отправки: JSON
#define C_INFO_DEVICE_TYPE @"DEV_TYPE" // = "i" (iOS)
#define C_INFO_LOG @"INF_LOG" //формат отправки: строка из строк с разделителем ||
#define C_INFO_TOKEN_ID @"TOKEN_ID"


// Net:
//#define C_DEFAULT_URL1 @"http://homeplus.kz/cs/sync.php"
#define C_DEFAULT_URL1 @"http://46.101.234.87/cs/sync.php"
#define C_DEFAULT_URL2 @"http://82.200.139.162/cs/sync.php"
#define C_DEFAULT_URL3 @"http://178.89.186.131/~crystals/cs/sync.php"
#define C_DEFAULT_URL4 @"http://fb.crystalspring.kz/cs/sync.php"
#define C_DEFAULT_URL5 @"http://www.ios-apps.kz/cs"

//109.233.240.226 cs old

//#define C_DEFAULT_URL @"http://192.168.1.50/cs/sync.php"

#define C_LOGIN_PAGE @"login.html"

#define C_START_PAGE @"informer.html" 
//#define C_START_PAGE @"iphone_start_090.html" 
//#define C_START_PAGE @"andr_start_091.html" 
//#define C_START_PAGE @"informer_test.html" //debug!!!!

#define C_ERROR_PAGE @"error.html"
#define C_NOTIFS_PAGE @"notifs.html"
#define C_ONSHOW_JS_PROCNAME @"try{OnAppShowProc();}catch(e){}";

#define C_ERROR_MSG_SQL_PREPARE @"SQL prepare error"
#define C_ERROR_MSG_SQL_EXEC @"SQL exec error"
#define C_ERROR_MSG_ROW_NOT_FOUND @"Row is not found"
#define C_ERROR_CRYPTO @"Crypto error"
#define C_ERROR_NET @"Net error"
