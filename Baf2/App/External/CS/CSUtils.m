//
//  CSUtils.m
//  CS
//
//  Created by EArt on 21.07.11.
//  Copyright 2011 CS LLC. All rights reserved.
//

#import "CSUtils.h"
#import "DBHelper.h"
#import "CSConst.h"
#include <sys/sysctl.h>

#define kApplicationUUIDKey @"appUUID"

@implementation CSUtils

/**
 @function strPos Получение позиции подстроки pSubString в строке pString
 @param pSubString искомая подстрока
 @param pString строка
 @return позиция подстроки или -1
 */
+ (int) strPos :(NSString *) pString :(NSString *) pSubString {
    NSRange vRange = [pString rangeOfString:pSubString options:0];
    if (vRange.length == 0) return -1;
    return (int)vRange.location;
}

/**
 @function getSysInfoByName служебный метод получения системной информации о девайсе
 @param typeSpecifier
 @return 
 */
+ (NSString *) getSysInfoByName:(char *)typeSpecifier {
	size_t size;
    sysctlbyname(typeSpecifier, NULL, &size, NULL, 0);
    char *answer = malloc(size);
	sysctlbyname(typeSpecifier, answer, &size, NULL, 0);
	NSString *results = [NSString stringWithCString:answer encoding: NSUTF8StringEncoding];
	free(answer);
	return results;
}

/**
 @function getPlatform Описание платформы устройства
 @return строка с описанием
 if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
 if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
 if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
 if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
 if ([platform isEqualToString:@"iPhone3,2"])    return @"Verizon iPhone 4";
 if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
 if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
 if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
 if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
 if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
 if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
 if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
 if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
 if ([platform isEqualToString:@"i386"])         return @"Simulator";
 
 */
+ (NSString *) getPlatform {
	return [self getSysInfoByName:"hw.machine"];
}

/**
 @function getModel получение наименования модели устройства
 @return строка описания
 */
+ (NSString *) getModel {
	return [self getSysInfoByName:"hw.model"];
}

/**
 @function getDeviceInfo Получение строки JSON c описанием устройства
 @return строка с описанием
 */
+ (NSString *) getDeviceInfo {
    NSLog(@"identifierForVendor: %@", [self identifierForVendor]);
    return [NSString stringWithFormat:@"{\"MODEL\":\"%@\",\"PLATFORM\":\"%@\",\"MAC\":\"%@\"}", [self getModel], [self getPlatform], [self identifierForVendor]];
}

// Get unique random string
+ (NSString *) getUniqueId {
    CFUUIDRef vCFUUIDRef = CFUUIDCreate(kCFAllocatorDefault);
    NSString* vResult = (NSString *)CFBridgingRelease(CFUUIDCreateString(NULL, vCFUUIDRef));
    CFRelease(vCFUUIDRef);
    return vResult;
}

// Get identifierForVendor which doesn't change untill iOS is reinstalled or updated etc.
+ (NSString *)identifierForVendor {
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

/**
 @function getDeviceId получение device id
 @return строка с device id
 */
+ (NSString *) getDeviceId {
    NSString* vResult = [DBHelper GetVar:C_VAR_DEVICE_ID :@"-1"];
    if ([vResult isEqualToString:@"-1"]) {
        vResult = [self identifierForVendor];
        [DBHelper SetVar:C_VAR_DEVICE_ID :vResult];
    }
    return vResult;
//    return [NSString stringWithFormat:@"%@", [[UIDevice currentDevice] identifierForVendor]]; - each exec differnet id!!!
//    return [self getMacAddress]; // deprecated! EArt 17 01 2014
//    return [[UIDevice currentDevice] uniqueIdentifier]; // deprecated! EArt 05 09 2012
}

//+ (NSString *) getAppList {
//    NSString *vResult = @"{\"A\":[";
//    NSString *vAppFolderPath = [[NSBundle mainBundle] resourcePath];
//    NSArray *vDirectoryContents = [[NSFileManager defaultManager] directoryContentsAtPath: vAppFolderPath];
//    for(int i = 0; i<[vDirectoryContents count]; i++) {
//        NSString *vFileName = [vDirectoryContents objectAtIndex:i];
//        NSString *vComma = @"";
//        if ([vResult length] > 6) vComma = @",";
//        vResult = [NSString stringWithFormat:@"%@%@\"%@\"", vResult, vComma, vFileName];
//    }
//    vResult = [NSString stringWithFormat:@"%@]}", vResult];
//    return vResult;
//}

//+ (NSString *) getProcList {
//    NSString *vResult = @"{\"P\":[";
//    NSArray *vAppList = [[NSWorkspace sharedWorkspace] runningApplications];
//}


 

// ***************************************************************
// JSON Parser:
// ***************************************************************

/**
 @function getTag получение подстроки из строки pStr начиная с подстроки pStrBegin и заканчивая подстрокой pStrEnd
    Поиск ведется начиная с позиции pSearchStartPosition
 @param pStr исходная строка 
 @param pStrBegin подстрока начала тэга
 @param pStrEnd подсрока окончания тэга
 @param pSearchOption - параметры поиска
 @param pSearchStartPosition
 @return содержимое тэга или NULL
 */
+ (NSDictionary *) getTag :(NSString *)pStr :(NSString *)pStrBegin :(NSString *)pStrEnd :(int)pSearchOption :(int)pSearchStartPosition {
//    [LogHelper aLog_DEBUG :C_TAG :@"getTag" :[NSString stringWithFormat:@"pStr_len=%i, pStrBegin=%@, pStrEnd=%@, pSearchOption=%i, pSearchStartPosition=%i", [pStr length], pStrBegin, pStrEnd, pSearchOption, pSearchStartPosition]];
    int vStrLen = (int)[pStr length];
    NSRange vRangeB = [pStr rangeOfString:pStrBegin options:0 range:NSMakeRange(pSearchStartPosition, vStrLen-pSearchStartPosition)];
    if (vRangeB.length == 0) return nil;
    int vStrBeginLen = (int)[pStrBegin length];
    NSRange vRangeSearch = NSMakeRange(vRangeB.location+vStrBeginLen, vStrLen-vStrBeginLen- vRangeB.location);
    NSRange vRangeE = [pStr rangeOfString:pStrEnd options:pSearchOption range:vRangeSearch];
    if (vRangeE.length == 0) return nil;
    NSRange vRangeResult = NSMakeRange(vRangeSearch.location, vRangeE.location - vRangeSearch.location);
    NSString *vRes = [pStr substringWithRange:vRangeResult];
    //    [LogHelper aLog :3 :@"getTag" :[NSString stringWithFormat:@" bl=%i sl=%i se=%i el=%i rl=%i re=%i r=[%@]", vRangeB.location, vRangeSearch.location, vRangeSearch.length, vRangeE.location, vRangeResult.location, vRangeResult.length, vRes]];
    
    return @{@"res" : vRes, @"pos" : @(vRangeE.location)};
}

/**
 @function getTag получение подстроки из строки pStr начиная с подстроки pStrBegin и заканчивая подстрокой pStrEnd
 @param pStr исходная строка 
 @param pStrBegin подстрока начала тэга
 @param pStrEnd подсрока окончания тэга
 @return содержимое тэга или NULL
 */
+ (NSString *) getTag :(NSString *)pStr :(NSString *)pStrBegin :(NSString *)pStrEnd {
//    [LogHelper aLog :3 :@"getTag" :[NSString stringWithFormat:@"pStr_len=%i, pStrBegin=%@, pStrEnd=%@", [pStr length], pStrBegin, pStrEnd]];    
    int vSearchStartPosition = 0;
    NSDictionary *vDict = [self getTag :pStr :pStrBegin :pStrEnd :0 :vSearchStartPosition];
    NSString * vRes = vDict[@"res"];
    return vRes;    
}

/**
 @function getTag получение подстроки из строки pStr начиная с подстроки pStrBegin и заканчивая подстрокой pStrEnd
 @param pStr исзодная строка 
 @param pStrBegin подстрока начала тэга
 @param pStrEnd подсрока окончания тэга
 @param pSearchOption - параметры поиска
 @return содержимое тэга или NULL
 */
+ (NSString *) getTag :(NSString *)pStr :(NSString *)pStrBegin :(NSString *)pStrEnd :(int)pSearchOption {
//    [LogHelper aLog :3 :@"getTag" :[NSString stringWithFormat:@"pStr_len=%i, pStrBegin=%@, pStrEnd=%@, pSearchOption=%i", [pStr length], pStrBegin, pStrEnd, pSearchOption]];
    int vSearchStartPosition = 0;
    NSDictionary *vDict = [self getTag :pStr :pStrBegin :pStrEnd :pSearchOption :vSearchStartPosition];
    NSString * vRes = vDict[@"res"];
    return vRes;    
}

/**
 @function getTag получение значения переменой в строке JSON pStr с именем pTagName
 Поиск ведется начиная с позиции pSearchStartPosition
 @param pStr исзодная строка 
 @param pTagName имя тега
 @return содержимое тэга или NULL
 */
+ (NSString *) getTag :(NSString *)pStr :(NSString *)pTagName {
//    [LogHelper aLog :3 :@"getTag" :[NSString stringWithFormat:@"pStr_len=%i, pTagName=%@", [pStr length], pTagName]]; 
    NSString *vBeginStr = [[@"\"" stringByAppendingString:pTagName] stringByAppendingString:@"\":\""];
    return [self getTag :pStr :vBeginStr :@"\""];
}

+ (NSString *)Data2Hex:(NSData*) dataValue {
    UInt32 byteLength =(UInt32) [dataValue length], byteCounter = 0;
    UInt32 stringLength = (byteLength*2) + 1, stringCounter = 0;
    unsigned char dstBuffer[stringLength];
    unsigned char srcBuffer[byteLength];
    unsigned char *srcPtr = srcBuffer;
//    [dataValue getBytes:srcBuffer];
    [dataValue getBytes:srcBuffer length:byteLength];
    const unsigned char t[16] = "0123456789ABCDEF";
    
    for (;byteCounter < byteLength; byteCounter++){
        unsigned src = *srcPtr;
        dstBuffer[stringCounter++] = t[src>>4];
        dstBuffer[stringCounter++] = t[src & 15];
        srcPtr++;
    }
    dstBuffer[stringCounter] = '\0';
    
    return [NSString stringWithUTF8String:(char*)dstBuffer];
}

+ (NSDate *) String2Date :(NSString *)pSDate {
    NSDateFormatter *vDateFormatter = [[NSDateFormatter alloc] init];
    [vDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *vDate = [vDateFormatter dateFromString:pSDate];
    //    NSDate *vDate = [[NSDate alloc] init];
    //    vDate = [vDateFormatter dateFromString:vSDate];
    return vDate;
}

+ (NSString *) Date2String :(NSDate *)pDate {
    NSDateFormatter *vDateFormatter = [[NSDateFormatter alloc] init];
    [vDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *vResult = [vDateFormatter stringFromDate:pDate];
    return vResult;
}

+ (unsigned long long) GetFreeDiskSpace {
//    float totalSpace = 0.0f;
//    float vTotalFreeSpace = 0.0f;
    unsigned long long vResult = 0;
    NSError *vError = nil;  
    NSArray *vPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);  
    NSDictionary *vDictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[vPaths lastObject] error: &vError];  
    
    if (!vError) {
        vResult = [[vDictionary objectForKey:NSFileSystemFreeSize] longLongValue];
//        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];  
//        NSNumber *freeFileSystemSizeInBytes = [vDictionary objectForKey:NSFileSystemFreeSize];
//        totalSpace = [fileSystemSizeInBytes floatValue];
//        vTotalFreeSpace = [freeFileSystemSizeInBytes floatValue];
    } else { 
        [NSException raise:@"Error Obtaining System Memory" format:@" Domain:%@ err:%@ code:%li", [vError domain], [vError localizedDescription], (long)[vError code]];
    }
    return vResult;
}

+ (double) GetMin :(double)p1 :(double)p2 {
    if (p1 > p2) {
        return p2;
    }
    return p1;
}
+ (double) GetMax :(double)p1 :(double)p2 {
    if (p1 < p2) {
        return p2;
    }
    return p1;
}

//
//+ (double) LimitDouble :(double)pValue :(signed int)pMin :(signed int)pMax :(signed int)pDef {
//    if ((pValue > pMax) | (pValue < pMin)) {
//        return pDef;
//    }
//    return pValue;
//}

+ (float) GetOsVersion {
   return [[[UIDevice currentDevice] systemVersion] floatValue];
}

+ (void) ShowMessage :(NSString *)pTitle :(NSString *)pMessage {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:pTitle message:pMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
}

+ (float) FilterVal :(float)pIncrementalVal :(float)pCurrentVal :(float)pFilterFactor {
    float vFilterFactor = pFilterFactor;
    if (fabsf(pIncrementalVal) > 0.1) {
        float vDelta = fabsf((pCurrentVal - pIncrementalVal)/pIncrementalVal);
        if (vDelta > 1) {
            vFilterFactor = 0.4;
        } else {
            vFilterFactor = vFilterFactor * vDelta; //чем меньше разница, тем сильнее фильтрация
        }
    }
    return vFilterFactor * pCurrentVal + (1-vFilterFactor) * pIncrementalVal;
}

+ (void) CheckDevInfo
{
    // Если deviceId старый от мак адреса (iOS7) нужно почистить данные устройства
    NSString *deviceId = [self getDeviceId];
    if([deviceId isEqual:@"020000000000"]) {
        NSLog(@"deleting the device id 02000000000");
        [DBHelper DelVar:C_VAR_DEV_INFO_FLAG];
    }
    
    NSString *vDevInfo = [CSUtils getDeviceInfo];
    NSString *vDbDevInfo = [DBHelper GetVar:C_VAR_DEV_INFO_FLAG :@"-"];
    
    if (![vDbDevInfo isEqualToString:vDevInfo]) {
        [DBHelper AddOutDataRec :C_INFO_DEVICE_TYPE :[@"i" dataUsingEncoding:NSUTF8StringEncoding]];
        long vRec = [DBHelper AddOutDataRec :C_INFO_DEVICE :[vDevInfo dataUsingEncoding:NSUTF8StringEncoding]];
        if (vRec > -1){
            [DBHelper SetVar:C_VAR_DEV_INFO_FLAG :vDevInfo];
        }
    }
}


@end
