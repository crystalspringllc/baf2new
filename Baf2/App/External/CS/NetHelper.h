//
//  NetHelper.h
//  CS
//
//  Created by EArt on 21.07.11.
//  Copyright 2011 CS LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *kNetHelperSyncFinishedNotification = @"NetHelperSyncFinishedNotification";

@interface NetHelper : NSObject {
    
}

+ (void) useServerAppID:(NSString *)_srvAppID;
+ (NSString *) srvAppID;

+ (BOOL) IsSyncWorking;
+ (void) Sync;
+ (void) SyncStartAsThread;

@end
