//
//  NotificationsHelper.h
//  CSInfo
//
//  Created by EArt on 05.10.11.
//  Copyright 2011 CS LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NotificationsHelper : NSObject {
    
}

+ (void)AddLocalNotif :(int)pBundleNumber :(NSDate *)pDate :(NSString *)pAlertHeader :(NSString *)pAlertMessage :(NSString *)pUrl;
+ (void) CancelAllNotifs;
+ (void) DelNotify :(int)pNotifyId;

@end
