//
//  NetHelper.m
//  CS
//
//  Created by EArt on 21.07.11.
//  Copyright 2011 CS LLC. All rights reserved.
//
//  Работа с сетью

#import "NSData+Base64.h"
#import "NSData+Compression.h"
#import "SecurityHelper.h"
#import "NetHelper.h"
#import "LogHelper.h"
#import "DBHelper.h"
#import "FSHelper.h"
#import "CSUtils.h"
#import "CSConst.h"
//#import "CSInfoAppDelegate.h"
#import "NotificationsHelper.h"

static NSString *srvAppID = nil;

@implementation NetHelper

#define C_CLASS_TAG @"NetHelper"
BOOL fIsSyncWorking = FALSE;

/* указываем ID приложения на сервере
 */
+ (void) useServerAppID:(NSString *)_srvAppID {
    srvAppID = _srvAppID;
}

/* получение ID приложения на сервере
 */
+ (NSString *)srvAppID {
    return srvAppID;
}

/* получение состояния загрузки
 */
+ (BOOL) IsSyncWorking {
    return fIsSyncWorking;
}

/**
 @function DownloadData Загрузка данных с адреса pUrl 
 Если pOutData != NULL то предварительно эти данные передаются на сервер методом post
 @param pUrl адрес сервера с парамтерами 
 @param pOutData исходящие данные
 @return загруженные данные, может быть exception
 */
+ (NSData *) DownloadData :(NSString *) pUrl :(NSString *) pOutData {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"DownloadData" :[NSString stringWithFormat:@"pUrl=%@ - start", pUrl]]; 
    NSMutableURLRequest *vRequest = [[NSMutableURLRequest alloc] init];
    [vRequest setURL:[NSURL URLWithString:pUrl]];
    [vRequest setHTTPMethod:@"POST"];
    //set headers
    [vRequest addValue:[NSString stringWithFormat:@"application/json;charset=UTF-8"] forHTTPHeaderField: @"Content-Type"];
    
    if (pOutData != NULL) {
        [LogHelper Log_MESSAGE :C_CLASS_TAG :@"DownloadData" :[NSString stringWithFormat:@"pOutData_len=%lu", (unsigned long)[pOutData length]]];
        NSData *postData = [[NSString stringWithString:pOutData] dataUsingEncoding:NSUTF8StringEncoding];
        // ??       [vRequest setValue:[NSString stringWithFormat:@"%i", [pOutData length]] forHTTPHeaderField:@"Content-Length"];
        [vRequest setHTTPBody:postData];
    }
    // загрузка результата
    NSHTTPURLResponse *vResponse = nil;  
    NSError *vError = NULL; 
    NSData *vResult = NULL;
    vResult = [NSURLConnection sendSynchronousRequest:vRequest returningResponse:&vResponse error:&vError];  
    if (vError != NULL) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"DownloadData" :[NSString stringWithFormat:@"pUrl=%@, eCode=%li eReason=%@", pUrl, (long)[vError code], [vError localizedDescription]]]; 
        return NULL;
    }
    if ([vResponse statusCode] != 200 ) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"DownloadData" :[NSString stringWithFormat:@"pUrl=%@, Response code:%ld", pUrl, (long)[vResponse statusCode]]]; 
        return NULL;
    }
    if (vResult == NULL) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"DownloadData" :[NSString stringWithFormat:@"pUrl=%@, vResult=NULL - end", pUrl]]; 
        return NULL;
    } 
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"DownloadData" :[NSString stringWithFormat:@"pUrl=%@, vResult_len=%lu - end", pUrl, (unsigned long)[vResult length]]];
    
    //!!! - del it!!!
    //    NSString *vJSONString = [[NSString alloc] initWithData:vResult encoding:NSUTF8StringEncoding];
    //    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"DownloadData" :vJSONString];
    //!!!
    
    return vResult;
}


/**
 @function makeUrl получение полной строки адреса кэширующего сервера с параметрами
 @param pUrl адрес процедуры синхронизации кжширующего сервера 
 @return строка адреса с параметрами
 */
+ (NSString *) makeUrl :(NSString *)pUrl :(Boolean)pIsUploadOnly{
    NSString *vLastRec = @"9999999999";
    
    if (!pIsUploadOnly) {
        vLastRec = [DBHelper GetVar:C_VAR_SYNC_ID :@"0"];
    }
    //!! NSString *vResult = [NSString stringWithFormat:@"%@%?d=iOS&r=%@&id=%@&app=Pointplus_i2&n=%i",@"http://crystalspring.kz/cs/sync1.php", vLastRec, [CSUtils getDeviceId], CFAbsoluteTimeGetCurrent()];

    NSString *vResult = [NSString stringWithFormat:@"%@?d=iOS&r=%@&id=%@&app=%@&n=%f", pUrl, vLastRec, [CSUtils getDeviceId], [NetHelper srvAppID], CFAbsoluteTimeGetCurrent()];
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"makeUrl" :[NSString stringWithFormat:@"vResult=%@", vResult]];
    return vResult;
}


/**
 @function Sync_GetJSONData Получение новых данных обновления в формате JSON,
 с одного их кэширующих серверов. Отправка исходящих данных - после обрабоки входящих, 
 иначе будет двойная отправка исходящих данных! 
 Адреса серверов берутся из исписка адресов в БД, причем в порядке убывания даты 
 последнего успешного обращения. Перед отправкой формируется массив JSON исходящих даных
 @return загруженные данные в формате JSON, может быть exception
 */
+ (NSData *) Sync_GetJSONData {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Sync_GetJSONData" :@"- start"];
    NSData *vResult = NULL;
    NSMutableArray *vArrOfURL = [DBHelper GetUrls];
    for (int i = 0; i < [vArrOfURL count] / 2; i++) {
        @try {
            NSNumber *vKey = [vArrOfURL objectAtIndex:(i*2)];
            NSString *vUrl = [self makeUrl :[vArrOfURL objectAtIndex:(i*2)+1] :NO];
            vResult = [self DownloadData:vUrl :NULL];
            if (vResult != NULL) {
                if ( [vResult length] > 0 ) {
                    // установка для текущего адреса признака актуальности
                    if ([vKey intValue] >= 0) { 
                        // <0 - это адреса по умолчанию, их нет в БД
                        [DBHelper SetAccessDateOfUrl:[vKey intValue]];
                    }
                    break;
                }
            }
        }
        @catch (NSException *exception) {
            if (exception != NULL) {
                [LogHelper Log_ERROR :C_CLASS_TAG :@"Sync_GetJSONData" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
            } else {
                [LogHelper Log_ERROR :C_CLASS_TAG :@"Sync_GetJSONData" :@"Unknown exception"];
            }
        }
    }
    if (vResult == NULL) {
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"Sync_GetJSONData" :@"vResult=NULL - end"];
    } else {
        [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Sync_GetJSONData" :[NSString stringWithFormat:@"vResult_len=%lu - end", (unsigned long)[vResult length]]];
    }
   /* NSString *vUrl = @"http://178.89.186.131/~crystals/cs/sync.php";
    NSString *vUrl = [self makeUrl:C_DEFAULT_URL6 :NO];
    vResult = [self DownloadData:vUrl :NULL];
    NSLog(@"Response string: %@", vResult);*/
    return vResult;
}



/**
 @function PostOutData Отправка исходящих данных.
 */
+ (void) PostOutData {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"PostOutData" :@"- start"];
    
    int vLastOutId = [DBHelper GetLastOutId];
    NSString *vOutData = NULL;
    if (vLastOutId > 0) {
        vOutData = [DBHelper GetOutData:vLastOutId];
    }
    if (vOutData != NULL) {
        NSMutableArray *vArrOfURL = [DBHelper GetUrls];
        @try{
            for (int i = 0; i < [vArrOfURL count] / 2; i++) {
                NSString *vUrl = [self makeUrl :[vArrOfURL objectAtIndex:(i*2)+1] :YES];
                if ([self DownloadData:vUrl :vOutData] != NULL)
                    break;
            }
        }
        @catch (NSException *exception) {
            [LogHelper Log_ERROR :C_CLASS_TAG :@"PostOutData" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
        }
    }
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"PostOutData" :@"vResult=NULL - end"];
}


// Обработка принятой записи типа CN - подтверждение приема, удаление даных из исходящей очереди
+ (void) Process_CN :(NSString *) pRec {
    int vPos = [CSUtils strPos:pRec :@":"];
    int vRecId = [[pRec substringToIndex:vPos] intValue];
    NSString *vRecDate = [pRec substringFromIndex:vPos+1];
    if ([DBHelper IsOutIdExists:vRecId :vRecDate]) {
        // удаление только если запись еще имеется, т к в SQLite после удаления
        // всех записей нумерация начинается сначала!
        [DBHelper DelOutData :vRecId];
    }
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Process_CN" :pRec]; 
}

// Обработка принятой записи типа CD - удаление файла
+ (void) Process_CD :(NSString *) pRec {
    [FSHelper delFile:pRec];
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Process_CD" :pRec]; 
}

// Обработка принятой записи типа CU - добавление или обновление файла
// pCrypted - если T то файл зашифрован индивидуальным ключем, ему добавляется расширение ".crypted"
// pRec - строка в формате fileName,fileDataB64
+ (void) Process_CU :(NSString *) pCrypted :(NSString *) pRec {
    int vPos = [CSUtils strPos:pRec :@","];
    NSString *vFileName = [pRec substringToIndex:vPos];
    NSString *vFileData = [pRec substringFromIndex:vPos+1];
    
    if (vFileName != NULL) {
        if ([vFileName length] == 0) {
            [LogHelper Log_ERROR :C_CLASS_TAG :@"Process_CU" :@"Empty FileName!"];
            return;
        }
    }
    
    if ([pCrypted isEqualToString:@"T"]) {
        vFileName = [NSString stringWithFormat:@"%@.crypted", vFileName];
    }    
    NSData *vData = [NSData dataFromBase64String:vFileData];
    if (vData == NULL) {
        [NSException raise:C_ERROR_NET format:@"CU:vData is null!"];
    }
    [FSHelper updateFile:vFileName :vData];
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Process_CU" :[NSString stringWithFormat:@"vFileName=%@, vIsCrypted=%@, Data_len=%lu", vFileName, pCrypted, (unsigned long)[vData length]]];
}

// Обработка принятой записи типа VD - удаление переменной
// pRec - VarName
+ (void) Process_VD :(NSString *) pRec {
    [DBHelper DelVar:pRec];
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Process_VD" :pRec];
}

// Обработка принятой записи типа VU - добавление или обновление переменной
// pRec - строка в формате VarName,VarValue
+ (void) Process_VU :(NSString *) pRec {
    int vPos = [CSUtils strPos:pRec :@","];
    NSString *vVarName = [pRec substringToIndex:vPos];
    NSString *vVarValue = [pRec substringFromIndex:vPos+1];
    [DBHelper SetVar:vVarName :vVarValue];
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Process_VU" :pRec];
}


// Обработка принятой записи типа UD - удаление URL из списка адресов серверов
// pRec - ид записи
+ (void) Process_UD :(NSString *) pRec {
    int vUrlId = [pRec intValue];
    [DBHelper DelUrl:vUrlId];
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Process_UD" :pRec];
}

// Обработка принятой записи типа UU - добавление или обновление адреса кэширующего сервера
// pRec строка в формате ID,URL
+ (void) Process_UU :(NSString *) pRec {
    int vPos = [CSUtils strPos:pRec :@","];
    NSString *vUrlId = [pRec substringToIndex:vPos];
    NSString *vUrl = [pRec substringFromIndex:vPos+1];    
    [DBHelper UpdateUrl:[vUrlId intValue] :vUrl];
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Process_UU" :pRec];
}


// Обработка принятой записи типа ND - удаление информации увдеомления
// pRec - ид записи
+ (void) Process_ND :(NSString *) pRec {
    int vNotifId = [pRec intValue];
    [NotificationsHelper DelNotify:vNotifId];
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Process_ND" :pRec];
}

// Обработка принятой записи типа NU - добавление или обновление данных уведомления (канала)
// @param pRec строка в формате 100002,N,start_date,stop_date,latitude,longitude,url,text_in_base64,pic_in_base64
//	"100002","N","","","","","Q1NJbmZNi4yNw==","rates.html?r0=NB.kz&r1=UCH&r2=KZT&r3=USD",""
//	100002,N,start_date,stop_date,latitude,longitude,text_in_base64,url,pic_in_base64   
// dates in format yyyy-MM-dd hh:mm:ss
+ (void) Process_NU :(NSString *) pRec {
    NSArray *vSArr = [pRec componentsSeparatedByString:@"\",\""];
    NSString *vSDate = [vSArr objectAtIndex:2];
    NSString *vText = [vSArr objectAtIndex:6];
    NSString *vUrl = [vSArr objectAtIndex:7];
    NSDate *vDate = [NSDate dateWithTimeIntervalSinceNow:5]; //  через 5 сек
    if ([vSDate length] > 0 ) {
        vDate = [CSUtils String2Date :vSDate];
    }
    NSData *vBText = [NSData dataFromBase64String:vText];
    NSString *vSText = [[NSString alloc] initWithData:vBText encoding:NSUTF8StringEncoding];
    vSText = [vSText stringByReplacingOccurrencesOfString:@"|" withString:@"; "];
    [NotificationsHelper AddLocalNotif :1 :vDate :@"Open" :vSText :vUrl];
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Process_NU" :pRec];
}

// Обработка принятой записи типа PR - добавление или обновление публичного клча сервера
// @param pRec - публичный ключ в x509 Base64
+ (void) Process_PK :(NSString *) pRec {
    [SecurityHelper SetSrvPublicKey:pRec];
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Process_PK" :pRec];
}

// Обработка принятой записи типа LL - установка уровня журналирования
// pRec - уровень журналирования
+ (void) Process_LL :(NSString *) pRec {
    [LogHelper SetLogLevel:[pRec intValue]];
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Process_LL" :pRec];
}



/**
 @function Sync_ProcessJSON Обработка загруженных данных в формате JSON
 @param pJSONString Зуруженные данные
 */
+ (void) Sync_ProcessJSON :(NSString *)pJSONString {
    //NSString *vJSONString = @"{\"S\":[{\"J\":[1,\"Ss\",\"T\",\"F\",\"vQ_SIGNsdf asdf sd asf\",\"\"]},{\"J\":[221,\"TYPE2\",\"Q_ZIPPED2\",\"Q_CRYPTED2\",\"SIGN2\",\"vQ_DATA2\"]},{\"J\":[3,\"TYPE3\",\"Q_ZIPPED3\",\"Q_CRYPTED3\",\"vQ_SIGN3\",\"vQ_DATA3\"]}]}";
    
    //    NSString *vJSonString = {"S":[{"J":[$vJ_ID,$vQ_TYPE,$vQ_ZIPPED,$vQ_CRYPTED,$vQ_SIGN,$vQ_DATA]},{..}...]}
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Sync_ProcessJSON" :[NSString stringWithFormat:@"pJSONString_len=%lu - start", (unsigned long)[pJSONString length]]];
    @try {
        if (pJSONString == NULL) {
            [NSException raise:C_ERROR_NET format:@"pJSONString is null!"];
        }
        SecKeyRef vSrvPublicKey = NULL;
        int vPos = 0; // is position of last processed item in the array
        while (YES) {
            NSDictionary *vDict = [CSUtils getTag:pJSONString :@"{\"J\":" :@"]}" :0 :vPos];
            NSString *vRec = [vDict objectForKey:@"res"];
            if (vRec == nil) break;
            vPos = [[vDict objectForKey:@"pos"] intValue];
            
            int vPz = 0;
            NSDictionary *vDz;
            vDz = [CSUtils getTag :vRec :@"[" :@"," :0 :vPz];
            NSString *vId = [vDz objectForKey:@"res"];
            
            vDz = [CSUtils getTag :vRec :@"\"" :@"\"," :0 :vPz];
            vPz = [[vDz objectForKey:@"pos"] intValue];
            NSString *vType = [vDz objectForKey:@"res"];
            
            vDz = [CSUtils getTag :vRec :@",\"" :@"\"," :0 :vPz];
            vPz = [[vDz objectForKey:@"pos"] intValue];
            NSString *vIsZipped = [vDz objectForKey:@"res"];
            
            vDz = [CSUtils getTag :vRec :@",\"" :@"\"," :0 :vPz];
            vPz = [[vDz objectForKey:@"pos"] intValue];
            NSString *vIsCrypted = [vDz objectForKey:@"res"];
            
            vDz = [CSUtils getTag :vRec :@",\"" :@"\"" :0 :vPz];
            vPz = [[vDz objectForKey:@"pos"] intValue];
            NSString *vSign = [vDz objectForKey:@"res"];
            
            vDz = [CSUtils getTag :vRec :@",\"" :@"\"" :0 :vPz];
            vPz = [[vDz objectForKey:@"pos"] intValue];
            NSString *vData = [vDz objectForKey:@"res"];
            
            [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Sync_ProcessJSON" :[NSString stringWithFormat:@"vID=%@, vType=%@, vIsZipped=%@, vIsCrypted=%@, vSign_len=%lu, vData_len=%lu", vId, vType, vIsZipped, vIsCrypted, (unsigned long)[vSign length], (unsigned long)[vData length]] ]; 
            
            NSData *vBData = [NSData dataFromBase64String:vData];
            NSData *vBSign = [NSData dataFromBase64String:vSign];
            
            // проверяем цифровую подпись серверным публичным ключем:
            if (vSrvPublicKey == NULL) {
                vSrvPublicKey = [SecurityHelper GetSrvPublicKeyRef];        
                if (vSrvPublicKey == NULL) {
                    [NSException raise:C_ERROR_NET format:@"vSrvPublicKey is null!"];
                }
            }
            if ( ![SecurityHelper VerifyData:vSrvPublicKey :vBData :vBSign]) {
                [NSException raise:C_ERROR_NET format:@"Signature is invalid! vID=%@", vId];
            }
            // расшифровываем общим симметриным ключем:
            vBData = [SecurityHelper AES256DecryptWithCommonKey:vBData];
            if (vBData == NULL) {
                [NSException raise:C_ERROR_NET format:@"AES decrypting error! vID=%@", vId];
            }
            // при необходимости разархивируем:
            if ([vIsZipped isEqualToString:@"T"]) {
                vBData = [vBData gzipInflate];
                if (vBData == NULL) {
                    [NSException raise:C_ERROR_NET format:@"Unzip error! vID=%@", vId];
                }
            }
            NSString *vCRec = [[NSString alloc] initWithData:vBData encoding:NSUTF8StringEncoding];
            
            
            [LogHelper Log_CRITICAL :C_CLASS_TAG :@"Sync_ProcessJSON" :[NSString stringWithFormat:@"vID=%@, vType=%@, vIsZipped=%@, vIsCrypted=%@, vSign_len=%lu, vData_len=%lu", vId, vType, vIsZipped, vIsCrypted, (unsigned long)[vSign length], (unsigned long)[vBData length]] ]; 
            
            
            if ([vType isEqualToString:@"CD"]) {
                [self Process_CD :vCRec];
            } else  if ([vType isEqualToString:@"CU"]) {
                [self Process_CU :vIsCrypted :vCRec];					
            } else  if ([vType isEqualToString:@"VD"]) {
                [self Process_VD :vCRec];
            } else  if ([vType isEqualToString:@"VU"]) {
                [self Process_VU :vCRec];
            } else  if ([vType isEqualToString:@"UD"]) {
                [self Process_UD :vCRec];
            } else  if ([vType isEqualToString:@"UU"]) {
                [self Process_UU :vCRec];
            } else  if ([vType isEqualToString:@"ND"]) {
                [self Process_ND :vCRec];
            } else  if ([vType isEqualToString:@"NU"]) {
                [self Process_NU :vCRec];
            } else  if ([vType isEqualToString:@"PK"]) {
                [self Process_PK :vCRec];
            } else  if ([vType isEqualToString:@"LL"]) {
                [self Process_LL :vCRec];
            } else  if ([vType isEqualToString:@"CN"]) {
                [self Process_CN:vCRec];
            } else  if ([vType isEqualToString:@"VACUUM"]) {
                [DBHelper VacuumDB];
            }
            [DBHelper SetVar:C_VAR_SYNC_ID :vId];
            
        } 
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"Sync_ProcessJSON" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"Sync_ProcessJSON" :@"end"];
}


/*
 @function DownloadPack загрузка пакета данных 
 Пакет представляет собой байтовый массив в формате 
 Sign(126)_FileName1Len(4)_FileName1(..)_FileData1Len(4)_FileData1(..)_ ... _FileNameNLen(4)_FileNameN(..)_FileDataNLen(4)_FileDataN(..) 
 @param pUrl - урл файла пакета
 [self DownloadPackAsThread :@"http://192.168.1.50/map.pk"];
 
 */
+ (BOOL) DownloadPack :(NSString *)pUrl {
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"DownloadPack" :[NSString stringWithFormat:@"pUrl=%@, start", pUrl]];
    NSError *vError = nil;
    BOOL vResult = false;
    @try {
        NSData *vData = [NSData dataWithContentsOfURL:[NSURL URLWithString:pUrl] options:NSDataReadingUncached error:&vError];
        if(vError != NULL) {
            [NSException raise:C_ERROR_NET format:@"%@", [vError localizedDescription]];
        }
        if (vData == NULL) {
            [NSException raise:C_ERROR_NET format:@"%@", @"downloaded data is null!"];
        }
        if ([vData length] < 200) {
            [NSException raise:C_ERROR_NET format:@"%@", @"downloaded data length < 200!"];
        }
        // расшифровываем общим ключем
        vData = [SecurityHelper AES256DecryptWithCommonKey:vData];
        // извлекаем цифровую подпись (первые 128 байт)
        NSRange vR = NSMakeRange(0, 128);
        NSData *vSign = [vData subdataWithRange:vR];
        NSInteger vPacLen = [vData length] - 128;
        vData = [vData subdataWithRange:NSMakeRange(128, vPacLen)];
        // проверяем подпись
        if (! [SecurityHelper VerifyData:[SecurityHelper GetSrvPublicKeyRef] :vData :vSign]) {
            [NSException raise:C_ERROR_NET format:@"Signature is invalid!"];
        }
        UInt32 vDLen;
        NSInteger vPos = 0;
        while (vPos < vPacLen) {
            // получаем длину имени файла
            [vData getBytes:&vDLen range:NSMakeRange(vPos, 4)];
            vPos = vPos + 4;
            vDLen = (unsigned)NSSwapLong(vDLen); // CFSwapInt32(vDLen);
            // получаем имя файла
            NSData *vBFileName = [vData subdataWithRange:NSMakeRange(vPos, vDLen)];
            vPos = vPos + vDLen; 
            NSString *vFileName = [[NSString alloc] initWithData:vBFileName encoding:NSUTF8StringEncoding];            
            // получаем длину содержимого файла
            [vData getBytes:&vDLen range:NSMakeRange(vPos, 4)];
            vDLen = (unsigned)NSSwapLong(vDLen);
            vPos = vPos + 4;
            // получаем данные файла
            NSData *vBFileData = [vData subdataWithRange:NSMakeRange(vPos, vDLen)];
            vPos = vPos + vDLen;
            [FSHelper updateFile:vFileName :vBFileData];
            [LogHelper Log_MESSAGE :C_CLASS_TAG :@"DownloadPack" :[NSString stringWithFormat:@"Name=%@, len=%lu", vFileName, (unsigned long)[vBFileData length]] ];
            
        }
        vResult = TRUE;
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"DownloadPack" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"DownloadPack" :@"end"];
    return vResult;
}

+ (void) DownloadPacks {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"DownloadPacks" :@"start"];
    
    NSMutableArray *vArrOfURL = [DBHelper GetPackUrls];
    for (int i = 0; i < [vArrOfURL count] / 2; i++) {
        @try {
            NSNumber *vKey = [vArrOfURL objectAtIndex:(i*2)];
            NSString *vUrl = [vArrOfURL objectAtIndex:(i*2)+1];
            int vPackId = [vKey intValue];
            if ([self DownloadPack:vUrl]) {
                [LogHelper Log_CRITICAL :C_CLASS_TAG :@"DownloadPacks" :[NSString stringWithFormat:@"ok id=%i, url=%@", vPackId, vUrl] ];
                [DBHelper SetStatusOfPack :vPackId :@"P"];
            };
        }
        @catch (NSException *exception) {
            if (exception != NULL) {
                [LogHelper Log_ERROR :C_CLASS_TAG :@"DownloadPacks" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
            } else {
                [LogHelper Log_ERROR :C_CLASS_TAG :@"DownloadPacks" :@"Unknown exception"];
            }
        }
    }
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"DownloadPacks" :@"end"];
}

// загрузка данных на сервер из исходящей очереди, загрузка и обработка данных входящей очереди
+ (void) Sync {
    // загрузка данных
    NSData *vJSONData = [self Sync_GetJSONData];
    if (vJSONData != NULL) { // !=NULL - значит есть связь!
        // получение входящих данных
        NSString *vJSONString = [[NSString alloc] initWithData:vJSONData encoding:NSUTF8StringEncoding];
        [self Sync_ProcessJSON:vJSONString];
        // сохраняем накопившийся в буфере журнал в базу
        [LogHelper SaveErrLog];
        // запускаем отправку исходящих данных
        [self PostOutData];
        // запускаем загрузку пакетов
        [self DownloadPacks];    
    }
}







/**
 @function SyncAsThread процедура синхронизации с одним из кэширующих серверов
 */
+ (void) SyncAsThread {
    if (fIsSyncWorking) {
        [LogHelper Log_CRITICAL :C_CLASS_TAG :@"SyncProc" :@"Sync is running!"];
        return;
    }
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"SyncProc" :@"start"];
    UIApplication* vApp = [UIApplication sharedApplication];
    //EArt 13 06 2013 - commented: NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    @try {
        //      [fActIndicator startAnimating];
        @autoreleasepool { //EArt 13 06 2013 - added
            fIsSyncWorking = TRUE;
            vApp.networkActivityIndicatorVisible = YES;
            [NetHelper Sync];
        }
        //          Переход к главной странице после обновления
        //        [CSInfoAppDelegate GoToStartPageAfterSync];
        //      [fActIndicator stopAnimating];
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"SyncProc" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    @finally {
        fIsSyncWorking =  FALSE;
        //EArt 13 06 2013 - commented: [pool release];
        vApp.networkActivityIndicatorVisible = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kNetHelperSyncFinishedNotification object:nil];

        [LogHelper Log_MESSAGE :C_CLASS_TAG :@"SyncProc" :@"end"];
    }
}


/**
 @function syncStartAsThread запуск синхронизации в отдельном потоке
 */
+ (void) SyncStartAsThread {
    if([self srvAppID] == nil) {
        NSLog(@"WARNING: srvAppID not defined! Sync stopped!");
        return;
    }
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"syncStartAsThread" :@""];
    [NSThread detachNewThreadSelector:@selector(SyncAsThread) toTarget:self withObject:nil];
}




@end
