//
//  LogHelper.h
//  CS
//
//  Created by EArt on 21.07.11.
//  Copyright 2011 CS LLC. All rights reserved.
//
//  Логирование

#import <Foundation/Foundation.h>

@interface LogHelper : NSObject {
    int fCurrentLogLevel;
}

+ (void) Log_DEBUG :(NSString *)pClassTag :(NSString *)pProcName :(NSString *)pMessage;
+ (void) Log_MESSAGE :(NSString *)pClassTag :(NSString *)pProcName :(NSString *)pMessage;
+ (void) Log_CRITICAL :(NSString *)pClassTag :(NSString *)pProcName :(NSString *)pMessage;
+ (void) Log_ERROR :(NSString *)pClassTag :(NSString *)pProcName :(NSString *)pMessage;
+ (void) SetLogLevel :(int)pLevel;
+ (void) SaveErrLog;
+ (void) FreeLogBuffer;
+ (NSString *) GetLogString;

@end
