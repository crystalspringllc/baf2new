//
//  SecurityHelper.h
//  CSInfo
//
//  Created by EArt on 12.09.11.
//  Copyright 2011 CS LLC. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SecurityHelper : NSObject {
    
}

+ (void) Test;
+ (BOOL) isUserSessionActive;
+ (NSData *)AES256DecryptWithCommonKey :(NSData *)pData;
+ (NSData *)AES256EncryptWithCommonKey :(NSData *)pData;
+ (NSData *)AES256DecryptWithDeviceKey :(NSData *)pData;
+ (NSData *)AES256EncryptWithDeviceKey :(NSData *)pData;

+ (NSString *)GetB64SignOfDataWithDevicePrivateKey :(NSData *)pData;

+ (SecKeyRef) GetPublicKeyRef;
+ (SecKeyRef) GetPrivateKeyRef;
+ (SecKeyRef) GetSrvPublicKeyRef;
+ (BOOL) SetSrvPublicKey :(NSString *)pemPublicKeyString;

+ (NSData *)EncryptWithPublicKey :(SecKeyRef) pPublicKey :(NSData *)pData;
+ (NSData *)DecryptWithPrivateKey :(SecKeyRef)pPrivateKey :(NSData *)pData;
+ (BOOL) VerifyData :(SecKeyRef)pPublicKey :(NSData *)pData :(NSData *)pSignature;
+ (NSData *) GetSignOfData :(SecKeyRef)pPrivateKey :(NSData *)pData;

+ (BOOL) SaveNewPin :(NSString *)pOldPin :(NSString *)pNewPin;
+ (BOOL) OpenSession :(NSString *)pPin;
//+ (BOOL) CreateKeyPair;
//+ (NSString *)getX509FormattedPublicKey;



@end
