//
//  CLog.m
//  CS
//
//  Created by EArt on 21.07.11.
//  Copyright 2011 CS LLC. All rights reserved.
//
// журналирование 

#import "LogHelper.h"
#import "DBHelper.h"
#import "CSUtils.h"
#import "CSConst.h"

@implementation LogHelper

static int C_CURRENT_LOG_LEVEL = C_DEFAULT_LOG_LEVEL;
static NSMutableString *fLog = NULL;

/**
 @function SetLogLevel Установка уровня журналирования, до перезагрузки приложения или следующей установки
 @param pLevel Уровень критичности записи (от 0 - ошибки до 3 - спам)
 */
+ (void) SetLogLevel :(int)pLevel {
    C_CURRENT_LOG_LEVEL = pLevel;
}

/**
  сохраняем ошбики из журнала в базу и очищаем бужер журнала
 */	
+ (void) SaveErrLog {
    if ([fLog length] > 0) {
        if ([DBHelper AddOutDataRec :C_INFO_LOG :[fLog dataUsingEncoding:NSUTF8StringEncoding] ] != -1) {
            [fLog setString:@""];
        }
    }
}

+ (NSString *) GetLogString {
    return [fLog copy];
}

+ (void) FreeLogBuffer {
    fLog = NULL;
}

/**
 @function Log_Internal Логирование и при необходимости сохранение записи в журнал для передачи на сервер
 @param pLevel Уровень критичности записи (от 0 - ошибки до 3 - спам)
 @param pClassTag Имя класса
 @param pProcName Имя метода или процедуры
 @param pMessage Сообщение
 */
+ (void) Log_Internal :(int)pLevel :(NSString *)pClassTag :(NSString *)pProcName :(NSString *)pMessage {
    if (pLevel <= C_CURRENT_LOG_LEVEL) {
        NSString *vMsg = [NSString stringWithFormat:@"%i %@ %@ %@", pLevel, pClassTag, pProcName, pMessage];
        NSLog(@"%@", vMsg);
        if ( [fLog length] <= C_LOG_MAX_LENGTH) {
            NSString *vLog = [NSString stringWithFormat:@"||%@,%@", [CSUtils Date2String:[NSDate dateWithTimeIntervalSinceNow:0]], vMsg];
            if (fLog == NULL) {
                fLog = [[NSMutableString alloc] init];
            }
            [fLog appendString:vLog];
        }
    }
}

/**
 @function Log_DEBUG Логирование и при необходимости сохранение записи в журнал для передачи на сервер
    Уровень критичности - минимальный
 @param pClassTag Имя класса
 @param pProcName Имя метода или процедуры
 @param pMessage Сообщение
 */
+ (void) Log_DEBUG :(NSString *)pClassTag :(NSString *)pProcName :(NSString *)pMessage {
    [self Log_Internal :C_LOG_LEVEL_DEBUG :pClassTag :pProcName :pMessage];
}

/**
 @function Log_MESSAGE Логирование и при необходимости сохранение записи в журнал для передачи на сервер
 Уровень критичности - обычный
 @param pClassTag Имя класса
 @param pProcName Имя метода или процедуры
 @param pMessage Сообщение
 */
+ (void) Log_MESSAGE :(NSString *)pClassTag :(NSString *)pProcName :(NSString *)pMessage {
    [self Log_Internal :C_LOG_LEVEL_MESSAGE :pClassTag :pProcName :pMessage];
}

/**
 @function Log_CRITICAL Логирование и при необходимости сохранение записи в журнал для передачи на сервер
 Уровень критичности - критичный
 @param pClassTag Имя класса
 @param pProcName Имя метода или процедуры
 @param pMessage Сообщение
 */
+ (void) Log_CRITICAL :(NSString *)pClassTag :(NSString *)pProcName :(NSString *)pMessage {
    [self Log_Internal :C_LOG_LEVEL_CRITICAL :pClassTag :pProcName :pMessage];
}

/**
 @function Log_ERROR Логирование и при необходимости сохранение записи в журнал для передачи на сервер
 Уровень критичности - ошибки
 @param pClassTag Имя класса
 @param pProcName Имя метода или процедуры
 @param pMessage Сообщение
 */
+ (void) Log_ERROR :(NSString *)pClassTag :(NSString *)pProcName :(NSString *)pMessage {
    [self Log_Internal :C_LOG_LEVEL_ERROR :pClassTag :pProcName :pMessage];
}



@end
