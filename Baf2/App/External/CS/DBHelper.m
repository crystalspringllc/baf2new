//
//  DBHelper.m
//  Работа с БД
//
//  Created by EArt on 21.07.11.
//  Copyright 2011 CS LLC. All rights reserved.
//
//  Работа с БД


#import "NSData+Base64.h"
#import "NSData+Compression.h"
#import "DBHelper.h"
#import "CSConst.h"
#import "LogHelper.h"
#import "FSHelper.h"
#import "SecurityHelper.h"

@implementation DBHelper

#define C_CLASS_TAG @"DBHelper"


/**
 @function openDB Открыть БД
 Если файл БД не найден (при первой инициализации или после очистки) то в Documents 
 копируется файл шаблона БД, и затем открывается
 @return Дескриптор открытой БД
 */
+ (sqlite3 *) OpenDB {
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"openDB" :@"start"];
    NSString *vDBFileName = [FSHelper getFileWithPath:C_DB_FILE_NAME];
    if (! [FSHelper isFileWithPathExists:vDBFileName]) {
        [LogHelper Log_CRITICAL :C_CLASS_TAG :@"openDB" :@"Mess=DB file not found!"];
        [FSHelper copyAssetFileToDocuments :C_DB_FILE_NAME :C_DB_FILE_NAME];      
    }
    sqlite3 *vDB;
    if (sqlite3_open([vDBFileName UTF8String], &vDB) != SQLITE_OK){
        [LogHelper Log_ERROR :C_CLASS_TAG :@"openDB" :[NSString stringWithFormat:@"eMess=%s", sqlite3_errmsg(vDB)]];
    };
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"openDB" :@"end"];
    return vDB;
}

/**
 @function closeDB Закрытие БД
 @param pDB дексриптор БД, полученный ранее с помощью openDB
 */
+ (void) CloseDB :(sqlite3 *)pDB {
    sqlite3_close(pDB);  
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"closeDB" :@""];
}


/**
 @function Vacuum Сжатие базы (по команде с сервера)
 */
+ (void) VacuumDB {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"Vacuum" :@"start"];
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "VACUUM";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        sqlite3_step(vStmt);
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"Vacuum" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"Vacuum" :@"end"];
    }
    [LogHelper Log_CRITICAL :C_CLASS_TAG :@"Vacuum" :@""];
}

/**
 @function getVar Прлучение из БД значения переменной по имени pVarName. Если переменная не найдена то возвращается значение по умолчанию pVarDefValue
 @param pVarName имя переменной
 @param pVarDefValue значение по умолчанию
 @return Значение переменной
 */
+ (NSString *) GetVar :(NSString *) pVarName :(NSString *) pVarDefValue {
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"getVar" :[NSString stringWithFormat:@"pVarName=%@, pVarDefValue=%@ - start", pVarName, pVarDefValue]];
    NSString *vResult = pVarDefValue;
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "SELECT V_VALUE FROM T_VARS WHERE V_NAME=?";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK) {
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_text(vStmt, 1, [pVarName UTF8String], -1, NULL);
        if (sqlite3_step(vStmt) != SQLITE_ROW) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%@", vSQL, C_ERROR_MSG_ROW_NOT_FOUND];
        }
        vResult = [NSString stringWithUTF8String:(char *)sqlite3_column_text(vStmt,0)];
    }
    @catch (NSException *exception) {
        [LogHelper Log_MESSAGE :C_CLASS_TAG :@"getVar" :[NSString stringWithFormat:@"pVarName=%@, pVarDefValue=%@, eName=%@, eReason=%@", pVarName, pVarDefValue, [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"getVar" :[NSString stringWithFormat:@"pVarName=%@, vResult=%@ - end", pVarName, vResult]];
        return vResult;
    }        
}

/**
 @function Получение строки JSON-массива наименований и значений переменных по заданому условия
 @param pMask - значение условия выбора переменный (like pMask)
 @return строка JSON-массива наименований и значений переменных
 */
+ (NSString *) GetVars :(NSString *) pMask {
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"getVars" :[NSString stringWithFormat:@"pMask=%@ - start", pMask]];
    NSString *vResult = @"{\"V\":[";
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "SELECT V_NAME, V_VALUE FROM T_VARS WHERE V_NAME like ?";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_text(vStmt, 1, [pMask UTF8String], -1, NULL);
        while (sqlite3_step(vStmt) == SQLITE_ROW) {       
            NSString *vName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(vStmt,0)];
            NSString *vValue = [NSString stringWithUTF8String:(char *)sqlite3_column_text(vStmt,1)];
            vName = [vName stringByReplacingOccurrencesOfString:@"\"" withString:@"`"];
            vValue = [vValue stringByReplacingOccurrencesOfString:@"\"" withString:@"`"];
            if ([vResult length] < 7 ) {
                vResult = [NSString stringWithFormat:@"%@[\"%@\",\"%@\"]", vResult, vName, vValue];
            } else {
                vResult = [NSString stringWithFormat:@"%@,[\"%@\",\"%@\"]", vResult, vName, vValue];
            }
        }        
        vResult = [NSString stringWithFormat:@"%@]}", vResult];
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"getVars" :[NSString stringWithFormat:@"pMask=%@, eName=%@, eReason=%@", pMask, [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"getVars" :[NSString stringWithFormat:@"pMask=%@, res_len=%ld - end", pMask, (long)[vResult length]]];
        return vResult;
    }        
}

/**
 @function setVar Установка значения переменной 
 @param pName - наименование переменной 
 @param pValue - значение переменной
 */
+ (void) SetVar :(NSString *) pVarName :(NSString *) pVarValue {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"setVar" :[NSString stringWithFormat:@"pVarName=%@, pVarValue=%@ - start", pVarName, pVarValue]];
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt_Upd = nil;
    sqlite3_stmt *vStmt_Ins = nil;
    @try {
        const char *vSQL_Upd = "UPDATE T_VARS SET V_VALUE=? WHERE V_NAME=?;";
        if (sqlite3_prepare_v2(vDB, vSQL_Upd, -1, &vStmt_Upd, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL_Upd, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_text(vStmt_Upd, 1, (char *) [pVarValue UTF8String], -1,  NULL);
        sqlite3_bind_text(vStmt_Upd, 2, (char *) [pVarName UTF8String], -1, NULL);
        if (sqlite3_step(vStmt_Upd) != SQLITE_DONE) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%s", vSQL_Upd, sqlite3_errmsg(vDB)];
        };
        if (sqlite3_changes(vDB) != 0) { // update is OK
            return;
        }
        const char *vSQL_Ins = "INSERT INTO T_VARS (V_NAME, V_VALUE) VALUES (?, ?);";
        if (sqlite3_prepare_v2(vDB, vSQL_Ins, -1, &vStmt_Ins, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL_Ins, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_text(vStmt_Ins, 1, (char *) [pVarName UTF8String], -1, NULL);
        sqlite3_bind_text(vStmt_Ins, 2, (char *) [pVarValue UTF8String], -1, NULL);
        if (sqlite3_step(vStmt_Ins) != SQLITE_DONE) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%s", vSQL_Ins, sqlite3_errmsg(vDB)];
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"setVar" :[NSString stringWithFormat:@"pVarName=%@, pVarValue=%@, eName=%@, eReason=%@" , pVarName, pVarValue, [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt_Upd) { 
            sqlite3_finalize(vStmt_Upd);
        }
        if (vStmt_Ins) { 
            sqlite3_finalize(vStmt_Ins);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_MESSAGE :C_CLASS_TAG :@"setVar" :[NSString stringWithFormat:@"pVarName=%@ - end", pVarName]];
    }
}

/**
 @function delVar Удаление переменной в БД 
 @param pVarName - наименование переменной 
 */
+ (void) DelVar :(NSString *)pVarName {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"delVar" :[NSString stringWithFormat:@"pVarName=%@ - start", pVarName]];
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "DELETE FROM T_VARS WHERE V_NAME=?;";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_text(vStmt, 1, (char *) [pVarName UTF8String], -1, NULL);
        if (sqlite3_step(vStmt) != SQLITE_DONE) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        if (sqlite3_changes(vDB) == 0) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%@", vSQL, C_ERROR_MSG_ROW_NOT_FOUND];
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"delVar" :[NSString stringWithFormat:@"pVarName=%@, eName=%@, eReason=%@" , pVarName, [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"delVar" :@"end"];
    }
}


/**
 @function InsOutDataRec_Internal Вставка записи в исходящую очередь для отправки на сервер (internal)
 @param pType - тип записи (см. описание в formats.rtf)
 @param pZipped - признак того что инфомрация сжата, T  или F
 @param pCrypted - признак того что инфомрация зашифрована индивидуальным ключем, T  или F
 @param pSign - цифровая подпись (имеется только в пользовательских сообщениях)
 @param pData - данные в Base64
 @return - ид вставленной записи, -1 в случае ошибки
 */
+ (long) AddOutDataRec_Internal :(NSString *) pType :(NSString *) pZipped :(NSString *) pCrypted :(NSString *) pSign :(NSString *) pData {
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"AddOutDataRec_Internal" :[NSString stringWithFormat:@"pType=%@ - start", pType]];
    long vResult = -1;
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "INSERT INTO T_OUT (OUT_TYPE, OUT_ZIPPED, OUT_CRYPTED, OUT_SIGN, OUT_DATA) VALUES (?,?,?,?,?);";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };        
        sqlite3_bind_text(vStmt, 1, (char *) [pType UTF8String], -1, NULL);
        sqlite3_bind_text(vStmt, 2, (char *) [pZipped UTF8String], -1, NULL);
        sqlite3_bind_text(vStmt, 3, (char *) [pCrypted UTF8String], -1, NULL);
        sqlite3_bind_text(vStmt, 4, (char *) [pSign UTF8String], -1, NULL);
        sqlite3_bind_text(vStmt, 5, (char *) [pData UTF8String], -1, NULL);
        if (sqlite3_step(vStmt) != SQLITE_DONE) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        }
        vResult =  (long) sqlite3_last_insert_rowid(vDB);
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"AddOutDataRec_Internal" :[NSString stringWithFormat:@"pType=%@, eName=%@, eReason=%@", pType, [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"AddOutDataRec_Internal" :[NSString stringWithFormat:@"pType=%@ - end", pType]];
    }
    return vResult;
}

/**
 @function AddOutDataRec
 Вставка записи в исходящую очередь для отправки на сервер (системные, служебные сообщения)
 Данные шифруются общим ключем
 @param pType - тип записи (см. описание в formats.rtf)
 @param pOutData - данные в Base64
 @return - ид вставленной записи, -1 в случае ошибки
 */
+ (int) AddOutDataRec :(NSString *) pType :(NSData *) pData {
    if (pData == NULL) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"AddOutDataRec" :[NSString stringWithFormat:@"pType=%@ pData=NULL - start", pType]];
        return -1;
    }
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"AddOutDataRec" :[NSString stringWithFormat:@"pType=%@ pData_len=%lu - start", pType, (unsigned long)[pData length]]];
    NSString *vZipped = @"F";
    NSData *vData = pData;  //[pData dataUsingEncoding:NSUTF8StringEncoding];
    if ([pData length] > 200) {
        vData = [vData gzipDeflate];
        vZipped = @"T";
    }
    vData = [SecurityHelper AES256EncryptWithCommonKey :vData];
    NSString *vB64Data = [vData base64EncodedString];
    
    long vResult = [self AddOutDataRec_Internal :pType :vZipped :@"F" :@"" :vB64Data];
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"AddOutDataRec" :[NSString stringWithFormat:@"vResult=%ld - end", vResult]];
    return (int)vResult;
}


/**
 @function AddDeviceOutDataRec Вставка записи в исходящую очередь для отправки на сервер (пользовательские сообщения)
 Данные шифруются индивидуальным ключем и подписываются пользовательским сертификатом
 Внимание! Должен быть доступен индивидуальный секретный ключ и приватный ключ, т.е. отправка 
 производится только из активной пользовательской web-сессии!
 @param pType - тип записи (см. описание в formats.rtf)
 @param pOutData - данные (Base64)
 @return - ид вставленной записи
 */
+ (int) AddDeviceOutDataRec :(NSString *) pType :(NSData *) pData {
    if (pData == NULL) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"AddDeviceOutDataRec" :[NSString stringWithFormat:@"pType=%@ pData=NULL - start", pType]];
        return -1;
    }
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"AddDeviceOutDataRec" :[NSString stringWithFormat:@"pType=%@ pData_len=%lu - start", pType, (unsigned long)[pData length]]];
    NSString *vZipped = @"F";
    NSData *vData = pData; // [pData dataUsingEncoding:NSUTF8StringEncoding];
    if ([pData length] > 200) {
        vData = [vData gzipDeflate];
        vZipped = @"T";
    }
    if ( ![SecurityHelper isUserSessionActive]) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"AddDeviceOutDataRec" :@"Session is inactive!"];
        return -1;
    }
    vData = [SecurityHelper AES256EncryptWithDeviceKey :vData];
    NSString *vB64Data = [vData base64EncodedString];
    
    NSString *vB64Sign = [SecurityHelper GetB64SignOfDataWithDevicePrivateKey :vData];
    long vResult = [self AddOutDataRec_Internal :pType :vZipped :@"T" :vB64Sign :vB64Data];
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"AddDeviceOutDataRec" :[NSString stringWithFormat:@"vResult=%ld - end", vResult]];
    return (int)vResult;
}

/**
 @function GetLastOutId Прлучение id последней записи, используется перед вызовом метода GetOutData, для исключения потери данных при параллельной обработке
 @return  id последней записи или -1
 */
+ (int) GetLastOutId {
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"GetLastOutId" :@"start"];
    int vResult = -1;
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "SELECT MAX(OUT_ID) FROM T_OUT";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        if (sqlite3_step(vStmt) == SQLITE_ROW) {
            vResult = sqlite3_column_int(vStmt, 0);
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"GetLastOutId" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"GetLastOutId" :[NSString stringWithFormat:@"vResult=%i - end", vResult]];
        return vResult;
    }        
}

/**
 @function IsOutIdExists Проверка наличия id последней записи с указнной датой формирования
 используется перед вызовом метода DelOutData (в SQLite при удалении всех записей нумерация 
 начинается сначала, поэтому уникальный ид = ид + дата)
 @param pLastId ид последней отправленной записи
 @param pRecDate - дата записи с ид отправленной записи 
 @return true or false
 */
+ (Boolean) IsOutIdExists :(int) pLastId  :(NSString *)pRecDate {
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"IsOutIdExists" :@"start"];
    Boolean vResult = false;
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "SELECT count(1) FROM T_OUT WHERE OUT_ID=? and datetime(OUT_DATE,'localtime')=?;";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        
        sqlite3_bind_int(vStmt, 1, pLastId);
        sqlite3_bind_text(vStmt, 2, (char *) [pRecDate UTF8String], -1, NULL);
        
        if (sqlite3_step(vStmt) == SQLITE_ROW) {
            int vI = sqlite3_column_int(vStmt, 0);
            // удаление записей до pLastId только в случае наличия pLastId+OUT_DATE!
            vResult = vI == 1;
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"IsOutIdExists" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"IsOutIdExists" :[NSString stringWithFormat:@"vResult=%i - end", vResult]];
        return vResult;
    }        
}

/**
 @function DelOutData Удаление отправленных записей из исходящей очереди 
 @param pLastId ид последней отправленной записи (получен перед отправкой с помощью метода GetLastOutId)
 */
+ (void) DelOutData :(int) pLastId {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"DelOutData" :[NSString stringWithFormat:@"pLastId=%i - start", pLastId]];
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "DELETE FROM T_OUT WHERE OUT_ID<=?;";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_int(vStmt, 1, pLastId);
        if (sqlite3_step(vStmt) != SQLITE_DONE) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"DelOutData" :[NSString stringWithFormat:@"pLastId=%i, eName=%@, eReason=%@", pLastId, [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"DelOutData" :@"end"];
    }
}

/**
 @function GetOutData Получение строки JSON-массива записей исходящей очереди (см. описание в formats.rtf)
 @param pLastId - ид последней записи (получен предварительно с помощью метода GetLastOutId)
 @return строка JSON-массива записей исходящей очереди
 */
+ (NSString *) GetOutData :(int) pLastId {
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"GetOutData" :[NSString stringWithFormat:@"pLastId=%i - start", pLastId]];
    NSString *vResult = NULL;
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "SELECT OUT_ID, OUT_TYPE, OUT_ZIPPED, OUT_CRYPTED, datetime(OUT_DATE,'localtime') OUT_DATE, OUT_SIGN, OUT_DATA FROM T_OUT WHERE OUT_ID<=? ORDER BY OUT_ID";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_int(vStmt, 1, pLastId);
        while (sqlite3_step(vStmt) == SQLITE_ROW) {    
            int vId = sqlite3_column_int(vStmt, 0);
            NSString *vType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(vStmt, 1)];
            NSString *vZipped = [NSString stringWithUTF8String:(char *)sqlite3_column_text(vStmt, 2)];
            NSString *vCrypted = [NSString stringWithUTF8String:(char *)sqlite3_column_text(vStmt, 3)];
            NSString *vDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(vStmt, 4)];
            NSString *vSign = [NSString stringWithUTF8String:(char *)sqlite3_column_text(vStmt, 5)];
            NSString *vData = [NSString stringWithUTF8String:(char *)sqlite3_column_text(vStmt, 6)];
            if (vResult == NULL) {
                vResult = [NSString stringWithFormat:@"{\"O\":[{\"R\":[\"%i\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\"]}", vId, vType, vZipped, vCrypted, vDate, vSign, vData];
            } else {
                vResult = [NSString stringWithFormat:@"%@,{\"R\":[\"%i\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\"]}", vResult, vId, vType, vZipped, vCrypted, vDate, vSign, vData];
            }
        }
        if (vResult != NULL) {
            vResult = [NSString stringWithFormat:@"%@]}", vResult];
        } 
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"GetOutData" :[NSString stringWithFormat:@"pLastId=%i, eName=%@, eReason=%@", pLastId, [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"GetOutData" :@"end"];
        return vResult;
    }        
}


/**
 @function updateUrl 
 @discussion Добавление или обновление адреса кэширующего сервера для id pUrlId
 @param pUrlId ид адреса
 @param pUrl адрес процедуры обмена
 */
+ (void) UpdateUrl :(int)pUrlId :(NSString *)pUrl {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"updateUrl" :[NSString stringWithFormat:@"pUrlId=%i, pUrl=%@ - start", pUrlId, pUrl]];
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt_Upd = nil;
    sqlite3_stmt *vStmt_Ins = nil;
    @try {
        const char *vSQL_Upd = "UPDATE T_URLS SET U_URL=?, U_ACCESS_DATE=? WHERE U_ID=?;";
        if (sqlite3_prepare_v2(vDB, vSQL_Upd, -1, &vStmt_Upd, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL_Upd, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_text(vStmt_Upd, 1, (char *) [pUrl UTF8String], -1, NULL);
        sqlite3_bind_double(vStmt_Upd, 2, [NSDate timeIntervalSinceReferenceDate]);
        sqlite3_bind_int(vStmt_Upd, 3, pUrlId);
        if (sqlite3_step(vStmt_Upd) != SQLITE_DONE) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%s", vSQL_Upd, sqlite3_errmsg(vDB)];
        };
        if (sqlite3_changes(vDB) != 0) { //update is OK
            return;
        }
        const char *vSQL_Ins = "INSERT INTO T_URLS (U_ID, U_URL, U_ACCESS_DATE) VALUES (?, ?, ?);";
        if (sqlite3_prepare_v2(vDB, vSQL_Ins, -1, &vStmt_Ins, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL_Ins, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_int(vStmt_Ins, 1, pUrlId);
        sqlite3_bind_text(vStmt_Ins, 2, (char *) [pUrl UTF8String], -1, NULL);
        sqlite3_bind_double(vStmt_Ins, 3, [NSDate timeIntervalSinceReferenceDate]);
        if (sqlite3_step(vStmt_Ins) != SQLITE_DONE) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%s", vSQL_Ins, sqlite3_errmsg(vDB)];
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"updateUrl" :[NSString stringWithFormat:@"pUrlId=%i, pUrl=%@, eName=%@, eReason=%@" , pUrlId, pUrl, [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt_Upd) { 
            sqlite3_finalize(vStmt_Upd);
        }
        if (vStmt_Ins) { 
            sqlite3_finalize(vStmt_Ins);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"updateUrl" :@"end"];
    }
    
}

/**
 @function setAccessDateOfUrl Обновление даты последнего успешного обращения к указанному кэширующему серверу 
 @param pDB дексриптор БД, полученный ранее с помощью openDB
 @return TRUE если все ОК, FALSE или exception если возникла ошибка
 */
+ (void) SetAccessDateOfUrl :(int)pUrlId {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"setLastUrl" :[NSString stringWithFormat:@"pUrlId=%i - start", pUrlId]];
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "UPDATE T_URLS SET U_ACCESS_DATE=? WHERE U_ID=?;";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        double vCurDate = [NSDate timeIntervalSinceReferenceDate];
        sqlite3_bind_double(vStmt, 1, vCurDate);
        sqlite3_bind_int(vStmt, 2, pUrlId);
        if (sqlite3_step(vStmt) != SQLITE_DONE) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        }
        if (sqlite3_changes(vDB) == 0) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%@", vSQL, C_ERROR_MSG_ROW_NOT_FOUND];
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"setLastUrl" :[NSString stringWithFormat:@"pUrlId=%i, eName=%@, eReason=%@" , pUrlId, [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"setLastUrl" :@"end"];
    }
}

/**
 @function Удаление из списка адресов кэширующих серверов указанного адреса
 @param pUrlId - ид адреса кэширующего сервера
 */
+ (void) DelUrl :(int)pUrlId {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"delUrl" :[NSString stringWithFormat:@"pUrlId=%i - start", pUrlId]];
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "DELETE FROM T_URLS WHERE U_ID=?;";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_int(vStmt, 1, pUrlId);
        if (sqlite3_step(vStmt) != SQLITE_DONE) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        if (sqlite3_changes(vDB) == 0) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%@", vSQL, C_ERROR_MSG_ROW_NOT_FOUND];
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"delUrl" :[NSString stringWithFormat:@"pUrlId=%i, eName=%@, eReason=%@" , pUrlId, [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"delUrl" :@"end"];
    }
}

/**
 @function getUrls Солучение списка адресов кжширующих серверов
 @return возвращаемый список NSMutableArray
 */
+ (NSMutableArray *) GetUrls {
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"getUrls" :@"start"];
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    NSMutableArray *vResult = [[NSMutableArray alloc] init];
    @try {
        //    sqlite3_exec(vDB, "delete from T_URLS", nil, nil, nil);
        const char *vSQL = "SELECT U_ID, U_URL FROM T_URLS ORDER BY U_ACCESS_DATE DESC";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        while (sqlite3_step(vStmt) == SQLITE_ROW) {       
            int vUrlId = sqlite3_column_int(vStmt,0);
            NSString *vUrl = [NSString stringWithUTF8String:(char *)sqlite3_column_text(vStmt,1)];
           // [vResult addObject:[NSNumber numberWithInt:vUrlId]];
           // [vResult addObject:vUrl];
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"getUrls" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        if ([vResult count] == 0){
            //[vResult addObject:[NSNumber numberWithInt:-6]];
            //[vResult addObject:C_DEFAULT_URL6];
            [vResult addObject:[NSNumber numberWithInt:-5]];
            [vResult addObject:C_DEFAULT_URL5];
            [vResult addObject:[NSNumber numberWithInt:-4]];
            [vResult addObject:C_DEFAULT_URL4];
            [vResult addObject:[NSNumber numberWithInt:-3]];
            [vResult addObject:C_DEFAULT_URL1];
            [vResult addObject:[NSNumber numberWithInt:-2]];
            [vResult addObject:C_DEFAULT_URL2];
            [vResult addObject:[NSNumber numberWithInt:-1]];
            [vResult addObject:C_DEFAULT_URL1];
            [LogHelper Log_DEBUG :C_CLASS_TAG :@"getUrls" :@"Msg=Add default Urls"];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"getUrls" :[NSString stringWithFormat:@"vRecsCount=%lu - end", (unsigned long)[vResult count]]];
        return vResult;
    }
}




/**
 @function updatePack 
 @discussion Добавление или обновление адреса пакета в очереди для загрузки 
 @param pPackId ид адреса
 @param pUrl адрес пакета
 */
+ (BOOL) UpdatePack :(int)pPackId :(NSString *)pUrl {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"updatePack" :[NSString stringWithFormat:@"pPackId=%i, pUrl=%@ - start", pPackId, pUrl]];
    BOOL vResult = FALSE;
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt_Upd = nil;
    sqlite3_stmt *vStmt_Ins = nil;
    @try {
        const char *vSQL_Upd = "UPDATE T_PACK SET P_URL=?, P_STATUS='N' WHERE P_ID=?;";
        if (sqlite3_prepare_v2(vDB, vSQL_Upd, -1, &vStmt_Upd, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL_Upd, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_text(vStmt_Upd, 1, (char *) [pUrl UTF8String], -1, NULL);
        sqlite3_bind_int(vStmt_Upd, 2, pPackId);
        if (sqlite3_step(vStmt_Upd) != SQLITE_DONE) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%s", vSQL_Upd, sqlite3_errmsg(vDB)];
        };
        if (sqlite3_changes(vDB) != 0) { //update is OK
            vResult = TRUE;
        } else {
            const char *vSQL_Ins = "INSERT INTO T_PACK (P_ID, P_URL, P_STATUS) VALUES (?, ?, 'N');";
            if (sqlite3_prepare_v2(vDB, vSQL_Ins, -1, &vStmt_Ins, nil) != SQLITE_OK){
                [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL_Ins, sqlite3_errmsg(vDB)];
            };
            sqlite3_bind_int(vStmt_Ins, 1, pPackId);
            sqlite3_bind_text(vStmt_Ins, 2, (char *) [pUrl UTF8String], -1, NULL);
            if (sqlite3_step(vStmt_Ins) != SQLITE_DONE) {
                [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%s", vSQL_Ins, sqlite3_errmsg(vDB)];
            }
            vResult = TRUE;
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"updatePack" :[NSString stringWithFormat:@"pId=%i, pUrl=%@, eName=%@, eReason=%@" , pPackId, pUrl, [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt_Upd) { 
            sqlite3_finalize(vStmt_Upd);
        }
        if (vStmt_Ins) { 
            sqlite3_finalize(vStmt_Ins);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"updatePack" :@"end"];
    }
    return vResult;
}


/**
 @function delPack Удаление из списка адресов пакетов очереди загрузки указанного адреса
 @param pPackId - ид адреса пакета
 */
+ (BOOL) DelPack :(int)pPackId {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"delPack" :[NSString stringWithFormat:@"pPackId=%i - start", pPackId]];
    BOOL vResult = FALSE;    
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "DELETE FROM T_PACK WHERE P_ID=?;";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_int(vStmt, 1, pPackId);
        if (sqlite3_step(vStmt) != SQLITE_DONE) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        if (sqlite3_changes(vDB) == 0) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%@", vSQL, C_ERROR_MSG_ROW_NOT_FOUND];
        }
        vResult = TRUE;
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"delPack" :[NSString stringWithFormat:@"pPackId=%i, eName=%@, eReason=%@" , pPackId, [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"delPack" :@"end"];
    }
    return vResult;
}



/**
 @function setStatusOfPack Обновление даты последнего успешного обращения к указанному кэширующему серверу 
 @param pDB дексриптор БД, полученный ранее с помощью openDB
 @param pPackId - ид пакета
 @param pStatus - статус (N - не загружен, P - загружен
 @return TRUE если все ОК, FALSE или exception если возникла ошибка
 */
+ (BOOL) SetStatusOfPack :(int)pPackId :(NSString *)pStatus {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"setStatusOfPack" :[NSString stringWithFormat:@"pPackId=%i - start", pPackId]];
    BOOL vResult = FALSE;
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "UPDATE T_PACK SET P_STATUS=? WHERE P_ID=?;";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_text(vStmt, 1, (char *) [pStatus UTF8String], -1, NULL);
        sqlite3_bind_int(vStmt, 2, pPackId);
        if (sqlite3_step(vStmt) != SQLITE_DONE) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        }
        if (sqlite3_changes(vDB) == 0) {
            [NSException raise:C_ERROR_MSG_SQL_EXEC format:@" sql:%s err:%@", vSQL, C_ERROR_MSG_ROW_NOT_FOUND];
        }
        vResult = TRUE;
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"setStatusOfPack" :[NSString stringWithFormat:@"pPackId=%i, pStatus=%@, eName=%@, eReason=%@" , pPackId, pStatus, [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"setStatusOfPack" :@"end"];
    }
    return vResult;
}


/**
 @function GetStatusOfPack Прлучение статуса загружки пакета по ид пакета
 @return N или P или null если не строка не найдена
 */
+ (NSString *) GetStatusOfPack :(int)pPackId {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"GetStatusOfPack" :[NSString stringWithFormat:@"pPackId=%i - start", pPackId]];
    NSString *vResult = NULL;
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = "SELECT P_STATUS FROM T_PACK WHERE P_ID=?";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        sqlite3_bind_int(vStmt, 1, pPackId);
        if (sqlite3_step(vStmt) == SQLITE_ROW) {
            vResult = [NSString stringWithUTF8String :(char *)sqlite3_column_text(vStmt, 0)];
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"GetStatusOfPack" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"GetStatusOfPack" :[NSString stringWithFormat:@"pPackId=%i, vResult=%@ - end", pPackId, vResult]];
        return vResult;
    }        
}


/**
 @function GetPackUrls Получение списка адресов пакетов которые нужно загрузить
 @return возвращаемый список NSMutableDictionary
 */
+ (NSMutableArray *) GetPackUrls {
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"GetPackUrls" :@"start"];
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    NSMutableArray *vResult = [[NSMutableArray alloc] init];
    @try {
        const char *vSQL = "SELECT P_ID, P_URL FROM T_PACK WHERE P_STATUS='N' ORDER BY P_ID";
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK){
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        while (sqlite3_step(vStmt) == SQLITE_ROW) {
            int vUrlId = sqlite3_column_int(vStmt,0);
            NSString *vUrl = [NSString stringWithUTF8String:(char *)sqlite3_column_text(vStmt,1)];
            [vResult addObject:[NSNumber numberWithInt:vUrlId]];
            [vResult addObject:vUrl];
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"GetPackUrls" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"GetPackUrls" :@"end"];
        return vResult;
    }
}

/**
 * @function ExecSQL Выполнение любого SQL-запроса без возврата результата
 * @param pSQL текст запроса
 * @return int -2 = ошибка получения дескриптора БД, -1 = ошибка выполнения запроса, =0 - без ошибок 
 * Примеры запросов: 
 * create table cc (f1 integer, f2 datetime, f3 varchar(20))
 * insert into cc values (1, now(), 'ssss')
 * delete from cc where f1=1
 * drop table cc
 */
+ (NSString *) ExecSQL :(NSString *)pSQL {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"ExecSQL" :[NSString stringWithFormat:@"pSQL=%@ - start", pSQL]];
    NSString *vResult = @"-2";
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = [pSQL UTF8String];
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK) {
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        if (sqlite3_step(vStmt) == SQLITE_DONE) {
            vResult = @"0";
        }        
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"ExecSQL" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
        vResult = @"-1";
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"ExecSQL" :[NSString stringWithFormat:@"pSQL=%@, vResult=%@ - end", pSQL, vResult]];
        return vResult;
    }        
}

/**
 * @function ExecSQL_Select Выполнение любого SQL-запроса select c возвращением результата одной строкой JSON. 
 * Select должен возвращать одну строковую колонку! 
 * @param pSQL - текст SQL запроса SELECT
 * @return string - результат выполнения запроса в виде JSON. 
 * в формате {"R":[a1,a2,...,aN]} где a1..aN - значения колонки для записей 1..N
 * select '{"f1":"' || f1 || '","f2":"' || datetime(f2,'localtime') || '"}' from cc where f3 like '%'
 * Пример запроса: 
 * select '{"fname1":"' || field1 || '","fname2":"' || datetime(field2_date,'localtime') || '"}' from table_name where field1 > 12
 * 
 * Если при выполнении запроса возникает ошибка, результат выдается в виде {"E":["текст ошибки"]}. 
 * Пустой результат возвращается в виде {"R":[]}
 *  
 * Документация по SQLLite: 
 * http://www.sqlite.org/docs.html  
 * http://www.sqlite.org/lang.html
 */
+ (NSString *) ExecSQL_Select :(NSString *)pSQL {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"ExecSQL_Select" :[NSString stringWithFormat:@"pSQL=%@ - start", pSQL]];
    NSString *vResult = @"-2";
    sqlite3 *vDB = [self OpenDB];
    sqlite3_stmt *vStmt = nil;
    @try {
        const char *vSQL = [pSQL UTF8String];
        if (sqlite3_prepare_v2(vDB, vSQL, -1, &vStmt, nil) != SQLITE_OK) {
            [NSException raise:C_ERROR_MSG_SQL_PREPARE format:@" sql:%s err:%s", vSQL, sqlite3_errmsg(vDB)];
        };
        vResult = @"{\"R\":[";
        while (sqlite3_step(vStmt) == SQLITE_ROW) {
            NSString *vRec = [NSString stringWithUTF8String:(char *)sqlite3_column_text(vStmt, 0)];
            //??            vRec = [vRec stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
            if ([vResult length] > 6) {
                vResult = [vResult stringByAppendingString:@","];
            }
            vResult = [vResult stringByAppendingString:vRec];
        }
        vResult = [vResult stringByAppendingString:@"]}"];
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"ExecSQL_Select" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
        vResult = [NSString stringWithFormat:@"{\"E\":[\"%@,%@\"]}", [exception name], [exception reason]];
    }
    @finally {
        if (vStmt) { 
            sqlite3_finalize(vStmt);
        }
        if (vDB) {
            [self CloseDB:vDB];
        }
        [LogHelper Log_DEBUG :C_CLASS_TAG :@"ExecSQL_Select" :[NSString stringWithFormat:@"pSQL=%@, vResult=%@ - end", pSQL, vResult]];
        return vResult;
    }        
}


@end
