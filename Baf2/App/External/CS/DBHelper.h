//
//  DBHelper.h
//  CS
//
//  Created by EArt on 21.07.11.
//  Copyright 2011 CS LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"


@interface DBHelper : NSObject {
    
}
+ (sqlite3 *) OpenDB;
+ (void) CloseDB :(sqlite3 *)pDB;
+ (NSString *) GetVar :(NSString *) pVarName :(NSString *) pVarDefValue;
+ (NSString *) GetVars :(NSString *) pMask;
+ (void) SetVar :(NSString *) pVarName :(NSString *) pVarValue;
+ (void) DelVar :(NSString *)pVarName;
+ (int) AddOutDataRec :(NSString *) pType :(NSData *) pData;
+ (int) AddDeviceOutDataRec :(NSString *) pType :(NSData *) pData;
+ (int) GetLastOutId;
+ (void) DelOutData :(int) pLastId;
+ (NSString *) GetOutData :(int) pLastId;
+ (Boolean) IsOutIdExists :(int) pLastId  :(NSString *)pRecDate;
+ (void) UpdateUrl :(int)pUrlId :(NSString *)pUrl;
+ (void) SetAccessDateOfUrl :(int)pUrlId;
+ (void) DelUrl :(int)pUrlId;
+ (NSMutableArray *) GetUrls;
+ (void) VacuumDB;
+ (BOOL) UpdatePack :(int)pPackId :(NSString *)pUrl;
+ (BOOL) DelPack :(int)pPackId;
+ (BOOL) SetStatusOfPack :(int)pPackId :(NSString *)pStatus;
+ (NSString *) GetStatusOfPack :(int)pPackId;
+ (NSMutableArray *) GetPackUrls;
+ (NSString *) ExecSQL :(NSString *)pSQL;
+ (NSString *) ExecSQL_Select :(NSString *)pSQL;

@end
