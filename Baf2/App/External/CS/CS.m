//
//  CS.m
//  BAF
//
//  Created by Almas Adilbek on 1/14/15.
//
//

#import "CS.h"

@implementation CS

/**
* Вызываем этот метод в AppDelegate в методе didFinishLaunchingWithOptions, который, установит сервер AppID и начнет закачку \ обновление файлов.
*/
+ (void)startWithServerAppID:(NSString *)serverAppID
{
    // Set server app ID
    [NetHelper useServerAppID:serverAppID];

    if ([FSHelper isFileExists:C_DB_FILE_NAME]) {
    } else {

        // файла БД еще нет, значит это первоначальный запуск:
        // устанавливаем уазатель на класс для обновления стартовой страницы после синхронизации:
        // копируем файлы из Assets
        [FSHelper copyAssetFilesToDocuments];
        [SecurityHelper SetSrvPublicKey:C_SRV_PUBLIC_KEY];
        [DBHelper SetVar:C_VAR_SYNC_ID :@"0"];
        // и сразу добавляем в новую базу в исходящую очередь запись с информацией об устройстве
    }
    [CSUtils CheckDevInfo];
    //[self gotoPage :vStartUrl];
    // регистрация для получения push notifications:
//	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];

    [NetHelper SyncStartAsThread];
}


@end
