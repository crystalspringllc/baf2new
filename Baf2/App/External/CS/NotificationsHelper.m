//
//  NotificationsHelper.m
//  CSInfo
//
//  Created by EArt on 05.10.11.
//  Copyright 2011 CS LLC. All rights reserved.
//

#import "NotificationsHelper.h"
#import "LogHelper.h"


@implementation NotificationsHelper

#define C_CLASS_TAG @"NotificationsHelper"

/**
 @function AddLocalNotif добавить локальное отложенное уведомление
 @param 
 @return 
 */
+ (void)AddLocalNotif :(int)pBundleNumber :(NSDate *)pDate :(NSString *)pAlertHeader :(NSString *)pAlertMessage :(NSString *)pUrl {
//    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
//    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
//    [dateComps setDay:27];
//    [dateComps setMonth:7];
//    [dateComps setYear:2011];
//    [dateComps setHour:16];
//    [dateComps setMinute:35];
//    NSDate *itemDate = [calendar dateFromComponents:dateComps];
//    [dateComps release];
    
    UILocalNotification *vLocalNotif = [[UILocalNotification alloc] init];
    @try {
        vLocalNotif.fireDate = pDate;
        vLocalNotif.timeZone = [NSTimeZone defaultTimeZone];
        vLocalNotif.alertBody = pAlertMessage;
        vLocalNotif.alertAction = NSLocalizedString(pAlertHeader, nil);
        vLocalNotif.soundName = UILocalNotificationDefaultSoundName;
        vLocalNotif.applicationIconBadgeNumber = pBundleNumber;
        vLocalNotif.userInfo = [NSDictionary dictionaryWithObject:pUrl forKey:@"url"];
        [[UIApplication sharedApplication] scheduleLocalNotification:vLocalNotif];
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"AddLocalNotif" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    @finally {
        vLocalNotif = nil;
    }
    
    
    //    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    //    if (localNotif) {
    //        localNotif.alertBody = [NSString stringWithFormat:
    //                                NSLocalizedString(@"%@ has a message for you.", nil), @" friend"];
    //        localNotif.alertAction = NSLocalizedString(@"Read Message", nil);
    //        localNotif.soundName = @"alarmsound.caf";
    //        localNotif.applicationIconBadgeNumber = 4;
    //        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
    //        [localNotif release];
    //    }
}

/**
 @function CancelAllNotifs отменить все уведомления
 */
+ (void) CancelAllNotifs {
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

+ (void) DelNotify :(int)pNotifyId {
    // у локальных нотификаций нет id! 
    [self CancelAllNotifs];

    // !!! добавить id и сделать перебор в цикле:
    
    //    NSString *myIDToCancel = @"some_id_to_cancel";
    //    UILocalNotification *notificationToCancel=nil;
    //    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
    //        if([aNotif.userInfo objectForKey:@"ID"] isEqualToString:myIDToCancel]) {
    //            notificationToCancel=aNotif;
    //            break;
    //        }
    //    }
    //    [[UIApplication sharedApplication] cancelLocalNotification:notificationToCancel];

}

@end
