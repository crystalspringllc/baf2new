//
//  FSHelper.h
//  CS
//
//  Created by EArt on 21.07.11.
//  Copyright 2011 CS LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FSHelper : NSObject {
    
}

+ (NSString *) getDocumentsDir;
+ (NSString *) getFileWithPath: (NSString *)pFileName;
+ (BOOL) isFileExists : (NSString *) pFileName;
+ (BOOL) isFileWithPathExists : (NSString *) pFileName;
+ (BOOL) delFileWithPath :(NSString *)pFileName;
+ (BOOL) delFile: (NSString *) pFileName;
+ (BOOL) saveFile :(NSString *) pFileName :(NSData *) pFileData;
+ (NSData *) loadFile :(NSString *) pFileName;
+ (BOOL) createSubDirsIfNeed :(NSString *) pFileName;
+ (BOOL) updateFile :(NSString *)pFileName :(NSData *)pFileData;
+ (BOOL) copyAssetFileToDocuments :(NSString *)pAssetFileName :(NSString *)pDocFileName;
+ (BOOL) copyAssetFilesToDocuments;
+ (BOOL) beforeOpenDBFile :(NSString *)pFileName;

@end
