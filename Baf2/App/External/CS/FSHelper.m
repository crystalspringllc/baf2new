//
//  FSHelper.m
//  CS
//
//  Created by EArt on 21.07.11.
//  Copyright 2011 CS LLC. All rights reserved.
//  
//  Работа с файловой системой

#import "FSHelper.h"
#import "LogHelper.h"
#import "CSUtils.h"
#import "CSConst.h"
#include <sys/xattr.h>

@implementation FSHelper


#define C_CLASS_TAG @"FSHelper"

//  http://stackoverflow.com/questions/9620651/use-nsurlisexcludedfrombackupkey-without-crashing-on-ios-5-0
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
#ifndef NSURLIsExcludedFromBackupKey                   
    // iOS <= 5.0.1.
    const char* filePath = [[URL path] fileSystemRepresentation];
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;    
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return result == 0;
#else                                                                       
    // iOS >= 5.1
    // First try and remove the extended attribute if it is present
    int result = getxattr(filePath, attrName, NULL, sizeof(u_int8_t), 0, 0);
    if (result != -1) {
        // The attribute exists, we need to remove it
        int removeResult = removexattr(filePath, attrName, 0);
        if (removeResult == 0) {
            NSLog(@"Removed extended attribute on file %@", URL);
        }
    }
    return [URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:nil];
#endif
}

/**
 @function addDoNotBackutAttrToFileWithPath Установка файлу с путем pFileName признака Do Not Backup
 */
//проверить!!!
+ (void) addDoNotBackutAttrToFileWithPath : (NSString *)pFileNameWithPath {
    NSURL *vURL = [NSURL fileURLWithPath :pFileNameWithPath];
    [self addSkipBackupAttributeToItemAtURL:vURL];
    //Err in iOS<5?    [vURL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:NULL];
}

/**
 @function getDocumentsDir Получение пути к папке документов приложения 
 @return строка - путь 
 */
+ (NSString *) getDocumentsDir {
    NSArray *vPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); 
//    NSArray *vPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES); // EArt before 16 10 2012 ??
    NSString *vResult = [vPaths objectAtIndex:0];
    return vResult;
}


/**
 @function getFileWithPath Получение полного пути к файлу по имени файла (добавлеяется путь к Documents\)
 @param pFileName имя файла
 @return строка - путь и имя файла
 */
+ (NSString *) getFileWithPath: (NSString *)pFileName {
    NSString *vResult = [[self getDocumentsDir] stringByAppendingPathComponent:pFileName];
//    [LogHelper Log_DEBUG :C_CLASS_TAG :@"getFileWithPath" :[NSString stringWithFormat:@"pFileName=%@, Result=%@", pFileName, vResult]];
    return vResult;
}

/**
 @function isFileExists Проверка наличия файла (в pFileName должно быть с относительным путем начиная с Documents)
 @param pFileName имя файла
 @return TRUE если все файл имеется, FALSE если нет
 */
+ (BOOL) isFileExists : (NSString *) pFileName {
    BOOL vResult = [[NSFileManager defaultManager] fileExistsAtPath:[FSHelper getFileWithPath:pFileName] isDirectory:false];
//    [LogHelper Log_DEBUG :C_CLASS_TAG :@"isFileExists" :[NSString stringWithFormat:@"pFileName=%@, Result=%i", pFileName, vResult]];
    return vResult;
}

/**
 @function isFileExists Проверка наличия файла (в pFileName должно быть имя с полным путем)
 @param pFileName имя файла
 @return TRUE если все файл имеется, FALSE если нет
 */
+ (BOOL) isFileWithPathExists : (NSString *) pFileName {
    BOOL vResult = [[NSFileManager defaultManager] fileExistsAtPath:pFileName isDirectory:false];
    //    [LogHelper Log_DEBUG :C_CLASS_TAG :@"isFileExists" :[NSString stringWithFormat:@"pFileName=%@, Result=%i", pFileName, vResult]];
    return vResult;
}

/**
 @function delFileWithPath Удаление файла (в pFileName должно быть имя с полным путем)
 @param pFileName имя файла
 @return TRUE если все файл имеется, FALSE если нет
 */
// delete file, pFileName must include full path!
+ (BOOL) delFileWithPath :(NSString *)pFileName {
    if (![self isFileWithPathExists:pFileName]){
        return FALSE;
    }
    NSError *vError = nil;
    [[NSFileManager defaultManager] removeItemAtPath:pFileName error:&vError];
    if(vError) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"delFileWithPath" :[NSString stringWithFormat:@"pFileName=%@, eCode=%li, eMess=%@", pFileName, (long)[vError code], [vError localizedDescription]]];
        return FALSE;
    }
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"delFileWithPath" :[NSString stringWithFormat:@"pFileName=%@, Result=ok", pFileName]];
    return TRUE;
}

/**
 @function delFile Удаление файла (в pFileName должно быть имя без пути к нему, удаление производится из папки Documents приложения)
 @param pFileName имя файла
 @return TRUE если все файл имеется, FALSE если нет
 */
// delete file, pFileName must not include path, file name only!
+ (BOOL) delFile: (NSString *) pFileName {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"delFile" :[NSString stringWithFormat:@"pFileName=%@", pFileName]];
    NSString *vFileNameWithPath = [self getFileWithPath:pFileName];
    return [self delFileWithPath:vFileNameWithPath];
}




/**
 @function saveFile Создание или пересоздание файла с именем pFileName и содержимым pFileData
    (в pFileName должно быть имя с полным путем)
 @param pFileName имя файла c путем
 @return TRUE если все ОК, FALSE или exception если возникла ошибка
 */
+ (BOOL) saveFile :(NSString *) pFileName :(NSData *) pFileData {
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"saveFile" :[NSString stringWithFormat:@"pFileName=%@, Len=%lu", pFileName, (unsigned long)[pFileData length]]];
    if ([pFileData writeToFile:pFileName atomically:NO]){
        [LogHelper Log_MESSAGE :C_CLASS_TAG :@"saveFile" :[NSString stringWithFormat:@"pFileName=%@, Result=ok", pFileName]];
        return TRUE;
    };
    
    // EArt 16 10 2012 - установка признака Do Not Backup
    [self addDoNotBackutAttrToFileWithPath:pFileName];
    
    [LogHelper Log_ERROR :C_CLASS_TAG :@"saveFile" :[NSString stringWithFormat:@"pFileName=%@, eCode=-1, eMess=Unknown!", pFileName]];
    return FALSE;
}

/**
 @function loadFile Чтение содержимого файла с именем pFileData
 (в pFileName должно быть имя с полным путем)
 @param pFileName имя файла c путем
 @return Данные или exception если возникла ошибка
 */
+ (NSData *) loadFile :(NSString *) pFileName {
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"loadFile" :[NSString stringWithFormat:@"pFileName=%@", pFileName]];
    NSData *vResult = [NSData dataWithContentsOfFile:pFileName];
//    NSError *vError = nil; 
//    NSData *vResult = [NSData dataWithContentsOfFile:pFileName options:nil error:&vError];
//    if (vError != nil) {
//         [NSException raise:@"FS Error" format:@" Domain:%s err:%s code:%i", [vError domain], [vError localizedDescription], [vError code]];
//    };
//    [LogHelper Log_ERROR :C_CLASS_TAG :@"loadFile" :[NSString stringWithFormat:@"pFileName=%@, eCode=-1, eMess=Unknown!", pFileName]];
    return vResult;
}

/**
 @function createSubDirsIfNeed Создание если еще не созданы поддпиректорий в пути начиная от Documents
 @param pFileName имя файла с косвенным путем начиная от Documents
 @return TRUE если все ОК, FALSE или exception если возникла ошибка
 */
+ (BOOL) createSubDirsIfNeed :(NSString *) pFileName {
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"createSubDirsIfNeed" :[NSString stringWithFormat:@"pFileName=%@", pFileName]];
    NSRange vRange = [pFileName rangeOfString:@"/" options:NSBackwardsSearch];
    if (vRange.length != 0) {
        NSString *vDirName = [pFileName substringToIndex:vRange.location];
        NSString *vDirPath = [[self getDocumentsDir] stringByAppendingPathComponent:vDirName];
        if ( ![[NSFileManager defaultManager] fileExistsAtPath:vDirPath] ){
//            [LogHelper Log_MESSAGE :C_CLASS_TAG :@"createSubDirsIfNeed" :[NSString stringWithFormat:@"vDirName=%@, Mess=dir not exists", vDirName]];
            NSError *vError = nil;
            [[NSFileManager defaultManager] createDirectoryAtPath:vDirPath withIntermediateDirectories:YES attributes:nil error:&vError];
            if (vError) {
                [LogHelper Log_ERROR :C_CLASS_TAG :@"createSubDirsIfNeed" :[NSString stringWithFormat:@"vDirName=%@, eCode=%li, eMess=%@", vDirName, (long)[vError code], [vError localizedDescription]]];
                return FALSE;
            }
            [LogHelper Log_MESSAGE :C_CLASS_TAG :@"createSubDirsIfNeed" :[NSString stringWithFormat:@"vDirName=%@, Result=ok", vDirName]];
        }
    }
    return TRUE;
}

/**
 @function updateFile Обновление файла в Documents. Если уже имеется - удаляется, потом cоздается  заново. В имени файла может быть указан косвенный путь начиная от Documents, В этом случае поддиректория проверяется на наличие и при необходимости создаются
 @param pFileName - только имя файла, может быть с косвенным путем начиная от Documents 
 @param pFileData -  данные для записи
 @return TRUE если все ОК, FALSE или exception если возникла ошибка
 
 // EArt 16 10 2012  - для файлов БД   (с разширением .sqlite) файлы создаются с добавлением к расширению подстроки ".new", и потом при открытии базы переименовываются в .sqlite
 
 */
+ (BOOL) updateFile :(NSString *)pFileName :(NSData *)pFileData {
    if (pFileData == NULL) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"updateFile" :[NSString stringWithFormat:@"pFileName=%@, pFileData is NULL!", pFileName]];
        return FALSE;        
    }
    
    // EArt 16 10 2012  - для файлов БД:
    //!!!  проверить!    
    NSString *vFileName = NULL;
    if ([CSUtils strPos :vFileName :@".sqlite"] == -1){
        vFileName = pFileName;
    } else {
        vFileName = [NSString stringWithFormat:@"%@.tmp", pFileName];
    }

//    vFileName = [pFileName stringByReplacingOccurrencesOfString:@".." withString:@"/"];
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"updateFile" :[NSString stringWithFormat:@"pFileName=%@, Len=%lu - start", vFileName, (unsigned long)[pFileData length] ]];
    [self createSubDirsIfNeed:vFileName];
    NSString *vFileNameWithPath = [self getFileWithPath:vFileName];
    [self delFileWithPath:vFileNameWithPath];
    [self saveFile:vFileNameWithPath :pFileData];
    
    // EArt 16 10 2012 - установка признака Do Not Backup
    [self addDoNotBackutAttrToFileWithPath:vFileNameWithPath];
    
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"updateFile" :@"end"];
    return TRUE;
}



/** // EArt 16 10 2012
 @function renameFileWithPath Переименование файла pFromFileName в pToFileName (оба имени должны быть с полным путем)
 @param pFromFileName имя старого файла
 @param pToFileName имя нового файла
 @return TRUE если все ok 
 */
+ (BOOL) renameFileWithPath :(NSString *)pFromFileName :(NSString *)pToFileName{
    //!!!  проверить!
    NSError *vError = nil;
    [[NSFileManager defaultManager] moveItemAtPath:pFromFileName toPath:pToFileName error:&vError];
    if(vError) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"renameFileWithPath" :[NSString stringWithFormat:@"pFromFileName=%@, pToFileName=%@, eCode=%li, eMess=%@", pFromFileName, pToFileName, (long)[vError code], [vError localizedDescription]]];
        return FALSE;
    }
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"renameFileWithPath" :[NSString stringWithFormat:@"pFromFileName=%@, pToFileName=%@, Result=ok", pFromFileName, pToFileName]];
    return TRUE;
}

/**
 @function beforeOpenDBFile Обновление имени файла БД в Documents. Метод вызывается перед открытием БД, когда еще файл незаблокирован. Файлы БД при приеме сохраняются с добавочным расширением .tmp, а вэтом методе добавочное расширение убирается.
 @param pFileName - только имя файла БД, может быть с косвенным путем начиная от Documents 
 @return TRUE если все ОК, FALSE или exception если возникла ошибка
 EArt 16 10 2012  - для файлов БД   (с раcширением .sqlite) файлы создаются с добавлением к расширению подстроки ".new", и потом при открытии базы переименовываются в .sqlite
 */
+ (BOOL) beforeOpenDBFile :(NSString *)pFileName {
    //!!!  проверить!
    BOOL vResult = NO;
    @try{
        NSString *vTmpFileNameWithPath = [self getFileWithPath :[NSString stringWithFormat:@"%@.tmp", pFileName]];
        NSString *vFileNameWithPath = [self getFileWithPath :pFileName];
        if ([self isFileWithPathExists:vTmpFileNameWithPath]){
            [self delFileWithPath:vFileNameWithPath];
            vResult = [self renameFileWithPath:vTmpFileNameWithPath :vFileNameWithPath];
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"beforeOpenDBFile" :[NSString stringWithFormat:@"pFileName=%@, eName=%@, eReason=%@", pFileName, [exception name], [exception reason]] ];
    }
    vResult = YES;
}

/**
 @function copyAssetFileToDocuments Копирование файла из папки Assets в Documents
 @param pAssetFileName имя файла без пути в папке Assets
 @param pDocFileName имя файла без пути в папке Documents
 @return TRUE если все ОК, FALSE или exception если возникла ошибка
 */
+ (BOOL) copyAssetFileToDocuments :(NSString *)pAssetFileName :(NSString *)pDocFileName {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"copyAssetFileToDocuments" :[NSString stringWithFormat:@"pAssetFileName=%@, pDocFileName=%@", pAssetFileName, pDocFileName]];
    NSString *vDocFileName = [self getFileWithPath:pDocFileName];
    NSString *vAssetFileName = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:pAssetFileName];
    [self delFileWithPath:vDocFileName];
    NSError *vError = nil;
    [[NSFileManager defaultManager] copyItemAtPath:vAssetFileName toPath:vDocFileName error:&vError];    
    if (vError) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"copyAssetFileToDocuments" :[NSString stringWithFormat:@"pAssetFileName=%@, pDocFileName=%@, eCode=%li, eMess=%@", pAssetFileName, pDocFileName, (long)[vError code], [vError localizedFailureReason]]];
        return FALSE;
    }
    // EArt 16 10 2012 - установка признака Do Not Backup
    [self addDoNotBackutAttrToFileWithPath:vDocFileName];
    return TRUE;
}

/**
 @function copyAssetFilesToDocuments Копирование всех файлов из папки assets в Documents 
    (кроме служебных файлов Info.plist, MainWindow.nib)
 @return TRUE если все ОК, FALSE или exception если возникла ошибка
 */
+ (BOOL) copyAssetFilesToDocuments {
    [LogHelper Log_MESSAGE :C_CLASS_TAG :@"copyAssetFilesToDocuments" :@"- start"];
    NSString *vAssetDir = [[NSBundle mainBundle] resourcePath];
    vAssetDir = [vAssetDir stringByAppendingPathComponent:C_ASSETS_DIR];

    NSArray *directoryContents = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:vAssetDir error:nil];
    BOOL vResult = TRUE;
    for(int i = 0; i<[directoryContents count]; i++) {
        NSString *vFileName = [directoryContents objectAtIndex:i];
        if (![self isFileExists:vFileName]) {
            NSString *vAssetsFileName = [NSString stringWithFormat:@"%@/%@", C_ASSETS_DIR, vFileName];
            if (![self copyAssetFileToDocuments :vAssetsFileName :vFileName]) {
                vResult = FALSE;
            };
        } 
    }
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"copyAssetFilesToDocuments" :@"- end"];
    return vResult;
}

//- (void) clearDocumentsDir {
//    NSArray *directoryContents = [[NSFileManager defaultManager] directoryContentsAtPath: [self getDocumentsDir]];
//    for(int i = 0; i<[directoryContents count]; i++) {
//        NSString *s = [directoryContents objectAtIndex:i];
//        [self delFile :s];
//    }
//}


@end