//
//  Utils.h
//  CS
//
//  Created by EArt on 21.07.11.
//  Copyright 2011 CS LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CSUtils : NSObject {
    
}


+ (int) strPos :(NSString *) pString :(NSString *) pSubString;
+ (NSString *) getDeviceId;
+ (NSString *) getModel;
+ (NSString *) getPlatform;
+ (NSString *) getDeviceInfo;

+ (NSDictionary *) getTag :(NSString *)pStr :(NSString *)pStrBegin :(NSString *)pStrEnd :(int)pSearchOption :(int)pSearchStartPosition;
+ (NSString *) getTag :(NSString *)pStr :(NSString *)pStrBegin :(NSString *)pStrEnd;
+ (NSString *) getTag :(NSString *)pStr :(NSString *)pStrBegin :(NSString *)pStrEnd :(int)pSearchOption;
+ (NSString *) getTag :(NSString *)pStr :(NSString *)pTagName;
+ (NSString *)Data2Hex:(NSData*) dataValue;
+ (NSDate *) String2Date :(NSString *)pSDate;
+ (NSString *) Date2String :(NSDate *)pDate;
+ (unsigned long long) GetFreeDiskSpace;
+ (double) GetMin :(double)p1 :(double)p2;
+ (double) GetMax :(double)p1 :(double)p2;
+ (float) GetOsVersion;
+ (void) ShowMessage :(NSString *)pTitle :(NSString *)pMessage;
+ (float) FilterVal :(float)pIncrementalVal :(float)pCurrentVal :(float)pFilterFactor;
+ (void) CheckDevInfo;
+ (NSString *) getUniqueId;

@end
