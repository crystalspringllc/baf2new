//
//  SecurityHelper.m
//  CSInfo
//
//  Created by EArt on 12.09.11.
//  Copyright 2011 CS LLC. All rights reserved.
//

#import "SecurityHelper.h"
#import "NSData+Base64.h"
#import "CSConst.h"
#import "LogHelper.h"
#import "DBHelper.h"
#import <CommonCrypto/CommonHMAC.h>

#import <CommonCrypto/CommonCryptor.h>


@implementation SecurityHelper

#define C_CLASS_TAG @"SecurityHelper"

const size_t RSA_BUFFER_SIZE = 256;
static char C_DEVICE_KEY[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};


/**
 Шифрование буфера pInData с помощью публичного ключа pPublicKey
 @param pPublicKey указатель на публичный ключ
 @param pInData входящие данные (размер до 200 Байт!)
 @param pOutData зашифрованные данные
 @return TRUE если все ОК, FALSE или exception если возникла ошибка
 */
+ (NSData *)EncryptWithPublicKey :(SecKeyRef) pPublicKey :(NSData *)pData {
    NSData *vResult = NULL;
    uint8_t *vInData = (uint8_t *)calloc(RSA_BUFFER_SIZE, sizeof(uint8_t));
    uint8_t *vOutData = (uint8_t *)calloc(RSA_BUFFER_SIZE, sizeof(uint8_t));
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"EncryptWithPublicKey" :@"start"];
    @try {
        strncpy((char *)vInData, [pData bytes], [pData length]);
        size_t vInDataSize = [pData length];
        size_t vOutDataSize = RSA_BUFFER_SIZE;
        OSStatus vStatus = SecKeyEncrypt(
            pPublicKey,
            kSecPaddingNone,
            vInData,
            vInDataSize,
            &vOutData[0],
            &vOutDataSize
        );
        if (vStatus == 0) {
            vResult = [NSData dataWithBytes:vOutData length:vOutDataSize];
        } else {
            [LogHelper Log_ERROR :C_CLASS_TAG :@"EncryptWithPublicKey" :[NSString stringWithFormat:@"encryption result=%d, in_len=%zi", (int)vStatus, vInDataSize]];
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"EncryptWithPublicKey" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    @finally {
        free(vInData);
        free(vOutData);
    }
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"EncryptWithPublicKey" :@"end"];
    return vResult;
}

+ (NSData *)DecryptWithPrivateKey :(SecKeyRef)pPrivateKey :(NSData *)pData {
    NSData *vResult = NULL;
    uint8_t *vInData = (uint8_t *)calloc(RSA_BUFFER_SIZE, sizeof(uint8_t));
    uint8_t *vOutData = (uint8_t *)calloc(RSA_BUFFER_SIZE, sizeof(uint8_t));
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"DecryptWithPrivateKey" :@"start"];
    @try {
        strncpy((char *)vInData, [pData bytes], [pData length]);
        size_t vInDataSize = [pData length];
        size_t vOutDataSize = RSA_BUFFER_SIZE;
        OSStatus vStatus = SecKeyDecrypt(pPrivateKey,
                                        kSecPaddingNone,
                                        vInData,
                                        vInDataSize,
                                        &vOutData[0],
                                        &vOutDataSize
                                        );
        if (vStatus == 0) {
            vResult = [NSData dataWithBytes:vOutData length:vOutDataSize];
        } else {
            [LogHelper Log_ERROR :C_CLASS_TAG :@"DecryptWithPrivateKey" :[NSString stringWithFormat:@"decryption result=%d, in_len=%zi", (int)vStatus, vInDataSize]];
        }
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"DecryptWithPrivateKey" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    @finally {
        free(vInData);
        free(vOutData);
    }
    [LogHelper Log_DEBUG :C_CLASS_TAG :@"encryptWithPublicKey" :@"end"];
    return vResult;
}


+ (BOOL)VerifyData :(SecKeyRef)pPublicKey :(NSData *)pData :(NSData *)pSignature {
    
    if ((pData == NULL) || (pSignature == NULL) || (pPublicKey == NULL)) return FALSE;
    if ((pData.length == 0) || (pSignature.length == 0)) return FALSE;
    
    // Make SHA-1 hash of the data
    uint8_t vHData[CC_SHA1_DIGEST_LENGTH];
    NSData *vD = [NSData dataWithData:pData];
//                  dataWithBytesNoCopy:pData.bytes length:pData.length];
    CC_SHA1(vD.bytes, (unsigned)vD.length, vHData);
    NSData *vHash = [NSData dataWithBytes:vHData length:CC_SHA1_DIGEST_LENGTH];
    BOOL vResult = FALSE;
    OSStatus secStatus = SecKeyRawVerify(pPublicKey, 
                                         kSecPaddingPKCS1SHA1,
                                         vHash.bytes, 
                                         vHash.length,
                                         pSignature.bytes,
                                         pSignature.length);
    if (secStatus == errSecSuccess) {
        vResult = TRUE;
    }
    return vResult;
}


+ (NSData *) GetSignOfData :(SecKeyRef)pPrivateKey :(NSData *)pData {
    
    if ((pData == NULL) || (pPrivateKey == NULL)) return FALSE;
    if (pData.length == 0) return FALSE;

    uint8_t vHData[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(pData.bytes, (unsigned)pData.length, vHData);
    NSData *vHash = [NSData dataWithBytes:vHData length:CC_SHA1_DIGEST_LENGTH];
    
    size_t sigLen = 1024; // 128!
    uint8_t sigBuf[sigLen];
    OSStatus secStatus = SecKeyRawSign(pPrivateKey, 
                                       kSecPaddingPKCS1SHA1,
                                       vHash.bytes, 
                                       vHash.length,
                                       sigBuf, 
                                       &sigLen);
    if (secStatus == errSecSuccess) {
        NSData *vBSign = [NSData dataWithBytes: sigBuf length: sigLen];
        return vBSign;
    }
    return NULL;
}

/*
+ (NSData *)getPublicKeyBits :(const char *)pPublicKeyTag {
    OSStatus sanityCheck = noErr;
    NSData * publicKeyBits = nil;
    
    NSData * publicTag = [NSData dataWithBytes:C_PUBLIC_KEY_ID length:strlen((const char *)pPublicKeyTag)];
    
    NSMutableDictionary * queryPublicKey = [[NSMutableDictionary alloc] init];
    [queryPublicKey setObject:(id)kSecClassKey forKey:(id)kSecClass];
    [queryPublicKey setObject:publicTag forKey:(id)kSecAttrApplicationTag];
    [queryPublicKey setObject:(id)kSecAttrKeyTypeRSA forKey:(id)kSecAttrKeyType];
    [queryPublicKey setObject:[NSNumber numberWithBool:YES] forKey:(id)kSecReturnData];
    
    sanityCheck = SecItemCopyMatching((CFDictionaryRef)queryPublicKey, (CFTypeRef *)&publicKeyBits);
    if (sanityCheck != noErr) {
        publicKeyBits = nil;
    }
    [queryPublicKey release];
//    [publicTag release];
    
    return publicKeyBits;
}

+ (NSData *)getPrivateKeyBits :(const char *)pPrivateKeyTag {
    OSStatus sanityCheck = noErr;
    NSData * publicKeyBits = nil;
    
    NSData * privateTag = [NSData dataWithBytes:C_PRIVATE_KEY_ID length:strlen((const char *)pPrivateKeyTag)];
        
    NSMutableDictionary * queryPrivateKey = [[NSMutableDictionary alloc] init];
    [queryPrivateKey setObject:(id)kSecClassKey forKey:(id)kSecClass];
    [queryPrivateKey setObject:privateTag forKey:(id)kSecAttrApplicationTag];
    [queryPrivateKey setObject:(id)kSecAttrKeyTypeRSA forKey:(id)kSecAttrKeyType];
    [queryPrivateKey setObject:[NSNumber numberWithBool:YES] forKey:(id)kSecReturnData];
    
    sanityCheck = SecItemCopyMatching((CFDictionaryRef)queryPrivateKey, (CFTypeRef *)&publicKeyBits);
    if (sanityCheck != noErr) {
        publicKeyBits = nil;
    }
    [queryPrivateKey release];
//    [privateTag release];
    
    return publicKeyBits;
}
*/

+ (SecKeyRef) GetSrvPublicKeyRef {
    SecKeyRef vPublicKey = NULL;
    
    NSData * publicTag = [NSData dataWithBytes:C_SRV_PUBLIC_KEY_ID length:strlen((const char *)C_SRV_PUBLIC_KEY_ID)];
    
    NSMutableDictionary * queryPublicKey = [[NSMutableDictionary alloc] init];
    
    // Set the public key query dictionary.
    [queryPublicKey setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
    [queryPublicKey setObject:publicTag forKey:(__bridge id)kSecAttrApplicationTag];
    [queryPublicKey setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
    [queryPublicKey setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecReturnRef];
    
    CFTypeRef vPubKey = nil;
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)queryPublicKey, &vPubKey);
    if(status == noErr){
//        vPublicKey = (__bridge SecKeyRef)(CFBridgingRelease(vPubKey)); // error in device only!
        NSData *publicKeyBits = (__bridge NSData *)vPubKey;
        vPublicKey = (__bridge SecKeyRef)publicKeyBits;
    }
    return vPublicKey;
}



+ (SecKeyRef)GetPublicKeyRef {
    SecKeyRef vPublicKey = NULL;
    
    NSData * publicTag = [NSData dataWithBytes:C_PUBLIC_KEY_ID
                                        length:strlen((const char *)C_PUBLIC_KEY_ID)];
    
    NSMutableDictionary * queryPublicKey = [[NSMutableDictionary alloc] init];
    
    // Set the public key query dictionary.
    [queryPublicKey setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
    [queryPublicKey setObject:publicTag forKey:(__bridge id)kSecAttrApplicationTag];
    [queryPublicKey setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
    [queryPublicKey setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecReturnRef];
    
  /* EArt 01 07 2013
    OSStatus resultCode = SecItemCopyMatching((__bridge CFDictionaryRef)queryPublicKey, (CFTypeRef *)&vPublicKey); // EArt 13 06 2013 added CFBridgingRetain()
    if(resultCode != noErr){
        vPublicKey = NULL;
    }
 */
    CFTypeRef vPubKey = nil;
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)queryPublicKey, &vPubKey);
    if(status == noErr){
        NSData *publicKeyBits = (__bridge NSData *)vPubKey;
        vPublicKey = (__bridge SecKeyRef)publicKeyBits;
    }
    return vPublicKey;
}


+ (SecKeyRef)GetPrivateKeyRef {
    SecKeyRef vPrivateKey = nil;
    NSMutableDictionary * queryPrivateKey = [[NSMutableDictionary alloc] init];
    
    NSData * privateTag = [NSData dataWithBytes:C_PRIVATE_KEY_ID
                                         length:strlen((const char *)C_PRIVATE_KEY_ID)];
    
    // Set the private key query dictionary.
    [queryPrivateKey setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
    [queryPrivateKey setObject:privateTag forKey:(__bridge id)kSecAttrApplicationTag];
    [queryPrivateKey setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
    [queryPrivateKey setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecReturnRef];
    
    /* EArt 01 07 2013
    resultCode = SecItemCopyMatching((__bridge CFDictionaryRef)queryPrivateKey, (CFTypeRef *)&vPrivateKey); // EArt 13 06 2013 added CFBridgingRetain()
    
    if(resultCode != noErr) {
        vPrivateKey = NULL;
    }
    */
    
    CFTypeRef vPrKey = nil;
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)queryPrivateKey, &vPrKey);
    if(status == noErr){
        NSData *vKeyBits = (__bridge NSData *)vPrKey;
        vPrivateKey = (__bridge SecKeyRef)vKeyBits;
    }
    
    return vPrivateKey;
}

/*!
 
 */
+ (BOOL) CreateKeyPair {
    OSStatus status = noErr;
    NSMutableDictionary *privateKeyAttr = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *publicKeyAttr = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *keyPairAttr = [[NSMutableDictionary alloc] init];
    
    NSData * publicTag = [NSData dataWithBytes:C_PUBLIC_KEY_ID length:strlen((const char *)C_PUBLIC_KEY_ID)];
    NSData * privateTag = [NSData dataWithBytes:C_PRIVATE_KEY_ID length:strlen((const char *)C_PRIVATE_KEY_ID)];
    
    SecKeyRef publicKey = NULL;
    SecKeyRef privateKey = NULL;
    
    [keyPairAttr setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
    [keyPairAttr setObject:[NSNumber numberWithInt:1024] forKey:(__bridge id)kSecAttrKeySizeInBits];
    [privateKeyAttr setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecAttrIsPermanent];
    
    [privateKeyAttr setObject:privateTag forKey:(__bridge id)kSecAttrApplicationTag];
    [publicKeyAttr setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecAttrIsPermanent];
    [publicKeyAttr setObject:publicTag forKey:(__bridge id)kSecAttrApplicationTag];
    [keyPairAttr setObject:privateKeyAttr forKey:(__bridge id)kSecPrivateKeyAttrs];
    [keyPairAttr setObject:publicKeyAttr forKey:(__bridge id)kSecPublicKeyAttrs]; // 12
    
    status = SecKeyGeneratePair((__bridge CFDictionaryRef)keyPairAttr, &publicKey, &privateKey); // EArt 13 06 2013 added CFBridgingRetain()
    //ключи, в т ч приватный, лежат в KeyChains в зашифрованном паролем пользователя виде

    if(publicKey) CFRelease(publicKey);
    if(privateKey) CFRelease(privateKey);
    
    return status == errSecSuccess;
}

// encode length in ASN.1 DER format
size_t encodeLength(unsigned char * buf, size_t length) { 
    if (length < 128) {
        buf[0] = length;
        return 1;
    }
    size_t i = (length / 256) + 1;
    buf[0] = i + 0x80;
    for (size_t j = 0 ; j < i; ++j) {        
        buf[i - j] = length & 0xFF;  
        length = length >> 8;
    }
    
    return i + 1;
}

/* Получение публичного ключа устройства в формате PEM (без заголовка и подвала) для передачи его на сервер
 */
+ (NSString *)getX509FormattedPublicKey { 
    static unsigned char oidSequence[] = { 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x01, 0x05, 0x00 };
    NSMutableDictionary * queryPublicKey = NULL;
    NSMutableData *encKey = NULL;
    NSData * publicKeyBits = NULL;
    NSString *returnString = NULL;
    @try {
        queryPublicKey = [[NSMutableDictionary alloc] init];
        encKey = [[NSMutableData alloc] init];
        
        NSData * publicTag = [NSData dataWithBytes:C_PUBLIC_KEY_ID length:strlen((const char *) C_PUBLIC_KEY_ID)];
        [queryPublicKey setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
        [queryPublicKey setObject:publicTag forKey:(__bridge id)kSecAttrApplicationTag];
        [queryPublicKey setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
        [queryPublicKey setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecReturnData];
// EArt 13 06 2013 commented:   OSStatus err = SecItemCopyMatching((CFDictionaryRef)CFBridgingRetain(queryPublicKey),(CFTypeRef *)&publicKeyBits);
        
        CFDictionaryRef cfresult = NULL;
        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef)queryPublicKey,(CFTypeRef *)&cfresult);
        
        
        CFTypeRef vPubKey = nil;
        OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)queryPublicKey, &vPubKey);
        if(status != noErr){
            [NSException raise:C_ERROR_CRYPTO format:@"Public key is not found!"];
        }
        publicKeyBits = (__bridge NSData *)vPubKey;
        
        // EArt 01 06 2013  publicKeyBits = (NSData *)CFBridgingRelease(cfresult);
        
// EArt 13 06 2013 variant with CFBridgingRetain:
        /*
        CFDictionaryRef cfquery = (CFDictionaryRef)CFBridgingRetain(queryPublicKey);
        CFDictionaryRef cfresult = NULL;
        
        OSStatus err = SecItemCopyMatching(cfquery, (CFTypeRef *)&cfresult);
        CFRelease(cfquery);
        publicKeyBits = (NSData *)CFBridgingRelease(cfresult);
        */
        
        
        if (err != noErr) {
            [NSException raise:C_ERROR_CRYPTO format:@"Public key is not found! status=%i", (int)err];
        }
        unsigned char builder[15];

        int bitstringEncLength;
        // в зависимости от размера ключа разный размер преабмулы ASN.1
        if ([publicKeyBits length ] + 1  < 128 ) {
            bitstringEncLength = 1 ;
        } else {
            bitstringEncLength = (unsigned)((([publicKeyBits length ] +1 ) / 256 ) + 2);
        }
        // ASN.1 encoding representing a SEQUENCE
        builder[0] = 0x30;
        // Build up overall size made up of -
        // size of OID + size of bitstring encoding + size of actual key
        size_t i = sizeof(oidSequence) + 2 + bitstringEncLength + [publicKeyBits length];
        size_t j = encodeLength(&builder[1], i);
        [encKey appendBytes:builder length:j +1];
        [encKey appendBytes:oidSequence length:sizeof(oidSequence)];
        // Now add the bitstring
        builder[0] = 0x03;
        j = encodeLength(&builder[1], [publicKeyBits length] + 1);
        builder[j+1] = 0x00;
        [encKey appendBytes:builder length:j + 2];
        // Now the actual key
        [encKey appendData:publicKeyBits];
//    NSString *returnString = [NSString stringWithFormat:@"%@\n",@"-----BEGIN PUBLIC KEY-----"];
//    returnString = [returnString stringByAppendingString:[encKey base64EncodedString]];
//    returnString = [returnString stringByAppendingFormat:@"\n%@",@"-----END PUBLIC KEY-----"];
        returnString =[encKey base64EncodedString];
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"getX509FormattedPublicKey" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    @finally {
        if (queryPublicKey) queryPublicKey = nil;
        if (encKey) encKey = nil;
        if (publicKeyBits) publicKeyBits = nil;
    }
    return returnString;
}

/* сохранение серверного публичного ключа в keychains
 */
+ (BOOL)SetSrvPublicKey :(NSString *)pemPublicKeyString {
    BOOL vResult = FALSE;
    NSData * publicTag = [NSData dataWithBytes:C_SRV_PUBLIC_KEY_ID length:strlen((const char *)C_SRV_PUBLIC_KEY_ID)];
//    NSMutableDictionary *vKeyDict = [[[NSMutableDictionary alloc] init] autorelease];
    NSMutableDictionary *vKeyDict = [[NSMutableDictionary alloc] init];
    @try {
        // удаление старого ключа:
        [vKeyDict setObject:(__bridge id) kSecClassKey forKey:(__bridge id)kSecClass];
        [vKeyDict setObject:(__bridge id) kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
        [vKeyDict setObject:publicTag forKey:(__bridge id)kSecAttrApplicationTag];
        SecItemDelete((__bridge CFDictionaryRef)vKeyDict); // EArt 13 06 2013 added CFBridgingRetain()
        // удаление префикса OID pem-ключа:
        NSString *strippedKey = pemPublicKeyString;
        NSData *vPublicKeyData = [NSData dataFromBase64String:strippedKey];
//    NSLog(@"\nPublic Key Bytes:\n%@\n\n",[strippedPublicKeyData description]);
        unsigned char * bytes = (unsigned char *)[vPublicKeyData bytes];
        size_t bytesLen = [vPublicKeyData length];
        size_t i = 0;
        if (bytes[i++] != 0x30) {
            [NSException raise:C_ERROR_CRYPTO format:@"Unknown format!"];
        }
        if (bytes[i] > 0x80) {
            i += bytes[i] - 0x80 + 1;
        } else {
            i++;
        }        
        if (i >= bytesLen) {
            [NSException raise:C_ERROR_CRYPTO format:@"Unknown format!"];
        }
        if (bytes[i] != 0x30) {
            [NSException raise:C_ERROR_CRYPTO format:@"Unknown format!"];
        }
        i += 15;
        if (i >= bytesLen - 2) {
            [NSException raise:C_ERROR_CRYPTO format:@"Unknown format!"];
        }
        if (bytes[i++] != 0x03) {
            [NSException raise:C_ERROR_CRYPTO format:@"Unknown format!"];
        }
        if (bytes[i] > 0x80) {
            i += bytes[i] - 0x80 + 1;
        } else {
            i++;
        }
        if (i >= bytesLen) {
            [NSException raise:C_ERROR_CRYPTO format:@"Unknown format!"];
        }
        if (bytes[i++] != 0x00) {
            [NSException raise:C_ERROR_CRYPTO format:@"Unknown format!"];
        }
        if (i >= bytesLen) {
            [NSException raise:C_ERROR_CRYPTO format:@"Unknown format!"];
        }
        vPublicKeyData = [NSData dataWithBytes:&bytes[i] length:bytesLen - i];
        if (vPublicKeyData == nil) {
            [NSException raise:C_ERROR_CRYPTO format:@"Unknown format!"];
        }
        // сохранение ключа:
        CFTypeRef persistKey = nil;
        [vKeyDict setObject:vPublicKeyData forKey:(__bridge id)kSecValueData];
        [vKeyDict setObject:(__bridge id) kSecAttrKeyClassPublic forKey:(__bridge id)kSecAttrKeyClass];
        [vKeyDict setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecReturnPersistentRef];
        OSStatus secStatus = SecItemAdd((__bridge  CFDictionaryRef)vKeyDict, &persistKey); // EArt 13 06 2013 added CFBridgingRetain()
        if (persistKey != NULL) {
            CFRelease(persistKey);
        }
        if ((secStatus != noErr) && (secStatus != errSecDuplicateItem)) {
            [NSException raise:C_ERROR_CRYPTO format:@"Key addition error!"];
        }
        /*        
        проверка:
        SecKeyRef keyRef = nil;
        [publicKey removeObjectForKey:(id)kSecValueData];
        [publicKey removeObjectForKey:(id)kSecReturnPersistentRef];
        [publicKey setObject:[NSNumber numberWithBool:YES] forKey:(id)kSecReturnRef];
        [publicKey setObject:(id) kSecAttrKeyTypeRSA forKey:(id)kSecAttrKeyType];
    
        SecItemCopyMatching((CFDictionaryRef)publicKey,(CFTypeRef *)&keyRef);
        if(keyRef) CFRelease(keyRef);
        
        if (keyRef == nil) {
            [NSException raise:C_ERROR_CRYPTO format:@"Key checking error!"];
        }
        */
        vResult = TRUE;
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"setSrvPublicKey" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    @finally {
        vKeyDict = nil;
    }
    return vResult;
}




//========================================================

// шифрование AES265, ключ должен быть 32 байта null-padded!
+ (NSData *)AES256EncryptWithKey :(char *)key :(NSData *)pData {
	char keyPtr[32];
    memcpy(keyPtr, key, 32);    
	NSUInteger dataLength = [pData length];
	//результат может быть больше на 1 блок 256b!
	size_t bufferSize = dataLength + kCCBlockSizeAES128 * 2;
	void *buffer = malloc(bufferSize);
	size_t numBytesEncrypted = 0;
	CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding, keyPtr, kCCKeySizeAES256, C_AES_IV, [pData bytes], dataLength, buffer, bufferSize, &numBytesEncrypted);
	if (cryptStatus == kCCSuccess) {
        // free делается на вызывающей стороне!
		return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
	}
	free(buffer);
	return nil;
}

// расшифрование AES265, ключ должен быть 32 байта null-padded!
+ (NSData *)AES256DecryptWithKey:(char *)key :(NSData *)pData{
	char keyPtr[32];
    memcpy(keyPtr, key, 32);
	NSUInteger dataLength = [pData length];
	//результат может быть больше на 1 блок 256 b!
	size_t bufferSize = dataLength + kCCBlockSizeAES128 * 2;
	void *buffer = malloc(bufferSize);
	size_t numBytesDecrypted = 0;
	CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding, keyPtr, kCCKeySizeAES256, C_AES_IV, [pData bytes], dataLength, buffer, bufferSize, &numBytesDecrypted);
	if (cryptStatus == kCCSuccess) {
        // free делается на вызывающей стороне!
		return [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
	}
	free(buffer);
	return nil;
}

+ (NSData *)AES256DecryptWithKeyAsNSData:(NSData *)pKey :(NSData *)pData {
    char vKey[32];
    memcpy(vKey, [pKey bytes], 32);
    return [self AES256DecryptWithKey : vKey :pData];
}

+ (NSData *)AES256EncryptWithKeyAsNSData:(NSData *)pKey :(NSData *)pData {
    char vKey[32];
    memcpy(vKey, [pKey bytes], 32);
    return [self AES256EncryptWithKey : vKey :pData];
}

+ (NSData *)AES256EncryptWithCommonKey :(NSData *)pData {
    NSData *vResult = [self AES256EncryptWithKey :(char *)C_AES_COMMON_KEY :pData];
    return vResult;
}

+ (NSData *)AES256DecryptWithCommonKey :(NSData *)pData { 
    NSData *vResult = [self AES256DecryptWithKey :(char *)C_AES_COMMON_KEY :pData];
    return vResult;
}

+ (BOOL) isUserSessionActive {
    return C_DEVICE_KEY[0] != 0;
}

+ (void) ClearUserSession {
    memset((void *)C_DEVICE_KEY, 0x0, 32);
}

+ (NSData *)AES256DecryptWithDeviceKey :(NSData *)pData{
    if (![self isUserSessionActive]) {
        [NSException raise:C_ERROR_CRYPTO format:@"AES256DecryptWithDeviceKey - device key is NUILL!"];
    }
    char vKey[32];
    memcpy(vKey, C_DEVICE_KEY, 32);
    return [self AES256DecryptWithKey :vKey :pData];
}

+ (NSData *)AES256EncryptWithDeviceKey :(NSData *)pData{
    if (![self isUserSessionActive]) {
        [NSException raise:C_ERROR_CRYPTO format:@"AES256EncryptWithDeviceKey - device key is NUILL!"];
    }
    char vKey[32];
    memcpy(vKey, C_DEVICE_KEY, 32);    
    
    return [self AES256EncryptWithKey :vKey :pData];
}


// получение дайджеста SHA256 (32b) по блоку данных
+ (NSData *) GetSHA256 :(NSData *)pData {
    uint8_t vHash[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(pData.bytes, (unsigned)pData.length, vHash);
    NSData *vResult = [NSData dataWithBytes:vHash length:CC_SHA256_DIGEST_LENGTH];
    return vResult;
}


// получение дайджеста SHA256 (32b) пароля для получения ключа шифрования индивидуального ключа
+ (NSData *) GetSHA256_Salt :(NSString *)pPin {
    NSString *vS = [NSString stringWithFormat:@"1i4n56%@24sdf", pPin];
    NSData *vData = [vS dataUsingEncoding:NSUTF8StringEncoding];
    return [self GetSHA256 :vData];
}

// получение дайджеста SHA256 (32b) пароля для проверки пароля
+ (NSData *) GetSHA256_Key :(NSString *)pPin {
    NSData *vData = [pPin dataUsingEncoding:NSUTF8StringEncoding];
    return [self GetSHA256 :vData];
}

+ (BOOL) CreateNewDeviceKey {
    [self ClearUserSession];
    OSStatus vRes = SecRandomCopyBytes(kSecRandomDefault, 32, (void *) C_DEVICE_KEY);
    if (vRes != noErr){
//       @"Problem generating the symmetric key, OSStatus == %d.",  
        return FALSE;
    }
    // первый байт в ключе - флаг наличия ключа, поэтому если с сгенерированном ключе первый байт будет нулевой то ставим его в 1
    if (C_DEVICE_KEY[0] == 0) {
        C_DEVICE_KEY[0] = 1;
    }
    return TRUE;
}

+ (void) SetDeviceKey :(NSData *)pData {
    memcpy(C_DEVICE_KEY, [pData bytes], 32);    
}


+ (NSString *)GetB64SignOfDataWithDevicePrivateKey :(NSData *)pData {
    SecKeyRef vPrivateKey = NULL;
    vPrivateKey = [self GetPrivateKeyRef];
    if (vPrivateKey == NULL) {
        [NSException raise:C_ERROR_CRYPTO format:@"Private key is not found!"];
    }
    NSData *vBSign = [self GetSignOfData :vPrivateKey :pData];
    NSString *vSSign = [vBSign base64EncodedString];
    CFRelease(vPrivateKey);
    return vSSign;
}

// проверка пароля с извлечением индивидуального ключа в переменную
+ (BOOL) OpenSession :(NSString *)pPin {
    
    
    BOOL vResult = FALSE;
    @try {
        if ([pPin length] < 4) {
            [NSException raise:C_ERROR_CRYPTO format:@"Length of pin must be 4 or more!"];
        }        
        NSString *vUserPassDigest = [DBHelper GetVar:C_VAR_USER_DIGEST_KEY :@""];
        if ([vUserPassDigest isEqualToString:@""] ) {
            [NSException raise:C_ERROR_CRYPTO format:@"Digest is not found!"];
        }
        //  проверяем пароль по дайджесту:
        NSData *vDPinDigest = [self GetSHA256_Salt:pPin];
        if (![vUserPassDigest isEqualToString:[vDPinDigest base64EncodedString]]) {
            [NSException raise:C_ERROR_CRYPTO format:@"Incorrect pin!"];
        }
        // извлекаем, расшифровываем и сохраняем в переменной индивидуальный ключ
        NSString *vSDeviceKeyData = [DBHelper GetVar:C_VAR_DEVICE_KEY :@""];
        NSData *vDeviceKeyData = [NSData dataFromBase64String:vSDeviceKeyData];
        vDeviceKeyData = [self AES256DecryptWithKeyAsNSData:vDPinDigest :vDeviceKeyData];
        [self SetDeviceKey:vDeviceKeyData];
//[self SaveNewPin :pPin :pPin]; //debug !!!
        vResult = TRUE;
    }
    @catch (NSException *exception) {
        [LogHelper Log_ERROR :C_CLASS_TAG :@"OpenSession" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    return vResult;
}

// создание или перешифровка ключей
+ (BOOL) SaveNewPin :(NSString *)pOldPin :(NSString *)pNewPin {
    BOOL vResult = FALSE;
    @try {
        if (pOldPin == NULL | pNewPin == NULL) {
            [NSException raise:C_ERROR_CRYPTO format:@"Pin is null!"];
        }
        if ([pNewPin length] < 4) {
            [NSException raise:C_ERROR_CRYPTO format:@"Length of new pin must be 4 or more!"];
        }
        // првоерка на наличие дайджеста старого пароля:
        NSString *vUserPassDigest = [DBHelper GetVar:C_VAR_USER_DIGEST_KEY :@""];
//vUserPassDigest = @""; //debug!!!
        if ([vUserPassDigest isEqualToString:@""]) {
            //  первоначальная установка - создание новых ключей:
            [self CreateKeyPair];
            [self CreateNewDeviceKey];
        } else {
            // проверяем старый пин:
            NSData *vDOldPinDigest = [self GetSHA256_Salt:pOldPin];
            NSString *vSOldPinDigest = [vDOldPinDigest base64EncodedString];
            if (![vSOldPinDigest isEqualToString:vUserPassDigest]) {
                [NSException raise:C_ERROR_CRYPTO format:@"Old pin is incorrect!"];
            }
            // индивидуальный ключ уже должен быть в переменной C_DEVICE_KEY!
        }
        // шифрование и сохранение индивидуального ключа и дайджеста в БД
        NSData *vDNewPinDigest = [self GetSHA256_Salt:pNewPin];
        NSData *vBDeviceKey = [NSData dataWithBytes:C_DEVICE_KEY length:32];
        NSData *vBEncryptedDeviceKey = [self AES256EncryptWithKeyAsNSData:vDNewPinDigest :vBDeviceKey];
        [DBHelper SetVar:C_VAR_DEVICE_KEY :[vBEncryptedDeviceKey base64EncodedString]];
        [DBHelper SetVar:C_VAR_USER_DIGEST_KEY :[vDNewPinDigest base64EncodedString]];
        // шифрование индивидуального ключа серверным публичным ключем для отправки на сервер
        SecKeyRef vPublicKey = [self GetSrvPublicKeyRef];
        vBEncryptedDeviceKey = [self EncryptWithPublicKey :vPublicKey :vBDeviceKey];
        CFRelease(vPublicKey);
        // отправка ключей на сервер
        [DBHelper AddOutDataRec:C_REGISTRATION_DIGEST :vDNewPinDigest];
        NSString *vB64PubKey = [self getX509FormattedPublicKey];
        [DBHelper AddOutDataRec:C_REGISTRATION_PUB_KEY :[NSData dataFromBase64String:vB64PubKey]];
        [DBHelper AddOutDataRec:C_REGISTRATION_DEVICE_KEY :vBEncryptedDeviceKey];
        vResult = TRUE;
    }
    @catch (NSException *exception) {
         [LogHelper Log_ERROR :C_CLASS_TAG :@"setSrvPublicKey" :[NSString stringWithFormat:@"eName=%@, eReason=%@", [exception name], [exception reason]] ];
    }
    return vResult;
}



+ (void) Test {
    
    
    SecKeyRef vSrvPublicKey = [self GetSrvPublicKeyRef];

    NSString *vSData = @"QSTJLuR0qXoGwLry6hQIlUDsz/cvRyngn1Sy3wha8KkMl9/A0S76op5Rl7V/oh0wlvjELZEiOk16ZF9Rs0h3sI/lHf+2foGpSFhbHbpmeVN5nH2JN7UUcVwbQrE3kg/YON+6CT4gUjhQdc3BIPqqBFZIiP95Rw+Bn63a/yK9nzpk+nB7j+fJ66avxYPXO8BkHMANcMJ3WxsGphcs/kDgELAEs2ALv6x5YaAU0QB9bCM=";
    NSString *vSSign = @"HACvq/JIunQ75nX1a85TB2wWUnyMtResyiaS7E0z5f/aMD6lVFh8OQXciv5FMHkZlnlPo2Oqw8ppEr1fhkiHb0LMUkMlCH45kFwGZeUdLEqy+G5zVcIkOE0STPgQw4wmYV3QHyf/GmJ7j1aSAZMpVEEodtbI2Ucm/g7HNiQP/L8=";
    NSData *vBData = [NSData dataFromBase64String:vSData];
    NSData *vBSign = [NSData dataFromBase64String:vSSign];
    
    if ([self VerifyData :vSrvPublicKey :vBData :vBSign]) {
        NSLog(@"ok");
    }
    
    
    SecKeyRef vPublicKey = NULL;
    SecKeyRef vPrivateKey = NULL;
    
    vPublicKey = [self GetPublicKeyRef];
    vPrivateKey = [self GetPrivateKeyRef];
    
    if (vPublicKey == NULL) {
        [self CreateKeyPair];
        vPublicKey = [self GetPublicKeyRef];
        vPrivateKey = [self GetPrivateKeyRef];
    }
    
    NSString *vS = [self getX509FormattedPublicKey];
    NSLog(@"%@", vS);
    
    NSString *vMess = @"message";
    NSData *vData = [vMess dataUsingEncoding:NSUTF8StringEncoding];
    
    NSData *vSign = [self GetSignOfData :vPrivateKey :vData];
    if ([self VerifyData :vPublicKey :vData :vSign]) {
        NSLog(@"ok");
    }

    NSData *vData1 = [@"messagE" dataUsingEncoding:NSUTF8StringEncoding];
    if ([self VerifyData :vPublicKey :vData1 :vSign]) {
        NSLog(@"err!");
    } else {
        NSLog(@"ok");
    }
    
    if ([self CreateNewDeviceKey]) {
        NSData *vPlain = [NSData dataFromBase64String:@"bl9yX3BfS0FTRV9TUlZfS1pUX1VTRD1uXzAuMDU="];
        NSString *vS = [[NSString alloc] initWithData:vPlain encoding:NSUTF8StringEncoding];
        NSLog(@"%@", vS);
        
        NSData *vEn1 = [self AES256EncryptWithDeviceKey :vPlain];
        NSData *vDe1 = [self AES256DecryptWithDeviceKey :vEn1];
        vS = [[NSString alloc] initWithData:vDe1 encoding:NSUTF8StringEncoding];
        NSLog(@"%@", vS);
        
        NSData *vC1 = [self GetSHA256_Salt :@"проверка"];
        NSLog(@"%@", [vC1 base64EncodedString]);
        NSData *vC2 = [self GetSHA256_Salt :@"проверка"];
        NSLog(@"%@", [vC2 base64EncodedString]);
        //exit(0);
    }
}

    /*
    if ([self CreateNewDeviceKey]) {
        NSData *vPlain = [NSData dataFromBase64String:@"bl9yX3BfS0FTRV9TUlZfS1pUX1VTRD1uXzAuMDU="];
        NSString *vS = [[NSString alloc] initWithData:vPlain encoding:NSUTF8StringEncoding];
        NSLog(@"%@", vS); 
        
        NSData *vEn1 = [self AES256EncryptWithDeviceKey :vPlain];
        NSData *vDe1 = [self AES256DecryptWithDeviceKey :vEn1];
        vS = [[NSString alloc] initWithData:vDe1 encoding:NSUTF8StringEncoding];
        NSLog(@"%@", vS);  
        
        NSData *vC1 = [self GetSHA256_Salt :@"проверка"];
        NSLog(@"%@", [vC1 base64EncodedString]);   
        NSData *vC2 = [self GetSHA256_Salt :@"проверка"];
        NSLog(@"%@", [vC2 base64EncodedString]);
        exit(0);
    }
     */
  
/*
    
    SecKeyRef vPublicKey = NULL;
    SecKeyRef vPrivateKey = NULL;
    
    vPublicKey = [self getPublicKeyRef];
    vPrivateKey = [self getPrivateKeyRef];
    
    NSString *vMess = @"message";
    NSData *vData = [vMess dataUsingEncoding:NSUTF8StringEncoding];
    NSString *vB64Data = [vData base64EncodedString];
    
    
    NSString *vSign = [self SignData :vPrivateKey :vB64Data];
    if ([self verifyData :vPublicKey :vB64Data :vSign]) {
        NSLog(@"ok");
    }
    

    SecKeyRef vSrvPublicKey = [self getSrvPublicKeyRef];
    if (vSrvPublicKey == NULL) {
        [self setSrvPublicKey :C_SRV_PUBLIC_KEY];
        vSrvPublicKey = [self getSrvPublicKeyRef];
    }
    NSString *vSData = @"QSTJLuR0qXoGwLry6hQIlUDsz/cvRyngn1Sy3wha8KkMl9/A0S76op5Rl7V/oh0wlvjELZEiOk16ZF9Rs0h3sI/lHf+2foGpSFhbHbpmeVN5nH2JN7UUcVwbQrE3kg/YON+6CT4gUjhQdc3BIPqqBFZIiP95Rw+Bn63a/yK9nzpk+nB7j+fJ66avxYPXO8BkHMANcMJ3WxsGphcs/kDgELAEs2ALv6x5YaAU0QB9bCM=";
    NSString *vSSign = @"HACvq/JIunQ75nX1a85TB2wWUnyMtResyiaS7E0z5f/aMD6lVFh8OQXciv5FMHkZlnlPo2Oqw8ppEr1fhkiHb0LMUkMlCH45kFwGZeUdLEqy+G5zVcIkOE0STPgQw4wmYV3QHyf/GmJ7j1aSAZMpVEEodtbI2Ucm/g7HNiQP/L8=";
    if ([self verifyData :vSrvPublicKey :vSData :vSSign]) {
        NSLog(@"ok");
        
        char vKey[32] = {0xD8, 0x7F, 0x91, 0x3E, 0xA6, 0x59, 0x5B, 0x74, 0xBA, 0x83, 0xF7, 0x8A, 0x0E, 0x98, 0x23, 0x01, 0xE2, 0x04, 0xCE, 0x47, 0x9A, 0x18, 0x45, 0x65, 0xB1, 0xFA, 0xE4, 0x8F, 0x54, 0xC0, 0x06, 0x22};
 
        
        NSData *vPlain = [NSData dataFromBase64String:@"bl9yX3BfS0FTRV9TUlZfS1pUX1VTRD1uXzAuMDU="];
        NSData *vEn = [NSData dataFromBase64String:vSData];

        
        NSData *vEn1 = [self AES256EncryptWithKey :vKey :vPlain];
        NSData *vDe1 = [self AES256DecryptWithKey :vKey :vEn1];
        NSString *vSDe1 = [[NSString alloc] initWithData:vDe1 encoding:NSASCIIStringEncoding];
        NSLog(@"%@", vSDe1);
        
  
        NSData *vDe = [self AES256DecryptWithKey :vKey :vEn];
//        vDe = [vDe gzipInflate];
        NSString *vSDe = [[NSString alloc] initWithData:vDe encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@", vSDe);
        
    }
    
    if(vPublicKey) CFRelease(vPublicKey);
    if(vPrivateKey) CFRelease(vPrivateKey);
    
//    NSLog(@"%@", [self getX509FormattedPublicKey]);
    
    
    
    SecKeyRef vPublicKey = NULL;
    SecKeyRef vPrivateKey = NULL;
    
    vPublicKey = [self getPublicKeyRef];
    vPrivateKey = [self getPrivateKeyRef];
    if (vPublicKey == NULL) {
        [self CreateKeyPair];
        vPublicKey = [self getPublicKeyRef];
        vPrivateKey = [self getPrivateKeyRef];
    }
    
    
    
    
    
    uint8_t *plainBuffer;
    uint8_t *cipherBuffer;
    uint8_t *decryptedBuffer;
    
    const char inputString[] = "this is a test.  this is only a test.  please remain calm.";
    int len = strlen(inputString);
    this is a hack since i know inputString length will be less than BUFFER_SIZE
    if (len > BUFFER_SIZE) len = BUFFER_SIZE-1;
    
    plainBuffer = (uint8_t *)calloc(BUFFER_SIZE, sizeof(uint8_t));
    cipherBuffer = (uint8_t *)calloc(CIPHER_BUFFER_SIZE, sizeof(uint8_t));
    decryptedBuffer = (uint8_t *)calloc(BUFFER_SIZE, sizeof(uint8_t));
    
    strncpy( (char *)plainBuffer, inputString, len);
    
    NSLog(@"init() plainBuffer: %s", plainBuffer);
    [self encryptWithPublicKey :vPublicKey :(UInt8 *)plainBuffer cipherBuffer:cipherBuffer];
    NSLog(@"encrypted data: %s", cipherBuffer);

    [self decryptWithPrivateKey :vPrivateKey :cipherBuffer plainBuffer:decryptedBuffer];
    NSLog(@"decrypted data: %s", decryptedBuffer);

    free(plainBuffer);
    free(cipherBuffer);
    free(decryptedBuffer);    
    
    if(vPublicKey) CFRelease(vPublicKey);
    if(vPrivateKey) CFRelease(vPrivateKey);
*/    



@end
