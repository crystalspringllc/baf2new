//
//  AstanaRefreshControl.h
//  AstanaRefreshControl
//
//  Created by Shyngys Kassymov on 28.10.15.
//  Copyright © 2015 CrystalSpring LLP. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum AstanaRefreshControlState:NSInteger {
    AstanaRefreshControlStateNormal = 0,
    AstanaRefreshControlStateRefreshing = 1,
    AstanaRefreshControlStateDisappearing = 2
} AstanaRefreshControlState;

@interface AstanaRefreshControl : UIControl

@property (nonatomic, strong) UIColor *shineColor;
@property (nonatomic, assign) id target;
@property (nonatomic) SEL action;
@property AstanaRefreshControlState animationState;

- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)scrollViewWillEndDragging;
- (void)finishingLoading;

@end
