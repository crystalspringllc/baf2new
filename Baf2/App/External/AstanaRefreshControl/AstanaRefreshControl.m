//
//  AstanaRefreshControl.m
//  AstanaRefreshControl
//
//  Created by Shyngys Kassymov on 28.10.15.
//  Copyright © 2015 CrystalSpring LLP. All rights reserved.
//

#import "AstanaRefreshControl.h"

#define TRIANGLES_SPACING 0.05

static const CGFloat kdisappearDuration = 0.5;
static const CGFloat krelativeHeightFactor = 1.f/2.f;

@interface AstanaRefreshControl ()

@property (nonatomic, strong) UIView *refreshContainerView;
@property (nonatomic, strong) UIView *astanaLogoView;
@property (nonatomic) NSInteger refreshAnimationIteration;
@property (nonatomic, strong) NSMutableArray *translationXList;
@property (nonatomic, strong) UIView *shadowView;
@property (nonatomic, strong) CAGradientLayer *gradient;
@property (nonatomic, strong) CADisplayLink *displayLink;
@property (nonatomic, weak) UIScrollView *scrollView;

@property (nonatomic) CGFloat dropHeight;
@property (nonatomic) CGFloat originalTopContentInset;
@property (nonatomic) CGFloat disappearProgress;
@property (nonatomic) CGFloat internalAnimationFactor;
@property (nonatomic) int horizontalRandomness;

@end

@implementation AstanaRefreshControl

- (instancetype)init {
    self = [super init];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup {
    // Make spinner transparent
    self.tintColor = [UIColor clearColor];
    
    // init vars
    self.translationXList = @[@(0), @(0), @(0), @(0), @(0), @(0), @(0), @(0)].mutableCopy;
    self.animationState = AstanaRefreshControlStateNormal;
    self.dropHeight = 80.0;
    self.horizontalRandomness = 150;
    self.internalAnimationFactor = 0.7;
    
    self.frame = CGRectMake(0, -self.dropHeight, [UIScreen mainScreen].bounds.size.width, self.dropHeight);
    
    [self addAstanaLogoView];
}

- (void)addAstanaLogoView {
    if (!self.refreshContainerView) {
        self.refreshContainerView = [[UIView alloc] initWithFrame:self.bounds];
        
        /*
         
         |
         | Assuming that AstanaLogoView will have square frame
         |
         |         /\
         |        / 8\
         |       /____\
         |      /\    /\
         |     / 5\ 7/ 6\
         |    /____\/____\
         |   /\    /\    /\
         |  / 1\ 3/  \4 / 2\
         | /____\/    \/____\
         |
         | <----> - triangle width (== triangle height)
         
         */
        
        self.astanaLogoView = [UIView new];
        self.astanaLogoView.frame = CGRectMake(0, 0, 30, 30);
        
        CGFloat sizeOfTriangle = self.astanaLogoView.frame.size.width / 3.0 - 2 * TRIANGLES_SPACING;
        CGFloat horizontalTrianglesSpacing = 2 * TRIANGLES_SPACING / 4.0;
        
        // Add triangles
        UIView *triangle1 = [[UIView alloc] initWithFrame:CGRectMake(0, 2 * sizeOfTriangle + 2 * TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
        triangle1.backgroundColor = [UIColor colorWithRed:12/255.0 green:74/255.0 blue:110/255.0 alpha:1.0];
        triangle1.layer.mask = [self createTriangleMask:YES];
        [self.astanaLogoView addSubview:triangle1];
        
        UIView *triangle3 = [[UIView alloc] initWithFrame:CGRectMake(sizeOfTriangle/2.0 + horizontalTrianglesSpacing, 2 * sizeOfTriangle + 2 * TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
        triangle3.backgroundColor = [UIColor colorWithRed:20/255.0 green:117/255.0 blue:136/255.0 alpha:1.0];
        triangle3.layer.mask = [self createTriangleMask:NO];
        [self.astanaLogoView addSubview:triangle3];
        
        UIView *triangle5 = [[UIView alloc] initWithFrame:CGRectMake(sizeOfTriangle/2.0 + horizontalTrianglesSpacing, sizeOfTriangle + TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
        triangle5.backgroundColor = [UIColor colorWithRed:26/255.0 green:144/255.0 blue:128/255.0 alpha:1.0];
        triangle5.layer.mask = [self createTriangleMask:YES];
        [self.astanaLogoView addSubview:triangle5];
        
        UIView *triangle7 = [[UIView alloc] initWithFrame:CGRectMake(sizeOfTriangle + 2 * horizontalTrianglesSpacing, sizeOfTriangle + TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
        triangle7.backgroundColor = [UIColor colorWithRed:152/255.0 green:204/255.0 blue:70/255.0 alpha:1.0];
        triangle7.layer.mask = [self createTriangleMask:NO];
        [self.astanaLogoView addSubview:triangle7];
        
        UIView *triangle8 = [[UIView alloc] initWithFrame:CGRectMake(sizeOfTriangle + 2 * horizontalTrianglesSpacing, 0, sizeOfTriangle, sizeOfTriangle)];
        triangle8.backgroundColor = [UIColor colorWithRed:201/255.0 green:221/255.0 blue:87/255.0 alpha:1.0];
        triangle8.layer.mask = [self createTriangleMask:YES];
        [self.astanaLogoView addSubview:triangle8];
        
        UIView *triangle6 = [[UIView alloc] initWithFrame:CGRectMake(1.5 * sizeOfTriangle + 3 * horizontalTrianglesSpacing, sizeOfTriangle + TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
        triangle6.backgroundColor = [UIColor colorWithRed:82/255.0 green:170/255.0 blue:108/255.0 alpha:1.0];
        triangle6.layer.mask = [self createTriangleMask:YES];
        [self.astanaLogoView addSubview:triangle6];
        
        UIView *triangle4 = [[UIView alloc] initWithFrame:CGRectMake(1.5 * sizeOfTriangle + 3 * horizontalTrianglesSpacing, 2 * sizeOfTriangle + 2 * TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
        triangle4.backgroundColor = [UIColor colorWithRed:41/255.0 green:144/255.0 blue:131/255.0 alpha:1.0];
        triangle4.layer.mask = [self createTriangleMask:NO];
        [self.astanaLogoView addSubview:triangle4];
        
        UIView *triangle2 = [[UIView alloc] initWithFrame:CGRectMake(2 * sizeOfTriangle + 4 * horizontalTrianglesSpacing, 2 * sizeOfTriangle + 2 * TRIANGLES_SPACING, sizeOfTriangle, sizeOfTriangle)];
        triangle2.backgroundColor = [UIColor colorWithRed:18/255.0 green:119/255.0 blue:111/255.0 alpha:1.0];
        triangle2.layer.mask = [self createTriangleMask:YES];
        [self.astanaLogoView addSubview:triangle2];
        
        [self.refreshContainerView addSubview:self.astanaLogoView];
        self.astanaLogoView.center = self.refreshContainerView.center;
        [self addSubview:self.refreshContainerView];
    }
}

#pragma mark - Actions

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!self.scrollView) {
        self.scrollView = scrollView;
    }
    
//    if (self.originalTopContentInset == 0) self.originalTopContentInset = self.scrollView.contentInset.top;
    self.originalTopContentInset = 0;
    
    self.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, self.realContentOffsetY*krelativeHeightFactor);
    
    // Don't animate if the pull to refresh not started
    if (self.animationState == AstanaRefreshControlStateNormal) {
        [self updateBarItemsWithProgress:self.animationProgress];
    }
}

- (void)scrollViewWillEndDragging {
    if (self.animationState == AstanaRefreshControlStateNormal && self.realContentOffsetY < -self.dropHeight) {
        
        if (self.animationProgress == 1) self.animationState = AstanaRefreshControlStateRefreshing;
        
        if (self.animationState == AstanaRefreshControlStateRefreshing) {
            
            UIEdgeInsets newInsets = self.scrollView.contentInset;
            newInsets.top = self.originalTopContentInset + self.dropHeight;
            CGPoint contentOffset = self.scrollView.contentOffset;
            
            [UIView animateWithDuration:0 animations:^(void) {
                self.scrollView.contentInset = newInsets;
                self.scrollView.contentOffset = contentOffset;
            }];
            
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            
            if ([self.target respondsToSelector:self.action])
                [self.target performSelector:self.action withObject:self];
            
#pragma clang diagnostic pop
            
            
            [self animateRefreshView];
        }
    }
}

- (CGFloat)realContentOffsetY {
    return self.scrollView.contentOffset.y + self.originalTopContentInset;
}

- (CGFloat)animationProgress {
    return MIN(1.f, MAX(0, fabs(self.realContentOffsetY)/self.dropHeight));
}

#pragma mark - Animation

- (void)updateBarItemsWithProgress:(CGFloat)progress {
    for (UIView *view in self.astanaLogoView.subviews) {
        NSInteger index = [self.astanaLogoView.subviews indexOfObject:view];
        CGFloat startPadding = (1 - self.internalAnimationFactor) / self.astanaLogoView.subviews.count * index;
        CGFloat endPadding = 1 - self.internalAnimationFactor - startPadding;
        
        if (progress == 1 || progress >= 1 - endPadding) {
            view.transform = CGAffineTransformIdentity;
        }
        else if (progress >= 0 && progress <= endPadding) {
            [self setHorizontalRandomness:self.horizontalRandomness dropHeight:self.dropHeight forView:view atIndex:index];
        }
        else {
            CGFloat realProgress;
            if (progress <= startPadding)
                realProgress = 0;
            else
                realProgress = MIN(1, (progress - startPadding) / self.internalAnimationFactor);
            view.transform = CGAffineTransformMakeTranslation([self.translationXList[index] integerValue] * (1 - realProgress), -self.dropHeight * (1 - realProgress));
            view.transform = CGAffineTransformRotate(view.transform, M_PI * (realProgress));
            view.transform = CGAffineTransformScale(view.transform, realProgress, realProgress);
        }
    }
}

- (void)animateRefreshView {
    if (self.animationState == AstanaRefreshControlStateRefreshing) {
        if (self.refreshAnimationIteration == self.astanaLogoView.subviews.count) {
            self.refreshAnimationIteration = 0;
        }
    
        UIView *currentView = self.astanaLogoView.subviews[self.refreshAnimationIteration];
        currentView.alpha = 0.4;
        
        [UIView animateWithDuration:0.45 animations:^{
            currentView.alpha = 1;
        } completion:^(BOOL finished) {
        }];
        
        if (self.animationState == AstanaRefreshControlStateRefreshing) {
            self.refreshAnimationIteration++;
            [self performSelector:@selector(animateRefreshView) withObject:nil afterDelay:0.15 inModes:@[NSRunLoopCommonModes]];
        }
    }
}

- (void)updateDisappearAnimation {
    if (self.disappearProgress >= 0 && self.disappearProgress <= 1) {
        self.disappearProgress -= 1/60.f / kdisappearDuration;
        //60.f means this method get called 60 times per second
        [self updateBarItemsWithProgress:self.disappearProgress];
    }
}

- (void)finishingLoading {
    self.animationState = AstanaRefreshControlStateDisappearing;
    
    UIEdgeInsets newInsets = self.scrollView.contentInset;
    newInsets.top = self.originalTopContentInset;
    [UIView animateWithDuration:kdisappearDuration animations:^(void) {
        self.scrollView.contentInset = newInsets;
    } completion:^(BOOL finished) {
        self.animationState = AstanaRefreshControlStateNormal;
        [self.displayLink invalidate];
        self.disappearProgress = 1;
    }];
    
    for (UIView *view in self.astanaLogoView.subviews) {
        [view.layer removeAllAnimations];
        view.alpha = 1;
    }
    self.refreshAnimationIteration = 0;
    
    self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(updateDisappearAnimation)];
    [self.displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    self.disappearProgress = 1;
}

#pragma mark - Helpers

- (CAShapeLayer *)createTriangleMask:(BOOL)isUp {
    CGFloat sizeOfTriangle = self.astanaLogoView.frame.size.width / 3.0 - 2 * TRIANGLES_SPACING;
    
    if (isUp) {
        // Mask path to form up triangle
        UIBezierPath *upTrianglePath = [UIBezierPath new];
        [upTrianglePath moveToPoint:(CGPoint){0, sizeOfTriangle}];
        [upTrianglePath addLineToPoint:(CGPoint){sizeOfTriangle/2.0, 0}];
        [upTrianglePath addLineToPoint:(CGPoint){sizeOfTriangle, sizeOfTriangle}];
        [upTrianglePath addLineToPoint:(CGPoint){0, sizeOfTriangle}];
        
        CAShapeLayer *upTriangleShapeMask = [CAShapeLayer new];
        upTriangleShapeMask.frame = CGRectMake(0, 0, sizeOfTriangle, sizeOfTriangle);
        upTriangleShapeMask.path = upTrianglePath.CGPath;
        
        return upTriangleShapeMask;
    }
    
    // Mask path to form down triangle
    UIBezierPath *downTrianglePath = [UIBezierPath new];
    [downTrianglePath moveToPoint:(CGPoint){0, 0}];
    [downTrianglePath addLineToPoint:(CGPoint){sizeOfTriangle, 0}];
    [downTrianglePath addLineToPoint:(CGPoint){sizeOfTriangle/2.0, sizeOfTriangle}];
    [downTrianglePath addLineToPoint:(CGPoint){0, 0}];
    
    CAShapeLayer *downTriangleShapeMask = [CAShapeLayer new];
    downTriangleShapeMask.frame = CGRectMake(0, 0, sizeOfTriangle, sizeOfTriangle);
    downTriangleShapeMask.path = downTrianglePath.CGPath;
    
    return downTriangleShapeMask;
}

- (void)setHorizontalRandomness:(int)horizontalRandomness dropHeight:(CGFloat)dropHeight forView:(UIView *)view atIndex:(NSInteger)index {
    int randomNumber = -horizontalRandomness + arc4random() % horizontalRandomness * 2;
    [self.translationXList setObject:@(randomNumber) atIndexedSubscript:index];
    view.transform = CGAffineTransformMakeTranslation(randomNumber, -dropHeight);
}

@end
