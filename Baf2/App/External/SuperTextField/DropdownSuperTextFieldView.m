//
//  DropdownSuperTextFieldView.m
//  ACB
//
//  Created by Almas Adilbek on 5/29/15.
//
//

#import "DropdownSuperTextFieldView.h"
//#import "MagicalRecord+Options.h"
#import "AAFieldActionSheet.h"

@implementation DropdownSuperTextFieldView {

}

- (void)initVars {
    [super initVars];

    _visibleItems = 7;
    _selectedIndex = -1;
    self.options = [[NSMutableArray alloc] init];
}

- (void)configUI {
    [super configUI];

    // Set icon.
    [self setRightImageViewWithImageName:@"icon-open-accordion"];

    // Make tappable.
    self.tappable = YES;
    [self hideClearButton];
}

- (void)tap
{
    if(self.superview) {
        [self.superview endEditing:YES];
    }

    if(_options.count == 0) {
        NSLog(@"DropdownSuperTextFieldView options data is empty");
        return;
    }

    NSString *title = self.textField.placeholder;
    if(self.title && self.title.length > 0) {
        title = self.title;
    }

    AAFieldActionSheet *actionSheet = [[AAFieldActionSheet alloc] initWithTitle:title];
    actionSheet.visibleItems = self.visibleItems;
    actionSheet.options = _options;
    actionSheet.selectedIndex = self.selectedIndex;
    [actionSheet show];

    __weak DropdownSuperTextFieldView * weakSelf = self;
    [actionSheet onValueChange:^(NSDictionary *option, NSInteger index) {

        if(_selectedIndex != index) {
            [weakSelf selectWithIndex:index];

            if(weakSelf.onOptionChangedBlock) {
                weakSelf.onOptionChangedBlock(option, index);
            }
        }

    }];

}

- (void)addOptionWithId:(id)optionId title:(NSString *)title {
    [self addOptionWithId:optionId title:title icon:nil];
}

- (void)addOptionWithId:(id)optionId title:(NSString *)title icon:(id)icon {
    [_options addObject:[AAFieldActionSheet optionWithId:optionId title:title icon:icon]];
}

- (void)clearOptions {
    [self unselect];
    [_options removeAllObjects];
}

-(void)onOptionChanged:(DropdownSuperTextFieldViewOptionChangedBlock)block {
    self.onOptionChangedBlock = block;
}

- (void)unselect {
    [self setSelectedIndex:-1];
}

- (void)selectOptionAtIndex:(NSUInteger)index {
    [self setSelectedIndex:index];
}

- (void)selectLastOption {
    [self setSelectedIndex:_options.count - 1];
}


#pragma mark -
#pragma mark Override

- (void)setSelectedIndex:(NSInteger)index {
    [self selectWithIndex:index];
}

- (void)enabled:(BOOL)enabled
{
    [super enabled:enabled];
    [rightView setAlpha:(CGFloat) (enabled ? 1.0 : 0.5)];
}

#pragma mark -
#pragma mark Helper

- (void)selectWithIndex:(NSInteger)index
{
    if(_options && _options.count > 0 && index < (int)_options.count ) {
        _selectedIndex = index;
        if (index >= 0) {
            NSDictionary *option = _options[(NSUInteger) index];
            self.optionId = option[AAFieldActionSheetOptionIDKey];
            [self setValue:option[AAFieldActionSheetOptionTitleKey]];
        } else {
            self.optionId = nil;
            [self setValue:nil];
        }
    }
}

@end
