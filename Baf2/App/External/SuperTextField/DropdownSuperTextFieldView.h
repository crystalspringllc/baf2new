//
//  DropdownSuperTextFieldView.h
//  ACB
//
//  Created by Almas Adilbek on 5/29/15.
//
//



#import "SuperTextFieldView.h"

static const NSString *AAFieldActionSheetOptionIDKey = @"id";
static const NSString *AAFieldActionSheetOptionTitleKey = @"title";
static const NSString *AAFieldActionSheetOptionIcon = @"icon";

static const NSString *AAFieldActionSheetOptionNoData = @"nodata";

@interface DropdownSuperTextFieldView : SuperTextFieldView

@property(nonatomic, strong) NSMutableArray *options;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, assign) NSUInteger visibleItems;
@property(nonatomic, assign) NSInteger selectedIndex;
@property(nonatomic, strong) DropdownSuperTextFieldViewOptionChangedBlock onOptionChangedBlock;

@property(nonatomic, copy) id optionId;

- (void)addOptionWithId:(id)optionId title:(NSString *)title;
- (void)addOptionWithId:(id)optionId title:(NSString *)title icon:(id)icon;

- (void)clearOptions;
- (void)onOptionChanged:(DropdownSuperTextFieldViewOptionChangedBlock)block;


- (void)unselect;

- (void)selectOptionAtIndex:(NSUInteger)index;
- (void)selectLastOption;

@end
