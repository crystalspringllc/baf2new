//
//  IinSuperTextFieldView.h
//  ACB
//
//  Created by Almas Adilbek on 7/14/15.
//
//



#import "SuperTextFieldView.h"

@interface IinSuperTextFieldView : SuperTextFieldView

- (BOOL)isFilled;

@end
