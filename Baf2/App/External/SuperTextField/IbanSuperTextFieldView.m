//
//  IbanSuperTextFieldView.m
//  BAF
//
//  Created by Almas Adilbek on 8/20/15.
//
//

#import "IbanSuperTextFieldView.h"

#define kIbanLength 20

@implementation IbanSuperTextFieldView

- (void)initView {
    [super initView];

    self.maxCharacters = kIbanLength;
}

- (BOOL)isFilled {
    return self.value.length == kIbanLength;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"uppercase works");
    NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];

    if (lowercaseCharRange.location != NSNotFound) {
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }

    return YES;
}


@end
