//
//  SuperTextFieldView+Styles.h
//  ACB
//
//  Created by Almas Adilbek on 5/27/15.
//
//

#import "SuperTextFieldView.h"

@interface SuperTextFieldView (Styles)

- (void)defaultStyle;
- (void)disabledStyle;

@end
