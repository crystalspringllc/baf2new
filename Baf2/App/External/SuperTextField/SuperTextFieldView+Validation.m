//
//  SuperTextFieldView+Validation.m
//  ACB
//
//  Created by Almas Adilbek on 5/27/15.
//
//

#import "SuperTextFieldView+Styles.h"
#import "NSString+Ext.h"

@implementation SuperTextFieldView (Validation)

-(BOOL)validate {
    if([self value].length == 0) {
        [self focus];
        return NO;
    }
    return YES;
}

- (BOOL)validate:(NSString *)message {
    if(![self validate]) {
        [UIHelper showAlertWithMessage:message];
        return NO;
    }
    return YES;
}

-(BOOL)validatePhoneNumber
{
    NSString *value = [self validPhoneNumber];
    if(value.length == 0) {
        [self focus];
        [UIHelper showAlertWithMessageTitle:@"" message:NSLocalizedString(@"enter_phone_number", nil)];
        return NO;
    }
    if(value.length != 10) {
        [self focus];
        [UIHelper showAlertWithMessageTitle:@"" message:NSLocalizedString(@"incorrect_phone_number", nil)];
        return NO;
    }
    return YES;
}

- (BOOL)validateIin {
    return [self validateIinWithMessage:NSLocalizedString(@"type_your_iin", nil)];
}

- (BOOL)validateIBAN {
    return [self validateIBAN:YES];
}

- (BOOL)validateIBAN:(BOOL)focus {
    if([self value].length == 20) return YES;
    else if([self value].length > 0) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_correct_iban_value", nil)];
        if(focus) [self focus];
        return NO;
    }
    return [self validateWithMessage:NSLocalizedString(@"enter_iban", nil)];
}

- (BOOL)validateBIN {
    return [self validateBIN:YES];
}

- (BOOL)validateBIN:(BOOL)focus {
    if([self value].length == 12) return YES;
    else if([self value].length > 0) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_correct_bin", nil)];
        if(focus) [self focus];
        return NO;
    }
    return [self validateWithMessage:NSLocalizedString(@"Введите БИН", nil)];
}

- (BOOL)validateIinWithMessage:(NSString *)message
{
    if(![self validate]) {
        [UIHelper showAlertWithMessage:message];
        return NO;
    }

    if(![[self value] isNumeric]) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"iin_must_contain_only_numbers", nil)];
        [self focus];
        return NO;
    }

    if([self value].length != 12) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"entered_incorrect_iin", nil)];
        [self focus];
        return NO;
    }
    return YES;
}

- (BOOL)validateEmail
{
    NSString *emailRegex =
            @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
                    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
                    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
                    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
                    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
                    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
                    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    if(![emailTest evaluateWithObject:[self value]]) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_correct_email_number", nil)];
        [self focus];
        return NO;
    }

    return YES;
}

- (BOOL)validateWithMessage:(NSString *)message
{
    if(![self validate]) {
        [UIHelper showAlertWithMessage:message];
        return NO;
    }

    return YES;
}

#pragma mark -
#pragma mark Format corrections

- (NSString *)validPhoneNumber {
    NSString *value = [self value];
    value = [value stringByReplacingOccurrencesOfString:@"+7" withString:@""];
    value = [value stringByReplacingOccurrencesOfString:@"+" withString:@""];
    value = [value stringByReplacingOccurrencesOfString:@" " withString:@""];
    value = [value stringByReplacingOccurrencesOfString:@"(" withString:@""];
    value = [value stringByReplacingOccurrencesOfString:@")" withString:@""];

    return value;
}

@end