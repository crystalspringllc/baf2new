//
//  SuperTextFieldView.m
//  ACB
//
//  Created by Almas Adilbek on 5/27/15.
//
//

#import <PureLayout/ALView+PureLayout.h>
#import <PureLayout/NSLayoutConstraint+PureLayout.h>
#import <UIImageView+AFNetworking.h>
#import "SuperTextFieldView.h"
#import "SuperTextFieldView+Styles.h"
#import "UIView+AAPureLayout.h"

// Constants.
static const CGFloat kTextFieldSideInset = 10;

@interface SuperTextFieldView ()
@property(nonatomic, strong) NSLayoutConstraint *containerBottomConstraint;

@property (nonatomic, strong) UIImageView *successFailureImageView;
@end

@implementation SuperTextFieldView {

    // Constraints.
    NSLayoutConstraint *textFieldPinRightConstraint;
    NSLayoutConstraint *textFieldPinLeftConstraint;
    NSLayoutConstraint *rightViewPinLeftWithTextFieldConstraint;

    // Blocks.
    SuperTextFieldViewOnValueChangedBlock _onValueChangedBlock;
    SuperTextFieldViewValueChangeBlock _onValueChangeBlock;

    // Mix.
    NSString *previousValue;
    UIControl *tapControl;
    UILabel *messageLabel;
}

- (id)init {
    self = [super init];
    if(self) {
        [self initView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self initView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    [self initVars];
    [self configUI];
}

- (void)initVars
{
    self.maxCharacters = NSIntegerMax;

    _placeholderColor = [UIColor fromRGB:0xC2C6C2];
    _textFieldIconSpacing = 8;
    _textFieldRightViewSpacing = 8;
}

#pragma mark -
#pragma mark Actions

- (void)tap {
    // Inherit in subclasses.
}

#pragma mark -
#pragma mark TextField Methods

- (NSString *)value {
    return [_textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (CGFloat)floatValue {
    NSString *stringValue = [[self value] stringByReplacingOccurrencesOfString:@"," withString:@"."];
    stringValue = [stringValue stringByReplacingOccurrencesOfString:@" " withString:@""];
    return [stringValue floatValue];
}

- (double)doubleValue {
    NSString *stringValue = [[self value] stringByReplacingOccurrencesOfString:@"," withString:@"."];
    stringValue = [stringValue stringByReplacingOccurrencesOfString:@" " withString:@""];
    return [stringValue doubleValue];
}

- (void)setValue:(NSString *)value {
    _textField.text = value ?: @"";
}

- (void)setPlaceholder:(NSString *)placeholder {
    _textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: self.placeholderColor}];
}

- (void)clear {
    if(_textField) _textField.text = @"";
}

- (void)enabled:(BOOL)enabled {
    self.userInteractionEnabled = enabled;
    _textField.enabled = enabled;
    if(tapControl) {
        tapControl.enabled = enabled;
    }
}

- (void)focus {
    [_textField becomeFirstResponder];
}

- (void)hideClearButton {
    self.textField.clearButtonMode = UITextFieldViewModeNever;
}

- (BOOL)isset {
    return [self value].length > 0;
}

- (void)setFont:(UIFont *)font {
    _textField.font = font;
}

- (void)setTextColor:(UIColor *)color {
    _textField.textColor = color;
}


#pragma mark -
#pragma mark UITextField Delegates

- (void)textFieldDidChange:(id)textFieldDidChange
{
    // Check if entered text field length is more that max allowed length.
    NSString *text = [_textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if(text.length > self.maxCharacters) {
        text = [text substringToIndex:self.maxCharacters];
        _textField.text = text;
        return;
    }

    // Call value change block
    if (_onValueChangeBlock) {
        _onValueChangeBlock(text);
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    previousValue = textField.text;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if(_onValueChangedBlock && previousValue && ![previousValue isEqual:textField.text]) _onValueChangedBlock(self.value);
    if ([self.superTextFieldDelegate respondsToSelector:@selector(superTextFieldDidFinishEditing:)]) {
        [self.superTextFieldDelegate superTextFieldDidFinishEditing:textField];
    }
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(self.numeric || self.decimalNumeric)
    {
        NSString *fulltext = [textField.text stringByAppendingString:string];
        NSString *charactersSetString = @"0123456789";

        // For decimal keyboard, allow "dot" and "comma" characters.
        if(self.decimalNumeric) {
            charactersSetString = [charactersSetString stringByAppendingString:@".,"];
        }

        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:charactersSetString];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:fulltext];

        // If typed character is out of Set, ignore it.
        BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        if(!stringIsValid) {
            return NO;
        }

        if(self.decimalNumeric)
        {
            NSString *currentText = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

            // Change the "," (appears in other locale keyboards, such as russian) key of "."
            fulltext = [fulltext stringByReplacingOccurrencesOfString:@"," withString:@"."];

            // Check the statements of decimal value.
            if([fulltext isEqualToString:@"."]) {
                textField.text = @"0.";
                return NO;
            }

            if([fulltext rangeOfString:@".."].location != NSNotFound) {
                textField.text = [fulltext stringByReplacingOccurrencesOfString:@".." withString:@"."];
                return NO;
            }

            // If second dot is typed, ignore it.
            NSArray *dots = [fulltext componentsSeparatedByString:@"."];
            if(dots.count > 2) {
                textField.text = currentText;
                return NO;
            }

            // If first character is zero and second character is > 0, replace first with second. 05 -> 5;
            if(fulltext.length == 2) {
                if([[fulltext substringToIndex:1] isEqualToString:@"0"] && ![fulltext isEqualToString:@"0."]) {
                    textField.text = [fulltext substringWithRange:NSMakeRange(1, 1)];
                    return NO;
                }
            }
        }
    }

    // Check the max characters typed.
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;

    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;

    return newLength <= _maxCharacters || returnKey;
}

#pragma mark -
#pragma mark Blocks

- (void)onValueChanged:(SuperTextFieldViewOnValueChangedBlock)block {
    _onValueChangedBlock = block;
}

- (void)onValueChange:(SuperTextFieldViewValueChangeBlock)block {
    _onValueChangeBlock = block;
}

#pragma mark -
#pragma mark Prefix

- (void)setPrefix:(NSString *)prefix {
    [self setPrefix:prefix font:nil];
}

- (void)setPrefix:(NSString *)prefix font:(UIFont *)font {
    [self setPrefix:prefix font:font textColor:nil];
}

- (void)setPrefix:(NSString *)prefix textColor:(UIColor *)color {
    [self setPrefix:prefix font:nil textColor:color];
}

- (void)setPrefix:(NSString *)prefix font:(UIFont *)font textColor:(UIColor *)color
{
    if(!font) font = _textField.font;
    if(!color) color = _textField.textColor;

    if(prefixLabel) {
        [prefixLabel removeFromSuperview];
        prefixLabel = nil;
    }

    prefixLabel = [[UILabel alloc] init];
    prefixLabel.backgroundColor = [UIColor clearColor];
    prefixLabel.font = font;
    prefixLabel.textColor = color;
    prefixLabel.text = prefix;
    [prefixLabel sizeToFit];

    if(iconView) {
        [iconView removeFromSuperview];
    }

    [self setLeftView:prefixLabel];
}

#pragma mark -
#pragma mark Postfix

- (void)setPostfix:(NSString *)prefix {
    [self setPostfix:prefix font:nil];
}

- (void)setPostfix:(NSString *)prefix font:(UIFont *)font {
    [self setPostfix:prefix font:font textColor:nil];
}

- (void)setPostfix:(NSString *)prefix textColor:(UIColor *)color {
    [self setPostfix:prefix font:nil textColor:color];
}

- (void)setPostfix:(NSString *)postfix font:(UIFont *)font textColor:(UIColor *)color
{
    if(!font) font = _textField.font;
    if(!color) color = _textField.textColor;

    if(postfixLabel) {
        [postfixLabel removeFromSuperview];
        postfixLabel = nil;
    }

    postfixLabel = [[UILabel alloc] init];
    postfixLabel.backgroundColor = [UIColor clearColor];
    postfixLabel.font = font;
    postfixLabel.textColor = color;
    postfixLabel.text = postfix;
    [postfixLabel sizeToFit];
    [self addSubview:postfixLabel];

    if(iconView) {
        [iconView removeFromSuperview];
    }

    [self setRightView:postfixLabel];
}


#pragma mark -
#pragma mark Keyboard

- (void)makeEmailKeyboard {
    _textField.keyboardType = UIKeyboardTypeEmailAddress;
}

- (void)makePhoneNumberKeyboard {
    _textField.keyboardType = UIKeyboardTypePhonePad;
}

- (void)makePasswordField {
    _textField.secureTextEntry = YES;
}

- (void)makeNumberKeyboard {
    _textField.keyboardType = UIKeyboardTypeDecimalPad;
}

#pragma mark -
#pragma mark UI Methods

- (void)setRightView:(UIView *)view
{
    if(rightView) {
        [rightView removeFromSuperview];
        rightView = nil;
        [self removeCustomConstraint:rightViewPinLeftWithTextFieldConstraint];
    }

    [self insertSubview:view aboveSubview:_textField];
    [view aa_autoSetDimensions];
    [view aa_centerVertical];
    [view aa_superviewRight:kTextFieldSideInset];
    
    rightView = view;

    // Change TF right constraint.
    [self removeCustomConstraint:textFieldPinRightConstraint];
    textFieldPinRightConstraint = nil;

    rightViewPinLeftWithTextFieldConstraint = [view aa_pinHorizontalAfterView:_textField offset:_textFieldRightViewSpacing];
}

- (void)setRightImageViewWithImageName:(NSString *)imageName {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [self setRightView:imageView];
}

- (void)setRightImageViewWithImageUrl:(NSString *)url {
    [self setIconWithURL:url left:NO];
}

- (void)setLeftView:(UIView *)view
{
    [self addSubview:view];
    [view aa_autoSetDimensions];
    [view aa_centerVertical];
    [view aa_superviewLeft:kTextFieldSideInset];

    // Change TF left constraint.
    [self removeCustomConstraint:textFieldPinLeftConstraint];
    textFieldPinLeftConstraint = [_textField aa_pinHorizontalAfterView:view offset:_textFieldIconSpacing];
}

- (void)setIcon:(NSString *)iconName {
    [self setIcon:iconName fixedWidth:0];
}

- (void)setIcon:(NSString *)iconName fixedWidth:(CGFloat)width {
    if(iconView) {
        [iconView removeFromSuperview];
    }
    iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:iconName]];

    if(width > 0)
    {
        UIView *iconViewContainer = [UIView new];
        iconViewContainer.width = width;
        iconViewContainer.height = iconView.bounds.size.height;
        [iconViewContainer addSubview:iconView];
        iconView.centerX = iconViewContainer.middleX;

        [self setLeftView:iconViewContainer];
    }
    else
    {
        [self setLeftView:iconView];
    }
}

- (void)setIconWithURL:(NSString *)url {
    [self setIconWithURL:url left:YES];
}

- (void)setIconWithURL:(NSString *)url left:(BOOL)left {
    if(url) {
        [self removeLeftIcon];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [imageView setImageWithURL:[[NSURL alloc] initWithString:url]];

        if(left) {
            imageView.tag = SuperTextFieldLeftIconTag;
            [self setLeftView:imageView];
        } else {
            [self setRightView:imageView];
        }
    }
}

- (void)setRightButtonWithIcon:(UIImage *)icon target:(id)target selector:(SEL)sel
{
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    UIImage *rightIcon = icon;
    rightButton.width = rightIcon.size.width;
    rightButton.height = rightIcon.size.height;
    [rightButton setImage:rightIcon forState:UIControlStateNormal];
    [self addSubview:rightButton];

    [self setRightView:rightButton];
}

- (void)removeLeftIcon {
    for (UIView *subview in self.subviews) {
        if (subview.tag == SuperTextFieldLeftIconTag) {
            [subview removeFromSuperview];
        }
    }
    [self removeCustomConstraint:textFieldPinLeftConstraint];
    textFieldPinLeftConstraint = [_textField autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:kTextFieldSideInset];
}

#pragma mark -
#pragma mark Loader

-(void)startLoading
{
    if(!self.isLoading) 
    {
        // Hide elements.
        [self hideComponents:YES];

        self.loading = YES;
        self.userInteractionEnabled = NO;

        loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self addSubview:loader];
        [loader aa_autoSetDimensions];
        [loader autoCenterInSuperview];
        [loader startAnimating];
    }
}

-(void)stopLoading
{
    if(self.isLoading) {
        [self hideComponents:NO];

        self.loading = NO;
        self.userInteractionEnabled = YES;

        if(loader) {
            [loader removeFromSuperview];
            loader = nil;
        }
    }
}

#pragma mark -
#pragma mark Overrides

// To make TF tappable as a button. For date picker, drop down text fields.
- (void)setTappable:(BOOL)tappable {
    _tappable = tappable;
    
    if(_tappable) {
        if(!tapControl) {
            tapControl = [UIControl newAutoLayoutView];
            [tapControl addTarget:self action:@selector(tap) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:tapControl];
            [tapControl autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
        }
    } else {
        if(tapControl) {
            [tapControl removeFromSuperview];
            tapControl = nil;
        }
    }
}

- (void)setNumeric:(BOOL)numeric {
    _numeric = numeric;
    self.textField.keyboardType = UIKeyboardTypeNumberPad;

    // Once set numeric YES, cancel decimalNumeric.
    if(_numeric) _decimalNumeric = NO;
}

- (void)setDecimalNumeric:(BOOL)decimalNumeric {
    _decimalNumeric = decimalNumeric;
    self.textField.keyboardType = UIKeyboardTypeDecimalPad;

    // Once set decimalNumeric YES, cancel numeric.
    if(_decimalNumeric) _numeric = NO;
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor {
    _placeholderColor = placeholderColor;
    [self setPlaceholder:_textField.placeholder];
}

#pragma mark -
#pragma mark Helper

- (void)removeCustomConstraint:(NSLayoutConstraint *)constraint {
    if(constraint) {
        [constraint autoRemove];
    }
}

- (void)hideComponents:(BOOL)hide {
    _textField.hidden = hide;
    if(iconView) iconView.hidden = hide;
    if(rightView) rightView.hidden = hide;
    if(prefixLabel) prefixLabel.hidden = hide;
}

#pragma mark -
#pragma mark Config

- (void)configUI
{
    // View configs.
    self.backgroundColor = [UIColor clearColor];
    self.tintColor = [UIColor blackColor];

    // Create background view.
    backgroundView = [UIImageView newAutoLayoutView];
    [self addSubview:backgroundView];
    [backgroundView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];

    // Create UITextField.
    self.textField = [UITextField newAutoLayoutView];
    _textField.delegate = self;
    _textField.font = [UIFont helveticaNeue:16];
    _textField.textColor = [UIColor blackColor];
    _textField.autocorrectionType = UITextAutocorrectionTypeNo;
    _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self addSubview:_textField];

    [_textField aa_superviewTop:0];
    self.containerBottomConstraint = [_textField aa_superviewBottom:0];

    textFieldPinLeftConstraint = [_textField autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:kTextFieldSideInset];
    textFieldPinRightConstraint = [_textField autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:kTextFieldSideInset];

    // TF event.
    [_textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    // Set default style.
    [self defaultStyle];
}

#pragma mark - hint

- (void)setHint:(NSString *)text {
    [self setHint:text color:[UIColor grayColor]];
}

- (void)setError:(NSString *)text {
    [self setHint:text color:[UIColor redColor]];
}

- (void)removeHint
{
    if(self.hintLabel) {
        [self.hintLabel removeFromSuperview];
        self.hintLabel = nil;

        // Set bottom constraint.
        [self.containerBottomConstraint autoRemove];
        self.containerBottomConstraint = [self.bottomLine aa_superviewBottom:0];
    }
}

- (void)setHint:(NSString *)text color:(UIColor *)color
{
    if(self.hintLabel) {
        [self.hintLabel removeFromSuperview];
        self.hintLabel = nil;
    }

    self.hintLabel = [UILabel newAutoLayoutView];
    self.hintLabel.backgroundColor = [UIColor clearColor];
    self.hintLabel.font = [UIFont systemFontOfSize:11];
    self.hintLabel.textColor = color;
    self.hintLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.hintLabel.numberOfLines = 0;
    self.hintLabel.text = text;
    [self addSubview:self.hintLabel];

    [self.hintLabel aa_superviewLeft:10];
    [self.hintLabel aa_superviewRight:10];
    [self.hintLabel aa_pinUnderView:self.textField offset:3];

    [self.containerBottomConstraint autoRemove];
    self.containerBottomConstraint = [self.hintLabel aa_superviewBottom:0];
}

#pragma mark - Dealloc

- (void)dealloc {
    _onValueChangedBlock = nil;
    _onValueChangeBlock = nil;
}


@end
