//
//  SuperTextFieldView.h
//  ACB
//
//  Created by Almas Adilbek on 5/27/15.
//
//

typedef void (^SuperTextFieldViewOnValueChangedBlock)(NSString *value);
typedef void (^SuperTextFieldViewValueChangeBlock)(NSString *value);
typedef void (^DropdownSuperTextFieldViewOptionChangedBlock)(NSDictionary *option, NSInteger index);

static const int SuperTextFieldLeftIconTag = 101;

@protocol SuperTextFieldViewDelegate <NSObject>

@optional
- (void)superTextFieldDidFinishEditing:(UITextField *)textField;

@end

@interface SuperTextFieldView : UIView <UITextFieldDelegate> {
    // The text field background
    UIImageView *backgroundView;

    // UI.
    UIImageView *iconView;
    UILabel *prefixLabel;
    UILabel *postfixLabel;
    UIActivityIndicatorView *loader;
    UIView *rightView;
}

// UI
@property(nonatomic, strong) UITextField *textField;
@property(nonatomic, strong) UIView *bottomLine;
@property(nonatomic, strong) UILabel *hintLabel;

// Params
@property(nonatomic, assign, getter=isTappable) BOOL tappable;
@property(nonatomic, assign, getter=isLoading) BOOL loading;
@property(nonatomic, assign) NSUInteger maxCharacters;
@property(nonatomic, assign) BOOL numeric;
@property(nonatomic, assign) BOOL decimalNumeric;
@property(weak) id<SuperTextFieldViewDelegate> superTextFieldDelegate;

// Config
@property(nonatomic, strong) UIColor *placeholderColor;
@property(nonatomic, assign) CGFloat textFieldRightViewSpacing;
@property(nonatomic, assign) CGFloat textFieldIconSpacing;

- (void)initView;

// The initializer methods.
- (void)initVars;
- (void)configUI;

- (void)setHint:(NSString *)text;
- (void)setError:(NSString *)text;
- (void)removeHint;

// On self tap, if tappable YES
- (void)tap;

// TextField Methods
- (NSString *)value;
- (CGFloat)floatValue;
- (double)doubleValue;

- (void)setValue:(NSString *)value;
- (void)setPlaceholder:(NSString *)placeholder;
- (void)clear;
- (void)enabled:(BOOL)enabled;
- (void)focus;
- (void)hideClearButton;

- (BOOL)isset;

- (void)setFont:(UIFont *)font;
- (void)setTextColor:(UIColor *)color;

// Prefix
- (void)setPrefix:(NSString *)prefix;
- (void)setPrefix:(NSString *)prefix font:(UIFont *)font;
- (void)setPrefix:(NSString *)prefix textColor:(UIColor *)color;
- (void)setPrefix:(NSString *)prefix font:(UIFont *)font textColor:(UIColor *)color;

// Postfix
- (void)setPostfix:(NSString *)prefix;
- (void)setPostfix:(NSString *)prefix font:(UIFont *)font;
- (void)setPostfix:(NSString *)prefix textColor:(UIColor *)color;
- (void)setPostfix:(NSString *)prefix font:(UIFont *)font textColor:(UIColor *)color;

// Keyboard
- (void)makeEmailKeyboard;
- (void)makePhoneNumberKeyboard;
- (void)makePasswordField;
- (void)makeNumberKeyboard;

// UI Methods
- (void)setRightView:(UIView *)view;
- (void)setRightImageViewWithImageName:(NSString *)imageName;
- (void)setRightImageViewWithImageUrl:(NSString *)url;
- (void)setIcon:(NSString *)iconName;
- (void)setIcon:(NSString *)iconName fixedWidth:(CGFloat)width;
- (void)setIconWithURL:(NSString *)url;
- (void)setRightButtonWithIcon:(UIImage *)icon target:(id)target selector:(SEL)sel;
- (void)removeLeftIcon;

// Loading.
- (void)startLoading;
- (void)stopLoading;

// Blocks
- (void)onValueChanged:(SuperTextFieldViewOnValueChangedBlock)block;
- (void)onValueChange:(SuperTextFieldViewValueChangeBlock)block;


@end
