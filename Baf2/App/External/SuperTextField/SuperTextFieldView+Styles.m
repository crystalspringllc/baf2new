//
//  SuperTextFieldView+Styles.m
//  ACB
//
//  Created by Almas Adilbek on 5/27/15.
//
//

#import <PureLayout/ALView+PureLayout.h>
#import "SuperTextFieldView+Styles.h"
#import "UIView+AAPureLayout.h"

@implementation SuperTextFieldView (Styles)

- (void)defaultStyle {
    if (!self.bottomLine) {
        self.bottomLine = [UIView newAutoLayoutView];
        [backgroundView addSubview:self.bottomLine];
        self.bottomLine.backgroundColor = [UIColor fromRGB:0xE3E3E3];
        [self.bottomLine autoSetDimension:ALDimensionHeight toSize:1];
        [self.bottomLine aa_superviewLeft:0];
        [self.bottomLine aa_superviewRight:0];
        [self.bottomLine aa_superviewBottom:0];
    } else {
        self.bottomLine.backgroundColor = [UIColor fromRGB:0xE3E3E3];
    }

}

- (void)disabledStyle {
//    backgroundView.image = [[UIImage imageNamed:@"inputField-disabled-pattern"] stretchableImageWithLeftCapWidth:15 topCapHeight:15];

    if (self.bottomLine) {
        self.bottomLine.backgroundColor = [UIColor clearColor];
    }

}

@end
