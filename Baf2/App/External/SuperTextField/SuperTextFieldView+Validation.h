//
// Created by Almas Adilbek on 6/1/15.
//

#import <Foundation/Foundation.h>
#import "SuperTextFieldView.h"

@interface SuperTextFieldView (Validation)

- (BOOL)validate;
- (BOOL)validate:(NSString *)message;

- (BOOL)validatePhoneNumber;
- (BOOL)validateIin;
- (BOOL)validateIBAN;
- (BOOL)validateIBAN:(BOOL)focus;
- (BOOL)validateBIN;
- (BOOL)validateBIN:(BOOL)focus;
- (BOOL)validateIinWithMessage:(NSString *)message;
- (BOOL)validateEmail;
- (BOOL)validateWithMessage:(NSString *)message;
- (NSString *)validPhoneNumber;

@end