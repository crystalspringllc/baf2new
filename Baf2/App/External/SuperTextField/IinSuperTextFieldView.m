//
//  IinSuperTextFieldView.m
//  ACB
//
//  Created by Almas Adilbek on 7/14/15.
//
//

#import "IinSuperTextFieldView.h"

#define kIinLength 12

@implementation IinSuperTextFieldView

- (void)initView {
    [super initView];

    self.maxCharacters = kIinLength;
    self.numeric = YES;
}

- (BOOL)isFilled {
    return self.value.length == kIinLength;
}


@end
