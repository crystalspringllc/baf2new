//
//  DatePickerSuperTextFieldView.m
//  ACB
//
//  Created by Almas Adilbek on 5/28/15.
//
//

#import "DatePickerSuperTextFieldView.h"

@interface DatePickerSuperTextFieldView () <SuperTextFieldViewDelegate>

@end

@implementation DatePickerSuperTextFieldView {
    UIView *dim;
}

- (void)initVars {
    [super initVars];

    self.superTextFieldDelegate = self;
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"dd.MM.yyyy"];
}

- (void)configUI {
    [super configUI];

    // Set icon.
    [self setRightImageViewWithImageName:@"calendar_gray_icn"];

    // Make tappable.
    self.tappable = YES;
    [self hideClearButton];
}

#pragma mark -
#pragma mark Actions

- (void)tap {
    if(self.superview) {
        [self.superview endEditing:YES];
    }
    [self showDatePicker];

    // If value isset, set the date picker date.
    if(self.isset) {
        NSDate *date = [self.dateFormatter dateFromString:self.value];
        if(date) {
            [self.datePicker setDate:date animated:NO];
        }
    }
}

- (void)datePickerDoneClicked
{
    [UIView animateWithDuration:0.25 animations:^{
        dim.alpha = 0;
    } completion:^(BOOL finished) {
        [dim removeFromSuperview];
    }];

    [self.textField resignFirstResponder];
}

- (void)datePickerDateChanged:(UIDatePicker *)datePicker
{
    self.date = [datePicker date];
    [self setValue:[self.dateFormatter stringFromDate:[datePicker date]]];
    
    if ([self.delegate respondsToSelector:@selector(datePickerDateChanged:)]) {
        [self.delegate datePickerDateChanged:datePicker];
    }
}

#pragma mark - SuperTextFieldViewDelegate

- (void)superTextFieldDidFinishEditing:(UITextField *)textField {
    [UIView animateWithDuration:0.25 animations:^{
        dim.alpha = 0;
    } completion:^(BOOL finished) {
        [dim removeFromSuperview];
    }];
}

#pragma mark - Blocks

- (void)onValueChanged:(SuperTextFieldViewOnValueChangedBlock)block {
    [UIView animateWithDuration:0.25 animations:^{
        dim.alpha = 0;
    } completion:^(BOOL finished) {
        [dim removeFromSuperview];
    }];
    
    [super onValueChanged:block];
    
}

#pragma mark -
#pragma mark Helper

- (void)showDatePicker
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];

    // Create dim.
    dim = [[UIView alloc] initWithFrame:screenRect];
    dim.backgroundColor = [UIColor blackColor];
    dim.alpha = 0.0;
    [window addSubview:dim];

    [UIView animateWithDuration:0.2 animations:^{
        dim.alpha = 0.15;
    }];

    // Create date picker view.
    self.datePicker = [[UIDatePicker alloc] init];
    [self.datePicker setDatePickerMode:UIDatePickerModeDate];
    [self.datePicker addTarget:self action:@selector(datePickerDateChanged:) forControlEvents:UIControlEventValueChanged];
    self.datePicker.backgroundColor = [UIColor whiteColor];

    UIView *datePickerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenRect.size.width, self.datePicker.height + (self.isToolbarHidden ? 0 : 44))];
    datePickerView.backgroundColor = [UIColor clearColor];
    [datePickerView addSubview:self.datePicker];

    //Toolbar
    if (!self.isToolbarHidden) {
        UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, screenRect.size.width, 44)];
        pickerToolbar.barStyle = UIBarStyleDefault;
        pickerToolbar.tintColor = self.textField.textColor;
        [pickerToolbar sizeToFit];

        NSMutableArray *barItems = [NSMutableArray array];
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [barItems addObject:flexSpace];

        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDoneClicked)];
        [barItems addObject:doneButton];

        [pickerToolbar setItems:barItems animated:NO];
        [datePickerView addSubview:pickerToolbar];
    }
    
    // Set input view.
    self.textField.inputView = self.datePicker;

    // Show keyboard.
    [self focus];
    
    // Delegate
    if ([self.delegate respondsToSelector:@selector(datePickerShowed:)]) {
        [self.delegate datePickerShowed:self.datePicker];
    }
}

@end
