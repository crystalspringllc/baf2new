//
//  DatePickerSuperTextFieldView.h
//  ACB
//
//  Created by Almas Adilbek on 5/28/15.
//
//


#import "SuperTextFieldView.h"

@protocol DatePickerSuperTextFieldViewDelegate <NSObject>

@optional
- (void)datePickerDateChanged:(UIDatePicker *)datePicker;
- (void)datePickerShowed:(UIDatePicker *)datePicker;

@end

@interface DatePickerSuperTextFieldView : SuperTextFieldView

@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (weak) id<DatePickerSuperTextFieldViewDelegate> delegate;
@property (nonatomic) BOOL isToolbarHidden;

@end
