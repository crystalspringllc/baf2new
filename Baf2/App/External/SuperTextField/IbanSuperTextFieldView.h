//
//  IbanSuperTextFieldView.h
//  BAF
//
//  Created by Almas Adilbek on 8/20/15.
//
//

#import "SuperTextFieldView.h"

@interface IbanSuperTextFieldView : SuperTextFieldView

- (BOOL)isFilled;

@end
