//
//  TheButton.m
//  ACB
//
//  Created by nmaksut on 25.05.15.
//
//

#import "TheButton.h"
#import "UIButton+Styles.h"
#import "UIFont+Custom.h"

@interface TheButton ()
@end

@implementation TheButton

- (void)commonInit
{
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.font = [UIFont helveticaNeue:16];
}

- (instancetype)init
{
    self = [super init];
    [self commonInit];
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInit];
    }
    return self;
}

@end
