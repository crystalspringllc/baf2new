//
//  BlueButton.m
//  BAF
//
//  Created by Almas Adilbek on 6/9/15.
//
//

#import "BlueButton.h"
#import "UIButton+Styles.h"

@implementation BlueButton

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self blueStyle];
    }
    return self;
}

@end
