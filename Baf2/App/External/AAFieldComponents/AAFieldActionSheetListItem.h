//
//  AAFieldActionSheetListItem.h
//  AAFieldComponents
//
//  Created by Almas Adilbek on 7/24/13.
//  Copyright (c) 2013 GoodApp inc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^AAFieldActionSheetListItemOnTap)(NSUInteger index);

@interface AAFieldActionSheetListItem : UIView {
    NSInteger optionIndex;
    CGFloat iconViewSize;
}

@property (nonatomic, assign) NSInteger optionIndex;

- (id)initWithTitle:(NSString *)title width:(CGFloat)width;
-(void)setIcon:(id)icon;
-(void)select;

-(void)onTap:(AAFieldActionSheetListItemOnTap)block;

@end
