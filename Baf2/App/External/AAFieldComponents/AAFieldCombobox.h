//
//  AAFieldCombobox.h
//  AAFieldComponents
//
//  Created by Almas Adilbek on 7/23/13.
//  Copyright (c) 2013 GoodApp inc. All rights reserved.
//

#import "AAField.h"

typedef void(^AAFieldComboboxOnValueChangeBlock)(NSDictionary *option);

@interface AAFieldCombobox : AAField {
    NSInteger selectedIndex;
    NSInteger visibleItems;
}

@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, assign) NSInteger visibleItems;

- (NSString *)optionValue;


- (void)addOptionWithID:(NSString *)optionID title:(NSString *)optionTitle icon:(id)icon; // Icon may be URL or UIImage object
- (void)addOptionWithId:(NSString *)optionID type:(NSString *)type title:(NSString *)optionTitle icon:(id)icon;

- (void)selectItemAtIndex:(NSInteger)index;
- (void)selectByOptionId:(NSString *)value;
- (void)removeAllOptions;
- (void)unselect;
- (void)enabled:(BOOL)enabled;

- (BOOL)isset;

- (NSInteger)countOptions;

- (void)onValueChange:(AAFieldComboboxOnValueChangeBlock)block;

@end
