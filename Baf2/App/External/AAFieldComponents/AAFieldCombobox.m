//
//  AAFieldCombobox.m
//  AAFieldComponents
//
//  Created by Almas Adilbek on 7/23/13.
//  Copyright (c) 2013 GoodApp inc. All rights reserved.
//

#import "AAFieldCombobox.h"
#import "AAFieldActionSheet.h"

@implementation AAFieldCombobox {
    UIImageView *listIcon;
    NSMutableArray *options;
    AAFieldComboboxOnValueChangeBlock onValueChangeBlock;
}

@synthesize selectedIndex, visibleItems;

- (id)initWithWidth:(CGFloat)width
{
    self = [super initWithWidth:width];
    if (self) {
        [self initConfig2];
    }
    return self;
}

- (void)initConfig2 {
    self.selectedIndex = -1;
    self.visibleItems = 5;
    self.inputField.userInteractionEnabled = NO;

    listIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-open-accordion.png"]];
    listIcon.userInteractionEnabled = NO;
    [fieldBackgroundView addSubview:listIcon];

    [self enableFieldBackgroundViewTrigger];
}

#pragma mark -
#pragma mark Methods

-(NSString *)optionValue {
    if(self.selectedIndex == -1) return nil;
    return [options[(NSUInteger) self.selectedIndex] objectForKey:@"id"];
}

-(void)onValueChange:(AAFieldComboboxOnValueChangeBlock)block {
    onValueChangeBlock = block;
}

- (void)addOptionWithID:(NSString *)optionID title:(NSString *)optionTitle icon:(id)icon {
    if(!options) options = [[NSMutableArray alloc] init];
    if(!icon) icon = @"";
    [options addObject:@{@"id":optionID, @"title":optionTitle, @"icon":icon}];
}

- (void)addOptionWithId:(NSString *)optionID type:(NSString *)type title:(NSString *)optionTitle icon:(id)icon {
    if(!options) options = [[NSMutableArray alloc] init];
    if(!icon) icon = @"";
    [options addObject:@{@"id":optionID, @"type" : type, @"title":optionTitle, @"icon":icon}];
}

-(void)selectItemAtIndex:(NSInteger)index
{
    if(index >= 0 && index < options.count) {
        self.selectedIndex = index;
        [self setValue:[options[index] objectForKey:@"title"]];

        id icon = [options[index] objectForKey:@"icon"] ;
        if(icon) {
            if([icon isKindOfClass:[NSString class]]) {
                if([(NSString *)icon length] > 0) {
                    [self setIconWithURL:[NSURL URLWithString:icon]];
                }
            } else {
                [self setIcon:icon];
            }
        }
    }
}

- (void)selectByOptionId:(NSString *)value
{
    for (int i = 0; i < options.count; i++)
    {
        NSString *optionId = [options[i] objectForKey:@"id"];
        if([optionId isEqual:value]) {
            [self selectItemAtIndex:i];
            break;
        }
    }
}

-(void)removeAllOptions
{
    self.selectedIndex = -1;
    [options removeAllObjects];
    options = nil;
    [self setValue:@""];
    [self setIcon:nil];
}

- (void)unselect {
    self.selectedIndex = -1;
    [self setValue:@""];
    [self setIcon:nil];
}

- (void)enabled:(BOOL)enabled {
    self.userInteractionEnabled = enabled;

    if(!enabled) {
        titleLabel.alpha = 0.5;
    } else {
        titleLabel.alpha = 1;
    }
    listIcon.alpha = titleLabel.alpha;
}


- (BOOL)isset {
    return [self value].length > 0;
}

-(NSInteger)countOptions {
    return options?options.count:0;
}

#pragma mark -
#pragma mark Overrides

-(void)focus {
    [self actionFieldBackgroundTapped];
}

-(void)actionFieldBackgroundTapped
{
    [super actionFieldBackgroundTapped];
    [self.superview endEditing:YES];
    
    AAFieldActionSheet *actionSheet = [[AAFieldActionSheet alloc] initWithTitle:self.titleLabel.text];
    actionSheet.visibleItems = self.visibleItems;
    actionSheet.options = options;
    actionSheet.selectedIndex = self.selectedIndex;
    [actionSheet show];
    
    __weak AAFieldComboboxOnValueChangeBlock weakOnValueChangeBlock = onValueChangeBlock;
    __weak AAFieldCombobox *wSelf = self;
    [actionSheet onValueChange:^(NSDictionary *option, NSInteger index) {
        if(wSelf.selectedIndex != index)
        {
            wSelf.selectedIndex = index;
            [wSelf setValue:option[@"title"]];

            id icon = option[@"icon"];
            if(([icon isKindOfClass:[NSString class]] && [icon length] > 0) || [icon isKindOfClass:[UIImage class]]) [wSelf setIcon:option[@"icon"]];
            if(weakOnValueChangeBlock) weakOnValueChangeBlock(option);
        }
    }];
}

-(void)updateUI {
    [super updateUI];
    
    CGRect f = listIcon.frame;
    f.origin.x = (CGFloat) (fieldWidth - disclosurePaddingRight - f.size.width * 0.5);
    f.origin.y = (CGFloat) ((fieldBackgroundViewHeight - f.size.height) * 0.5);
    listIcon.frame = f;
}

#pragma mark -

- (void)dealloc
{
    listIcon = nil;
    options = nil;
    onValueChangeBlock = nil;
}

@end
