//
//  AAFieldActionSheet.h
//  AAFieldComponents
//
//  Created by Almas Adilbek on 7/23/13.
//  Copyright (c) 2013 GoodApp inc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^AAFieldActionSheetValueChangedBlock)(NSDictionary *option, NSInteger index);

@interface AAFieldActionSheet : UIView {
    NSMutableArray *options;
    NSInteger selectedIndex;
    NSInteger visibleItems;
}

@property (nonatomic, strong) NSMutableArray *options;
@property (nonatomic, assign) NSInteger visibleItems;
@property (nonatomic, assign) NSInteger selectedIndex;

- (id)initWithTitle:(NSString *)title;
-(void)show;
-(void)hide;

-(void)onValueChange:(AAFieldActionSheetValueChangedBlock)block;

+ (NSDictionary *)optionWithId:(id)optionID title:(NSString *)title;
+ (NSDictionary *)optionWithId:(id)optionID title:(NSString *)title icon:(NSString *)icon;

@end
