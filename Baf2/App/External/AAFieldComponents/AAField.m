//
//  AAField.m
//  AAFieldComponents
//
//  Created by Almas Adilbek on 7/22/13.
//  Copyright (c) 2013 GoodApp inc. All rights reserved.
//

#import <M13BadgeView/M13BadgeView.h>
#import "AAField.h"
#import "AAFieldIconView.h"

@interface AAField()
-(void)createIconView;
@end

@implementation AAField {
    AAFieldOnTap onTapBlock;
    AAFieldOnValueChanged onValueChangedBlock;
    NSMutableArray *onFocusBlocks;

    UIActivityIndicatorView *loader;

    NSString *oldValue;
    M13BadgeView *rightBadgeView;
}

@synthesize inputField, keyboardType = _keyboardType, isLoading, secureTextEntry = _secureTextEntry, inputFieldEditable = _inputFieldEditable;;

- (id)initWithWidth:(CGFloat)width
{
    self = [super initWithWidth:width];
    if (self) {
        [self initConfig];
    }
    return self;
}

- (void)initConfig
{
    inputFieldPaddingLeft = 15;
    inputFieldPaddingRight = 30;
    iconViewPaddingLeft = 10;
    iconViewSize = 24;
    iconViewInputFieldSpacing = 8;
    disclosurePaddingRight = 20;
    self.maxCharacters = NSIntegerMax;

    _inputFieldEditable = YES;

    self.inputField = [[UITextField alloc] initWithFrame:CGRectZero];
    inputField.delegate = self;
    inputField.backgroundColor = [UIColor clearColor];
    inputField.font = [UIFont systemFontOfSize:fieldTextSize];
    inputField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    inputField.autocorrectionType = UITextAutocorrectionTypeNo;
    inputField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [inputField addTarget:self action:@selector(inputFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    [fieldBackgroundView addSubview:inputField];
}

#pragma mark -
#pragma mark GET

-(NSString *)value {
    return [inputField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(BOOL)filled {
    return [[self value] length] > 0;
}

-(BOOL)requiredFilled {
    if(!self.required) return YES;
    return [[self value] length] > 0;
}

#pragma mark -
#pragma mark SET

-(void)onTap:(AAFieldOnTap)block {
    onTapBlock = block;
}

-(void)onValueChanged:(AAFieldOnValueChanged)block {
    onValueChangedBlock = block;
}

-(void)onFocus:(AAFieldOnFocus)block {
    if(!onFocusBlocks) {
        onFocusBlocks = [[NSMutableArray alloc] init];
    }
    [onFocusBlocks addObject:block];
}

-(void)setIcon:(UIImage *)iconImage
{
    if(iconImage == nil) {
        if(iconView) {
            [iconView removeFromSuperview];
            iconView = nil;
        }
        [self updateInputFieldUI];
        return;
    }
    
    [self createIconView];
    [iconView setIcon:iconImage];
    
    [self updateInputFieldUI];
}

-(void)setIconWithURL:(NSURL *)url
{
    [self createIconView];
    [self updateInputFieldUI];

    [iconView setIcon:url];
}

-(void)setValue:(NSString *)inputFieldValue
{
    oldValue = inputField.text;
    inputField.text = inputFieldValue;

    if(onValueChangedBlock && ![oldValue isEqual:inputFieldValue]) {
        onValueChangedBlock(inputFieldValue);
    }
}

-(void)setPlaceholder:(NSString *)placeholderText {
    inputField.placeholder = placeholderText;
}

- (void)setPrefix:(NSString *)prefix
{
    UILabel *prefixLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    prefixLabel.backgroundColor = [UIColor clearColor];
    prefixLabel.font = inputField.font;
    prefixLabel.textColor = inputField.textColor;
    prefixLabel.text = prefix;
    [prefixLabel sizeToFit];
    prefixLabel.x = fieldSidePadding;
    prefixLabel.y = 22;
    [self addSubview:prefixLabel];

    if(iconView) {
        prefixLabel.x = iconView.right + 12;
    }

    CGFloat x1 = inputField.x;
    inputField.x = prefixLabel.right;
    inputField.width -= (inputField.x - x1);
}

-(void)setKeyboardType:(UIKeyboardType)aKeyboardType {
    _keyboardType = aKeyboardType;
    inputField.keyboardType = _keyboardType;
}

-(void)clear {
    inputField.text = @"";
}

-(void)focus {
    if(self.inputFieldEditable && !self.isLoading) [inputField becomeFirstResponder];
}

-(void)setSecureTextEntry:(BOOL)aSecureTextEntry {
    _secureTextEntry = aSecureTextEntry;
    inputField.secureTextEntry = _secureTextEntry;
}

-(void)setInputFieldEditable:(BOOL)anInputFieldEditable {
    _inputFieldEditable = anInputFieldEditable;
    inputField.userInteractionEnabled = _inputFieldEditable;
}

// Loader

-(void)startLoading
{
    if(!self.isLoading) {
        self.isLoading = YES;
        self.userInteractionEnabled = NO;
        if(inputField) inputField.hidden = YES;
//    if(iconView) iconView.hidden = YES;

        loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGRect f = loader.frame;
        f.origin.x = (CGFloat) ((fieldWidth - f.size.width) * 0.5);
        f.origin.y = (CGFloat) ((fieldBackgroundViewHeight - f.size.height) * 0.5);
        loader.frame = f;
        [fieldBackgroundView addSubview:loader];
        [loader startAnimating];
    }
}

-(void)stopLoading
{
    if(self.isLoading) {
        self.isLoading = NO;
        self.userInteractionEnabled = YES;

        if(loader) {
            [loader removeFromSuperview];
            loader = nil;
        }

        if(inputField) inputField.hidden = NO;
        if(iconView) iconView.hidden = NO;
    }
}

// Other

-(void)updateInputFieldUI
{
    CGRect f1 = inputField.frame;
    if(iconView)
    {
        CGRect f = iconView.frame;
        f.origin.x = iconViewPaddingLeft;
        f.origin.y = (CGFloat) ((fieldBackgroundViewHeight - f.size.height) * 0.5);
        iconView.frame = f;
        
        f1.origin.x = f.origin.x + iconViewSize + iconViewInputFieldSpacing;
        f1.size.width = fieldWidth - f1.origin.x - inputFieldPaddingRight;
    }
    else
    {
        f1.origin.x = inputFieldPaddingLeft;
    }
    
    inputField.frame = f1;
}

-(void)enableFieldBackgroundViewTrigger
{
//    NSLog(@"enableFieldBackgroundViewTrigger %@ %@", NSStringFromClass([self class]), triggerButton?@"true":@"false");
    if(!triggerButton) {
        triggerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        triggerButton.frame = fieldBackgroundView.frame;
        [triggerButton addTarget:self action:@selector(actionSelectFieldBackground) forControlEvents:UIControlEventTouchDown];
        [triggerButton addTarget:self action:@selector(actionFieldBackgroundTapped) forControlEvents:UIControlEventTouchUpInside];
        [triggerButton addTarget:self action:@selector(actionUnselectFieldBackground) forControlEvents:UIControlEventTouchUpOutside];
        [triggerButton addTarget:self action:@selector(actionUnselectFieldBackground) forControlEvents:UIControlEventTouchCancel];
        [self addSubview:triggerButton];
    }
}

#pragma mark -
#pragma mark Badge

- (void)setRightBadgeWithText:(NSString *)badgeText backgroundColor:(UIColor *)color
{
    if(!rightBadgeView) {
        rightBadgeView = [[M13BadgeView alloc] initWithFrame:CGRectZero];
        rightBadgeView.animateChanges = NO;
        [rightBadgeView setFont:[UIFont boldSystemFontOfSize:11]];
        rightBadgeView.badgeBackgroundColor = color;
        rightBadgeView.textColor = [UIColor blackColor];
        rightBadgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentLeft;
        rightBadgeView.verticalAlignment = M13BadgeViewVerticalAlignmentNone;
        rightBadgeView.cornerRadius = 4;
        [self addSubview:rightBadgeView];
    }
    [rightBadgeView setText:badgeText];
    rightBadgeView.x = self.bounds.size.width - rightBadgeView.bounds.size.width - fieldSidePadding;
    rightBadgeView.centerY = inputField.middleY + 10;
}

- (void)removeRightBadge
{
    if(rightBadgeView) {
        [rightBadgeView removeFromSuperview];
        rightBadgeView = nil;
    }
}

#pragma mark -
#pragma mark Overrides

-(void)actionFieldBackgroundTapped {
    [super actionFieldBackgroundTapped];
    
    if(onTapBlock) onTapBlock();
}

-(void)updateUI
{
    [super updateUI];
    
    CGRect f;
    
    if(triggerButton) triggerButton.frame = fieldBackgroundView.frame;
    
    f = inputField.frame;
    f.origin.x = inputFieldPaddingLeft;
    f.size.width = fieldWidth - inputFieldPaddingLeft - inputFieldPaddingRight;
    f.size.height = fieldBackgroundViewHeight;
    inputField.frame = f;
    
    [self updateInputFieldUI];
}

#pragma mark -
#pragma mark UITextField

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(onFocusBlocks) {
        for(AAFieldOnFocus block in onFocusBlocks) {
            block(self);
        }
    }

    [self actionSelectFieldBackground];
    oldValue = textField.text;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    [self actionUnselectFieldBackground];

    if(onValueChangedBlock && ![oldValue isEqual:textField.text]) {
        onValueChangedBlock(textField.text);
    }
}

- (void)inputFieldDidChange:(UITextField *)textField
{
    NSString *text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    // Check if entered text field length is more that max allowed length.
    if(text.length > self.maxCharacters) {
        text = [text substringToIndex:self.maxCharacters];
        textField.text = text;
        return;
    }

    if(self.isFieldTypeAmount)
    {
        // Change the "," (appears in other locale keyboards, such as russian) key ot "."
        text = [text stringByReplacingOccurrencesOfString:@"," withString:@"."];

        if([text isEqualToString:@"."]) {
            textField.text = @"0.";
            return;
        }

        if([text rangeOfString:@".."].location != NSNotFound) {
            textField.text = [text stringByReplacingOccurrencesOfString:@".." withString:@"."];
            return;
        }

        NSArray *dots = [text componentsSeparatedByString:@"."];
        if(dots.count > 1) {
            textField.text = [NSString stringWithFormat:@"%@.%@", dots[0], dots[1]];
        }

        if(text.length == 2) {
            if([[text substringToIndex:1] isEqualToString:@"0"] && ![text isEqualToString:@"0."]) {
                textField.text = [text substringWithRange:NSMakeRange(1, 1)];
                return;
            }
        }
    }
}

#pragma mark -

-(void)createIconView {
    if(!iconView) {
        iconView = [[AAFieldIconView alloc] initWithWidth:iconViewSize height:iconViewSize];
        [fieldBackgroundView addSubview:iconView];
    }
    if(self.isLoading) iconView.hidden = YES;
}

#pragma mark -

- (void)dealloc {
    self.inputField = nil;
    iconView = nil;
    triggerButton = nil;
    onTapBlock = nil;
    onFocusBlocks = nil;
}

@end
