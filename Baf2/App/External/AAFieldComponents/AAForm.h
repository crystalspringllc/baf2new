//
//  AAForm.h
//  Myth
//
//  Created by Almas Adilbek on 08/08/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#define kFormSidePadding 12

@class AAFieldBase;

@interface AAForm : NSObject {

}

@property(nonatomic, assign) CGFloat contentScrollViewBottomPadding;

- (id)initWithScrollView:(UIScrollView *)scrollView;

- (void)pushView:(UIView *)view;
- (void)pushView:(UIView *)view marginTop:(CGFloat)marginTop;
- (void)pushView:(UIView *)view marginTop:(CGFloat)marginTop centered:(BOOL)center;
- (void)pushView:(UIView *)view centered:(BOOL)center;

- (void)pushView:(UIView *)view beforeView:(UIView *)bottomView;
- (void)pushView:(UIView *)view beforeView:(UIView *)bottomView marginTop:(CGFloat)marginTop;
- (void)pushView:(UIView *)view beforeView:(UIView *)bottomView marginTop:(CGFloat)marginTop centered:(BOOL)centered;
- (void)pushView:(UIView *)view beforeView:(UIView *)bottomView marginTop:(CGFloat)marginTop marginBottom:(CGFloat)marginBottom centered:(BOOL)centered;

- (void)pushViewAtTop:(UIView *)view;
- (void)pushViewAtTop:(UIView *)view marginBottom:(CGFloat)marginBottom;
- (void)pushViewAtTop:(UIView *)view centered:(BOOL)centered;
- (void)pushViewAtTop:(UIView *)view marginBottom:(CGFloat)marginBottom centered:(BOOL)centered;
- (void)pushViewAtTop:(UIView *)view marginTop:(CGFloat)marginTop marginBottom:(CGFloat)marginBottom centered:(BOOL)centered;

- (void)pushView:(UIView *)view marginLeft:(CGFloat)marginLeft marginTop:(CGFloat)marginTop;

- (void)updateViewBounds:(UIView *)view;

- (void)removeView:(UIView *)view;
- (void)clear;

- (BOOL)hasView:(UIView *)view;

- (BOOL)isRequiredFieldsFilled;
- (void)initKeyboardControls;

@end
