//
//  BAFTabScrollView.h
//  BAFTabPagerViewController
//
//  Created by Mustafin Askar on 21/04/16.
//  Copyright (c) 2015 Crystal Spring. All rights reserved.
//

#import <ChameleonFramework/ChameleonMacros.h>
#import "BAFTabScrollView.h"
#import "UIView+AAPureLayout.h"
#import "UIView+ConcisePureLayout.h"

#define MAP(a, b, c) MIN(MAX(a, b), c)

@interface BAFTabScrollView ()
@property (strong, nonatomic) NSLayoutConstraint *tabIndicatorDisplacement;
@property (nonatomic, strong) NSLayoutConstraint *tabIndicatorWidth;
@property (nonatomic, strong) NSMutableArray *tabImageViews;
@property (nonatomic, strong) NSMutableArray *tabViews;
@property (nonatomic, strong) NSArray *tabImageNames;
@property (nonatomic, strong) NSArray *tabLabelNames;
@end

@implementation BAFTabScrollView

#pragma mark - Initialize Methods

- (instancetype)initWithFrame:(CGRect)frame
                   nameLabels:(NSArray *)nameLabels
                   imageNames:(NSArray *)imageNames {

    self = [super initWithFrame:frame];

    if (self) {

        self.tabImageNames = imageNames;
        self.tabLabelNames = nameLabels;

        [self configTabImageViews];

        UIView *contentView = [UIView newAutoLayoutView];
        [contentView setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:contentView];
        [contentView autoPinEdgesToSuperviewEdges];

        self.tabViews = [NSMutableArray new];

        for ( int i = 0; i < self.tabImageViews.count; i++ ) {

            UIView *tabView = self.tabImageViews[(NSUInteger) i];
            tabView.translatesAutoresizingMaskIntoConstraints = false;
            tabView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.1];
            [tabView setTag:i];
            [tabView setUserInteractionEnabled:YES];
            [tabView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tabTapHandler:)]];
            [contentView addSubview:tabView];
            [self.tabViews addObject:tabView];

            [tabView autoPinEdgeToSuperviewEdge:ALEdgeTop];
            [tabView autoPinEdgeToSuperviewEdge:ALEdgeBottom];

            if (i == 0) {
                [tabView aa_superviewLeft:0];
            } else if (i == self.tabImageViews.count - 1) {
                [tabView aa_pinAfterView:self.tabImageViews[(NSUInteger) (i - 1)] offset:0];
                [tabView aa_superviewRight:0];
                [tabView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.tabImageViews[(NSUInteger) (i - 1)]];
            } else {
                [tabView aa_pinAfterView:self.tabImageViews[(NSUInteger) (i - 1)] offset:0];
                [tabView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.tabImageViews[(NSUInteger) (i - 1)]];
            }
        }


        //Tab indicator
        UIView *tabIndicator = [UIView new];
        [tabIndicator setTranslatesAutoresizingMaskIntoConstraints:NO];
        [contentView addSubview:tabIndicator];
        [tabIndicator setBackgroundColor:[UIColor flatOrangeColor]];

        [self setTabIndicatorDisplacement:[NSLayoutConstraint constraintWithItem:tabIndicator
                                                                       attribute:NSLayoutAttributeLeading
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:contentView
                                                                       attribute:NSLayoutAttributeLeading
                                                                      multiplier:1.0f
                                                                        constant:0 / 2]];

        [self setTabIndicatorWidth:[NSLayoutConstraint constraintWithItem:tabIndicator
                                                                attribute:NSLayoutAttributeWidth
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:nil
                                                                attribute:0
                                                               multiplier:1.0f
                                                                 constant:[self.tabViews[0] frame].size.width]];

        [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[S(2)]-0-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:@{@"S": tabIndicator}]];

        [contentView addConstraints:@[[self tabIndicatorDisplacement], [self tabIndicatorWidth]]];
    }

    return self;
}

#pragma mark - crating tab views


/**
 *  Initialize tabImageViews array of UIViews that contain images and titles
 */
- (void)configTabImageViews {

    for (int i = 0; i < self.tabLabelNames.count; i++) {
        NSString *tabImageName = self.tabImageNames[(NSUInteger) i];
        NSString *tabTitle = self.tabLabelNames[(NSUInteger) i];

        UIView *tabView = [self getTabViewWithImage:tabImageName titleName:tabTitle];
        if (!self.tabImageViews) {
            self.tabImageViews = [[NSMutableArray alloc] init];
        }
        [self.tabImageViews addObject:tabView];
    }
}

/**
 * Initialize UIView with image and title in it
 */
- (UIView *)getTabViewWithImage:(NSString *)imageName titleName:(NSString *)titleName {

    UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth] / self.tabLabelNames.count, 50)];
    view1.translatesAutoresizingMaskIntoConstraints = false;

    UIImageView *imageView1;

    if (imageName) {
        UIImage *image1 = [UIImage imageNamed:imageName];
        imageView1 = [[UIImageView alloc] initWithImage:image1];
        imageView1.translatesAutoresizingMaskIntoConstraints = false;
        imageView1.contentMode = UIViewContentModeScaleAspectFit;
        [view1 addSubview:imageView1];
        [imageView1 autoSetDimension:ALDimensionHeight toSize:20];
        [imageView1 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:8];
        [imageView1 autoPinEdgeToSuperviewEdge:ALEdgeLeft];
        [imageView1 autoPinEdgeToSuperviewEdge:ALEdgeRight];
    }

    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont boldSystemFontOfSize:9];
    titleLabel.textColor = [UIColor fromRGB:0x444444];
    titleLabel.text = [titleName uppercaseString];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [view1 addSubview:titleLabel];

    if (imageView1) {
        [titleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:imageView1 withOffset:5];
    } else {
        [titleLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:5];
    }

    [titleLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [titleLabel autoPinEdgeToSuperviewEdge:ALEdgeRight];
    [titleLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:5];

    return view1;
}

#pragma mark - animations

- (void)animateToTabAtIndex:(NSInteger)index {
    [self animateToTabAtIndex:index animated:YES];
}

- (void)animateToTabAtIndex:(NSInteger)index animated:(BOOL)animated {

    CGFloat animatedDuration = 0.2f;
    if (!animated) {
        animatedDuration = 0.0f;
    }

    CGFloat x = [[self tabViews][0] frame].origin.x;

    for (int i = 0; i < index; i++) {
        x += [[self tabViews][i] frame].size.width;
    }

    CGFloat w = [[self tabViews][index] frame].size.width;

    [UIView animateWithDuration:animatedDuration
                     animations:^{

                         [[self tabIndicatorDisplacement] setConstant:x];
                         [[self tabIndicatorWidth] setConstant:w];
                         [self layoutIfNeeded];

                     }];

    [self selectTabImageAtIndex:index];

}

#pragma mark - actions

- (void)selectTabImageAtIndex:(NSInteger)index {

    for ( int i = 0; i < self.tabImageViews.count; i++ ) {
        UIView *view = self.tabImageViews[(NSUInteger) i];

        if ([view.subviews[0] isKindOfClass:[UIImageView class]]) {
            UIImageView *tempImageView = view.subviews[0];
            tempImageView.image = [UIImage imageNamed:self.tabImageNames[(NSUInteger) i]];
        }

        if ([view.subviews[0] isKindOfClass:[UILabel class]]) {
            UILabel *tabLabel = view.subviews[0];
            tabLabel.textColor = [UIColor fromRGB:0x444444];
        }

        view.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.1];
    }

    UIView *tempView = self.tabImageViews[(NSUInteger) index];

    tempView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
}

- (void)tabTapHandler:(UITapGestureRecognizer *)gestureRecognizer {
    if ([[self tabScrollDelegate] respondsToSelector:@selector(tabScrollView:didSelectTabAtIndex:)]) {
        NSInteger index = [[gestureRecognizer view] tag];
        [[self tabScrollDelegate] tabScrollView:self didSelectTabAtIndex:index];
        [self animateToTabAtIndex:index];
    }
}

@end