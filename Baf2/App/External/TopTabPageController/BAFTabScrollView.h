//
//  BAFTabScrollView.h
//  BAFTabPagerViewController
//
//  Created by Mustafin Askar on 21/04/16.
//  Copyright (c) 2015 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BAFTabScrollDelegate;

@interface BAFTabScrollView : UIView

@property (weak, nonatomic) id<BAFTabScrollDelegate> tabScrollDelegate;

- (instancetype)initWithFrame:(CGRect)frame
                   nameLabels:(NSArray *)nameLabels
                   imageNames:(NSArray *)imageNames;
- (void)animateToTabAtIndex:(NSInteger)index;
- (void)animateToTabAtIndex:(NSInteger)index animated:(BOOL)animated;

@end

@protocol BAFTabScrollDelegate <NSObject>

- (void)tabScrollView:(BAFTabScrollView *)tabScrollView didSelectTabAtIndex:(NSInteger)index;

@end