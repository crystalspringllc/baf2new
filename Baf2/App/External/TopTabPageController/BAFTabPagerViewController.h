//
//  BAFTabPagerViewController.h
//  BAFTabPagerViewController
//
//  Created by Mustafin Askar.
//  Copyright (c) 2015 Mustafin Askar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BAFTabPagerDataSource;
@protocol BAFTabPagerDelegate;
#import "BAFTabScrollView.h"

@interface BAFTabPagerViewController : UIViewController

@property (strong, nonatomic) BAFTabScrollView *header;
@property (nonatomic, strong) NSArray *tabImageNames;
@property (nonatomic, strong) NSArray *tabLabelNames;

@property (weak, nonatomic) id<BAFTabPagerDataSource> dataSource;
@property (weak, nonatomic) id<BAFTabPagerDelegate> delegate;

@property (nonatomic, assign) NSInteger startSegmentIndex;

- (NSInteger)selectedIndex;
- (void)reloadData;

- (void)selectTabbarIndex:(NSInteger)index;
- (void)selectTabbarIndex:(NSInteger)index animation:(BOOL)animation;

@end

@protocol BAFTabPagerDataSource <NSObject>

@required
- (NSInteger)numberOfViewControllers;
- (UIViewController *)viewControllerForIndex:(NSInteger)index;

@end

@protocol BAFTabPagerDelegate <NSObject>

@optional
- (void)tabPager:(BAFTabPagerViewController *)tabPager willTransitionToTabAtIndex:(NSInteger)index;
- (void)tabPager:(BAFTabPagerViewController *)tabPager didTransitionToTabAtIndex:(NSInteger)index;

@end