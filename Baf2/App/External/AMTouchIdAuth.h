//
// Created by Mustafin Askar on 30/10/15.
// Copyright (c) 2015 Банк Астаны. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <LocalAuthentication/LocalAuthentication.h>


@interface AMTouchIdAuth : NSObject

- (void)askUseTouchIdCompletion:(void (^)(BOOL success))completion;
- (void)authorizeWithTouchIdCompletion:(void (^)(BOOL success))completion;
+ (BOOL)hasTouchID; //once new devices are released with Touch ID, the model array will have to be updated manually.
+ (BOOL)touchIdIsAvailable;

@end