//
// Created by Almas Adilbek on 10/1/14.
// Copyright (c) 2014 Almas Adilbek. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BlurScreenView : UIView

- (id)initWithImage:(UIImage *)image blurRadius:(NSInteger)blurRadius;

- (void)show;
- (void)show:(BOOL)animated;
- (void)hide;

+ (UIImage *)capturedScreen;

@end