//
// Created by Almas Adilbek on 10/1/14.
// Copyright (c) 2014 Almas Adilbek. All rights reserved.
//

#import "BlurScreenView.h"
#import "UIImage+StackBlur.h"


@implementation BlurScreenView {

}

-(id)init {
    UIImage *screenImage = [[self class] capturedScreen];
    return [self initWithImage:screenImage blurRadius:35];
}

-(id)initWithImage:(UIImage *)image blurRadius:(NSInteger)blurRadius {
    self = [super initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeight])];
    if(self) {
        UIImageView *bgView = [[UIImageView alloc] initWithFrame:self.bounds];
        bgView.image = [[image normalize] stackBlur:(NSUInteger) blurRadius];
        [self addSubview:bgView];

        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        visualEffectView.frame = self.bounds;
        [self addSubview:visualEffectView];

        self.alpha = 0;
    }
    return self;
}

- (void)show {
    [self show:true];
}

- (void)show:(BOOL)animated {
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
    
    self.userInteractionEnabled = NO;
    
    if (animated) {
        [UIView animateWithDuration:0.25 animations:^{
            self.alpha = 1;
        } completion:^(BOOL finished) {
            self.userInteractionEnabled = YES;
        }];
    } else {
        self.alpha = 1;
        self.userInteractionEnabled = YES;
    }
}

-(void)hide {
    self.userInteractionEnabled = NO;
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

+ (UIImage *) capturedScreen {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    CGRect rect = [keyWindow bounds];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [keyWindow.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

@end