//
//  PinAuthView.h
//  BAF
//
//  Created by Almas Adilbek on 11/19/14.
//
//



#import "BlurScreenView.h"
#import "NumpadKeyboardView.h"
#import "RatesBannerView.h"

@class Ticker;

typedef NS_ENUM(NSInteger , PinAuthViewPinType) {
    PinAuthViewPinTypeNew = 0,
    PinAuthViewPinTypeAuth = 1
};

@interface PinAuthView : BlurScreenView <NumpadKeyboardViewDelegate>

@property (nonatomic, copy) NSString *pinCode;
@property (nonatomic, assign) PinAuthViewPinType pinType;

@property (nonatomic, copy) void (^onLoginWithTouchIDSuccess)(NSString *pinCode);
@property (nonatomic, copy) void (^pinAuthViewWillDismissAfterCancel)(PinAuthView *pinAuthView);
@property (nonatomic, copy) void (^pinAuthViewPinEntered)(PinAuthView *pinAuthView);

- (id)initWithPinType:(PinAuthViewPinType)authViewPinType;
- (void)reset;
- (void)setCancelButtonTitle:(NSString *)title;
- (void)shakePinsView;
- (void)setRateType:(RateType)rateType buy:(NSString *)buy sell:(NSString *)sell;
- (void)hideRateBannerView;

@property (nonatomic, strong) Ticker *ticker;

@end
