//
//  PinAuthView.m
//  BAF
//
//  Created by Almas Adilbek on 11/19/14.
//
//

#import "PinAuthView.h"
#import "PinCharacterView.h"
#import "PinHelper.h"
#import "AMTouchIdAuth.h"
#import "RatesBannerView.h"
#import "TickerBannerView.h"
#import "Ticker.h"

#define kUserUseTouchID @"useTouchID"
#define kUseTouchIDAsked @"useTouchIDAsked"

@interface PinAuthView(){}

@property (nonatomic, strong) UIView *pinsView;
@property (nonatomic, strong) RatesBannerView *ratesBannerView;

@end

@implementation PinAuthView {
    NSMutableArray *pinViews;
    UIButton *loginOtherButton;
}

- (id)initWithPinType:(PinAuthViewPinType)authViewPinType {
    self = [super initWithImage:[UIImage imageNamed:@"login-blur-background.jpg"] blurRadius:45];
    if (self) {
        self.pinType = authViewPinType;

        [self configUI];
    }
    return self;
}

#pragma makr - config actions

/**
 * Authorization by touch id
 */
- (void)authByTouchId {
    BOOL useTouchID = [[NSUserDefaults standardUserDefaults] boolForKey:kUserUseTouchID];

    if (useTouchID) {
        AMTouchIdAuth *amTouchIdAuth = [[AMTouchIdAuth alloc] init];
        [amTouchIdAuth authorizeWithTouchIdCompletion:^(BOOL success) {
            if (success) {
                NSString *pinCode = [PinHelper pinCode];

                if (!pinCode) {
                    [UIAlertView showWithTitle:nil
                                       message:NSLocalizedString(@"for_user_touchid_in_future", nil)
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil
                                      tapBlock:nil];
                    return;
                }

                if (self.onLoginWithTouchIDSuccess) {
                    self.onLoginWithTouchIDSuccess(pinCode);
                    [self hide];
                }
            }
        }];
    }
}

- (void)authByTouchIdFromKeyboard {
    BOOL useTouchID = [[NSUserDefaults standardUserDefaults] boolForKey:kUserUseTouchID];
    AMTouchIdAuth *amTouchIdAuth = [[AMTouchIdAuth alloc] init];

    if (useTouchID) {
        [amTouchIdAuth authorizeWithTouchIdCompletion:^(BOOL success) {
            if (success) {
                NSString *pinCode = [PinHelper pinCode];
                
                if (!pinCode) {
                    [UIAlertView showWithTitle:nil
                                       message:NSLocalizedString(@"for_user_touchid_in_future", nil)
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil
                                      tapBlock:nil];
                    return;
                }
                
                if (self.onLoginWithTouchIDSuccess) {
                    self.onLoginWithTouchIDSuccess(pinCode);
                    [self hide];
                }
            }
        }];
    }else{
        if(self.pinType == PinAuthViewPinTypeNew){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Введите код"
                                                            message:@"Вы не ввели код для быстрого доступа"
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"good", nil)
                                                  otherButtonTitles:nil];
            [alert show];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"У вас не установлен Touch ID"
                                                            message:@"Пожалуйста, установите Touch ID в Настройках"
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"done", nil)
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void)loginWithUserAndPasswordTapped {
    if (_pinType == PinAuthViewPinTypeAuth) {
        [self hide];
        
        if (self.pinAuthViewWillDismissAfterCancel) {
            self.pinAuthViewWillDismissAfterCancel(self);
        }
    } else if(_pinType == PinAuthViewPinTypeNew) {
        // Just hide pin view
        [self hide];
        
        if (self.pinAuthViewWillDismissAfterCancel) {
            self.pinAuthViewWillDismissAfterCancel(self);
        }
    }
}

- (void)reset {
    self.pinCode = nil;
    for (PinCharacterView *characterView in pinViews) {
        if (characterView.isSelected) {
            [characterView setSelected:NO];
        }
    }
}


#pragma mark - show/hide/shake pins

- (void)shakePinsView {
    self.pinsView.transform = CGAffineTransformMakeTranslation(20, 0);
    [UIView animateWithDuration:0.4
                          delay:0.0
         usingSpringWithDamping:0.2
          initialSpringVelocity:1.0
                        options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.pinsView.transform = CGAffineTransformIdentity;
    } completion:nil];
}

- (void)show {
    [super show];
    [self authByTouchId];
}

- (void)show:(BOOL)animated {
    [super show:animated];
    [self authByTouchId];
}

- (void)menuTapped{
    
}

#pragma mark - keyboard

- (void)numpadKeyboardViewTappedNumber:(NSInteger)number {
    NSMutableString *pincodeString = [@"" mutableCopy];
    if (_pinCode) {
        pincodeString = [NSMutableString stringWithFormat:@"%@", _pinCode];
    }

    if (pincodeString.length < 4) {
        for(PinCharacterView *characterView in pinViews) {
            if(!characterView.isSelected) {
                [characterView setSelected:YES];
                break;
            }
        }

        [pincodeString appendString:[NSString stringWithFormat:@"%i", (int)number]];
        _pinCode = pincodeString;

        if (pincodeString.length == 4) {
            if (self.pinAuthViewPinEntered) {
                self.pinAuthViewPinEntered(self);
            }
        }
    }
}

- (void)numpadKeyboardViewDeleteTapped {
    if (_pinCode) {
        for (NSInteger i = pinViews.count - 1; i >= 0; --i) {
            PinCharacterView *characterView = pinViews[i];
            if (characterView.isSelected) {
                [characterView setSelected:NO];
                break;
            }
        }

        NSMutableString *pincodeString = [NSMutableString stringWithFormat:@"%@", _pinCode];
        [pincodeString deleteCharactersInRange:NSMakeRange(pincodeString.length - 1, 1)];
        if(pincodeString.length > 0) {
            _pinCode = pincodeString;
        } else {
            _pinCode = nil;
        }
    }
}

-(void)numpadKeyboardViewTouchIdTapped{
    [self authByTouchIdFromKeyboard];
}

#pragma mark - config ui

- (void)configUI {
    CGFloat nexty = 20;
    
    if ([ASize isIPhone5OrLater] || [ASize isIpad]) {
//        UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_bank_astana_wht.png"]];
//        logoView.width = 225;
//        logoView.height = 40;
//        logoView.contentMode = UIViewContentModeScaleAspectFit;
//        logoView.centerX = self.middleX;
//        logoView.y = [ASize iphone4:30 iphone5:40 iphone6:50 iphone6Plus:50 ipad:60];
//        [self addSubview:logoView];
//        nexty = logoView.bottom;
        self.ratesBannerView = [[RatesBannerView alloc] initWithFrame:CGRectMake(0, 0, RATE_VIEW_PREFERRED_SIZE.width, RATE_VIEW_PREFERRED_SIZE.height)];
        [self.ratesBannerView applyLightTheme];
        self.ratesBannerView.centerX = self.middleX;
        self.ratesBannerView.y = [ASize iphone4:30 iphone5:40 iphone6:50 iphone6Plus:50 ipad:60];
        [self addSubview:self.ratesBannerView];
        nexty = self.ratesBannerView.bottom;

        [self.ratesBannerView startFlipTimer];
    }
    
//    UIButton *menuButton = [[UIButton alloc]initWithFrame:CGRectMake([ASize screenWidth] - 60, [ASize iphone4:30 iphone5:40 iphone6:50 iphone6Plus:50 ipad:60], 40, 40)];
//    [menuButton setImage:[[UIImage imageNamed:@"ic_menu_white"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
//    [menuButton.imageView setTintColor:[UIColor whiteColor]];
//    [menuButton addTarget:self action:@selector(menuTapped) forControlEvents:UIControlEventTouchUpInside];
//    [self addSubview:menuButton];
    
    
    NSString *title = @"";
    if(_pinType == PinAuthViewPinTypeAuth) {
        title = NSLocalizedString(@"enter_pin_code", nil);
    } else if(_pinType == PinAuthViewPinTypeNew) {
        title = NSLocalizedString(@"install_code_for_fast_access", nil);
    }
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, nexty + [ASize iphone4:10 iphone5:30 iphone6:30 iphone6Plus:30 ipad:30], (CGFloat) ([ASize screenWidth] * 0.8), 1)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont helveticaNeue:14];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.numberOfLines = 0;
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.text = title;
    [titleLabel sizeToFit];
    titleLabel.centerX = self.middleX;
    [self addSubview:titleLabel];
    
    self.pinsView = [[UIView alloc] initWithFrame:CGRectZero];
    CGFloat pinViewsSpacing = 6;
    
    pinViews = [[NSMutableArray alloc] init];
    for(NSInteger i = 0; i < 4; ++i)
    {
        PinCharacterView *characterView = [[PinCharacterView alloc] init];
        characterView.x = i * (characterView.width + pinViewsSpacing);
        [self.pinsView addSubview:characterView];
        
        self.pinsView.width = characterView.right;
        self.pinsView.height = characterView.height;
        
        [pinViews addObject:characterView];
    }
    
    self.pinsView.centerX = self.middleX;
    self.pinsView.y = titleLabel.bottom + 10;
    [self addSubview:self.pinsView];
    
    // Alternative login link
    loginOtherButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.pinsView.bottom + [ASize iphone4:12 iphone5:22 iphone6:35 iphone6Plus:35 ipad:50], self.width - 40, 20)];
    [loginOtherButton addTarget:self action:@selector(loginWithUserAndPasswordTapped) forControlEvents:UIControlEventTouchUpInside];
    NSMutableAttributedString *buttonTitle = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"enter_with_login_and_password", nil)];
    [buttonTitle addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, buttonTitle.length)];
    [buttonTitle addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, buttonTitle.length)];
    [buttonTitle addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, buttonTitle.length)];
    [loginOtherButton setAttributedTitle:buttonTitle forState:UIControlStateNormal];
    loginOtherButton.centerX = self.middleX;
    [self addSubview:loginOtherButton];
    
    // Keyboard
    NumpadKeyboardView *keyboardView = [[NumpadKeyboardView alloc] init];
    keyboardView.delegate = self;
    keyboardView.y = loginOtherButton.bottom + [ASize iphone4:10 iphone5:10 iphone6:20 iphone6Plus:25 ipad:50];
    [self addSubview:keyboardView];
}

- (void)setCancelButtonTitle:(NSString *)title {
    NSMutableAttributedString *buttonTitle = [[NSMutableAttributedString alloc] initWithString:title];
    [buttonTitle addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, buttonTitle.length)];
    [buttonTitle addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, buttonTitle.length)];
    [buttonTitle addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, buttonTitle.length)];
    [loginOtherButton setAttributedTitle:buttonTitle forState:UIControlStateNormal];
}

- (void)setRateType:(RateType)rateType buy:(NSString *)buy sell:(NSString *)sell {
    if (self.ratesBannerView) {
        [self.ratesBannerView setRateType:rateType buy:buy sell:sell];
    }
}

- (void)setTicker:(Ticker *)ticker {
    _ticker = ticker;
    if (self.ticker) {
        self.ratesBannerView.tickerBannerView = [TickerBannerView configuredView];
        [self.ratesBannerView.tickerBannerView setDarkStyle];
        self.ratesBannerView.tickerBannerView.ticker = self.ticker;
    }
}

#pragma mark - hide rate banner

- (void)hideRateBannerView {
    [self.ratesBannerView removeFromSuperview];
    UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_bank_astana_wht.png"]];
    logoView.width = 225;
    logoView.height = 40;
    logoView.contentMode = UIViewContentModeScaleAspectFit;
    logoView.centerX = self.middleX;
    logoView.y = [ASize iphone4:30 iphone5:40 iphone6:50 iphone6Plus:50 ipad:60];
    [self addSubview:logoView];
}

@end
