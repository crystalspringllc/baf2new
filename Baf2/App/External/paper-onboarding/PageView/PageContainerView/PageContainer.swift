//
//  PageContaineView.swift
//  AnimatedPageView
//
//  Created by Alex K. on 13/04/16.
//  Copyright © 2016 Alex K. All rights reserved.
//

import UIKit

class PageContrainer: UIView {
    
    var items: [PageViewItem]?
    let space: CGFloat // space between items
    var currentIndex = 0
    
    fileprivate let itemRadius: CGFloat
    fileprivate let selectedItemRadius: CGFloat
    fileprivate let itemsCount: Int
    fileprivate let animationKey = "animationKey"
    
    init(radius: CGFloat, selectedRadius: CGFloat, space: CGFloat, itemsCount: Int) {
        self.itemsCount         = itemsCount
        self.space              = space
        self.itemRadius         = radius
        self.selectedItemRadius = selectedRadius
        super.init(frame: CGRect.zero)
        items = createItems(itemsCount, radius: radius, selectedRadius: selectedRadius)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: public

extension PageContrainer {
    
    func currenteIndex(_ index: Int, duration: Double, animated: Bool) {
        guard let items = self.items ,
            index != currentIndex else {return}
        
        animationItem(items[index], selected: true, duration: duration)
        
        let fillColor = index > currentIndex ? true : false
        animationItem(items[currentIndex], selected: false, duration: duration, fillColor: fillColor)
        
        currentIndex = index
    }
}

// MARK: animations

extension PageContrainer {
    
    fileprivate func animationItem(_ item: PageViewItem, selected: Bool, duration: Double, fillColor: Bool = false) {
        let toValue = selected == true ? selectedItemRadius * 2 : itemRadius * 2
        item.constraints
            .filter{ $0.identifier == "animationKey" }
            .forEach {
                $0.constant = toValue
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: {
            self.layoutIfNeeded()
            }, completion: nil)
        
        item.animationSelected(selected, duration: duration, fillColor: fillColor)
    }
}
// MARK: create

extension PageContrainer {
    
    fileprivate func createItems(_ count: Int, radius: CGFloat, selectedRadius: CGFloat) -> [PageViewItem] {
        var items = [PageViewItem]()
        // create first item
        var item = createItem(radius, selectedRadius: selectedRadius, isSelect: true)
        addConstraintsToView(item, radius: selectedRadius)
        items.append(item)
        
        for _ in 1..<count {
            let nextItem = createItem(radius, selectedRadius: selectedRadius)
            addConstraintsToView(nextItem, leftItem: item, radius: radius)
            items.append(nextItem)
            item = nextItem
        }
        return items
    }
    
    fileprivate func createItem(_ radius: CGFloat, selectedRadius: CGFloat, isSelect: Bool = false) -> PageViewItem {
        let item = Init(PageViewItem(radius: radius, selectedRadius: selectedRadius, isSelect: isSelect)) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor                           = UIColor.clear
        }
        self.addSubview(item)
        
        return item
    }
    
    fileprivate func addConstraintsToView(_ item: UIView, radius: CGFloat) {
        [NSLayoutAttribute.left, NSLayoutAttribute.centerY].forEach { attribute in
            let left = (self, item)
            var info = ConstraintInfo()
            info.attribute = attribute
            info.secondAttribute = info.secondAttribute == .notAnAttribute ? info.attribute : info.secondAttribute
            
            let constraint = NSLayoutConstraint(item: left.1,
                                                attribute: info.attribute,
                                                relatedBy: info.relation,
                                                toItem: left.0,
                                                attribute: info.secondAttribute,
                                                multiplier: 1,
                                                constant: info.constant)
            constraint.identifier = info.identifier
            left.0.addConstraint(constraint)
        }
        
        [NSLayoutAttribute.width, NSLayoutAttribute.height].forEach { attribute in
            let left = item
            var info = ConstraintInfo()
            info.attribute = attribute
            info.constant = radius * 2.0
            info.identifier = animationKey
            
            let constraint = NSLayoutConstraint(item: left,
                                                attribute: info.attribute,
                                                relatedBy: info.relation,
                                                toItem: nil,
                                                attribute: info.attribute,
                                                multiplier: 1,
                                                constant: info.constant)
            constraint.identifier = info.identifier
            left.addConstraint(constraint)
        }
    }
    
    fileprivate func addConstraintsToView(_ item: UIView, leftItem: UIView, radius: CGFloat) {
        let left = (self, item)
        var info = ConstraintInfo()
        info.attribute = .centerY
        
        info.secondAttribute = info.secondAttribute == .notAnAttribute ? info.attribute : info.secondAttribute
        
        let constraint = NSLayoutConstraint(item: left.1,
                                            attribute: info.attribute,
                                            relatedBy: info.relation,
                                            toItem: left.0,
                                            attribute: info.secondAttribute,
                                            multiplier: 1,
                                            constant: info.constant)
        constraint.identifier = info.identifier
        left.0.addConstraint(constraint)
        
        let left1 = (self, item, leftItem)
        var info1 = ConstraintInfo()
        info1.attribute = .leading
        info1.secondAttribute = .trailing
        info1.constant = space
        
        info1.secondAttribute = info1.secondAttribute == .notAnAttribute ? info1.attribute : info1.secondAttribute
        
        let constraint1 = NSLayoutConstraint(item: left1.1,
                                             attribute: info1.attribute,
                                             relatedBy: info1.relation,
                                             toItem: left1.2,
                                             attribute: info1.secondAttribute,
                                             multiplier: 1,
                                             constant: info1.constant)
        constraint1.identifier = info1.identifier
        left1.0.addConstraint(constraint1)
        
        [NSLayoutAttribute.width, NSLayoutAttribute.height].forEach { attribute in
            let left = item
            var info = ConstraintInfo()
            info.attribute = attribute
            info.constant = radius * 2.0
            info.identifier = animationKey
            
            let constraint = NSLayoutConstraint(item: left,
                                                attribute: info.attribute,
                                                relatedBy: info.relation,
                                                toItem: nil,
                                                attribute: info.attribute,
                                                multiplier: 1,
                                                constant: info.constant)
            constraint.identifier = info.identifier
            left.addConstraint(constraint)
        }
    }
}
