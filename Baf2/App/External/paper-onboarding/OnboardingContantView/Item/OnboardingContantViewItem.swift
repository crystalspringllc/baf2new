//
//  OnboardingContentViewItem.swift
//  AnimatedPageView
//
//  Created by Alex K. on 21/04/16.
//  Copyright © 2016 Alex K. All rights reserved.
//

import UIKit

open class OnboardingContentViewItem: UIView {
    
    var bottomConstraint: NSLayoutConstraint?
    var centerConstraint: NSLayoutConstraint?
    
    open var imageView: UIImageView?
    open var titleLabel: UILabel?
    open var descriptionLabel: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: public

extension OnboardingContentViewItem {
    
    class func itemOnView(_ view: UIView) -> OnboardingContentViewItem {
        let item = Init(OnboardingContentViewItem(frame:CGRect.zero)) {
            $0.backgroundColor                           = UIColor.clear
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        
        view.addSubview(item)
        
        // add constraints
        let left = item
        var info = ConstraintInfo()
        info.attribute = .height
        info.constant = 10000
        info.relation = .lessThanOrEqual
        
        let constraint = NSLayoutConstraint(item: left,
                                            attribute: info.attribute,
                                            relatedBy: info.relation,
                                            toItem: nil,
                                            attribute: info.attribute,
                                            multiplier: 1,
                                            constant: info.constant)
        constraint.identifier = info.identifier
        left.addConstraint(constraint)
        
        for attribute in [NSLayoutAttribute.leading, NSLayoutAttribute.trailing] {
            let left = (view, item)
            var info = ConstraintInfo()
            info.attribute = attribute
            info.secondAttribute = info.secondAttribute == .notAnAttribute ? info.attribute : info.secondAttribute
            
            let constraint = NSLayoutConstraint(item: left.1,
                                                attribute: info.attribute,
                                                relatedBy: info.relation,
                                                toItem: left.0,
                                                attribute: info.secondAttribute,
                                                multiplier: 1,
                                                constant: info.constant)
            constraint.identifier = info.identifier
            left.0.addConstraint(constraint)
        }
        
        for attribute in [NSLayoutAttribute.centerX, NSLayoutAttribute.centerY] {
            let left = (view, item)
            var info = ConstraintInfo()
            info.attribute = attribute
            info.secondAttribute = info.secondAttribute == .notAnAttribute ? info.attribute : info.secondAttribute
            
            let constraint = NSLayoutConstraint(item: left.1,
                                                attribute: info.attribute,
                                                relatedBy: info.relation,
                                                toItem: left.0,
                                                attribute: info.secondAttribute,
                                                multiplier: 1,
                                                constant: info.constant)
            constraint.identifier = info.identifier
            left.0.addConstraint(constraint)
        }
        
        return item
    }
}

// MARK: create

private extension OnboardingContentViewItem {
    
    func commonInit() {
        
        let titleLabel       = createTitleLabel(self)
        let descriptionLabel = createDescriptionLabel(self)
        let imageView        = createImage(self)
        
        // added constraints
        let left = (self, titleLabel, imageView)
        var info = ConstraintInfo()
        info.attribute = .top
        info.secondAttribute = .bottom
        info.constant = 50
        
        info.secondAttribute = info.secondAttribute == .notAnAttribute ? info.attribute : info.secondAttribute
        
        let constraint = NSLayoutConstraint(item: left.1,
                                            attribute: info.attribute,
                                            relatedBy: info.relation,
                                            toItem: left.2,
                                            attribute: info.secondAttribute,
                                            multiplier: 1,
                                            constant: info.constant)
        constraint.identifier = info.identifier
        left.0.addConstraint(constraint)
        
        let left1 = (self, descriptionLabel, titleLabel)
        var info1 = ConstraintInfo()
        info1.attribute = .top
        info1.secondAttribute = .bottom
        info1.constant = 10
        
        info1.secondAttribute = info1.secondAttribute == .notAnAttribute ? info1.attribute : info1.secondAttribute
        
        let constraint1 = NSLayoutConstraint(item: left1.1,
                                             attribute: info1.attribute,
                                             relatedBy: info1.relation,
                                             toItem: left1.2,
                                             attribute: info1.secondAttribute,
                                             multiplier: 1,
                                             constant: info1.constant)
        constraint1.identifier = info1.identifier
        left1.0.addConstraint(constraint1)
        
        self.titleLabel       = titleLabel
        self.descriptionLabel = descriptionLabel
        self.imageView        = imageView
    }
    
    func createTitleLabel(_ onView: UIView) -> UILabel {
        let label = Init(createLabel()) {
            $0.font = UIFont(name: "Nunito-Bold" , size: 36)
        }
        onView.addSubview(label)
        
        // add constraints
        let left = label
        var info = ConstraintInfo()
        info.attribute = .height
        info.constant = 10000
        info.relation = .lessThanOrEqual
        
        let constraint = NSLayoutConstraint(item: left,
                                            attribute: info.attribute,
                                            relatedBy: info.relation,
                                            toItem: nil,
                                            attribute: info.attribute,
                                            multiplier: 1,
                                            constant: info.constant)
        constraint.identifier = info.identifier
        left.addConstraint(constraint)
        
        for attribute in [NSLayoutAttribute.centerX, NSLayoutAttribute.leading, NSLayoutAttribute.trailing] {
            let left = (onView, label)
            var info = ConstraintInfo()
            info.attribute = attribute
            
            info.secondAttribute = info.secondAttribute == .notAnAttribute ? info.attribute : info.secondAttribute
            
            let constraint = NSLayoutConstraint(item: left.1,
                                                attribute: info.attribute,
                                                relatedBy: info.relation,
                                                toItem: left.0,
                                                attribute: info.secondAttribute,
                                                multiplier: 1,
                                                constant: info.constant)
            constraint.identifier = info.identifier
            left.0.addConstraint(constraint)
        }
        return label
    }
    
    func createDescriptionLabel(_ onView: UIView) -> UILabel {
        let label = Init(createLabel()) {
            $0.font          = UIFont(name: "OpenSans-Regular" , size: 14)
            $0.numberOfLines = 0
        }
        onView.addSubview(label)
        
        // add constraints
        let left = label
        var info = ConstraintInfo()
        info.attribute = .height
        info.constant = 10000
        info.relation = .lessThanOrEqual
        
        let constraint = NSLayoutConstraint(item: left,
                                            attribute: info.attribute,
                                            relatedBy: info.relation,
                                            toItem: nil,
                                            attribute: info.attribute,
                                            multiplier: 1,
                                            constant: info.constant)
        constraint.identifier = info.identifier
        left.addConstraint(constraint)
        
        for (attribute, constant) in [(NSLayoutAttribute.leading, 30), (NSLayoutAttribute.trailing, -30)] {
            let left = (onView, label)
            var info = ConstraintInfo()
            info.attribute = attribute
            info.constant = CGFloat(constant)
            
            info.secondAttribute = info.secondAttribute == .notAnAttribute ? info.attribute : info.secondAttribute
            
            let constraint = NSLayoutConstraint(item: left.1,
                                                attribute: info.attribute,
                                                relatedBy: info.relation,
                                                toItem: left.0,
                                                attribute: info.secondAttribute,
                                                multiplier: 1,
                                                constant: info.constant)
            constraint.identifier = info.identifier
            left.0.addConstraint(constraint)
        }
        let left1 = (onView, label)
        var info1 = ConstraintInfo()
        info1.attribute = .centerX
        
        info1.secondAttribute = info1.secondAttribute == .notAnAttribute ? info1.attribute : info1.secondAttribute
        
        let constraint1 = NSLayoutConstraint(item: left1.1,
                                             attribute: info1.attribute,
                                             relatedBy: info1.relation,
                                             toItem: left1.0,
                                             attribute: info1.secondAttribute,
                                             multiplier: 1,
                                             constant: info1.constant)
        constraint1.identifier = info1.identifier
        left1.0.addConstraint(constraint1)
        
        let left2 = (onView, label)
        var info2 = ConstraintInfo()
        info2.attribute = .bottom
        
        info2.secondAttribute = info2.secondAttribute == .notAnAttribute ? info2.attribute : info2.secondAttribute
        
        let constraint2 = NSLayoutConstraint(item: left2.1,
                                             attribute: info2.attribute,
                                             relatedBy: info2.relation,
                                             toItem: left2.0,
                                             attribute: info2.secondAttribute,
                                             multiplier: 1,
                                             constant: info2.constant)
        constraint2.identifier = info2.identifier
        left2.0.addConstraint(constraint2)
        bottomConstraint = constraint2
        
        return label
    }
    
    func createLabel() -> UILabel {
        return Init(UILabel(frame: CGRect.zero)) {
            $0.backgroundColor                           = UIColor.clear
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.textAlignment                             = .center
            $0.textColor                                 = UIColor.white
        }
    }
    
    func createImage(_ onView: UIView) -> UIImageView {
        let imageView = Init(UIImageView(frame: CGRect.zero)) {
            $0.contentMode                               = .scaleAspectFit
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        
        onView.addSubview(imageView)
        
        // add constratints
        for attribute in [NSLayoutAttribute.width, NSLayoutAttribute.height] {
            let left = imageView
            var info = ConstraintInfo()
            info.attribute = attribute
            info.constant = 188
            
            let constraint = NSLayoutConstraint(item: left,
                                                attribute: info.attribute,
                                                relatedBy: info.relation,
                                                toItem: nil,
                                                attribute: info.attribute,
                                                multiplier: 1,
                                                constant: info.constant)
            constraint.identifier = info.identifier
            left.addConstraint(constraint)
        }
        
        for attribute in [NSLayoutAttribute.centerX, NSLayoutAttribute.top] {
            let left = (onView, imageView)
            var info = ConstraintInfo()
            info.attribute = attribute
            
            info.secondAttribute = info.secondAttribute == .notAnAttribute ? info.attribute : info.secondAttribute
            
            let constraint = NSLayoutConstraint(item: left.1,
                                                attribute: info.attribute,
                                                relatedBy: info.relation,
                                                toItem: left.0,
                                                attribute: info.secondAttribute,
                                                multiplier: 1,
                                                constant: info.constant)
            constraint.identifier = info.identifier
            left.0.addConstraint(constraint)
        }
        
        return imageView
    }
}
