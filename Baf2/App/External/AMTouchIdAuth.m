//
// Created by Mustafin Askar on 30/10/15.
// Copyright (c) 2015 Банк Астаны. All rights reserved.
//

#import "AMTouchIdAuth.h"

#define kTouchIDNotEnrolledAlertShowed @"kTouchIDNotEnrolledAlertShowed"
#define kPasscodeNotSetAlertShowed @"kPasscodeNotSetAlertShowed"

@implementation AMTouchIdAuth {}

- (void)askUseTouchIdCompletion:(void (^)(BOOL success))completion {
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;

    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {

        [self showUseTouchIdAlertWithCompletiton:completion];

    } else {
        completion(false);
        
        switch (error.code) {

            case LAErrorTouchIDNotAvailable : {

                break;
            }
            case LAErrorPasscodeNotSet: {

                NSLog(@"Authentication could not start, because passcode is not set on the device.");
                [self showUserSettingsForPassodeAlert];

                break;
            }
            case LAErrorTouchIDNotEnrolled: {
                
                NSLog(@"No registration of the finger Touch ID. ");
                [self showUserSettingsForTouchIdAlert];
                break;
            }
            default:
                NSLog(@"Touch ID configuration");
                break;
        }
    }
}

- (void)authorizeWithTouchIdCompletion:(void (^)(BOOL success))completion {
    LAContext *context = [[LAContext alloc] init];
    context.localizedFallbackTitle = @"";
    [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
            localizedReason:@" "
                      reply:^(BOOL success, NSError *error2) {
                          
          dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
              dispatch_async(dispatch_get_main_queue(), ^(){
                  if (success) {
                      if (completion) {
                          completion(YES);
                      }
                      return;
                  }
                  switch (error2.code) {
                      case LAErrorAuthenticationFailed: {
                          
                          NSLog(@"Authentication was not successful, because user failed to provide valid credentials.");
                          [UIAlertView showWithTitle:nil
                                             message:NSLocalizedString(@"could_not_enter_with_touch_id", nil)
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil
                                            tapBlock:nil];
                          
                          break;
                      }
                      case LAErrorUserCancel: {
                          
                          NSLog(@"Authentication was canceled by user (e.g. tapped Cancel button).");
                          
                          
                          break;
                      }
                      case LAErrorUserFallback: {
                          
                          NSLog(@"Authentication was canceled, because the user tapped the fallback button (Enter Password).");
                          
                          
                          break;
                      }
                      case LAErrorSystemCancel: {
                          
                          NSLog(@"Authentication was canceled by system (e.g. another application went to foreground).");
                          
                          
                          break;
                      }
                      case LAErrorPasscodeNotSet: {
                          
                          NSLog(@"Authentication could not start, because passcode is not set on the device.");
                          [self showUserSettingsForPassodeAlert];
                          
                          break;
                      }
                      case LAErrorTouchIDNotAvailable: {
                          
                          NSLog(@"Authentication could not start, because Touch ID is not available on the device.");
                          
                          
                          break;
                      }
                      case LAErrorTouchIDNotEnrolled: {
                          
                          NSLog(@"Authentication could not start, because Touch ID has no enrolled fingers.");
                          
                          [self showUserSettingsForTouchIdAlert];
                          
                          break;
                      }
                      default:
                          NSLog(@"Touch ID configuration");
                          break;
                  }
                  completion(false);
              });
          });
      }];
}

- (void)showUserSettingsForTouchIdAlert {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kTouchIDNotEnrolledAlertShowed]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kTouchIDNotEnrolledAlertShowed];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^(){
                [UIAlertView showWithTitle:nil
                                   message:NSLocalizedString(@"for_using_touch_id_add", nil)
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil
                                  tapBlock:nil];
            });
        });
    }
    
}

- (void)showUserSettingsForPassodeAlert {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kPasscodeNotSetAlertShowed]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kPasscodeNotSetAlertShowed];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^(){
                [UIAlertView showWithTitle:nil
                                   message:NSLocalizedString(@"for_using_touch_id_add", nil)
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil
                                  tapBlock:nil];
            });
        });
    }

    /*
    ^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == alertView.cancelButtonIndex) {
                              if (&UIApplicationOpenSettingsURLString != NULL) {
                                  //[[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@kz.crystalspring.baf", UIApplicationOpenSettingsURLString]]];
                              }
                          }
                      }
     */
}

- (void)showUseTouchIdAlertWithCompletiton:(void (^)(BOOL success))completion {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^(){
            UIAlertView *alertView = [UIAlertView showWithTitle:nil
                                                        message:NSLocalizedString(@"wanna_use_touchid", nil)
                                              cancelButtonTitle:NSLocalizedString(@"dialog_no", nil)
                                              otherButtonTitles:@[NSLocalizedString(@"dialog_yes", nil)]
                                                       tapBlock:nil];
            alertView.didDismissBlock = ^(UIAlertView *av, NSInteger buttonIndex) {
                if (completion) {
                    completion(buttonIndex != av.cancelButtonIndex);
                } else {
                    return;
                }
            };
        });
    });
}

int sysctlbyname(const char *, void *, size_t *, void *, size_t);

+ (NSString *)getSysInfoByName:(char *)typeSpecifier
{
    size_t size;
    sysctlbyname(typeSpecifier, NULL, &size, NULL, 0);

    char *answer = malloc(size);
    sysctlbyname(typeSpecifier, answer, &size, NULL, 0);

    NSString *results = [NSString stringWithCString:answer encoding: NSUTF8StringEncoding];

    free(answer);
    return results;
}

+ (NSString *)modelIdentifier
{
    return [self getSysInfoByName:"hw.machine"];
}

+ (BOOL)hasTouchID
{
    NSArray *touchIDModels = @[ @"iPhone6,1", @"iPhone6,2", @"iPhone7,1", @"iPhone7,2", @"iPad5,3", @"iPad5,4", @"iPad4,7", @"iPad4,8", @"iPad4,9" ];

    NSString *model = [self modelIdentifier];

    return [touchIDModels containsObject:model];
}

+ (BOOL)touchIdIsAvailable {
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    
    if (![context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        if (error.code == LAErrorTouchIDNotAvailable) {
            return false;
        }
    }
    
    return true;
}

@end