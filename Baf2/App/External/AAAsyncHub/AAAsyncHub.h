//
//  AAAsyncHub.h
//  Myth
//
//  Created by Almas Adilbek on 12/11/12.
//
//

#import <UIKit/UIKit.h>

@interface AAAsyncHub : UIView {
    int backgroundCornerRadius;
    float backgroundOpacity;
    int padding;
}

@property (nonatomic, assign) int backgroundCornerRadius;
@property (nonatomic, assign) float backgroundOpacity;
@property (nonatomic, assign) int padding;

+(AAAsyncHub *)sharedInstance;

-(void)showAtBottomLeft;
+(void)hide;

@end
