//
//  AAAsyncHub.m
//  Myth
//
//  Created by Almas Adilbek on 12/11/12.
//
//

#import "AAAsyncHub.h"

#define kAAAsyncHubSize 40
#define kAnimateDuration 0.2

static AAAsyncHub *_sharedInstance = nil;

@interface AAAsyncHub()
-(void)showAnimated:(BOOL)animated;
@end

@implementation AAAsyncHub

@synthesize backgroundCornerRadius, backgroundOpacity, padding;

+(AAAsyncHub *)sharedInstance {
    if(!_sharedInstance) {
        _sharedInstance = [[AAAsyncHub alloc] initWithFrame:CGRectMake(0, 0, kAAAsyncHubSize, kAAAsyncHubSize)];
    }
    return _sharedInstance;
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        self.userInteractionEnabled = NO;
        self.alpha = 0;
        
        self.backgroundCornerRadius = floor(self.frame.size.width * 0.5);
        self.backgroundOpacity = 0.8;
        self.padding = 10;
        
        // Background
        UIView *bg = [[UIView alloc] initWithFrame:self.bounds];
        bg.backgroundColor = [UIColor blackColor];
        bg.alpha = backgroundOpacity;
        bg.layer.cornerRadius = backgroundCornerRadius;
        bg.layer.masksToBounds = YES;
        [self addSubview:bg];
        
        // Loader
        UIActivityIndicatorView *loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        loader.hidesWhenStopped = YES;
        CGRect f = loader.frame;
        f.origin.x = (kAAAsyncHubSize - f.size.width) * 0.5;
        f.origin.y = (kAAAsyncHubSize - f.size.height) * 0.5;
        loader.frame = f;
        [self addSubview:loader];
        [loader startAnimating];
    }
    return self;
}

#pragma mark -
#pragma mark Methods

-(void)showAtBottomLeft
{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    [window addSubview:self];
    
    CGSize size = [[UIScreen mainScreen] bounds].size;
    CGRect f = self.frame;
    f.origin.x = padding;
    f.origin.y = size.height- f.size.height - padding;
    self.frame = f;
    
    [self showAnimated:YES];
}

+(void)hide {
    if(_sharedInstance) {
        [UIView animateWithDuration:kAnimateDuration animations:^{
            _sharedInstance.alpha = 0;
        } completion:^(BOOL finished) {
            [_sharedInstance removeFromSuperview];
            _sharedInstance = nil;
        }];
    }
}

#pragma mark -
#pragma mark Helper functions

-(void)showAnimated:(BOOL)animated {
    if(animated) {
        [UIView animateWithDuration:kAnimateDuration animations:^{
            self.alpha = 1;
        }];
    } else {
        self.alpha = 1;
    }
}

@end
