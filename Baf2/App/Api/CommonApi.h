//
//  CommonApi.h
//  Baf2
//
//  Created by Shyngys Kassymov on 11.04.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "APIClientOLD.h"

@interface CommonApi : APIClientOLD

+ (void)getCaptcha:(Success)success failure:(Failure)failure;
+ (void)getCaptchaWithSession:(Success)success failure:(Failure)failure;
+ (NSURLSessionDataTask *)getBafMakeCallDirs:(Success)success failure:(Failure)failure;
+ (void)getDepositOrLoanRequest:(NSDictionary *)params success:(Success)success failure:(Failure)failure;

@end
