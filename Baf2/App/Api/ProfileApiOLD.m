//
//  User.m
//  Myth
//
//  Created by Almas Adilbek on 8/2/12.
//  Copyright (c) 2012 CrystalSpring. All rights reserved.
//

#import "ProfileApiOLD.h"

@implementation ProfileApiOLD

+ (void)updateClientProfile:(User *)user
                oldPassword:(NSString *)oldPassword
             secretQuestion:(NSString *)secretQuestion
           secretQuestionId:(NSNumber *)secretQuestionId
                avatarImage:(UIImage *)avatarImage
                    success:(Success)success
                    failure:(Failure)failure {

    NSString *imageBase64;

    if (avatarImage) {
        NSData *imagedata = [NSData dataWithData:UIImagePNGRepresentation(avatarImage)];
        imageBase64 = [imagedata base64EncodedString];
    }

    NSDictionary *parameters = @{
            @"session_key" : user.sessionID ? : [NSNull null],
            @"avatar" : imageBase64 ? : [NSNull null],
            @"last_name" : user.lastname ? : [NSNull null],
            @"first_name" : user.firstname ? : [NSNull null],
            @"middle_name" : user.middlename ? : [NSNull null],
            @"gender" : user.gender ? : [NSNull null],
            @"date_of_birthday" : user.birthday ? : [NSNull null],
            @"phone" : user.phoneNumber ? : [NSNull null],
            @"email" : user.email ? : [NSNull null],
            @"country_id" : [NSNull null],
            @"nickname" : user.nickname ? : [NSNull null],
            @"password" : oldPassword ? : @"",
            @"question_answer" : secretQuestion ? : [NSNull null],
            @"question_id" : secretQuestionId ? : [NSNull null]
    };

    [self POST:@"updateClientProfile" parameters:parameters success:success failure:failure];
}

+ (void)updateClientPasswordOldPassword:(NSString *)oldPassword newPassword:(NSString *)newPassword success:(Success)success failure:(Failure)failure {
    [self POST:@"updateClientPassword" parameters:@{@"password" : oldPassword, @"new_password" : newPassword} success:success failure:failure];
}

+ (void)getClientInfoWithSuccess:(Success)success failure:(Failure)failure {
    [self POST:@"getClientInfo" parameters:nil success:success failure:failure];
}

@end
