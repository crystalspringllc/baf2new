//
//  CardsApi.m
//  Myth
//
//  Created by Almas Adilbek on 12/19/13.
//
//

#import "CardsApi.h"

@implementation CardsApi

+ (void)sendSMSAgainWithSuccess:(Success)success failure:(Failure)failure {
    [self POST:@"sendConfirmationSmsPayment" parameters:nil success:^(id response) {
        success(response);
    } failure:failure];
}

@end