//
//  ContractsAPIOLD.m
//  Myth
//
//  Created by Almas Adilbek on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ContractsAPIOLD.h"

@implementation ContractsAPIOLD

@synthesize data;

-(id)initWithDatas:(NSDictionary *)datas {
    self = [super init];
    if (self) {
        self.data = datas;
    }
    return self;
}

#pragma mark -
#pragma mark Helper functions

+(NSArray *)responseToContracts:(id)response
{
    NSMutableArray *contracts = [NSMutableArray array];
    for (NSDictionary *o in response) {
        Contract *contract = [Contract instanceFromDictionary:o];
        [contracts addObject:contract];
    }
    return contracts;
}

@end