//
//  InvoiceApi.m
//  Baf2
//
//  Created by nmaksut on 25.07.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "InvoiceApi.h"

@implementation InvoiceApi

+ (NSURLSessionDataTask *)getInvoiceIdRecentByContract:(NSString *)contract
                                            providerId:(NSNumber *)providerId
                                               success:(success)success
                                               failure:(failure)failure {

    return [self POST:@"invoices/get_invoice"
           parameters:@{@"providerId" : providerId, @"contract" : contract}
              success:success
              failure:failure];
}

+ (NSURLSessionDataTask *)setInvoiceRejectedByInvoiceContract:(NSString *)invoiceContract
                                                   contractId:(NSNumber *)contractId
                                                   providerId:(NSNumber *)providerId
                                                   formedDate:(NSString *)formedDate
                                                  rejectState:(RejectState)rejectState
                                                      success:(success)success
                                                      failure:(failure)failure {

    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"invoiceContract"] = invoiceContract;
    params[@"providerId"]      = providerId;
    params[@"contractId"]      = contractId;
    params[@"formedDate"]      = formedDate;
    NSString *state;
    if (rejectState == RejectStateAlreadyPaid) {
        state = @"1";
    } else if (rejectState == RejectStatePaidFromOther) {
        state = @"2";
    } else if (rejectState == RejectStateWillPayNextMonth) {
        state = @"3";
    }

    params[@"rejectedState"] = state;

    return [self POST:@"invoices/reject_invoice" parameters:params success:success failure:failure];
    
}


+ (NSURLSessionDataTask *)getInvoiceById:(NSNumber *)invoiceId success:(success)success failure:(failure)failure {
    return [self POST:@"invoices/get_invoice_by_id"
           parameters:@{@"invoiceId" : invoiceId}
              success:success
              failure:failure];
}

+ (NSURLSessionDataTask *)cancelInvoiceRejectByInvoiceContract:(NSString *)invoiceContract
                                                    contractId:(NSNumber *)contractId
                                                    providerId:(NSNumber *)providerId
                                                    formedDate:(NSString *)formedDate
                                                       success:(success)success
                                                       failure:(failure)failure {
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"invoiceContract"] = invoiceContract;
    params[@"providerId"] = providerId;
    params[@"contractId"] = contractId;
    params[@"formedDate"] = formedDate;

    return [self POST:@"invoices/cancel_invoice_reject" parameters:params success:success failure:failure];
    
}

+ (NSURLSessionDataTask *)getInvoicesHistory:(NSString *)invoiceContract providerId:(NSNumber *)providerId success:(success)success failure:(failure)failure {
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"invoiceContract"] = invoiceContract;
    params[@"providerId"] = providerId;

    return [self POST:@"invoices/invoices_history" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)saveInvoiceWithInvoiceContract:(NSString *)invoiceContract
                                                 invoice:(NSString *)invoice
                                              providerId:(NSNumber *)providerId
                                              contractId:(NSNumber *)contractId
                                                 success:(success)success
                                                 failure:(failure)failure {

    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"invoiceContract"] = invoiceContract;
    params[@"jsonInvoice"]     = invoice;
    params[@"providerId"]      = providerId;
    params[@"contractId"]      = contractId;
    params[@"invoiceVersion"]  = @1;
    
    return [self POST:@"invoices/save_invoice" parameters:params success:success failure:failure];
    
}

+ (NSURLSessionDataTask *)getServiceLogIdByInvoiceId:(NSNumber *)invoiceId
                                             success:(success)success
                                             failure:(failure)failure {
    
    return [self POST:@"invoices/get_invoice_servicelog_id" parameters:@{@"invoiceId" : invoiceId} success:success failure:failure];
}

@end
