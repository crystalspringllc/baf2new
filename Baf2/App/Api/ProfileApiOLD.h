//
//  User.h
//  Myth
//
//  Created by Almas Adilbek on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIClientOLD.h"
#import "LastOperation.h"
#import "User.h"
#import "NSData+Base64.h"


#define kKeyUsername @"username"

@interface ProfileApiOLD : APIClientOLD


+ (void)getClientInfoWithSuccess:(Success)success failure:(Failure)failure;


/**
 *  @brief Редактирование профиля
 */
+ (void)updateClientProfile:(User *)user
                oldPassword:(NSString *)oldPassword
             secretQuestion:(NSString *)secretQuestion
           secretQuestionId:(NSNumber *)secretQuestionId
                avatarImage:(UIImage *)avatarImage
                    success:(Success)success
                    failure:(Failure)failure;

/**
 *  @brief Смена пароля
 */
+ (void)updateClientPasswordOldPassword:(NSString *)oldPassword
                            newPassword:(NSString *)newPassword
                                success:(Success)success
                                failure:(Failure)failure;



@end