//
//  NewsApi.h
//  Baf2
//
//  Created by nmaksut on 16.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ApiClient.h"
#import "News.h"

@interface NewsApi : ApiClient

/**
 * @brief getiing bank news
 * @param last date of last item
 * @param success - block to handle success result object
 * @param failure - block to handle failure result code and message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)getBankNewsWithLastItem:(NSString *)last
                                          success:(success)success
                                          failure:(failure)failure;

/**
 * @brief getiing farum
 * @param last date of last item
 * @param success - block to handle success result object
 * @param failure - block to handle failure result code and message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)getGeneralNewsWithLastItem:(NSString *)last
                                             success:(success)success
                                             failure:(failure)failure;

/**
 * @brief sens new post to server
 * @param subject - subject of post
 * @param message - message of post
 * @param share - "support" or "public" by default
 * @param imageUrls - image urls
 * @param success - block to handle success result object
 * @param failure - block to handle failure result code and message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)addPostWithSubject:(NSString *)subject
                                     message:(NSString *)message
                                       share:(NSString *)share
                                   imageUrls:(NSArray *)imageUrls
                                     success:(success)success
                                     failure:(failure)failure;

/**
 * @brief send image to store on server
 * @param image - UIImage to send
 * @param success - block with url to handle success result object
 * @param failure - block to handle failure code and message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)storeImage:(UIImage *)image
                             success:(success)success
                             failure:(failure)failure;

/**
 * @brief Add comment to a news or to a post
 * @param newsId - news id
 * @param message - text message
 * @param success - block to handle success result object
 * @param failure - block to handle failure code and message
 * @return - NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)addCommentNewsId:(NSString *)newsId
                                   message:(NSString *)message
                                   success:(success)success
                                   failure:(failure)failure;

/**
 * @brief Getting news by id
 * @param newsId - news id to get
 * @param success - block to handle success result object
 * @param failure - block to handle failure code and message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)getNewsById:(NSString *)newsId
                              success:(success)success
                              failure:(failure)failure;

/**
 * @brief get private news (smth like chat)
 * @param last - date of last private news
 * @param success - block to handle success result object
 * @param failure - block to handle failure code and message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)getPrivateNewsWithLast:(NSString *)last
                                         success:(success)success
                                         failure:(failure)failure;

/**
 * @brief set like to a news, general news or private news
 * @param objectId - news id
 * @param type - type of news/ commet
 * @param success - block to handle success result object
 * @param failure - block to handle failure code and message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)likeObjectID:(NSString *)objectId
                                  type:(NSString *)type
                               success:(success)success
                               failure:(failure)failure;

/**
 * @brief set dislike to a news, general news or private news
 * @param objectId - news id
 * @param type - type of news/ commet
 * @param success - block to handle success result object
 * @param failure - block to handle failure code and message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)dislikeObjectID:(NSString *)objectId
                                     type:(NSString *)type
                                  success:(success)success
                                  failure:(failure)failure;

/**
 * @brief remove news, general news or private news
 * @param objectId - news id
 * @param type - type of news/ commet
 * @param success - block to handle success result object
 * @param failure - block to handle failure code and message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)deleteObjectID:(NSString *)objectId
                                    type:(NSString *)type
                                 success:(success)success
                                 failure:(failure)failure;

@end
