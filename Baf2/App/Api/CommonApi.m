//
//  CommonApi.m
//  Baf2
//
//  Created by Shyngys Kassymov on 11.04.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "CommonApi.h"
#import "Captcha.h"
#import "BafMakeCallDirs.h"

@implementation CommonApi

+ (void)getCaptcha:(Success)success failure:(Failure)failure {
    [self POST:@"getCaptcha" parameters:nil success:^(id response) {
        success([Captcha instanceFromDictionary:response]);
    } failure:failure];
}

+ (void)getCaptchaWithSession:(Success)success failure:(Failure)failure {
    [self POST:@"getCaptchaWithSession" parameters:nil success:^(id response) {
        success([Captcha instanceFromDictionary:response]);
    } failure:failure];
}

+ (NSURLSessionDataTask *)getBafMakeCallDirs:(Success)success failure:(Failure)failure {
    return [self POST:@"getBafMakeCallDirs" parameters:nil success:^(id response) {
        success([BafMakeCallDirs instanceWithResponse:response]);
    } failure:failure];
}

+ (void)getDepositOrLoanRequest:(NSDictionary *)params success:(Success)success failure:(Failure)failure {
    [self POST:@"getDepositOrLoanRequest" parameters:params success:success failure:failure];
}

@end