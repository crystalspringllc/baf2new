//
//  APIClientOLD.m
//  Baf2
//
//  Created by Askar Mustafin on 4/7/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "APIClientOLD.h"

@implementation APIClientOLD

+ (AFHTTPSessionManager *)sharedManager {
    static AFHTTPSessionManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [AFHTTPSessionManager manager];
        sharedManager.responseSerializer.acceptableContentTypes = [sharedManager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        sharedManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    });

    return sharedManager;
}

#pragma mark - methods

/**
 *
 *  Making POST request
 *
 */

+ (NSURLSessionDataTask *)POST:(NSString *)command parameters:(NSDictionary *)parameters success:(Success)success failure:(Failure)failure {

    NSString *API_BASE_URL = [self BASE_URL];

    if(!parameters) parameters = [NSMutableDictionary dictionary];
    NSMutableDictionary *fullParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    fullParameters[@"command"] = command;
    fullParameters[@"locale"] = [Localization currentLocale];
    fullParameters[@"isAndroid"] = @"0";
    fullParameters[@"os"] = @"ios";
    fullParameters[@"appVersion"] = [AppUtils appVersion];
    fullParameters[@"deviceInfo"] = [AppUtils deviceInfoJsonString];
    fullParameters[@"deviceId"] = [AppUtils deviceID];
    fullParameters[@"brandid"] = kBrandId;
    if(!fullParameters[@"session_key"] && [User sessionID])  {
        fullParameters[@"session_key"] = [User sessionID];
    }

    NSLog(@"METHOD: %@ %@", API_BASE_URL, command);
    NSLog(@"PARAMETERS: %@", [fullParameters JSONRepresentation]);

    //
    NSURLSessionDataTask *requestOperation = [[self sharedManager] POST:API_BASE_URL parameters:fullParameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        NSLog(@"RESPONSE: %@", [responseObject JSONRepresentation]);

        [self handleResponse:responseObject success:success failure:failure];


    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSString *errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"DETAILED ERROR: %@", errorResponse);
        
        failure(error.code, errorResponse);
    }];

    return requestOperation;
}

#pragma mark - handling response

+ (void)handleResponse:(id)responseObject success:(Success)success failure:(Failure)failure {

    if (responseObject) {

        NSString *result = [responseObject objectForKey:@"result"];

        if ([result isEqualToString:@"success"]) {

            if (success) {

                NSString *messageTitle = [responseObject objectForKey:@"messageTitle"];
                NSString *message = [responseObject objectForKey:@"message"];
                if (message.length > 0 || messageTitle.length > 0) {
#if !(WATCH_OS)
                    [UIHelper showAlertWithMessageTitle:messageTitle message:message];
#endif
                }


                if ([responseObject objectForKey:@"data"]) {
                    success([responseObject objectForKey:@"data"]);
                } else {
                    success(responseObject);
                }

            }

        } else if ([result isEqualToString:@"error"]) {
            NSInteger errcode = [[responseObject objectForKey:@"errcode"] integerValue];
            switch (errcode) {
                case 905:
                {
#if !(WATCH_OS)
                    [((AppDelegate *)[UIApplication sharedApplication].delegate) logoutUser];
#endif
                }
                    break;
                    
                default:
                    break;
            }
            
            NSString *messageTitle = [responseObject objectForKey:@"messageTitle"];
            NSString *message = [responseObject objectForKey:@"message"];
            if (message.length > 0 || messageTitle.length > 0) {
#if !(WATCH_OS)
                [UIHelper showAlertWithMessageTitle:messageTitle message:message];
#endif
            }

            if (failure) {
                failure([[responseObject objectForKey:@"errcode"] integerValue], [responseObject objectForKey:@"message"]);
            }

        }
    }
}

#pragma mark - security policy

+ (void)setSecurityPolicyOnManager:(AFHTTPSessionManager *)_sharedSecureManager {
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    NSString *certPath_bank = [[NSBundle mainBundle] pathForResource:@"*.myth.kz" ofType:@"cer"];
    NSString *certPath_root = [[NSBundle mainBundle] pathForResource:@"COMODO High-Assurance Secure Server CA" ofType:@"cer"];
    NSString *certPath_validation = [[NSBundle mainBundle] pathForResource:@"AddTrust External CA Root" ofType:@"cer"];
    securityPolicy.pinnedCertificates = [[NSSet alloc] initWithArray:@[[NSData dataWithContentsOfFile:certPath_bank], [NSData dataWithContentsOfFile:certPath_root], [NSData dataWithContentsOfFile:certPath_validation]]];
    _sharedSecureManager.securityPolicy = securityPolicy;
}

#pragma mark -

+ (NSString *)BASE_URL {
    if (IS_API_URL_TEST) {
        return kMythTestBaseURL;
    } else {
        return kMythProdBaseURL;
    }
}

@end
