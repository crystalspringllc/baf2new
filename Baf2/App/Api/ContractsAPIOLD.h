//
//  ContractsAPIOLD.h
//  Myth
//
//  Created by Almas Adilbek on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIClientOLD.h"
#import "Contract.h"
#import "Payment.h"
#import "Operation.h"

@interface ContractsAPIOLD : NSObject {
    NSDictionary *data;
}

@property (nonatomic, strong) NSDictionary *data;

-(id)initWithDatas:(NSDictionary *)datas;
+(NSArray *)responseToContracts:(id)response;

@end