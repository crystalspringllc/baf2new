//
//  APIClientOLD.h
//  Baf2
//
//  Created by Askar Mustafin on 4/7/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "RequestsCache.h"
#import "AppUtils.h"
#import "User.h"
#import "Localization.h"
#import "NSObject+Json.h"
#import "Constants.h"


#define kMythProdBaseURL @"https://24.bankastana.kz/MobileApplication/mythapi"
//#define kMythTestBaseURL @"http://192.168.4.71:8080/MobileApplication/mythapi" // MAIN LOCAL
//#define kMythTestBaseURL @"http://test.24.bankastana.kz/MobileApplication/mythapi"
#define kMythTestBaseURL @"http://test.myth.kz/MobileApplication/mythapi" // THIS IS TST FOR GLOBAL
//#define kMythTestBaseURL @"http://pp-rest.cs.kz:8080/ServiceSystem" // PREPROD

typedef void (^Success)(id response);
typedef void (^Failure)(NSInteger code, NSString *message);
typedef void (^successMessage)(NSString *message);
typedef void (^cacheSuccess)(id response);
typedef void (^successStatus)(BOOL isSuccess);
typedef void (^finally)(BOOL isSuccess);

static NSString *kSessionKeyParam = @"session_key";

#define kBrandId @"2"

@interface APIClientOLD : AFHTTPSessionManager

+ (NSURLSessionDataTask *)POST:(NSString *)command parameters:(NSDictionary *)parameters success:(Success)success failure:(Failure)failure;

@end
