//
//  NewsApi.m
//  Baf2
//
//  Created by nmaksut on 16.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NewsApi.h"
#import "NSDate+Ext.h"
#import "NSData+Base64.h"

@implementation NewsApi

+ (NSURLSessionDataTask *)getBankNewsWithLastItem:(NSString *)last
                                          success:(success)success
                                          failure:(failure)failure {
    NSMutableDictionary *params;
    if (last) {
        params = [NSMutableDictionary new];
        params[@"last"] = @([NSDate timestampFromString:last]);
    }
    NSLog(@"Последнее новости банка %@", params[@"last"]);
    
    return [self POST:@"news_feed/get_bank_news" parameters:params success:^(id response) {
        NSString *last2;
        last2 = [response objectForKey:@"last"];
        if (!last2) last2 = @"";
        
        NSMutableArray *news = [[NSMutableArray alloc] init];
        for (NSDictionary *object in response[@"news"]) {
            News *newsItem = [News instanceFromDictionary:object];
            [news addObject:newsItem];
        }

        success(@{@"news" : news, @"last" : last2});

    } failure:failure];
}

+ (NSURLSessionDataTask *)getGeneralNewsWithLastItem:(NSString *)last
                                             success:(success)success
                                             failure:(failure)failure {
    NSMutableDictionary *params;
    if (last) {
        params = [NSMutableDictionary new];
        params[@"last"] = @([NSDate timestampFromString:last]);
    }
    NSLog(@"Последнее форум банка %@", params[@"last"]);
    return [self POST:@"news_feed/get_general_news" parameters:params success:^(id response) {
        NSString *last2;
        last2 = [response objectForKey:@"last"];
        if (!last2) last2 = @"";

        NSMutableArray *news = [[NSMutableArray alloc] init];
        for (NSDictionary *object in response[@"news"]) {
            News *newsItem = [News instanceFromDictionary:object];
            [news addObject:newsItem];
        }
        
        success(@{@"news" : news, @"last" : last2});
    } failure:failure];
}



+ (NSURLSessionDataTask *)addPostWithSubject:(NSString *)subject
                                     message:(NSString *)message
                                       share:(NSString *)share
                                   imageUrls:(NSArray *)imageUrls
                                     success:(success)success
                                     failure:(failure)failure {
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"subject"] = subject ? subject : @"";
    params[@"message"] = message;
    params[@"share"]   = share;
    
    if (!imageUrls) imageUrls = [NSArray new];

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:imageUrls options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    params[@"images"] = jsonString;
    
    return [self POST:@"news_feed/add_post" parameters:params success:success failure:failure];
}


+ (NSURLSessionDataTask *)storeImage:(UIImage *)image
                             success:(success)success
                             failure:(failure)failure {
    NSData *data = UIImageJPEGRepresentation(image, 0.8);
    NSString *base64 = [data base64EncodedString];
    
    return [self POST:@"news_feed/store_news_image" parameters:@{@"imageB64" : base64} success:success failure:failure];
}


+ (NSURLSessionDataTask *)addCommentNewsId:(NSString *)newsId
                                   message:(NSString *)message
                                   success:(success)success
                                   failure:(failure)failure {
    NSDictionary *params = @{@"newsId" : newsId, @"message" : message};
    return [self POST:@"news_feed/add_comment" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)getNewsById:(NSString *)newsId
                              success:(success)success
                              failure:(failure)failure {
    
    NSDictionary *params = @{@"newsId" : newsId};
    return [self POST:@"news_feed/get_news_by_id" parameters:params success:^(id response) {
        NSMutableArray *news = [[NSMutableArray alloc] init];
        for (NSDictionary *object in response[@"news"]) {
            News *newsItem = [News instanceFromDictionary:object];
            [news addObject:newsItem];
        }
        success(news.firstObject);
        
    } failure:failure];
}

+ (NSURLSessionDataTask *)getPrivateNewsWithLast:(NSString *)last
                                         success:(success)success
                                         failure:(failure)failure {
    NSDictionary *params;
    if (last) {
        params = @{@"last" : @([NSDate timestampFromString:last])};
    }
    
    return [self POST:@"news_feed/get_private_news" parameters:params success:^(id response) {
        NSMutableArray *news = [[NSMutableArray alloc] init];
        for (NSDictionary *object in response[@"news"]) {
            News *newsItem = [News instanceFromDictionary:object];
            [news addObject:newsItem];
        }

        NSString *last = [response objectForKey:@"last"];
        if (!news) news = [NSArray new];
        if (!last) last = @"";
        success(@{@"news" : news, @"last" : last});
        
    } failure:failure];
}


+ (NSURLSessionDataTask *)likeObjectID:(NSString *)objectId
                                  type:(NSString *)type
                               success:(success)success
                               failure:(failure)failure {
    NSDictionary *params = @{@"id" : objectId, @"type" : type};
    return [self POST:@"news_feed/like" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)dislikeObjectID:(NSString *)objectId
                                     type:(NSString *)type
                                  success:(success)success
                                  failure:(failure)failure {
    NSDictionary *params = @{@"id" : objectId, @"type" : type};
    return [self POST:@"news_feed/dislike" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)deleteObjectID:(NSString *)objectId
                                    type:(NSString *)type
                                 success:(success)success
                                 failure:(failure)failure {
    NSDictionary *params = @{@"id" : objectId, @"type" : type};
    return [self POST:@"news_feed/delete" parameters:params success:success failure:failure];
}

@end
