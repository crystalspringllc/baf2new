//
//  InvoiceApi.h
//  Baf2
//
//  Created by nmaksut on 25.07.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ApiClient.h"

typedef enum : NSUInteger {
    RejectStateAlreadyPaid,
    RejectStatePaidFromOther,
    RejectStateWillPayNextMonth,
} RejectState;

@interface InvoiceApi : ApiClient

+ (NSURLSessionDataTask *)getInvoiceIdRecentByContract:(NSString *)contract
                                            providerId:(NSNumber *)providerId
                                               success:(success)success
                                               failure:(failure)failure;

+ (NSURLSessionDataTask *)setInvoiceRejectedByInvoiceContract:(NSString *)invoiceContract
                                                   contractId:(NSNumber *)contractId
                                                   providerId:(NSNumber *)providerId
                                                   formedDate:(NSString *)formedDate
                                                  rejectState:(RejectState)rejectState
                                                      success:(success)success
                                                      failure:(failure)failure;


+ (NSURLSessionDataTask *)getInvoiceById:(NSNumber *)invoiceId
                                 success:(success)success
                                 failure:(failure)failure;

+ (NSURLSessionDataTask *)cancelInvoiceRejectByInvoiceContract:(NSString *)invoiceContract
                                                    contractId:(NSNumber *)contractId
                                                    providerId:(NSNumber *)providerId
                                                    formedDate:(NSString *)formedDate
                                                       success:(success)success
                                                       failure:(failure)failure;

+ (NSURLSessionDataTask *)saveInvoiceWithInvoiceContract:(NSString *)invoiceContract
                                                 invoice:(NSString *)invoice
                                              providerId:(NSNumber *)providerId
                                              contractId:(NSNumber *)contractId
                                                 success:(success)success
                                                 failure:(failure)failure;

+ (NSURLSessionDataTask *)getInvoicesHistory:(NSString *)invoice
                                  providerId:(NSNumber *)providerId
                                     success:(success)success failure:(failure)failure;

+ (NSURLSessionDataTask *)getServiceLogIdByInvoiceId:(NSNumber *)invoiceId
                                             success:(success)success
                                             failure:(failure)failure;


@end


