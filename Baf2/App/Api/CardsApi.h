//
//  CardsApi.h
//  Myth
//
//  Created by Almas Adilbek on 12/19/13.
//
//

#import <Foundation/Foundation.h>
#import "APIClientOLD.h"

#define kStatusEpayActivationFailed @"epay_activation_failed"
#define kStatusCardExpired @"card_expired"

@interface CardsApi : APIClientOLD

+ (void)sendSMSAgainWithSuccess:(Success)success failure:(Failure)failure;

@end