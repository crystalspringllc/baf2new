//
// Created by Almas Adilbek on 8/28/14.
//

#import "AADatePicker.h"

@implementation AADatePicker {
    UIView *datePickerView;
    UIView *dim;
    UIDatePicker *datePicker;
}

-(id)init {
    self = [super init];
    if(self) {

    }
    return self;
}

- (void)show {
    [self showWithDate:[NSDate date]];
}

- (void)showWithDate:(NSDate *)date
{
    self.date = date;

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];

    dim = [[UIView alloc] initWithFrame:screenRect];
    dim.backgroundColor = [UIColor blackColor];
    dim.alpha = 0.0;
    [window addSubview:dim];

    if(!datePickerView)
    {
        datePicker = [[UIDatePicker alloc] init];
        if (_date) datePicker.date = _date;
        [datePicker setDatePickerMode:UIDatePickerModeDate];
        [datePicker addTarget:self action:@selector(datePickerDateChanged:) forControlEvents:UIControlEventValueChanged];
        datePicker.backgroundColor = [UIColor whiteColor];

        CGSize pickerSize = [datePicker sizeThatFits:CGSizeZero];
        CGRect f = datePicker.frame;
        f.origin.y = 44;
        f.size.width = screenRect.size.width;
        datePicker.frame = f;

        datePickerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenRect.size.width, pickerSize.height + 44)];
        datePickerView.backgroundColor = [UIColor clearColor];
        [datePickerView addSubview:datePicker];

        //Toolbar
        UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, screenRect.size.width, 44)];
        pickerToolbar.barStyle=UIBarStyleDefault;
        [pickerToolbar sizeToFit];

        NSMutableArray *barItems = [NSMutableArray array];

        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [barItems addObject:flexSpace];

        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDoneClicked)];
        [barItems addObject:doneButton];

        UIBarButtonItem *flexSpace2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        flexSpace2.width = 3;
        [barItems addObject:flexSpace2];

        [pickerToolbar setItems:barItems animated:NO];
        [datePickerView addSubview:pickerToolbar];

        [window addSubview:datePickerView];
    }

    CGSize pickerViewSize = datePickerView.frame.size;
    CGRect startRect = CGRectMake(0.0, screenRect.origin.y + screenRect.size.height, pickerViewSize.width, pickerViewSize.height);
    datePickerView.frame = startRect;

    CGRect pickerRect = CGRectMake(0.0, screenRect.origin.y + screenRect.size.height - pickerViewSize.height, pickerViewSize.width, pickerViewSize.height);

    [UIView animateWithDuration:0.25 animations:^{
        dim.alpha = 0.2;
        datePickerView.frame = pickerRect;
    }];
}

#pragma mark -
#pragma mark UIBarButtonItem

- (void)datePickerDoneClicked
{
    CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
    CGSize pickerViewSize = datePickerView.frame.size;
    CGRect startRect = CGRectMake(0.0, screenRect.origin.y + screenRect.size.height, pickerViewSize.width, pickerViewSize.height);
    [UIView animateWithDuration:0.25 animations:^{
        datePickerView.frame = startRect;
        dim.alpha = 0;
    } completion:^(BOOL finished) {
        [dim removeFromSuperview];
        [datePickerView removeFromSuperview];
        datePickerView = nil;
    }];

    if([self.delegate respondsToSelector:@selector(aaDatePickerDoneTapped:)]) {
        [self.delegate aaDatePickerDoneTapped:self];
    }
}

#pragma mark -
#pragma mark UIDatePicker

- (void)datePickerDateChanged:(UIDatePicker *)_datePicker
{
    self.date = datePicker.date;
    if([self.delegate respondsToSelector:@selector(aaDatePicker:dateChanged:)]) {
        [self.delegate aaDatePicker:self dateChanged:_date];
    }
}

@end