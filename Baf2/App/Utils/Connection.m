//
//  Connection.m
//  AstanaKzPublicControl
//
//  Created by Almas Adilbek on 6/4/13.
//  Copyright (c) 2013 Astana.kz. All rights reserved.
//

#import "Connection.h"
#import "Reachability.h"

#if TARGET_BAF
#import "UIHelper.h"
#endif

NSString *const kConnectionReachabilityChanged = @"connectionReachabilityChanged";

static BOOL _isReachable;

@implementation Connection

+(void)setIsReachable:(BOOL)isReachable {
    _isReachable = isReachable;
}

+(BOOL)isReachable {
    return [self isReachabilityReachable];
}

+(BOOL)isNotReachable {
    return ![self isReachabilityReachable];
}

+(BOOL)isReachableM {
    BOOL b = [self isReachable];
    #if TARGET_BAF
    if(!b) [UIHelper showOffile];
    #endif
    return b;
}

+(BOOL)isReachabilityReachable {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    return internetStatus != NotReachable;
}

@end
