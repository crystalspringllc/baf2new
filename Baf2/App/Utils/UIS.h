//
//  UIS.h
//  Myth
//
//  Created by Almas Adilbek on 08/08/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIS : NSObject

+(void)presentTermsAndConditionsWithTarget:(id)target;

@end
