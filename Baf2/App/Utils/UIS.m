//
//  UIS.m
//  Myth
//
//  Created by Almas Adilbek on 08/08/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "UIS.h"
#import "NBrowserViewController.h"
#import "UIHelper.h"

#define kBankPrivacyUrl @"https://www.myth.kz/public/politics-baf/politics-ru.html"

@implementation UIS

+(void)presentTermsAndConditionsWithTarget:(id)target {
//    NBrowserViewController *v = [[NBrowserViewController alloc] initWithUrls:[NSURL URLWithString:@"https://www.24.bankastana.kz/public/politics-baf/politics-ru.html"]];
    NBrowserViewController *v = [[NBrowserViewController alloc] initWithUrls:[NSURL URLWithString:kBankPrivacyUrl]];
    v.toolbar.tintColor = [UIColor blueColor];
    UINavigationController *nc = [UIHelper defaultNavigationController];
    nc.navigationBar.barStyle = UIBarStyleDefault;
    [nc setViewControllers:@[v]];
    [target presentViewController:nc animated:YES completion:nil];
}

@end
