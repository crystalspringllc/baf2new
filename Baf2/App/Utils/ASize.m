//
//  ASize.m
//  PointPlus
//
//  Created by Almas Adilbek on 11/9/12.
//
//

#import "ASize.h"
#import <UIKit/UIKit.h>
#import "Version.h"

@implementation ASize

+(float)screenWidth {
    return [[UIScreen mainScreen] bounds].size.width;
}
+(float)screenHeight {
    return [[UIScreen mainScreen] bounds].size.height;
}
+(float)screenHeightWithoutStatusBar {
    return [self screenHeight] - 20;
}
+(float)screenHeightWithoutStatusBarAndTabBar {
    return [self screenHeightWithoutStatusBar] - 49;
}
+(float)screenHeightWithoutStatusBarAndNavigationBar {
    return [self screenHeightWithoutStatusBar] - 44;
}
+(float)screenHeightWithoutStatusBarAndNavigationBarAndTabBar {
    return [self screenHeightWithoutStatusBarAndTabBar] - 49;
}
+(float)screenHeightWithoutStatusBarAndNavigationBarAndTabBarAndKeyboard {
    return [self screenHeightWithoutStatusBarAndNavigationBar] - [self UIKeyboardHeight];
}
+(float)screenHeightWithoutNavigationBar {
    return [self screenHeight] - 44;
}

+ (CGFloat)screenWidthSidePadding:(CGFloat)padding {
    return [self screenWidth] - 2 * padding;
}

+ (CGFloat)screenWidthPercent:(CGFloat)percent {
    return (CGFloat) ([self screenWidth] * (percent * 0.01));
}

+ (CGFloat)screenMiddleX {
    return (CGFloat) ([self screenWidth] * 0.5);
}

+(BOOL)isIpad {
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
}

+ (BOOL)is35Screen {
    return [self screenHeight] == 480;
}

+ (BOOL)isOlderThanIphone6 {
    return [self screenWidth] == 320;
}

+(BOOL)isIPhone5OrLater {
    return ![self isIpad] && [self screenHeight] > 480;
}

+(float)groupTableSidePadding {
    return [Version iOS7OrLater]?0:(![self isIpad]?9:44);
}
+(float)groupTableEndCoordinateX {
    return [self screenWidth] - [self groupTableSidePadding];
}
+(float)groupTableWidth {
    return [self screenWidth] - 2 * [self groupTableSidePadding];
}

+(float)UIKeyboardHeight {
    return [self isIpad]?kUIiPadKeyboardHeight:kUIKeyboardHeight;
}

+(float)UIKeyboardYWithoutNavigationBarAndStatusBar {
    return [self screenHeight] - [self UIKeyboardHeight];
}

+(float)UIKeyboardY {
    return [self screenHeightWithoutStatusBarAndNavigationBar] - [self UIKeyboardHeight];
}

+(CGFloat)iphone:(CGFloat)iphoneValue ipad:(CGFloat)ipadValue {
    return ![self isIpad]?iphoneValue:ipadValue;
}

+(float)iphone5:(float)iphone5Value iphone4:(float)iphone4Value {
    return [self isIPhone5OrLater]?iphone5Value:iphone4Value;
}

+ (CGFloat)screen35:(CGFloat)screen35Value higherIphone:(CGFloat)higherIphoneValue ipad:(CGFloat)ipadValue {
    if([self isIpad]) return ipadValue;
    if([self is35Screen]) return screen35Value;
    return higherIphoneValue;
}

+ (CGFloat)olderThanIphone6:(CGFloat)orderThanIphone6 higherIphone:(CGFloat)higherIphoneValue ipad:(CGFloat)ipadValue {
    if([self isIpad]) return ipadValue;
    if([self isOlderThanIphone6]) return orderThanIphone6;
    return higherIphoneValue;
}

+(float)iOS7:(float)ios7Value or:(float)orValue {
    return [Version iOS7OrLater]?ios7Value:orValue;
}

+(float)iOS7StatusBarBottom {
    return [self iOS7:20 or:0];
}

+ (CGFloat)iphone4:(CGFloat)iphone4Value iphone5:(CGFloat)iphone5Value iphone6:(CGFloat)iphone6Value iphone6Plus:(CGFloat)iphone6HDValue {
    CGFloat w = [[UIScreen mainScreen] bounds].size.width;
    CGFloat h = [[UIScreen mainScreen] bounds].size.height;
    if(w == 320) {
        if(h == 480) return iphone4Value;
        else return iphone5Value;
    }
    if(w == 375) return iphone6Value;
    return iphone6HDValue;
}

+ (CGFloat)iphone4:(CGFloat)iphone4Value iphone5:(CGFloat)iphone5Value iphone6:(CGFloat)iphone6Value iphone6Plus:(CGFloat)iphone6HDValue ipad:(CGFloat)ipadValue {
    if([self isIpad]) return ipadValue;

    CGFloat w = [[UIScreen mainScreen] bounds].size.width;
    CGFloat h = [[UIScreen mainScreen] bounds].size.height;
    if(w == 320) {
        if(h == 480) return iphone4Value;
        else return iphone5Value;
    }
    if(w == 375) return iphone6Value;
    return iphone6HDValue;
}


+ (CGFloat)iphone5:(CGFloat)iphone5Value iphone6:(CGFloat)iphone6Value iphone6HD:(CGFloat)iphone6HDValue
{
    CGFloat w = [[UIScreen mainScreen] bounds].size.width;
    if(w == 320) return iphone5Value;
    if(w == 375) return iphone6Value;
    return iphone6HDValue;
}

+ (CGFloat)iphone6:(CGFloat)iphone6Value iphone6Plus:(CGFloat)iphone6PlusValue {
    return [self isIphone6] ? iphone6Value : iphone6PlusValue;
}

+(BOOL)isIphone6 {
    CGFloat w = [[UIScreen mainScreen] bounds].size.width;
    return w == 375;
}

+(BOOL)isIphone6Plus {
    CGFloat w = [[UIScreen mainScreen] bounds].size.width;
    return w == 414;
}

@end
