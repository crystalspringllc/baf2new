//
//  Common.m
//  Kurs
//
//  Created by Mustafin Askar on 17.06.13.
//  Copyright (c) 2013 askar. All rights reserved.
//

#import "Common.h"

@implementation Common

+ (NSString *)getPathOfNeededFile:(NSString *)file withType:(NSString *)type {
    NSArray *dirPaths  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = dirPaths[0];
    NSString *storePath;
    if (type){
        storePath = [NSString stringWithFormat:@"%@/%@.%@", docsDir, file, type];
    }else{
        storePath = [NSString stringWithFormat:@"%@/%@", docsDir, file];
    }

    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:storePath]) {
        return [[NSBundle mainBundle] pathForResource:file ofType:type];
    }
    return storePath;
}

@end