//
// Created by Askar Mustafin on 4/10/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface UIHelper : NSObject

+ (void)showAlertWithMessage:(NSString *)message;
+ (void)showAlertWithMessageTitle:(NSString *)title message:(NSString *)message;

+ (UINavigationController *)defaultNavigationController;

+(UIView *)successMessageWithIconWithContentWidth:(CGFloat)width message:(NSString *)message;
+(UIView *)failureMessageWithIconWithContentWidth:(CGFloat)width message:(NSString *)message;

+(UIView *)messageWithIcon:(NSString *)iconName contentWidth:(CGFloat)width message:(NSString *)message;


// Getters
+ (AppDelegate *)appDelegate;
+ (UIWindow *)delegateWindow;

// Alerts
+(void)showErrorAlertWithMessage:(NSString *)message;
+(void)showAlertTryAgain;
+(void)showAlertWithFioTreatmentAndMessage:(NSString *)message;
+(void)showAlertContractNotValidForPayment;

+ (void)showActionSheetAuthorizationRequiredWithTarget:(id)target;

+(void)showOffile;

// Redirects
+ (void)showEmailPhoneConfirmationWindow:(BOOL)showMessage target:(void(^)())completion;
+ (UINavigationController *)rootNavigationController;

// Helper functions
+(void)closeOpenModalWindowAnimated:(BOOL)animated;
+(void)showModalViewControllerWithDefaultNavigationController:(UIViewController *)viewController;
+(void)presentBrowserIn:(id)target withUrl:(NSString *)url;

//Call phone
+ (void)callPhone:(NSString *)phone showSheetInView:(UIView *)view;

@end