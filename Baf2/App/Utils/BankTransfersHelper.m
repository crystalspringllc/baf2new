//
//  BankTransfersHelper.m
//  BAF
//
//  Created by Almas Adilbek on 3/4/15.
//
//

#import "BankTransfersHelper.h"

@implementation BankTransfersHelper

+ (BOOL)isResidentWithKbe:(NSString *)kbe {
    if(kbe.length > 1) {
        NSString *value = [kbe substringToIndex:1];
        return [value isEqual:@"1"];
    }
    return NO;
}

+ (NSString *)economySectorWithKbe:(NSString *)kbe
{
    if(kbe.length > 1) {
        return [kbe substringFromIndex:1];
    }
    return @"";
}


@end
