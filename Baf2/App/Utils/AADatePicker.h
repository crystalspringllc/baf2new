//
// Created by Almas Adilbek on 8/28/14.
//

#import <Foundation/Foundation.h>

@protocol AADatePickerDelegate;

@interface AADatePicker : NSObject

@property(nonatomic, weak) id <AADatePickerDelegate> delegate;
@property(nonatomic, assign) NSUInteger tag;
@property(nonatomic, strong) NSDate *date;

- (void)show;
- (void)showWithDate:(NSDate *)date;

@end


@protocol AADatePickerDelegate<NSObject>
-(void)aaDatePicker:(AADatePicker *)datePicker dateChanged:(NSDate *)date;
-(void)aaDatePickerDoneTapped:(AADatePicker *)datePicker;
@end