//
//  AccountHelper.m
//  BAF
//
//  Created by Almas Adilbek on 3/10/15.
//
//

#import "AccountHelper.h"
#import "CurrentAccount.h"
#import "Account.h"
#import "BeneficiaryAccount.h"
#import "Card.h"
#import "Current.h"
#import "Deposit.h"
#import "Beneficiary.h"
#import "CardAccount.h"
#import "ExtCard.h"
#import "Loan.h"

@implementation AccountHelper

+ (NSString *)accountCurrency:(id)account
{
    if ([self isCurrentAccount:account])
    {
        return [(CurrentAccount *)account currency];
    }
    else if ([account isKindOfClass:[FinancialProduct class]])
    {
        return [(FinancialProduct *)account currency];
    }
    else if ([account isKindOfClass:[BeneficiaryAccount class]])
    {
        return [(BeneficiaryAccount *)account currency];
    } else {
        return [(Account *)account currency];
    }
    return nil;
}

+ (NSString *)accountIdentifierAlias:(id)account
{
    if ([account isKindOfClass:[Card class]])
    {
        return [(Card *)account number];
    }
    else if ([account isKindOfClass:[Deposit class]])
    {
        return [(Deposit *)account alias];
    } else if ([account isKindOfClass:[Current class]])
    {
        return [(Current *)account alias];
    } else if ([account isKindOfClass:[Beneficiary class]])
    {
        return [(Beneficiary *)account alias];
    }
    else if ([account isKindOfClass:[FinancialProduct class]])
    {
        return [NSString stringWithFormat:@"%@ %@", [(FinancialProduct *)account iban], [(FinancialProduct *)account currency]];
    }
    else if ([account isKindOfClass:[BeneficiaryAccount class]])
    {
        return [(BeneficiaryAccount *)account fullAliasWithCurrency];
    } else {
        return [NSString stringWithFormat:@"%@ (%@)", [(Account *)account alias], [(Account *)account currency]];
    }
    
    return nil;
}

+ (NSNumber *)accountId:(id)account {
    if([self isCurrentAccount:account]) {
        return [(CurrentAccount *) account accountId];
    } else if([account isKindOfClass:[FinancialProduct class]]) {
        return [(FinancialProduct *) account accountId];
    } else if([account isKindOfClass:[BeneficiaryAccount class]]) {
        return [(BeneficiaryAccount *) account beneficiaryAccountId];
    }
    return nil;
}

+ (BOOL)isCurrentAccount:(id)account {
    return [account isKindOfClass:[CurrentAccount class]];
}

+ (NSNumber *)getIdOfAccount:(id)account {
    if ([account isKindOfClass: [Card class]]) {
        Card *tCard = account;
        return tCard.cardId;
    } else if ([account isKindOfClass: [CardAccount class]]) {
        CardAccount *tCardAcccount = account;
        return tCardAcccount.cardAccountId;
    } else if ([account isKindOfClass: [Current class]]) {
        Current *tCurrent = account;
        return tCurrent.accountId;
    } else if ([account isKindOfClass: [Deposit class]]) {
        Deposit *tDeposit = account;
        return tDeposit.accountId;
    } else if ([account isKindOfClass: [ExtCard class]]) {
        ExtCard *tExtCard = account;
        return tExtCard.accountId;
    } else if ([account isKindOfClass: [Loan class]]) {
        Loan *tLoan = account;
        return tLoan.loanId;
    } else if ([account isKindOfClass:[Beneficiary class]]) {
        Beneficiary *tBeneficiary = account;
        return tBeneficiary.accountId;
    } else if ([account isKindOfClass:[Account class]]) {
        Account *tAccount = account;
        return tAccount.accountId;
    } else {
        return nil;
    }
}

@end
