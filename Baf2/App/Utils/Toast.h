//
//  Toast.h
//  Kiwi.kz
//
//  Created by Almas Adilbek on 08/20/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//



@interface Toast : NSObject

+ (void)showToast:(NSString *)title;

@end
