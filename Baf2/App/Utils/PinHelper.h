//
//  PinHelper.h
//  ACB
//
//  Created by Almas Adilbek on 6/1/15.
//
//

#import <Foundation/Foundation.h>

#define kKeyUsername @"username"
#define kUserUseTouchID @"useTouchID"
#define kUseTouchIDAsked @"useTouchIDAsked"

@interface PinHelper : NSObject

#define kPinTokenKey @"pin_token"

//+ (BOOL)isPinTokenSet;
+ (BOOL)savePinToken:(NSString *)pinToken withPin:(NSString*)pinCode;
+ (NSString *)pinToken;
+ (NSString *)pinCode;

+ (void)deletePinToken;

@end
