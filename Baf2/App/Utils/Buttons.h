//
//  Buttons.h
//  BAF
//
//  Created by Almas Adilbek on 10/22/14.
//
//

#import <UIKit/UIKit.h>

@interface Buttons : NSObject

+ (UIButton *)mapButtonWithTarget:(id)target selector:(SEL)_selector;
+ (UIButton *)backButtonWithTarget:(id)target selector:(SEL)_selector;
+ (UIButton *)questionButtonWithTarget:(id)target selector:(SEL)_selector;
+ (UIButton *)infoButtonWithTarget:(id)target selector:(SEL)_selector;
+ (UIButton *)refreshButtonWithTarget:(id)target selector:(SEL)_selector;
+ (UIButton *)settingsButtonWithTarget:(id)target selector:(SEL)_selector;
+ (UIButton *)closeButtonWithTarget:(id)target selector:(SEL)_selector;
+ (UIButton *)closeButtonWithTarget:(id)target selector:(SEL)_selector btnColorLight:(BOOL)btnColorLight;
+ (UIButton *)writeButtonWithTarget:(id)target selector:(SEL)_selector;
+ (UIButton *)searchButtonWithTarget:(id)target selector:(SEL)_selector;

@end
