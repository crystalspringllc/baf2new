//
//  ScrollDirectionsHelper.h
//  KZNews
//
//  Created by Almas Adilbek on 8/8/15.
//  Copyright (c) 2015 Almas Adilbek. All rights reserved.
//

typedef enum ScrollDirection {
    ScrollDirectionUp = 0,
    ScrollDirectionDown = 1,
} ScrollDirection;

@interface ScrollDirectionsHelper : NSObject

@property(nonatomic, assign) CGFloat lastContentOffset;
@property(nonatomic, assign) CGFloat triggerOffset;
@property(nonatomic, assign) CGFloat scrollDuration;

- (void)scrollView:(UIView *)view pinTop:(CGFloat)offset scrollView:(UIScrollView *)scrollView;
- (void)scrollViewAnimated:(UIView *)view pinTop:(CGFloat)offset scrollView:(UIScrollView *)scrollView;
- (void)showViewAnimated:(UIView *)view topOffset:(CGFloat)offset;

- (void)scrollView:(UIView *)view viewTopY:(CGFloat)viewTopY scrollView:(UIScrollView *)scrollView;
- (void)scrollAutolayoutView:(UIView *)view scrollView:(UIScrollView *)scrollView;

- (void)setBottomConstraint:(UIView *)view offset:(CGFloat)offset;
- (void)setTopConstraint:(UIView *)view offset:(CGFloat)offset;

- (void)showAutolayoutView:(UIView *)view;

@end
