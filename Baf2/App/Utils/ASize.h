//
//  ASize.h
//  PointPlus
//
//  Created by Almas Adilbek on 11/9/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

#define kUIKeyboardHeight 216
#define kUIiPadKeyboardHeight 263

@interface ASize : NSObject

+(float)screenWidth;
+(float)screenHeight;
+(float)screenHeightWithoutStatusBar;
+(float)screenHeightWithoutStatusBarAndTabBar;
+(float)screenHeightWithoutStatusBarAndNavigationBar;
+(float)screenHeightWithoutStatusBarAndNavigationBarAndTabBar;
+(float)screenHeightWithoutStatusBarAndNavigationBarAndTabBarAndKeyboard;
+(float)screenHeightWithoutNavigationBar;

+ (CGFloat)screenWidthSidePadding:(CGFloat)padding;
+ (CGFloat)screenWidthPercent:(CGFloat)percent;

+ (CGFloat)screenMiddleX;

+(BOOL)isIpad;
+(BOOL)is35Screen;
+(BOOL)isIPhone5OrLater;
+(float)groupTableSidePadding;
+(float)groupTableEndCoordinateX;
+(float)groupTableWidth;

+(float)UIKeyboardHeight;
+(float)UIKeyboardYWithoutNavigationBarAndStatusBar;
+(float)UIKeyboardY;

+(CGFloat)iphone:(CGFloat)iphoneValue ipad:(CGFloat)ipadValue;
+(float)iphone5:(float)iphone5Value iphone4:(float)iphone4Value;
+(CGFloat)screen35:(CGFloat)screen35Value higherIphone:(CGFloat)higherIphoneValue ipad:(CGFloat)ipadValue;
+ (CGFloat)olderThanIphone6:(CGFloat)orderThanIphone6 higherIphone:(CGFloat)higherIphoneValue ipad:(CGFloat)ipadValue;

+(float)iOS7StatusBarBottom;
+(float)iOS7:(float)ios7Value or:(float)orValue;

+ (CGFloat)iphone4:(CGFloat)iphone4Value iphone5:(CGFloat)iphone5Value iphone6:(CGFloat)iphone6Value iphone6Plus:(CGFloat)iphone6HDValue;
+ (CGFloat)iphone4:(CGFloat)iphone4Value iphone5:(CGFloat)iphone5Value iphone6:(CGFloat)iphone6Value iphone6Plus:(CGFloat)iphone6HDValue ipad:(CGFloat)ipadValue;
+ (CGFloat)iphone5:(CGFloat)iphone5Value iphone6:(CGFloat)iphone6Value iphone6HD:(CGFloat)iphone6HDValue;
+ (CGFloat)iphone6:(CGFloat)iphone6Value iphone6Plus:(CGFloat)iphone6PlusValue;
+ (BOOL)isIphone6;
+ (BOOL)isIphone6Plus;

@end
