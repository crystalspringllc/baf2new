//
//  BackgroundTaskManager.h
//  BAF
//
//  Created by Almas Adilbek on 10/30/13.
//
//

#import <Foundation/Foundation.h>

typedef void(^BackgroundTaskManagerTaskExpirationHandler)(void);

@interface BackgroundTaskManager : NSObject {
    UIBackgroundTaskIdentifier backgroundTask;
}

@property (nonatomic) UIBackgroundTaskIdentifier backgroundTask;

+(BackgroundTaskManager *)shared;

-(void)startTaskWithExpiration:(BackgroundTaskManagerTaskExpirationHandler)block;
-(void)stopTask;

+(void)kill;

@end
