//
//  AATableSection.m
//  BAF
//
//  Created by Almas Adilbek on 1/22/15.
//
//

#import "AATableSection.h"

@implementation AATableSection

- (id)initWithSectionId:(NSString *)sectionId_ {
    self = [[AATableSection alloc] init];
    if(self) {
        self.sectionId = sectionId_;
    }
    return self;
}

- (NSMutableArray *)rows {
    if(!_rows) {
        self.rows = [[NSMutableArray alloc] init];
    }
    return _rows;
}

- (void)addRow:(id)object
{
    if(!object) return;
    if(!_rows) {
        self.rows = [[NSMutableArray alloc] init];
    }
    [self.rows addObject:object];
}


@end