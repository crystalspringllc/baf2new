//
//  RequestsCache.h
//  Baf2
//
//  Created by Shyngys Kassymov on 14.04.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <PINCache/PINCache.h>

#define kLastUserId @"lastUserId"
#define kGetContractsPath @"getContracts"
#define kProvidersListPath @"providersList"
#define CacheSkipResponseAlert @"skipResponseAlert"
#define CacheAppEnterBackgroundDate @"appEnterBackgroundDate"
#define CacheShowEmailTelActivationAfterReg @"shoeEmailTelActivationAfterReg"
#define CacheShowRequestCardsAfterReg @"showRequestCardsAfterReg"

#define CacheResponseMyAccountsPostFix @"myAccountsCache"
#define CacheResponseNews @"newsCache"

#define kCacheCards @"CASHE_CARDS"

@interface RequestsCache : PINCache


+ (id)getUserResponseFromDiskWithPath:(NSString *)path;
+ (void)storeInDiskUserResponse:(id)response forPath:(NSString *)path;


+ (id)getResponseWithPath:(NSString *)path;
+ (void)storeResponse:(id)response forPath:(NSString *)path;


+ (id)getUserResponseWithPath:(NSString *)path;
+ (void)storeUserResponse:(id)response forPath:(NSString *)path;


+ (void)setValue:(id)value forKey:(NSString *)key;
+ (id)valueForKey:(NSString *)key;


+ (void)setBool:(BOOL)boolValue forKey:(NSString *)key;
+ (BOOL)boolForKey:(NSString *)key;
+ (BOOL)onceBoolForKey:(NSString *)key;


#pragma mark - Responses

+ (void)setResponseJson:(NSString *)json withKey:(NSString *)responseKey;

#pragma mark - Specific Methods

+ (void)storeProvidersListResponse:(NSDictionary *)response;


#pragma mark - Helpers

+ (NSString *)userPathWithPath:(NSString *)path;

@end
