//
//  Toast.m
//  Kiwi.kz
//
//  Created by Almas Adilbek on 08/20/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "Toast.h"

#define TOAST_DURATION 6.0f

@implementation Toast

+ (void)showToast:(NSString *)title {
    [((AppDelegate *)[UIApplication sharedApplication].delegate).window makeToast:title duration:TOAST_DURATION position:CSToastPositionBottom];
}

@end