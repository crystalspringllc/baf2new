//
//  GAN.h
//  PointPlus
//
//  Created by Almas Adilbek on 9/19/12.
//
//

#import <Foundation/Foundation.h>
#import "GAI.h"

#define kGanCategoryCards @"Платежные карты"

@interface GAN : NSObject

+(NSString *)kAnalyticsAccountId;

+(void)start;
+(void)stop;

+(void)sendView:(NSString *)pageName;
+(void)sendEventWithCategory:(NSString *)category action:(NSString *)action;
+(void)sendEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label;
+(void)sendEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label value:(int)value;

+(void)sendException:(NSString *)description;
+(void)sendException:(NSString *)description withFatal:(NSNumber *)fatal;

@end
