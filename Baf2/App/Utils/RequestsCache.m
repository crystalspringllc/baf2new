//
//  RequestsCache.m
//  Baf2
//
//  Created by Shyngys Kassymov on 14.04.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "RequestsCache.h"
#import "User.h"
#import "NSObject+Json.h"
#import "NSObject+Ext.h"

@implementation RequestsCache

/**
 *  Getting stored object by path
 */
+ (id)getUserResponseFromDiskWithPath:(NSString *)path {
    return [[PINDiskCache sharedCache] objectForKey:[self userPathWithPath:path]];
}

/**
 *  Store object in disk by path
 */
+ (void)storeInDiskUserResponse:(id)response forPath:(NSString *)path {
    [[PINDiskCache sharedCache] setObject:response forKey:[self userPathWithPath:path]];
}


+ (id)getResponseWithPath:(NSString *)path {
    NSString *jsonString = [[RequestsCache sharedCache] objectForKey:path];
    if (jsonString) {
        return [jsonString JSONValue];
    }
    
    return nil;
}

+ (void)storeResponse:(id)response forPath:(NSString *)path {
    NSString *jsonString = [response JSONRepresentation];
    [[self sharedCache] setObject:jsonString forKey:path];
}

+ (id)getUserResponseWithPath:(NSString *)path {
    return [self getResponseWithPath:[self userPathWithPath:path]];
}

+ (void)storeUserResponse:(id)response forPath:(NSString *)path {
    [self storeResponse:response forPath:[self userPathWithPath:path]];
}

+ (void)setValue:(id)value forKey:(NSString *)key {
    [[self sharedCache] setObject:value forKey:key];
}

+ (id)valueForKey:(NSString *)key {
    return [[self sharedCache] objectForKey:key];
}

+ (void)setBool:(BOOL)boolValue forKey:(NSString *)key {
    [[self sharedCache] setObject:boolValue?@"1":@"0" forKey:key];
}

+ (BOOL)boolForKey:(NSString *)key {
    id value = [[self sharedCache] objectForKey:key];
    if([self isNull:value]) return NO;
    return [value intValue] == 1;
}

+ (BOOL)onceBoolForKey:(NSString *)key {
    BOOL b = [self boolForKey:key];
    if (b) {
        [self setValue:[NSNull null] forKey:key];
    }
    return b;
}

#pragma mark Responses

+ (void)setResponseJson:(NSString *)json withKey:(NSString *)responseKey {
    [self setValue:json forKey:responseKey];
}

#pragma mark - Specific Methods

+ (void)storeProvidersListResponse:(NSDictionary *)response {
    if (response) {
        [RequestsCache storeResponse:response[@"categoriesProviders"] forPath:kProvidersListPath];
        [RequestsCache storeResponse:response[@"contracts"] forPath:kGetContractsPath];
    }
}

#pragma mark - Helpers

+ (NSString *)userPathWithPath:(NSString *)path {
    return [NSString stringWithFormat:@"%@_%@", path, @([[User sharedInstance] userID])];
}

@end
