//
//  AppUtils.m
//  Myth
//
//  Created by Almas Adilbek on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppUtils.h"

#if !(WATCH_OS)
#import "UIDevice-Hardware.h"
#import "ASize.h"
#endif

#import "NSObject+Json.h"
#import "AuthApi.h"
#import <arpa/inet.h>
#import <ifaddrs.h>


#define kDeviceIDKey @"deviceId"

@implementation AppUtils

+ (NSString *)appVersion {
    return [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
}

+ (BOOL)isRetina {
#if !(WATCH_OS)
    return ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0));
#endif
    return false;
}

+ (float)iOSVersion {
#if !(WATCH_OS)
    NSString *ver = [UIDevice currentDevice].systemVersion;
    NSArray *versionCompatibility = [ver componentsSeparatedByString:@"."];
    if([versionCompatibility count] > 2) {
        ver = [NSString stringWithFormat:@"%@.%@", versionCompatibility[0], versionCompatibility[1]];
    }
    return [ver floatValue];
#endif
    return 0;
}

+ (NSString *)ipAddress {
    NSString *address = @"error-getting-ip";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

+ (NSString *)deviceID {
    NSString *deviceId = [[NSUserDefaults standardUserDefaults] valueForKey:kDeviceIDKey];
    if(!deviceId) {
#ifndef WATCH_OS
        deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
#else
        deviceId = [[NSUUID UUID] UUIDString];
#endif
        [[NSUserDefaults standardUserDefaults] setValue:deviceId forKey:kDeviceIDKey];
    }
    return deviceId;
}

+(NSDictionary *)deviceInfo
{
    NSDictionary *deviceInfo;
#if !(WATCH_OS)
    deviceInfo = (NSDictionary *)@{
            @"board":@"",
            @"brand":@"Apple",
            @"device":[[UIDevice currentDevice] platformString],
            @"display":[NSString stringWithFormat:@"%@Retina", [AppUtils isRetina]?@"":@"Not "],
            @"model":[[UIDevice currentDevice] model],
            @"product":[NSString stringWithFormat:@"%f", [AppUtils iOSVersion]],
            @"simoperatorname":@"Kcell",
            /*@"IMEIAndroid" : [AppUtils deviceID],*/
            @"IMEIAndroid" : [AppUtils deviceID],
            @"app_id":[AppUtils appVersion],
            @"display_size":[NSString stringWithFormat:@"%.f:%.f", [ASize screenWidth], [ASize screenHeight]],
            @"v_release":[AppUtils appVersion],
            @"ip":[AppUtils ipAddress],
            @"os" : @"iOS",
            @"push_token": [[NSUserDefaults standardUserDefaults] objectForKey:@"devicePushTokenId"] ? : @""
    };


#else
    deviceInfo = (NSDictionary *)@{
            @"board" : @"",
            @"brand" : @"Apple",
            @"device" : @"Apple Watch",
            @"display" : @"Not Retina",
            @"model" : @"Watch",
            @"product" : [NSString stringWithFormat:@"%f", 9.0],
            @"simoperatorname":@"",
            @"IMEIAndroid" : [AppUtils deviceID],
            @"app_id" : [AppUtils appVersion],
            @"display_size" : @"272:340",
            @"v_release":[AppUtils appVersion],
            @"ip":[AppUtils ipAddress],
            @"os" : @"iOS",
            @"push_token": [[NSUserDefaults standardUserDefaults] objectForKey:@"devicePushTokenId"] ? : @""
    };
#endif
    
    return deviceInfo;
}

+ (NSString *)deviceInfoJsonString {
    return [[self deviceInfo] JSONRepresentation];
}

#pragma mark -

+ (BOOL)isAskar {
#if TARGET_IPHONE_SIMULATOR
    NSProcessInfo* pinfo = [NSProcessInfo processInfo];
    unsigned long long int memory = pinfo.physicalMemory;
    //NSLog(@"%qu", memory);
    return memory == 8589934592;
#endif
    return NO;
}

+ (BOOL)isFirstRun {
    NSString *key = @"appFirstRunDone";
    NSString *isFirstRun = [AppUtils userDefaults:key];
    if(!isFirstRun) {
        [AppUtils setUserDefaults:@"1" key:key];
        return YES;
    }
    return NO;
}

+ (BOOL)isRunningFromDevice {
#if !(WATCH_OS)
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
#endif
    return true;
}

+ (void)setUserDefaults:(id)value key:(NSString *)key {
    NSUserDefaults *ud = [[NSUserDefaults alloc] init];
    [ud setObject:value forKey:key];
}

+ (NSString *)userDefaults:(NSString *)key {
    NSUserDefaults *ud = [[NSUserDefaults alloc] init];
    NSString *value = [ud objectForKey:key];
    return value;
}

+ (BOOL)validateEmail:(NSString *)email {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark -
#pragma mark FileManager

+ (BOOL)isFileExistsInLibraryDirWithFilename:(NSString *)filename {
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL exists = [fm fileExistsAtPath:[AppUtils libraryFilePathWithFilename:filename]];
    return exists;
}

+ (BOOL)isFileExistsInDocumentDirWithFilename:(NSString *)filename {
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL exists = [fm fileExistsAtPath:[AppUtils documentFilePathWithFilename:filename]];
    return exists;
}

+ (BOOL)isFileExistsInBundleWithFilename:(NSString *)filename {
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL exists = [fm fileExistsAtPath:[AppUtils bundleFilePathWithFilename:filename]];
    return exists;
}

+ (NSString *)documentsPath {
    NSArray *a = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return a[0];
}

+ (NSString *)bundleFilePathWithFilename:(NSString *)filename {
    return [[NSBundle mainBundle] pathForResource:filename ofType:@""];
}

+ (NSString *)libraryFilePathWithFilename:(NSString *)filename {
    NSArray *a = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    return [a[0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", filename]];
}

+ (NSString *)documentFilePathWithFilename:(NSString *)filename {
    NSArray *a = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [a[0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", filename]];
}

@end
