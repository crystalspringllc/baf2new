//
//  Buttons.m
//  BAF
//
//  Created by Almas Adilbek on 10/22/14.
//
//

#import "Buttons.h"

@implementation Buttons {

}

+ (UIButton *)mapButtonWithTarget:(id)target selector:(SEL)_selector {
    return [self circleButtonWithTarget:target selector:_selector imagePrefix:@"btn_map"];
}

+ (UIButton *)backButtonWithTarget:(id)target selector:(SEL)_selector {
    return [self buttonWithTarget:target selector:_selector normalImageName:@"back-chevron" pressedImageName:@""];
//    return [self buttonWithTarget:target selector:_selector normalImageName:@"icon-arrow-back" pressedImageName:@""];
}

+ (UIButton *)questionButtonWithTarget:(id)target selector:(SEL)_selector {
    return [self circleButtonWithTarget:target selector:_selector imagePrefix:@"btn_question"];
}

+ (UIButton *)infoButtonWithTarget:(id)target selector:(SEL)_selector {
    return [self circleButtonWithTarget:target selector:_selector imagePrefix:@"btn_info"];
}

+ (UIButton *)refreshButtonWithTarget:(id)target selector:(SEL)_selector {
    return [self circleButtonWithTarget:target selector:_selector imagePrefix:@"btn_refresh"];
}

+ (UIButton *)settingsButtonWithTarget:(id)target selector:(SEL)_selector {
    return [self circleButtonWithTarget:target selector:_selector imagePrefix:@"btn_settings"];
}

+ (UIButton *)closeButtonWithTarget:(id)target selector:(SEL)_selector btnColorLight:(BOOL)btnColorLight {
    return [self circleButtonWithTarget:target selector:_selector imagePrefix:btnColorLight ? @"btn_close" : @"btn_close_dark"];
}

+ (UIButton *)closeButtonWithTarget:(id)target selector:(SEL)_selector {
    return [self closeButtonWithTarget:target selector:_selector btnColorLight:YES];
}

+ (UIButton *)writeButtonWithTarget:(id)target selector:(SEL)_selector {
    return [self circleButtonWithTarget:target selector:_selector imagePrefix:@"btn_redaktir"];
}

+ (UIButton *)searchButtonWithTarget:(id)target selector:(SEL)_selector {
    return [self circleButtonWithTarget:target selector:_selector imagePrefix:@"btn_search"];
}


#pragma mark -
#pragma mark Helper

+ (UIButton *)circleButtonWithTarget:(id)target selector:(SEL)_selector imagePrefix:(NSString *)imagePrefix {
    return [self buttonWithTarget:target selector:_selector normalImageName:[NSString stringWithFormat:@"%@_normal", imagePrefix] pressedImageName:[NSString stringWithFormat:@"%@_pressed", imagePrefix]];
}

+ (UIButton *)buttonWithTarget:(id)target selector:(SEL)_selector normalImageName:(NSString *)normalImage pressedImageName:(NSString *)pressedImage
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:target action:_selector forControlEvents:UIControlEventTouchUpInside];
    UIImage *stateImage = [UIImage imageNamed:normalImage];
    button.frame = CGRectMake(0, 0, stateImage.size.width, stateImage.size.height);
    [button setBackgroundImage:stateImage forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:pressedImage] forState:UIControlStateHighlighted];
    button.tintColor = [UIColor fromRGB:0x444444];
    
    return button;
}

@end
