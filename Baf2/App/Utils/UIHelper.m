//
// Created by Askar Mustafin on 4/10/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "Version.h"
#import "NBrowserViewController.h"
#import "NoConnectionView.h"
#import <UIActionSheet+Blocks/UIActionSheet+Blocks.h>
#import "NotificationCenterHelper.h"
#import "RegistrationStep3ViewController.h"
#import "User.h"

static NoConnectionView *noConnectionView;

@implementation UIHelper {}


+ (void)showAlertWithMessage:(NSString *)message {
    [self showAlertWithMessageTitle:nil message:message];
}

+ (void)showAlertWithMessageTitle:(NSString *)title message:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"good", nil) otherButtonTitles:nil];
    [alertView show];
}

#pragma mark UINavigationController

+ (UINavigationController *)defaultNavigationController {

    UINavigationController *navigationController = [UINavigationController new];
    //navigationController.navigationBar.barStyle = UIBarStyleBlack;
    //[UINavigationBar appearance].barStyle = UIBarStyleBlack;
    [UINavigationBar appearance].tintColor = [UIColor fromRGB:0x444444]; //bar elements
    [UINavigationBar appearance].barTintColor = [UIColor whiteColor]; //bar background
    [UINavigationBar appearance].translucent = NO;
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    return navigationController;

}

#pragma mark - Custom

+ (UIView *)successMessageWithIconWithContentWidth:(CGFloat)width message:(NSString *)message {
    return [self messageWithIcon:@"icon-success-outline" contentWidth:width message:message];
}

+(UIView *)failureMessageWithIconWithContentWidth:(CGFloat)width message:(NSString *)message {
    return [self messageWithIcon:@"icon-failure-outline" contentWidth:width message:message];
}

+(UIView *)messageWithIcon:(NSString *)iconName contentWidth:(CGFloat)width message:(NSString *)message
{
    UIView *conview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 1)];
    
    UIImageView *successIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:iconName]];
    successIcon.x = (CGFloat) ((conview.width - successIcon.width) * 0.5);
    successIcon.layer.shadowColor = [UIColor blackColor].CGColor;
    successIcon.layer.shadowRadius = 2;
    successIcon.layer.shadowOffset = CGSizeMake(0, 1);
    successIcon.layer.shadowOpacity = 0.2;
    [conview addSubview:successIcon];
    
    CGFloat messageTopSpacing = [Version iphone4:5 taller:25];
    
    UILabel *successLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, successIcon.bottom + messageTopSpacing, conview.width, 1)];
    successLabel.backgroundColor = [UIColor clearColor];
    successLabel.font = [UIFont ptSansCaption:14];
    successLabel.textAlignment = NSTextAlignmentCenter;
    successLabel.lineBreakMode = NSLineBreakByWordWrapping;
    successLabel.numberOfLines = 0;
    successLabel.textColor = [UIColor fromRGB:0x333333];
    successLabel.text = message;
    [successLabel sizeToFit];
    successLabel.x = (CGFloat) ((conview.width - successLabel.width) * 0.5);
    [conview addSubview:successLabel];
    
    conview.height = successLabel.bottom;
    
    return conview;
}

#pragma mark -
#pragma mark Getters

+(AppDelegate *)appDelegate {
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
+(UIWindow *)delegateWindow {
    return [[[UIApplication sharedApplication] delegate] window];
}

#pragma mark -
#pragma mark Alerts

+(void)showErrorAlertWithMessage:(NSString *)message
{
    if(message) {
        NSString *title = NSLocalizedString(@"errorr", nil);
        
        if([message rangeOfString:NSLocalizedString(@"error_session", nil)].location != NSNotFound) {
            title = NSLocalizedString(@"session_interrupted", nil);
        }
        [UIHelper showAlertWithMessageTitle:title message:message];
    }
}

+(void)showAlertTryAgain {
    [self showErrorAlertWithMessage:NSLocalizedString(@"_error", nil)];
}

+ (void)showAlertWithFioTreatmentAndMessage:(NSString *)message {
    [self showAlertWithMessageTitle:[NSString stringWithFormat:@"%@,", [[User sharedInstance] fio]] message:message];
}


+ (void)showAlertContractNotValidForPayment {
    [self showAlertWithMessageTitle:NSLocalizedString(@"sorry", nil) message:NSLocalizedString(@"this_service_not_available_soon", nil)];
}

+ (void)showActionSheetAuthorizationRequiredWithTarget:(id)target
{
    UIWindow *window = [UIHelper delegateWindow];
    [UIActionSheet showInView:window
                    withTitle:NSLocalizedString(@"authorization_needed", nil)
            cancelButtonTitle:NSLocalizedString(@"cancel", nil)
       destructiveButtonTitle:nil otherButtonTitles:@[NSLocalizedString(@"enter", nil), NSLocalizedString(@"registration", nil)] tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (actionSheet.cancelButtonIndex != buttonIndex) {

            void (^sendNotificationsBlock)(void) = ^void(void) {
                if (buttonIndex == 0) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGotoLogin object:nil];
                } else {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGotoRegistration object:nil];
                }
            };

            if (target) {
                //                if([target isModal]) {
                //                    [target dismissViewControllerAnimated:YES completion:^{
                //                        sendNotificationsBlock();
                //                    }];
                //                    [[self appDelegate] showTabbar:NO];
                //                }
                sendNotificationsBlock();
            } else sendNotificationsBlock();
        }
    }];
}

+(void)showOffile {
    [self showNoConnectionWithIcon:@"icon-no-connection" message:NSLocalizedString(@"connection_error", nil)];
}

#pragma mark -
#pragma mark Redirects

+(void)showEmailPhoneConfirmationWindow:(BOOL)showMessage target:(void(^)())completion {
    [UIHelper closeOpenModalWindowAnimated:NO];
    RegistrationStep3ViewController *vc = [[UIStoryboard storyboardWithName:@"Registration" bundle:nil] instantiateViewControllerWithIdentifier:@"RegistrationStep3ViewController"];
    vc.isModal = true;
    vc.didConfirm = completion;
    [self showModalViewControllerWithDefaultNavigationController:vc];
}

+ (UINavigationController *)rootNavigationController {
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    LGSideMenuController *sideMenuController = (LGSideMenuController *)[window rootViewController];
    UINavigationController *navigationController = (UINavigationController *) sideMenuController.rootViewController;
    return navigationController;
}

#pragma mark -
#pragma mark NoConnectionView

+(void)showNoConnectionWithIcon:(NSString *)iconName message:(NSString *)message
{
    if(!noConnectionView)
    {
        noConnectionView = [[NoConnectionView alloc] init];
        [noConnectionView setIcon:iconName andMessage:message];
//        [GAN sendView:@"No Connection"];
    }
    if(noConnectionView) [noConnectionView show];
}

#pragma mark -
#pragma mark Helper functions

+(void)showModalViewControllerWithDefaultNavigationController:(UIViewController *)viewController {
    UINavigationController *nc = [self defaultNavigationController];
    [nc setViewControllers:@[viewController]];
    UIViewController *rvc = [[self delegateWindow] rootViewController];
    [rvc presentViewController:nc animated:YES completion:nil];
}

+(void)closeOpenModalWindowAnimated:(BOOL)animated {
    @try {
        [[[self delegateWindow] rootViewController] dismissViewControllerAnimated:animated completion:nil];
    }
    @catch (NSException *exception) {
        //
    }
}

+(void)presentBrowserIn:(id)target withUrl:(NSString *)url {
    NBrowserViewController *v = [[NBrowserViewController alloc] initWithUrls:[NSURL URLWithString:url]];
    UINavigationController *nc = [self defaultNavigationController];
    [nc setViewControllers:@[v]];
    [target presentViewController:nc animated:YES completion:nil];
}

#pragma mark -

+ (void)callPhone:(NSString *)phone showSheetInView:(UIView *)view {
    if (phone.length > 0) {
        NSString *callAlertTitle = [NSString stringWithFormat:@"%@ %@ ?", NSLocalizedString(@"call_to_number", nil),phone];
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"tel:%@",phone]];
        
        [UIActionSheet showInView:view withTitle:callAlertTitle cancelButtonTitle:NSLocalizedString(@"dialog_no", nil) destructiveButtonTitle:nil otherButtonTitles:@[NSLocalizedString(@"dialog_yes", nil)] tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
            if (buttonIndex != actionSheet.cancelButtonIndex) {
                [[UIApplication sharedApplication] openURL:phoneUrl];
            }
        }];
    } else {
        [UIAlertView showWithTitle:nil message:NSLocalizedString(@"phone_not_exist", nil) cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
    }
}

@end