//
//  AppUtils.h
//  Myth
//
//  Created by Almas Adilbek on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#if !(WATCH_OS)
#import <UIKit/UIKit.h>
#endif

@interface AppUtils : NSObject;

+ (NSString *)appVersion;
+ (BOOL)isRetina;
+ (float)iOSVersion;
+ (NSString *)ipAddress;
+ (NSString *)deviceID;
+ (NSDictionary *)deviceInfo;
+ (NSString *)deviceInfoJsonString;

+ (BOOL)isAskar;

+ (BOOL)isFirstRun;
+ (BOOL)isRunningFromDevice;

+ (void) setUserDefaults:(id)value key:(NSString *)key;
+ (NSString *) userDefaults:(NSString *)key;

+ (BOOL)validateEmail:(NSString *)email;

// FileManager
+ (NSString *)documentsPath;
+ (NSString *)bundleFilePathWithFilename:(NSString *)filename;
+ (NSString *)libraryFilePathWithFilename:(NSString *)filename;
+ (NSString *)documentFilePathWithFilename:(NSString *)filename;
+ (BOOL)isFileExistsInLibraryDirWithFilename:(NSString *)filename;
+ (BOOL)isFileExistsInDocumentDirWithFilename:(NSString *)filename;
+ (BOOL)isFileExistsInBundleWithFilename:(NSString *)filename;

@end
