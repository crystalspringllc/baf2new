//
//  Values.h
//  BAF
//
//  Created by Almas Adilbek on 2/25/15.
//
//



@interface Values : NSObject

+ (NSInteger)banksComboboxVisibleItems;
+ (NSInteger)accountsComboboxVisibleItems;

@end
