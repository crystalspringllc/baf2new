//
//  NotificationCenterHelper.h
//  BAF
//
//  Created by Almas Adilbek on 6/18/15.
//
//

#import <Foundation/Foundation.h>

#define kNotificationUserUpdatedCard @"kNotificationUserAddedCard"
#define kNotificationContractAliasChanged @"kNotificationContractAliasChanged"
#define kNotificationUserUpdatedBeneficiaryAccount @"kNotificationUserUpdatedBeneficiaryAccount"
#define kNotificationLastOperationsFilterChanged @"kNotificationLastOperationsFilterChanged"
#define kNotificationLastOperationsUpdate @"kNotificationLastOperationsUpdate"
#define kNotificationBankCardBlocked @"kNotificationBankCardBlocked"
#define kNotificationClientAccountsConnected @"NotificationClientAccountsConnected"
#define kNotificationTransferRetailBeneficiarAdded @"kNotificationTransferRetailBeneficiarAdded"
#define kNotificationTransferCorporateBeneficiarAdded @"kNotificationTransferCorporateBeneficiarAdded"
#define kNotificationOtherBankCardAliasChanged @"kNotificationOtherBankCardAliasChanged"

#define kTabbarViewAnotherTabSelectedNotification @"tabbarViewAnotherTabSelectedNotification"

static NSString *kNotificationGotoLogin = @"notificationGotoLogin";
static NSString *kNotificationGotoRegistration = @"notificationGotoRegistration";
static NSString *kNotificationUserLoggedIn = @"notificationUserLoggedIn";
static NSString *kNotificationUserLoggedOut = @"notificationUserLoggedOut";
static NSString *kNotificationUserContractsUpdated = @"notificationUserContractsUpdated";
static NSString *kNotificationUserCommentAdded = @"notificationUserCommentAdded";
static NSString *kNotificationUserAddedPost = @"notificationUserAddedPost";
static NSString *kNotificationUserRegistered = @"notificationUserRegistered";
static NSString *kNotificationUserUpdatedProfile = @"kNotificationUserUpdatedProfile";
static NSString *kNotificationMenuOpenBecomeClient = @"notificationMenuOpenBecomeClient";
static NSString *kNotificationMenuOpenBankOffers = @"notificationMenuOpenBankOffers";
static NSString *kNotificationMenuOpenLogin = @"notificationMenuOpenLogin";
static NSString *kNotificationTransferDoneGotoHistory = @"kNotificationTransferDoneGotoHistory";
static NSString *kNotificationTransfersTemplatesUpdated = @"kNotificationTransfersTemplatesUpdated";
static NSString *kNotificationTransfersOperationAccountsUpdated = @"kNotificationTransfersOperationAccountsUpdated";
static NSString *kNotificationPaymentTemplatesUpdated = @"kNotificationPaymentTemplatesUpdated";
static NSString *kNotificationPaymentTemplatesNeedsReload = @"kNotificationPaymentTemplatesNeedsReload";
static NSString *kNotificationPaymentTemplateDeleted = @"kNotificationPaymentTemplateDeleted";
static NSString *kNotificationCardInfoChanged = @"kNotificationCardInfoChanged";
static NSString *kNotificationAccountAliasChanged = @"kNotificationAccountAliasChanged";
static NSString *kNotificationAccountBlocked = @"kNotificationAccountBlocked";
static NSString *kNotificationAccountFavouriteChanged = @"kNotificationAccountFavouriteChanged";
static NSString *kNotificationAccountLimitChanged = @"kNotificationAccountLimitChanged";
static NSString *kNotificationNewsDeleted = @"kNotificationNewsDeleted";
static NSString *kNotificationMenuOpenOnlineDeposit = @"kNotificationMenuOpenOnlineDeposit";
static NSString *kNotificationOpenAtmPoints = @"kNotificationOpenAtmPoints";
static NSString *kNotificationOpenNewTransfers = @"kNotificationOpenNewTransfer";
static NSString *kNotificationOpenPayments = @"kNotificationOpenPayments";
// Watch App
static NSString *kNotificationSendPinToken = @"kNotificationSendPinToken";
static NSString *kNotificationLanguageDidChange = @"kNotificationLanguageDidChanged";

@interface NotificationCenterHelper : NSObject

@end
