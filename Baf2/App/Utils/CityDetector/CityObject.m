//
//  CityObject.m
//  BAF
//
//  Created by Almas Adilbek on 7/9/15.
//
//

#import "CityObject.h"

@implementation CityObject

+ (CityObject *)city:(NSNumber *)cityId name:(NSString *)name lat:(double)lat lon:(double)lon
{
    CityObject *city = [[CityObject alloc] init];
    city.cityId = cityId;
    city.name = name;
    city.lat = lat;
    city.lon = lon;
    return city;
}

- (CLLocationCoordinate2D)coordinate {
    return CLLocationCoordinate2DMake(self.lat, self.lon);
}


@end
