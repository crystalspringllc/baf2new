//
//  KzCityDetector.h
//  BAF
//
//  Created by Almas Adilbek on 7/9/15.
//
//

#import <CoreLocation/CoreLocation.h>

@class CityObject;

@interface KzCityDetector : NSObject

+ (NSArray *)cities;

+ (CityObject *)cityOfLocation:(CLLocation *)location;

@end
