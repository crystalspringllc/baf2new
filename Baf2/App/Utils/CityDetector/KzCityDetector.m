//
//  KzCityDetector.m
//  BAF
//
//  Created by Almas Adilbek on 7/9/15.
//
//

#import "KzCityDetector.h"
#import "CityObject.h"
#import "FMDatabase.h"

@implementation KzCityDetector

+ (NSArray *)cities
{
    NSMutableArray * cities = [NSMutableArray array];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"t_cities" ofType:@"sqlite" inDirectory:@"assets"];
    FMDatabase *db = [FMDatabase databaseWithPath:path];
    
    if ([db open]) {
        NSString *query = @"SELECT city_id, name, city_lat, city_lng FROM t_city_coord";
        FMResultSet *s = [db executeQuery:query];
        NSLog(@"result: %@", s);
        while ([s next]) {
            CityObject *city = [CityObject new];
            city.cityId = @([s intForColumn:@"city_id"]);
            city.name = [s stringForColumn:@"name"];
            city.lat = [s doubleForColumn:@"city_lat"];
            city.lon = [s doubleForColumn:@"city_lng"];
            [cities addObject:city];
        }
        
        [db close];
    }
    
    return cities;
}

+ (CityObject *)cityOfLocation:(CLLocation *)location
{
    CityObject *foundCity = nil;

    CGFloat minDistance = MAXFLOAT;
    for(CityObject *city in [self cities]) {
        CGFloat distance = [self metersBetween:CLLocationCoordinate2DMake(city.lat, city.lon) and:location.coordinate];
        if(distance < minDistance) {
            minDistance = distance;
            foundCity = city;
        }
    }

    return foundCity;
}

#pragma mark -
#pragma mark Helper

+(long)metersBetween:(CLLocationCoordinate2D)place1 and:(CLLocationCoordinate2D)place2
{
    // Haversine formula
    int R = 6371;  // earth's mean radius in km
    double dLat = [self toRadian:(place2.latitude - place1.latitude)];
    double dLon = [self toRadian:(place2.longitude - place1.longitude)];

    double a = pow(sin(dLat / 2), 2) + cos([self toRadian:place1.latitude]) * cos([self toRadian:place2.latitude]) * pow(sin(dLon / 2), 2);
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));
    double d = R * c;

    return (long)floor(d * 1000);
}

+(double)toRadian:(CLLocationDegrees)value {
    return M_PI * value / 180;
}

@end
