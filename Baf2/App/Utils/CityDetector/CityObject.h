//
//  CityObject.h
//  BAF
//
//  Created by Almas Adilbek on 7/9/15.
//
//

#import <CoreLocation/CoreLocation.h>

@interface CityObject : NSObject

@property(nonatomic, copy) NSNumber *cityId;
@property(nonatomic, copy) NSString *name;
@property(nonatomic, assign) double lat;
@property(nonatomic, assign) double lon;

+ (CityObject *)city:(NSNumber *)cityId name:(NSString *)name lat:(double)lat lon:(double)lon;
- (CLLocationCoordinate2D)coordinate;

@end
