//
//  PinHelper.m
//  ACB
//
//  Created by Almas Adilbek on 6/1/15.
//
//

#import "PinHelper.h"
#import "NSUserDefaults+sharedDefaults.h"
#import <UICKeyChainStore.h>

@implementation PinHelper

+ (BOOL)savePinToken:(NSString *)pinToken withPin:(NSString*)pinCode {
    [[NSUserDefaults shared] setObject:pinToken forKey:@"pinToken"];
    [[NSUserDefaults shared] synchronize];
    [[self sharedKeychain] setString:pinCode forKey:@"pinCode"];
    
    return true;
}

+ (NSString *)pinToken {
    return [[NSUserDefaults shared] objectForKey:@"pinToken"];
}

+ (NSString *)pinCode {
    return [[self sharedKeychain] stringForKey:@"pinCode"];
}

+ (void)deletePinToken {
    [[NSUserDefaults shared] removeObjectForKey:@"pinToken"];
    [[NSUserDefaults shared] synchronize];
    [[self sharedKeychain] removeItemForKey:@"pinCode"];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUserUseTouchID];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUseTouchIDAsked];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (UICKeyChainStore *)sharedKeychain {
    return [UICKeyChainStore keyChainStoreWithService:@"baf.git" accessGroup:@"group.kz.crystalspring.baf"];
}

@end