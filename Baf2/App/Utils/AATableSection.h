//
//  AATableSection.h
//  BAF
//
//  Created by Almas Adilbek on 1/22/15.
//
//



@interface AATableSection : NSObject

-(id)initWithSectionId:(NSString *)sectionId_;

@property(nonatomic, copy) NSString *sectionId;
@property(nonatomic, copy) NSString *headerTitle;
@property(nonatomic, copy) NSString *footerTitle;
@property(nonatomic, strong) NSMutableArray *rows;
@property(nonatomic, strong) NSMutableArray *rows2;

- (void)addRow:(id)object;

@end
