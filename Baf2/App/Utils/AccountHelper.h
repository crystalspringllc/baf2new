//
//  AccountHelper.h
//  BAF
//
//  Created by Almas Adilbek on 3/10/15.
//
//

#import <Foundation/Foundation.h>

@interface AccountHelper : NSObject

+ (NSString *)accountCurrency:(id)account;
+ (NSString *)accountIdentifierAlias:(id)account;
+ (NSNumber *)accountId:(id)account;
+ (NSNumber *)getIdOfAccount:(id)account;
+ (BOOL)isCurrentAccount:(id)account;

@end
