//
//  Values.m
//  BAF
//
//  Created by Almas Adilbek on 2/25/15.
//
//

#import "Values.h"
#import "Version.h"

@implementation Values

+ (NSInteger)banksComboboxVisibleItems {
    return (NSInteger) [Version iphone4:7 iphone5:9 iphone6:11 iphone6Plus:13 ipad:20];
}

+ (NSInteger)accountsComboboxVisibleItems {
    return [self banksComboboxVisibleItems];
}


@end
