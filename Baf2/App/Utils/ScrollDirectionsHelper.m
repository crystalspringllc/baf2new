//
//  ScrollDirectionsHelper.m
//  KZNews
//
//  Created by Almas Adilbek on 8/8/15.
//  Copyright (c) 2015 Almas Adilbek. All rights reserved.
//

#import "ScrollDirectionsHelper.h"
#import "ASize.h"
#import "NSLayoutConstraint+PureLayout.h"
#import "UIView+ConcisePureLayout.h"
#import <FrameAccessor/FrameAccessor.h>

typedef NS_ENUM(NSInteger , ScrollDirectionPinType) {
    ScrollDirectionPinTypeTop = 0,
    ScrollDirectionPinTypeBottom
};

@implementation ScrollDirectionsHelper
{
    BOOL animating;

    NSLayoutConstraint *topConstraint;
    NSLayoutConstraint *bottomConstraint;

    CGFloat bottomOffset;
    CGFloat topOffset;

    ScrollDirectionPinType _pinType;
}

- (id)init {
    self = [super init];
    if(self) {
        self.lastContentOffset = 0;
        self.triggerOffset = 60;
        self.scrollDuration = 0.25;
    }
    return self;
}

- (void)scrollView:(UIView *)view pinTop:(CGFloat)offset scrollView:(UIScrollView *)scrollView
{
    ScrollDirection scrollDirection;
    CGFloat contentOffsetY = scrollView.contentOffsetY;
    if (_lastContentOffset < contentOffsetY) {
        scrollDirection = ScrollDirectionUp;
    } else {
        scrollDirection = ScrollDirectionDown;
    }

    if(scrollDirection == ScrollDirectionDown)
    {
        if(view.y < offset) {
            view.y += ABS(contentOffsetY - _lastContentOffset);
            if(view.y > offset) view.y = offset;
        }
    }
    else if(scrollDirection == ScrollDirectionUp && contentOffsetY > self.triggerOffset)
    {
        CGFloat hiddenY = offset - view.frame.size.height;
        if(view.y > hiddenY) {
            view.y -= ABS(_lastContentOffset - contentOffsetY) * 1.1;
            if(view.y < hiddenY) view.y = hiddenY;
        }
    }

    self.lastContentOffset = contentOffsetY;
}

- (void)scrollViewAnimated:(UIView *)view pinTop:(CGFloat)offset scrollView:(UIScrollView *)scrollView
{
    ScrollDirection scrollDirection;
    CGFloat contentOffsetY = scrollView.contentOffsetY;
    if (_lastContentOffset < contentOffsetY) {
        scrollDirection = ScrollDirectionUp;
    } else {
        scrollDirection = ScrollDirectionDown;
    }

    CGFloat y = 0;
    if(scrollDirection == ScrollDirectionDown)
    {
        y = offset;
    }
    else if(scrollDirection == ScrollDirectionUp && contentOffsetY > self.triggerOffset)
    {
        y = offset - view.frame.size.height;
    }

    [self animateView:view y:y];

    self.lastContentOffset = contentOffsetY;
}

- (void)showViewAnimated:(UIView *)view topOffset:(CGFloat)offset {
    if(view.frame.origin.y != offset) {
        [self animateView:view y:offset];
    }
}

- (void)scrollView:(UIView *)view viewTopY:(CGFloat)viewTopY scrollView:(UIScrollView *)scrollView
{
    ScrollDirection scrollDirection;
    CGFloat contentOffsetY = scrollView.contentOffsetY;
    if (_lastContentOffset < contentOffsetY) {
        scrollDirection = ScrollDirectionUp;
    } else {
        scrollDirection = ScrollDirectionDown;
    }

    if(scrollDirection == ScrollDirectionDown && contentOffsetY < (scrollView.contentSize.height - [ASize screenHeight]))
    {
        CGFloat appearedY = viewTopY;
        if(view.y > appearedY) {
            view.y -= ABS(_lastContentOffset - contentOffsetY);
            if(view.y < appearedY) view.y = appearedY;
        }
    }
    else if(scrollDirection == ScrollDirectionUp && contentOffsetY > 0)
    {
        if(view.y < [ASize screenHeight]) {
            view.y += ABS(_lastContentOffset - contentOffsetY);
            if(view.y > [ASize screenHeight]) view.y = [ASize screenHeight];
        }
    }

    self.lastContentOffset = contentOffsetY;
}

- (void)scrollAutolayoutView:(UIView *)view scrollView:(UIScrollView *)scrollView
{
    if(animating) return;

    ScrollDirection scrollDirection;
    CGFloat contentOffsetY = scrollView.contentOffsetY;
    if (_lastContentOffset < contentOffsetY) {
        scrollDirection = ScrollDirectionUp;
    } else {
        scrollDirection = ScrollDirectionDown;
    }

    if(scrollDirection == ScrollDirectionUp)
    {
        if(_pinType == ScrollDirectionPinTypeBottom)
        {
            if(!topConstraint && scrollView.contentOffsetY > self.triggerOffset)
            {
                if(bottomConstraint) {
                    [bottomConstraint autoRemove];
                    bottomConstraint = nil;
                }

                topConstraint = [view aa_superviewTop:[ASize screenHeight]];
                [self animateAutolayoutView:view];
            }
        }
        else
        {
            if(scrollView.contentOffsetY > self.triggerOffset) {
                topConstraint.constant = -view.height;
                [self animateAutolayoutView:view];
            }
        }
    }
    else
    {
        [self showAutolayoutView:view];
    }

    self.lastContentOffset = contentOffsetY;
}

- (void)setBottomConstraint:(UIView *)view offset:(CGFloat)offset
{
    _pinType = ScrollDirectionPinTypeBottom;

    if(bottomConstraint) {
        [bottomConstraint autoRemove];
    }
    bottomOffset = offset;
    bottomConstraint = [view aa_superviewBottom:offset];
}

- (void)setTopConstraint:(UIView *)view offset:(CGFloat)offset
{
    _pinType = ScrollDirectionPinTypeTop;

    if(topConstraint) {
        [topConstraint autoRemove];
    }
    topOffset = offset;
    topConstraint = [view aa_superviewTop:offset];
}

- (void)showAutolayoutView:(UIView *)view
{
    if(_pinType == ScrollDirectionPinTypeBottom)
    {
        if(!bottomConstraint) {
            if(topConstraint) {
                [topConstraint autoRemove];
                topConstraint = nil;
            }

            bottomConstraint = [view aa_superviewBottom:bottomOffset];
            [self animateAutolayoutView:view];
        }
    }
    else
    {
        topConstraint.constant = topOffset;
        [self animateAutolayoutView:view];
    }
}

- (void)animateAutolayoutView:(UIView *)view {
    animating = YES;
    [UIView animateWithDuration:self.scrollDuration animations:^{
        [view layoutIfNeeded];
    } completion:^(BOOL finished) {
        animating = NO;
    }];
}

- (void)animateView:(UIView *)view y:(CGFloat)y {
    animating = YES;
    [UIView animateWithDuration:self.scrollDuration animations:^{
        view.y = y;
    } completion:^(BOOL finished) {
        animating = NO;
    }];
}

@end
