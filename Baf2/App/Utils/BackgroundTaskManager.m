//
//  BackgroundTaskManager.m
//  BAF
//
//  Created by Almas Adilbek on 10/30/13.
//
//

#import "BackgroundTaskManager.h"

static BackgroundTaskManager *_shared = nil;

@implementation BackgroundTaskManager {
    BackgroundTaskManagerTaskExpirationHandler expirationHandlerBlock;
}

@synthesize backgroundTask;

+(BackgroundTaskManager *)shared {
    if(!_shared) {
        _shared = [[[self class] alloc] init];
        _shared.backgroundTask = UIBackgroundTaskInvalid;
    }
    return _shared;
}

-(void)startTaskWithExpiration:(BackgroundTaskManagerTaskExpirationHandler)block {
    expirationHandlerBlock = block;
    
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        if(expirationHandlerBlock) expirationHandlerBlock();
    }];
}

-(void)stopTask {
    BackgroundTaskManager *m = [[self class] shared];
    if(m.backgroundTask != UIBackgroundTaskInvalid) {
        [[UIApplication sharedApplication] endBackgroundTask:m.backgroundTask];
        m.backgroundTask = UIBackgroundTaskInvalid;
    }
    expirationHandlerBlock = nil;
}

+(void)kill {
    [[self shared] stopTask];
    _shared = nil;
}

@end
