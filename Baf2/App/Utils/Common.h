//
//  Common.h
//  Kurs
//
//  Created by Mustafin Askar on 17.06.13.
//  Copyright (c) 2013 askar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Common : NSObject

+ (NSString *)getPathOfNeededFile:(NSString *)file withType:(NSString *)type;

@end
