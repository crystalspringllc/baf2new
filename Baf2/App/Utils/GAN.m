//
//  GAN.m
//  PointPlus
//
//  Created by Almas Adilbek on 9/19/12.
//
//

#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

@implementation GAN

+ (NSString *)kAnalyticsAccountId {
    return @"UA-49930121-2";
}

+ (void)start {
    [GAI sharedInstance].dispatchInterval = 20;
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [[GAI sharedInstance] trackerWithTrackingId:[self kAnalyticsAccountId]];
}
+ (void)stop {

}

+ (void)sendView:(NSString *)pageName {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:pageName];
    [tracker send:[[GAIDictionaryBuilder createScreenView]  build]];
}

#pragma mark -

+ (void)sendEventWithCategory:(NSString *)category action:(NSString *)action {
    [self sendEventWithCategory:category action:action label:@""];
}

+ (void)sendEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label {
    [self sendEventWithCategory:category action:action label:label value:0];
}
+(void)sendEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label value:(int)value {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category                                   // Event category (required)
                                                          action:action                                     // Event action (required)
                                                           label:label                                      // Event label
                                                           value:@(value)] build]];   // Event value
}

+(void)sendException:(NSString *)description {
    [self sendException:description withFatal:nil];
}

+(void)sendException:(NSString *)description withFatal:(NSNumber *)fatal {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createExceptionWithDescription:description withFatal:fatal]  build]];
}

@end
