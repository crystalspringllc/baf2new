//
//  Page.h
//  Myth
//
//  Created by Almas Adilbek on 12/24/12.
//
//

#import "ModelObject.h"

@interface Page : ModelObject {
    NSUInteger pageID;
    NSString *name;
    NSString *logoUrl;
    NSUInteger likes;
    BOOL meLiked;
}

@property (nonatomic, assign) NSUInteger pageID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *logoUrl;
@property (nonatomic, assign) NSUInteger likes;
@property (nonatomic, assign) BOOL meLiked;

-(id)initWithData:(NSDictionary *)data;
+(NSArray *)arrayOfPages:(NSArray *)pagesArray;

@end
