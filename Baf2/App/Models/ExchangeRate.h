#import "ModelObject.h"


@interface ExchangeRate : ModelObject {

}

@property (nonatomic, copy) NSNumber *buy;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, copy) NSNumber *currencyCode;
@property (nonatomic, copy) NSNumber *sell;
@property (nonatomic, copy) NSString *updated;

+ (ExchangeRate *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)getSellDecimal;
- (NSString *)getBuyDecimal;

@end
