#import "City.h"

@implementation City

@synthesize code;
@synthesize cityId;
@synthesize name;

+ (City *)instanceFromDictionary:(NSDictionary *)aDictionary {

    City *instance = [[City alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    objectData = aDictionary;

    self.code = [self stringValueForKey:@"code"];
    self.cityId = [self stringValueForKey:@"id"];
    self.name = [self stringValueForKey:@"name"];

    objectData = nil;
}

@end