#import "ModelObject.h"


@interface Captcha : ModelObject {

    NSString *bitmap;
    NSNumber *code;
    NSString *sessionKey;

}

@property (nonatomic, copy) NSString *bitmap;
@property (nonatomic, copy) NSNumber *code;
@property (nonatomic, copy) NSString *sessionKey;

+ (Captcha *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
