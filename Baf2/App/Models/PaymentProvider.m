//
//  PaymentProvider.m
//  
//
//  Created by Almas Adilbek on 10/16/13.
//  Copyright (c) 2013. All rights reserved.
//

#import "PaymentProvider.h"
#import "Constants.h"

@implementation PaymentProvider

+ (PaymentProvider *)instanceFromDictionary:(NSDictionary *)aDictionary {

    PaymentProvider *instance = [[PaymentProvider alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;

    self.paymentProviderId = [self numberValueForKey:@"id"];
    self.name = [self stringValueForKey:@"name"];
    self.providerCode = [self stringValueForKey:@"providerCode"];
    self.info = [self stringValueForKey:@"info"];
    self.contractRegExp = [self stringValueForKey:@"contractRegExp"];
    self.contractMask = [self stringValueForKey:@"contractMask"];
    self.infoRequestType = [self numberValueForKey:@"infoRequestType"];
    
    self.contractFormat = [self stringValueForKey:@"contractFormat"];
    self.contractName = [self stringValueForKey:@"contractName"];
    self.disabled = [self boolValueForKey:@"disabled"];
    self.logo = [self stringValueForKey:@"logo"];
    
    self.hasDisposableContracts = [self boolValueForKey:@"hasDisposableContracts"];
    self.serviceId = [self numberValueForKey:@"serviceId"];
    self.longProcessingTime = [self boolValueForKey:@"longProcessingTime"];
    self.categoryProviderName = [self stringValueForKey:@"categoryProviderName"];
    
    objectData = nil;
}

-(BOOL)isProviderAvailableForPayment {
    return !self.disabled;
//    return (
//            [self isInfoRequestTypeEquialTo:1] ||
//            [self isInfoRequestTypeEquialTo:3] ||
//            [self isInfoRequestTypeEquialTo:10] ||
//            [self isInfoRequestTypeEquialTo:4]
//            ) &&
}

-(BOOL)isInfoRequestTypeEquialTo:(int)number {
    return [self.infoRequestType intValue] == number;
}

- (NSString *)logo {
    if (!_logo) {
        return _logo;
    }
    
    return [NSString stringWithFormat:@"%@%@", PROVIDER_IMAGE_URL, _logo.lowercaseString];
}

#pragma mark NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.paymentProviderId = [decoder decodeObjectForKey:@"paymentProviderId"];
    self.name = [decoder decodeObjectForKey:@"name"];
    self.providerCode = [decoder decodeObjectForKey:@"providerCode"];
    self.info = [decoder decodeObjectForKey:@"info"];
    self.contractRegExp = [decoder decodeObjectForKey:@"contractRegExp"];
    self.contractMask = [decoder decodeObjectForKey:@"contractMask"];
    self.infoRequestType = [decoder decodeObjectForKey:@"infoRequestType"];
    
    self.contractFormat = [decoder decodeObjectForKey:@"contractFormat"];
    self.contractName = [decoder decodeObjectForKey:@"contractName"];
    self.disabled = [decoder decodeBoolForKey:@"disabled"];
    self.logo = [decoder decodeObjectForKey:@"logo"];
    
    self.hasDisposableContracts = [decoder decodeBoolForKey:@"hasDisposableContracts"];
    self.serviceId = [decoder decodeObjectForKey:@"serviceId"];
    self.longProcessingTime = [decoder decodeBoolForKey:@"longProcessingTime"];
    self.categoryProviderName = [decoder decodeObjectForKey:@"categoryProviderName"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.paymentProviderId forKey:@"paymentProviderId"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.providerCode forKey:@"providerCode"];
    [encoder encodeObject:self.info forKey:@"info"];
    [encoder encodeObject:self.contractRegExp forKey:@"contractRegExp"];
    [encoder encodeObject:self.contractMask forKey:@"contractMask"];
    [encoder encodeObject:self.infoRequestType forKey:@"infoRequestType"];
    
    [encoder encodeObject:self.contractFormat forKey:@"contractFormat"];
    [encoder encodeObject:self.contractName forKey:@"contractName"];
    [encoder encodeBool:self.disabled forKey:@"disabled"];
    [encoder encodeObject:self.logo forKey:@"logo"];
    
    [encoder encodeBool:self.hasDisposableContracts forKey:@"hasDisposableContracts"];
    [encoder encodeObject:self.serviceId forKey:@"serviceId"];
    [encoder encodeBool:self.longProcessingTime forKey:@"longProcessingTime"];
    [encoder encodeObject:self.categoryProviderName forKey:@"categoryProviderName"];
}

- (InfoRequestType)getInfoRequestTypeEnum {
    return (InfoRequestType) [self.infoRequestType intValue];
}

@end
