#import "ModelObject.h"


@interface TcInfo : ModelObject {

}

@property (nonatomic, copy) NSString *companyname;
@property (nonatomic, copy) NSString *dateout;
@property (nonatomic, copy) NSString *fio;
@property (nonatomic, copy) NSString *idn;
@property (nonatomic, copy) NSString *isconstant;
@property (nonatomic, copy) NSString *isinactive;
@property (nonatomic, assign) BOOL isjuridical;
@property (nonatomic, assign) BOOL isResident;
@property (nonatomic, copy) NSString *priznakfl;
@property (nonatomic, copy) NSString *reasonout;
@property (nonatomic, copy) NSString *rnn;
@property (nonatomic, copy) NSString *taxcode;

+ (TcInfo *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (BOOL)isCorporate;
- (NSString *)firstname;
- (NSString *)surname;

@end
