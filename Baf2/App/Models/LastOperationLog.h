#import "ModelObject.h"


@interface LastOperationLog : ModelObject {

}

@property (nonatomic, copy) NSNumber *amount;
@property (nonatomic, copy) NSString *cardType;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *operationDate;
@property (nonatomic, copy) NSString *operationError;
@property (nonatomic, copy) NSString *operationSubtitle;
@property (nonatomic, copy) NSString *operationSumm;
@property (nonatomic, copy) NSString *operationTitle;
@property (nonatomic, copy) NSString *operationUrl;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *text2;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *typeDesc;
@property (nonatomic, copy) NSString *v;

+ (LastOperationLog *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
