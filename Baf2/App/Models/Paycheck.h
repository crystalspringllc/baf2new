#import "ModelObject.h"

@class Contract, PaymentProvider;

@interface Paycheck : ModelObject {

}

@property (nonatomic, copy) NSString *acquiring;
@property (nonatomic, copy) NSString *amount;
@property (nonatomic, copy) NSString *amounWithtCommission;
@property (nonatomic, copy) NSNumber *approvalCode;
@property (nonatomic, copy) NSString *cardNum;
@property (nonatomic, copy) NSString *bank;
@property (nonatomic, strong) Contract *contract;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *order;
@property (nonatomic, copy) NSString *ref;

@property (nonatomic, copy) NSNumber *bonusAmount;
@property (nonatomic, copy) NSString *cardNumber;
@property (nonatomic, copy) NSString *cardType;
@property (nonatomic) BOOL hasError;
@property (nonatomic, copy) NSString *logstate;
@property (nonatomic, copy) NSNumber *orderId;
@property (nonatomic, strong) PaymentProvider *provider;
@property (nonatomic, copy) NSNumber *providerId;
@property (nonatomic, copy) NSNumber *reference;
@property (nonatomic, copy) NSNumber *terminal;

+ (Paycheck *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)normalDate;

@end
