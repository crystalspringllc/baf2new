//
//  SecretQuestion.m
//  Baf2
//
//  Created by Shyngys Kassymov on 10.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "SecretQuestion.h"

@implementation SecretQuestion

+ (SecretQuestion *)instanceFromDictionary:(NSDictionary *)aDictionary {
    SecretQuestion *instance = [[SecretQuestion alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.secretQuestionId = [self numberValueForKey:@"id"];
    self.value = [self stringValueForKey:@"value"];
    self.example = [self stringValueForKey:@"example"];
    
    objectData = nil;
}

@end
