//
//  SecretQuestion.h
//  Baf2
//
//  Created by Shyngys Kassymov on 10.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface SecretQuestion : ModelObject

@property (nonatomic, strong) NSNumber *secretQuestionId;
@property (nonatomic, strong) NSString *value;
@property (nonatomic, strong) NSString *example;

+ (SecretQuestion *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
