#import "CardProduct.h"

#import "CardCity.h"

@implementation CardProduct

@synthesize amount;
@synthesize category;
@synthesize cities;
@synthesize code;
@synthesize descriptionText;
@synthesize logo;
@synthesize name;
@synthesize cardProductId;

+ (CardProduct *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CardProduct *instance = [[CardProduct alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    [self setValuesForKeysWithDictionary:aDictionary];

}

- (void)setValue:(id)value forKey:(NSString *)key {

    if ([key isEqualToString:@"cities"]) {

        if ([value isKindOfClass:[NSArray class]]) {

            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                CardCity *populatedMember = [CardCity instanceFromDictionary:valueMember];
                [myMembers addObject:populatedMember];
            }

            self.cities = myMembers;

        }

    } else {
        [super setValue:value forKey:key];
    }

}


- (void)setValue:(id)value forUndefinedKey:(NSString *)key {

    if ([key isEqualToString:@"description"]) {
        [self setValue:value forKey:@"descriptionText"];
    } else if ([key isEqualToString:@"id"]) {
        [self setValue:value forKey:@"cardProductId"];
    } else {
        [super setValue:value forUndefinedKey:key];
    }

}



@end
