//
//  NewPaymentOperationDetail.m
//  Baf2
//
//  Created by developer on 13.12.17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "NewPaymentOperationDetail.h"

@implementation NewPaymentOperationDetail

@synthesize alias, amountBonus, amountCard, amountContract, amountWithCommission, card, cardBin, commission, contract, contractId, dtcreated, lastOperationDetailsId, infoRequestType, invoiceId, logstate, processingCode, providerCode, providerId, providerLogo, providerName, reason, rrn, serviceId, terminal;

+ (NewPaymentOperationDetail *)instanceFromDictionary:(NSDictionary *)aDictionary {
    
    NewPaymentOperationDetail *instance = [[NewPaymentOperationDetail alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
    
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.alias = [self stringValueForKey:@"alias"];
    self.amountBonus = [self stringValueForKey:@"amount_bonus"];
    self.amountCard = [self stringValueForKey:@"amount_card"];
    self.amountContract = [self stringValueForKey:@"amount_contract"];
    self.amountWithCommission = [self stringValueForKey:@"amount_with_commission"];
    self.card = [self stringValueForKey:@"card"];
    self.cardBin = [self stringValueForKey:@"card_bin"];
    self.commission = [self stringValueForKey:@"commission"];
    self.contract = [self stringValueForKey:@"contract"];
    self.contractId = [self stringValueForKey:@"contract_id"];
    self.dtcreated = [self stringValueForKey:@"dtcreated"];
    self.lastOperationDetailsId = [self stringValueForKey:@"id"];
    self.infoRequestType = [self stringValueForKey:@"info_request_type"];
    self.invoiceId = [self stringValueForKey:@"invoice_id"];
    self.logstate = [self stringValueForKey:@"logstate"];
    self.processingCode = [self stringValueForKey:@"processing_code"];
    self.providerCode = [self stringValueForKey:@"provider_code"];
    self.providerId = [self stringValueForKey:@"provider_id"];
    self.providerLogo = [self stringValueForKey:@"provider_logo"];
    self.providerName = [self stringValueForKey:@"provider_name"];
    self.providerUrl = [self stringValueForKey:@"provider_url"];
    self.reason = [self stringValueForKey:@"reason"];
    self.rrn = [self stringValueForKey:@"rrn"];
    self.serviceId = [self stringValueForKey:@"service_id"];
    self.terminal = [self stringValueForKey:@"terminal"];
    self.statusCode = [self stringValueForKey:@"status_code"];
    self.statusText = [self stringValueForKey:@"status_text"];
    
    if([self.commission isEqual:@""]) self.commission = @"0.00";
    self.dtcreated = [self.dtcreated stringByReplacingOccurrencesOfString:@".0" withString:@""];
    
    // Transfer info
  
    objectData = nil;
}


@end
