//
//  PaymentProvidersByCategory.h
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@class ProviderCategory;

@interface PaymentProvidersByCategory : ModelObject <NSCoding>

@property (nonatomic, strong) NSArray *paymentProviderListWS;
@property (nonatomic, strong) ProviderCategory *category;

+ (PaymentProvidersByCategory *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
