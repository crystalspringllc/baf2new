//
// Created by Askar Mustafin on 7/20/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "FromExternalToAccountButtonCheckResponse.h"
#import "TransferCurrencyRates.h"
#import "PaymentProvider.h"
#import "Account.h"


@implementation FromExternalToAccountButtonCheckResponse {}

+ (FromExternalToAccountButtonCheckResponse *)instanceFromDictionary:(NSDictionary *)aDictionary {
    FromExternalToAccountButtonCheckResponse *instance = [[FromExternalToAccountButtonCheckResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.direction = [self boolValueForKey:@"direction"];
    self.isValid = [self boolValueForKey:@"isValid"];

    self.type = [self stringValueForKey:@"type"];
    self.session = [self stringValueForKey:@"session"];
    self.requestorAmount = [self numberValueForKey:@"requestorAmount"];
    self.requestorCurrency = [self stringValueForKey:@"requestorCurrency"];
    self.destinationId = [self numberValueForKey:@"destinationId"];
    self.destinationType = [self stringValueForKey:@"destinationType"];
    self.destinationAmount = [self numberValueForKey:@"destinationAmount"];
    self.destinationCurrency = [self stringValueForKey:@"destinationCurrency"];
    self.amount = [self numberValueForKey:@"amount"];
    self.currency = [self stringValueForKey:@"currency"];
    self.comission = [self numberValueForKey:@"comission"];
    self.comissionCurrency = [self stringValueForKey:@"comissionCurrency"];
    self.safetyLevel = [self stringValueForKey:@"safetyLevel"];
    self.operdate = [self stringValueForKey:@"operdate"];
    self.clientId = [self numberValueForKey:@"clientId"];

    self.destinationAccount = [Account instanceFromDictionary:objectData[@"destinationAccount"]];
    self.provider = [PaymentProvider instanceFromDictionary:objectData[@"provider"]];
    self.exchange = [TransferCurrencyRates instanceFromDictionary:objectData[@"exchange"]];
    self.warningMessages = [self arrayValueForKey:@"warningMessages"];
    self.knpList = objectData[@"knpList"];

    //when init paybox transfer
    self.servicelogId = [self numberValueForKey:@"servicelogId"];
    self.uuid = [self stringValueForKey:@"uuid"];
    self.isPinVerifyRequired = [self boolValueForKey:@"isPinVerifyRequired"];
    self.alias = [self stringValueForKey:@"alias"];
    self.processLink = [self stringValueForKey:@"processLink"];
    self.backLink = [self stringValueForKey:@"backLink"];

    objectData = nil;
}

@end
