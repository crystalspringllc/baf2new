//
//  EconomicSector.m
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "EconomicSector.h"

@implementation EconomicSector

+ (EconomicSector *)instanceFromDictionary:(NSDictionary *)aDictionary {
    EconomicSector *instance = [[EconomicSector alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.code = [self stringValueForKey:@"code"];
    self.economicSectorDescription = [self stringValueForKey:@"description"];
    
    objectData = nil;
}


@end
