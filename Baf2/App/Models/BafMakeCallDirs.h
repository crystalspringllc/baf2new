//
//  BafMakeCallDirs.h
//  BAF
//
//  Created by Almas Adilbek on 11/1/14.
//
//



#import "ModelObject.h"

@interface BafMakeCallDirs : ModelObject

@property(nonatomic, strong) NSArray *topics;
@property(nonatomic, strong) NSArray *cities;

+ (BafMakeCallDirs *)instanceWithResponse:(id)response;

@end
