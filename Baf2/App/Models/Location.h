//
//  Location.h
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface Location : ModelObject

@property (nonatomic, strong) NSString *locationId;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *name;

+ (Location *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
