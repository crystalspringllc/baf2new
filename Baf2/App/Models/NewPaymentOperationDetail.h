//
//  NewPaymentOperationDetail.h
//  Baf2
//
//  Created by developer on 13.12.17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface NewPaymentOperationDetail : ModelObject

@property (nonatomic, copy) NSString *alias;
@property (nonatomic, copy) NSString *amountBonus;
@property (nonatomic, copy) NSString *amountCard;
@property (nonatomic, copy) NSString *amountContract;
@property (nonatomic, copy) NSString *amountWithCommission;
@property (nonatomic, copy) NSString *card;
@property (nonatomic, copy) NSString *cardBin;
@property (nonatomic, copy) NSString *commission;
@property (nonatomic, copy) NSString *contract;
@property (nonatomic, copy) NSString *contractId;
@property (nonatomic, copy) NSString *dtcreated;
@property (nonatomic, copy) NSString *lastOperationDetailsId;
@property (nonatomic, copy) NSString *infoRequestType;
@property (nonatomic, copy) NSString *invoiceId;
@property (nonatomic, copy) NSString *logstate;
@property (nonatomic, copy) NSString *processingCode;
@property (nonatomic, copy) NSString *providerCode;
@property (nonatomic, copy) NSString *providerId;
@property (nonatomic, copy) NSString *providerLogo;
@property (nonatomic, copy) NSString *providerName;
@property (nonatomic, copy) NSString *providerUrl;
@property (nonatomic, copy) NSString *reason;
@property (nonatomic, copy) NSString *rrn;
@property (nonatomic, copy) NSString *serviceId;
@property (nonatomic, copy) NSString *terminal;
@property (nonatomic, copy) NSString *statusCode;
@property (nonatomic, copy) NSString *statusText;

//@property(nonatomic, strong) OperationTransferInfo *transferInfo;

//+ (LastOperationDetails *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)dateWithTimeString;

- (BOOL)isSuccess;
- (BOOL)isError;
- (BOOL)isRejected;
- (BOOL)isSentToPS;

- (NSString *)transferCommissionWithCurrency;

@end
