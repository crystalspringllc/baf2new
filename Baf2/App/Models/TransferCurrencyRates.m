#import "TransferCurrencyRates.h"
#import "NSString+Ext.h"
#import "NSDate+Ext.h"
#import "NSNumber+Ext.h"

@implementation TransferCurrencyRates

+ (TransferCurrencyRates *)instanceFromDictionary:(NSDictionary *)aDictionary {

    TransferCurrencyRates *instance = [[TransferCurrencyRates alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.updated = [self stringValueForKey:@"updated"];
    self.convertedAmount = [self numberValueForKey:@"amount"];
    self.rate = [self numberValueForKey:@"rate"];
    self.presentation = [self stringValueForKey:@"description"];
    self.fromCurrency = [self stringValueForKey:@"fromCurrency"];
    self.fromSell = [self numberValueForKey:@"fromSell"];
    self.toCurrency = [self stringValueForKey:@"toCurrency"];
    self.toBuy = [self numberValueForKey:@"toBuy"];

    objectData = nil;
}

- (NSString *)getUpdateTime {
    if (self.updated) {
        NSDate *date = [self.updated dateWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        return [date stringWithDateFormat:@"HH:mm:ss"];
    } else {
        return @"";
    }
}

#pragma mark -

/**
 * Composing update time from self.updated
 * @return
 */
- (NSString *)composeUpdateTime {
    if (self.updated) {
        NSDate *updated = [self.updated dateWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *resultString = [NSString stringWithFormat:@"Курс на %@", [updated stringWithDateFormat:@"dd.MM.yyyy HH:mm"]];
        return resultString;
    } else {
        return nil;
    }
}

- (NSString *)composeCurrencyRateMessage {
    NSString *currencyRate = @"";

    BOOL showFrom = self.fromCurrency && ![self.fromCurrency.uppercaseString isEqualToString:@"KZT"];
    BOOL showTo = self.toCurrency && ![self.toCurrency.uppercaseString isEqualToString:@"KZT"];
    if (showFrom) {
        currencyRate = [currencyRate stringByAppendingFormat:@"1 %@ = %@ KZT%@", self.fromCurrency, [self.fromSell decimalFormatString], (showTo ? @"\n" : @"")];
    }
    if (showTo) {
        currencyRate = [currencyRate stringByAppendingFormat:@"1 %@ = %@ KZT%@", self.toCurrency, [self.toBuy decimalFormatString], (showFrom ? @"\n" : @"")];
    }
    if (showFrom && showTo) {
        if ([self.fromSell doubleValue] >= [self.toBuy doubleValue]) {
            currencyRate = [currencyRate stringByAppendingFormat:@"1 %@ = %@ %@", self.fromCurrency, [self.rate decimalFormatString], self.toCurrency];
        } else {
            currencyRate = [currencyRate stringByAppendingFormat:@"1 %@ = %@ %@", self.toCurrency, [self.rate decimalFormatString], self.fromCurrency];
        }
    }
    return currencyRate;
}

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setUpdated:[self.updated copyWithZone:zone]];
        [copy setConvertedAmount:[self.convertedAmount copyWithZone:zone]];
        [copy setRate:[self.rate copyWithZone:zone]];
        [copy setPresentation:[self.presentation copyWithZone:zone]];
        [copy setFromCurrency:[self.fromCurrency copyWithZone:zone]];
        [copy setFromSell:[self.fromSell copyWithZone:zone]];
        [copy setToCurrency:[self.toCurrency copyWithZone:zone]];
        [copy setToBuy:[self.toBuy copyWithZone:zone]];
  
    }
    
    return copy;
}

@end
