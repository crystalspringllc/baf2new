#import "ModelObject.h"


@interface CommentSrc : ModelObject <NSCoding>

@property (nonatomic, copy) NSString *avaUrl;
@property (nonatomic, copy) NSNumber *commentSrcId;
@property (nonatomic, copy) NSString *nick;

+ (CommentSrc *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
