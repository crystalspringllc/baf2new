//
//  Contract.m
//  Myth
//
//  Created by Almas Adilbek on 11/13/12.
//
//

#import "Contract.h"

@implementation Contract

+ (Contract *)instanceFromDictionary:(NSDictionary *)aDictionary {
    
    Contract *instance = [[Contract alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
    
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.alias = [self stringValueForKey:@"alias"];
    self.identifier = [self numberValueForKey:@"id"];
    self.contract = [self stringValueForKey:@"contract"];
    
    self.provider = [PaymentProvider instanceFromDictionary:objectData[@"provider"]];
    
    objectData = nil;
}

- (NSString *)stringIdentifier {
    return [self.identifier stringValue];
}

#pragma mark NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.alias = [decoder decodeObjectForKey:@"alias"];
    self.identifier = [decoder decodeObjectForKey:@"identifier"];
    self.contract = [decoder decodeObjectForKey:@"contract"];
    self.provider = [decoder decodeObjectForKey:@"provider"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.alias forKey:@"alias"];
    [encoder encodeObject:self.identifier forKey:@"identifier"];
    [encoder encodeObject:self.contract forKey:@"contract"];
    [encoder encodeObject:self.provider forKey:@"provider"];
}

@end
