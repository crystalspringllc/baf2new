#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface City : ModelObject {

    NSString *code;
    NSString *cityId;
    NSString *name;

}

@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *cityId;
@property (nonatomic, copy) NSString *name;

+ (City *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
