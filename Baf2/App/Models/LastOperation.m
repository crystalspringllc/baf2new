//
//  LastOperation.m
//  Myth
//
//  Created by Almas Adilbek on 12/25/12.
//
//

#import "LastOperation.h"

@implementation LastOperation

@synthesize amount, amountCommission, operationID, dateString, status, message;

-(LastOperation *)initWithDatas:(NSDictionary *)datas {
    self = [super init];
    if(self) {
        self.amount = [self floatValueFrom:datas forKey:@"operationAmount"];
        self.amountCommission = [self floatValueFrom:datas forKey:@"operationAmountCommission"];
        self.operationID = [self stringValueFrom:datas forKey:@"operationId"];
        self.dateString = [self stringValueFrom:datas forKey:@"operationDate"];
        self.status = [self stringValueFrom:datas forKey:@"operationStatus"];
        self.message = [self stringValueFrom:datas forKey:@"operationText"];
        self.operationType = [self stringValueFrom:datas forKey:@"operationType"];
        self.operationText = [self stringValueFrom:datas forKey:@"operationText"];
        self.currency = [self stringValueFrom:datas forKey:@"currency"];
        self.providerLogo = [self stringValueFrom:datas forKey:@"provider_logo"];
        self.providerTitle = [self stringValueFrom:datas forKey:@"provider_title"];
    }
    return self;
}

-(NSDate *)date {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    return [df dateFromString:self.dateString];
}

-(BOOL)isSuccess {
    return [status isEqual:@"COMPLETE"];
}
-(BOOL)isFailed {
    return [status isEqual:@"ERROR"];
}
-(BOOL)isInProccess {
    return [status isEqual:@"IN_PROGRESS"];
}

- (BOOL)isPaymentType {
    return [[self.operationType lowercaseString] isEqual:@"payment"];
}

- (BOOL)isTransferType {
    return [[self.operationType lowercaseString] isEqual:@"transfer"];
}

- (BOOL)isSpringdocType {
    return [[self.operationType lowercaseString] isEqual:@"springdoc"];;
}

- (BOOL)isMulticcardInnerConvert {
    NSString *providerTitleLowerCase = [self.providerTitle lowercaseString];
    return [providerTitleLowerCase isEqualToString:@"my_multiccard_inner_convert"];
}

- (NSString *)statusImageName
{
    NSString *iconName = nil;
    if([self isSuccess]) iconName = @"icon-ok";
    else if([self isFailed]) iconName = @"icon-error";
    else if([self isInProccess]) iconName = @"icon-loading";

    return iconName;
}

- (NSString *)currencyString {
    return _currency && _currency.length > 0 ? _currency : @"KZT";
}

- (CGFloat)comission {
    return (self.amountCommission - self.amount);
}

- (NSString *)formattedDate {
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"dd.MM.yyyy HH:mm"];
    return [f stringFromDate:self.date];
}


@end
