//
//  EconomicSector.h
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface EconomicSector : ModelObject

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *economicSectorDescription;

+ (EconomicSector *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
