//
//  OperationHistoryFilter.h
//  Baf2
//
//  Created by Shyngys Kassymov on 21.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSDate+DateTools.h"

@interface OperationHistoryFilter : NSObject

@property (nonatomic) NSInteger count;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *endDate;
@property (nonatomic, strong) NSString *minAmount;
@property (nonatomic, strong) NSString *maxAmount;
@property (nonatomic, strong) NSArray *selectedOperationStatuses;
@property (nonatomic, strong) NSString *providerId;
@property (nonatomic, strong) NSString *contractId;
@property (nonatomic, strong) NSString *paymentId;

+ (instancetype)defaultFilter;
- (NSMutableDictionary *)toJSON;

@end
