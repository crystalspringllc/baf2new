//
//  ProviderCategory.h
//  
//
//  Created by Almas Adilbek on 10/16/13.
//  Copyright (c) 2013. All rights reserved.
//

#import "ModelObject.h"

@interface ProviderCategory : ModelObject <NSCoding> {
    NSString *categoryLogo;
    NSString *categoryName;
    NSNumber *providerCategoryId;
    NSArray *paymentProviders;
}

@property (nonatomic, copy) NSString *categoryLogo;
@property (nonatomic, copy) NSString *categoryName;
@property (nonatomic, copy) NSNumber *providerCategoryId;
@property (nonatomic, copy) NSArray *paymentProviders;

+ (ProviderCategory *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSArray *)supportedProviders;
- (BOOL)hasProviders;
- (BOOL)hasSupportedProviders;
- (NSInteger)providersCount;

+(NSArray *)responseToProviderCategories:(id)response;

@end
