//
//  OperationHistory.h
//  Baf2
//
//  Created by Shyngys Kassymov on 21.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

typedef enum StatusType:NSInteger {
    StatusTypeError,
    StatusTypeComplete,
    StatusTypeInProgress
} StatusType;

@interface OperationHistory : ModelObject

@property (nonatomic, strong) NSNumber *operationId;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *providerCode;
@property (nonatomic, strong) NSString *providerLogo;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSNumber *amountWithCommission;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *commissionCurrency;

+ (OperationHistory *)instanceFromDictionary:(NSDictionary *)aDictionary;
+ (NSArray *)instancesFromArray:(NSArray *)data;

#pragma mark - Helpers

- (NSString *)prettyStatus;

- (BOOL)isPaymenType;

+ (NSString *)stringFromStatus:(StatusType)statusType;
+ (StatusType)statusFromString:(NSString *)status;

+ (NSDateFormatter *)dateFormatterToServer;
+ (NSDateFormatter *)dateFormatterFromServer;

- (NSString *)dateNice;

@end
