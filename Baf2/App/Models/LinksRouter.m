//
//  LinksRouter.m
//  Myth
//
//  Created by Almas Adilbek on 08/20/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "LinksRouter.h"

static LinksRouter *_sharedInstance;

@implementation LinksRouter

+(LinksRouter *)sharedInstance {
    if(!_sharedInstance) {
        _sharedInstance = [[LinksRouter alloc] init];
    }
    return _sharedInstance;
}

-(void)routeUrl:(NSURL *)url
{
    NSString *urlString = [[url absoluteString] lowercaseString];
    UIViewController *rootViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    NSRange rangeEmail = [urlString rangeOfString:@"mailto:"];
    if(rangeEmail.location != NSNotFound)
    {
        urlString = [urlString stringByReplacingOccurrencesOfString:@"mailto:" withString:@""];
        MFMailComposeViewController *tempMailCompose = [[MFMailComposeViewController alloc] init];
        tempMailCompose.navigationBar.tintColor = [UIColor fromRGB:0x444444];
        [tempMailCompose.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor fromRGB:0x444444]}];
        tempMailCompose.mailComposeDelegate = self;

        [tempMailCompose setToRecipients:@[urlString]];
        
        if ([MFMailComposeViewController canSendMail]) {
            [rootViewController presentViewController:tempMailCompose animated:YES completion:^{
//                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:true];
            }];
        } else {
            [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"device_not_support_sending_mail", nil)];
        }
        return;
    }

    NSRange rangeTel = [urlString rangeOfString:@"tel:"];
    if(rangeTel.location != NSNotFound) {
        urlString = [urlString stringByReplacingOccurrencesOfString:@"tel:" withString:@""];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"call_on_number", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancellation", nil) destructiveButtonTitle:nil otherButtonTitles:urlString, nil];
        [actionSheet showInView:rootViewController.view];
        return;
    }

    if([urlString rangeOfString:@"http"].location != NSNotFound || [urlString rangeOfString:@"www"].location != NSNotFound) {
        [UIHelper presentBrowserIn:rootViewController withUrl:urlString];
//        [[UIHelper appDelegate] hideTabbar:YES];
    }
}

#pragma mark -
#pragma mark MFMailComposeViewController

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled: break;
        case MFMailComposeResultSaved: break;
        case MFMailComposeResultSent:
            [Toast showToast:NSLocalizedString(@"sent", nil)];
            
            break;
        case MFMailComposeResultFailed:
            [Toast showToast:NSLocalizedString(@"sending_error", nil)];
            break;
        default: break;
    }
    [controller dismissViewControllerAnimated:YES completion:nil];
//    [[UIHelper appDelegate] showTabbar:NO];
}

#pragma mark -
#pragma mark UIActionSheet

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex != actionSheet.cancelButtonIndex) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [actionSheet buttonTitleAtIndex:buttonIndex]]]];
    }
}

@end
