//
//  TransferTemplate.m
//  Baf2
//
//  Created by Shyngys Kassymov on 10.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "TransferTemplate.h"

@implementation TransferTemplate

+ (TransferTemplate *)instanceFromDictionary:(NSDictionary *)aDictionary {
    TransferTemplate *instance = [[TransferTemplate alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.templateId = [self numberValueForKey:@"templateId"];
    self.clientId = [self numberValueForKey:@"clientId"];
    self.alias = [self stringValueForKey:@"alias"];
    self.direction = [self boolValueForKey:@"direction"];
    self.isFavorite = [self boolValueForKey:@"isFavorite"];
    self.templateIsActive = [self boolValueForKey:@"templateIsActive"];
    self.date = [self stringValueForKey:@"date"];
    self.type = [self stringValueForKey:@"type"];
    
    self.provider = [PaymentProvider instanceFromDictionary:aDictionary[@"provider"]];
    
    self.requestorId = [self numberValueForKey:@"requestorId"];
    self.requestorAccount = [Account instanceFromDictionary:aDictionary[@"requestorAccount"]];
    self.requestorAmount = [self numberValueForKey:@"requestorAmount"];
    self.requestorType = [self stringValueForKey:@"requestorType"];
    self.requestorCurrency = [self stringValueForKey:@"requestorCurrency"];
    
    self.destinationId = [self numberValueForKey:@"destinationId"];
    self.destinationAccount = [Account instanceFromDictionary:aDictionary[@"destinationAccount"]];
    self.destinationAmount = [self numberValueForKey:@"destinationAmount"];
    self.destinationType = [self stringValueForKey:@"destinationType"];
    self.destinationCurrency = [self stringValueForKey:@"destinationCurrency"];

    self.knpName = [self stringValueForKey:@"knpName"];
    self.reference = [self stringValueForKey:@"reference"];
    
    objectData = nil;
}

#pragma mark - Getters/Setters

- (BOOL)templateIsActive {
    return _templateIsActive && _requestorAccount && _requestorAccount.accountId && _destinationAccount && _destinationAccount.accountId;
}

@end
