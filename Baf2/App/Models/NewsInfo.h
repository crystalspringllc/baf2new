#import "ModelObject.h"


@interface NewsInfo : ModelObject <NSCoding>
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *subject;

+ (NewsInfo *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
