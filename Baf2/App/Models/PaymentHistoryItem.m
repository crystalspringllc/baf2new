//
//  PaymentHistoryItem.m
//  Myth
//
//  Created by Almas Adilbek on 12/4/12.
//
//

#import "PaymentHistoryItem.h"

@implementation PaymentHistoryItem

@synthesize amount, contractId, operationID, date, description, contract;

-(PaymentHistoryItem *)initWithDatas:(NSDictionary *)datas
{
    self = [super init];
    if(self) {
        self.amount = [self numberValueFrom:datas forKey:@"amount"];
        self.operationID = [self numberValueFrom:datas forKey:@"id"];
        self.date = [self dateValueFrom:datas forKey:@"dtcreated" dateFormat:@""];
        self.contractId = [self numberValueFrom:datas forKey:@"contractId"];
        self.descriptionText = [self stringValueFrom:datas forKey:@"description"];
        
        Contract *contactObject = [Contract instanceFromDictionary:[datas objectForKey:@"contract"]];
        self.contract = contactObject;
    }
    return self;
}

@end
