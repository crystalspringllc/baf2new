#import "CardTypeOLD.h"

#import "CardProduct.h"

@implementation CardTypeOLD

@synthesize brand;
@synthesize code;
@synthesize descriptionText;
@synthesize enable;
@synthesize cardTypeId;
@synthesize logo;
@synthesize name;
@synthesize products;
@synthesize sicCode;

+ (CardTypeOLD *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CardTypeOLD *instance = [[CardTypeOLD alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    [self setValuesForKeysWithDictionary:aDictionary];

}

- (void)setValue:(id)value forKey:(NSString *)key {

    if ([key isEqualToString:@"products"]) {

        if ([value isKindOfClass:[NSArray class]]) {

            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                CardProduct *populatedMember = [CardProduct instanceFromDictionary:valueMember];
                [myMembers addObject:populatedMember];
            }

            self.products = myMembers;

        }

    } else {
        [super setValue:value forKey:key];
    }

}


- (void)setValue:(id)value forUndefinedKey:(NSString *)key {

    if ([key isEqualToString:@"description"]) {
        [self setValue:value forKey:@"descriptionText"];
    } else if ([key isEqualToString:@"id"]) {
        [self setValue:value forKey:@"cardTypeId"];
    } else if ([key isEqualToString:@"sic_code"]) {
        [self setValue:value forKey:@"sicCode"];
    } else {
        [super setValue:value forUndefinedKey:key];
    }

}



@end
