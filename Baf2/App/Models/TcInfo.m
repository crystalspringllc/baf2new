#import "TcInfo.h"

@implementation TcInfo

@synthesize companyname, dateout, fio, idn, isconstant, isinactive, isjuridical, isResident, priznakfl, reasonout, rnn, taxcode;

+ (TcInfo *)instanceFromDictionary:(NSDictionary *)aDictionary {

    TcInfo *instance = [[TcInfo alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;
    
    self.companyname = [self stringValueForKey:@"companyname"];
    self.dateout = [self stringValueForKey:@"dateout"];
    self.fio = [self stringValueForKey:@"fio"];
    self.idn = [self stringValueForKey:@"idn"];
    self.isconstant = [self stringValueForKey:@"isconstant"];
    self.isinactive = [self stringValueForKey:@"isinactive"];
    self.isjuridical = [self boolValueForKey:@"isjuridical"];
    self.isResident = [self boolValueForKey:@"isresident"];
    self.priznakfl = [self stringValueForKey:@"priznakfl"];
    self.reasonout = [self stringValueForKey:@"reasonout"];
    self.rnn = [self stringValueForKey:@"rnn"];
    self.taxcode = [self stringValueForKey:@"taxcode"];

    objectData = nil;
}

- (BOOL)isCorporate {
    return self.isjuridical;
}

- (NSString *)firstname {
    NSArray *array = [self.fio componentsSeparatedByString:@" "];
    if(array.count > 1) return array[1];
    return @"";
}

- (NSString *)surname
{
    NSArray *array = [self.fio componentsSeparatedByString:@" "];
    if(array.count > 0) return array[0];
    return @"";
}


@end
