//
//  Contract.h
//  Myth
//
//  Created by Almas Adilbek on 11/13/12.
//
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"
#import "PaymentProvider.h"

typedef NS_ENUM(NSInteger, ContractCategoryType) {
    ContractCategoryTypeCellular = 1,
    ContractCategoryTypeCommunal = 3,
    ContractCategoryTypeFixedPhone = 4,
    ContractCategoryTypeTvAndInternet = 5,
    ContractCategoryTypeSecurity = 6,
    ContractCategoryTypeGamesOnline = 7,
    ContractCategoryTypeOther = 9,
    ContractCategoryTypeCharity = 10,
    ContractCategoryTypePDD = 11,
    ContractCategoryTypeServices = 12
};

@interface Contract : ModelObject <NSCoding>

@property (nonatomic, strong) NSString *alias;
@property (nonatomic, strong) NSNumber *identifier;
@property (nonatomic, strong) NSString *contract;
@property (nonatomic, strong) PaymentProvider *provider;

+ (Contract *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)stringIdentifier;



@end
