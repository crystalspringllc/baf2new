//
//  Payment.h
//  Myth
//
//  Created by Almas Adilbek on 11/29/12.
//
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface Payment : ModelObject {
    NSNumber *amount;
    NSString *contractIdentifier;
    NSString *contractAlias;
    NSString *providerCode;
}

@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSString *contractIdentifier;
@property (nonatomic, strong) NSString *contractAlias;
@property (nonatomic, strong) NSString *providerCode;

-(Payment *)initWithDatas:(NSDictionary *)datas;

@end
