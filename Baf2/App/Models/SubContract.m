//
// Created by Askar Mustafin on 10/7/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "SubContract.h"
#import "NSObject+Json.h"
#import "NSString+Ext.h"
#import "NSDate+Helper.h"


@implementation SubContract {

}

+ (SubContract *)instanceFromDictionary:(NSDictionary *)aDictionary {
    SubContract *subContract = [[SubContract alloc] init];
    [subContract setAttributesFromDictionary:aDictionary];
    return subContract;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.name = [self stringValueForKey:@"name"];
    self.acc = [self stringValueForKey:@"acc"];
    self.amount = [self stringValueForKey:@"amount"];
    self.canPay = [self boolValueForKey:@"canPay"];

    self.sum = [self stringValueForKey:@"sum"];
    self.num = [self stringValueForKey:@"num"];
    self.date = [self stringValueForKey:@"date"];

    objectData = nil;
}

- (NSString *)getAmountString {
    NSString *amountWithoutDot = [self.amount stringByReplacingOccurrencesOfString:@"\\s"
                                                                withString:@""
                                                                   options:NSRegularExpressionSearch
                                                                     range:NSMakeRange(0, self.amount.length)];

    return amountWithoutDot;
}

- (NSString *)getJsonStringNotation {
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc]init];
    if (self.name) jsonDict[@"name"] = self.name;
    if (self.acc) jsonDict[@"acc"] = self.acc;
    if (self.amount) jsonDict[@"amount"] = self.amount;
    if (self.canPay) jsonDict[@"canPay"] = @(self.canPay);

    if (self.sum) jsonDict[@"sum"] = self.sum;
    if (self.num) jsonDict[@"num"] = self.num;
    if (self.date) jsonDict[@"date"] = self.date;

    return [jsonDict JSONRepresentation];
}

- (NSString *)getDateString {
    NSDate *date = [[self.date substringToIndex:8] dateWithDateFormat:@"yyyyMMdd"];
    return [date stringWithFormat:@"dd.MM.yyyy"];
}


@end