//
//  OperationHistoryDetails.h
//  Baf2
//
//  Created by Shyngys Kassymov on 21.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"
#import "Contract.h"
#import "PaymentProvider.h"
#import "Invoice.h"
#import "TransferTemplate.h"

@interface OperationHistoryDetails : ModelObject

@property (nonatomic, strong) NSNumber *orderId;
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *logstate;
@property (nonatomic, strong) NSNumber *serviceId;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSNumber *commission;
@property (nonatomic, strong) NSNumber *amountWithCommission;
@property (nonatomic, strong) NSNumber *bonusAmount;
@property (nonatomic, strong) NSNumber *providerId;
@property (nonatomic, strong) NSString *providerName;
@property (nonatomic, strong) NSString *providerLogo;
@property (nonatomic, strong) NSString *providerCode;
@property (nonatomic, strong) NSString *processingCode;
@property (nonatomic, strong) NSString *processing;
@property (nonatomic, strong) NSNumber *invoiceId;
@property (nonatomic, strong) NSString *reference;
@property (nonatomic, strong) NSNumber *cardId;
@property (nonatomic, strong) NSString *cardNumber;
@property (nonatomic, strong) NSString *terminal;
@property (nonatomic) BOOL smsAuthNeed;

@property (nonatomic, strong) Contract *contract;
@property (nonatomic, strong) PaymentProvider *provider;
@property (nonatomic, strong) Invoice *invoice;
@property (nonatomic, strong) TransferTemplate *transfer;

+ (OperationHistoryDetails *)instanceFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)dateNice;

@end
