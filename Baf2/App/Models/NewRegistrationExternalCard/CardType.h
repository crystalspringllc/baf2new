//
// Created by Askar Mustafin on 6/8/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface CardType : ModelObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *descr;

+ (CardType *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end