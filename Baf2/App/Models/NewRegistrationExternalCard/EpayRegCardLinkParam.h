#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface EpayRegCardLinkParam : ModelObject

@property (nonatomic, copy) NSString *approveLink;
@property (nonatomic, copy) NSString *backLink;
@property (nonatomic, copy) NSString *base64Content;
@property (nonatomic, copy) NSString *failBackLink;
@property (nonatomic, copy) NSString *failPostLink;
@property (nonatomic, copy) NSString *postlink;
@property (nonatomic, copy) NSString *template;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *uuid;
@property (nonatomic, copy) NSString *appendix;
@property (nonatomic, copy) NSString *language;

+ (EpayRegCardLinkParam *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
