//
// Created by Askar Mustafin on 6/9/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "SaveCardResponse.h"
#import "ExtCard.h"


@implementation SaveCardResponse {}

+ (SaveCardResponse *)instanceFromDictionary:(NSDictionary *)aDictionary {
    SaveCardResponse *instance = [[SaveCardResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.orderId = [self numberValueForKey:@"orderId"];
    self.state = [self stringValueForKey:@"state"];
    self.uuid = [self stringValueForKey:@"uuid"];
    self.extCard = [ExtCard instanceFromDictionary:objectData[@"card"]];

    objectData = nil;
}

@end