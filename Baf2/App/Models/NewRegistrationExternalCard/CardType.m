//
// Created by Askar Mustafin on 6/8/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "CardType.h"


@implementation CardType {}

+ (CardType *)instanceFromDictionary:(NSDictionary *)aDictionary {
    CardType *instance = [[CardType alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.name = [self stringValueForKey:@"name"];
    self.descr = [self stringValueForKey:@"description"];

    objectData = nil;
}

@end