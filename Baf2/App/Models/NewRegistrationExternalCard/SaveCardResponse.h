//
// Created by Askar Mustafin on 6/9/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"

@class ExtCard;


@interface SaveCardResponse : ModelObject

@property (nonatomic, strong) NSNumber *orderId;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) ExtCard *extCard;

+ (SaveCardResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end