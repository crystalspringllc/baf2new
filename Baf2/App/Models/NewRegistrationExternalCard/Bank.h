//
// Created by Askar Mustafin on 6/9/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"


@interface Bank : ModelObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *descr;

+ (Bank *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end