//
// Created by Askar Mustafin on 6/9/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "Bank.h"


@implementation Bank {

}

+ (Bank *)instanceFromDictionary:(NSDictionary *)aDictionary {
    Bank *instance = [[Bank alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.name = [self stringValueForKey:@"name"];
    self.descr = [self stringValueForKey:@"description"];

    objectData = nil;
}

@end