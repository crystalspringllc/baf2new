#import "EpayRegCardLinkParam.h"

@implementation EpayRegCardLinkParam

+ (EpayRegCardLinkParam *)instanceFromDictionary:(NSDictionary *)aDictionary {

    EpayRegCardLinkParam *instance = [[EpayRegCardLinkParam alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.approveLink = [self stringValueForKey:@"approveLink"];
    self.backLink = [self stringValueForKey:@"backLink"];
    self.base64Content = [self stringValueForKey:@"base64Content"];
    self.failBackLink = [self stringValueForKey:@"failBackLink"];
    self.failPostLink = [self stringValueForKey:@"failPostLink"];
    self.postlink = [self stringValueForKey:@"postlink"];
    self.template = [self stringValueForKey:@"template"];
    self.url = [self stringValueForKey:@"url"];
    self.uuid = [self stringValueForKey:@"uuid"];
    self.language = [self stringValueForKey:@"language"];
    self.appendix = [self stringValueForKey:@"appendix"];

    objectData = nil;

}


@end
