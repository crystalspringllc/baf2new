#import "NewsSrc.h"

@implementation NewsSrc
- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    [encoder encodeObject:self.avaUrl forKey:@"avaUrl"];
    [encoder encodeObject:self.newsSrcId forKey:@"newsSrcId"];
    [encoder encodeObject:self.nick forKey:@"nick"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super initWithCoder:decoder])) {
        self.avaUrl = [decoder decodeObjectForKey:@"avaUrl"];
        self.newsSrcId = [decoder decodeObjectForKey:@"newsSrcId"];
        self.nick = [decoder decodeObjectForKey:@"nick"];
    }
    return self;
}

+ (NewsSrc *)instanceFromDictionary:(NSDictionary *)aDictionary {

    NewsSrc *instance = [[NewsSrc alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;
    
    self.avaUrl    = [self stringValueForKey:@"avaUrl"];
    self.newsSrcId = [self numberValueForKey:@"id"];
    self.nick      = [self stringValueForKey:@"nick"];
    
    objectData = nil;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {

    if ([key isEqualToString:@"id"]) {
        [self setValue:value forKey:@"newsSrcId"];
    } else {
        [super setValue:value forUndefinedKey:key];
    }

}


- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.avaUrl) {
        [dictionary setObject:self.avaUrl forKey:@"avaUrl"];
    }

    if (self.newsSrcId) {
        [dictionary setObject:self.newsSrcId forKey:@"newsSrcId"];
    }

    if (self.nick) {
        [dictionary setObject:self.nick forKey:@"nick"];
    }

    return dictionary;

}


@end
