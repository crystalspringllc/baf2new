#import "RequestCard.h"
#import "IdTitleObject.h"

@implementation RequestCard

@synthesize requestCardId, image, imagebig, title, amount;

+ (RequestCard *)instanceFromDictionary:(NSDictionary *)aDictionary {

    RequestCard *instance = [[RequestCard alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.infos = @[];

    self.title = [self stringValueForKey:@"title"];
    self.requestCardId = [self stringValueForKey:@"id"];
    self.image = [self stringValueForKey:@"image"];
    self.imagebig = [self stringValueForKey:@"imagebig"];
    self.amount = [self numberValueForKey:@"amount"];

    NSArray *receivedInfo = aDictionary[@"info"];
    if ([receivedInfo isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedInfo = [NSMutableArray arrayWithCapacity:[receivedInfo count]];
        for (NSDictionary *item in receivedInfo) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedInfo addObject:[IdTitleObject instanceWithId:item[@"title"] title:item[@"text"]]];
            }
        }

        self.infos = populatedInfo;
    }
}


@end
