#import "RequestCardTariffs.h"
#import "IdTitleObject.h"

@implementation RequestCardTariffs

@synthesize items, note, title;

+ (RequestCardTariffs *)instanceFromDictionary:(NSDictionary *)aDictionary {

    RequestCardTariffs *instance = [[RequestCardTariffs alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.note = aDictionary[@"note"];
    self.title = aDictionary[@"title"];

    NSArray *receivedItems = aDictionary[@"items"];
    if ([receivedItems isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedItems = [NSMutableArray array];
        for (NSDictionary *item in receivedItems) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedItems addObject:[IdTitleObject instanceWithId:item[@"title"] title:item[@"text"]]];
            }
        }

        self.items = populatedItems;
    } else self.items = @[];

}


@end
