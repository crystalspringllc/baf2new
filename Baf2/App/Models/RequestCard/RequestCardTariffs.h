#import "ModelObject.h"


@interface RequestCardTariffs : ModelObject {

    NSArray *items;
    NSString *note;
    NSString *title;

}

@property (nonatomic, copy) NSArray *items;
@property (nonatomic, copy) NSString *note;
@property (nonatomic, copy) NSString *title;

+ (RequestCardTariffs *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
