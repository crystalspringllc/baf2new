
#import "RequestCardResponse.h"
#import "RequestCardTariffs.h"
#import "RequestCard.h"
#import "IdTitleObject.h"

@implementation RequestCardResponse

@synthesize allcardsinfo, cards, cities;

+ (RequestCardResponse *)instanceFromDictionary:(NSDictionary *)aDictionary {
    RequestCardResponse *instance = [[RequestCardResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.allcardsinfo = @[];
    self.cards = @[];
    self.cities = @[];

    NSArray *receivedAllcardsinfo = aDictionary[@"allcardsinfo"];
    if ([receivedAllcardsinfo isKindOfClass:[NSArray class]])
    {
        NSMutableArray *populatedAllcardsinfo = [NSMutableArray array];
        for (NSDictionary *item in receivedAllcardsinfo) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedAllcardsinfo addObject:[RequestCardTariffs instanceFromDictionary:item]];
            }
        }
        self.allcardsinfo = populatedAllcardsinfo;
    }

    NSArray *receivedCards = aDictionary[@"cards"];
    if ([receivedCards isKindOfClass:[NSArray class]])
    {
        NSMutableArray *populatedCards = [NSMutableArray array];
        for (NSDictionary *item in receivedCards) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedCards addObject:[RequestCard instanceFromDictionary:item]];
            }
        }
        self.cards = populatedCards;
    }

    NSArray *receivedCities = aDictionary[@"cities"];
    if ([receivedCities isKindOfClass:[NSArray class]])
    {
        NSMutableArray *populatedCities = [NSMutableArray array];
        for (NSDictionary *item in receivedCities) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedCities addObject:[IdTitleObject instanceWithId:item[@"id"] title:item[@"name"] data:item[@"cardissuevalid"]]];
            }
        }
        self.cities = populatedCities;
    }

}


@end
