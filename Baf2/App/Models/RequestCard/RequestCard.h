#import "ModelObject.h"


@interface RequestCard : ModelObject {

}

@property (nonatomic, copy) NSString *requestCardId;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *imagebig;
@property (nonatomic, copy) NSArray *infos;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSNumber *amount;

+ (RequestCard *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
