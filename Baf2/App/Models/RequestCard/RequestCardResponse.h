#import "ModelObject.h"


@interface RequestCardResponse : ModelObject {

}

@property (nonatomic, copy) NSArray *allcardsinfo;
@property (nonatomic, copy) NSArray *cards;
@property (nonatomic, copy) NSArray *cities;

+ (RequestCardResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
