#import "BankInfo.h"

@implementation BankInfo

@synthesize bic, code, name, namekz;

+ (BankInfo *)instanceFromDictionary:(NSDictionary *)aDictionary {

    BankInfo *instance = [[BankInfo alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.bic = [self stringValueFrom:aDictionary forKey:@"bic"];
    self.code = [self stringValueFrom:aDictionary forKey:@"code"];
    self.name = [self stringValueFrom:aDictionary forKey:@"name"];
    self.namekz = [self stringValueFrom:aDictionary forKey:@"namekz"];
}


@end
