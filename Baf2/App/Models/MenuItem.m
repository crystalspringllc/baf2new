//
// Created by Askar Mustafin on 9/5/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <ChameleonFramework/ChameleonMacros.h>
#import "MenuItem.h"
#import "MainViewController.h"
#import "MyCardsAndAccountsViewController.h"
#import "PaymentsViewController.h"
#import "OperationHistoryViewController.h"
#import "NewsViewController.h"
#import "MessageSupportViewController.h"
#import "SettingsTableViewController.h"
#import "BankOffersViewController.h"
#import "ViewController.h"
#import "ExchangeRatesViewController.h"
#import "ContactsViewController.h"
#import "ATMandDepartmentsViewController.h"
#import "BankDetailsTableViewController.h"
#import "NBrowserViewController.h"
#import "OpportunitiesViewController.h"
#import "TransfersQuickViewController.h"
#import "TransferViewController.h"
#import "User.h"
#import "NewProfileViewController.h"
#import "TickerViewController.h"
#import "OnlineDepositStartViewController.h"


@implementation MenuItem {}

- (id)initWithName:(NSString *)name icon:(NSString *)icon color:(UIColor *)color vc:(UIViewController *)vc {
    self = [super init];
    if (self) {
        self.name = name;
        self.icon = icon;
        self.color = color;
        self.correspondingVC = vc;
    }
    return self;
}


+ (NSArray *)userMenuItems {
    NBrowserViewController *policyVC = [[NBrowserViewController alloc] initWithUrls:[NSURL URLWithString:kBankPrivacyUrl]];
    policyVC.showMenuInstreadOfBackButton = true;
    policyVC.toolbar.tintColor = [UIColor blueColor];
    policyVC.title = MENU_TITLE_POLICY;

    MyCardsAndAccountsViewController *myCardsAndAccountsVC = [[MyCardsAndAccountsViewController alloc] init];
    myCardsAndAccountsVC.isOpenedFromMenu = YES;
    if ([User sharedInstance].accountsResponse) {
        myCardsAndAccountsVC.accountsResponse = [User sharedInstance].accountsResponse;
    }


    BankOffersViewController *bankOffersViewController = [BankOffersViewController createVC];
    bankOffersViewController.isOpenedFromMenu = YES;

    NBrowserViewController *privilegeVC = [[NBrowserViewController alloc] initWithUrls:[NSURL URLWithString:kBankPriilegeUrl]];
    privilegeVC.showMenuInstreadOfBackButton = true;
    privilegeVC.toolbar.tintColor = [UIColor blueColor];
    privilegeVC.title = MENU_TITLE_PRIVILEGE;
    
    
    return @[
            [[MenuItem alloc] initWithName:MENU_TITLE_PROFILE icon:@"" color:[UIColor flatBrownColor] vc:[NewProfileViewController createVC]],
            [[MenuItem alloc] initWithName:MENU_TITLE_MAINVC icon:@"iconDashboard" color:[UIColor flatYellowColor] vc:[[MainViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_CARDSANDACCOUNTS icon:@"iconCardMenu" color:[UIColor flatForestGreenColor] vc:myCardsAndAccountsVC],
            [[MenuItem alloc] initWithName:MENU_TITLE_PAYMENTS icon:@"iconPortmone" color:[UIColor flatSkyBlueColorDark] vc:[[PaymentsViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_TRANSFERS icon:@"transf" color:[UIColor flatMintColorDark] vc:[TransfersQuickViewController createVC]],
           // [[MenuItem alloc] initWithName:MENU_TITLE_TRANSFERS icon:@"transf" color:[UIColor flatMintColorDark] vc:[TransferViewController createVC]],
            [[MenuItem alloc] initWithName:MENU_TITLE_HISTORY icon:@"iconArchieve" color:[UIColor flatMagentaColorDark] vc:[[OperationHistoryViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_DEPOSIT icon:@"icn_menu_deposit" color:[UIColor flatGreenColor] vc:[OnlineDepositStartViewController createVC]],
            [[MenuItem alloc] initWithName:MENU_TITLE_NEWS icon:@"iconLenta" color:[UIColor flatPowderBlueColorDark] vc:[NewsViewController createVC]],
            [[MenuItem alloc] initWithName:MENU_TITLE_SUPPORT icon:@"icn_chat" color:[UIColor flatGreenColor] vc:[[MessageSupportViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_SETTINGS icon:@"iconSettings1" color:[UIColor flatPowderBlueColor] vc:[SettingsTableViewController createVC]],
            [[MenuItem alloc] initWithName:MENU_TITLE_EXIT icon:@"iconExit" color:[UIColor flatBlackColor] vc:nil],
            [[MenuItem alloc] initWithName:MENU_TITLE_EXCHANGERATES icon:@"exchng" color:nil vc:[[ExchangeRatesViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_TICKERS icon:@"ic_ticker" color:nil vc:[[TickerViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_EPIN icon:@"icn_menu_epin" color:nil vc:nil],
            [[MenuItem alloc] initWithName:MENU_TITLE_BANKOFFERS icon:@"packet" color:nil vc:bankOffersViewController],
            [[MenuItem alloc] initWithName:MENU_TITLE_CONTACTS icon:@"icn_menu_contacts" color:nil vc:[[ContactsViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_ATMANDDEP icon:@"pin" color:nil vc:[[ATMandDepartmentsViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_PRIVILEGE icon:@"icon_privilege" color:nil vc:privilegeVC],
            [[MenuItem alloc] initWithName:MENU_TITLE_BANKDETAILS icon:@"iconInfo" color:nil vc:[[BankDetailsTableViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_POLICY icon:@"icn_menu_law" color:nil vc:policyVC]
    ];
}

+ (NSArray *)guestMenuItems {
    BankOffersViewController *becomeClientVC = [BankOffersViewController createVC];
    becomeClientVC.isOpenedFromMenu = YES;
    becomeClientVC.becomeClient = YES;

    BankOffersViewController *bankOffersViewController = [BankOffersViewController createVC];
    bankOffersViewController.isOpenedFromMenu = YES;

    NBrowserViewController *policyVC = [[NBrowserViewController alloc] initWithUrls:[NSURL URLWithString:kBankPrivacyUrl]];
    policyVC.showMenuInstreadOfBackButton = true;
    policyVC.toolbar.tintColor = [UIColor blueColor];
    policyVC.title = MENU_TITLE_POLICY;

    NBrowserViewController *privilegeVC = [[NBrowserViewController alloc] initWithUrls:[NSURL URLWithString:kBankPriilegeUrl]];
    privilegeVC.showMenuInstreadOfBackButton = true;
    privilegeVC.toolbar.tintColor = [UIColor blueColor];
    privilegeVC.title = MENU_TITLE_PRIVILEGE;

    return @[
            [[MenuItem alloc] initWithName:MENU_TITLE_OPPORTUNITIES icon:@"iconHelm" color:[UIColor flatYellowColor] vc:[[OpportunitiesViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_BECOMECLIENT icon:@"iconAddUser" color:[UIColor flatForestGreenColor] vc:becomeClientVC],
            [[MenuItem alloc] initWithName:MENU_TITLE_LOGINREGISTER icon:@"iconEnter" color:[UIColor flatPowderBlueColor] vc:[ViewController createVC]],
            [[MenuItem alloc] initWithName:MENU_TITLE_EASYTRANSFER icon:@"easytransfer" color:nil vc:nil],
            [[MenuItem alloc] initWithName:MENU_TITLE_NEWS icon:@"iconLenta" color:nil vc:[NewsViewController createVC]],
            [[MenuItem alloc] initWithName:MENU_TITLE_EXCHANGERATES icon:@"exchng" color:nil vc:[[ExchangeRatesViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_TICKERS icon:@"ic_ticker" color:nil vc:[[TickerViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_EPIN icon:@"icn_menu_epin" color:nil vc:nil],
            [[MenuItem alloc] initWithName:MENU_TITLE_BANKOFFERS icon:@"packet" color:nil vc:bankOffersViewController],
            [[MenuItem alloc] initWithName:MENU_TITLE_CONTACTS icon:@"icn_menu_contacts" color:nil vc:[[ContactsViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_ATMANDDEP icon:@"pin" color:nil vc:[[ATMandDepartmentsViewController alloc] init]],
            [[MenuItem alloc] initWithName:MENU_TITLE_POLICY icon:@"icn_menu_law" color:nil vc:policyVC],
            [[MenuItem alloc] initWithName:MENU_TITLE_PRIVILEGE icon:@"icon_privilege" color:nil vc:privilegeVC],
            [[MenuItem alloc] initWithName:MENU_TITLE_BANKDETAILS icon:@"iconInfo" color:nil vc:[[BankDetailsTableViewController alloc] init]]
    ];
}

+ (NSIndexPath *)rowIndexPathIn:(NSArray *)array byName:(NSString *)name {
    for (int i = 0; i < array.count; i++) {
        MenuItem *menuItem = array[(NSUInteger) i];
        if ([menuItem.name isEqualToString:name]) {
            return [NSIndexPath indexPathForRow:i inSection:0];
        }
    }
    return nil;
}

@end
