//
//  OperationHistoryDetails.m
//  Baf2
//
//  Created by Shyngys Kassymov on 21.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "OperationHistoryDetails.h"
#import "NSString+Ext.h"
#import "NSDate+Ext.h"

@implementation OperationHistoryDetails

+ (OperationHistoryDetails *)instanceFromDictionary:(NSDictionary *)aDictionary {
    OperationHistoryDetails *instance = [[OperationHistoryDetails alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.orderId = [self numberValueForKey:@"orderId"];
    self.uuid = [self stringValueForKey:@"uuid"];
    self.date = [self stringValueForKey:@"date"];
    self.logstate = [self stringValueForKey:@"logstate"];
    self.serviceId = [self numberValueForKey:@"serviceId"];
    self.amount = [self numberValueForKey:@"amount"];
    self.commission = [self numberValueForKey:@"commission"];
    self.amountWithCommission = [self numberValueForKey:@"amountWithCommission"];
    self.bonusAmount = [self numberValueForKey:@"bonusAmount"];
    self.providerId = [self numberValueForKey:@"providerId"];
    self.providerName = [self stringValueForKey:@"providerName"];
    self.providerLogo = [self stringValueForKey:@"providerLogo"];
    self.providerCode = [self stringValueForKey:@"providerCode"];
    self.processingCode = [self stringValueForKey:@"processingCode"];
    self.processing = [self stringValueForKey:@"processing"];
    self.invoiceId = [self numberValueForKey:@"invoiceId"];
    self.reference = [self stringValueForKey:@"reference"];
    self.cardId = [self numberValueForKey:@"cardId"];
    self.cardNumber = [self stringValueForKey:@"cardNumber"];
    self.terminal = [self stringValueForKey:@"terminal"];
    self.smsAuthNeed = [self boolValueForKey:@"smsAuthNeed"];
    
    self.contract = [Contract instanceFromDictionary:aDictionary[@"contract"]];
    self.provider = [PaymentProvider instanceFromDictionary:aDictionary[@"provider"]];
    self.invoice = [Invoice instanceFromDictionary:aDictionary[@"invoice"]];
    self.transfer = [TransferTemplate instanceFromDictionary:aDictionary[@"transfer"]];
    
    objectData = nil;
}

#pragma mark - Methods

- (NSString *)providerLogo {
    if (_providerLogo && _providerLogo.length > 0) {
        return [NSString stringWithFormat:@"%@%@", PROVIDER_IMAGE_URL, _providerLogo.lowercaseString];
    }
    
    return @"";
}

- (NSString *)dateNice {
    NSDate *operationDate = [self.date dateWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [operationDate stringWithDateFormat:@"dd.MM.yyyy HH:mm:ss"];
}

@end
