#import "LastOperationDetails.h"
#import "OperationTransferInfo.h"
#import "NSString+Ext.h"
#import "NSNumber+Ext.h"

@implementation LastOperationDetails

@synthesize alias, amountBonus, amountCard, amountContract, amountWithCommission, card, cardBin, commission, contract, contractId, dtcreated, lastOperationDetailsId, infoRequestType, invoiceId, logstate, processingCode, providerCode, providerId, providerLogo, providerName, reason, rrn, serviceId, terminal;

+ (LastOperationDetails *)instanceFromDictionary:(NSDictionary *)aDictionary {

    LastOperationDetails *instance = [[LastOperationDetails alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.alias = [self stringValueForKey:@"alias"];
    self.amountBonus = [self stringValueForKey:@"amount_bonus"];
    self.amountCard = [self stringValueForKey:@"amount_card"];
    self.amountContract = [self stringValueForKey:@"amount_contract"];
    self.amountWithCommission = [self stringValueForKey:@"amount_with_commission"];
    self.card = [self stringValueForKey:@"card"];
    self.cardBin = [self stringValueForKey:@"card_bin"];
    self.commission = [self stringValueForKey:@"commission"];
    self.contract = [self stringValueForKey:@"contract"];
    self.contractId = [self stringValueForKey:@"contract_id"];
    self.dtcreated = [self stringValueForKey:@"dtcreated"];
    self.lastOperationDetailsId = [self stringValueForKey:@"id"];
    self.infoRequestType = [self stringValueForKey:@"info_request_type"];
    self.invoiceId = [self stringValueForKey:@"invoice_id"];
    self.logstate = [self stringValueForKey:@"logstate"];
    self.processingCode = [self stringValueForKey:@"processing_code"];
    self.providerCode = [self stringValueForKey:@"provider_code"];
    self.providerId = [self stringValueForKey:@"provider_id"];
    self.providerLogo = [self stringValueForKey:@"provider_logo"];
    self.providerName = [self stringValueForKey:@"provider_name"];
    self.providerUrl = [self stringValueForKey:@"provider_url"];
    self.reason = [self stringValueForKey:@"reason"];
    self.rrn = [self stringValueForKey:@"rrn"];
    self.serviceId = [self stringValueForKey:@"service_id"];
    self.terminal = [self stringValueForKey:@"terminal"];
    self.statusCode = [self stringValueForKey:@"status_code"];
    self.statusText = [self stringValueForKey:@"status_text"];

    if([self.commission isEqual:@""]) self.commission = @"0.00";
    self.dtcreated = [self.dtcreated stringByReplacingOccurrencesOfString:@".0" withString:@""];

    // Transfer info
    NSDictionary *transferInfoDic = aDictionary[@"transfer-info"];
    if(transferInfoDic) {
        self.transferInfo = [OperationTransferInfo instanceFromDictionary:transferInfoDic];
    }

    objectData = nil;
}

- (NSString *)dateWithTimeString
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSDate *date = [df dateFromString:self.dtcreated];

    [df setDateFormat:@"yyyy.MM.dd HH:mm"];
    return [df stringFromDate:date];
}

- (BOOL)isSuccess {
    return [[self.logstate lowercaseString] isEqual:@"success"];
}

- (BOOL)isError {
    return [[self.logstate lowercaseString] isEqual:@"error"];
}

- (BOOL)isRejected {
    return [[self.logstate lowercaseString] isEqual:@"rejected"];
}

- (BOOL)isSentToPS {
    return [[self.logstate lowercaseString] isEqual:@"sent_to_ps"];
}

- (NSString *)transferCommissionWithCurrency {
    if(_transferInfo && [_transferInfo.comission isNotEmpty]) {
        return self.transferInfo.comissionWithCurrency;
    }
    NSString *currency = _transferInfo ? _transferInfo.cURRENCY : @"KZT";
    return [NSString stringWithFormat:@"%@ %@", [@([self.commission floatValue]) decimalFormatString], currency];
}


@end
