//
//  Operation.h
//  Myth
//
//  Created by Almas Adilbek on 11/30/12.
//
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface Operation : ModelObject {
    NSNumber *amount;
    NSNumber *contractId;
    NSNumber *operationID;
    NSString *description;
    NSNumber *providerId;
    NSDate *date;
}

@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSNumber *contractId;
@property (nonatomic, strong) NSNumber *operationID;
@property (nonatomic, strong) NSString *descriptionText;
@property (nonatomic, strong) NSNumber *providerId;
@property (nonatomic, strong) NSDate *date;

-(Operation *)initWithDatas:(NSDictionary *)datas;

@end
