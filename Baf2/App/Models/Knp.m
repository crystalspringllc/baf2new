//
//  Knp.m
//  Baf2
//
//  Created by Shyngys Kassymov on 03.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "Knp.h"

@implementation Knp

+ (Knp *)instanceFromDictionary:(NSDictionary *)aDictionary {
    Knp *instance = [[Knp alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.code = [self stringValueForKey:@"code"];
    self.name = [self stringValueForKey:@"name"];
    self.knpDescription = [self stringValueForKey:@"description"];
    self.categoryName = [self stringValueForKey:@"categoryName"];
    self.isLegal = [self boolValueForKey:@"isLegal"];
    
    objectData = nil;
}

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setCode:[self.code copyWithZone:zone]];
        [copy setName:[self.name copyWithZone:zone]];
        [copy setCategoryName:[self.categoryName copyWithZone:zone]];
        [copy setKnpDescription:[self.knpDescription copyWithZone:zone]];
    }
    
    return copy;
}


@end
