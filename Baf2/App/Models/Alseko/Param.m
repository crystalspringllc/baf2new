#import "Param.h"

@implementation Param

@synthesize parId;
@synthesize parName;
@synthesize parValue;

+ (Param *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Param *instance = [[Param alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.parId = [self numberValueForKey:@"parId"];
    self.parName = [self stringValueForKey:@"parName"];
    self.parValue = [self numberValueForKey:@"parValue"];

    objectData = nil;
}

- (NSDictionary *)representInDictionary {
    return @{
            @"parId" : self.parId,
            @"parName" : self.parName,
            @"parValue" : self.parValue
    };
}


@end
