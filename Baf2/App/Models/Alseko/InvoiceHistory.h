#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface InvoiceHistory : ModelObject {
    NSString *amount;
    NSString *date;
    NSString *invoiceDate;
    NSNumber *invoiceId;
    NSString *state;
}

@property (nonatomic, copy) NSString *amount;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *invoiceDate;
@property (nonatomic, copy) NSNumber *invoiceId;
@property (nonatomic, copy) NSString *state;

+ (InvoiceHistory *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;
- (NSString *)getInvoiceForMonthString;
- (NSString *)getStateMessage;

@end