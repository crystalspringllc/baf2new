#import "InvoiceHistory.h"
#import "NSString+Ext.h"
#import "NSDate+Ext.h"

@implementation InvoiceHistory

@synthesize amount;
@synthesize date;
@synthesize invoiceDate;
@synthesize invoiceId;
@synthesize state;

+ (InvoiceHistory *)instanceFromDictionary:(NSDictionary *)aDictionary {
    InvoiceHistory *instance = [[InvoiceHistory alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    amount = [self stringValueForKey:@"amount"];
    date = [self stringValueForKey:@"date"];
    invoiceDate = [self stringValueForKey:@"invoiceDate"];
    invoiceId = [self numberValueForKey:@"invoiceId"];
    state = [self stringValueForKey:@"state"];

    objectData = nil;
}

- (NSString *)getInvoiceForMonthString {
    NSDictionary *months = @{
            NSLocalizedString(@"january_lower", nil) : NSLocalizedString(@"january_upper", nil),
            NSLocalizedString(@"february_lower", nil) : NSLocalizedString(@"february_upper", nil),
            NSLocalizedString(@"martch_lower", nil) : NSLocalizedString(@"martch_upper", nil),
            NSLocalizedString(@"april_lower", nil) : NSLocalizedString(@"april_upper", nil),
            NSLocalizedString(@"may_lower", nil) : NSLocalizedString(@"may_upper", nil),
            NSLocalizedString(@"june_lower", nil) : NSLocalizedString(@"june_upper", nil),
            NSLocalizedString(@"july_lower", nil) : NSLocalizedString(@"july_upper", nil),
            NSLocalizedString(@"august_lower", nil) : NSLocalizedString(@"august_upper", nil),
            NSLocalizedString(@"september_lower", nil) : NSLocalizedString(@"september_upper", nil),
            NSLocalizedString(@"october_lower", nil) : NSLocalizedString(@"october_upper", nil),
            NSLocalizedString(@"november_lower", nil) : NSLocalizedString(@"november_upper", nil),
            NSLocalizedString(@"december_lower", nil) : NSLocalizedString(@"december_upper", nil)
    };
    NSDate *tempInvoiceDate = [self.invoiceDate dateWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *monthWithYear = [tempInvoiceDate stringWithDateFormat:@"MMMM yyyy" isCurrentLocale:YES];
    NSArray *tempArray = [monthWithYear componentsSeparatedByString:@" "];

    for (NSString *key in months) {
        if ([key isEqualToString:tempArray[0]]) {
            NSString *newmonth = months[key];
            return [NSString stringWithFormat:@"%@ %@", newmonth, tempArray[1]];
        }
    }
    return monthWithYear;
}

- (NSString *)getStateMessage {
    if ([self.state isEqualToString:@"expired"]) {
        return NSLocalizedString(@"expired", nil);
    } else if ([self.state isEqualToString:@"ready"]) {
        return NSLocalizedString(@"ready", nil);
    } else if ([self.state isEqualToString:@"inprogress"]) {
        return NSLocalizedString(@"inprogress", nil);
    } else if ([self.state isEqualToString:@"payed"]) {
        return NSLocalizedString(@"payed1", nil);
    } else if ([self.state isEqualToString:@"payed1"]) {
        return NSLocalizedString(@"payed1", nil);
    } else if ([self.state isEqualToString:@"payed2"]) {
        return NSLocalizedString(@"payed1", nil);
    } else if ([self.state isEqualToString:@"rejected"]) {
        return NSLocalizedString(@"rejected", nil);
    } else {
        return NSLocalizedString(@"unknown_status", nil);
    }
}

@end