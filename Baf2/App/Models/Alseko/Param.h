#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface Param : ModelObject {

    NSNumber *parId;
    NSString *parName;
    NSNumber *parValue;

}

@property (nonatomic, copy) NSNumber *parId;
@property (nonatomic, copy) NSString *parName;
@property (nonatomic, copy) NSNumber *parValue;

+ (Param *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;


- (NSDictionary *)representInDictionary;

@end
