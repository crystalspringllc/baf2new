#import <objc/runtime.h>
#import "Invoice.h"

#import "Param.h"
#import "Service.h"
#import "NSString+Ext.h"
#import "NSDateFormatter+Ext.h"
#import "NSDate+Ext.h"

@implementation Invoice

@synthesize account;
@synthesize address;
@synthesize expireDate;
@synthesize formedDate;
@synthesize invoiceIds;
@synthesize params;
@synthesize services;
@synthesize st;

+ (Invoice *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Invoice *instance = [[Invoice alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.account = [self stringValueForKey:@"account"];
    self.address = [self stringValueForKey:@"address"];
    self.expireDate = [[self stringValueForKey:@"expireDate"] dateWithDateFormat:@"yyyyMMddHHmmss"];
    self.formedDate = [[self stringValueForKey:@"formedDate"] dateWithDateFormat:@"yyyyMMddHHmmss"];
    self.st = [self numberValueForKey:@"st"];


    if ([objectData[@"invoiceIds"] isKindOfClass:[NSArray class]]) {
        NSArray *value = objectData[@"invoiceIds"];
        NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
        for (NSString *invoiceId in value) {
            [myMembers addObject:invoiceId];
        }
        self.invoiceIds = myMembers;
    }


    if ([objectData[@"params"] isKindOfClass:[NSArray class]]) {
        NSArray *value = objectData[@"params"];
        NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
        for (NSDictionary *valueMember in value) {
            Param *param = [Param instanceFromDictionary:valueMember];
            [myMembers addObject:param];
        }
        self.params = myMembers;
    }


    if ([objectData[@"services"] isKindOfClass:[NSArray class]]) {
        NSArray *value = objectData[@"services"];
        NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
        for (NSDictionary *valueMember in value) {
            Service *service = [Service instanceFromDictionary:valueMember];
            [myMembers addObject:service];
        }
        self.services = myMembers;
    }

    objectData = nil;
}

/**
*  Getting date strings
*/

- (NSString *)getExpireDateString {
    NSDateFormatter *df = [NSDateFormatter enUS];
    df.dateStyle = NSDateFormatterLongStyle;
    [df setTimeZone:[NSTimeZone localTimeZone]];
    return [df stringFromDate:self.expireDate];
}

- (NSString *)getLongFormedDateString {
    NSDateFormatter *df = [NSDateFormatter ruRu];
    df.dateStyle = NSDateFormatterLongStyle;
    [df setTimeZone:[NSTimeZone localTimeZone]];
    return [df stringFromDate:self.formedDate];
}

/**
*
*/

- (double)calcTotalSum {
    double totalSum = 0;
    for (Service *service in self.services) {
        if (service.includeInBill) {
            if (service.includeDebt) {
                totalSum += service.paySum.doubleValue;
            } else {
                if ([service.debtSumm doubleValue] > 0) {
                    totalSum += service.paySum.doubleValue + [service.debtSumm doubleValue];
                } else {
                    totalSum += service.paySum.doubleValue - ([service.debtSumm doubleValue] * -1);
                }
            }
        }
    }
    return totalSum;
}

- (Param *)getNumberOfLivingsParam {
    for (Param *param in self.params) {
        if ([param.parName isEqualToString:@"Количество проживающих"]) {
            return param;
        }
    }
    return nil;
}

- (NSDictionary *)repreentJSONWithServices:(NSArray *)servicesArray {
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];

    if (self.account) {
        dictionary[@"account"] = self.account;
    }

    if (self.address) {
        dictionary[@"address"] = self.address;
    }

    if (self.expireDate) {
        dictionary[@"expireDate"] = [self.expireDate stringWithDateFormat:@"yyyyMMddHHmmss"];
    }

    if (self.formedDate) {
        dictionary[@"formedDate"] = [self.formedDate stringWithDateFormat:@"yyyyMMddHHmmss"];
    }

    if (self.invoiceIds) {
        dictionary[@"invoiceIds"] = self.invoiceIds;
    }

    if (self.params) {
        NSMutableArray *tParams = [[NSMutableArray alloc] init];
        for (Param *param in self.params) {
            [tParams addObject:[param representInDictionary]];
        }
        dictionary[@"params"] = tParams;
    }

    if (self.st) {
        dictionary[@"st"] = self.st;
    }

    if (self.invoiceIds) {
        NSMutableArray *tServices = [[NSMutableArray alloc] init];
        for (Service *service in servicesArray) {
            [tServices addObject:[service representJSON]];
        }
        dictionary[@"services"] = tServices;
    }

    return dictionary;
}


@end
