#import <Foundation/Foundation.h>
#import "ModelObject.h"

@class Param;

@interface Invoice : ModelObject {

    NSString *account;
    NSString *address;
    NSDate *expireDate;
    NSDate *formedDate;
    NSArray *invoiceIds;
    NSArray *params;
    NSArray *services;
    NSNumber *st;

}

@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSDate *expireDate;
@property (nonatomic, copy) NSDate *formedDate;
@property (nonatomic, copy) NSNumber *st;
@property (nonatomic, copy) NSArray *invoiceIds;
@property (nonatomic, copy) NSArray *params;
@property (nonatomic, copy) NSArray *services;

+ (Invoice *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

/**
*  Getting date strings
*/

- (NSString *)getExpireDateString;
- (NSString *)getLongFormedDateString;

- (double)calcTotalSum;

- (Param *)getNumberOfLivingsParam;


#pragma mark -

- (NSDictionary *)repreentJSONWithServices:(NSArray *)servicesArray;


@end
