#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface ServiceTariff2 : ModelObject {

    NSNumber *maxTariffValue;
    NSNumber *middleTariffThreshold;
    NSNumber *middleTariffValue;
    NSNumber *minTariffThreshold;
    NSNumber *minTariffValue;

}

@property (nonatomic, copy) NSNumber *maxTariffValue;
@property (nonatomic, copy) NSNumber *middleTariffThreshold;
@property (nonatomic, copy) NSNumber *middleTariffValue;
@property (nonatomic, copy) NSNumber *minTariffThreshold;
@property (nonatomic, copy) NSNumber *minTariffValue;

+ (ServiceTariff2 *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;


#pragma mark -

- (NSDictionary *)representJSON;

@end
