#import <Foundation/Foundation.h>
#import "ModelObject.h"

@class ServiceTariff2;

@interface Service : ModelObject {

    BOOL autocalc;
    NSString *debtInfo;
    NSNumber *debtSumm;
    NSNumber *fixCount;
    NSNumber *fixSum;
    BOOL includeDebt;
    BOOL includeInBill;
    NSString *invoiceId;
    BOOL isCounterService;
    NSNumber *lastCount;
    NSDate *lastCountDate;
    NSString *measure;
    NSNumber *paySum;
    NSNumber *prevCount;
    NSDate *prevCountDate;
    NSNumber *serviceId;
    NSString *serviceName;
    NSNumber *tariff;
    ServiceTariff2 *tariff2;

}

@property (nonatomic, assign) BOOL autocalc;
@property (nonatomic, copy) NSString *debtInfo;
@property (nonatomic, copy, readonly) NSNumber *debtSumm;
@property (nonatomic, copy) NSNumber *fixCount;
@property (nonatomic, copy, readonly) NSNumber *fixSum;
@property (nonatomic, assign) BOOL includeDebt;
@property (nonatomic, assign) BOOL includeInBill;
@property (nonatomic, copy) NSString *invoiceId;
@property (nonatomic, assign) BOOL isCounterService;
@property (nonatomic, copy) NSNumber *lastCount;
@property (nonatomic, copy) NSString *measure;
@property (nonatomic, copy) NSNumber *paySum;
@property (nonatomic, copy) NSNumber *prevCount;
@property (nonatomic, copy) NSDate *lastCountDate;
@property (nonatomic, copy) NSDate *prevCountDate;
@property (nonatomic, copy) NSNumber *serviceId;
@property (nonatomic, copy) NSString *serviceName;
@property (nonatomic, copy) NSNumber *tariff;
@property (nonatomic, strong) ServiceTariff2 *tariff2;

+ (Service *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;
+ (double)calculateFixCountWithLastCount:(double)lastCount andPrevCount:(double)prevCount;
+ (double)calculateLastCountWithFixCount:(double)fixCount andPrevCount:(double)prevCount;
- (double)calcTotalSumWithFixCount:(double)realTimeFixCount numberLivings:(NSInteger)numberLivings lastDate:(NSDate *)lastDate threshholdValues:(void (^)(double min, double middle, double max, double minLim, double middleLim))threshholdValues;

#pragma mark -

- (NSDictionary *)representJSON;

@end
