#import "Service.h"
#import "ServiceTariff2.h"
#import "NSString+Ext.h"
#import "NSDate+Ext.h"
#import <DateTools.h>

@implementation Service

@synthesize autocalc;
@synthesize debtInfo;
@synthesize debtSumm;
@synthesize fixCount;
@synthesize fixSum;
@synthesize includeDebt;
@synthesize includeInBill;
@synthesize invoiceId;
@synthesize isCounterService;
@synthesize lastCount;
@synthesize lastCountDate;
@synthesize measure;
@synthesize paySum;
@synthesize prevCount;
@synthesize prevCountDate;
@synthesize serviceId;
@synthesize serviceName;
@synthesize tariff;
@synthesize tariff2;

+ (Service *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Service *instance = [[Service alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.autocalc  = [self boolValueForKey:@"autocalc"];
    self.debtInfo = [self stringValueForKey:@"debtInfo"];
    debtSumm = [self numberValueForKey:@"debtSumm"];
    fixCount  = [self numberValueForKey:@"fixCount"];
    fixSum = [self numberValueForKey:@"fixSum"];
    includeDebt = [self boolValueForKey:@"includeDebt"];
    self.includeInBill = [self boolValueForKey:@"includeInBill"];
    self.invoiceId = [self stringValueForKey:@"invoiceId"];
    self.isCounterService = [self boolValueForKey:@"IsCounterService"];
    lastCount  = [self numberValueForKey:@"lastCount"];
    self.measure = [self stringValueForKey:@"measure"];
    prevCount = [self numberValueForKey:@"prevCount"];
    paySum = [self numberValueForKey:@"paySum"];
    self.lastCountDate = [[self stringValueForKey:@"lastCountDate"] isEqualToString:@""] ? [NSDate date] : [[self stringValueForKey:@"lastCountDate"] dateWithDateFormat:@"yyyyMMddHHmmss"];
    self.prevCountDate = [[self stringValueForKey:@"prevCountDate"] isEqualToString:@""] ? [[NSDate date] dateBySubtractingDays:30] : [[self stringValueForKey:@"prevCountDate"] dateWithDateFormat:@"yyyyMMddHHmmss"];
    self.serviceId = [self numberValueForKey:@"serviceId"];
    self.serviceName = [self stringValueForKey:@"serviceName"];
    self.tariff = [self numberValueForKey:@"tariff"];


    if ([objectData[@"tariff2"] isKindOfClass:[NSDictionary class]] && [objectData[@"tariff2"] count] > 0) {
        self.tariff2 = [ServiceTariff2 instanceFromDictionary:objectData[@"tariff2"]];
    }

    objectData = nil;

}


#pragma mark - calculate counts

+ (double)calculateFixCountWithLastCount:(double)lastCount andPrevCount:(double)prevCount {
    return lastCount - prevCount;
}

+ (double)calculateLastCountWithFixCount:(double)fixCount andPrevCount:(double)prevCount {
    return fixCount + prevCount;
}

#pragma mark - calculate total service count

- (double)calcTotalSumWithFixCount:(double)realTimeFixCount numberLivings:(NSInteger)numberLivings lastDate:(NSDate *)lastDate threshholdValues:(void (^)(double min, double middle, double max, double minLim, double middleLim))threshholdValues {
    NSInteger daysBetweenLastAndPrevDates = [NSDate daysBetweenDate:self.prevCountDate andDate:lastDate];
    double minLim = ceil([self.tariff2.minTariffThreshold doubleValue] / 30.5 * (daysBetweenLastAndPrevDates * numberLivings));
    double middleLim = ceil([self.tariff2.middleTariffThreshold doubleValue] / 30.5 * (daysBetweenLastAndPrevDates * numberLivings));

    double minVal = [self.tariff2.minTariffValue doubleValue];
    double midVal = [self.tariff2.middleTariffValue doubleValue];
    double maxVal = [self.tariff2.maxTariffValue doubleValue];

    if (realTimeFixCount <= minLim) {
        threshholdValues(realTimeFixCount, 0, 0, minLim, middleLim);
        return realTimeFixCount * minVal;

    } else if (realTimeFixCount > minLim && realTimeFixCount <= middleLim) {

        double leftForMiddle = realTimeFixCount - minLim;
        double firstLevelSum = minLim * minVal;
        double secondLevelSum = leftForMiddle * midVal;
        threshholdValues(minLim, leftForMiddle, 0, minLim, middleLim);
        return secondLevelSum + firstLevelSum;

    } else if (realTimeFixCount >= middleLim) {

        double forMax = realTimeFixCount - middleLim;
        double forMiddle = realTimeFixCount - minLim - forMax;
        double forMin = minLim;
        threshholdValues(forMin, forMiddle, forMax, minLim, middleLim);
        return (forMin * minVal) + (forMiddle * midVal) + (forMax * maxVal);

    }
    return 0;
}



#pragma mark -

- (NSDictionary *)representJSON {

    NSMutableDictionary *dict = [@{
                @"autocalc" : @(self.autocalc),
                @"debtInfo" : self.debtInfo,
                @"debtSumm" : self.debtSumm,
                @"fixCount" : self.fixCount,
                @"fixSum" : self.fixSum,
                @"includeDebt" : @(self.includeDebt),
                @"includeInBill" : @(self.includeInBill),
                @"invoiceId" : self.invoiceId,
                @"IsCounterService" : @(self.isCounterService),
                @"lastCount" : self.lastCount,
                @"measure" : self.measure,
                @"prevCount" : self.prevCount,
                @"paySum" : self.paySum,
                @"lastCountDate" : [self.lastCountDate stringWithDateFormat:@"yyyyMMddHHmmss"],
                @"prevCountDate" : [self.prevCountDate stringWithDateFormat:@"yyyyMMddHHmmss"],
                @"serviceId" : self.serviceId,
                @"serviceName" : self.serviceName,
                @"tariff" : self.tariff
        } mutableCopy];

    if (self.tariff2) {
        [dict setValue:[self.tariff2 representJSON] forKey:@"tariff2"];
    }

    return [dict mutableCopy];
}

@end
