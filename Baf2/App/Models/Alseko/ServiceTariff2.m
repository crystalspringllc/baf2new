#import "ServiceTariff2.h"

@implementation ServiceTariff2

@synthesize maxTariffValue;
@synthesize middleTariffThreshold;
@synthesize middleTariffValue;
@synthesize minTariffThreshold;
@synthesize minTariffValue;

+ (ServiceTariff2 *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ServiceTariff2 *instance = [[ServiceTariff2 alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.maxTariffValue = [self numberValueForKey:@"maxTariffValue"];
    self.middleTariffThreshold = [self numberValueForKey:@"middleTariffThreshold"];
    self.middleTariffValue = [self numberValueForKey:@"middleTariffValue"];
    self.minTariffThreshold = [self numberValueForKey:@"minTariffThreshold"];
    self.minTariffValue = [self numberValueForKey:@"minTariffValue"];

    objectData = nil;

}


#pragma mark -

- (NSDictionary *)representJSON {
    return @{
              @"maxTariffValue" : self.maxTariffValue,
              @"middleTariffThreshold" : self.middleTariffThreshold,
              @"middleTariffValue" : self.middleTariffValue,
              @"minTariffThreshold" : self.minTariffThreshold,
              @"minTariffValue" : self.minTariffValue
    };
}


@end
