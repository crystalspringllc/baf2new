//
//  ModelObject.m
//  Myth
//
//  Created by Almas Adilbek on 11/13/12.
//
//

#import "ModelObject.h"

@implementation ModelObject

-(id)idValueForKey:(NSString *)key {
    return [self idValueFrom:objectData forKey:key];
}

-(NSString *)stringValueForKey:(NSString *)key {
    return [self stringValueFrom:objectData forKey:key];
}

-(NSNumber *)numberValueForKey:(NSString *)key {
    return [self numberValueFrom:objectData forKey:key];
}

-(NSNumber *)decimalNumberValueForKey:(NSString *)key {
    return [self decimalNumberValueFrom:objectData forKey:key];
}

-(NSDate *)dateValueForKey:(NSString *)key dateFormat:(NSString *)dateFormat {
    return [self dateValueFrom:objectData forKey:key dateFormat:dateFormat];
}

-(NSInteger)intValueForKey:(NSString *)key {
    return [self intValueFrom:objectData forKey:key];
}

-(CGFloat)floatValueForKey:(NSString *)key {
    return [self floatValueFrom:objectData forKey:key];
}

-(BOOL)boolValueForKey:(NSString *)key {
    return [objectData[key] boolValue];
}

-(NSArray *)arrayValueForKey:(NSString *)key {
    return [self arrayValueFrom:objectData forKey:key];
}

#pragma mark -

-(id)idValueFrom:(id)object forKey:(NSString *)key {
    id value = [object objectForKey:key];
    return [self isValid:value]?value:@"";
}

-(NSString *)stringValueFrom:(id)object forKey:(NSString *)key {
    id value = [object objectForKey:key];
    return [self isValid:value]?[NSString stringWithFormat:@"%@", value]:@"";
}

-(NSNumber *)numberValueFrom:(id)object forKey:(NSString *)key
{
    id apiValue = [object objectForKey:key];
    NSString *stringValue = [NSString stringWithFormat:@"%@", apiValue];

    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSNumber *value = [f numberFromString:stringValue];

    return [self isValid:value]?value: @0;
}

-(NSNumber *)decimalNumberValueFrom:(id)object forKey:(NSString *)key
{
    id apiValue = [object objectForKey:key];
    NSString *stringValue = [NSString stringWithFormat:@"%@", apiValue];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *value = [f numberFromString:stringValue];
    
    return [self isValid:value]?value: @0;
}

-(NSDate *)dateValueFrom:(id)object forKey:(NSString *)key dateFormat:(NSString *)dateFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:dateFormat];
    NSDate *date = [df dateFromString:[object objectForKey:key]];
    return date;
}

-(NSInteger)intValueFrom:(id)object forKey:(NSString *)key {
    id value = [object objectForKey:key];
    return [self isValid:value]?[value intValue]:0;
}

-(CGFloat)floatValueFrom:(id)object forKey:(NSString *)key {
    id value = [object objectForKey:key];
    return [self isValid:value]?[value floatValue]:0;
}

-(BOOL)boolValueFrom:(id)object forKey:(NSString *)key {
    return [[object objectForKey:key] boolValue];
}

-(NSArray *)arrayValueFrom:(id)object forKey:(NSString *)key {
    id value = [object objectForKey:key];
    return [self isValid:value]?value:@[];
}

#pragma mark -

-(BOOL)isValid:(id)value {
    return (value && ![value isEqual:[NSNull null]]);
}

#pragma mark -

-(void)traceParams {
    NSLog(@"Trance Params methods not implemented");
}

#pragma mark - conforming to NSCoding

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super init]) {

        objectData = [coder decodeObjectForKey:@"objectData"];

    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:objectData forKey:@"objectData"];
}

@end
