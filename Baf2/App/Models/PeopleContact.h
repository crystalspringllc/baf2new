//
//  PeopleContact.h
//  Baf2
//
//  Created by Shyngys Kassymov on 14.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PeopleContact : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSArray *phoneNumbers;

@property (nonatomic, readonly) NSString *fullName;

@end
