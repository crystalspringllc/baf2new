//
//  PaymentHistoryItem.h
//  Myth
//
//  Created by Almas Adilbek on 12/4/12.
//
//

#import "ModelObject.h"
#import "Contract.h"

@interface PaymentHistoryItem : ModelObject {
    NSNumber *amount;
    NSNumber *contractId;
    NSNumber *operationID;
    NSString *description;
    NSDate *date;
    Contract *contract;
}

@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSNumber *contractId;
@property (nonatomic, strong) NSNumber *operationID;
@property (nonatomic, strong) NSString *descriptionText;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) Contract *contract;

-(PaymentHistoryItem *)initWithDatas:(NSDictionary *)datas;

@end
