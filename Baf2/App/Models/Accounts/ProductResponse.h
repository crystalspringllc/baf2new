#import "ModelObject.h"

typedef NS_ENUM(NSUInteger, ProductType) {
    ProductTypeDeposit,
    ProductTypeCurrentAccount,
    ProductTypeLoan,
    ProductTypeBankCardAccount
};

@interface ProductResponse : ModelObject {

    NSArray *list;
    NSString *type;
    NSString *typeAlias;
    ProductType productType;

}

@property (nonatomic, copy) NSArray *list;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *typeAlias;
@property (nonatomic, assign) ProductType productType;

+ (ProductResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (instancetype)initWithCoder:(NSCoder *)coder;
- (void)encodeWithCoder:(NSCoder *)coder;


@end
