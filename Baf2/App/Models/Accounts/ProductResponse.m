#import "ProductResponse.h"

@implementation ProductResponse

@synthesize list, type, typeAlias, productType;

+ (ProductResponse *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ProductResponse *instance = [[ProductResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.type = [self stringValueFrom:aDictionary forKey:@"type"];
    self.typeAlias = [self stringValueFrom:aDictionary forKey:@"type-alias"];
}

#pragma mark - conforming to NSCoding

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super init]) {
        self.list = [coder decodeObjectForKey:@"list"];
        self.type = [coder decodeObjectForKey:@"type"];
        self.typeAlias = [coder decodeObjectForKey:@"typeAlias"];
        self.productType = (ProductType) [coder decodeIntegerForKey:@"productType"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:list forKey:@"list"];
    [coder encodeObject:type forKey:@"type"];
    [coder encodeObject:typeAlias forKey:@"typeAlias"];
    [coder encodeInteger:productType forKey:@"productType"];
}

@end