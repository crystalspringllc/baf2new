#import "BeneficiaryAccount.h"
#import "BankTransfersHelper.h"

@implementation BeneficiaryAccount

@synthesize added, alias, bank, card, clientId, firstName, iban, beneficiaryAccountId, iin, isLegal, lastName, name, panExtId, contractType, bankTitle;

+ (BeneficiaryAccount *)instanceFromDictionary:(NSDictionary *)aDictionary {

    BeneficiaryAccount *instance = [[BeneficiaryAccount alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.added = [self stringValueForKey:@"added"];
    self.alias = [self stringValueForKey:@"alias"];
    self.bank = [self stringValueForKey:@"bank"];
    self.bankTitle = [self stringValueForKey:@"bank_title"];
    self.card = [self stringValueForKey:@"card"];
    self.clientId = [self numberValueForKey:@"client_id"];
    self.firstName = [self stringValueForKey:@"first_name"];
    self.iban = [self stringValueForKey:@"iban"];
    self.beneficiaryAccountId = [self numberValueForKey:@"id"];
    self.iin = [self stringValueForKey:@"iin"];
    self.isLegal = [self boolValueForKey:@"is_legal"];
    self.lastName = [self stringValueForKey:@"last_name"];
    self.name = [self stringValueForKey:@"name"];
    self.panExtId = [self stringValueForKey:@"pan_ext_id"];
    self.contractType = [self stringValueForKey:@"contract_type"];
    self.currency = [self stringValueForKey:@"currency"];
    self.kbe = [self stringValueForKey:@"kbe"];

    objectData = nil;
}

- (BOOL)isCurrentAccount {
    return [[self.contractType lowercaseString] isEqual:@"current_individuals"];
}

- (BOOL)isDeposit {
    return [[self.contractType lowercaseString] isEqual:@"deposit_individuals"];
}

- (BOOL)isCard {
    return [[self.contractType lowercaseString] isEqual:@"card"];
}

- (BOOL)isResident {
    return [BankTransfersHelper isResidentWithKbe:self.kbe];
}

- (NSString *)fullAlias
{
    NSString *number;
    if([self isCard]) {
        number = self.card;
    } else {
        number = self.iban;
    }

    return [NSString stringWithFormat:@"%@ (%@)", self.alias, number];
}

- (NSString *)aliasWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", self.alias, self.currency];
}

- (NSString *)aliasWithCurrencyAndIBAN {
    return [NSString stringWithFormat:@"%@ %@\n%@", self.alias, self.currency, self.iban];
}

- (NSString *)fullAliasWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", [self fullAlias], self.currency];
}

- (NSString *)economySector {
    return [BankTransfersHelper economySectorWithKbe:self.kbe];
}

- (NSString *)residentTitle {
    return [self isResident] ? @"Резидент": @"Не резидент";
}

@end