#import "ModelObject.h"


@interface BeneficiaryAccount : ModelObject {

}

@property (nonatomic, copy) NSString *added;
@property (nonatomic, copy) NSString *alias;
@property (nonatomic, copy) NSString *bank;
@property (nonatomic, copy) NSString *bankTitle;
@property (nonatomic, copy) NSString *card;
@property (nonatomic, copy) NSNumber *clientId;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *iban;
@property (nonatomic, copy) NSNumber *beneficiaryAccountId;
@property (nonatomic, copy) NSString *iin;
@property (nonatomic, assign) BOOL isLegal;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *panExtId;
@property (nonatomic, copy) NSString *contractType;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, copy) NSString *kbe;

+ (BeneficiaryAccount *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (BOOL)isCurrentAccount;
- (BOOL)isDeposit;
- (BOOL)isCard;

- (BOOL)isResident;

- (NSString *)fullAlias;
- (NSString *)aliasWithCurrency;
- (NSString *)aliasWithCurrencyAndIBAN;

- (NSString *)fullAliasWithCurrency;
- (NSString *)economySector;
- (NSString *)residentTitle;

@end
