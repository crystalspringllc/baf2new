#import "ModelObject.h"

@interface OtherBankCard : ModelObject {

    NSString *alias;
    BOOL alphaApproved;
    NSString *bank;
    NSString *cardBin;
    NSString *cardProcessing;
    NSString *cardType;
    NSString *dateStatus;
    BOOL deleted;
    NSString *descr;
    NSString *dtApproved;
    NSString *dtDeleted;
    NSString *dtEpayAttempt;
    BOOL epayApproved;
    NSString *eventId;
    NSString *expMonth;
    NSString *expYear;
    NSString *registeredCardId;
    NSString *logo;
    BOOL mainCard;
    NSString *mask;
    NSString *ref;
    NSString *registered;
    NSString *status;
}

@property (nonatomic, copy) NSString *alias;
@property (nonatomic, assign) BOOL alphaApproved;
@property (nonatomic, copy) NSString *bank;
@property (nonatomic, copy) NSString *cardBin;
@property (nonatomic, copy) NSString *cardProcessing;
@property (nonatomic, copy) NSString *cardType;
@property (nonatomic, copy) NSString *dateStatus;
@property (nonatomic, assign) BOOL deleted;
@property (nonatomic, copy) NSString *descr;
@property (nonatomic, copy) NSString *dtApproved;
@property (nonatomic, copy) NSString *dtDeleted;
@property (nonatomic, copy) NSString *dtEpayAttempt;
@property (nonatomic, assign) BOOL epayApproved;
@property (nonatomic, assign) BOOL isEpayCardRegistered;
@property (nonatomic, copy) NSString *eventId;
@property (nonatomic, copy) NSString *expMonth;
@property (nonatomic, copy) NSString *expYear;
@property (nonatomic, copy) NSString *registeredCardId;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, assign) BOOL mainCard;
@property (nonatomic, copy) NSString *mask;
@property (nonatomic, copy) NSString *ref;
@property (nonatomic, copy) NSString *registered;
@property (nonatomic, copy) NSString *status;

+ (OtherBankCard *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

-(BOOL)isCardProcessingKbb;
-(BOOL)isCardExpired;

@end
