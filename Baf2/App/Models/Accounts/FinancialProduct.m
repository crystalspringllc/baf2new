#import "FinancialProduct.h"
#import "NSNumber+Ext.h"

@implementation FinancialProduct

@synthesize accountId, alias, balance, clientId, currency, currencyCode, deal, deleted, hold, iban, isFavorite,
lastUpdate, opened, product, productAlias, status, statusName, systemId, type, typeAlias, warningStatus, statusDescr,
reqType;

+ (FinancialProduct *)instanceFromDictionary:(NSDictionary *)aDictionary {

    FinancialProduct *instance = [[FinancialProduct alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.accountId = [self numberValueForKey:@"id"];
    self.alias = [self stringValueForKey:@"alias"];
    self.balance = [self numberValueForKey:@"balance"];
    self.clientId = [self numberValueForKey:@"clientId"];
    self.currency = [self stringValueForKey:@"currency"];
    self.currencyCode = [self stringValueForKey:@"currencyCode"];
    self.deal = [self stringValueForKey:@"deal"];
    self.deleted = [self numberValueForKey:@"deleted"];
    self.hold = [self stringValueForKey:@"hold"];
    self.iban = [self stringValueForKey:@"iban"];
    self.isFavorite = [[self stringValueForKey:@"isFavorite"] boolValue];
    self.lastUpdate = [self stringValueForKey:@"lastUpdate"];
    self.opened = [self stringValueForKey:@"opened"];
    self.product = [self stringValueForKey:@"product"];
    self.productAlias = [self stringValueForKey:@"productAlias"];
    self.status = [self stringValueForKey:@"status"];
    self.statusName = [self stringValueForKey:@"statusName"];
    self.systemId = [self numberValueForKey:@"systemId"];
    self.type = [self stringValueForKey:@"type"];
    self.typeAlias = [self stringValueForKey:@"typeAlias"];
    self.systemId = [self numberValueForKey:@"system_id"];
    self.reqType = [self stringValueForKey:@"req_type"];

    //TAGS
    self.warningStatus = [self boolValueForKey:@"warning_status"];
    self.statusDescr = [self stringValueForKey:@"status_descr_1"];

    objectData = nil;
}

- (NSString *)balanceWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", [self.balance decimalFormatString], self.currency];
}

- (NSString *)ibanWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", self.iban, self.currency];
}

- (NSString *)dealWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", self.deal, self.currency];
}

#pragma mark -
#pragma mark Date

- (NSDate *)lastUpdatedDate {
    return [self dateWithParam:self.lastUpdate];
}

-(NSDate *)dateWithParam:(NSString *)param {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"M dd, yyyy HH:mm:ss a"];
    return [df dateFromString:param];
}

- (NSString *)lastUpdatedShortDate {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd.MM.yyyy"];
    return [df stringFromDate:[self lastUpdatedDate]];
}

- (NSString *)lastUpdatedDateWithTime {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd.MM.yyyy HH:mm"];
    return [df stringFromDate:[self lastUpdatedDate]];
}

- (NSString *)openedShortDate {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd.MM.yyyy"];
    return [df stringFromDate:[self dateWithParam:self.opened]];
}

- (BOOL)isCurrentAccount {
    return [[self.reqType lowercaseString] isEqualToString:[@"CURRENT_INDIVIDUALS" lowercaseString]];
}

- (BOOL)isDeposit {
    return [[self.reqType lowercaseString] isEqualToString:[@"DEPOSIT_INDIVIDUALS" lowercaseString]];
}


#pragma mark - confirm to NSCoding

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super init]) {

        accountId = [coder decodeObjectForKey:@"accountId"];
        alias = [coder decodeObjectForKey:@"alias"];
        balance = [coder decodeObjectForKey:@"balance"];
        clientId = [coder decodeObjectForKey:@"clientId"];
        currency = [coder decodeObjectForKey:@"currency"];
        currencyCode = [coder decodeObjectForKey:@"currencyCode"];
        deal = [coder decodeObjectForKey:@"deal"];
        deleted = [coder decodeObjectForKey:@"deleted"];
        hold = [coder decodeObjectForKey:@"hold"];
        iban = [coder decodeObjectForKey:@"iban"];
        isFavorite = [coder decodeBoolForKey:@"isFavorite"];
        lastUpdate = [coder decodeObjectForKey:@"lastUpdate"];
        opened = [coder decodeObjectForKey:@"opened"];
        product = [coder decodeObjectForKey:@"product"];
        productAlias = [coder decodeObjectForKey:@"productAlias"];
        status = [coder decodeObjectForKey:@"status"];
        statusName = [coder decodeObjectForKey:@"statusName"];
        systemId = [coder decodeObjectForKey:@"systemId"];
        type = [coder decodeObjectForKey:@"type"];
        typeAlias = [coder decodeObjectForKey:@"typeAlias"];
        reqType = [coder decodeObjectForKey:@"reqType"];
        warningStatus = [coder decodeBoolForKey:@"warningStatus"];
        statusDescr = [coder decodeObjectForKey:@"statusDescr"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:accountId forKey:@"accountId"];
    [coder encodeObject:alias forKey:@"alias"];
    [coder encodeObject:balance forKey:@"balance"];
    [coder encodeObject:clientId forKey:@"clientId"];
    [coder encodeObject:currency forKey:@"currency"];
    [coder encodeObject:currencyCode forKey:@"currencyCode"];
    [coder encodeObject:deal forKey:@"deal"];
    [coder encodeObject:deleted forKey:@"deleted"];
    [coder encodeObject:hold forKey:@"hold"];
    [coder encodeObject:iban forKey:@"iban"];
    [coder encodeBool:isFavorite forKey:@"isFavorite"];
    [coder encodeObject:lastUpdate forKey:@"lastUpdate"];
    [coder encodeObject:opened forKey:@"opened"];
    [coder encodeObject:product forKey:@"product"];
    [coder encodeObject:productAlias forKey:@"productAlias"];
    [coder encodeObject:status forKey:@"status"];
    [coder encodeObject:statusName forKey:@"statusName"];
    [coder encodeObject:systemId forKey:@"systemId"];
    [coder encodeObject:type forKey:@"type"];
    [coder encodeObject:typeAlias forKey:@"typeAlias"];
    [coder encodeObject:reqType forKey:@"reqType"];
    [coder encodeBool:warningStatus forKey:@"warningStatus"];
    [coder encodeObject:statusDescr forKey:@"statusDescr"];
}

@end