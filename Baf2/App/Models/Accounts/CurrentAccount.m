#import "CurrentAccount.h"
#import "NSNumber+Ext.h"

@implementation CurrentAccount

@synthesize filial, filialAlias;

+ (CurrentAccount *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CurrentAccount *instance = [[CurrentAccount alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    [super setAttributesFromDictionary:aDictionary];

    objectData = aDictionary;

    self.filial = [self stringValueForKey:@"filial"];
    self.filialAlias = [self stringValueForKey:@"filial_alias"];

    objectData = nil;
}

#pragma mark - confirm to NSCoding

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        self.filial = [coder decodeObjectForKey:@"filial"];
        self.filialAlias = [coder decodeObjectForKey:@"filialAlias"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [super encodeWithCoder:coder];
    [coder encodeObject:filial forKey:@"filial"];
    [coder encodeObject:filialAlias forKey:@"filialAlias"];
}

@end