#import "ModelObject.h"


@interface FinancialProduct : ModelObject {

}

@property (nonatomic, copy) NSNumber *accountId;
@property (nonatomic, copy) NSString *alias;
@property (nonatomic, copy) NSNumber *balance;
@property (nonatomic, copy) NSNumber *clientId;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, copy) NSString *currencyCode;
@property (nonatomic, copy) NSString *deal;
@property (nonatomic, copy) NSNumber *deleted;
@property (nonatomic, copy) NSString *hold;
@property (nonatomic, copy) NSString *iban;
@property (nonatomic, assign) BOOL isFavorite;
@property (nonatomic, copy) NSString *lastUpdate;
@property (nonatomic, copy) NSString *opened;
@property (nonatomic, copy) NSString *product;
@property (nonatomic, copy) NSString *productAlias;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *statusName;
@property (nonatomic, copy) NSNumber *systemId;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *typeAlias;
@property (nonatomic, copy) NSString *reqType;

//TAGS
@property (nonatomic, assign) BOOL warningStatus;
@property (nonatomic, strong) NSString *statusDescr;



+ (FinancialProduct *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)balanceWithCurrency;
- (NSString *)ibanWithCurrency;
- (NSString *)dealWithCurrency;

- (NSDate *)lastUpdatedDate;

- (NSDate *)dateWithParam:(NSString *)param;

- (NSString *)lastUpdatedShortDate;
- (NSString *)lastUpdatedDateWithTime;

- (NSString *)openedShortDate;

- (BOOL)isCurrentAccount;
- (BOOL)isDeposit;

- (instancetype)initWithCoder:(NSCoder *)coder;
- (void)encodeWithCoder:(NSCoder *)coder;

@end