//
//  IdTitleObject.m
//  WashMe
//
//  Created by Almas Adilbek on 10/22/14.
//  Copyright (c) 2014 Almas Adilbek. All rights reserved.
//

#import "IdTitleObject.h"

@implementation IdTitleObject

+ (IdTitleObject *)instanceWithId:(id)objId title:(NSString *)objectTitle
{
    return [self instanceWithId:objId title:objectTitle data:nil];
}

+ (IdTitleObject *)instanceWithId:(id)objId title:(NSString *)objectTitle data:(id)data_
{
    IdTitleObject *obj = [[IdTitleObject alloc] init];
    obj.objectId = objId;
    obj.title = objectTitle;
    if(data_) obj.data = data_;
    return obj;
}


+ (IdTitleObject *)instanceFromArray:(NSArray *)array
{
    IdTitleObject *instance = [[IdTitleObject alloc] init];
    instance.objectId = array[0];
    instance.title = array[1];
    return instance;
}


@end
