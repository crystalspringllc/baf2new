//
//  IdTitleObject.h
//  WashMe
//
//  Created by Almas Adilbek on 10/22/14.
//  Copyright (c) 2014 Almas Adilbek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IdTitleObject : NSObject

@property(nonatomic, strong) id objectId;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, strong) id data;

+ (IdTitleObject *)instanceWithId:(id)objId title:(NSString *)objectTitle;
+ (IdTitleObject *)instanceWithId:(id)objId title:(NSString *)objectTitle data:(id)data_;
+ (IdTitleObject *)instanceFromArray:(NSArray *)array;

@end
