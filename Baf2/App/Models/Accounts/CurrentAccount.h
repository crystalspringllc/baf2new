#import "ModelObject.h"
#import "FinancialProduct.h"


@interface CurrentAccount : FinancialProduct {
    NSString *filial;
    NSString *filialAlias;
    NSNumber *depositAccountId;
}

@property (nonatomic, copy) NSString *filial;
@property (nonatomic, copy) NSString *filialAlias;

+ (CurrentAccount *)instanceFromDictionary:(NSDictionary *)aDictionary;

- (instancetype)initWithCoder:(NSCoder *)coder;
- (void)encodeWithCoder:(NSCoder *)coder;

@end
