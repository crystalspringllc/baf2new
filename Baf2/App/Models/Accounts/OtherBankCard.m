#import "OtherBankCard.h"
#import "CardsApi.h"

@implementation OtherBankCard

@synthesize alias, alphaApproved, bank, cardBin, cardProcessing, cardType, dateStatus, deleted, descr, dtApproved, dtDeleted, dtEpayAttempt, epayApproved, eventId, expMonth, expYear, registeredCardId, logo, mainCard, mask, ref, registered, status;

+ (OtherBankCard *)instanceFromDictionary:(NSDictionary *)aDictionary {

    OtherBankCard *instance = [[OtherBankCard alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;

    self.alias = [self stringValueForKey:@"alias"];
    self.alphaApproved = [self boolValueForKey:@"alpha_approved"];
    self.bank = [self stringValueForKey:@"bank"];
    self.cardBin = [self stringValueForKey:@"card_bin"];
    self.cardProcessing = [self stringValueForKey:@"card_processing"];
    self.cardType = [self stringValueForKey:@"card_type"];
    self.dateStatus = [self stringValueForKey:@"dateStatus"];
    self.deleted = [self boolValueForKey:@"deleted"];
    self.descr = [self stringValueForKey:@"descr"];
    self.dtApproved = [self stringValueForKey:@"dt_approved"];
    self.dtDeleted = [self stringValueForKey:@"dt_deleted"];
    self.dtEpayAttempt = [self stringValueForKey:@"dt_epay_attempt"];
    self.isEpayCardRegistered = [self boolValueForKey:@"epay_approved"];
    self.epayApproved = [self boolValueForKey:@"epay_approved"];
    self.eventId = [self stringValueForKey:@"event_id"];
    self.expMonth = [self stringValueForKey:@"exp_month"];
    self.expYear = [self stringValueForKey:@"exp_year"];
    self.registeredCardId = [self stringValueForKey:@"id"];
    self.logo = [self stringValueForKey:@"logo"];
    self.mainCard = [self boolValueForKey:@"main_card"];
    self.mask = [self stringValueForKey:@"mask"];
    self.ref = [self stringValueForKey:@"ref"];
    self.registered = [self stringValueForKey:@"registered"];
    self.status = [self stringValueForKey:@"status"];

    objectData = nil;
}

@end
