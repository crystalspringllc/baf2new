#import "NewsInfo.h"

@implementation NewsInfo
- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    [encoder encodeObject:self.date forKey:@"date"];
    [encoder encodeObject:self.message forKey:@"message"];
    [encoder encodeObject:self.subject forKey:@"subject"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super initWithCoder:decoder])) {
        self.date = [decoder decodeObjectForKey:@"date"];
        self.message = [decoder decodeObjectForKey:@"message"];
        self.subject = [decoder decodeObjectForKey:@"subject"];
    }
    return self;
}

+ (NewsInfo *)instanceFromDictionary:(NSDictionary *)aDictionary {

    NewsInfo *instance = [[NewsInfo alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.date    = [self stringValueForKey:@"date"];
    self.message = [self stringValueForKey:@"message"];
    self.subject = [self stringValueForKey:@"subject"];
    
    objectData = nil;
    
}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.date) {
        [dictionary setObject:self.date forKey:@"date"];
    }

    if (self.message) {
        [dictionary setObject:self.message forKey:@"message"];
    }

    if (self.subject) {
        [dictionary setObject:self.subject forKey:@"subject"];
    }

    return dictionary;

}


@end
