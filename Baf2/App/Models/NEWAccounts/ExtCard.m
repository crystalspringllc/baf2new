#import "ExtCard.h"
#import "Constants.h"

@implementation ExtCard

+ (ExtCard *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ExtCard *instance = [[ExtCard alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    [super setAttributesFromDictionary:aDictionary];

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.alphaApproved = [self boolValueForKey:@"alphaApproved"];
    self.bank = [self stringValueForKey:@"bank"];
    self.cardType = [self stringValueForKey:@"cardType"];
    self.dateStatus = [self stringValueForKey:@"dateStatus"];
    self.deleted = [self boolValueForKey:@"deleted"];
    self.descriptionText = [self stringValueForKey:@"description"];
    self.epayApproved = [self boolValueForKey:@"epayApproved"];
    self.isMain = [self boolValueForKey:@"isMain"];
    self.processing = [self stringValueForKey:@"processing"];
    self.registeredDate = [self stringValueForKey:@"registeredDate"];
    self.dtDeleted = [self stringValueForKey:@"dtDeleted"];
    self.cardBin = [self stringValueForKey:@"cardBin"];
    self.cardHash = [self stringValueForKey:@"cardHash"];
    self.expMonth = [self stringValueForKey:@"expMonth"];
    self.expYear = [self stringValueForKey:@"expYear"];
    self.eventId = [self numberValueForKey:@"eventId"];
    self.epayCardId = [self stringValueForKey:@"epayCardId"];
    self.epayAttemptDate = [self stringValueForKey:@"epayAttemptDate"];
    self.approvedDate = [self stringValueForKey:@"approvedDate"];
    self.mask = [self stringValueForKey:@"mask"];
    self.reference = [self stringValueForKey:@"reference"];

    objectData = nil;
}

#pragma mark - model logic

- (NSString *)getMaskOrEmpty {
    if (self.mask && self.mask.length > 0) {
        return self.mask;
    } else {
        return @"XXXX-XXXX-XXXX-XXXX";
    }
}

- (NSString *)getDateStatusText {
    if ([self.dateStatus isEqualToString:@"card_exp_date_ok"]) {
        return @"";
    } else if ([self.dateStatus isEqualToString:@"card_expired"]) {
        return @"Срок действия карты истек";
    } else if ([self.dateStatus isEqualToString:@"card_expire_soon"]) {
        return @"Срок действия карты истекает в скором времени";
    }
    return nil;
}

- (BOOL)isCardExpired {
    if ([self.dateStatus isEqualToString:@"card_expired"]) {
        return YES;
    } else {
        return NO;
    }
}

- (NSString *)logo {
    return [NSString stringWithFormat:@"%@banks/%@.png", BASE_IMAGE_URL, self.bank.lowercaseString];
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setBank:[self.bank copyWithZone:zone]];
        [copy setCardType:[self.cardType copyWithZone:zone]];
        [copy setDateStatus:[self.dateStatus copyWithZone:zone]];
        [copy setDescriptionText:[self.descriptionText copyWithZone:zone]];
        [copy setProcessing:[self.processing copyWithZone:zone]];
        [copy setRegisteredDate:[self.registeredDate copyWithZone:zone]];
        [copy setDtDeleted:[self.dtDeleted copyWithZone:zone]];
        [copy setCardBin:[self.cardBin copyWithZone:zone]];
        [copy setCardHash:[self.cardHash copyWithZone:zone]];
        [copy setExpMonth:[self.expMonth copyWithZone:zone]];
        [copy setExpYear:[self.expYear copyWithZone:zone]];
        [copy setEventId:[self.eventId copyWithZone:zone]];
        [copy setEpayCardId:[self.epayCardId copyWithZone:zone]];
        [copy setEpayAttemptDate:[self.epayAttemptDate copyWithZone:zone]];
        [copy setApprovedDate:[self.approvedDate copyWithZone:zone]];
        [copy setMask:[self.mask copyWithZone:zone]];
        [copy setReference:[self.reference copyWithZone:zone]];
        
        // Set primitives
        [copy setAlphaApproved:self.alphaApproved];
        [copy setDeleted:self.deleted];
        [copy setEpayApproved:self.epayApproved];
    }
    
    return copy;
}

@end