//
// Created by Askar Mustafin on 6/3/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "AccountStatementsResponse.h"
#import "Statement.h"


@implementation AccountStatementsResponse {}

+ (instancetype)instanceFromDictionary:(NSDictionary *)aDictionary {
    AccountStatementsResponse *instance = [[AccountStatementsResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.fromDate = [self stringValueForKey:@"fromDate"];
    self.toDate = [self stringValueForKey:@"toDate"];

    NSArray *statementObjectData = objectData[@"statements"];
    NSMutableArray *statementObjects = [[NSMutableArray alloc] init];
    for (NSDictionary *statementDict in statementObjectData) {
        [statementObjects addObject:[Statement instanceFromDictionary:statementDict]];
    }
    self.statements = statementObjects;


    NSArray *blockStatementsObjectData = objectData[@"blockStatements"];
    NSMutableArray *blockStatementObjects = [[NSMutableArray alloc] init];
    for (NSDictionary *blockStatementDict in blockStatementsObjectData) {
        [blockStatementObjects addObject:[Statement instanceFromDictionary:blockStatementDict]];
    }
    self.blockStatements = blockStatementObjects;


    NSArray *aviaStatementsbjectData = objectData[@"aviaStatements"];
    NSMutableArray *aviaStatementObjects = [[NSMutableArray alloc] init];
    for (NSDictionary *aviaStatementDict in aviaStatementsbjectData) {
        [aviaStatementObjects addObject:[Statement instanceFromDictionary:aviaStatementDict]];
    }
    self.aviaStatements = aviaStatementObjects;
    
    self.cashBack = [self numberValueForKey:@"cashBack"];
    self.cashBackCurrency = [self stringValueForKey:@"cashBackCurrency"];
}

@end
