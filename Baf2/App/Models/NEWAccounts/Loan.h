#import <Foundation/Foundation.h>
#import "Account.h"

@interface Loan : Account <NSCopying>

@property (nonatomic, copy) NSNumber *loanId;
@property (nonatomic, copy) NSNumber *linkedAccountId;
@property (nonatomic, copy) NSString *linkedAccountType;
@property (nonatomic, copy) NSString *linkedAccountAlias;
@property (nonatomic, copy) NSString *rate;
@property (nonatomic, copy) NSNumber *periodMonth;
@property (nonatomic, copy) NSNumber *totalPrincipal;
@property (nonatomic, copy) NSNumber *totalInterest;
@property (nonatomic, copy) NSString *planDate;
@property (nonatomic, copy) NSNumber *planInterest;
@property (nonatomic, copy) NSNumber *planPrincipal;
@property (nonatomic, copy) NSNumber *planAmount;
@property (nonatomic, copy) NSNumber *fineInterest;
@property (nonatomic, copy) NSNumber *finePrincipal;
@property (nonatomic, copy) NSNumber *overdueInterest;
@property (nonatomic, copy) NSNumber *overduePrincipal;
@property (nonatomic, copy) NSNumber *overdueOverinterest;

+ (Loan *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)productAliasWithPrincipal;
- (CGFloat)repaymentAmount;
- (NSString *)repaymentAmountWithCurrency;
- (NSString *)debtAmountWithCurrency;

- (NSNumber *)accountId;

@end
