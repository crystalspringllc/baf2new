//
// Created by Askar Mustafin on 5/31/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "Account.h"
#import "CheckAndInitTransferResponse.h"
#import "OperationAccounts.h"

@implementation Account {

}

+ (Account *)instanceFromDictionary:(NSDictionary *)aDictionary {
    Account *instance = [[Account alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.accountId = [self numberValueForKey:@"id"];
    self.number = [self stringValueForKey:@"number"];
    self.key = [self stringValueForKey:@"key"];
    self.iban = [self stringValueForKey:@"iban"];
    self.alias = [self stringValueForKey:@"alias"];
    self.type = [self stringValueForKey:@"type"];
    self.balance = [self numberValueForKey:@"balance"];
    self.balanceKzt = [self numberValueForKey:@"balanceKzt"]; // в карташке КЗТ вот стока есть
    self.balanceUsd = [self numberValueForKey:@"balanceUsd"];
    self.balanceEur = [self numberValueForKey:@"balanceEur"];
    self.balanceRub = [self numberValueForKey:@"balanceRub"];
    self.balanceGbp = [self numberValueForKey:@"balanceGbp"];
    self.availableInGbp = [self numberValueForKey:@"availableInGbp"]; //в эквивалетне всего всех кармашков!!!
    self.availableInUsd = [self numberValueForKey:@"availableInUsd"];
    self.availableInEur = [self numberValueForKey:@"availableInEur"];
    self.availableInRub = [self numberValueForKey:@"availableInRub"];
    self.block = [self numberValueForKey:@"block"];
    self.currency = [self stringValueForKey:@"currency"];
    self.status = [self stringValueForKey:@"status"];
    self.statusDescr = [[self stringValueForKey:@"statusDescr"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    self.product = [self stringValueForKey:@"product"];
    self.filial = [self stringValueForKey:@"filial"];
    self.isMulty = [self boolValueForKey:@"isMulty"];
    self.isWarning = [self boolValueForKey:@"isWarning"];
    self.isFavorite = [self boolValueForKey:@"isFavorite"];
    self.isOwner = [self boolValueForKey:@"isOwner"];
    self.isLegal = [self boolValueForKey:@"isLegal"];
    self.isIslamic = [self boolValueForKey:@"isIslamic"];
    self.cashbackOn = [self boolValueForKey:@"cashbackOn"];
    self.updated = [self stringValueForKey:@"updated"];
    self.opened = [self stringValueForKey:@"opened"];
    self.closed = [self stringValueForKey:@"closed"];
    self.endDate = [self stringValueForKey:@"endDate"];
    
    self.cardType = [self stringValueForKey:@"cardType"];
    self.bank = [self stringValueForKey:@"bank"];
    self.bankLogo = [self stringValueForKey:@"bankLogo"];

    objectData = nil;
}

#pragma makr - helper

- (NSString *)getBalanceWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", [self.balance decimalFormatString], self.currency];
}

- (AccountType)accountType {
    if ([self.type isEqualToString:@"account"]) {
        return AccountTypeAccount;
    } else if ([self.type isEqualToString:@"card"]) {
        return AccountTypeCard;
    } else if ([self.type isEqualToString:@"deposit_individuals"]) {
        return AccountTypeDeposit;
    } else if ([self.type isEqualToString:@"loan"]) {
        return AccountTypeLoan;
    } else if ([self.type isEqualToString:@"current_individuals"]) {
        return AccountTypeCurrent;
    } else if ([self.type isEqualToString:@"benificiary"]) {
        return AccountTypeBeneficiary;
    }
    
    return AccountTypeAccount;
}

+ (NSString *)accountTypeToString:(AccountType)accountType {
    NSString *accountTypeToString = @"";
    
    switch (accountType) {
        case AccountTypeAccount:
            accountTypeToString = @"account";
            break;
        case AccountTypeCard:
            accountTypeToString = @"card";
            break;
        case AccountTypeDeposit:
            accountTypeToString = @"deposit";
            break;
        case AccountTypeLoan:
            accountTypeToString = @"loan";
            break;
        case AccountTypeCurrent:
            accountTypeToString = @"current_individuals";
            break;
        case AccountTypeBeneficiary:
            accountTypeToString = @"benificiary";
            break;
        default:
            accountTypeToString = @"account";
            break;
    }
    
    return accountTypeToString;
}

- (NSString *)logo {
    if (self.bankLogo && self.bankLogo.length > 0) {
        return [NSString stringWithFormat:@"%@banks/%@.png", BASE_IMAGE_URL, self.bankLogo.lowercaseString];
    } else if (self.bank && self.bank.length > 0) {
        return [NSString stringWithFormat:@"%@banks/%@.png", BASE_IMAGE_URL, self.bank.lowercaseString];
    } else if (self.cardType && self.cardType.length > 0) {
        return [NSString stringWithFormat:@"%@cards/small/%@.png", BASE_IMAGE_URL, self.cardType.lowercaseString];
    }
    
    return @"";
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setAccountId:[self.accountId copyWithZone:zone]];
        [copy setKey:[self.key copyWithZone:zone]];
        [copy setIban:[self.iban copyWithZone:zone]];
        [copy setAlias:[self.alias copyWithZone:zone]];
        [copy setType:[self.type copyWithZone:zone]];
        [copy setBalance:[self.balance copyWithZone:zone]];
        [copy setBalanceKzt:[self.balanceKzt copyWithZone:zone]];
        [copy setBalanceUsd:[self.balanceUsd copyWithZone:zone]];
        [copy setBalanceEur:[self.balanceEur copyWithZone:zone]];
        [copy setBalanceRub:[self.balanceRub copyWithZone:zone]];
        [copy setBlock:[self.block copyWithZone:zone]];
        [copy setCurrency:[self.currency copyWithZone:zone]];
        [copy setStatus:[self.status copyWithZone:zone]];
        [copy setStatusDescr:[self.statusDescr copyWithZone:zone]];
        [copy setProduct:[self.product copyWithZone:zone]];
        [copy setFilial:[self.filial copyWithZone:zone]];
        [copy setUpdated:[self.updated copyWithZone:zone]];
        [copy setOpened:[self.opened copyWithZone:zone]];
        [copy setClosed:[self.closed copyWithZone:zone]];
        [copy setEndDate:[self.endDate copyWithZone:zone]];
        [copy setCardType:[self.cardType copyWithZone:zone]];
        [copy setBank:[self.bank copyWithZone:zone]];
        [copy setBankLogo:[self.bankLogo copyWithZone:zone]];
        
        // Set primitives
        [copy setIsMulty:self.isMulty];
        [copy setIsMulty:self.isWarning];
        [copy setIsMulty:self.isFavorite];
        [copy setIsMulty:self.isOwner];
        [copy setIsMulty:self.isLegal];
    }
    
    return copy;
}

- (BOOL)isBlockedIBFL {
    if ([self.status isEqualToString:@"Card do not honor IBFL"]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isBlocked {
    if ([self.status isEqualToString:@"Card Do not honor"]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isBlockStole {
    if ([self.status isEqualToString:@"PickUp L 41"]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isBlockByLock {
    if ([self.status isEqualToString:@"PickUp S 43"]) {
        return YES;
    } else {
        return NO;
    }
}



- (NSString *)balanceAvailableWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", [self.balance decimalFormatString], self.currency];
}

- (NSString *)balanceWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", [self.balance decimalFormatString], self.currency];
}


- (NSString *)availableBalanceWithCardAliasOrCardNumberWithCurrency {
    NSString * alias;
    if(![self.alias isEqual:@""]) {
        alias = self.alias;
    } else {
        alias = self.number;
    }
    return [NSString stringWithFormat:@"%@ (%@)", alias, [self balanceAvailableWithCurrency]];
}

@end
