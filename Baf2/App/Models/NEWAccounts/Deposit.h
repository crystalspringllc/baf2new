#import <Foundation/Foundation.h>
#import "Account.h"

@interface Deposit : Account <NSCopying>

@property (nonatomic, copy) NSString *deal;
@property (nonatomic, copy) NSString *fee;
@property (nonatomic, assign) BOOL isCapitalize;
@property (nonatomic, copy) NSString *minimal;
@property (nonatomic, copy) NSString *period;

+ (Deposit *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)balanceWithCurrency;

@end