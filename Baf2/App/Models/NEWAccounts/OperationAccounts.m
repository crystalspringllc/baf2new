//
// Created by Askar Mustafin on 5/31/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "OperationAccounts.h"
#import "Accounts.h"

@implementation OperationAccounts {}

+ (OperationAccounts *)instanceFromDictionary:(NSDictionary *)aDictionary {
    OperationAccounts *instance = [[OperationAccounts alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;


    NSDictionary *creditAccountsDict = objectData[@"creditAccounts"];
    if(creditAccountsDict){
    creditAccountsDict = objectData[@"creditAccounts"];
    self.targetAccounts = [Accounts instanceFromDictionary:creditAccountsDict];


    NSDictionary *debetAccountsDict = objectData[@"debetAccounts"];
    self.sourceAccounts = [Accounts instanceFromDictionary:debetAccountsDict];
    }else{
    NSDictionary *creditAccountsDict = objectData[@"destinations"];
    self.targetAccounts = [Accounts instanceFromDictionary:creditAccountsDict];
    
    
    NSDictionary *debetAccountsDict = objectData[@"requestors"];
    self.sourceAccounts = [Accounts instanceFromDictionary:debetAccountsDict];
    }
    objectData = nil;
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setTargetAccounts:[self.targetAccounts copyWithZone:zone]];
        [copy setSourceAccounts:[self.sourceAccounts copyWithZone:zone]];
    }
    
    return copy;
}

@end
