#import <Foundation/Foundation.h>
#import "Account.h"

@interface CardAccount : Account

@property (nonatomic, copy) NSNumber *cardAccountId;
@property (nonatomic, copy) NSArray *cards;

+ (CardAccount *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSNumber *)accountId;

@end
