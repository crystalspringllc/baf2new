//
//  Accounts.m
//  Baf2
//
//  Created by Shyngys Kassymov on 31.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "Accounts.h"
#import "Card.h"
#import "ExtCard.h"
#import "Current.h"
#import "Deposit.h"
#import "Loan.h"
#import "Beneficiary.h"

@implementation Accounts

+ (instancetype)instanceFromDictionary:(NSDictionary *)aDictionary {
    Accounts *instance = [[Accounts alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    NSArray *cardsData = objectData[@"cards"];
    NSMutableArray *cardsMembers = [[NSMutableArray alloc] init];
    for (NSDictionary *cardsDict in cardsData) {
        [cardsMembers addObject:[Card instanceFromDictionary:cardsDict]];
    }
    self.cards = cardsMembers;
    
    NSArray *extCardsData = objectData[@"extCards"];
    NSMutableArray *extCardsMembers = [[NSMutableArray alloc] init];
    for (NSDictionary *extCardsDict in extCardsData) {
        [extCardsMembers addObject:[ExtCard instanceFromDictionary:extCardsDict]];
    }
    self.extCards = extCardsMembers;
    
    NSArray *currentsData = objectData[@"currents"];
    NSMutableArray *currentsMembers = [[NSMutableArray alloc] init];
    for (NSDictionary *currentsDict in currentsData) {
        [currentsMembers addObject:[Current instanceFromDictionary:currentsDict]];
    }
    self.currents = currentsMembers;
    
    NSArray *depositsData = objectData[@"deposits"];
    NSMutableArray *depositsMembers = [[NSMutableArray alloc] init];
    for (NSDictionary *depositsDict in depositsData) {
        [depositsMembers addObject:[Deposit instanceFromDictionary:depositsDict]];
    }
    self.deposits = depositsMembers;
    
    NSArray *loansData = objectData[@"loans"];
    NSMutableArray *loansMembers = [[NSMutableArray alloc] init];
    for (NSDictionary *loansDict in loansData) {
        [loansMembers addObject:[Loan instanceFromDictionary:loansDict]];
    }
    self.loans = loansMembers;
    
    NSArray *beneficiarsData = objectData[@"beneficiars"];
    NSMutableArray *beneficiarsMembers = [[NSMutableArray alloc] init];
    for (NSDictionary *beneficiarsDict in beneficiarsData) {
        [beneficiarsMembers addObject:[Beneficiary instanceFromDictionary:beneficiarsDict]];
    }
    self.beneficiars = beneficiarsMembers;
    
    objectData = nil;
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setCards:[self.cards copyWithZone:zone]];
        [copy setExtCards:[self.extCards copyWithZone:zone]];
        [copy setCurrents:[self.currents copyWithZone:zone]];
        [copy setDeposits:[self.deposits copyWithZone:zone]];
        [copy setLoans:[self.loans copyWithZone:zone]];
        [copy setBeneficiars:[self.beneficiars copyWithZone:zone]];
    }
    
    return copy;
}

@end
