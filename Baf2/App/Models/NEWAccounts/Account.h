//
// Created by Askar Mustafin on 5/31/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"
#import "NSNumber+Ext.h"
#import "Constants.h"
#import "CVCmodel.h"

typedef enum AccountType:NSInteger {
    AccountTypeAccount,
    AccountTypeCard,
    AccountTypeDeposit,
    AccountTypeLoan,
    AccountTypeCurrent,
    AccountTypeBeneficiary
} AccountType;

@interface Account : ModelObject <NSCopying>

@property (nonatomic, copy) NSNumber *accountId;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *iban;
@property (nonatomic, copy) NSString *alias;
@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSNumber *balance;

@property (nonatomic, copy) NSNumber *balanceKzt;
@property (nonatomic, copy) NSNumber *balanceUsd;
@property (nonatomic, copy) NSNumber *balanceEur;
@property (nonatomic, copy) NSNumber *balanceRub;
@property (nonatomic, copy) NSNumber *balanceGbp;

@property (nonatomic, copy) NSNumber *availableInUsd;
@property (nonatomic, copy) NSNumber *availableInEur;
@property (nonatomic, copy) NSNumber *availableInRub;
@property (nonatomic, copy) NSNumber *availableInGbp;
@property (nonatomic, copy) NSNumber *cashBack;
@property (nonatomic, copy) NSNumber *block;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *statusDescr;
@property (nonatomic, copy) NSString *product;
@property (nonatomic, copy) NSString *filial;
@property (nonatomic, assign) BOOL isMulty;
@property (nonatomic, assign) BOOL isWarning;
@property (nonatomic, assign) BOOL isFavorite;
@property (nonatomic, assign) BOOL isOwner;
@property (nonatomic, assign) BOOL isLegal;
@property (nonatomic, assign) BOOL isIslamic;
@property (nonatomic, assign) BOOL cashbackOn;
@property (nonatomic, copy) NSString *updated;
@property (nonatomic, copy) NSString *opened;
@property (nonatomic, copy) NSString *closed;
@property (nonatomic, copy) NSString *endDate;
@property (nonatomic, strong) NSArray *currencies;

// additional
@property (nonatomic, copy) NSString *cardType;
@property (nonatomic, copy) NSString *bank;
@property (nonatomic, copy) NSString *bankLogo;

+ (Account *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)getBalanceWithCurrency;

- (AccountType)accountType;


+ (NSString *)accountTypeToString:(AccountType)accountType;
- (NSString *)logo;

- (BOOL)isBlockedIBFL;
- (BOOL)isBlocked;
- (BOOL)isBlockByLock;


- (NSString *)availableBalanceWithCardAliasOrCardNumberWithCurrency;
- (NSString *)balanceAvailableWithCurrency;
- (NSString *)balanceWithCurrency;

- (BOOL)isBlockStole;

@end
