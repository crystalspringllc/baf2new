#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface DepositGroup : ModelObject

@property (nonatomic, copy) NSString *alias;
@property (nonatomic, copy) NSString *deal;
@property (nonatomic, copy) NSArray *deposits;
@property (nonatomic, copy) NSString *product;
@property (nonatomic, assign) BOOL isFavorite;

+ (DepositGroup *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
