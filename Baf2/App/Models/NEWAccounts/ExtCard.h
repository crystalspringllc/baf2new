#import <Foundation/Foundation.h>
#import "Account.h"

@interface ExtCard : Account <NSCopying>

@property (nonatomic, assign) BOOL alphaApproved;
@property (nonatomic, copy) NSString *dateStatus;
@property (nonatomic, assign) BOOL deleted;
@property (nonatomic, copy) NSString *descriptionText;
@property (nonatomic, assign) BOOL epayApproved;
@property (nonatomic, assign) BOOL isMain;
@property (nonatomic, copy) NSString *processing;
@property (nonatomic, copy) NSString *registeredDate;
@property (nonatomic, copy) NSString *dtDeleted;
@property (nonatomic, copy) NSString *cardBin;
@property (nonatomic, copy) NSString *cardHash;
@property (nonatomic, copy) NSString *expMonth;
@property (nonatomic, copy) NSString *expYear;
@property (nonatomic, copy) NSNumber *eventId;
@property (nonatomic, copy) NSString *epayCardId;
@property (nonatomic, copy) NSString *epayAttemptDate;
@property (nonatomic, copy) NSString *approvedDate;
@property (nonatomic, copy) NSString *mask;
@property (nonatomic, copy) NSString *reference;

+ (ExtCard *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;


- (NSString *)getMaskOrEmpty;
- (NSString *)getDateStatusText;
- (NSString *)logo;
- (BOOL)isCardExpired;

@end