#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface AccountsResponse : ModelObject {

    NSArray *cardAccounts;
    NSArray *currents;
    NSArray *depositGroups;
    NSArray *extCards;
    BOOL inUpdating;
    NSArray *loans;
    NSString *updated;

}

@property (nonatomic, copy) NSArray *cardAccounts;
@property (nonatomic, copy) NSArray *currents;
@property (nonatomic, copy) NSArray *depositGroups;
@property (nonatomic, copy) NSArray *extCards;
@property (nonatomic, copy) NSArray *loans;

@property (nonatomic, copy) NSString *updated;
@property (nonatomic, assign) BOOL inUpdating;

+ (AccountsResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;


- (NSArray *)getAllCards;

- (BOOL)isEmpty;

- (NSString *)getUpdatedNiceFormat;

- (BOOL)hasAtLeastOneAccount;

@end