#import "Loan.h"
#import "NSNumber+Ext.h"

@implementation Loan

+ (Loan *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Loan *instance = [[Loan alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    [super setAttributesFromDictionary:aDictionary];

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.loanId = [self numberValueForKey:@"id"];
    self.linkedAccountId = [self numberValueForKey:@"linkedAccountId"];
    self.linkedAccountType = [self stringValueForKey:@"linkedAccountType"];
    self.linkedAccountAlias = [self stringValueForKey:@"linkedAccountAlias"];
    self.rate = [self stringValueForKey:@"rate"];
    self.periodMonth = [self numberValueForKey:@"periodMonth"];
    self.totalPrincipal = [self numberValueForKey:@"totalPrincipal"];
    self.totalInterest = [self numberValueForKey:@"totalInterest"];
    self.planDate = [self stringValueForKey:@"planDate"];
    self.planInterest = [self numberValueForKey:@"planInterest"];
    self.planPrincipal = [self numberValueForKey:@"planPrincipal"];
    self.planAmount = [self numberValueForKey:@"planAmount"];
    self.fineInterest = [self numberValueForKey:@"fineInterest"];
    self.finePrincipal = [self numberValueForKey:@"finePrincipal"];
    self.overdueInterest = [self numberValueForKey:@"overdueInterest"];
    self.overduePrincipal = [self numberValueForKey:@"overduePrincipal"];
    self.overdueOverinterest = [self numberValueForKey:@"overdueOverinterest"];

    objectData = nil;

}

#pragma mark - Methods

- (NSString *)productAliasWithPrincipal {
    return [NSString stringWithFormat:@"%@ (%@ %@)", self.alias, self.totalPrincipal, self.currency];
}

- (CGFloat)repaymentAmount {
    return [self.planPrincipal floatValue] + [self.planInterest floatValue];
}

- (NSString *)repaymentAmountWithCurrency {
    NSNumber *numberValue = @([self repaymentAmount]);
    return [NSString stringWithFormat:@"%@ %@", [numberValue decimalFormatString], self.currency];
}

- (NSString *)debtAmountWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", [self.totalPrincipal decimalFormatString], self.currency];
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setLoanId:[self.loanId copyWithZone:zone]];
        [copy setLinkedAccountId:[self.linkedAccountId copyWithZone:zone]];
        [copy setLinkedAccountType:[self.linkedAccountType copyWithZone:zone]];
        [copy setLinkedAccountAlias:[self.linkedAccountAlias copyWithZone:zone]];
        [copy setRate:[self.rate copyWithZone:zone]];
        [copy setPeriodMonth:[self.periodMonth copyWithZone:zone]];
        [copy setTotalPrincipal:[self.totalPrincipal copyWithZone:zone]];
        [copy setTotalInterest:[self.totalInterest copyWithZone:zone]];
        [copy setPlanDate:[self.planDate copyWithZone:zone]];
        [copy setPlanInterest:[self.planInterest copyWithZone:zone]];
        [copy setPlanPrincipal:[self.planPrincipal copyWithZone:zone]];
        [copy setPlanAmount:[self.planAmount copyWithZone:zone]];
        [copy setFineInterest:[self.fineInterest copyWithZone:zone]];
        [copy setFinePrincipal:[self.finePrincipal copyWithZone:zone]];
        [copy setOverdueInterest:[self.overdueInterest copyWithZone:zone]];
        [copy setOverduePrincipal:[self.overduePrincipal copyWithZone:zone]];
        [copy setOverdueOverinterest:[self.overdueOverinterest copyWithZone:zone]];
    }
    
    return copy;
}

- (NSNumber *)accountId {
    return self.loanId;
}

@end