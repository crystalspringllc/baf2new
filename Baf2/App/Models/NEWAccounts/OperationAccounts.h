//
// Created by Askar Mustafin on 5/31/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"

@class Accounts;

@interface OperationAccounts : ModelObject <NSCopying>

@property (nonatomic, strong) Accounts *targetAccounts;
@property (nonatomic, strong) Accounts *sourceAccounts;

+ (OperationAccounts *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end