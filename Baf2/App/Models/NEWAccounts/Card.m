#import "Card.h"


@implementation Card

+ (Card *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Card *instance = [[Card alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    [super setAttributesFromDictionary:aDictionary];

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;
    
    self.cardId = [self numberValueForKey:@"id"];
    self.accountId = [self numberValueForKey:@"accountId"];
    self.aviaBonus = [self numberValueForKey:@"aviaBonus"];
    self.aviaBonusCharged = [self numberValueForKey:@"aviaBonusCharged"];
    self.aviaBonusRate = [self numberValueForKey:@"aviaBonusRate"];
    self.cardType = [self stringValueForKey:@"cardType"];
    self.creditLimit = [self numberValueForKey:@"creditLimit"];
    self.debtAmount = [self numberValueForKey:@"debtAmount"];
    self.expiry = [self stringValueForKey:@"expiry"];
    self.expiryDate = [self stringValueForKey:@"expiryDate"];
    self.firstName = [self stringValueForKey:@"firstName"];
    self.hasCreditLimit = [self boolValueForKey:@"hasCreditLimit"];
    self.interestAmount = [self numberValueForKey:@"interestAmount"];
    self.isAvia = [self boolValueForKey:@"isAvia"];
    self.isCredit = [self boolValueForKey:@"isCredit"];
    self.isMain = [self boolValueForKey:@"isMain"];
    self.isVirtual = [self boolValueForKey:@"isVirtual"];
    self.lastName = [self stringValueForKey:@"lastName"];
    self.nextPaymentDate = [self stringValueForKey:@"nextPaymentDate"];
    self.nextPaymentAmount = [self numberValueForKey:@"nextPaymentAmount"];
    self.number = [self stringValueForKey:@"number"];
    self.totalDebtAmount = [self numberValueForKey:@"totalDebtAmount"];

    objectData = nil;
}

- (NSString *)logo {
    return [NSString stringWithFormat:@"%@cards/small/%@.png", BASE_IMAGE_URL, self.cardType.lowercaseString];
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setAviaBonus:[self.aviaBonus copyWithZone:zone]];
        [copy setAviaBonusCharged:[self.aviaBonusCharged copyWithZone:zone]];
        [copy setAviaBonusRate:[self.aviaBonusRate copyWithZone:zone]];
        [copy setCardType:[self.cardType copyWithZone:zone]];
        [copy setCreditLimit:[self.creditLimit copyWithZone:zone]];
        [copy setDebtAmount:[self.debtAmount copyWithZone:zone]];
        [copy setExpiry:[self.expiry copyWithZone:zone]];
        [copy setExpiryDate:[self.expiryDate copyWithZone:zone]];
        [copy setFirstName:[self.firstName copyWithZone:zone]];
        [copy setInterestAmount:[self.interestAmount copyWithZone:zone]];
        [copy setLastName:[self.lastName copyWithZone:zone]];
        [copy setNextPaymentDate:[self.nextPaymentDate copyWithZone:zone]];
        [copy setNextPaymentAmount:[self.nextPaymentAmount copyWithZone:zone]];
        [copy setNumber:[self.number copyWithZone:zone]];
        [copy setTotalDebtAmount:[self.totalDebtAmount copyWithZone:zone]];
        
        // Set primitives
        [copy setHasCreditLimit:self.hasCreditLimit];
        [copy setIsAvia:self.isAvia];
        [copy setIsCredit:self.isCredit];
        [copy setIsMain:self.isMain];
        [copy setIsVirtual:self.isVirtual];
    }
    
    return copy;
}

- (NSNumber *)accountId {
    return self.cardId;
}

@end
