//
//  CardLimit.h
//  Baf2
//
//  Created by Shyngys Kassymov on 12.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"

typedef enum CardLimitCode:NSInteger {
    CardLimitAtmCashWithdraw, // Лимит на снятия наличных в банкоматах
    CardLimitAbroadPayment, // Лимит на оплату товаров и услуг в пунктах торговли и сервиса за пределами Казахстана
    CardLimitEcommerce // Лимит на оплату товаров и услуг в сети интернет (электронная коммерция)
} CardLimitCode;

@interface CardLimit : ModelObject

@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *step;

@property (nonatomic, strong) NSString *activityFrom;
@property (nonatomic, strong) NSString *activityTo;

@property (nonatomic, strong) NSNumber *minAmount;
@property (nonatomic, strong) NSNumber *maxAmount;

#pragma mark - Methods

+ (NSArray *)instancesFromArray:(NSArray *)array;
+ (CardLimit *)instanceFromDictionary:(NSDictionary *)aDictionary;

- (NSDate *)activityFromDate;
- (NSDate *)activityToDate;

+ (NSString *)limitCodeToString:(CardLimitCode)limitCode;

- (NSString *)amoungAndCurrency;

@end
