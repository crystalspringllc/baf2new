//
//  Beneficiary.h
//  Baf2
//
//  Created by Shyngys Kassymov on 02.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "Account.h"

@interface Beneficiary : Account <NSCopying>

@property (nonatomic, strong) NSString *accountType;
@property (nonatomic, strong) NSString *bankBic;
@property (nonatomic, strong) NSString *iin;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *kbe;
@property (nonatomic) BOOL isInterbank;
@property (nonatomic) BOOL isAlreadyExist;
@property (nonatomic) BOOL isDeleted;

+ (Beneficiary *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)balanceWithCurrency;

- (NSString *)logo;

@end
