#import <Foundation/Foundation.h>
#import "Account.h"

@interface Card : Account <NSCopying>

@property (nonatomic, copy) NSNumber *cardId;
@property (nonatomic, copy) NSNumber *aviaBonus;
@property (nonatomic, copy) NSNumber *aviaBonusCharged;
@property (nonatomic, copy) NSNumber *aviaBonusRate;
@property (nonatomic, copy) NSNumber *creditLimit;
@property (nonatomic, copy) NSNumber *debtAmount;
@property (nonatomic, copy) NSString *expiry;
@property (nonatomic, copy) NSString *expiryDate;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, assign) BOOL hasCreditLimit;
@property (nonatomic, copy) NSNumber *interestAmount;
@property (nonatomic, assign) BOOL isAvia;
@property (nonatomic, assign) BOOL isCredit;
@property (nonatomic, assign) BOOL isMain;
@property (nonatomic, assign) BOOL isVirtual;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *nextPaymentDate;
@property (nonatomic, copy) NSNumber *nextPaymentAmount;
@property (nonatomic, copy) NSNumber *totalDebtAmount;

+ (Card *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSNumber *)accountId;

- (NSString *)availableBalanceWithCardAliasOrCardNumberWithCurrency;

- (NSString *)logo;

@end
