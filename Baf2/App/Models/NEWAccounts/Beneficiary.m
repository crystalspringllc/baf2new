//
//  Beneficiary.m
//  Baf2
//
//  Created by Shyngys Kassymov on 02.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "Beneficiary.h"
#import "NSNumber+Ext.h"
#import "Constants.h"

@implementation Beneficiary

+ (Beneficiary *)instanceFromDictionary:(NSDictionary *)aDictionary {
    
    Beneficiary *instance = [[Beneficiary alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    [super setAttributesFromDictionary:aDictionary];
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.accountType = [self stringValueForKey:@"accountType"];
    self.bank = [self stringValueForKey:@"bank"];
    self.bankBic = [self stringValueForKey:@"bankBic"];
    self.bankLogo = [self stringValueForKey:@"bankLogo"];
    self.iin = [self stringValueForKey:@"iin"];
    self.firstName = [self stringValueForKey:@"firstName"];
    self.lastName = [self stringValueForKey:@"lastName"];
    self.kbe = [self stringValueForKey:@"kbe"];
    self.isInterbank = [self boolValueForKey:@"isInterbank"];
    self.isAlreadyExist = [self boolValueForKey:@"isAlreadyExist"];
    self.isDeleted = [self boolValueForKey:@"isDeleted"];
    
    objectData = nil;
    
}

- (NSString *)balanceWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", [self.balance decimalFormatString], self.currency];
}


- (NSString *)logo {
    return [NSString stringWithFormat:@"%@banks/%@.png", BASE_IMAGE_URL, self.bankLogo.lowercaseString];
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setAccountType:[self.accountType copyWithZone:zone]];
        [copy setBank:[self.bank copyWithZone:zone]];
        [copy setBankBic:[self.bankBic copyWithZone:zone]];
        [copy setBankLogo:[self.bankLogo copyWithZone:zone]];
        [copy setIin:[self.iin copyWithZone:zone]];
        [copy setFirstName:[self.firstName copyWithZone:zone]];
        [copy setLastName:[self.lastName copyWithZone:zone]];
        [copy setKbe:[self.kbe copyWithZone:zone]];
        
        // Set primitives
        [copy setIsInterbank:self.isInterbank];
        [copy setIsDeleted:self.isDeleted];
        [copy setIsAlreadyExist:self.isAlreadyExist];
    }
    
    return copy;
}

@end
