#import "Current.h"
#import "NSNumber+Ext.h"

@implementation Current

+ (Current *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Current *instance = [[Current alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    [super setAttributesFromDictionary:aDictionary];

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    objectData = nil;

}

- (NSString *)balanceWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", [self.balance decimalFormatString], self.currency];
}

- (NSString *)logo {
    return kBafLogoUrlSmall;
}

@end
