//
// Created by Askar Mustafin on 6/3/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"


@interface Statement : ModelObject

@property (nonatomic, strong) NSString *operationId;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *alias;
@property (nonatomic, strong) NSString *descriptionText;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSNumber *billAmount;
@property (nonatomic, strong) NSString *billCurrency;
@property (nonatomic, strong) NSNumber *comission;
@property (nonatomic, strong) NSString *comissionCurrency;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, assign) BOOL isCredit;
@property (nonatomic, strong) NSString *iban;
@property (nonatomic, strong) NSNumber *cashback;
@property (nonatomic, strong) NSString *cashbackCurrency;
@property (nonatomic, strong) NSString *merchant;

//properties added after aviata statements
@property (nonatomic, strong) NSNumber *accountAmount;
@property (nonatomic, strong) NSString *accountCurrency;
@property (nonatomic, strong) NSString *bankingDate;
@property (nonatomic, strong) NSNumber *aviaBonusKzt;
@property (nonatomic, strong) NSNumber *aviaBonusMil;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *city;

+ (Statement *)instanceFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)getMerchantURLString;

@end