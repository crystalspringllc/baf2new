//
//  CardLimit.m
//  Baf2
//
//  Created by Shyngys Kassymov on 12.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "CardLimit.h"

@implementation CardLimit

+ (NSArray *)instancesFromArray:(NSArray *)array {
    NSMutableArray *instances = [NSMutableArray new];
    
    for (NSDictionary *data in array) {
        CardLimit *cardLimit = [self instanceFromDictionary:data];
        [instances addObject:cardLimit];
    }
    
    return instances;
}

+ (CardLimit *)instanceFromDictionary:(NSDictionary *)aDictionary {
    CardLimit *instance = [[CardLimit alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
    
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.amount = [self numberValueForKey:@"amount"];
    self.code = [self stringValueForKey:@"code"];
    self.currency = [self stringValueForKey:@"currency"];
    self.name = [self stringValueForKey:@"name"];
    self.step = [self numberValueForKey:@"step"];
    self.activityFrom = [self stringValueForKey:@"activityFrom"];
    self.activityTo = [self stringValueForKey:@"activityTo"];
    self.minAmount = [self numberValueForKey:@"minAmount"];
    self.maxAmount = [self numberValueForKey:@"maxAmount"];
    
    objectData = nil;
}

#pragma mark - Methods

- (NSDateFormatter *)dateFormatter {
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return df;
}

- (NSDate *)activityFromDate {
    return [[self dateFormatter] dateFromString:self.activityFrom];
}

- (NSDate *)activityToDate {
    return [[self dateFormatter] dateFromString:self.activityTo];
}

+ (NSString *)limitCodeToString:(CardLimitCode)limitCode {
    NSString *limitCodeToString = nil;
    
    switch (limitCode) {
        case CardLimitAtmCashWithdraw: {
            limitCodeToString = @"A_CASH_DAY";
        }
            break;
        case CardLimitAbroadPayment: {
            limitCodeToString = @"A_FR_RET";
        }
            break;
        case CardLimitEcommerce: {
            limitCodeToString = @"RET_E_COM";
        }
            break;
        default:
            break;
    }
    
    return limitCodeToString;
}

- (NSString *)amoungAndCurrency {
    return [NSString stringWithFormat:@"%@ %@", self.amount, self.currency];
}

@end
