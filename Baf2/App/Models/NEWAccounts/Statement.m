//
// Created by Askar Mustafin on 6/3/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "Statement.h"


@implementation Statement {}

+ (Statement *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Statement *instance = [[Statement alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.operationId = [self stringValueForKey:@"operationId"];
    self.number = [self stringValueForKey:@"number"];
    self.alias = [self stringValueForKey:@"alias"];
    self.descriptionText = [self stringValueForKey:@"description"];
    self.amount = [self numberValueForKey:@"amount"];
    self.currency = [self stringValueForKey:@"currency"];
    self.billAmount = [self numberValueForKey:@"billAmount"];
    self.billCurrency = [self stringValueForKey:@"billCurrency"];
    self.comission = [self numberValueForKey:@"comission"];
    self.comissionCurrency = [self stringValueForKey:@"comissionCurrency"];
    self.date = [self stringValueForKey:@"date"];
    self.isCredit = [self boolValueForKey:@"isCredit"];
    self.iban = [self stringValueForKey:@"iban"];
    self.cashback = [self numberValueForKey:@"cashback"];
    self.cashbackCurrency = [self stringValueForKey:@"cashbackCurrency"];
    self.merchant = [NSString stringWithFormat:@"%@merchants/%@.png", BASE_IMAGE_URL, [self stringValueForKey:@"merchant"]];

    //properties added after aviata statements
    self.accountAmount = [self numberValueForKey:@"accountAmount"];
    self.accountCurrency = [self stringValueForKey:@"accountCurrency"];
    self.bankingDate = [self stringValueForKey:@"bankingDate"];
    self.aviaBonusKzt = [self numberValueForKey:@"aviaBonusKzt"];
    self.aviaBonusMil = [self numberValueForKey:@"aviaBonusMil"];
    self.country = [self stringValueForKey:@"country"];
    self.city = [self stringValueForKey:@"city"];

    objectData = nil;
}

@end