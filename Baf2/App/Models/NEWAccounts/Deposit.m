#import "Deposit.h"
#import "NSNumber+Ext.h"

@implementation Deposit

+ (Deposit *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Deposit *instance = [[Deposit alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    [super setAttributesFromDictionary:aDictionary];

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.deal = [self stringValueForKey:@"deal"];
    self.fee = [self stringValueForKey:@"fee"];
    self.isCapitalize = [self boolValueForKey:@"isCapitalize"];
    self.minimal = [self stringValueForKey:@"minimal"];
    self.period = [self stringValueForKey:@"period"];

    objectData = nil;

}

- (NSString *)balanceWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", [self.balance decimalFormatString], self.currency];
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setDeal:[self.deal copyWithZone:zone]];
        [copy setFee:[self.fee copyWithZone:zone]];
        [copy setMinimal:[self.minimal copyWithZone:zone]];
        [copy setPeriod:[self.period copyWithZone:zone]];
        
        // Set primitives
        [copy setIsCapitalize:self.isCapitalize];
    }
    
    return copy;
}

- (NSString *)logo {
    return kBafLogoUrlSmall;
}


@end
