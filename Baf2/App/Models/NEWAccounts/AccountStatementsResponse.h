//
// Created by Askar Mustafin on 6/3/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"


@interface AccountStatementsResponse : ModelObject

@property (nonatomic, strong) NSArray *statements;
@property (nonatomic, strong) NSArray *blockStatements;
@property (nonatomic, strong) NSArray *aviaStatements;
@property (nonatomic, strong) NSString *fromDate;
@property (nonatomic, strong) NSString *toDate;

@property (nonatomic, strong) NSNumber *cashBack;
@property (nonatomic, strong) NSString *cashBackCurrency;


+ (instancetype)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
