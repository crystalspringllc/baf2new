#import <Foundation/Foundation.h>
#import "Account.h"

@interface Current : Account <NSCopying>

+ (Current *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)balanceWithCurrency;

@end
