//
//  Accounts.h
//  Baf2
//
//  Created by Shyngys Kassymov on 31.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface Accounts : ModelObject <NSCopying>

@property (nonatomic, strong) NSArray *cards;
@property (nonatomic, strong) NSArray *extCards;
@property (nonatomic, strong) NSArray *currents;
@property (nonatomic, strong) NSArray *deposits;
@property (nonatomic, strong) NSArray *loans;
@property (nonatomic, strong) NSArray *beneficiars;

+ (instancetype)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
