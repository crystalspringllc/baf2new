#import "CardAccount.h"

#import "Card.h"

@implementation CardAccount

+ (CardAccount *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CardAccount *instance = [[CardAccount alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    [super setAttributesFromDictionary:aDictionary];

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.cardAccountId = [self numberValueForKey:@"id"];

    NSArray *cardsData = objectData[@"cards"];
    NSMutableArray *cardMembers = [[NSMutableArray alloc] init];
    for (NSDictionary *cardDict in cardsData) {
        [cardMembers addObject:[Card instanceFromDictionary:cardDict]];
    }
    self.cards = cardMembers;

    objectData = nil;

}

- (NSNumber *)accountId {
    return self.cardAccountId;
}

@end
