#import "AccountsResponse.h"

#import "CardAccount.h"
#import "Current.h"
#import "DepositGroup.h"
#import "ExtCard.h"
#import "Loan.h"
#import "Card.h"
#import "NSString+Ext.h"
#import "NSDate+Ext.h"

@implementation AccountsResponse

@synthesize cardAccounts;
@synthesize currents;
@synthesize depositGroups;
@synthesize extCards;
@synthesize inUpdating;
@synthesize loans;
@synthesize updated;

+ (AccountsResponse *)instanceFromDictionary:(NSDictionary *)aDictionary {

    AccountsResponse *instance = [[AccountsResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;


    self.inUpdating = [self boolValueForKey:@"inUpdating"];
    self.updated = [self stringValueForKey:@"updated"];


    NSArray *cardAccountsData = objectData[@"cardAccounts"];
    NSMutableArray *cardAccountsMembers = [[NSMutableArray alloc] init];
    for (NSDictionary *cardAccountsDict in cardAccountsData) {
        [cardAccountsMembers addObject:[CardAccount instanceFromDictionary:cardAccountsDict]];
    }
    self.cardAccounts = cardAccountsMembers;


    NSArray *currentsData = objectData[@"currents"];
    NSMutableArray *currentsMembers = [[NSMutableArray alloc] init];
    for (NSDictionary *currentsDict in currentsData) {
        [currentsMembers addObject:[Current instanceFromDictionary:currentsDict]];
    }
    self.currents = currentsMembers;


    NSArray *depositGroupsData = objectData[@"depositGroups"];
    NSMutableArray *depositGroupsMembers = [[NSMutableArray alloc] init];
    for (NSDictionary *depositGroupsDict in depositGroupsData) {
        [depositGroupsMembers addObject:[DepositGroup instanceFromDictionary:depositGroupsDict]];
    }
    self.depositGroups = depositGroupsMembers;


    NSArray *extCardsData = objectData[@"extCards"];
    NSMutableArray *extCardsMembers = [[NSMutableArray alloc] init];
    for (NSDictionary *extCardsDict in extCardsData) {
        [extCardsMembers addObject:[ExtCard instanceFromDictionary:extCardsDict]];
    }
    self.extCards = extCardsMembers;


    NSArray *loansData = objectData[@"loans"];
    NSMutableArray *loansMembers = [[NSMutableArray alloc] init];
    for (NSDictionary *loansDict in loansData) {
        [loansMembers addObject:[Loan instanceFromDictionary:loansDict]];
    }
    self.loans = loansMembers;


    objectData = nil;

}

- (BOOL)isEmpty {
    return self.cardAccounts.count == 0
            && self.currents.count == 0
            && self.depositGroups.count == 0
            && self.extCards.count == 0
            && self.loans.count == 0;
}

- (BOOL)hasAtLeastOneAccount {
    BOOL hasAtLeastOneAccount;

    if ([self.cardAccounts count]
            || [self.currents count]
            || [self.depositGroups count]
            || [self.loans count]
            || [self.extCards count])
    {
        hasAtLeastOneAccount = YES;
    } else {
        hasAtLeastOneAccount = NO;
    }

    return hasAtLeastOneAccount;
}

#pragma mark - helpers

- (NSArray *)getAllCards {
    NSMutableArray *cards = [[NSMutableArray alloc] init];
    for (CardAccount *cardAccount in self.cardAccounts) {
        for (Card *card in cardAccount.cards) {
            [cards addObject:card];
        }
    }
    return cards;
}

- (NSString *)getUpdatedNiceFormat {
    NSString *updateNiceFormat;
    if (self.updated) {
        NSDate *updateDate = [self.updated dateWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        updateNiceFormat = [updateDate stringWithDateFormat:@"dd.MM.yyyy HH:mm:ss"];
        return updateNiceFormat;
    } else {
        return self.updated;
    }
}

@end
