#import "DepositGroup.h"

#import "Deposit.h"

@implementation DepositGroup

+ (DepositGroup *)instanceFromDictionary:(NSDictionary *)aDictionary {

    DepositGroup *instance = [[DepositGroup alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.alias = [self stringValueForKey:@"alias"];
    self.deal = [self stringValueForKey:@"deal"];
    self.product = [self stringValueForKey:@"product"];
    self.isFavorite = [self boolValueForKey:@"isFavorite"];


    NSArray *depositsData = objectData[@"deposits"];
    NSMutableArray *depositsMembers = [[NSMutableArray alloc] init];
    for (NSDictionary *depositsDict in depositsData) {
        [depositsMembers addObject:[Deposit instanceFromDictionary:depositsDict]];
    }
    self.deposits = depositsMembers;

    objectData = nil;

}

@end
