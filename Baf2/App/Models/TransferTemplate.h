//
//  TransferTemplate.h
//  Baf2
//
//  Created by Shyngys Kassymov on 10.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"
#import "Account.h"
#import "PaymentProvider.h"

@interface TransferTemplate : ModelObject

@property (nonatomic, strong) NSNumber *templateId;
@property (nonatomic, strong) NSNumber *clientId;
@property (nonatomic, strong) NSString *alias;
@property (nonatomic) BOOL direction;
@property (nonatomic) BOOL isFavorite;
@property (nonatomic) BOOL templateIsActive;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *type;

@property (nonatomic, strong) PaymentProvider *provider;

@property (nonatomic, strong) NSNumber *requestorId;
@property (nonatomic, strong) Account *requestorAccount;
@property (nonatomic, strong) NSNumber *requestorAmount;
@property (nonatomic, strong) NSString *requestorType;
@property (nonatomic, strong) NSString *requestorCurrency;

@property (nonatomic, strong) NSNumber *destinationId;
@property (nonatomic, strong) Account *destinationAccount;
@property (nonatomic, strong) NSNumber *destinationAmount;
@property (nonatomic, strong) NSString *destinationType;
@property (nonatomic, strong) NSString *destinationCurrency;

@property (nonatomic, strong) NSString *knpName;
@property (nonatomic, strong) NSString *reference;


+ (TransferTemplate *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
