//
//  ContactsSQLiteManager.h
//  BAF
//
//  Created by Shyngys Kassymov on 24.07.15.
//
//

#import <Foundation/Foundation.h>
#import "CityObject.h"

typedef NS_ENUM(NSInteger , MapObjectsType) {
    MapObjectsTypeAtms = 0,
    MapObjectsTypeBranches = 1
};

@interface ContactsSQLiteManager : NSObject

@property(nonatomic, strong) CityObject *selectedCity;
@property(nonatomic, assign) MapObjectsType selectedObjectsType;

+ (instancetype)sharedContactsSQLiteManager;
- (NSArray *)loadAtms:(NSNumber *)cityId;
- (NSArray *)loadBranches:(NSNumber *)cityId;

@end
