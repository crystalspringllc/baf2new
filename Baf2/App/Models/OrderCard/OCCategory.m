//
// Created by Askar Mustafin on 1/6/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "OCCategory.h"
#import "City.h"


@implementation OCCategory {}
+ (OCCategory *)instanceFromDictionary:(NSDictionary *)aDictionary {

    OCCategory *instance = [[OCCategory alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.code = [self stringValueForKey:@"code"];
    self.descr = [self stringValueForKey:@"description"];
    self.categoryId = [self numberValueForKey:@"categoryId"];
    self.logo = [self stringValueForKey:@"logo"];
    self.name = [self stringValueForKey:@"name"];

    if ([objectData[@"products"] isKindOfClass:[NSArray class]]) {
        NSMutableArray *b = [[NSMutableArray alloc]init];
        for (NSDictionary *d in objectData[@"products"]) {
            OCProduct *product = [OCProduct instanceFromDictionary:d];
            [b addObject:product];
        }
        self.products = b;
    }

    objectData = nil;
}
@end



@implementation OCProduct {}
+ (OCProduct *)instanceFromDictionary:(NSDictionary *)aDictionary {
    OCProduct *instance = [[OCProduct alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    objectData = aDictionary;
    self.categoryCode = [self stringValueForKey:@"categoryCode"];
    self.code = [self stringValueForKey:@"code"];
    self.logo = [self stringValueForKey:@"logo"];
    self.logoName = [self stringValueForKey:@"logoName"];
    self.name = [self stringValueForKey:@"name"];
    self.currencies = nil;

    if ([objectData[@"cities"] isKindOfClass:[NSArray class]]) {
        NSMutableArray *b = [[NSMutableArray alloc]init];
        for (NSDictionary *d in objectData[@"cities"]) {
            City *city = [City instanceFromDictionary:d];
            [b addObject:city];
        }
        self.cities = b;
    }

    self.properties = [OCProperty instanceFromDictionary:objectData[@"properties"]];

    objectData = nil;
}
@end



@implementation OCProperty {}
+ (OCProperty *)instanceFromDictionary:(NSDictionary *)aDictionary {
    OCProperty *instance = [[OCProperty alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    objectData = aDictionary;

    if ([objectData[@"benefits"] isKindOfClass:[NSArray class]]) {
        NSMutableArray *b = [[NSMutableArray alloc]init];
        for (NSDictionary *d in objectData[@"benefits"]) {
            OCBenefit *benefit = [OCBenefit instanceFromDictionary:d];
            [b addObject:benefit];
        }
        self.benefits = b;
    }

    if ([objectData[@"tariffs"] isKindOfClass:[NSArray class]]) {
        NSMutableArray *t = [[NSMutableArray alloc] init];
        for (NSDictionary *d in objectData[@"tariffs"]) {
            OCTariff *tariff = [OCTariff instanceFromDictionary:d];
            [t addObject:tariff];
        }
        self.tariffs = t;
    }
    objectData = nil;
}
@end




@implementation OCBenefit {}
+ (OCBenefit *)instanceFromDictionary:(NSDictionary *)aDictionary {
    OCBenefit *instance = [[OCBenefit alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    objectData = aDictionary;
    self.text = [self stringValueForKey:@"text"];
    self.title = [self stringValueForKey:@"title"];
    objectData = nil;
}
@end



@implementation OCTariff {}
+ (OCTariff *)instanceFromDictionary:(NSDictionary *)aDictionary {
    OCTariff *instance = [[OCTariff alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    objectData = aDictionary;
    self.text = [self stringValueForKey:@"text"];
    self.title = [self stringValueForKey:@"title"];
    objectData = nil;
}
@end