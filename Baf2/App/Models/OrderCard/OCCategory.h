//
// Created by Askar Mustafin on 1/6/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"

@class OCProperty;


@interface OCCategory : ModelObject

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *descr;
@property (nonatomic, strong) NSNumber *categoryId;
@property (nonatomic, strong) NSString *logo;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *products;

+ (OCCategory *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end


@interface OCProduct : ModelObject

@property (nonatomic, strong) NSString *categoryCode;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *logo;
@property (nonatomic, strong) NSString *logoName;
@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSArray *currencies;
@property (nonatomic, strong) NSArray *cities;
@property (nonatomic, strong) OCProperty *properties;

+ (OCProduct *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end


@interface OCProperty : ModelObject

@property (nonatomic, strong) NSArray *benefits;
@property (nonatomic, strong) NSArray *tariffs;

+ (OCProperty *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end


@interface OCBenefit : ModelObject

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *title;

+ (OCBenefit *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end


@interface OCTariff : ModelObject

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *title;

+ (OCTariff *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end