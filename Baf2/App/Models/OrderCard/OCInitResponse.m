//
// Created by Askar Mustafin on 1/6/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "OCInitResponse.h"


@implementation OCInitResponse {}

+ (OCInitResponse *)instanceFromDictionary:(NSDictionary *)aDictionary {
    OCInitResponse *instance = [[OCInitResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    objectData = aDictionary;

    self.aggreement = [self stringValueForKey:@"aggreement"];
    self.categoryCode = [self stringValueForKey:@"categoryCode"];
    self.channel = [self stringValueForKey:@"channel"];
    self.city = [self numberValueForKey:@"city"];
    self.codeword = [self stringValueForKey:@"codeword"];
    self.iin = [self stringValueForKey:@"iin"];
    self.isChildCard = [self boolValueForKey:@"isChildCard"];
    self.isCredit = [self boolValueForKey:@"isCredit"];
    self.isMulti = [self boolValueForKey:@"isMulti"];
    self.isVirtual = [self boolValueForKey:@"isVirtual"];
    self.phone = [self stringValueForKey:@"phone"];
    self.productCode = [self stringValueForKey:@"productCode"];
    self.servicelogId = [self numberValueForKey:@"servicelogId"];

    objectData = nil;

}

@end


@implementation OCRunResponse{}

+ (OCRunResponse *)instanceFromDictionary:(NSDictionary *)aDictionary {
    OCRunResponse *instance = [[OCRunResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    objectData = aDictionary;

    self.aggreement = [self stringValueForKey:@"aggreement"];
    self.categoryCode = [self stringValueForKey:@"categoryCode"];
    self.channel = [self stringValueForKey:@"channel"];
    self.cityCode = [self numberValueForKey:@"city"];
    self.codeword = [self stringValueForKey:@"codeword"];
    self.iin = [self stringValueForKey:@"iin"];
    self.isChildCard = [self boolValueForKey:@"isChildCard"];
    self.isCredit = [self boolValueForKey:@"isCredit"];
    self.isMulti = [self boolValueForKey:@"isMulti"];
    self.isVirtual = [self boolValueForKey:@"isVirtual"];
    self.phone = [self stringValueForKey:@"phone"];
    self.productCode = [self stringValueForKey:@"productCode"];
    self.servicelogId = [self numberValueForKey:@"servicelogId"];
    self.state = [self stringValueForKey:@"state"];

    objectData = nil;
}

@end