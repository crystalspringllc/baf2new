//
// Created by Askar Mustafin on 1/6/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"


@interface OCInitResponse : ModelObject

@property (nonatomic, strong) NSString *aggreement;
@property (nonatomic, strong) NSString *categoryCode;
@property (nonatomic, strong) NSString *channel;
@property (nonatomic, strong) NSNumber *city;
@property (nonatomic, strong) NSString *codeword;
@property (nonatomic, strong) NSString *iin;
@property (nonatomic, assign) BOOL isChildCard;
@property (nonatomic, assign) BOOL isCredit;
@property (nonatomic, assign) BOOL isMulti;
@property (nonatomic, assign) BOOL isVirtual;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *productCode;
@property (nonatomic, strong) NSNumber *servicelogId;

+ (OCInitResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;

@end


@interface OCRunResponse : ModelObject

@property (nonatomic, strong) NSString *aggreement;
@property (nonatomic, strong) NSString *categoryCode;
@property (nonatomic, strong) NSString *channel;
@property (nonatomic, strong) NSNumber *cityCode;
@property (nonatomic, strong) NSString *codeword;
@property (nonatomic, strong) NSString *iin;
@property (nonatomic, assign) BOOL isChildCard;
@property (nonatomic, assign) BOOL isCredit;
@property (nonatomic, assign) BOOL isMulti;
@property (nonatomic, assign) BOOL isVirtual;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *productCode;
@property (nonatomic, strong) NSNumber *servicelogId;
@property (nonatomic, strong) NSString *state;

+ (OCRunResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;

@end