//
// Created by Askar Mustafin on 10/7/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"


@interface SubContract : ModelObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *acc;
@property (nonatomic, strong) NSString *amount;
@property (nonatomic, assign) BOOL canPay;

@property (nonatomic, strong) NSString *sum;
@property (nonatomic, strong) NSString *num;
@property (nonatomic, strong) NSString *date;

+ (SubContract *)instanceFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)getAmountString;
- (NSString *)getJsonStringNotation;

- (NSString *)getDateString;

@end