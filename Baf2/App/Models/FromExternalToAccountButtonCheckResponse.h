//
// Created by Askar Mustafin on 7/20/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"
#import "CheckTransferWarningMessages.h"
#import "TransferCurrencyRates.h"
#import "PaymentProvider.h"
#import "Account.h"

@interface FromExternalToAccountButtonCheckResponse : ModelObject

@property (nonatomic, assign) BOOL direction;
@property (nonatomic, assign) BOOL isValid;
@property (nonatomic, strong) NSNumber *requestorAmount;
@property (nonatomic, strong) NSNumber *destinationId;
@property (nonatomic, strong) NSNumber *destinationAmount;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSNumber *clientId;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *session;
@property (nonatomic, strong) NSString *requestorCurrency;
@property (nonatomic, strong) NSString *destinationType;
@property (nonatomic, strong) NSString *destinationCurrency;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *comission;
@property (nonatomic, strong) NSString *comissionCurrency;
@property (nonatomic, strong) NSString *safetyLevel;
@property (nonatomic, strong) NSString *operdate;

@property (nonatomic, strong) Account *destinationAccount;
@property (nonatomic, strong) PaymentProvider *provider;
@property (nonatomic, strong) TransferCurrencyRates *exchange;
@property (nonatomic, strong) NSArray<CheckTransferWarningMessages*> *warningMessages;
@property (nonatomic, strong) NSDictionary *knpList;

//when init paybox transfer
@property (nonatomic, strong) NSNumber *servicelogId;
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, assign) BOOL isPinVerifyRequired;
@property (nonatomic, strong) NSString *alias;
@property (nonatomic, strong) NSString *processLink;
@property (nonatomic, strong) NSString *backLink;

+ (FromExternalToAccountButtonCheckResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end