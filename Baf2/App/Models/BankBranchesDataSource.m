//
//  BankBranchesDataSource.m
//  BAF
//
//  Created by Shyngys Kassymov on 24.07.15.
//
//

#import "BankBranchesDataSource.h"
#import "BankBranchTableViewCell.h"

@interface BankBranchesDataSource ()
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, copy) NSString *cellIdentifier;
@property (nonatomic, copy) BankBranchTableViewCellConfigureBlock configureCellBlock;
@end

@implementation BankBranchesDataSource

- (id)init {
    return nil;
}

- (id)initWithItems:(NSArray *)anItems
     cellIdentifier:(NSString *)aCellIdentifier
 configureCellBlock:(BankBranchTableViewCellConfigureBlock)aConfigureCellBlock {
    self = [super init];
    if (self) {
        self.items = anItems;
        self.cellIdentifier = aCellIdentifier;
        self.configureCellBlock = [aConfigureCellBlock copy];
    }
    return self;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.items.count) {
        return self.items[(NSUInteger) indexPath.row];
    } else {
        return nil;
    }
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (BankBranchTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BankBranchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    if (!cell) {
        cell = [[BankBranchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:self.cellIdentifier];
    }
    id item = [self itemAtIndexPath:indexPath];
    
    self.configureCellBlock(cell, item);
    
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    
    return cell;
}

@end
