//
// Created by Askar Mustafin on 4/7/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "User.h"
#import "NSString+Ext.h"
#import "NSDate+Ext.h"

@implementation User {}


+ (instancetype)sharedInstance {
    static User *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[User alloc] init];
        sharedInstance.loggedIn = NO;
    });
    return sharedInstance;
}

- (void)setUserProfileData:(NSDictionary *)datas {

    User *user = [User sharedInstance];

    user.loggedIn = YES;


    if (datas[@"session"]) {
        user.sessionID = datas[@"session"];
    }

    NSDictionary *profile = datas[@"profile"];

    user.userID = [self intValueFrom:datas forKey:@"id"];
    user.nickname = [self stringValueFrom:datas forKey:@"nickname"];
    user.firstname = [self stringValueFrom:datas forKey:@"firstName"];
    user.lastname = [self stringValueFrom:datas forKey:@"lastName"];
    user.middlename = [self stringValueFrom:datas forKey:@"middleName"];
    user.email = [self stringValueFrom:datas forKey:@"email"];
    user.phoneNumber = [self stringValueFrom:datas forKey:@"phone"];
    user.isEmailConfirmed = [self boolValueFrom:datas forKey:@"emailChecked"];
    user.isPhoneConfirmed = [self boolValueFrom:datas forKey:@"phoneChecked"];
    user.isClient = YES;//[self boolValueFrom:userObject forKey:@"is_client"];
    user.avatarImageUrlString = [self stringValueFrom:datas forKey:@"avatar"];
    user.iin = [self stringValueFrom:datas forKey:@"iin"];
    user.locale = [self stringValueFrom:datas forKey:@"locale"];
    user.isUpdatedByIin = [self boolValueFrom:datas forKey:@"updatedByIin"];

    
    if (profile) {
        user.birthday = [self stringValueFrom:profile[@"birthday"] forKey:@"value"];
        user.gender = [self stringValueFrom:profile[@"gender"] forKey:@"value"];
        user.bank = [self stringValueFrom:profile[@"bank"] forKey:@"value"];
        user.card = [self stringValueFrom:profile[@"card"] forKey:@"value"];
    }
//
//
//    __weak User *wSelf = self;
//    if (!self.isEmailConfirmed || !self.isPhoneConfirmed)
//    {
//        [UIHelper showEmailPhoneConfirmationWindow:YES target:^{
//            [AuthApi getClientBySession:wSelf.sessionID success:^(id response2) {
//                [wSelf setUserProfileData:response2];
//            } failure:^(NSString *code, NSString *message) {}];
//        }];
//    }
}

+ (NSString *)sessionID {
    NSString *sessionID = [[User sharedInstance] sessionID];
    return sessionID?sessionID:@"";
}

- (NSString *)fio {
    return [NSString stringWithFormat:@"%@ %@", [[User sharedInstance] firstname], [[User sharedInstance] lastname]];
}

+ (void)logout {
    [User sharedInstance].loggedIn = false;
    [User sharedInstance].sessionID = nil;
    
    [User sharedInstance].userID = 0;
    [User sharedInstance].nickname = nil;
    [User sharedInstance].firstname = nil;
    [User sharedInstance].lastname = nil;
    [User sharedInstance].middlename = nil;
    [User sharedInstance].email = nil;
    [User sharedInstance].phoneNumber = nil;
    [User sharedInstance].isEmailConfirmed = false;
    [User sharedInstance].isPhoneConfirmed = false;
    [User sharedInstance].isClient = false;
    [User sharedInstance].avatarImageUrlString = nil;
    [User sharedInstance].birthday = nil;
    [User sharedInstance].gender = nil;
    [User sharedInstance].bank = nil;
    [User sharedInstance].card = nil;
    [User sharedInstance].accountsResponse = nil;

    [User sharedInstance].graces = 1000;
    [User sharedInstance].expireDays = 1000;
    [User sharedInstance].isPasswordExpired = false;
}

#pragma mark Methods

+(BOOL)isEmailAndPhoneConfirmed {
    return ([[self sharedInstance] isEmailConfirmed] && [[self sharedInstance] isPhoneConfirmed]);
}
+(BOOL)isEmailAndPhoneConfirmedM {
    BOOL b = [self isEmailAndPhoneConfirmed];
    if(!b) {
#if TARGET_BAF
        [UIHelper showEmailPhoneConfirmationWindow:YES target:^{
            
        }];
#endif
    }
    return b;
}

- (NSString *)birthdayBeautyFormat {
    NSDate *birthdate = [[User sharedInstance].birthday dateWithDateFormat:@"dd/MM/yyyy"];
    return [birthdate stringWithDateFormat:@"dd.MM.yyyy"];
}

@end