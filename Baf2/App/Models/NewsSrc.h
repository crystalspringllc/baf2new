#import "ModelObject.h"


@interface NewsSrc : ModelObject <NSCoding> 

@property (nonatomic, copy) NSString *avaUrl;
@property (nonatomic, copy) NSNumber *newsSrcId;
@property (nonatomic, copy) NSString *nick;

+ (NewsSrc *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
