#import "CardCity.h"

@implementation CardCity

@synthesize code;
@synthesize name;

+ (CardCity *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CardCity *instance = [[CardCity alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    [self setValuesForKeysWithDictionary:aDictionary];

}


@end
