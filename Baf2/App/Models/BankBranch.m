//
//  BankBranch.m
//  BAF
//
//  Created by Almas Adilbek on 7/23/15.
//
//

#import "BankBranch.h"

@implementation BankBranch

- (CLLocationCoordinate2D)position {
    return CLLocationCoordinate2DMake(self.latitude, self.longitude);
}

- (CLLocationCoordinate2D)location {
    return [self position];
}

+ (NSArray *)getSortedBankBranchesFrom:(NSArray *)bankBranches {
    NSArray *sortedBankBranches = [NSArray new];
    
    if (bankBranches && [bankBranches isKindOfClass:[NSArray class]]) {
        // sort by distance
        sortedBankBranches = [self quickSort:bankBranches];
        
        // take only 50 items
        if ([sortedBankBranches count] > 50) {
            sortedBankBranches = [sortedBankBranches subarrayWithRange:NSMakeRange(0, 50)];
        }
    }
    
    return sortedBankBranches;
}

+ (NSArray *)quickSort:(NSArray *)bankBranches {
    NSMutableArray *unsortedDataArray = [[NSMutableArray alloc] initWithArray:bankBranches];
    NSMutableArray *lessArray = [[NSMutableArray alloc] init];
    NSMutableArray *greaterArray = [[NSMutableArray alloc] init];
    
    if ([unsortedDataArray count] < 1) {
        return nil;
    }
    
    int randomPivotPoint = arc4random() % [unsortedDataArray count];
    BankBranch *pivotValue = [unsortedDataArray objectAtIndex:randomPivotPoint];
    [unsortedDataArray removeObjectAtIndex:randomPivotPoint];
    
    for (BankBranch *bankBranch in unsortedDataArray) {
        if (bankBranch.distanceToUser < pivotValue.distanceToUser) {
            [lessArray addObject:bankBranch];
        } else {
            [greaterArray addObject:bankBranch];
        }
    }
    
    NSMutableArray *sortedArray = [[NSMutableArray alloc] init];
    [sortedArray addObjectsFromArray:[self quickSort:lessArray]];
    [sortedArray addObject:pivotValue];
    [sortedArray addObjectsFromArray:[self quickSort:greaterArray]];
    
    return sortedArray;
}

@end
