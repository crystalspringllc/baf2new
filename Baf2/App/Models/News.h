

#import "ModelObject.h"
#import "Comment.h"
#import "NewsInfo.h"
#import "NewsSrc.h"
#import "JSQMessage.h"

@interface News : ModelObject <NSCoding, JSQMessageData>

@property (nonatomic, copy)   NSArray  *comments;
@property (nonatomic, copy)   NSString *newsDate;
@property (nonatomic, copy)   NSString *newsId;
@property (nonatomic, copy)   NSArray  *images;
@property (nonatomic, strong) NewsInfo *info;
@property (nonatomic, assign) BOOL      isAdminPost;
@property (nonatomic, assign) BOOL      like;
@property (nonatomic, copy)   NSNumber *likes;
@property (nonatomic, assign) BOOL      perm;
@property (nonatomic, copy)   NSNumber *sessionId;
@property (nonatomic, copy)   NSString *share;
@property (nonatomic, strong) NewsSrc  *src;
@property (nonatomic, copy)   NSString *type;

- (void)updateAfterLike:(NSDictionary *)params;

- (NSDate *)formattedDate;
- (NSString *)formattedDateString;

+ (News *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
