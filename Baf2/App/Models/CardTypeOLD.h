#import <Foundation/Foundation.h>

@interface CardTypeOLD : NSObject {

    NSString *brand;
    NSString *code;
    NSString *descriptionText;
    NSNumber *enable;
    NSNumber *cardTypeId;
    NSString *logo;
    NSString *name;
    NSArray *products;
    NSString *sicCode;

}

@property (nonatomic, copy) NSString *brand;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *descriptionText;
@property (nonatomic, copy) NSNumber *enable;
@property (nonatomic, copy) NSNumber *cardTypeId;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSArray *products;
@property (nonatomic, copy) NSString *sicCode;

+ (CardTypeOLD *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end