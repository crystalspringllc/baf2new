//
//  BankBranchesDataSource.h
//  BAF
//
//  Created by Shyngys Kassymov on 24.07.15.
//
//

#import <Foundation/Foundation.h>
#import "BankBranch.h"

typedef void (^BankBranchTableViewCellConfigureBlock)(id cell, BankBranch *bankBranch);

@interface BankBranchesDataSource : NSObject <UITableViewDataSource>

- (id)initWithItems:(NSArray *)anItems
     cellIdentifier:(NSString *)aCellIdentifier
 configureCellBlock:(BankBranchTableViewCellConfigureBlock)aConfigureCellBlock;

- (id)itemAtIndexPath:(NSIndexPath *)indexPath;

@end
