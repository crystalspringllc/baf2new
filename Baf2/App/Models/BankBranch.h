//
//  BankBranch.h
//  BAF
//
//  Created by Almas Adilbek on 7/23/15.
//
//

#import <CoreLocation/CoreLocation.h>

@interface BankBranch : NSObject

@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *address;
@property(nonatomic, copy) NSString *workTime;
@property(nonatomic, copy) NSString *phone;
@property(nonatomic, copy) NSString *email;
@property(nonatomic, assign) NSInteger cityId;
@property(nonatomic, assign) double longitude;
@property(nonatomic, assign) double latitude;
@property(nonatomic, assign) int distanceToUser;

@property (nonatomic) CLLocationCoordinate2D location;

+ (NSArray *)getSortedBankBranchesFrom:(NSArray *)bankBranches;

@end
