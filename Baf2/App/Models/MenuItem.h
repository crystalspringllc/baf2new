//
// Created by Askar Mustafin on 9/5/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MENU_TITLE_PROFILE           NSLocalizedString(@"profile", nil)
#define MENU_TITLE_MAINVC            NSLocalizedString(@"preview", nil)
#define MENU_TITLE_CARDSANDACCOUNTS  NSLocalizedString(@"accounts_and_cards", nil)
#define MENU_TITLE_PAYMENTS          NSLocalizedString(@"ga_payments", nil)
#define MENU_TITLE_TRANSFERS         NSLocalizedString(@"ga_transfers", nil)
#define MENU_TITLE_HISTORY           NSLocalizedString(@"ga_operation_history", nil)
#define MENU_TITLE_SUPPORT           NSLocalizedString(@"chat_with_bank", nil)
#define MENU_TITLE_SETTINGS          NSLocalizedString(@"settings", nil)
#define MENU_TITLE_EXIT              NSLocalizedString(@"exit", nil)
#define MENU_TITLE_OPPORTUNITIES     NSLocalizedString(@"opportunities", nil)
#define MENU_TITLE_BECOMECLIENT      NSLocalizedString(@"become_client", nil)
#define MENU_TITLE_LOGINREGISTER     NSLocalizedString(@"login_registration", nil)
#define MENU_TITLE_NEWS              NSLocalizedString(@"ga_news", nil)
#define MENU_TITLE_EXCHANGERATES     NSLocalizedString(@"ga_exchange_rate", nil)
#define MENU_TITLE_BANKOFFERS        NSLocalizedString(@"ga_offers", nil)
#define MENU_TITLE_CONTACTS          NSLocalizedString(@"contacts", nil)
#define MENU_TITLE_ATMANDDEP         NSLocalizedString(@"ga_atm", nil)
#define MENU_TITLE_PRIVILEGE         NSLocalizedString(@"bank_privileges", nil)
#define MENU_TITLE_POLICY            NSLocalizedString(@"bank_politics", nil)
#define MENU_TITLE_BANKDETAILS       NSLocalizedString(@"requisite_of", nil)
#define MENU_TITLE_EPIN              @"EPIN"
#define MENU_TITLE_TICKERS           @"Акции Банка"
#define MENU_TITLE_DEPOSIT           @"Онлайн Депозит"
#define MENU_TITLE_EASYTRANSFER      NSLocalizedString(@"card_to_card_transfer", nil)

@interface MenuItem : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) UIViewController *correspondingVC;

+ (NSArray *)userMenuItems;
+ (NSArray *)guestMenuItems;
- (id)initWithName:(NSString *)name icon:(NSString *)icon color:(UIColor *)color vc:(UIViewController *)vc;
+ (NSIndexPath *)rowIndexPathIn:(NSArray *)array byName:(NSString *)name;
@end
