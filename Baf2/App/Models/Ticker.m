//
// Created by Askar Mustafin on 7/18/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "Ticker.h"


@implementation Ticker {

}

@synthesize message, difference, dt_actual, visible, updated, name, last_price, dt_updated, code, warning, info;

+ (Ticker *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Ticker *instance = [[Ticker alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.message = [self stringValueForKey:@"message"];
    self.difference = [self numberValueForKey:@"difference"];
    self.dt_actual = [self stringValueForKey:@"dt_actual"];
    self.visible = [self boolValueForKey:@"visible"];
    self.updated = [self stringValueForKey:@"updated"];
    self.name = [self stringValueForKey:@"name"];
    self.last_price = [self numberValueForKey:@"last_price"];
    self.dt_updated = [self stringValueForKey:@"dt_updated"];
    self.code = [self stringValueForKey:@"code"];
    self.warning = [self stringValueForKey:@"warning"];

    if (objectData[@"info"]) {
        self.info = objectData[@"info"];
    }

    self.infokeys = [self arrayValueForKey:@"info_keys"];

    objectData = nil;
}

@end
