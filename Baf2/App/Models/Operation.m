//
//  Operation.m
//  Myth
//
//  Created by Almas Adilbek on 11/30/12.
//
//

#import "Operation.h"

@implementation Operation

@synthesize amount, contractId, operationID, date, descriptionText, providerId;

-(Operation *)initWithDatas:(NSDictionary *)datas {
    self = [super init];
    if(self) {
        self.amount = [self numberValueFrom:datas forKey:@"amount"];
        self.operationID = [self numberValueFrom:datas forKey:@"id"];
        self.date = [self dateValueFrom:datas forKey:@"dtcreated" dateFormat:@""];
        self.contractId = [self numberValueFrom:datas forKey:@"contractId"];
        self.providerId = [self numberValueFrom:datas forKey:@"providerId"];
        self.descriptionText = [self stringValueFrom:datas forKey:@"description"];
    }
    return self;
}

@end
