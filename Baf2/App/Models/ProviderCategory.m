//
//  ProviderCategory.m
//  
//
//  Created by Almas Adilbek on 10/16/13.
//  Copyright (c) 2013. All rights reserved.
//

#import "ProviderCategory.h"

#import "PaymentProvider.h"

@implementation ProviderCategory {
    NSArray *supportedProviders;
}

@synthesize categoryLogo, categoryName, providerCategoryId, paymentProviders;

+ (ProviderCategory *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ProviderCategory *instance = [[ProviderCategory alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.categoryLogo = aDictionary[@"categoryLogo"];
    self.categoryName = aDictionary[@"categoryName"];
    self.providerCategoryId = aDictionary[@"id"];

    NSArray *receivedPaymentProviders = aDictionary[@"paymentProviders"];
    if ([receivedPaymentProviders isKindOfClass:[NSArray class]]) {

        NSMutableArray *populatedPaymentProviders = [NSMutableArray arrayWithCapacity:[receivedPaymentProviders count]];
        for (NSDictionary *item in receivedPaymentProviders) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [populatedPaymentProviders addObject:[PaymentProvider instanceFromDictionary:item]];
            }
        }

        self.paymentProviders = populatedPaymentProviders;

    }
}

-(NSArray *)supportedProviders
{
    NSMutableArray *arr = [NSMutableArray array];
    for (PaymentProvider *provider in self.paymentProviders) {
        if ([provider isProviderAvailableForPayment]) {
            [arr addObject:provider];
        }
    }
    return arr;
}

-(BOOL)hasProviders {
    return self.paymentProviders.count > 0;
}

- (BOOL)hasSupportedProviders {
    return [[self supportedProviders] count] > 0;
}

-(NSInteger)providersCount {
    return self.paymentProviders.count;
}

#pragma mark -

+(NSArray *)responseToProviderCategories:(id)response {
    NSMutableArray *arr = [NSMutableArray array];
    for (NSDictionary *o in response) {
        ProviderCategory *category = [self instanceFromDictionary:o];
        category.paymentProviders = [category supportedProviders];

        // Exclude charity provider
        if([category.providerCategoryId intValue] != kCharityProviderId) {
            [arr addObject:category];
        }
    }
    return arr;
}

#pragma mark NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.categoryLogo = [decoder decodeObjectForKey:@"categoryLogo"];
    self.categoryName = [decoder decodeObjectForKey:@"categoryName"];
    self.providerCategoryId = [decoder decodeObjectForKey:@"providerCategoryId"];
    self.paymentProviders = [decoder decodeObjectForKey:@"paymentProviders"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.categoryLogo forKey:@"paymentProviderListWS"];
    [encoder encodeObject:self.categoryName forKey:@"category"];
    [encoder encodeObject:self.providerCategoryId forKey:@"providerCategoryId"];
    [encoder encodeObject:self.paymentProviders forKey:@"paymentProviders"];
}

@end
