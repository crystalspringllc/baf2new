//
//  PaymentProvider.h
//  
//
//  Created by Almas Adilbek on 10/16/13.
//  Copyright (c) 2013. All rights reserved.
//

#import "ModelObject.h"

typedef NS_ENUM(NSInteger, InfoRequestType) {
    InfoRequestTypeNone = 1,
    InfoRequestTypeGetInvoice = 2,
    InfoRequestTypeGetBalance = 3,
    InfoRequestTypeGetAdditionalInfo = 4,
    InfoRequestTypeGetSubContract = 5,
    InfoRequestTypeSelectContract = 6,
    InfoRequestTypeGetCourses = 7,
    InfoRequestTypeKaspiBank = 8,
    InfoRequestTypeGetCoursesWithRounding = 9,
    InfoRequestTypeSimplePaymentCheckAccount = 10,
    InfoRequestTypeEnt = 11
};

@interface PaymentProvider : ModelObject <NSCoding>

@property (nonatomic, copy) NSString *contractFormat;
@property (nonatomic, copy) NSString *contractMask;
@property (nonatomic, copy) NSString *contractName;
@property (nonatomic, copy) NSString *contractRegExp;
@property (nonatomic, assign) BOOL disabled;
@property (nonatomic, copy) NSString *info;
@property (nonatomic, copy) NSNumber *infoRequestType;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *paymentProviderId;
@property (nonatomic, copy) NSString *providerCode;
@property (nonatomic) BOOL hasDisposableContracts;
@property (nonatomic, strong) NSNumber *serviceId;
@property (nonatomic) BOOL longProcessingTime;
@property (nonatomic, strong) NSString *categoryProviderName;

+ (PaymentProvider *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

-(BOOL)isProviderAvailableForPayment;
-(BOOL)isInfoRequestTypeEquialTo:(int)number;

- (InfoRequestType)getInfoRequestTypeEnum;

@end
