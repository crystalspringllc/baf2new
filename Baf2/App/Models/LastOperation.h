//
//  LastOperation.h
//  Myth
//
//  Created by Almas Adilbek on 12/25/12.
//
//

#import "ModelObject.h"

@class LastOperationDetails;

@interface LastOperation : ModelObject {

}

@property (nonatomic, copy) NSString *dateString;
@property (nonatomic, strong) NSString *operationID;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *operationType;
@property (nonatomic, copy) NSString *operationText;
@property (nonatomic, assign) CGFloat amount;
@property (nonatomic, assign) CGFloat amountCommission;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, copy) NSString *providerLogo;
@property (nonatomic, copy) NSString *providerTitle;

@property(nonatomic, strong) LastOperationDetails *details;
@property(nonatomic, strong) NSArray *logs;

-(LastOperation *)initWithDatas:(NSDictionary *)datas;
-(NSDate *)date;

-(BOOL)isSuccess;
-(BOOL)isFailed;
-(BOOL)isInProccess;

-(BOOL)isPaymentType;
-(BOOL)isTransferType;
-(BOOL)isSpringdocType;
- (BOOL)isMulticcardInnerConvert;

-(NSString *)statusImageName;

- (NSString *)currencyString;

- (CGFloat)comission;

- (NSString *)formattedDate;


//currency = KZT;
//operationAmount = "10.00";
//operationAmountCommission = "10.00";
//operationDate = "24/02/2015 13:22:36";
//operationId = UxMZBmr3BlSX;
//operationStatus = "IN_PROGRESS";
//operationText = "\U041f\U0435\U0440\U0435\U0432\U043e\U0434 \U0441 \U043a\U0430\U0440\U0442\U044b \U043d\U0430 \U0447\U0443\U0436\U0443\U044e \U043a\U0430\U0440\U0442\U0443";
//operationType = TRANSFER;
//"provider_logo" = "";
//"provider_title" = "MY_CARD_TO_OTHER_CARD";

@end
