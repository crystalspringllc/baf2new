#import "ModelObject.h"


@interface BankInfo : ModelObject {

}

@property (nonatomic, copy) NSString *bic;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *namekz;

+ (BankInfo *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
