//
//  Atm.m
//  BAF
//
//  Created by Almas Adilbek on 7/7/15.
//
//

#import "Atm.h"

@implementation Atm

- (CLLocationCoordinate2D)position {
    return CLLocationCoordinate2DMake(self.latitude, self.longitude);
}

- (CLLocationCoordinate2D)location {
    return [self position];
}

+ (NSArray *)getSortedAtmsFrom:(NSArray *)atms {
    NSArray *sortedAtms = [NSArray new];
    
    if (atms && [atms isKindOfClass:[NSArray class]]) {
        // sort by distance
        sortedAtms = [self quickSort:atms];
    
        // take only 50 items
        if ([sortedAtms count] > 50) {
            sortedAtms = [sortedAtms subarrayWithRange:NSMakeRange(0, 50)];
        }
    }
    
    return sortedAtms;
}

+ (NSArray *)quickSort:(NSArray *)atms {
    NSMutableArray *unsortedDataArray = [[NSMutableArray alloc] initWithArray:atms];
    NSMutableArray *lessArray = [[NSMutableArray alloc] init];
    NSMutableArray *greaterArray = [[NSMutableArray alloc] init];
    
    if ([unsortedDataArray count] < 1) {
        return nil;
    }
    
    int randomPivotPoint = arc4random() % [unsortedDataArray count];
    Atm *pivotValue = [unsortedDataArray objectAtIndex:randomPivotPoint];
    [unsortedDataArray removeObjectAtIndex:randomPivotPoint];
    
    for (Atm *atm in unsortedDataArray) {
        if (atm.distanceToUser < pivotValue.distanceToUser) {
            [lessArray addObject:atm];
        } else {
            [greaterArray addObject:atm];
        }
    }
    
    NSMutableArray *sortedArray = [[NSMutableArray alloc] init];
    [sortedArray addObjectsFromArray:[self quickSort:lessArray]];
    [sortedArray addObject:pivotValue];
    [sortedArray addObjectsFromArray:[self quickSort:greaterArray]];
    
    return sortedArray;
}

@end
