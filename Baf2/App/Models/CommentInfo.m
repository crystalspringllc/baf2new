#import "CommentInfo.h"

@implementation CommentInfo
- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    [encoder encodeObject:self.date forKey:@"date"];
    [encoder encodeObject:self.message forKey:@"message"];
    [encoder encodeObject:self.newsId forKey:@"newsId"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super initWithCoder:decoder])) {
        self.date = [decoder decodeObjectForKey:@"date"];
        self.message = [decoder decodeObjectForKey:@"message"];
        self.newsId = [decoder decodeObjectForKey:@"newsId"];
    }
    return self;
}

+ (CommentInfo *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CommentInfo *instance = [[CommentInfo alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;
    
    self.date    = [self stringValueForKey:@"date"];
    self.message = [self stringValueForKey:@"message"];
    self.newsId  = [self stringValueForKey:@"newsId"];
    
    objectData = nil;
}

- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.date) {
        [dictionary setObject:self.date forKey:@"date"];
    }

    if (self.message) {
        [dictionary setObject:self.message forKey:@"message"];
    }

    if (self.newsId) {
        [dictionary setObject:self.newsId forKey:@"newsId"];
    }

    return dictionary;

}


@end
