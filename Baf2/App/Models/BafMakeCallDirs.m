//
//  BafMakeCallDirs.m
//  BAF
//
//  Created by Almas Adilbek on 11/1/14.
//
//

#import "BafMakeCallDirs.h"
#import "IdTitleObject.h"
#import "NSObject+Json.h"

@implementation BafMakeCallDirs

+(BafMakeCallDirs *)instanceWithResponse:(id)response
{
    BafMakeCallDirs *dir = [[BafMakeCallDirs alloc] init];

    if(![response isKindOfClass:[NSDictionary class]]) {
        response = [response JSONValue];
        response = response[@"data"];
    }

    // Topics
    NSArray *topics = response[@"topics"];
    if(topics) {
        NSMutableArray *array = [NSMutableArray array];
        for(NSDictionary *item in topics) {
            if (![item[@"id"] isEqualToString: @"factoring"]
                    && ![item[@"id"] isEqualToString:@"overdraft"])
            {
                [array addObject:[IdTitleObject instanceWithId:item[@"id"] title:item[@"name"]]];
            }
        }
        dir.topics = array;
    } else {
        dir.topics = @[];
    }

    // Cities
    NSArray *cities = response[@"cities"];
    if(cities) {
        NSMutableArray *array = [NSMutableArray array];
        for(NSDictionary *item in cities) {
            [array addObject:[IdTitleObject instanceWithId:item[@"id"] title:item[@"name"]]];
        }
        dir.cities = array;
    } else {
        dir.cities = @[];
    }

    return dir;
}

@end
