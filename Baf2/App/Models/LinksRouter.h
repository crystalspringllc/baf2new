//
//  LinksRouter.h
//  Myth
//
//  Created by Almas Adilbek on 08/20/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <MessageUI/MessageUI.h>

@interface LinksRouter : NSObject<MFMailComposeViewControllerDelegate, UIActionSheetDelegate>

+(LinksRouter *)sharedInstance;
-(void)routeUrl:(NSURL *)url;

@end
