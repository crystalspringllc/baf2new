//
// Created by Askar Mustafin on 5/26/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "ENTCheckAccountResult.h"


@implementation ENTCheckAccountResult {}

@synthesize aggregator;
@synthesize amount;
@synthesize info;
@synthesize reason;
@synthesize state;


+ (ENTCheckAccountResult *)instanceFromDictionary:(NSDictionary *)aDictionary {
    ENTCheckAccountResult *instance = [[ENTCheckAccountResult alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    objectData = aDictionary;

    self.aggregator = [self stringValueForKey:@"aggregator"];
    self.amount = [self numberValueForKey:@"amount"];
    self.info = [self stringValueForKey:@"info"];
    self.reason = [self stringValueForKey:@"reason"];
    self.state = [self stringValueForKey:@"state"];

    objectData = nil;
}

- (BOOL)isAccepted {
    return [self.state isEqualToString:@"ACCEPTED"];
}

- (NSString *)amountString {
    return [self.amount decimalFormatString];
}


@end