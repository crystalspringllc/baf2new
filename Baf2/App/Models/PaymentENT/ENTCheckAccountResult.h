//
// Created by Askar Mustafin on 5/26/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"


@interface ENTCheckAccountResult : ModelObject

@property (nonatomic, strong) NSString *aggregator;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSString *info;
@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *state;


+ (ENTCheckAccountResult *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;


- (BOOL)isAccepted;
- (NSString *)amountString;

@end