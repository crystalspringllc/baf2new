#import <Foundation/Foundation.h>

@interface CardProduct : NSObject {

    NSNumber *amount;
    NSNumber *category;
    NSArray *cities;
    NSString *code;
    NSString *descriptionText;
    NSString *logo;
    NSString *name;
    NSNumber *cardProductId;

}

@property (nonatomic, copy) NSNumber *amount;
@property (nonatomic, copy) NSNumber *category;
@property (nonatomic, copy) NSArray *cities;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *descriptionText;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *cardProductId;

+ (CardProduct *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
