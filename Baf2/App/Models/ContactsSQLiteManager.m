//
//  ContactsSQLiteManager.m
//  BAF
//
//  Created by Shyngys Kassymov on 24.07.15.
//
//

#import "ContactsSQLiteManager.h"
#import "Atm.h"
#import "BankBranch.h"
#import "KzCityDetector.h"
#import "LocationHelper.h"
#import "Common.h"
#import "FMDatabase.h"

@implementation ContactsSQLiteManager {
    NSMutableArray *atms, *bankBranches;
}

+ (instancetype)sharedContactsSQLiteManager {
    static ContactsSQLiteManager *sharedContactsSQLiteManager;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedContactsSQLiteManager = [ContactsSQLiteManager new];
        sharedContactsSQLiteManager.selectedObjectsType = MapObjectsTypeBranches;
    });
    
    return sharedContactsSQLiteManager;
}

- (NSArray *)loadAtms:(NSNumber *)cityId {
    CLLocationCoordinate2D currentLocation = [LocationHelper myLocationCoordinate];
    
    //NSString *path = [[NSBundle mainBundle] pathForResource:@"t_map_atms" ofType:@"sqlite" inDirectory:@"assets"];
    NSString *path = [Common getPathOfNeededFile:@"t_map_atms" withType:@"sqlite"];
    FMDatabase *db = [FMDatabase databaseWithPath:path];
    
    NSMutableArray * results = [NSMutableArray array];
    if ([db open]) {
        NSString *query = [NSString stringWithFormat:@"SELECT bank_name, address_web, coord_lat, coord_lng FROM t_map_atms WHERE bank_id in (1, 37) AND valid = 1 AND city = %@", cityId];
        FMResultSet *s = [db executeQuery:query];
        while ([s next]) {
            Atm *atm = [Atm new];
            atm.bankName = [s stringForColumn:@"bank_name"];
            atm.address = [s stringForColumn:@"address_web"];
            atm.longitude = [s doubleForColumn:@"coord_lat"];
            atm.latitude = [s doubleForColumn:@"coord_lng"];
            atm.distanceToUser = [[[CLLocation alloc] initWithLatitude:currentLocation.latitude longitude:currentLocation.longitude] distanceFromLocation:[[CLLocation alloc] initWithLatitude:atm.latitude longitude:atm.longitude]];
            [results addObject:atm];
        }
        
        [db close];
    }
    
    return results;
}

- (NSArray *)loadBranches:(NSNumber *)cityId {
    CLLocationCoordinate2D currentLocation = [LocationHelper myLocationCoordinate];
    
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"baf_branches" ofType:@"sqlite" inDirectory:@"assets"];
    NSString *path = [Common getPathOfNeededFile:@"baf_branches" withType:@"sqlite"];
    FMDatabase *db = [FMDatabase databaseWithPath:path];

    NSMutableArray * results = [NSMutableArray array];
    if ([db open]) {
        NSString *query = [NSString stringWithFormat:@"SELECT title, address, work_time, phone, lat, lon, city FROM objects WHERE valid = 1 AND city = %@", cityId];
        FMResultSet *s = [db executeQuery:query];
        NSLog(@"result: %@", s);
        while ([s next]) {
            BankBranch *branch = [BankBranch new];
            branch.title = [s stringForColumn:@"title"];
            branch.address = [s stringForColumn:@"address"];
            branch.workTime = [s stringForColumn:@"work_time"];
            branch.phone = [s stringForColumn:@"phone"];
            branch.latitude = [s doubleForColumn:@"lat"];
            branch.longitude = [s doubleForColumn:@"lon"];
            branch.cityId = [s intForColumn:@"city"];
            branch.distanceToUser = [[[CLLocation alloc] initWithLatitude:currentLocation.latitude longitude:currentLocation.longitude] distanceFromLocation:[[CLLocation alloc] initWithLatitude:branch.latitude longitude:branch.longitude]];
            [results addObject:branch];
        }
        
        [db close];
    }
    
    return results;
}

@end
