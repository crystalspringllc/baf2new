#import "Comment.h"

#import "CommentInfo.h"
#import "CommentSrc.h"
#import "NSDate+Ext.h"

@implementation Comment
- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    [encoder encodeObject:self.date forKey:@"date"];
    [encoder encodeObject:self.commentId forKey:@"commentId"];
    [encoder encodeObject:self.images forKey:@"images"];
    [encoder encodeObject:self.info forKey:@"info"];
    [encoder encodeObject:[NSNumber numberWithBool:self.isAdminPost] forKey:@"isAdminPost"];
    [encoder encodeObject:[NSNumber numberWithBool:self.like] forKey:@"like"];
    [encoder encodeObject:self.likes forKey:@"likes"];
    [encoder encodeObject:[NSNumber numberWithBool:self.perm] forKey:@"perm"];
    [encoder encodeObject:self.sessionId forKey:@"sessionId"];
    [encoder encodeObject:self.share forKey:@"share"];
    [encoder encodeObject:self.src forKey:@"src"];
    [encoder encodeObject:self.type forKey:@"type"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super initWithCoder:decoder])) {
        self.date        = [decoder decodeObjectForKey:@"date"];
        self.commentId   = [decoder decodeObjectForKey:@"commentId"];
        self.images      = [decoder decodeObjectForKey:@"images"];
        self.info        = [decoder decodeObjectForKey:@"info"];
        self.isAdminPost = [(NSNumber *)[decoder decodeObjectForKey:@"isAdminPost"] boolValue];
        self.like        = [(NSNumber *)[decoder decodeObjectForKey:@"like"] boolValue];
        self.likes       = [decoder decodeObjectForKey:@"likes"];
        self.perm        = [(NSNumber *)[decoder decodeObjectForKey:@"perm"] boolValue];
        self.sessionId   = [decoder decodeObjectForKey:@"sessionId"];
        self.share       = [decoder decodeObjectForKey:@"share"];
        self.src         = [decoder decodeObjectForKey:@"src"];
        self.type        = [decoder decodeObjectForKey:@"type"];
    }
    return self;
}

+ (Comment *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Comment *instance = [[Comment alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;
    self.commentId   = [self stringValueForKey:@"id"];
    self.date        = [self stringValueForKey:@"date"];
    self.images      = [self arrayValueForKey:@"images"];
    self.info        = [CommentInfo instanceFromDictionary:aDictionary[@"info"]];
    self.isAdminPost = [self boolValueForKey:@"isAdminPost"];
    self.like        = [self boolValueForKey:@"like"];
    self.likes       = [self numberValueForKey:@"likes"];
    self.sessionId   = [self numberValueForKey:@"sessionId"];
    self.share       = [self stringValueForKey:@"share"];
    self.src         = [CommentSrc instanceFromDictionary:aDictionary[@"src"]];
    self.type        = [self stringValueForKey:@"type"];
    
    objectData = nil;

}

- (NSDate *)formattedDate {
    return [NSDate formattedDateFromString:self.date];
}

- (NSString *)formattedDateString {
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"dd.MM.yyyy HH:ss"];
    return [f stringFromDate:self.formattedDate];
}

- (void)updateAfterLike:(NSDictionary *)params {
    objectData = params;
    self.like        = [self boolValueForKey:@"like"];
    self.likes       = [self numberValueForKey:@"likes"];
    objectData = nil;
}


- (void)setValue:(id)value forKey:(NSString *)key {

    if ([key isEqualToString:@"images"]) {

        if ([value isKindOfClass:[NSArray class]]) {

            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                [myMembers addObject:valueMember];
            }

            self.images = myMembers;

        }

    } else if ([key isEqualToString:@"info"]) {

        if ([value isKindOfClass:[NSDictionary class]]) {
            self.info = [CommentInfo instanceFromDictionary:value];
        }

    } else if ([key isEqualToString:@"src"]) {

        if ([value isKindOfClass:[NSDictionary class]]) {
            self.src = [CommentSrc instanceFromDictionary:value];
        }

    } else {
        [super setValue:value forKey:key];
    }

}


- (void)setValue:(id)value forUndefinedKey:(NSString *)key {

    if ([key isEqualToString:@"id"]) {
        [self setValue:value forKey:@"commentId"];
    } else {
        [super setValue:value forUndefinedKey:key];
    }

}


- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.date) {
        [dictionary setObject:self.date forKey:@"date"];
    }

    if (self.commentId) {
        [dictionary setObject:self.commentId forKey:@"commentId"];
    }

    if (self.images) {
        [dictionary setObject:self.images forKey:@"images"];
    }

    if (self.info) {
        [dictionary setObject:self.info forKey:@"info"];
    }

    [dictionary setObject:[NSNumber numberWithBool:self.isAdminPost] forKey:@"isAdminPost"];

    [dictionary setObject:[NSNumber numberWithBool:self.like] forKey:@"like"];

    if (self.likes) {
        [dictionary setObject:self.likes forKey:@"likes"];
    }

    [dictionary setObject:[NSNumber numberWithBool:self.perm] forKey:@"perm"];

    if (self.sessionId) {
        [dictionary setObject:self.sessionId forKey:@"sessionId"];
    }

    if (self.share) {
        [dictionary setObject:self.share forKey:@"share"];
    }

    if (self.src) {
        [dictionary setObject:self.src forKey:@"src"];
    }

    if (self.type) {
        [dictionary setObject:self.type forKey:@"type"];
    }

    return dictionary;

}


@end
