#import "Captcha.h"

@implementation Captcha

@synthesize bitmap, code, sessionKey;

+ (Captcha *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Captcha *instance = [[Captcha alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    self.bitmap = [self stringValueFrom:aDictionary forKey:@"bitmap"];
    self.code = [self numberValueFrom:aDictionary forKey:@"code"];
    self.sessionKey = [self stringValueFrom:aDictionary forKey:@"session_key"];

}


@end
