//
//  Location.m
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "Location.h"

@implementation Location

+ (Location *)instanceFromDictionary:(NSDictionary *)aDictionary {
    Location *instance = [[Location alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.locationId = [self stringValueForKey:@"id"];
    self.code = [self stringValueForKey:@"code"];
    self.name = [self stringValueForKey:@"name"];
    
    objectData = nil;
}

@end
