#import <Foundation/Foundation.h>

@interface CardCity : NSObject {

    NSNumber *code;
    NSString *name;

}

@property (nonatomic, copy) NSNumber *code;
@property (nonatomic, copy) NSString *name;

+ (CardCity *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
