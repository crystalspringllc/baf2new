//
//  Page.m
//  Myth
//
//  Created by Almas Adilbek on 12/24/12.
//
//

#import "Page.h"

@implementation Page

@synthesize name, pageID, meLiked, likes, logoUrl;

-(id)initWithData:(NSDictionary *)data {
    self = [super init];
    if(self) {
        self.pageID = [self intValueFrom:data forKey:@"id"];
        self.name = [self stringValueFrom:data forKey:@"pageName"];
        self.logoUrl = [self stringValueFrom:data forKey:@"logo"];
        self.likes = [self intValueFrom:data forKey:@"likes"];
        self.meLiked = [self boolValueFrom:data forKey:@"liked"];
    }
    return self;
}

+(NSArray *)arrayOfPages:(NSArray *)pagesArray {
    NSMutableArray *pages = [NSMutableArray arrayWithCapacity:pagesArray.count];
    for (NSDictionary *o in pagesArray) {
        Page *page = [[Page alloc] initWithData:o];
        [pages addObject:page];
    }
    return pages;
}

@end
