//
//  Payment.m
//  Myth
//
//  Created by Almas Adilbek on 11/29/12.
//
//

#import "Payment.h"

@implementation Payment

@synthesize amount, contractAlias, contractIdentifier, providerCode;

-(Payment *)initWithDatas:(NSDictionary *)datas {
    self = [super init];
    if(self) {
        self.amount = [self numberValueFrom:datas forKey:@"amount"];
        self.contractAlias = [self stringValueFrom:datas forKey:@"contract_alias"];
        self.contractIdentifier = [self stringValueFrom:datas forKey:@"contract"];
        self.providerCode = [self stringValueFrom:datas forKey:@"provider_code"];
    }
    return self;
}

@end
