//
//  ContactsDataSource.h
//  BAF
//
//  Created by Shyngys Kassymov on 24.07.15.
//
//

#import <Foundation/Foundation.h>
#import "Atm.h"

typedef void (^AtmTableViewCellConfigureBlock)(id cell, Atm *atm);

@interface AtmsDataSource : NSObject <UITableViewDataSource>

- (id)initWithItems:(NSArray *)anItems
     cellIdentifier:(NSString *)aCellIdentifier
 configureCellBlock:(AtmTableViewCellConfigureBlock)aConfigureCellBlock;

- (id)itemAtIndexPath:(NSIndexPath *)indexPath;

@end
