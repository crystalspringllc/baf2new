#import "CommentSrc.h"

@implementation CommentSrc
- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    [encoder encodeObject:self.avaUrl forKey:@"avaUrl"];
    [encoder encodeObject:self.commentSrcId forKey:@"commentSrcId"];
    [encoder encodeObject:self.nick forKey:@"nick"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super initWithCoder:decoder])) {
        self.avaUrl = [decoder decodeObjectForKey:@"avaUrl"];
        self.commentSrcId = [decoder decodeObjectForKey:@"commentSrcId"];
        self.nick = [decoder decodeObjectForKey:@"nick"];
    }
    return self;
}

+ (CommentSrc *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CommentSrc *instance = [[CommentSrc alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;
    
    self.avaUrl       = [self stringValueForKey:@"avaUrl"];
    self.commentSrcId = [self numberValueForKey:@"id"];
    self.nick         = [self stringValueForKey:@"nick"];
    
    objectData = nil;
    
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {

    if ([key isEqualToString:@"id"]) {
        [self setValue:value forKey:@"commentSrcId"];
    } else {
        [super setValue:value forUndefinedKey:key];
    }

}


- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.avaUrl) {
        [dictionary setObject:self.avaUrl forKey:@"avaUrl"];
    }

    if (self.commentSrcId) {
        [dictionary setObject:self.commentSrcId forKey:@"commentSrcId"];
    }

    if (self.nick) {
        [dictionary setObject:self.nick forKey:@"nick"];
    }

    return dictionary;

}


@end
