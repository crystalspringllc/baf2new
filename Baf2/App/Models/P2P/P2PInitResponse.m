//
// Created by Askar Mustafin on 5/3/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "P2PInitResponse.h"



@implementation P2PInitResponse {}

@synthesize processLink;
@synthesize backLink;
@synthesize servicelogId;
@synthesize provider;
@synthesize uuid;
@synthesize clientId;
@synthesize type;
@synthesize session;

+ (P2PInitResponse *)instanceFromDictionary:(NSDictionary *)aDictionary {

    P2PInitResponse *instance = [[P2PInitResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.processLink = [self stringValueForKey:@"processLink"];
    self.backLink = [self stringValueForKey:@"backLink"];
    self.servicelogId = [self stringValueForKey:@"servicelogId"];
    self.provider = [self stringValueForKey:@"provider"];
    self.uuid = [self stringValueForKey:@"uuid"];
    self.clientId = [self stringValueForKey:@"clientId"];
    self.type = [self stringValueForKey:@"type"];
    self.session = [self stringValueForKey:@"session"];

    objectData = nil;
}

- (NSNumber *)getServiceLogIdNumber {
    int serviceLogIdInt = self.servicelogId;
    return @(serviceLogIdInt);
}

@end