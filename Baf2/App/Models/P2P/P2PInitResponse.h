//
// Created by Askar Mustafin on 5/3/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"


@interface P2PInitResponse : ModelObject

@property (nonatomic, strong) NSString *processLink;
@property (nonatomic, strong) NSString *backLink;
@property (nonatomic, strong) NSString *servicelogId;
@property (nonatomic, strong) NSString *provider;
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSString *clientId;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *session;

+ (P2PInitResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;


- (NSNumber *)getServiceLogIdNumber;

@end