//
// Created by Askar Mustafin on 4/7/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"
#import "AccountsResponse.h"

static NSString * const kUserSelectedCountry = @"kUserSelectedCountry";
static NSString * const kUserSelectedCountryName = @"kUserSelectedCountryName";
static NSString * const kUserSelectedCity = @"kUserSelectedCity";
static NSString * const kUserSelectedCityName = @"kUserSelectedCityName";

@interface User : ModelObject

@property (nonatomic, strong) NSString *sessionID;
@property (nonatomic, assign, getter=isLoggedIn) BOOL loggedIn;

@property (nonatomic, assign) NSInteger userID;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *firstname;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic, strong) NSString *middlename;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, assign) BOOL isEmailConfirmed;
@property (nonatomic, assign) BOOL isPhoneConfirmed;
@property (nonatomic, assign) BOOL isClient;
@property (nonatomic, strong) NSString *avatarImageUrlString;
@property (nonatomic, strong) NSString *birthday;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *bank;
@property (nonatomic, strong) NSString *card;
@property (nonatomic, strong) NSString *iin;
@property (nonatomic, strong) NSString *locale;
@property (nonatomic, assign) BOOL isUpdatedByIin;

@property (nonatomic) NSInteger graces;
@property (nonatomic) NSInteger expireDays;
@property (nonatomic) BOOL isPasswordExpired;

@property (nonatomic, assign) float birthdayAsTimeStamp;
@property (nonatomic, strong) NSString *phoneNumberFull;

@property (nonatomic, strong) AccountsResponse *accountsResponse;


+ (instancetype)sharedInstance;
- (void)setUserProfileData:(NSDictionary *)datas;
+ (NSString *)sessionID;

-(NSString *)fio;

+(BOOL)isEmailAndPhoneConfirmed;
+(BOOL)isEmailAndPhoneConfirmedM;

+ (void)logout;

- (NSString *)birthdayBeautyFormat;

@end