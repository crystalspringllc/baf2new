#import "ModelObject.h"


@interface CommentInfo : ModelObject <NSCoding>
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *newsId;

+ (CommentInfo *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
