//
//  PeopleContact.m
//  Baf2
//
//  Created by Shyngys Kassymov on 14.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "PeopleContact.h"

@implementation PeopleContact

- (NSString *)fullName {
    if (self.firstName && self.lastName) {
        return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
    } else if (self.firstName) {
        return self.firstName;
    } else if (self.lastName) {
        return self.lastName;
    }
    
    return @"";
}

@end
