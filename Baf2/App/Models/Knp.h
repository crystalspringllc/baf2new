//
//  Knp.h
//  Baf2
//
//  Created by Shyngys Kassymov on 03.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface Knp : ModelObject

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *knpDescription;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic) BOOL isLegal;

+ (Knp *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;
- (id)copyWithZone:(NSZone *)zone;

@end
