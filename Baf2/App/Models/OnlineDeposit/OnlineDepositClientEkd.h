//
// Created by Askar Mustafin on 10/24/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"

@class OnlineDepositEPDocument;


@interface OnlineDepositClientEkdDocument : ModelObject
@property (nonatomic, strong) NSString *docIssuedCode;
@property (nonatomic, strong) NSString *docTypeCode;
@property (nonatomic, strong) NSString *docnumber;
@property (nonatomic, strong) NSString *docIssuer;
@property (nonatomic, strong) NSString *docType;
@property (nonatomic, strong) NSString *issuedDate;
@property (nonatomic, strong) NSString *expireDate;
@property (nonatomic, strong) OnlineDepositEPDocument *epDocument;
+ (OnlineDepositClientEkdDocument *)instanceFromDictionary:(NSDictionary *)aDictionary;
@end

@interface OnlineDepositClientEkd : ModelObject
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *firstname;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic, strong) NSString *middlename;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *birthdate;
@property (nonatomic, strong) NSString *iin;
@property (nonatomic, strong) NSString *isresident;
@property (nonatomic, strong) OnlineDepositClientEkdDocument *document;
+ (OnlineDepositClientEkd *)instanceFromDictionary:(NSDictionary *)aDictionary;
@end
