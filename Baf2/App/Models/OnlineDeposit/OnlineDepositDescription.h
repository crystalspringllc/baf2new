#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface OnlineDepositDescription : ModelObject {

    NSString *additionalContributions;
    NSString *cashBack;
    NSString *compensation;
    NSString *conversionInBanking;
    NSString *dissolution;
    NSString *fixedRate;
    NSString *intrabankTransfer;
    NSString *minAmount;
    NSString *minBalance;
    NSString *partialWithdrawal;
    NSString *prolongation;
    NSString *replenishmentByAnyBankCard;

}

@property (nonatomic, copy) NSString *additionalContributions;
@property (nonatomic, copy) NSString *cashBack;
@property (nonatomic, copy) NSString *compensation;
@property (nonatomic, copy) NSString *conversionInBanking;
@property (nonatomic, copy) NSString *dissolution;
@property (nonatomic, copy) NSString *fixedRate;
@property (nonatomic, copy) NSString *intrabankTransfer;
@property (nonatomic, copy) NSString *minAmount;
@property (nonatomic, copy) NSString *minBalance;
@property (nonatomic, copy) NSString *partialWithdrawal;
@property (nonatomic, copy) NSString *prolongation;
@property (nonatomic, copy) NSString *replenishmentByAnyBankCard;

+ (OnlineDepositDescription *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSArray *)getDescriptions;

@end
