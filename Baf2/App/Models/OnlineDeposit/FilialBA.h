//
//  FilialBA.h
//  Baf2
//
//  Created by developer on 30.11.17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface FilialBA : ModelObject

@property (nonatomic, strong) NSString *filialTitle;
@property (nonatomic, strong) NSString *filialIdString;
@property (nonatomic, assign) BOOL isDepartment;
@property (nonatomic, strong) NSNumber *filialBin;

+ (FilialBA *)instanceFromDictionary:(NSDictionary *)aDictionary;

@end
