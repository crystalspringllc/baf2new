#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface OnlineDepositBenefits : ModelObject {

    NSString *footer;
    NSString *header;
    NSArray  *table;
    NSString *tableFooter;
    NSString *title;

}

@property (nonatomic, copy) NSString *footer;
@property (nonatomic, copy) NSString *header;
@property (nonatomic, copy) NSString *tableFooter;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSArray  *table;

+ (OnlineDepositBenefits *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
