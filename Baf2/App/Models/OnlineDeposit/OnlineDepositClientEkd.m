//
// Created by Askar Mustafin on 10/24/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositClientEkd.h"
#import "OnlineDepositEPDocument.h"


@implementation OnlineDepositClientEkd {}

+ (OnlineDepositClientEkd *)instanceFromDictionary:(NSDictionary *)aDictionary {
    OnlineDepositClientEkd *instance = [[OnlineDepositClientEkd alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.email = [self stringValueForKey:@"email"];
    self.firstname = [self stringValueForKey:@"firstname"];
    self.lastname = [self stringValueForKey:@"lastname"];
    self.middlename = [self stringValueForKey:@"middlename"];
    self.phone = [self stringValueForKey:@"phone"];
    self.birthdate = [self stringValueForKey:@"birthdate"];
    self.iin = [self stringValueForKey:@"iin"];
    self.isresident = [self stringValueForKey:@"isresident"];

    if (aDictionary[@"documents"][0]) {
        NSDictionary *document = aDictionary[@"documents"][0];
        OnlineDepositClientEkdDocument *ekdDocument = [OnlineDepositClientEkdDocument instanceFromDictionary:document];
        self.document = ekdDocument;
    }

    objectData = nil;
}

@end


@implementation OnlineDepositClientEkdDocument {}

+ (OnlineDepositClientEkdDocument *)instanceFromDictionary:(NSDictionary *)aDictionary {
    OnlineDepositClientEkdDocument *instance = [[OnlineDepositClientEkdDocument alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;
    
    self.docTypeCode = [self stringValueForKey:@"doctype"];
    self.docIssuedCode   = [self stringValueForKey:@"docissuer"];
    
    if (objectData[@"document"][@"E_P_DOCUMENT"]) {
        OnlineDepositEPDocument *epDocument1 = [OnlineDepositEPDocument instanceFromDictionary:objectData[@"document"][@"E_P_DOCUMENT"]];
        self.epDocument = epDocument1;
    }

    self.docnumber = self.epDocument.PDOCUMENTNUMBER;
    self.docIssuer = self.epDocument.DPDOCUMENTISSUER;
    self.docType = self.epDocument.DPDOCUMENTTYPE;
    self.issuedDate = self.epDocument.PDOCUMENTCREATEDATE;
    self.expireDate = self.epDocument.PDOCUMENTEXPIREDATE;

    objectData = nil;
}

@end
