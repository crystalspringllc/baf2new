#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface OnlineDepositBenefitTable : ModelObject {
    NSString *fIELD1;
    NSString *fIELD2;
    NSString *fIELD3;
}

@property (nonatomic, copy) NSString *fIELD1;
@property (nonatomic, copy) NSString *fIELD2;
@property (nonatomic, copy) NSString *fIELD3;

+ (OnlineDepositBenefitTable *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
