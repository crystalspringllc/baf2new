#import "OnlineDepositEPDocument.h"

@implementation OnlineDepositEPDocument

@synthesize DPDOCUMENTISSUER;
@synthesize DPDOCUMENTTYPE;
@synthesize DSTATUS;
@synthesize iD;
@synthesize PDOCUMENTCREATEDATE;
@synthesize PDOCUMENTEXPIREDATE;
@synthesize PDOCUMENTISSOTHER;
@synthesize PDOCUMENTNUMBER;
@synthesize SVERSIONINFO;

+ (OnlineDepositEPDocument *)instanceFromDictionary:(NSDictionary *)aDictionary {

    OnlineDepositEPDocument *instance = [[OnlineDepositEPDocument alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.SVERSIONINFO = [self stringValueForKey:@"S_VERSION_INFO"];
    self.PDOCUMENTCREATEDATE = [self stringValueForKey:@"P_DOCUMENT_CREATE_DATE"];
    self.DPDOCUMENTTYPE = [self stringValueForKey:@"D_P_DOCUMENT_TYPE"];
    self.PDOCUMENTISSOTHER = [self stringValueForKey:@"P_DOCUMENT_ISS_OTHER"];
    self.DPDOCUMENTISSUER = [self stringValueForKey:@"D_P_DOCUMENT_ISSUER"];
    self.PDOCUMENTEXPIREDATE = [self stringValueForKey:@"P_DOCUMENT_EXPIRE_DATE"];
    self.DSTATUS = [self stringValueForKey:@"D_STATUS"];
    self.PDOCUMENTNUMBER = [self stringValueForKey:@"P_DOCUMENT_NUMBER"];
    self.iD = [self stringValueForKey:@"ID"];

    objectData = nil;
}

@end