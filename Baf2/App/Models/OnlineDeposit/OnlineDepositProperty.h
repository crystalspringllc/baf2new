//
// Created by Askar Mustafin on 10/18/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"
#import "OnlineDepositDescription.h"
#import "OnlineDepositBenefits.h"


@interface OnlineDepositProperty : ModelObject

/**
 * @brief name of deposit
 */
@property (nonatomic, strong) NSString *name;

/**
 * @brief array of currency NSStrings like "USD" "EUR"
 */
@property (nonatomic, strong) NSArray *currencies;

/**
 * @brief array of period NSStrings like "12" "11"
 */
@property (nonatomic, strong) NSArray *periods;

/**
 * @brief each key named as period string and hold
 * dictionary of currency:percent type
 * @example
 *  "ratesMap": {
 *                  "12": {
 *                      "EUR": "1",
 *                      "KZT": "13.2",
 *                      "RUB": "1",
 *                      "USD": "1"
 *                  },...}
 *
 */
@property (nonatomic, strong) NSDictionary *ratesMap;

/**
 * @brief each key name as period string and hold
 * dictionary of currency:percent type
 * @example
 * "annualRatesMap": {
 *                  "12": {
 *                      "EUR": "1",
 *                      "KZT": "14",
 *                      "RUB": "1",
 *                      "USD": "1"
 *                  },...}
 */
@property (nonatomic, strong) NSDictionary *annualRatesMap;

/**
 * @brief each key name as currency string and hold
 * dictionary of min/max : number
 * @example
 * "investmentConstraint": {
 *                  "KZT": {
 *                      "min": "0",
 *                      "max": "1000000000"
 *                  }, ...}
 */
@property (nonatomic, strong) NSDictionary *investmentConstraint;

/**
 * @brief dictionary curr:number
 * @example
 * "minConversion": {
 *                  "EUR": "1.2", ...}
 */
@property (nonatomic, strong) NSDictionary *minConversion;

/**
 * @brief
 */
@property (nonatomic, strong) OnlineDepositDescription *propertyDescription;

/**
 * @brief
 */
@property (nonatomic, strong) OnlineDepositBenefits *benefits;

@property (nonatomic, strong) NSArray *benefitsArray;


+ (OnlineDepositProperty *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;


@end
