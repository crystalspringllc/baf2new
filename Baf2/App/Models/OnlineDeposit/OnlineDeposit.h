//
// Created by Askar Mustafin on 10/18/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"

@class OnlineDepositProperty;


@interface OnlineDeposit : ModelObject

@property (nonatomic, strong) NSArray <OnlineDepositProperty *> *properties;
@property (nonatomic, strong) NSString *code;// "ASTANA_DEPOSIT",
@property (nonatomic, strong) NSString *name;// "Астана",
@property (nonatomic, strong) NSString *logo;// "http://test.24.bankastana.kz/assets/images/dynamic/cards/big/",
@property (nonatomic, strong) NSString *categoryCode;// "DEPOSIT",
@property (nonatomic, strong) NSString *typeName;// "DEPOSIT",
@property (nonatomic, strong) NSArray *cities;
@property (nonatomic, strong) NSArray *currencies;

+ (OnlineDeposit *)instanceFromDictionary:(NSDictionary *)aDictionary;

- (NSArray *)getComposedInfo;
- (NSArray *)composeInfoCards;

- (NSString *)getDepositInterestRate:(NSString *)selectedTerm;
- (NSString *)getDepositEffectiveInterestRate:(NSString *)selectedTerm;

@end
