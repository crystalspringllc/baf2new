#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface OnlineDepositEPDocument : ModelObject {

    NSString *DPDOCUMENTISSUER;
    NSString *DPDOCUMENTTYPE;
    NSString *DSTATUS;
    NSString *iD;
    NSString *PDOCUMENTCREATEDATE;
    NSString *PDOCUMENTEXPIREDATE;
    NSString *PDOCUMENTISSOTHER;
    NSString *PDOCUMENTNUMBER;
    NSString *SVERSIONINFO;

}

@property (nonatomic, copy) NSString *DPDOCUMENTISSUER;
@property (nonatomic, copy) NSString *DPDOCUMENTTYPE;
@property (nonatomic, copy) NSString *DSTATUS;
@property (nonatomic, copy) NSString *iD;
@property (nonatomic, copy) NSString *PDOCUMENTCREATEDATE;
@property (nonatomic, copy) NSString *PDOCUMENTEXPIREDATE;
@property (nonatomic, copy) NSString *PDOCUMENTISSOTHER;
@property (nonatomic, copy) NSString *PDOCUMENTNUMBER;
@property (nonatomic, copy) NSString *SVERSIONINFO;

+ (OnlineDepositEPDocument *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
