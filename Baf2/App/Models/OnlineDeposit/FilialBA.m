//
//  FilialBA.m
//  Baf2
//
//  Created by developer on 30.11.17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "FilialBA.h"

@implementation FilialBA

+ (FilialBA *)instanceFromDictionary:(NSDictionary *)aDictionary {
    
    FilialBA *instance = [[FilialBA alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
  
    self.filialTitle    = [self stringValueForKey:@"filial_alias"];
    self.filialIdString = [self stringValueForKey:@"filial"];
    self.filialBin      = [self numberValueForKey:@"filial_bin"];
    self.isDepartment   = [self boolValueForKey:@"is_department"];
    

    objectData = nil;
}

@end
