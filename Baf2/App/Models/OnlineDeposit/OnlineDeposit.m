//
// Created by Askar Mustafin on 10/18/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDeposit.h"
#import "OnlineDepositProperty.h"


@implementation OnlineDeposit {

}

+ (OnlineDeposit *)instanceFromDictionary:(NSDictionary *)aDictionary {

    OnlineDeposit *instance = [[OnlineDeposit alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    NSArray *dProperties = objectData[@"properties"];
    if ([dProperties isKindOfClass:[NSArray class]]) {
        NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[dProperties count]];
        for (id valueMember in dProperties) {
            [myMembers addObject:[OnlineDepositProperty instanceFromDictionary:valueMember]];
        }
        self.properties = myMembers;
    }

    self.code = [self stringValueForKey:@"code"];
    self.name = [self stringValueForKey:@"name"];
    self.logo = [self stringValueForKey:@"logo"];
    self.categoryCode = [self stringValueForKey:@"categoryCode"];
    self.typeName = [self stringValueForKey:@"typeName"];


    objectData = nil;
}

/**
 * @brief prepare info cards
 *
 */
- (NSArray *)composeInfoCards {
    OnlineDepositProperty *onlineDepositProperty = self.properties[0];
    
    return @[
             @{@"title" : @"Cashback", @"image" : @"d-cashback", @"value" : @"На покупки до 10%"},
             @{@"title" : @"Компенсация до 10 000 тенге", @"image" : @"d-compensation", @"value" : @"За перевод из другого банка"},
             @{@"title" : @"Мультивалютность", @"image" : @"d-multi", @"value" : [onlineDepositProperty.currencies componentsJoinedByString:@","]},
             @{@"title" : @"Бесплатное пополнение", @"image" : @"d-transfer", @"value" : @"Перевод с карты любого банка РК на текущий счет/депозит"},
             @{@"title" : @"Конвертация 24/7", @"image" : @"d-24", @"value" : @"Без комиссий"},
             @{@"title" : @"Минимальная сумма вклада", @"image" : @"d-min", @"value" : @"Не предусмотрена"}
    ];
}

/**
 * @brief prepare information for table
 * @return
 */
- (NSArray *)getComposedInfo {
    OnlineDepositProperty *onlineDepositProperty = self.properties[0];

    //Валюта
    NSDictionary *currencies = @{@"title" : @"Валюта:\n", @"value" : [onlineDepositProperty.currencies componentsJoinedByString:@","]};

    //Годовая эффективная ставка вознаграждения
    NSMutableString *annualRates = [[NSMutableString alloc] init];
    for (NSString *key in onlineDepositProperty.annualRatesMap) {
        [annualRates appendString:[NSString stringWithFormat:@"%@ месяцев: ", key]];

        //hardcode for specific order
        for (NSString *currTempl in @[@"KZT", @"EUR", @"USD", @"RUB"]) {
            for (NSString *currKey in onlineDepositProperty.annualRatesMap[key]) {
                if ([currKey isEqualToString:currTempl]) {
                    [annualRates appendString:[NSString stringWithFormat:@"%@ %@%% ", currKey,onlineDepositProperty.annualRatesMap[key][currKey]]];
                }
            }
        }
        [annualRates appendString:@"\n"];
    }
    NSDictionary *annualRate = @{@"title" : @"Годовая эффективная ставка вознаграждения:\n", @"value" :  annualRates};

    //Минимальная сумма
    NSDictionary *minSum = @{@"title" : @"Минимальная сумма:\n", @"value" : onlineDepositProperty.propertyDescription.minAmount};

    //Конвертация в интернет-банкинге
    NSDictionary *conversionInBank = @{@"title" : @"Конвертация в интернет-банкинге:\n", @"value" : onlineDepositProperty.propertyDescription.conversionInBanking};

    //Минимальная сумма конвертации
    NSMutableString *minConversions = [[NSMutableString alloc] init];
    for (NSString *currTempl in @[@"KZT"]) {
        for (NSString *key in onlineDepositProperty.minConversion) {
            if ([currTempl isEqualToString:key]) {
                [minConversions appendString:[NSString stringWithFormat:@"%@%@  ", onlineDepositProperty.minConversion[key], key]];
            }
        }
    }
    NSDictionary *minConversion = @{@"title" : @"Минимальная сумма конвертации:\n", @"value" : minConversions};

    //Внутрибанковский перевод "между своими счетами"
    NSDictionary *intrabankTransfer = @{@"title" : @"Внутрибанковский перевод \"между своими счетами\":\n", @"value" : onlineDepositProperty.propertyDescription.intrabankTransfer};

    //Пополнение депозита через карту любого банка РК
    NSDictionary *depositRefil = @{@"title" : @"Пополнение депозита через карту любого банка РК:\n", @"value" : onlineDepositProperty.propertyDescription.replenishmentByAnyBankCard};

    //Неснижаемый остаток
    NSDictionary *minBalance = @{@"title" : @"Неснижаемый остаток:\n", @"value" : onlineDepositProperty.propertyDescription.minBalance};

    //Частичное изъятие
    NSDictionary *partialWithdrawal = @{@"title" : @"Частичное изъятие:\n", @"value" : onlineDepositProperty.propertyDescription.partialWithdrawal};

    return @[currencies, annualRate, minSum,
            conversionInBank, minConversion,
            intrabankTransfer, depositRefil,
            minBalance, partialWithdrawal];
}

- (NSString *)getDepositInterestRate:(NSString *)selectedTerm {
    OnlineDepositProperty *onlineDepositProperty = self.properties[0];
    NSMutableString *annualRates = [[NSMutableString alloc] init];
    for (NSString *c in @[@"KZT", @"USD", @"EUR", @"RUB"]) {
        for (NSString *currKey in onlineDepositProperty.annualRatesMap[selectedTerm]) {
            if ([c isEqualToString:currKey]) {
                [annualRates appendString:[NSString stringWithFormat:@"%@=%@%% ", currKey,onlineDepositProperty.annualRatesMap[selectedTerm][currKey]]];
            }
        }
    }
    return annualRates;
}

- (NSString *)getDepositEffectiveInterestRate:(NSString *)selectedTerm {
    OnlineDepositProperty *onlineDepositProperty = self.properties[0];
    NSMutableString *rates = [[NSMutableString alloc] init];
    for (NSString *c in @[@"KZT", @"USD", @"EUR", @"RUB"]) {
        for (NSString *currKey in onlineDepositProperty.ratesMap[selectedTerm]) {
            if ([c isEqualToString:currKey]) {
                [rates appendString:[NSString stringWithFormat:@"%@=%@%% ", currKey, onlineDepositProperty.ratesMap[selectedTerm][currKey]]];
            }
        }
    }
    return rates;
}

@end
