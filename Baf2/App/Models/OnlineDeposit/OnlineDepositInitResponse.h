//
// Created by Askar Mustafin on 10/25/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"


@interface OnlineDepositInitResponse : ModelObject

@property (nonatomic, strong) NSString *clientFirstName;
@property (nonatomic, strong) NSString *identityIssuer;
@property (nonatomic, strong) NSString *depositPeriod;
@property (nonatomic, strong) NSString *depositCurrency;
@property (nonatomic, strong) NSString *depositBankBranch;
@property (nonatomic, strong) NSString *bankAstanaBik;
@property (nonatomic, assign) BOOL identityChanged;
@property (nonatomic, strong) NSString *bankAstanaOfficialName;
@property (nonatomic, assign) BOOL depositCapitalization;
@property (nonatomic, strong) NSString *identityIssueDate;
@property (nonatomic, strong) NSString *depositEffectiveInterestRate;
@property (nonatomic, strong) NSString *depositAmount;
@property (nonatomic, strong) NSString *identityType;
@property (nonatomic, strong) NSString *bankAstanaBin;
@property (nonatomic, strong) NSString *identityScanUrl;
@property (nonatomic, strong) NSString *depositName;
@property (nonatomic, strong) NSString *clientBirthDate;
@property (nonatomic, strong) NSString *depositInterestRate;
@property (nonatomic, strong) NSString *identityExpireDate;
@property (nonatomic, strong) NSString *clientMiddleName;
@property (nonatomic, strong) NSString *clientPhone;
@property (nonatomic, strong) NSString *servicelogId;
@property (nonatomic, strong) NSString *identityNum;
@property (nonatomic, strong) NSString *clientLastName;

- (NSArray *)composeStatement; // Для последнего шага
+ (OnlineDepositInitResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;

@end
