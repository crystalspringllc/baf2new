#import "OnlineDepositBenefits.h"

#import "OnlineDepositBenefitTable.h"

@implementation OnlineDepositBenefits

@synthesize footer;
@synthesize header;
@synthesize table;
@synthesize tableFooter;
@synthesize title;

+ (OnlineDepositBenefits *)instanceFromDictionary:(NSDictionary *)aDictionary {

    OnlineDepositBenefits *instance = [[OnlineDepositBenefits alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.footer = [self stringValueForKey:@"footer"];
    self.header = [self stringValueForKey:@"header"];
    self.tableFooter = [self stringValueForKey:@"tableFooter"];
    self.title = [self stringValueForKey:@"title"];

    if ([aDictionary[@"table"] isKindOfClass:[NSArray class]]) {
        NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[aDictionary[@"table"] count]];
        for (id valueMember in aDictionary[@"table"]) {
            OnlineDepositBenefitTable *populatedMember = [OnlineDepositBenefitTable instanceFromDictionary:valueMember];
            [myMembers addObject:populatedMember];
        }
        self.table = myMembers;
    }
    
   /* self.footer = [self.footer stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    self.header = [self.header stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    self.tableFooter = [self.tableFooter stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    self.title = [self.title stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"]; */
    
   // NSString * htmlString = @"<html><body> Some html string </body></html>";

    
    //UILabel * myLabel = [[UILabel alloc] init];
    //myLabel.attributedText = attrStr;


    objectData = nil;

}

@end
