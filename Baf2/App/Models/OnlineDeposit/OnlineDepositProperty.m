//
// Created by Askar Mustafin on 10/18/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositProperty.h"


@implementation OnlineDepositProperty {

}

+ (OnlineDepositProperty *)instanceFromDictionary:(NSDictionary *)aDictionary {
    OnlineDepositProperty *instance = [[OnlineDepositProperty alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.name = [self stringValueForKey:@"name"];


    NSArray *dCurrencies = objectData[@"currencies"];
    if ([dCurrencies isKindOfClass:[NSArray class]]) {
        NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[dCurrencies count]];
        for (NSString *valueMember in dCurrencies) {
            [myMembers addObject:valueMember];
        }
        self.currencies = myMembers;
    }


    NSArray *dPeriods = objectData[@"periods"];
    if ([dPeriods isKindOfClass:[NSArray class]]) {
        NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[dPeriods count]];
        for (NSString *valueMember in dPeriods) {
            [myMembers addObject:valueMember];
        }
        self.periods = myMembers;
    }

    self.ratesMap = objectData[@"ratesMap"];
    self.annualRatesMap = objectData[@"annualRatesMap"];
    self.investmentConstraint = objectData[@"investmentConstraint"];
    self.minConversion = objectData[@"minConversion"];

    self.propertyDescription = [OnlineDepositDescription instanceFromDictionary:objectData[@"description"]];
    if(objectData[@"benefits"])
    {
        
        NSArray *array = objectData[@"benefits"];
        NSMutableArray *benefitsMutableArray = [NSMutableArray arrayWithCapacity:array.count];
        for(NSDictionary *obj in array)
        {
            OnlineDepositBenefits *benefits = [OnlineDepositBenefits instanceFromDictionary:obj];
            [benefitsMutableArray addObject:benefits];
        }
        self.benefitsArray = benefitsMutableArray;
    }
    //self.benefits = [OnlineDepositBenefits instanceFromDictionary:objectData[@"benefits"]];

    objectData = nil;
}

@end
