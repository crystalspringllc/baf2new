#import "OnlineDepositDescription.h"

@implementation OnlineDepositDescription

@synthesize additionalContributions;
@synthesize cashBack;
@synthesize compensation;
@synthesize conversionInBanking;
@synthesize dissolution;
@synthesize fixedRate;
@synthesize intrabankTransfer;
@synthesize minAmount;
@synthesize minBalance;
@synthesize partialWithdrawal;
@synthesize prolongation;
@synthesize replenishmentByAnyBankCard;

+ (OnlineDepositDescription *)instanceFromDictionary:(NSDictionary *)aDictionary {

    OnlineDepositDescription *instance = [[OnlineDepositDescription alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.additionalContributions = [self stringValueForKey:@"additionalContributions"];
    self.cashBack = [self stringValueForKey:@"cashBack"];
    self.compensation = [self stringValueForKey:@"compensation"];
    self.conversionInBanking = [self stringValueForKey:@"conversionInBanking"];
    self.dissolution = [self stringValueForKey:@"dissolution"];
    self.fixedRate = [self stringValueForKey:@"fixedRate"];
    self.intrabankTransfer = [self stringValueForKey:@"intrabankTransfer"];
    self.minAmount = [self stringValueForKey:@"minAmount"];
    self.minBalance = [self stringValueForKey:@"minBalance"];
    self.partialWithdrawal = [self stringValueForKey:@"partialWithdrawal"];
    self.prolongation = [self stringValueForKey:@"prolongation"];
    self.replenishmentByAnyBankCard = [self stringValueForKey:@"replenishmentByAnyBankCard"];

    objectData = nil;

}

- (NSArray *)getDescriptions {
    return nil;
}


@end
