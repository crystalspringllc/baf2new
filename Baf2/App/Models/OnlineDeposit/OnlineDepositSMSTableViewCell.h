//
//  OnlineDepositSMSTableViewCell.h
//  Baf2
//
//  Created by Askar Mustafin on 10/26/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnlineDepositSMSTableViewCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *smsTextfield;
@property (nonatomic, copy) void (^onSmsTextfieldEndEditing)(NSString *smsCode);
@property (nonatomic, copy) void (^onSendSmsCodeAgain)();
@end
