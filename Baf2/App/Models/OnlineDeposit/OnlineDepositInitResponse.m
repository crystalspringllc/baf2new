//
// Created by Askar Mustafin on 10/25/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositInitResponse.h"


@implementation OnlineDepositInitResponse {}

+ (OnlineDepositInitResponse *)instanceFromDictionary:(NSDictionary *)aDictionary {
    OnlineDepositInitResponse *instance = [[OnlineDepositInitResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.clientFirstName = [self stringValueForKey:@"clientFirstName"];
    self.identityIssuer = [self stringValueForKey:@"identityIssuer"];
    self.depositPeriod = [self stringValueForKey:@"depositPeriod"];
    self.depositCurrency = [self stringValueForKey:@"depositCurrency"];
    self.depositBankBranch = [self stringValueForKey:@"depositBankBranch"];
    self.bankAstanaBik = [self stringValueForKey:@"bankAstanaBik"];
    self.identityChanged = [self boolValueForKey:@"identityChanged"];
    self.bankAstanaOfficialName = [self stringValueForKey:@"bankAstanaOfficialName"];
    self.depositCapitalization = [self boolValueForKey:@"depositCapitalization"];
    self.identityIssueDate = [self stringValueForKey:@"identityIssueDate"];
    self.depositEffectiveInterestRate = [self stringValueForKey:@"depositEffectiveInterestRate"];
    self.depositAmount = [self stringValueForKey:@"depositAmount"];
    self.identityType = [self stringValueForKey:@"identityType"];
    self.bankAstanaBin = [self stringValueForKey:@"bankAstanaBin"];
    self.identityScanUrl = [self stringValueForKey:@"identityScanUrl"];
    self.depositName = [self stringValueForKey:@"depositName"];
    self.clientBirthDate = [self stringValueForKey:@"clientBirthDate"];
    self.depositInterestRate = [self stringValueForKey:@"depositInterestRate"];
    self.identityExpireDate = [self stringValueForKey:@"identityExpireDate"];
    self.clientMiddleName = [self stringValueForKey:@"clientMiddleName"];
    self.clientPhone = [self stringValueForKey:@"clientPhone"];
    self.servicelogId = [self stringValueForKey:@"servicelogId"];
    self.identityNum = [self stringValueForKey:@"identityNum"];
    self.clientLastName = [self stringValueForKey:@"clientLastName"];

    objectData = nil;
}


- (NSArray *)composeStatement {
    return @[
            @{@"title" : @"Наименование вклада:", @"value" : self.depositName},//Астана
            @{@"title" : @"ФИО клиента:", @"value" : [NSString stringWithFormat:@"%@ %@ %@", self.clientLastName, self.clientFirstName, self.clientMiddleName]},
            @{@"title" : @"Тип документа:", @"value" : self.identityType}, //Удостоверение гражданина РК
            @{@"title" : @"Номер удостоверения:", @"value" : self.identityNum}, //039767911
            @{@"title" : @"Дата выдачи:", @"value" : self.identityIssueDate },//16.02.2016
            @{@"title" : @"Срок действия:", @"value" : self.identityExpireDate },//15.02.2026
            @{@"title" : @"Орган выдачи:", @"value" : self.identityIssuer },//Министерство внутренних дел РК
            @{@"title" : @"Сумма вклада:", @"value" : self.depositAmount },//0
            @{@"title" : @"Филиал Банка:", @"value" : self.depositBankBranch },//Алматинский филиал
            @{@"title" : @"Ставка вознаграждения по Вкладу:", @"value" : self.depositInterestRate},//EUR=1% KZT=13.2% RUB=1% USD=1%
            @{@"title" : @"Эффективная ставка вознаграждения:", @"value" : self.depositEffectiveInterestRate}, //EUR=1% KZT=14% RUB=1% USD=1%
            @{@"title" : @"Прибавлять к сумме вклада/Переводить на текущий счет:", @"value" : self.depositCapitalization ? @"C капитализацией" : @"Без капитализации"}, //Без капитализации
            @{@"title" : @"Срок вклада:", @"value" : self.depositPeriod }, //12
            @{@"title" : @"Наименование Банка:", @"value" : self.bankAstanaOfficialName}, //АО «Банк Астаны»
            @{@"title" : @"БИН:", @"value" : self.bankAstanaBin },//080540014021
            @{@"title" : @"БИК:", @"value" : self.bankAstanaBik },//ASFBKZKA
            @{@"title" : @"Телефон:", @"value" : self.clientPhone }//7019602566
    ];
}

@end
