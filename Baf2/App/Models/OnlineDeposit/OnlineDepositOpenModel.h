//
// Created by Askar Mustafin on 10/25/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OnlineDepositOpenModel : NSObject

//fill on calc step
@property (nonatomic, strong) NSString *depositCurrency;
@property (nonatomic, strong) NSNumber *depositAmount;
@property (nonatomic, strong) NSNumber *depositPeriod;
@property (nonatomic, assign) BOOL depositCapitalization; //false
@property (nonatomic, strong) NSString *depositInterestRate; //EUR=1% KZT=13.2% RUB=1% USD=1% //ratesMap
@property (nonatomic, strong) NSString *depositEffectiveInterestRate; //EUR=1% KZT=14% RUB=1% USD=1% //annualRatesMap

//fill on 1st step
@property (nonatomic, assign) BOOL identityChanged;
@property (nonatomic, strong) NSString *identityScanUrl;
@property (nonatomic, strong) NSString *identityNum;
@property (nonatomic, strong) NSString *identityIssueDate;
@property (nonatomic, strong) NSString *identityExpireDate;
@property (nonatomic, strong) NSString *identityType;
@property (nonatomic, strong) NSString *identityIssuer;
@property (nonatomic, strong) NSString *clientBirthDate;

//fill on 2nd step
@property (nonatomic, strong) NSString *depositBankBranch; //0200
@property (nonatomic, strong) NSString *bankAstanaOfficialName; //АО «Банк Астаны»
@property (nonatomic, strong) NSString *bankAstanaBin; //080540014021
@property (nonatomic, strong) NSString *bankAstanaBik; //ASFBKZKA

//other for UI
@property (nonatomic, strong) NSString *issuedDocTitle; //for title in step 3
@property (nonatomic, strong) NSString *typeDocTitle; //for title in step 3
@property (nonatomic, strong) NSString *filialBATitle; //for title in step 3

@end
