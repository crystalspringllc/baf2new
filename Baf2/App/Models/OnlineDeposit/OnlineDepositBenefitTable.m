#import "OnlineDepositBenefitTable.h"

@implementation OnlineDepositBenefitTable

@synthesize fIELD1;
@synthesize fIELD2;
@synthesize fIELD3;

+ (OnlineDepositBenefitTable *)instanceFromDictionary:(NSDictionary *)aDictionary {

    OnlineDepositBenefitTable *instance = [[OnlineDepositBenefitTable alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.fIELD1 = [self stringValueForKey:@"FIELD1"];
    self.fIELD2 = [self stringValueForKey:@"FIELD2"];
    self.fIELD3 = [self stringValueForKey:@"FIELD3"];
    

    objectData = nil;

}

@end
