//
//  OnlineDepositSMSTableViewCell.m
//  Baf2
//
//  Created by Askar Mustafin on 10/26/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositSMSTableViewCell.h"

@implementation OnlineDepositSMSTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.smsTextfield.delegate = self;
}

#define MAXLENGTH 4

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    return newLength <= MAXLENGTH || returnKey;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.text.length > 0) {
        if (self.onSmsTextfieldEndEditing) {
            self.onSmsTextfieldEndEditing(textField.text);
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (IBAction)clickSendSmsAgainButton:(id)sender {
    if (self.onSendSmsCodeAgain) {
        self.onSendSmsCodeAgain();
    }
}

@end
