#import "ExchangeRate.h"
#import "NSNumber+Ext.h"

@implementation ExchangeRate

+ (ExchangeRate *)instanceFromDictionary:(NSDictionary *)aDictionary {

    ExchangeRate *instance = [[ExchangeRate alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.buy = [self numberValueForKey:@"buy"];
    self.currency = [self stringValueForKey:@"currency"];
    self.currencyCode = [self numberValueForKey:@"code"];
    self.sell = [self numberValueForKey:@"sell"];
    self.updated = [self stringValueForKey:@"updated"];

    objectData = nil;
}

- (NSString *)getSellDecimal {
    return [self.buy decimalFormatString];
}

- (NSString *)getBuyDecimal {
    return [self.sell decimalFormatString];
}

@end
