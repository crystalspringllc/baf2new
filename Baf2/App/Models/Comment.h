


#import "ModelObject.h"
#import "CommentInfo.h"
#import "CommentSrc.h"

@interface Comment : ModelObject <NSCoding>

@property (nonatomic, copy)   NSString    *date;
@property (nonatomic, copy)   NSString    *commentId;
@property (nonatomic, copy)   NSArray     *images;
@property (nonatomic, strong) CommentInfo *info;
@property (nonatomic, assign) BOOL         isAdminPost;
@property (nonatomic, assign) BOOL         like;
@property (nonatomic, copy)   NSNumber    *likes;
@property (nonatomic, assign) BOOL         perm;
@property (nonatomic, copy)   NSNumber    *sessionId;
@property (nonatomic, copy)   NSString    *share;
@property (nonatomic, strong) CommentSrc  *src;
@property (nonatomic, copy)   NSString    *type;

+ (Comment *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

- (NSDate *)formattedDate;
- (NSString *)formattedDateString;

- (void)updateAfterLike:(NSDictionary *)params;

@end
