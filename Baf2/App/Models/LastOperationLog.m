#import "LastOperationLog.h"

@implementation LastOperationLog

@synthesize amount, cardType, currency, date, operationDate, operationError, operationSubtitle, operationSumm, operationTitle, operationUrl, text, text2, type, typeDesc, v;

+ (LastOperationLog *)instanceFromDictionary:(NSDictionary *)aDictionary {

    LastOperationLog *instance = [[LastOperationLog alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.amount = [self numberValueForKey:@"amount"];
    self.cardType = [self stringValueForKey:@"cardType"];
    self.currency = [self stringValueForKey:@"currency"];
    self.date = [self stringValueForKey:@"date"];
    self.operationDate = [self stringValueForKey:@"operation_date"];
    self.operationError = [self stringValueForKey:@"operation_error"];
    self.operationSubtitle = [self stringValueForKey:@"operation_subtitle"];
    self.operationSumm = [self stringValueForKey:@"operation_summ"];
    self.operationTitle = [self stringValueForKey:@"operation_title"];
    self.operationUrl = [self stringValueForKey:@"operation_url"];
    self.text = [self stringValueForKey:@"text"];
    self.text2 = [self stringValueForKey:@"text2"];
    self.type = [self stringValueForKey:@"type"];
    self.typeDesc = [self stringValueForKey:@"type_desc"];
    self.v = [self stringValueForKey:@"v"];

    objectData = nil;
}


@end
