#import "ModelObject.h"

@class TransferDescriptionCode;


@interface TransferCurrencyRates : ModelObject

@property (nonatomic, strong) NSString *updated;
@property (nonatomic, strong) NSNumber *convertedAmount;
@property (nonatomic, strong) NSNumber *rate;
@property (nonatomic, strong) NSString *presentation;

@property (nonatomic, strong) NSString *fromCurrency;
@property (nonatomic, strong) NSNumber *fromSell;

@property (nonatomic, strong) NSString *toCurrency;
@property (nonatomic, strong) NSNumber *toBuy;

+ (TransferCurrencyRates *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)getUpdateTime;

- (NSString *)composeUpdateTime;
- (NSString *)composeCurrencyRateMessage;
- (id)copyWithZone:(NSZone *)zone;

@end
