//
//  Atm.h
//  BAF
//
//  Created by Almas Adilbek on 7/7/15.
//
//

#import <CoreLocation/CoreLocation.h>

@interface Atm : NSObject

@property(nonatomic, copy) NSString *bankName;
@property(nonatomic, copy) NSString *address;
@property(nonatomic, assign) double longitude;
@property(nonatomic, assign) double latitude;
@property(nonatomic, assign) int distanceToUser;
@property(nonatomic) CLLocationCoordinate2D location;
+ (NSArray *)getSortedAtmsFrom:(NSArray *)atms;

@end
