//
// Created by Askar Mustafin on 7/18/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"

@class TickerInfo;

@interface Ticker : ModelObject {
    NSString *message;
    NSNumber *difference;
    NSString *dt_actual;
    BOOL visible;
    NSString *updated;
    NSString *name;
    NSNumber *last_price;
    NSString *dt_updated;
    NSString *code;
    NSString *warning;
    NSDictionary *info;
    NSArray *infokeys;
}

@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSNumber *difference;
@property (nonatomic, strong) NSString *dt_actual;
@property (nonatomic, assign) BOOL visible;
@property (nonatomic, strong) NSString *updated;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *last_price;
@property (nonatomic, strong) NSString *dt_updated;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *warning;
@property (nonatomic, strong) NSDictionary *info;
@property (nonatomic, strong) NSArray *infokeys;

+ (Ticker *)instanceFromDictionary:(NSDictionary *)aDictionary;

@end


//@interface TickerInfo : ModelObject
//
//@property (nonatomic, strong)
//
//@end
//

/*

 {
    "status": "success",
    "data": {
        "message": "Маркет-мейкером выступает АО «Фридом Финанс». Call Center 8 800 080 3131 (звонок бесплатный) и 7555 (бесплатно со всех мобильных операторов Казахстана)",
        "difference": 0,
        "dt_actual": "2017-07-18 09:00:31",
        "visible": true,
        "updated": "Данные получены: 18.07.2017",
        "name": "Простые акции АО Банк Астаны",
        "last_price": 1163.99,
        "dt_updated": "2017-07-18 10:26:55",
        "code": "ABBN.KZ",
        "warning": "Данные по рынку предоставляются с задержкой в 15 минут",
        "info": {
            "Код эмитента": "ABBN.KZ",
            "Цена открытия": "1 155,00",
            "Минимальная цена": "1 150,01",
            "Максимальная цена": "1 171,10",
            "Цена последней сделки": "1 163,99",
            "Изменение цены последней сделки к цене открытия в %": "0,00",
            "Объем заключенных сделок в тенге": "48 131 576,70"
        }
    }
}


 */