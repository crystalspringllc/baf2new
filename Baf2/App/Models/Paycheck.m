#import "Paycheck.h"
#import "Contract.h"
#import "PaymentProvider.h"
#import "NSString+Ext.h"
#import "NSDate+Ext.h"

@implementation Paycheck

+ (Paycheck *)instanceFromDictionary:(NSDictionary *)aDictionary {

    Paycheck *instance = [[Paycheck alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;
    
    self.acquiring = [self stringValueForKey:@"acquiring"];
    self.amount = [self stringValueForKey:@"amount"];
    self.amounWithtCommission = [self stringValueForKey:@"amounWithtCommission"];
    self.approvalCode = [self numberValueForKey:@"approvalCode"];
    self.cardNum = [self stringValueForKey:@"cardNum"];
    self.bank = [self stringValueForKey:@"bank"];
    self.date = [self stringValueForKey:@"date"];
    self.order = [self stringValueForKey:@"order"];
    self.ref = [self stringValueForKey:@"ref"];
    self.terminal = [self numberValueForKey:@"terminal"];

    self.contract = [Contract instanceFromDictionary:aDictionary[@"contract"]];
    
    self.bonusAmount = [self numberValueForKey:@"bonusAmount"];
    self.cardNumber = [self stringValueForKey:@"cardNumber"];
    self.cardType = [self stringValueForKey:@"cardType"];
    self.hasError = [self boolValueForKey:@"hasError"];
    self.logstate = [self stringValueForKey:@"logstate"];
    self.orderId = [self numberValueForKey:@"orderId"];
    self.provider = [PaymentProvider instanceFromDictionary:aDictionary[@"provider"]];
    self.providerId = [self numberValueForKey:@"providerId"];
    self.reference = [self numberValueForKey:@"reference"];
    self.terminal = [self numberValueForKey:@"terminal"];
    
    objectData = nil;
}

- (NSString *)normalDate {

    NSString *updatedDateString = self.date;
//    updatedDateString = [updatedDateString stringByReplacingOccurrencesOfString:@" AM" withString:@""];
//    updatedDateString = [updatedDateString stringByReplacingOccurrencesOfString:@" PM" withString:@""];
    //NSDate *updatedDate = [updatedDateString dateWithDateFormat:@"MMM dd, yyyy HH:mm:ss"];
    //Tue, 14 Jun 2016 09:56:13 ALMT
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"E, d MMM yyyy HH:mm:ss"];
//    NSDate *updatedDate = [updatedDateString dateWithDateFormat:@"E, d MMM yyyy HH:mm:ss Z"];
//    NSDate *updatedDate = [df dateFromString:updatedDateString];
//    NSString *updateDateStandardFormatString = [updatedDate stringWithDateFormat:@"dd.MM.yyyy HH:mm:ss"];
    
    return updatedDateString;
//    return updateDateStandardFormatString;
}

@end