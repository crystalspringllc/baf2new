//
//  DestinationsTypes.m
//  Baf2
//
//  Created by developer on 23.01.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "DestinationsTypes.h"

@implementation DestinationsTypes

+ (DestinationsTypes *)instanceFromDictionary:(NSDictionary *)aDictionary {
    DestinationsTypes *instance = [[DestinationsTypes alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.name = [self stringValueForKey:@"name"];
    self.value = [self stringValueForKey:@"value"];

    objectData = nil;
}

@end
