//
//  TransferType.m
//  Baf2
//
//  Created by developer on 23.01.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "NewTransferType.h"
#import "DestinationsTypes.h"

@implementation NewTransferType

+ (NewTransferType *)instanceFromDictionary:(NSDictionary *)aDictionary {
    NewTransferType *instance = [[NewTransferType alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.typeName = [self stringValueForKey:@"name"];
    self.typeValue = [self stringValueForKey:@"value"];
    
    for(NSDictionary *dict in [self arrayValueForKey:@"dst_types"])
    {
        DestinationsTypes *destType = [DestinationsTypes instanceFromDictionary:dict];
        [self.destinationsTypes addObject:destType];
    }
    
    objectData = nil;
}


@end
