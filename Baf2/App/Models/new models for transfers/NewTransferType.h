//
//  TransferType.h
//  Baf2
//
//  Created by developer on 23.01.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface NewTransferType : ModelObject

@property (nonatomic, strong) NSString *typeName;
@property (nonatomic, strong) NSString *typeValue;
@property (nonatomic, strong) NSMutableArray *destinationsTypes;

+ (NewTransferType *)instanceFromDictionary:(NSDictionary *)aDictionary;

@end
