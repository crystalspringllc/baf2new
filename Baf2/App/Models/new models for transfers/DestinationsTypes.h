//
//  DestinationsTypes.h
//  Baf2
//
//  Created by developer on 23.01.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface DestinationsTypes : ModelObject

@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *value;

+ (DestinationsTypes *)instanceFromDictionary:(NSDictionary *)aDictionary;

@end
