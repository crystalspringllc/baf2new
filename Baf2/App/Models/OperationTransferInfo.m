#import "OperationTransferInfo.h"
#import "NSNumber+Ext.h"
#import "NSString+Ext.h"

@interface OperationTransferInfo ()

@end

@implementation OperationTransferInfo

@synthesize aMOUNT, cLIENTDESCRIPTION, cURRENCY, date, dESTINATIONCONTRACT, dESTINATIONCONTRACTID, dESTINATIONCONTRACTTYPE, DESTINATION_NUMBER, eXCHANGEDATE, EXCHANGE_RATE, rEFERENCEID, rEQUESTORCONTRACT, rEQUESTOR_NUMBER, rEQUESTORCONTRACTID, rEQUESTORCONTRACTTYPE, state, tRANSACTIONSTATUS, tRANSACTIONSTATUSDESC, tRANSFERDESCRIPTION, vALUEDATE;

+ (OperationTransferInfo *)instanceFromDictionary:(NSDictionary *)aDictionary {

    OperationTransferInfo *instance = [[OperationTransferInfo alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;

    self.aMOUNT = [self stringValueForKey:@"AMOUNT"];
    self.cLIENTDESCRIPTION = [self stringValueForKey:@"CLIENT_DESCRIPTION"];
    self.cURRENCY = [self stringValueForKey:@"CURRENCY"];
    self.date = [self stringValueForKey:@"date"];
    self.dESTINATIONCONTRACT = [self stringValueForKey:@"DESTINATION_CONTRACT"];
    self.dESTINATIONCONTRACTID = [self stringValueForKey:@"DESTINATION_CONTRACT_ID"];
    self.dESTINATIONCONTRACTTYPE = [self stringValueForKey:@"DESTINATION_CONTRACT_TYPE"];
    self.DESTINATION_NUMBER = [self stringValueForKey:@"DESTINATION_NUMBER"];
    self.eXCHANGEDATE = [self stringValueForKey:@"EXCHANGE_DATE"];
    self.rEFERENCEID = [self stringValueForKey:@"REFERENCE_ID"];
    self.rEQUESTORCONTRACT = [self stringValueForKey:@"REQUESTOR_CONTRACT"];
    self.rEQUESTOR_NUMBER = [self stringValueForKey:@"REQUESTOR_NUMBER"];
    self.rEQUESTORCONTRACTID = [self stringValueForKey:@"REQUESTOR_CONTRACT_ID"];
    self.rEQUESTORCONTRACTTYPE = [self stringValueForKey:@"REQUESTOR_CONTRACT_TYPE"];
    self.rEQUESTOR_MULTICC = [self stringValueForKey:@"REQUESTOR_MULTICC"];
    self.rEQUESTOR_CURRENCY = [self stringValueForKey:@"REQUESTOR_CURRENCY"];
    self.state = [self stringValueForKey:@"state"];
    self.tRANSACTIONSTATUS = [self stringValueForKey:@"TRANSACTION_STATUS"];
    self.tRANSACTIONSTATUSDESC = [self stringValueForKey:@"TRANSACTION_STATUS_DESC"];
    self.tRANSFERDESCRIPTION = [self stringValueForKey:@"TRANSFER_DESCRIPTION"];
    self.vALUEDATE = [self stringValueForKey:@"VALUE_DATE"];
    self.comission = [self stringValueForKey:@"COMISSION"];
    self.comissionCurrency = [self stringValueForKey:@"COMISSION_CURRENCY"];
    self.dESTINATION_CURRENCY = [self stringValueForKey:@"DESTINATION_CURRENCY"];
    self.EXCHANGE_RATE = [self stringValueForKey:@"EXCHANGE_RATE"];
    self.TRANSFER_DESCRIPTION = [self stringValueForKey:@"TRANSFER_DESCRIPTION"];
    self.TRANSFER_DESCRIPTION_CODE = [self stringValueForKey:@"TRANSFER_DESCRIPTION_CODE"];
    self.DESTINATION_BANK = [self stringValueForKey:@"DESTINATION_BANK"];
    self.DESTINATION_IBAN = [self stringValueForKey:@"DESTINATION_IBAN"];
    self.kbe = [self stringValueForKey:@"kbe"];

    objectData = nil;
}

- (NSString *)amountWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", [@([self.aMOUNT floatValue]) decimalFormatString], self.cURRENCY];
}

- (BOOL)isCurrentIndividuals {
    return [[self.rEQUESTORCONTRACTTYPE lowercaseString] isEqual:@"current_individuals"];
}

- (BOOL)isCard {
    return [[self.rEQUESTORCONTRACTTYPE lowercaseString] isEqual:@"card"];
}

- (BOOL)isDepositIndividuals {
    return [[self.rEQUESTORCONTRACTTYPE lowercaseString] isEqual:@"deposit_individuals"];
}

- (NSString *)comissionWithCurrency {
    return [NSString stringWithFormat:@"%@ %@", self.comission, self.comissionCurrency];
}

- (NSString *)requestorContractWithAccountNumberAndCurrency
{
    if([self.rEQUESTOR_NUMBER isNotEmpty]) {
        return [NSString stringWithFormat:@"%@\n%@ %@", self.rEQUESTORCONTRACT, self.rEQUESTOR_NUMBER, self.rEQUESTOR_CURRENCY];
    }
    return self.rEQUESTORCONTRACT;
}

- (NSString *)destinationContractWithAccountNumberAndCurrency
{
    if([self.DESTINATION_NUMBER isNotEmpty]) {
        return [NSString stringWithFormat:@"%@\n%@ %@", self.dESTINATIONCONTRACT, self.DESTINATION_NUMBER, self.dESTINATION_CURRENCY];
    }
    return self.dESTINATIONCONTRACT;
}

@end
