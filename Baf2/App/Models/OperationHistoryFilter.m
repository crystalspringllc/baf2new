//
//  OperationHistoryFilter.m
//  Baf2
//
//  Created by Shyngys Kassymov on 21.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "OperationHistoryFilter.h"
#import "OperationHistory.h"
#import "NSObject+Json.h"


@implementation OperationHistoryFilter

#pragma mark - Methods

+ (instancetype)defaultFilter {
    OperationHistoryFilter *defaultFilter = [OperationHistoryFilter new];
    
    defaultFilter.startDate = [[OperationHistory dateFormatterToServer] stringFromDate:[[NSDate date] dateBySubtractingDays:5]];
    defaultFilter.endDate = [[OperationHistory dateFormatterToServer] stringFromDate:[[NSDate date] dateByAddingDays:1]];
    
    return defaultFilter;
}

- (NSMutableDictionary *)toJSON {
    NSMutableDictionary *json = [NSMutableDictionary new];
    
    if (self.count > 0) json[@"count"] = @(self.count);
    if (self.startDate) json[@"startDate"] = self.startDate;
    if (self.endDate) json[@"endDate"] = self.endDate;
    if (self.minAmount) json[@"minAmount"] = self.minAmount;
    if (self.maxAmount) json[@"maxAmount"] = self.maxAmount;
    if (self.selectedOperationStatuses) json[@"selectedOperationStatuses"] = [self.selectedOperationStatuses JSONRepresentation];
    if (self.providerId) json[@"providerId"] = self.providerId;
    if (self.contractId) json[@"contractId"] = self.contractId;
    if (self.paymentId) json[@"paymentId"] = self.paymentId;
    
    return json;
}


- (void)dealloc {
    NSLog(@"DEALLOCDEALLOCDEALLOCDEALLOCDEALLOCDEALLOCDEALLOCDEALLOC");
}


@end
