
#import "News.h"
#import "NSDate+Ext.h"

@implementation News
- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    [encoder encodeObject:self.comments forKey:@"comments"];
    [encoder encodeObject:self.newsDate forKey:@"date"];
    [encoder encodeObject:self.newsId forKey:@"newsId"];
    [encoder encodeObject:self.images forKey:@"images"];
    [encoder encodeObject:self.info forKey:@"info"];
    [encoder encodeObject:[NSNumber numberWithBool:self.isAdminPost] forKey:@"isAdminPost"];
    [encoder encodeObject:[NSNumber numberWithBool:self.like] forKey:@"like"];
    [encoder encodeObject:self.likes forKey:@"likes"];
    [encoder encodeObject:[NSNumber numberWithBool:self.perm] forKey:@"perm"];
    [encoder encodeObject:self.sessionId forKey:@"sessionId"];
    [encoder encodeObject:self.share forKey:@"share"];
    [encoder encodeObject:self.src forKey:@"src"];
    [encoder encodeObject:self.type forKey:@"type"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super initWithCoder:decoder])) {
        self.comments    = [decoder decodeObjectForKey:@"comments"];
        self.newsDate    = [decoder decodeObjectForKey:@"date"];
        self.newsId      = [decoder decodeObjectForKey:@"newsId"];
        self.images      = [decoder decodeObjectForKey:@"images"];
        self.info        = [decoder decodeObjectForKey:@"info"];
        self.isAdminPost = [(NSNumber *)[decoder decodeObjectForKey:@"isAdminPost"] boolValue];
        self.like        = [(NSNumber *)[decoder decodeObjectForKey:@"like"] boolValue];
        self.likes       = [decoder decodeObjectForKey:@"likes"];
        self.perm        = [(NSNumber *)[decoder decodeObjectForKey:@"perm"] boolValue];
        self.sessionId   = [decoder decodeObjectForKey:@"sessionId"];
        self.share       = [decoder decodeObjectForKey:@"share"];
        self.src         = [decoder decodeObjectForKey:@"src"];
        self.type        = [decoder decodeObjectForKey:@"type"];
    }
    return self;
}

+ (News *)instanceFromDictionary:(NSDictionary *)aDictionary {

    News *instance = [[News alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;
    self.newsDate    = [self stringValueForKey:@"date"];
    self.newsId      = [self stringValueForKey:@"id"];
    self.images      = [self arrayValueForKey:@"images"];
    self.isAdminPost = [self boolValueForKey:@"isAdminPost"];
    self.like        = [self boolValueForKey:@"like"];
    self.likes       = [self numberValueForKey:@"likes"];
    self.perm        = [self boolValueForKey:@"perm"];
    self.sessionId   = [self numberValueForKey:@"sessionId"];
    self.share       = [self stringValueForKey:@"share"];
    self.type        = [self stringValueForKey:@"type"];
    self.info        = [NewsInfo instanceFromDictionary:aDictionary[@"info"]];
    self.src         = [NewsSrc instanceFromDictionary:aDictionary[@"src"]];



    if ([aDictionary[@"comments"] count] > 0) {
        NSMutableArray *comments = [[NSMutableArray alloc] init];
        for (NSDictionary *comment in aDictionary[@"comments"]) {
            [comments addObject:[Comment instanceFromDictionary:comment]];
        }
        self.comments = comments;
    }
    
    objectData = nil;
}

- (void)updateAfterLike:(NSDictionary *)params {
    objectData = params;
    self.like        = [self boolValueForKey:@"like"];
    self.likes       = [self numberValueForKey:@"likes"];
    objectData = nil;
}


- (NSString *)senderId {
    return self.src.newsSrcId.stringValue;
}

- (NSString *)senderDisplayName {
    return self.src.nick;
}

- (NSDate *)date {
    return [NSDate formattedDateFromString:self.newsDate];
}

- (BOOL)isMediaMessage {
    return NO;
}

- (NSUInteger)messageHash {
    return self.hash;
}

- (NSString *)text {
    return self.info.message;
}


- (NSDate *)formattedDate {
    return [NSDate formattedDateFromString:self.newsDate];
}

- (NSString *)formattedDateString {
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"dd.MM.yyyy HH:mm"];
    return [f stringFromDate:self.formattedDate];
}


- (void)setValue:(id)value forKey:(NSString *)key {

    if ([key isEqualToString:@"comments"]) {

        if ([value isKindOfClass:[NSArray class]]) {

            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                Comment *populatedMember = [Comment instanceFromDictionary:valueMember];
                [myMembers addObject:populatedMember];
            }

            self.comments = myMembers;

        }

    } else if ([key isEqualToString:@"images"]) {

        if ([value isKindOfClass:[NSArray class]]) {

            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                [myMembers addObject:valueMember];
            }

            self.images = myMembers;

        }

    } else if ([key isEqualToString:@"info"]) {

        if ([value isKindOfClass:[NSDictionary class]]) {
            self.info = [NewsInfo instanceFromDictionary:value];
        }

    } else if ([key isEqualToString:@"src"]) {

        if ([value isKindOfClass:[NSDictionary class]]) {
            self.src = [NewsSrc instanceFromDictionary:value];
        }

    } else {
        [super setValue:value forKey:key];
    }

}


- (void)setValue:(id)value forUndefinedKey:(NSString *)key {

    if ([key isEqualToString:@"id"]) {
        [self setValue:value forKey:@"newsId"];
    } else {
        [super setValue:value forUndefinedKey:key];
    }

}


- (NSDictionary *)dictionaryRepresentation {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.comments) {
        [dictionary setObject:self.comments forKey:@"comments"];
    }

    if (self.date) {
        [dictionary setObject:self.date forKey:@"date"];
    }

    if (self.newsId) {
        [dictionary setObject:self.newsId forKey:@"newsId"];
    }

    if (self.images) {
        [dictionary setObject:self.images forKey:@"images"];
    }

    if (self.info) {
        [dictionary setObject:self.info forKey:@"info"];
    }

    [dictionary setObject:[NSNumber numberWithBool:self.isAdminPost] forKey:@"isAdminPost"];

    [dictionary setObject:[NSNumber numberWithBool:self.like] forKey:@"like"];

    if (self.likes) {
        [dictionary setObject:self.likes forKey:@"likes"];
    }

    [dictionary setObject:[NSNumber numberWithBool:self.perm] forKey:@"perm"];

    if (self.sessionId) {
        [dictionary setObject:self.sessionId forKey:@"sessionId"];
    }

    if (self.share) {
        [dictionary setObject:self.share forKey:@"share"];
    }

    if (self.src) {
        [dictionary setObject:self.src forKey:@"src"];
    }

    if (self.type) {
        [dictionary setObject:self.type forKey:@"type"];
    }

    return dictionary;

}


@end
