//
//  PaymentProvidersByCategory.m
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "PaymentProvidersByCategory.h"
#import "ProviderCategory.h"
#import "PaymentProvider.h"

@implementation PaymentProvidersByCategory

+ (PaymentProvidersByCategory *)instanceFromDictionary:(NSDictionary *)aDictionary {
    
    PaymentProvidersByCategory *instance = [[PaymentProvidersByCategory alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
    
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    NSDictionary *categoryDict = objectData[@"category"];
    self.category = [ProviderCategory instanceFromDictionary:categoryDict];
    
    NSArray *ppl = objectData[@"paymentProviderListWS"][@"paymentProviders"];
    NSMutableArray *paymentProviderListWS = [NSMutableArray new];
    for (NSDictionary *ppd in ppl) {
        PaymentProvider *pp = [PaymentProvider instanceFromDictionary:ppd];
        [paymentProviderListWS addObject:pp];
    }
    self.paymentProviderListWS = paymentProviderListWS;
    
    objectData = nil;
}

#pragma mark NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.paymentProviderListWS = [decoder decodeObjectForKey:@"paymentProviderListWS"];
    self.category = [decoder decodeObjectForKey:@"category"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.paymentProviderListWS forKey:@"paymentProviderListWS"];
    [encoder encodeObject:self.category forKey:@"category"];
}

@end
