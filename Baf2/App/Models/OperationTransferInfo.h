#import "ModelObject.h"


@interface OperationTransferInfo : ModelObject {

}

@property (nonatomic, copy) NSString *aMOUNT;
@property (nonatomic, copy) NSString *cLIENTDESCRIPTION;
@property (nonatomic, copy) NSString *cURRENCY;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *dESTINATIONCONTRACT;
@property (nonatomic, copy) NSString *dESTINATIONCONTRACTID;
@property (nonatomic, copy) NSString *dESTINATIONCONTRACTTYPE;
@property (nonatomic, copy) NSString *DESTINATION_NUMBER;
@property (nonatomic, copy) NSString *eXCHANGEDATE;
@property (nonatomic, copy) NSString *rEFERENCEID;
@property (nonatomic, copy) NSString *rEQUESTORCONTRACT;
@property (nonatomic, copy) NSString *rEQUESTORCONTRACTID;
@property (nonatomic, copy) NSString *rEQUESTORCONTRACTTYPE;
@property (nonatomic, copy) NSString *rEQUESTOR_MULTICC;
@property (nonatomic, copy) NSString *rEQUESTOR_CURRENCY;
@property (nonatomic, copy) NSString *rEQUESTOR_NUMBER;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *tRANSACTIONSTATUS;
@property (nonatomic, copy) NSString *tRANSACTIONSTATUSDESC;
@property (nonatomic, copy) NSString *tRANSFERDESCRIPTION;
@property (nonatomic, copy) NSString *vALUEDATE;
@property (nonatomic, copy) NSString *comission;
@property (nonatomic, copy) NSString *comissionCurrency;
@property (nonatomic, copy) NSString *dESTINATION_CURRENCY;
@property (nonatomic, copy) NSString *EXCHANGE_RATE;
@property (nonatomic, copy) NSString *TRANSFER_DESCRIPTION;
@property (nonatomic, copy) NSString *TRANSFER_DESCRIPTION_CODE;
@property (nonatomic, copy) NSString *DESTINATION_BANK;
@property (nonatomic, copy) NSString *DESTINATION_IBAN;
@property (nonatomic, copy) NSString *kbe;

+ (OperationTransferInfo *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSString *)amountWithCurrency;
- (NSString *)comissionWithCurrency;
- (NSString *)requestorContractWithAccountNumberAndCurrency;
- (NSString *)destinationContractWithAccountNumberAndCurrency;

- (BOOL)isCurrentIndividuals;
- (BOOL)isCard;
- (BOOL)isDepositIndividuals;

@end
