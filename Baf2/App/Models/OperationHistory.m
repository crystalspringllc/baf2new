//
//  OperationHistory.m
//  Baf2
//
//  Created by Shyngys Kassymov on 21.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "OperationHistory.h"
#import "Constants.h"
#import "NSDate+Ext.h"
#import "NSString+Ext.h"

@implementation OperationHistory

+ (OperationHistory *)instanceFromDictionary:(NSDictionary *)aDictionary {
    OperationHistory *instance = [[OperationHistory alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.operationId = [self numberValueForKey:@"id"];
    self.type = [self stringValueForKey:@"type"];
    self.date = [self stringValueForKey:@"date"];
    self.text = [self stringValueForKey:@"text"];
    self.providerCode = [self stringValueForKey:@"providerCode"];
    self.providerLogo = [self stringValueForKey:@"providerLogo"];
    self.amount = [self numberValueForKey:@"amount"];
    self.amountWithCommission = [self numberValueForKey:@"amountWithCommission"];
    self.status = [self stringValueForKey:@"status"];
    self.reason = [self stringValueForKey:@"reason"];
    self.currency = [self stringValueForKey:@"currency"];
    self.commissionCurrency = [self stringValueForKey:@"commissionCurrency"];
    
    objectData = nil;
}

+ (NSArray *)instancesFromArray:(NSArray *)data {
    NSMutableArray *operations = [NSMutableArray new];
    
    for (NSDictionary *dict in data) {
        OperationHistory *operation = [self instanceFromDictionary:dict];
        [operations addObject:operation];
    }
    
    return operations;
}

#pragma mark - Methods

- (NSString *)currency {
    if (_currency && _currency.length > 0) {
        return _currency;
    }
    
    return @"KZT";
}

- (NSString *)commissionCurrency {
    if (_commissionCurrency && _commissionCurrency.length > 0) {
        return _commissionCurrency;
    }
    
    return @"KZT";
}

- (NSString *)providerLogo {
    if (_providerLogo && _providerLogo.length > 0) {
        return [NSString stringWithFormat:@"%@providers/%@.png", BASE_IMAGE_URL, _providerLogo.lowercaseString];
    }
    
    return @"";
}

#pragma mark - Helpers

- (NSString *)prettyStatus {
    StatusType status = [OperationHistory statusFromString:self.status];
    switch (status) {
        case StatusTypeError:
            return @"Неуспешный";
            break;
        case StatusTypeComplete:
            return @"Выполнен";
            break;
        case StatusTypeInProgress:
            return @"В процессе";
            break;
            
        default:
            break;
    }
    
    return self.status;
}

+ (NSString *)stringFromStatus:(StatusType)statusType {
    NSString *status = @"";
    
    switch (statusType) {
        case StatusTypeError:
            status = @"ERROR";
            break;
        case StatusTypeComplete:
            status = @"COMPLETE";
            break;
        case StatusTypeInProgress:
            status = @"IN_PROGRESS";
            break;
            
        default:
            break;
    }
    
    return status;
}

+ (StatusType)statusFromString:(NSString *)status {
    StatusType statusType = StatusTypeError;
    
    if ([status isEqualToString:@"ERROR"]) {
        statusType = StatusTypeError;
    } else if ([status isEqualToString:@"COMPLETE"]) {
        statusType = StatusTypeComplete;
    } else if ([status isEqualToString:@"IN_PROGRESS"]) {
        statusType = StatusTypeInProgress;
    }
    
    return statusType;
}



/*+ (OperationType)typeFromString:(NSString *)type {
    OperationType statusType = PaymentType;
    
    if ([type isEqualToString:@"payment"]) {
        statusType = PaymentType;
    } else if ([type isEqualToString:@"TRANSFER"]) {
        statusType = TransferType;
    } else if ([type isEqualToString:@"SPRINGDOC"]) {
        statusType = SpringdocType;
    }
    
    return statusType;
}*/

- (BOOL)isPaymenType
{
    return [self.type isEqualToString:@"payment"];
}

+ (NSDateFormatter *)dateFormatterToServer {
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"dd.MM.yyyy"];
    return df;
}

+ (NSDateFormatter *)dateFormatterFromServer {
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return df;
}

- (NSString *)dateNice {
    NSDate *operationDate = [self.date dateWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [operationDate stringWithDateFormat:@"dd.MM.yyyy HH:mm:ss"];
}


@end
