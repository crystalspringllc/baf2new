//
//  NSError+Extensions.m
//  Baf2
//
//  Created by Shyngys Kassymov on 05.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NSError+Extensions.h"

@implementation NSError (Extensions)

+ (instancetype)errorFromDomain:(NSString *)domain message:(NSString *)message code:(NSString *)code {
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    [details setValue:message forKey:NSLocalizedDescriptionKey];
    NSInteger integerCode = [[[NSNumberFormatter new] numberFromString:code] integerValue];
    NSError *error = [NSError errorWithDomain:@"kz.bankastana.24.Baf2" code:integerCode userInfo:details];
    
    return error;
}

@end
