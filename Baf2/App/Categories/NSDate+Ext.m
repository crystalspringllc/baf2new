//
// Created by Almas Adilbek on 4/12/14.
// Copyright (c) 2014 Crystal Spring LLC. All rights reserved.
//

#import "NSDate+Ext.h"


@implementation NSDate (Ext)

- (NSDate *)getDateYersAgo:(int)numOfYears {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponent = [[NSDateComponents alloc] init];
    [offsetComponent setYear:-1 * numOfYears];
    NSDate *result = [gregorian dateByAddingComponents:offsetComponent toDate:self options:0];
    return result;
}

- (NSDate *)getDateYears:(int)numOfYears monthsAgo:(int)numOfMonths andDaysAgo:(int)numOfDays {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponent = [[NSDateComponents alloc] init];
    [offsetComponent setDay:-1 * numOfDays];
    [offsetComponent setMonth:-1 * numOfMonths];
    [offsetComponent setYear:-1 * numOfYears];
    NSDate *result = [gregorian dateByAddingComponents:offsetComponent toDate:self options:0];
    return result;
}

- (NSDate *)getMonthStartDate {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:self];
    [dateComponents setDay:1];
    [dateComponents setMonth:dateComponents.month];
    [dateComponents setYear:dateComponents.year];
    NSDate *monthStartDate = [gregorian dateFromComponents:dateComponents];
    
    return monthStartDate;
}

- (NSDate *)oldDateBySubtractingDays:(int)daysNumber {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:-daysNumber];
    NSDate *oldDate = [gregorian dateByAddingComponents:offsetComponents toDate:self options:0];
    
    return oldDate;
}

- (NSDate *)oldDateBySubtractingMonths:(int)monthsNumber {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth:-monthsNumber];
    NSDate *oldDate = [gregorian dateByAddingComponents:offsetComponents toDate:self options:0];
    
    return oldDate;
}


- (NSDate *)oldDateBySubtractingYears:(int)yearsNumber {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setYear:-yearsNumber];
    NSDate *oldDate = [gregorian dateByAddingComponents:offsetComponents toDate:self options:0];
    
    return oldDate;
}


/**
 *
 * String with format dd.MM.yyyy
 *
 */
- (NSString *)ddMMyyyy {
    return [self stringWithDateFormat:@"dd.MM.yyyy"];
}


/**
 *
 *  String from nsdate with format
 *
 */
- (NSString *)stringWithDateFormat:(NSString *)dateFormat {
    return [self stringWithDateFormat:dateFormat isCurrentLocale:NO];
}


/**
 *
 * String from nsdate with format and locale
 *
 */
- (NSString *)stringWithDateFormat:(NSString *)dateFormat isCurrentLocale:(BOOL)isCurrentLocale {
    NSString *localeIdentifier;
    if (isCurrentLocale) {
        localeIdentifier = [NSLocale preferredLanguages][0];
    } else {
        localeIdentifier = @"en_US_POSIX";
    }

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:localeIdentifier]];
    [dateFormatter setDateFormat:dateFormat];
    return [dateFormatter stringFromDate:self];

}


/**
 *
 *  Day prefix on russian
 *
 */
+ (NSString *)dayPrefixFromDayNum:(int)day {
    NSString *dayPrefix;
    if (day % 10 == 1 && day % 100 != 11) {
        dayPrefix = @"день";
    } else if ((day % 10 == 2) && (day % 100 != 12)) {
        dayPrefix = @"день";
    }
    else {
        if ((day % 10 == 3) && (day % 100 != 13)) {
            dayPrefix = @"день";
        }
        else {
            if ((day % 10 == 4) && (day % 100 != 14)) {
                dayPrefix = @"день";
            } else {
                dayPrefix = @"дней";
            }
        }
    }
    return dayPrefix;
}

+ (double)timestampFromString:(NSString*)string {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    string = [string stringByReplacingOccurrencesOfString:@" ALMT" withString:@""];
    NSDate *time = [formatter dateFromString:string];
    return [time timeIntervalSince1970] * 1000;
}

+ (NSDate *)formattedDateFromString:(NSString *)string {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    string = [string stringByReplacingOccurrencesOfString:@" ALMT" withString:@""];
    NSDate *time = [formatter dateFromString:string];
    return time;
}

/**
 *
 * Day number between two dates
 *
 */
+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

@end