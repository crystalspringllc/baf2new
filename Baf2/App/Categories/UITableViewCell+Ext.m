//
//  UITableViewCell+Ext.m
//  Myth
//
//  Created by Almas Adilbek on 10/12/13.
//
//

#import "UITableViewCell+Ext.h"

#define kLoaderTag 555

@implementation UITableViewCell (Ext)

-(void)setupDefaultSelection {
    UIView *sel = [[UIView alloc] initWithFrame:self.bounds];
    sel.backgroundColor = [UIColor fromRGB:0xEFEFEF];
    self.selectedBackgroundView = sel;
}

- (void)setGreenCheckmark {
    self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_check"]];
}

- (void)showLoader {
    self.contentView.hidden = YES;
    UIActivityIndicatorView *loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loader.tag = kLoaderTag;
    loader.hidesWhenStopped = YES;
    loader.centerX = (CGFloat) (self.contentView.width * 0.5);
    loader.centerY = (CGFloat) (self.contentView.height * 0.5);
    [self addSubview:loader];
    [loader startAnimating];
}

- (void)hideLoader {
    UIActivityIndicatorView *loader = (UIActivityIndicatorView *) [self viewWithTag:kLoaderTag];
    if(loader) {
        [loader stopAnimating];
        [loader removeFromSuperview];
    }
    self.contentView.hidden = NO;
}


@end
