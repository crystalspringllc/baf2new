//
// Created by Almas Adilbek on 8/22/14.
//

#import "NSNumber+Ext.h"

@implementation NSNumber (Ext)

- (NSString *)decimalFormatString {
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    nf.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    nf.usesGroupingSeparator = YES;
    [nf setGroupingSeparator:@" "];
    [nf setMinimumFractionDigits:2];
    [nf setMaximumFractionDigits:2];
    [nf setNegativePrefix:[nf minusSign]];
    return [nf stringFromNumber:self];
}

@end