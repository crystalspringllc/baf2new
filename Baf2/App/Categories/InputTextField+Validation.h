//
// Created by Almas Adilbek on 9/8/14.
// Copyright (c) 2014 Almas Adilbek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InputTextField.h"

@interface InputTextField (Validation)
- (BOOL)validate;
- (BOOL)validatePhoneNumber;
- (BOOL)validateIin;
- (BOOL)validateIinWithMessage:(NSString *)message;
- (BOOL)validateEmail;
- (NSString *)validPhoneNumber;
@end