//
//  AtmTableViewCell+ConfigureForAtm.h
//  BAF
//
//  Created by Shyngys Kassymov on 24.07.15.
//
//

#import "AtmTableViewCell.h"

@class Atm;

@interface AtmTableViewCell (ConfigureForAtm)
- (void)configureForAtm:(Atm *)atm;
@end
