//
//  UIView+Ext.m
//  Toppy
//
//  Created by Almas Adilbek on 4/13/15.
//  Copyright (c) 2015 Toppy Inc. All rights reserved.
//

#import "UIView+Ext.h"

@implementation UIView (Ext)

- (void)setMaskWithRoundingCorners:(UIRectCorner)corners {
    UIBezierPath *rounded = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(5.0, 5.0)];
    CAShapeLayer *shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    self.layer.mask = shape;
}

- (void)setMaskWithRoundingCorners:(UIRectCorner)corners radius:(CGFloat)radius {
    UIBezierPath *rounded = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    self.layer.mask = shape;
}

- (void)setMaskWithRoundingCorners:(UIRectCorner)corners radius:(CGFloat)radius customFrame:(CGFloat)frame {
    UIBezierPath *rounded = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    self.layer.mask = shape;
}

+ (UIView *)containerViewWithSubviews:(NSArray *)subviews
{
    UIView *containerView = [[UIView alloc] init];
    CGFloat w = 0;
    CGFloat h = 0;
    for(UIView *view in subviews) {
        [containerView addSubview:view];
        w = MAX(w, view.right);
        h = MAX(h, view.bottom);
    }
    containerView.viewSize = CGSizeMake(w, h);
    return containerView;
}

- (void)makeEnabled {
    self.userInteractionEnabled = YES;
}

- (void)disabled {
    self.userInteractionEnabled = NO;
}

- (void)visible {
    self.hidden = NO;
}

- (void)invisible {
    self.hidden = YES;
}


- (void)scale:(CGFloat)scale {
    self.transform = CGAffineTransformMakeScale(scale, scale);
}

- (void)shadowRadius:(CGFloat)radius alpha:(CGFloat)alpha {
    self.layer.shadowRadius = radius;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = alpha;
    self.layer.shadowOffset = CGSizeMake(0, 0);
}

- (void)removeSubviews {
    for(UIView *subview in self.subviews) [subview removeFromSuperview];
}


@end
