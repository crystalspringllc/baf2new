//
//  NSString+Ext.h
//  HomeCredit
//
//  Created by Almas Adilbek on 3/18/14.
//  Copyright (c) 2014 Crystal Spring LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Ext)

- (BOOL)isNumeric;
- (BOOL)isEmpty;
- (BOOL)isNotEmpty;
- (BOOL)isNumericOnly;
- (NSString *)trim;
- (NSString *)or:(NSString *)orString;
- (NSDate *)dateWithDateFormat:(NSString *)dateFormat;
- (NSString *)preparePhone;

- (NSNumber *)numberDecimalFormat;

#if !(WATCH_OS)
-(float)heightWithFont:(UIFont *)font width:(int)width;
#endif

@end
