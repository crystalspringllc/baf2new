//
//  UIView+Coordinate.m
//  AstanaKzNationalControl
//
//  Created by Almas Adilbek on 5/4/13.
//  Copyright (c) 2013 Astana.kz. All rights reserved.
//

#import "UIView+Coordinate.h"
#import <FrameAccessor.h>

@implementation UIView (Coordinate)

-(void)centerH {
    CGRect f = self.frame;
    f.origin.x = ([self screenWidth] - f.size.width) * 0.5;
    self.frame = f;
}

-(void)centerIt {
    CGRect f = self.frame;
    f.origin.x = ([self screenWidth] - f.size.width) * 0.5;
    f.origin.y = ([self screenHeight] - f.size.height) * 0.5;
    self.frame = f;
}

-(void)centerInView:(UIView *)inView {
    CGRect f = self.frame;
    f.origin.x = (inView.frame.size.width - f.size.width) * 0.5;
    f.origin.y = (inView.frame.size.height - f.size.height) * 0.5;
    self.frame = f;
}

//-(void)yCenterRelativeTo:(UIView *)rView {
//    [self setY:((rView.frame.origin.y + rView.frame.size.height - )]
//}

-(void)setX:(float)x {
    CGRect f = self.frame;
    f.origin.x = x;
    self.frame = f;
}

-(void)setY:(float)y {
    CGRect f = self.frame;
    f.origin.y = y;
    self.frame = f;
}

-(void)setX:(float)x y:(float)y {
    CGRect f = self.frame;
    f.origin.x = x;
    f.origin.y = y;
    self.frame = f;
}

-(void)shiftX:(float)shiftX {
    CGRect f = self.frame;
    f.origin.x += shiftX;
    self.frame = f;
}

-(void)shiftY:(float)shiftY {
    CGRect f = self.frame;
    f.origin.y += shiftY;
    self.frame = f;
}

-(void)setWidth:(float)width {
    CGRect f = self.frame;
    f.size.width = width;
    self.frame = f;
}

-(void)setHeight:(float)height {
    CGRect f = self.frame;
    f.size.height = height;
    self.frame = f;
}

-(void)putBelow:(UIView *)aboveView withMargin:(float)margin {
    CGRect f = self.frame;
    f.origin.y = aboveView.frame.origin.y + aboveView.frame.size.height + margin;
    self.frame = f;
}

-(void)putAfter:(UIView *)afterView withMargin:(float)margin {
    CGRect f = self.frame;
    f.origin.x = afterView.frame.origin.x + afterView.frame.size.width + margin;
    self.frame = f;
}

-(void)putLeftFromCenter {
    [self putLeftFromCenterWithMargin:0];
}

-(void)putRightFromCenter {
    [self putRightFromCenterWithMargin:0];
}

-(void)putLeftFromCenterWithMargin:(float)margin {
    CGRect f = self.frame;
    f.origin.x = [self screenWidth] * 0.5 - self.frame.size.width - margin;
    self.frame = f;
}

-(void)putRightFromCenterWithMargin:(float)margin {
    CGRect f = self.frame;
    f.origin.x = [self screenWidth] * 0.5 + margin;
    self.frame = f;
}

// -----

-(CGSize) screenCurrentSize
{
    return [self screenSizeInOrientation:[UIApplication sharedApplication].statusBarOrientation];
}

-(CGSize) screenSizeInOrientation:(UIInterfaceOrientation)orientation
{
    CGSize size = [UIScreen mainScreen].bounds.size;
    UIApplication *application = [UIApplication sharedApplication];
    if (UIInterfaceOrientationIsLandscape(orientation))
    {
        size = CGSizeMake(size.height, size.width);
    }
    if (application.statusBarHidden == NO)
    {
        size.height -= MIN(application.statusBarFrame.size.width, application.statusBarFrame.size.height);
    }
    return size;
}

-(float)screenWidth {
    return [self screenCurrentSize].width;
}

-(float)screenHeight {
    return [self screenCurrentSize].height;
}

@end
