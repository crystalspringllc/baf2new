//
// Created by Askar Mustafin on 5/3/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "NSURL+Extension.h"


@implementation NSURL (Extension)

- (NSDictionary<NSString *, NSString *> *)parseKeyValues {
    NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:self resolvingAgainstBaseURL:NO];
    NSMutableDictionary<NSString *, NSString *> *queryParams = [NSMutableDictionary<NSString *, NSString *> new];
    for (NSURLQueryItem *queryItem in [urlComponents queryItems]) {
        if (queryItem.value == nil) {
            continue;
        }
        [queryParams setObject:queryItem.value forKey:queryItem.name];
    }
    return queryParams;
}

@end