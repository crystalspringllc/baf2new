//
//  UIView+Ext.h
//  Toppy
//
//  Created by Almas Adilbek on 4/13/15.
//  Copyright (c) 2015 Toppy Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Ext)

- (void)setMaskWithRoundingCorners:(UIRectCorner)corners;
- (void)setMaskWithRoundingCorners:(UIRectCorner)corners radius:(CGFloat)radius;
- (void)setMaskWithRoundingCorners:(UIRectCorner)corners radius:(CGFloat)radius customFrame:(CGFloat)frame;
+ (UIView *)containerViewWithSubviews:(NSArray *)subviews;
- (void)makeEnabled;
- (void)disabled;
- (void)visible;
- (void)invisible;
- (void)scale:(CGFloat)scale;
- (void)shadowRadius:(CGFloat)radius alpha:(CGFloat)alpha;
- (void)removeSubviews;

@end
