//
// Created by Almas Adilbek on 8/19/14.
//

#import <Foundation/Foundation.h>

@interface UILabel (Attributed)

- (void)setColor:(UIColor *)color forSubstring:(NSString *)substring;
- (void)setFont:(UIFont *)font forSubstring:(NSString *)substring;
- (void)setFont:(UIFont *)font textColor:(UIColor *)color forSubstring:(NSString *)substring;

@end