//
//  UIButton+Tap.h
//  Toppy
//
//  Created by Almas Adilbek on 4/19/15.
//  Copyright (c) 2015 Toppy Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Tap)

- (void)tap:(id)target selector:(SEL)sel;

@end
