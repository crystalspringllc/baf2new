//
//  UIColor+Myth.m
//  Myth
//
//  Created by Almas Adilbek on 10/22/13.
//
//

@implementation UIColor (Myth)

+(UIColor *)ocean {
    return [self fromRGB:0x4db3d1];
}
+(UIColor *)darkGreen {
    return [self fromRGB:0x2e946d];
}
+(UIColor *)darkBlue {
    return [self fromRGB:0x136687];
}
+(UIColor *)blue {
    return [self fromRGB:0x65a4cf];
}
+(UIColor *)linkColor {
    return [self fromRGB:0x1078b4];
}
+(UIColor *)deleteColor {
    return [self fromRGB:0xd45948];
}

+ (UIColor *)headerBgColor {
    return [self fromRGB:0xF4F4F4];
}

+ (UIColor *)headerTitleColor {
    return [self fromRGB:0x5C8193];
}

+ (UIColor *)amountColor {
    return [self fromRGB:0x21546D];
}

+ (UIColor *)lightLineColor {
    return [self fromRGB:0xF2F2F2];
}

+ (UIColor *)separatorLineColor {
    return [self fromRGB:0xE3E3E3];
}

+ (UIColor *)blueTitleColor {
    return [UIColor fromRGB:0x21546D];
}

+ (UIColor *)comissionColor
{
    return [self fromRGB:0xAE9765];
}


@end
