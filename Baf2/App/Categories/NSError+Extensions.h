//
//  NSError+Extensions.h
//  Baf2
//
//  Created by Shyngys Kassymov on 05.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (Extensions)

+ (instancetype)errorFromDomain:(NSString *)domain message:(NSString *)message code:(NSString *)code;

@end
