//
//  UIColor+Extensions.m
//  Kiwi.kz
//
//  Created by Almas Adilbek on 11/3/12.
//
//

#import <ChameleonFramework/ChameleonMacros.h>

@implementation UIColor (Extensions)

+(UIColor *)fromRGB:(int)rgb {
    return [UIColor colorWithRed:((float)((rgb & 0xFF0000) >> 16))/255.0 green:((float)((rgb & 0xFF00) >> 8))/255.0 blue:((float)(rgb & 0xFF))/255.0 alpha:1.0];
}

+ (UIColor *)bbb {
    return [self fromRGB:0xbbbbbb];
}

+ (UIColor *)bafBlueColor {
    return [self fromRGB:0x146888];
}

- (UIColor *)lighterColor
{
    CGFloat h, s, b, a;
    if ([self getHue:&h saturation:&s brightness:&b alpha:&a])
        return [UIColor colorWithHue:h
                          saturation:s
                          brightness:MIN(b * 1.3, 1.0)
                               alpha:a];
    return nil;
}

- (UIColor *)darkerColor
{
    CGFloat h, s, b, a;
    if ([self getHue:&h saturation:&s brightness:&b alpha:&a])
        return [UIColor colorWithHue:h
                          saturation:s
                          brightness:b * 0.75
                               alpha:a];
    return nil;
}

#pragma mark - App colors (3.0)

+ (UIColor *)appGradientTopGreenColor {
    return [UIColor flatGreenColorDark];
}

+ (UIColor *)appGradientBottomGreenColor {
    return [UIColor flatGreenColorDark];
}

+ (UIColor *)appDarkGreenColor {
    return [UIColor flatGreenColorDark];
}

+ (UIColor *)mainButtonColor {
    return [UIColor flatOrangeColor];
}

+ (UIColor *)mainButtonShadowColor{
    return [UIColor flatOrangeColorDark];
}

+ (UIColor *)appDisclaimerYellowColor {
    return [UIColor fromRGB:0xffffbb];
}

+ (UIColor *)appBlueColor {
    return [UIColor fromRGB:0x146888];
}

+ (UIColor *)appDarkBlueColor {
    return [UIColor fromRGB:0x0f4f74];
}

+ (UIColor *)appGrayColor {
    return [UIColor fromRGB:0x4c4c4c];
}

+ (UIColor *)appBrownColor {
    return [UIColor fromRGB:0x8D6D36];
}


@end
