//
//  NSDateFormatter+Ext.m
//  BAF
//
//  Created by Almas Adilbek on 1/26/15.
//
//

#import "NSDateFormatter+Ext.h"

@implementation NSDateFormatter (Ext)

+ (NSDateFormatter *)enUS {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    return df;
}

+ (NSDateFormatter *)ruRu {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
    return df;
}

+ (NSDateFormatter *)baf2DateFormatter {
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    return df;
}

@end