//
// Created by Almas Adilbek on 8/19/14.
//

#import <CoreText/CoreText.h>
#import "UILabel+Attributed.h"


@implementation UILabel (Attributed)

- (void)setColor:(UIColor *)color forSubstring:(NSString *)substring
{
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: self.attributedText];

    [text addAttribute:NSForegroundColorAttributeName value:color range:[self.text rangeOfString:substring]];
    [self setAttributedText: text];
}

- (void)setFont:(UIFont *)font forSubstring:(NSString *)substring {
    [self setFont:font textColor:nil forSubstring:substring];
}

- (void)setFont:(UIFont *)font textColor:(UIColor *)color forSubstring:(NSString *)substring
{
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: self.attributedText];
    NSRange substringRange = [self.text rangeOfString:substring];

    [text beginEditing];
    [text addAttribute:NSFontAttributeName value:font range:substringRange];
    if(color) [text addAttribute:NSForegroundColorAttributeName value:color range:substringRange];
    [text endEditing];

    self.attributedText = text;
}


@end