//
//  NSObject+Ext.h
//  Myth
//
//  Created by Almas Adilbek on 6/27/13.
//
//

#import <Foundation/Foundation.h>

@interface NSObject (Ext)

-(BOOL)isNull:(id)object;

@end
