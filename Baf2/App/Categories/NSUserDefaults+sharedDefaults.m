//
//  NSUserDefaults+sharedDefaults.m
//  BAF
//
//  Created by Almas Adilbek on 6/26/15.
//
//

#import "NSUserDefaults+sharedDefaults.h"


@implementation NSUserDefaults (sharedDefaults)

+ (NSUserDefaults *)shared {
    return [[NSUserDefaults alloc] initWithSuiteName:kAppGroupContainer];
}

@end