//
//  NSDateFormatter+Ext.h
//  BAF
//
//  Created by Almas Adilbek on 1/26/15.
//
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Ext)

+ (NSDateFormatter *)enUS;
+ (NSDateFormatter *)ruRu;

+ (NSDateFormatter *)baf2DateFormatter;

@end
