//
// Created by almasadilbek on 9/14/13.
//
// To change the template use AppCode | Preferences | File Templates.
//


@implementation UIFont (Custom)

+(UIFont *)ptSans:(int)fontSize {
    return [UIFont fontWithName:@"PTSans-Regular" size:fontSize];
}

+(UIFont *)ptSansCaption:(int)fontSize {
    return [UIFont fontWithName:@"PTSans-Caption" size:fontSize];
}

+(UIFont *)ptSansNarrow:(int)fontSize {
    return [UIFont fontWithName:@"PTSans-Narrow" size:fontSize];
}

+ (UIFont *)helveticaNeue:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
}

+ (UIFont *)boldHelveticaNeue:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:fontSize];
}

+ (UIFont *)helveticaNeueThin:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-Thin" size:fontSize];
}

+ (UIFont *)mediumHelveticaNeue:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize];
}


@end