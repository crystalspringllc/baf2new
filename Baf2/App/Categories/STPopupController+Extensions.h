//
//  STPopupController+Extensions.h
//  Baf2
//
//  Created by Shyngys Kassymov on 14.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <STPopup/STPopup.h>

@interface STPopupController (Extensions)

@property (nonatomic) BOOL dismissOnBackgroundTap;
@property (nonatomic, strong) UITapGestureRecognizer *tgr;

@end
