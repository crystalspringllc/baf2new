//
//  UITableViewCell+Ext.h
//  Myth
//
//  Created by Almas Adilbek on 10/12/13.
//
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Ext)

- (void)setupDefaultSelection;
- (void)setGreenCheckmark;

- (void)showLoader;
- (void)hideLoader;

@end
