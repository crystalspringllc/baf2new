//
// Created by Askar Mustafin on 8/31/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "UITableViewController+Extension.h"


@implementation UITableViewController (Extension)

- (void)setBAFTableViewBackground {
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_new"]];
}

- (void)setWGTableViewBackgorund {
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_wg"]];
}

@end
