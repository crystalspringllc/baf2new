//
//  UIImage+Extensions.h
//  Myth
//
//  Created by Almas Adilbek on 8/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extensions)

+ (UIImage *)imageWithName:(NSString *)imageName;
+ (UIImage *)imageWithView:(UIView *)view;
+ (UIImage *)localImageNamed:(NSString *)imageName;
- (UIImage *)resizeImageToMinimumEdge:(CGFloat)size;

- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;

- (UIImage *)blurredImageWithRadius:(CGFloat)radius;

+ (UIImage *)imageWithImage:(UIImage *)sourceImage scaledToWidth:(float)i_width;
+ (UIImage *)scaleImage:(UIImage *)image maxWidth:(int) maxWidth maxHeight:(int) maxHeight;

@end
