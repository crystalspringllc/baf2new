//
//  NSString+Validators.h
//  Baf2
//
//  Created by Shyngys Kassymov on 23.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Validators)

- (BOOL)validate;
- (BOOL)validatePhoneNumber;
- (BOOL)validatePassword;
- (BOOL)validateNewPassword;
- (BOOL)validateIin;
- (BOOL)validateIinWithMessage:(NSString *)message;
- (BOOL)validateEmail;

- (BOOL)validateWithMessage:(NSString *)message;

// Format corrections
- (NSString *)validPhoneNumber;

@end