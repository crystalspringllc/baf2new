//
//  UIViewController+Extension.h
//  Baf2
//
//  Created by Askar Mustafin on 4/6/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Extension)

//@property (nonatomic, strong) UIActivityIndicatorView *rightNavBarSpinner;

- (UIView *)createBafBackgroundView;
- (void)setBafBackground;

- (void)showBafLogo;

- (void)setNavigationBackButton;
- (void)hideBackButton;
- (void)hideBackButtonTitle;
- (void)showPhoneButton;
- (void)showMenuButton;
- (void)showRefreshAtRight:(BOOL)right;

- (void)setNavigationTitle:(NSString *)title;
- (void)setNavigationTitle:(NSString *)title withTextColor:(UIColor *)textColor;
- (void)setNavigationButton:(UIButton *)button atLeft:(BOOL)atLeft;

- (void)closeViewController;
- (void)setNavigationCloseButtonAtLeft:(BOOL)atLeft;
- (void)setNavigationCloseButtonAtLeft:(BOOL)atLeft btnColorLight:(BOOL)btnColorLight;

// New
//- (UIBarButtonItem *)setBarButtonWithTitle:(NSString *)title color:(UIColor *)color target:(id)target selector:(SEL)selector atLeft:(BOOL)atLeft;
//
//
//- (void)showRightSpinner;
//- (void)hideRightSpinner;

// V2
- (void)goBack;
- (void)popViewController;
- (void)showErrorMessageAndGoBack;
- (void)refresh;

- (BOOL)isModal;
- (void)hideKeyboard;

@end
