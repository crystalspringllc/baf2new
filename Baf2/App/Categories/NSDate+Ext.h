//
// Created by Almas Adilbek on 4/12/14.
// Copyright (c) 2014 Crystal Spring LLC. All rights reserved.
//

/*
 *
 * EEE, dd MMM yyyy HH:mm:ss zzz
 */


#import <Foundation/Foundation.h>

@interface NSDate (Ext)

- (NSDate *)getDateYersAgo:(int)numOfYears;
- (NSDate *)getDateYears:(int)numOfYears monthsAgo:(int)numOfMonths andDaysAgo:(int)numOfDays;

- (NSDate *)getMonthStartDate;
- (NSDate *)oldDateBySubtractingDays:(int)daysNumber;
- (NSDate *)oldDateBySubtractingMonths:(int)monthsNumber;
- (NSDate *)oldDateBySubtractingYears:(int)yearsNumber;

- (NSString *)stringWithDateFormat:(NSString *)dateFormat;
- (NSString *)stringWithDateFormat:(NSString *)dateFormat isCurrentLocale:(BOOL)isCurrentLocale;

+ (NSString *)dayPrefixFromDayNum:(int)day;
+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;

- (NSString *)ddMMyyyy;
+ (double)timestampFromString:(NSString*)string;


+ (NSDate *)formattedDateFromString:(NSString *)string;

@end