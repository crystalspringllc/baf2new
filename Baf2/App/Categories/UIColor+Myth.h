//
//  UIColor+Myth.h
//  Myth
//
//  Created by Almas Adilbek on 10/22/13.
//
//

#import <UIKit/UIKit.h>

@interface UIColor (Myth)

+(UIColor *)ocean;
+(UIColor *)darkGreen;
+(UIColor *)darkBlue;
+(UIColor *)blue;
+(UIColor *)linkColor;
+(UIColor *)deleteColor;

+(UIColor *)headerBgColor;
+(UIColor *)headerTitleColor;

+(UIColor *)amountColor;
+(UIColor *)lightLineColor;
+(UIColor *)separatorLineColor;
+(UIColor *)blueTitleColor;
+(UIColor *)comissionColor;

@end
