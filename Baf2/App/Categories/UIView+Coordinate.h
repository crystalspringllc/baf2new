//
//  UIView+Coordinate.h
//  AstanaKzNationalControl
//
//  Created by Almas Adilbek on 5/4/13.
//  Copyright (c) 2013 Astana.kz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Coordinate)

-(void)centerH;
-(void)centerIt;
-(void)centerInView:(UIView *)inView;

//-(void)yCenterRelativeTo:(UIView *)rView;

-(void)setX:(float)x;
-(void)setY:(float)y;
-(void)setX:(float)x y:(float)y;
-(void)setWidth:(float)width;
-(void)setHeight:(float)height;

-(void)shiftX:(float)shiftX;
-(void)shiftY:(float)shiftY;

-(void)putBelow:(UIView *)aboveView withMargin:(float)margin;
-(void)putAfter:(UIView *)afterView withMargin:(float)margin;

-(void)putLeftFromCenter;
-(void)putRightFromCenter;
-(void)putLeftFromCenterWithMargin:(float)margin;
-(void)putRightFromCenterWithMargin:(float)margin;

-(float)screenWidth;
-(float)screenHeight;
-(CGSize) screenCurrentSize;
-(CGSize) screenSizeInOrientation:(UIInterfaceOrientation)orientation;

@end
