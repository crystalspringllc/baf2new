//
//  UIViewController+NavigationController.h
//  BAF
//
//  Created by Almas Adilbek on 5/28/15.
//
//

#import <UIKit/UIKit.h>

@interface UIViewController (NavigationController)

- (void)popToRoot;
- (void)popToRoot:(BOOL)animated;

@end
