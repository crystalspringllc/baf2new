//
// Created by Almas Adilbek on 8/22/14.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Ext)

- (NSString *)decimalFormatString;

@end