//
// Created by Almas Adilbek on 8/25/14.
//

#import <Foundation/Foundation.h>
#import "AAField.h"

@interface AAField (Validation)

- (BOOL)validate;

- (BOOL)validatePhoneNumber;

- (BOOL)validateAmount;

- (BOOL)validateIin;

- (BOOL)validateEmail;

- (NSString *)validPhoneNumber;

@end