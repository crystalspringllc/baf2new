//
//  STPopupController+Extensions.m
//  Baf2
//
//  Created by Shyngys Kassymov on 14.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "STPopupController+Extensions.h"
#import <objc/runtime.h>

@implementation STPopupController (Extensions)

#pragma mark - Actions

- (void)backgroundViewDidTap {
    [self dismiss];
}

#pragma mark - Getters/Setters

- (BOOL)dismissOnBackgroundTap {
    return [objc_getAssociatedObject(self, @selector(dismissOnBackgroundTap)) boolValue];
}

- (void)setDismissOnBackgroundTap:(BOOL)dismissOnBackgroundTap {
    objc_setAssociatedObject(self, @selector(dismissOnBackgroundTap), @(dismissOnBackgroundTap), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    if (dismissOnBackgroundTap) {
        self.tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector (backgroundViewDidTap)];
        [self.backgroundView addGestureRecognizer:self.tgr];
    } else {
        [self.backgroundView removeGestureRecognizer:self.tgr];
    }
}

- (UITapGestureRecognizer *)tgr {
    return objc_getAssociatedObject(self, @selector(tgr));
}

- (void)setTgr:(UITapGestureRecognizer *)tgr {
    objc_setAssociatedObject(self, @selector(tgr), tgr, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
