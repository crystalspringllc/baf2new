//
//  NSObject+Ext.m
//  Myth
//
//  Created by Almas Adilbek on 6/27/13.
//
//

#import "NSObject+Ext.h"

@implementation NSObject (Ext)

-(BOOL)isNull:(id)object {
    return object == nil || [object isEqual:[NSNull null]];
}

@end
