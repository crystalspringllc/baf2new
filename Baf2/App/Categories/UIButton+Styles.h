//
//  UIButton+Styles.h
//  ACB
//
//  Created by nmaksut on 25.05.15.
//
//

#import <UIKit/UIKit.h>

@interface UIButton (Styles)

- (void)blueStyle;
- (void)whiteStyle;


@end
