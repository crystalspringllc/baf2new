//
//  UIButton+Styles.m
//  ACB
//
//  Created by nmaksut on 25.05.15.
//
//

#import "UIButton+Styles.h"

@implementation UIButton (Styles)

- (void)blueStyle
{
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor fromRGB:0xeeeeee] forState:UIControlStateHighlighted];

    [self setBackgroundImage:[[UIImage imageNamed:@"btn_blue_normal"] resizableImageWithCapInsets:UIEdgeInsetsMake(23, 20, 23, 20) resizingMode:UIImageResizingModeTile] forState:UIControlStateNormal];
    [self setBackgroundImage:[[UIImage imageNamed:@"btn_blue_pressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(23, 20, 23, 20) resizingMode:UIImageResizingModeTile] forState:UIControlStateHighlighted];
}

- (void)whiteStyle
{
    [self setBackgroundImage:[[UIImage imageNamed:@"btn_white_normal"] resizableImageWithCapInsets:UIEdgeInsetsMake(23, 20, 23, 20) resizingMode:UIImageResizingModeTile] forState:UIControlStateNormal];
    [self setBackgroundImage:[[UIImage imageNamed:@"btn_white_pressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(23, 20, 23, 20) resizingMode:UIImageResizingModeTile] forState:UIControlStateHighlighted];
}


@end
