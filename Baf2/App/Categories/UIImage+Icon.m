//
// Created by Almas Adilbek on 8/14/14.
//

#import "UIImage+Icon.h"
#import "OtherBankCard.h"
#import "UIImage+Extensions.h"

@implementation UIImage (Icon)

+ (UIImage *)imageWithBg:(NSString *)bgImageName icon:(NSString *)iconName
{
    UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:bgImageName]];

    UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:iconName]];
    iconView.x = (CGFloat) ((bgView.width - iconView.width) * 0.5);
    iconView.y = (CGFloat) ((bgView.height - iconView.height) * 0.5);
    [bgView addSubview:iconView];

    return [UIImage imageWithView:bgView];
}

+ (UIImage *)currencyIconImage:(NSString *)currency
{
    NSString *iconName = nil;
    if([currency isEqual:@"KZT"]) {
        iconName = @"icon_tenge";
    } else if([currency isEqual:@"USD"]) {
        iconName = @"icon-USD";
    } else if([currency isEqual:@"EUR"]) {
        iconName = @"icon-EUR";
    } else if([currency isEqual:@"RUB"]) {
        iconName = @"icon-RUB";
    } else if([currency isEqual:@"GBP"]) {
        iconName = @"icon-GBP";
    }

    return [UIImage imageWithBg:@"icon-product-bg-mini" icon:iconName];
}

@end