
//
//  UIColor+Extensions.h
//  Kiwi.kz
//
//  Created by Almas Adilbek on 11/3/12.
//
//

#import <UIKit/UIKit.h>

@class UIColor;

@interface UIColor (Extensions)


#pragma mark - App colors (3.0)

+ (UIColor *)fromRGB:(int)rgb;
- (UIColor *)darkerColor;
+ (UIColor *)bbb;
+ (UIColor *)bafBlueColor;
- (UIColor *)lighterColor;


+ (UIColor *)appDarkGreenColor;
+ (UIColor *)appGradientTopGreenColor;
+ (UIColor *)appGradientBottomGreenColor;
+ (UIColor *)appDisclaimerYellowColor;
+ (UIColor *)appBlueColor;
+ (UIColor *)appDarkBlueColor;
+ (UIColor *)appGrayColor;
+ (UIColor *)appBrownColor;
+ (UIColor *)mainButtonColor;
+ (UIColor *)mainButtonShadowColor;

@end
