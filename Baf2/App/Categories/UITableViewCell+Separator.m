//
//  UITableViewCell+Separator.m
//  Myth
//
//  Created by Almas Adilbek on 4/11/13.
//
//

#import "UITableViewCell+Separator.h"

#define kCellSeparatorTag 33

@implementation UITableViewCell (Separator)

-(void)setupSeparator {
    [self setupSeparatorWithCellHeight:0];
}
-(void)setupSeparatorWithCellHeight:(float)height
{
    UIImageView *separator = (UIImageView *)[self.contentView viewWithTag:kCellSeparatorTag];
    if(!separator) {
        separator = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 500, 2)];
        separator.tag = kCellSeparatorTag;
        separator.image = [UIImage imageNamed:@"pattern-sidemenu-cell-seperator"];
        [self.contentView addSubview:separator];
    }
    
    CGRect f = separator.frame;
    f.origin.y = height==0?self.contentView.frame.size.height:height;
    separator.frame = f;
}

@end
