//
//  BankBranchTableViewCell+ConfigureForBankBranch.m
//  BAF
//
//  Created by Shyngys Kassymov on 24.07.15.
//
//

#import "BankBranchTableViewCell+ConfigureForBankBranch.h"
#import "BankBranch.h"
#import "NSNumber+Ext.h"

@implementation BankBranchTableViewCell (ConfigureForBankBranch)

- (void)configureForBankBranch:(BankBranch *)bankBranch {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping; //set the line break mode
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont helveticaNeue:14], NSFontAttributeName, paragraphStyle, NSParagraphStyleAttributeName, nil];
    
    self.branchTitleLabel.text = bankBranch.title;
    CGRect rect = CGRectIntegral([bankBranch.title boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                            options:NSStringDrawingUsesLineFragmentOrigin
                                                         attributes:attributes
                                                            context:nil]);
    self.branchTitleLabel.frame = CGRectMake(self.branchTitleLabel.x, self.branchTitleLabel.y, self.branchTitleLabel.width, rect.size.height);
    
    self.branchAddressLabel.text = bankBranch.address;
    rect = CGRectIntegral([bankBranch.address boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                 attributes:attributes
                                                    context:nil]);
    self.branchAddressLabel.frame = CGRectMake(self.branchAddressLabel.x, self.branchTitleLabel.bottom + 7, self.branchAddressLabel.width, rect.size.height);
    
    self.branchPhoneLabel.text = bankBranch.phone;
    rect = CGRectIntegral([bankBranch.phone boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                        attributes:attributes
                                                           context:nil]);
    self.branchPhoneLabel.frame = CGRectMake(self.branchPhoneLabel.x, self.branchAddressLabel.bottom + 5, self.branchPhoneLabel.width, rect.size.height);
    
    self.branchWorkTimeLabel.text = bankBranch.workTime;
    rect = CGRectIntegral([bankBranch.workTime boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                        attributes:attributes
                                                           context:nil]);
    self.branchWorkTimeLabel.frame = CGRectMake(self.branchWorkTimeLabel.x, self.branchPhoneLabel.bottom + 5, self.branchWorkTimeLabel.width, rect.size.height);
    
    self.branchEmailAddressLabel.text = bankBranch.email;
    rect = CGRectIntegral([bankBranch.email boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                        attributes:attributes
                                                           context:nil]);
    self.branchEmailAddressLabel.frame = CGRectMake(self.branchEmailAddressLabel.x, self.branchWorkTimeLabel.bottom + 5, self.branchEmailAddressLabel.width, rect.size.height);
    
    if (bankBranch.distanceToUser >= 1000) {
        self.branchDistanceLabel.text = [NSString stringWithFormat:@"%@ %@", [[NSNumber numberWithFloat:bankBranch.distanceToUser/1000.0] decimalFormatString], NSLocalizedString(@"distance_kilometr", nil)];
    } else {
        self.branchDistanceLabel.text = [NSString stringWithFormat:@"%d %@", bankBranch.distanceToUser, NSLocalizedString(@"distance_metr", nil)];
    }
    rect = CGRectIntegral([self.branchDistanceLabel.text boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                attributes:attributes
                                                                   context:nil]);
    self.branchDistanceLabel.frame = CGRectMake(self.branchDistanceLabel.x, self.branchEmailAddressLabel.bottom + 5, self.branchDistanceLabel.width, rect.size.height);
}

@end
