//
// Created by Almas Adilbek on 8/25/14.
//

#import "AAField+Validation.h"
#import "NSString+Ext.h"

#define kZeroString @"0"

@implementation AAField (Validation)

-(BOOL)validate {
    if([self value].length == 0) {
        [self focus];
        return NO;
    }
    return YES;
}

-(BOOL)validatePhoneNumber
{
    NSString *value = [self validPhoneNumber];
    if(value.length == 0) {
        [self focus];
        [UIHelper showAlertWithMessageTitle:@"" message:NSLocalizedString(@"enter_phone_number", nil)];
        return NO;
    }
    if(value.length > 11) {
        [self focus];
        [UIHelper showAlertWithMessageTitle:@"" message:NSLocalizedString(@"incorrect_phone_number", nil)];
        return NO;
    }
    return YES;
}

-(BOOL)validateAmount
{
    NSString *value = [self value];
    if(value.length == 0 || [value isEqual:kZeroString]) {
        [UIHelper showAlertWithMessageTitle:@"" message:NSLocalizedString(@"enter_amount", nil)];
        [self focus];
        return NO;
    }

    if(![value isNumericOnly]) {
        [UIHelper showAlertWithMessageTitle:@"" message:NSLocalizedString(@"enter_correct_amount", nil)];
        [self focus];
        return NO;
    }

    return YES;
}

- (BOOL)validateIin
{
    if(![self validate]) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"type_your_iin", nil)];
        return NO;
    }

    if(![[self value] isNumericOnly]) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"iin_must_contain_only_numbers", nil)];
        [self focus];
        return NO;
    }

    if([self value].length != 12) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"entered_incorrect_iin", nil)];
        [self focus];
        return NO;
    }
    return YES;
}

- (BOOL)validateEmail
{
    NSString *emailRegex =
            @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
                    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
                    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
                    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
                    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
                    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
                    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    if(![emailTest evaluateWithObject:[self value]]) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"incorrect_email", nil)];
        [self focus];
        return NO;
    }

    return YES;
}

#pragma mark -
#pragma mark Format corrections

- (NSString *)validPhoneNumber {
    NSString *value = [self value];
    value = [value stringByReplacingOccurrencesOfString:@"+" withString:@""];
    value = [value stringByReplacingOccurrencesOfString:@" " withString:@""];
    value = [value stringByReplacingOccurrencesOfString:@"(" withString:@""];
    value = [value stringByReplacingOccurrencesOfString:@")" withString:@""];

    if(value.length == 11) {
        value = [value substringFromIndex:1];
    }
    return value;
}

@end