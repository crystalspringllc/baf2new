//
// Created by almasadilbek on 9/14/13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@interface UIFont (Custom)

+ (UIFont *)ptSans:(int)fontSize;
+ (UIFont *)ptSansCaption:(int)fontSize;
+ (UIFont *)ptSansNarrow:(int)fontSize;

+ (UIFont *)helveticaNeue:(CGFloat)fontSize;
+ (UIFont *)boldHelveticaNeue:(CGFloat)fontSize;
+ (UIFont *)helveticaNeueThin:(CGFloat)fontSize;
+ (UIFont *)mediumHelveticaNeue:(CGFloat)fontSize;

@end