//
//  BankBranchTableViewCell+ConfigureForBankBranch.h
//  BAF
//
//  Created by Shyngys Kassymov on 24.07.15.
//
//

#import "BankBranchTableViewCell.h"

@class BankBranch;

@interface BankBranchTableViewCell (ConfigureForBankBranch)
- (void)configureForBankBranch:(BankBranch *)bankBranch;
@end
