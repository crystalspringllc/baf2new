//
// Created by Askar Mustafin on 5/3/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Extension)

- (NSDictionary<NSString *, NSString *> *)parseKeyValues;

@end