//
//  NSString+Ext.m
//  HomeCredit
//
//  Created by Almas Adilbek on 3/18/14.
//  Copyright (c) 2014 Crystal Spring LLC. All rights reserved.
//

#import "NSString+Ext.h"

@implementation NSString (Ext)

#pragma mark - from number converter

- (NSNumber *)numberDecimalFormat {
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    nf.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    nf.usesGroupingSeparator = YES;
    NSString *numStr = [self stringByReplacingOccurrencesOfString:@"," withString:@"."];
    NSString *numStrWithoutSpaces = [numStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    return [nf numberFromString:numStrWithoutSpaces];
}

#pragma mark -

- (NSString *)preparePhone {
    NSString *phone = [self stringByReplacingOccurrencesOfString:@"+7" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@")" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];

    if (phone.length == 11) {
        if ([[phone substringToIndex:1] isEqualToString:@"8"]) {
            phone = [phone stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:@""];
        }
    }

    phone = [[phone componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];

    NSArray* words = [phone componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    return [words componentsJoinedByString:@""];
}

- (NSString *)trim {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(BOOL)isNumeric {
    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:self];
    return [alphaNums isSupersetOfSet:inStringSet];
}

- (NSDate *)dateWithDateFormat:(NSString *)dateFormat {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    NSDate *dateFromString = [dateFormatter dateFromString:self];
    return dateFromString;
}

- (BOOL)isEmpty {
    return [self trim].length == 0;
}

- (BOOL)isNotEmpty {
    return ![self isEmpty];
}

- (NSString *)or:(NSString *)orString
{
    if(![self isEmpty]) return self;
    if(!orString) orString = @"";
    return orString;
}

- (BOOL)isNumericOnly {
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    NSNumber *num = [nf numberFromString:self];
    return num != nil;
}

#if !(WATCH_OS)
-(float)heightWithFont:(UIFont *)font width:(int)width {
    CGSize size = [self boundingRectWithSize:CGSizeMake(width, MAXFLOAT)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:@{
                                                NSFontAttributeName : font
                                                }
                                      context:nil].size;

    CGSize labelSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
    return labelSize.height;
}
#endif

@end
