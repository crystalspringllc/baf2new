//
//  UIView+AAPureLayout.m
//  Toppy
//
//  Created by Almas Adilbek on 4/24/15.
//  Copyright (c) 2015 Toppy Inc. All rights reserved.
//

#import <PureLayout/ALView+PureLayout.h>
#import "UIView+AAPureLayout.h"

@implementation UIView (AAPureLayout)

- (NSArray *)aa_autoSetDimensions {
    if(self.translatesAutoresizingMaskIntoConstraints) {
        self.translatesAutoresizingMaskIntoConstraints = YES;
    }
    return [self autoSetDimensionsToSize:self.frame.size];
}

- (NSLayoutConstraint *)aa_verticalAlignWithView:(UIView *)view {
    return [self autoAlignAxis:ALAxisHorizontal toSameAxisOfView:view];
}

- (NSLayoutConstraint *)aa_centerVertical {
    return [self autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
}

- (NSLayoutConstraint *)aa_centerHorizontal
{
    return [self autoAlignAxisToSuperviewAxis:ALAxisVertical];
}

- (NSLayoutConstraint *)aa_pinHorizontalAfterView:(UIView *)view {
    return [self aa_pinHorizontalAfterView:view offset:0];
}

- (NSLayoutConstraint *)aa_pinHorizontalAfterView:(UIView *)view offset:(CGFloat)offset {
    return [self autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:view withOffset:offset];
}

- (NSLayoutConstraint *)aa_pinUnderView:(UIView *)view {
    return [self aa_pinUnderView:view offset:0];
}

- (NSLayoutConstraint *)aa_pinUnderView:(UIView *)view offset:(CGFloat)offset
{
    return [self autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:view withOffset:offset];
}

- (NSLayoutConstraint *)aa_sameLeftEdge:(UIView *)view
{
    return [self autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:view];
}

- (NSLayoutConstraint *)aa_sameRightEdge:(UIView *)view
{
    return [self autoPinEdge:ALEdgeTrailing toEdge:ALEdgeTrailing ofView:view];
}

- (NSArray *)aa_leftAndRight:(UIView *)view {
    NSLayoutConstraint *leftConstraint = [self aa_sameLeftEdge:view];
    NSLayoutConstraint *rightConstraint = [self aa_sameRightEdge:view];
    return @[leftConstraint, rightConstraint];
}

- (NSLayoutConstraint *)aa_superviewTop:(CGFloat)inset {
    return [self autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:inset];
}

- (NSLayoutConstraint *)aa_superviewLeft:(CGFloat)inset {
    return [self autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:inset];
}

- (NSLayoutConstraint *)aa_superviewRight:(CGFloat)inset {
    return [self autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:inset];
}

- (NSLayoutConstraint *)aa_superviewBottom:(CGFloat)inset {
    return [self autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:inset];
}

- (void)aa_insideSuperview:(CGFloat)inset
{
    [self aa_superviewLeft:inset];
    [self aa_superviewRight:inset];
    [self aa_superviewTop:inset];
    [self aa_superviewBottom:inset];
}

- (NSLayoutConstraint *)aa_setWidth:(CGFloat)width {
    return [self autoSetDimension:ALDimensionWidth toSize:width];
}

- (NSLayoutConstraint *)aa_setHeight:(CGFloat)height {
    return [self autoSetDimension:ALDimensionHeight toSize:height];
}

- (void)aa_centerWithView:(UIView *)view
{
    [self autoAlignAxis:ALAxisHorizontal toSameAxisOfView:view];
    [self autoAlignAxis:ALAxisVertical toSameAxisOfView:view];
}


@end
