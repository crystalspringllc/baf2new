//
//  UITableViewCell+Separator.h
//  Myth
//
//  Created by Almas Adilbek on 4/11/13.
//
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Separator)

-(void)setupSeparator;
-(void)setupSeparatorWithCellHeight:(float)height;

@end
