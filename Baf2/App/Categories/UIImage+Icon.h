//
// Created by Almas Adilbek on 8/14/14.
//

#import <Foundation/Foundation.h>

@class OtherBankCard;

@interface UIImage (Icon)

+(UIImage *)imageWithBg:(NSString *)bgImageName icon:(NSString *)iconName;
+(UIImage *)currencyIconImage:(NSString *)currency;

@end