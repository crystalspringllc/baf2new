//
//  UIViewController+NavigationController.m
//  BAF
//
//  Created by Almas Adilbek on 5/28/15.
//
//

#import "UIViewController+NavigationController.h"

@implementation UIViewController (NavigationController)

- (void)popToRoot {
    [self popToRoot:YES];
}

- (void)popToRoot:(BOOL)animated {
    [self.navigationController popToRootViewControllerAnimated:animated];
}


@end
