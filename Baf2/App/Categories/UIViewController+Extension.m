//
//  UIViewController+Extension.m
//  Baf2
//
//  Created by Askar Mustafin on 4/6/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "Buttons.h"

@implementation UIViewController (Extension)

#pragma mark - config ui


/**
 *
 * Backgrounds
 *
 */

- (UIView *)createBafBackgroundView {
    UIImageView *backgroundImageView = [UIImageView newAutoLayoutView];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.image = [UIImage imageNamed:@"bg_new"];
    backgroundImageView.clipsToBounds = true;
    return backgroundImageView;
}

- (void)setBafBackground
{
    /*self.view.backgroundColor = [UIColor whiteColor];
    UIView *backgroundImageView = [self createBafBackgroundView];
    [self.view addSubview:backgroundImageView];
    [self.view sendSubviewToBack:backgroundImageView];
    [backgroundImageView autoPinEdgesToSuperviewEdges];*/
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_new"]] ;
}

/**
 *
 * LOGO
 *
 */

- (void)showBafLogo {
    if(self.navigationController) {
        UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_logo_astana"]];
        logoView.height = 20;
        logoView.width = 100;
        logoView.contentMode = UIViewContentModeScaleAspectFit;
        self.navigationItem.titleView = logoView;
    }
}

/**
 *
 * Nav bar buttons
 *
 */
- (void)showRefreshAtRight:(BOOL)right {
    UIBarButtonItem *refreshButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh)];
    if (right) {
        self.navigationItem.rightBarButtonItem = refreshButtonItem;
    } else {
        self.navigationItem.leftBarButtonItem = refreshButtonItem;
    }
}

- (void)showMenuButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 40, 40);
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"ic_menu_white"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(anchorLeft) forControlEvents:UIControlEventTouchUpInside];
    [self setNavigationButton:button atLeft:NO];
}

- (void)showPhoneButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 40, 40);
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"ic_phone_white"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(callPhone:) forControlEvents:UIControlEventTouchUpInside];
    [self setNavigationButton:button atLeft:YES];
}

- (void)setNavigationBackButton {
    if (self.navigationController.viewControllers.count > 1) {
        [self setNavigationButton:[Buttons backButtonWithTarget:self selector:@selector(goBack)] atLeft:YES];
    }
}

- (void)setNavigationButton:(UIButton *)button atLeft:(BOOL)atLeft {
    if(atLeft) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    } else {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    }
}

- (void)hideBackButton {
    self.navigationItem.hidesBackButton = YES;
}


/**
 *
 * nav bar titles
 *
 */

- (void)hideBackButtonTitle {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}

- (void)setNavigationTitle:(NSString *)title {
    [self setNavigationTitle:title withTextColor:[UIColor fromRGB:0x444444]];
}

- (void)setNavigationTitle:(NSString *)title withTextColor:(UIColor *)textColor {
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont systemFontOfSize:16];
    titleLabel.textColor = textColor;
    titleLabel.text = title;

    [titleLabel sizeToFit];
    titleLabel.centerX = (CGFloat) ([ASize screenWidth] * 0.5);
    titleLabel.centerY = 22;
    self.navigationItem.titleView = titleLabel;
}

#pragma mark - config actions

- (void)refresh {
}

- (void)goBack {
    [self popViewController];
}

- (void)popViewController {
    if(self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)anchorLeft {
    [kSideMenuController showRightViewAnimated:YES completionHandler:nil];
}

- (void)search {

}

- (void)showErrorMessageAndGoBack {
    [self goBack];
}

- (BOOL)isModal {
    return self.presentingViewController.presentedViewController == self
            || self.navigationController.presentingViewController.presentedViewController == self.navigationController
            || [self.tabBarController.presentingViewController isKindOfClass:[UITabBarController class]];
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (void)callPhone:(UIButton *)button {
    UIAlertController *actionSheetController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"call_bank", nil) message:NSLocalizedString(@"select_number_to_call", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    actionSheetController.popoverPresentationController.sourceView = button;
    actionSheetController.popoverPresentationController.sourceRect = button.frame;
    
    [actionSheetController addAction:[UIAlertAction actionWithTitle:@"+7 (727) 259-60-60" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *phoneNumber = @"telprompt://+77272596060";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }]];
    
    [actionSheetController addAction:[UIAlertAction actionWithTitle:@"2555" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *phoneNumber = @"telprompt://2555";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }]];
    
    [actionSheetController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:actionSheetController animated:true completion:nil];
}

#pragma mark -

- (void)closeViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setNavigationBackButton:(UIImage *)image {
    [self setNavigationBackButton:image animated:NO];
}

- (void)setNavigationBackButton:(UIImage *)image animated:(BOOL)animated {
    if(self.navigationController)
    {
        if(!image) image = [UIImage imageNamed:@"icon-arrow-back.png"];
        [self.navigationItem hidesBackButton];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 0, 24, 24);
        [backButton setBackgroundImage:image forState:UIControlStateNormal];
        backButton.showsTouchWhenHighlighted = YES;
        [backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];

        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        [self.navigationItem setLeftBarButtonItem:backBarButton animated:animated];
    }
}

- (void)setNavigationCloseButtonAtLeft:(BOOL)atLeft {
    [self setNavigationCloseButtonAtLeft:atLeft btnColorLight:NO];
}

- (void)setNavigationCloseButtonAtLeft:(BOOL)atLeft btnColorLight:(BOOL)btnColorLight {
    if(self.navigationController)
    {
        UIButton *button = [Buttons closeButtonWithTarget:self selector:@selector(closeViewController) btnColorLight:btnColorLight];
        UIBarButtonItem *closeBarButton = [[UIBarButtonItem alloc] initWithCustomView:button];

        if(atLeft) {
            self.navigationItem.leftBarButtonItem = closeBarButton;
        } else {
            self.navigationItem.rightBarButtonItem = closeBarButton;
        }
    }
}

//- (UIBarButtonItem *)setBarButtonWithTitle:(NSString *)title color:(UIColor *)color target:(id)target selector:(SEL)selector atLeft:(BOOL)atLeft {
//    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:target action:selector];
//
//    if(atLeft) {
//        self.navigationItem.leftBarButtonItem = barButton;
//    } else {
//        self.navigationItem.rightBarButtonItem = barButton;
//    }
//
//    return barButton;
//}
//
//- (void)showRightSpinner {
//    if (!self.rightNavBarSpinner) {
//        self.rightNavBarSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    }
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightNavBarSpinner];
//    [self.rightNavBarSpinner startAnimating];
//}
//
//- (void)hideRightSpinner {
//    [self.rightNavBarSpinner stopAnimating];
//}

@end
