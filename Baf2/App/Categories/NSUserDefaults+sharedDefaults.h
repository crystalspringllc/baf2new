//
//  NSUserDefaults+sharedDefaults.h
//  BAF
//
//  Created by Almas Adilbek on 6/26/15.
//
//

#import <Foundation/Foundation.h>

#define kAppGroupContainer @"group.kz.crystalspring.baf"

@interface NSUserDefaults (sharedDefaults)

+ (NSUserDefaults *)shared;

@end