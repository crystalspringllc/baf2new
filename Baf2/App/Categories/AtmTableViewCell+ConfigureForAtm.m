//
//  AtmTableViewCell+ConfigureForAtm.m
//  BAF
//
//  Created by Shyngys Kassymov on 24.07.15.
//
//

#import "AtmTableViewCell+ConfigureForAtm.h"
#import "Atm.h"
#import "NSNumber+Ext.h"

@implementation AtmTableViewCell (ConfigureForAtm)

- (void)configureForAtm:(Atm *)atm {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping; //set the line break mode
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont helveticaNeue:14], NSFontAttributeName, paragraphStyle, NSParagraphStyleAttributeName, nil];
    
    self.atmTitleLabel.text = atm.bankName;
    CGRect rect = CGRectIntegral([atm.bankName boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:attributes
                                                     context:nil]);
    self.atmTitleLabel.frame = CGRectMake(self.atmTitleLabel.x, self.atmTitleLabel.y, self.atmTitleLabel.width, rect.size.height);
        
    self.atmAddressLabel.text = atm.address;
    rect = CGRectIntegral([atm.address boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:attributes
                                                     context:nil]);
    self.atmAddressLabel.frame = CGRectMake(self.atmAddressLabel.x, self.atmTitleLabel.bottom + 7, self.atmAddressLabel.width, rect.size.height);
    
    if (atm.distanceToUser >= 1000) {
        self.atmDistanceLabel.text = [NSString stringWithFormat:@"%@ %@", [[NSNumber numberWithFloat:atm.distanceToUser/1000.0] decimalFormatString], NSLocalizedString(@"distance_kilometr", nil)];
    } else {
        self.atmDistanceLabel.text = [NSString stringWithFormat:@"%d %@", atm.distanceToUser, NSLocalizedString(@"distance_metr", nil)];
    }
    
    rect = CGRectIntegral([self.atmDistanceLabel.text boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                 attributes:attributes
                                                    context:nil]);
    self.atmDistanceLabel.frame = CGRectMake(self.atmDistanceLabel.x, self.atmAddressLabel.bottom + 5, self.atmDistanceLabel.width, rect.size.height);
}

@end
