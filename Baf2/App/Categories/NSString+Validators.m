//
//  NSString+Validators.m
//  Baf2
//
//  Created by Shyngys Kassymov on 23.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NSString+Validators.h"
#import "NSString+Ext.h"

@implementation NSString (Validators)

- (BOOL)validate {
    if (self.length == 0) {
        return NO;
    }
    return YES;
}

- (BOOL)validatePhoneNumber {
    NSString *value = [self validPhoneNumber];
    if(value.length == 0) {
        [UIHelper showAlertWithMessageTitle:@"" message:NSLocalizedString(@"enter_phone_number", nil)];
        return NO;
    }
    if(value.length != 10) {
        [UIHelper showAlertWithMessageTitle:@"" message:NSLocalizedString(@"incorrect_phone_number", nil)];
        return NO;
    }
    return YES;
}

- (BOOL)validatePassword {
    if (![self validateWithMessage:NSLocalizedString(@"enter_password", nil)]) {
        return NO;
    }
    
    if (self.length < 8) {
        [UIHelper showAlertWithMessageTitle:@"" message:@"Пароль должен иметь минимум 8 символов"];
        return NO;
    }
    
    return YES;
}

- (BOOL)validateNewPassword {
    if (![self validateWithMessage:NSLocalizedString(@"enter_right_password", nil)]) {
        return NO;
    }
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^(?=.*?)([a-zA-Z0-9!@#$%^&*()_+|}{<>?”:);]){8,}" options:NSRegularExpressionCaseInsensitive error:nil];
    NSTextCheckingResult *match = [regex firstMatchInString:self options:0 range:NSMakeRange(0, [self length])];
    BOOL isMatch = match != nil;
    if(!isMatch) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"password_not_match_standard", nil)];
        return NO;
    }
    return YES;
}

- (BOOL)validateIin {
    return [self validateIinWithMessage:NSLocalizedString(@"type_your_iin", nil)];
}

- (BOOL)validateIinWithMessage:(NSString *)message {
    if (![self validate]) {
        [UIHelper showAlertWithMessage:message];
        return NO;
    }
    
    if (![self isNumeric]) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"iin_must_contain_only_numbers", nil)];
        return NO;
    }
    
    if (self.length != 12) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"entered_incorrect_iin", nil)];
        return NO;
    }
    
    NSString *number = self;
    char *digits = (char *) [number UTF8String];

    int sum = 0;
    int controlNumCal;
    
    if (number.length == 0) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_iin", nil)];
        return NO;
    }
    // length
    if (number.length != 12) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"your_iin_must_contain", nil)];
        return NO;
    }
    
    int controlNum = [[number substringWithRange:NSMakeRange(11, 1)] intValue];
    
    int q1[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
    int q2[] = { 3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2 };
    
    
    // finding a control number
    for (int i = 0; i < 11; i++) {
        sum = sum + digits[i] * q1[i];
    }
    controlNumCal = sum % 11;
    
    if (controlNumCal != controlNum && controlNumCal != 10) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"entered_incorrect_iin", nil)];
        return NO;
    }
    
    if (controlNumCal == 10) {
        sum = 0;
        // finding a control number2
        for (int i = 0; i < 11; i++) {
            sum = sum + digits[i] * q2[i];
        }
        controlNumCal = sum % 11;
        
        if (controlNumCal != controlNum) {
            [UIHelper showAlertWithMessage:NSLocalizedString(@"entered_incorrect_iin", nil)];
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)validateEmail {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    if (![emailTest evaluateWithObject:self]) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_correct_email_number", nil)];
        return NO;
    }
    
    return YES;
}


- (BOOL)validateWithMessage:(NSString *)message {
    if (![self validate]) {
        [UIHelper showAlertWithMessage:message];
        return NO;
    }
    
    return YES;
}


#pragma mark - Format corrections

- (NSString *)validPhoneNumber {
    NSString *value = self;
    value = [value stringByReplacingOccurrencesOfString:@"+7" withString:@""];
    value = [value stringByReplacingOccurrencesOfString:@"+" withString:@""];
    value = [value stringByReplacingOccurrencesOfString:@" " withString:@""];
    value = [value stringByReplacingOccurrencesOfString:@"(" withString:@""];
    value = [value stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    return value;
}

@end