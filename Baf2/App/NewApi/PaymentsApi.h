//
//  PaymentsApi.h
//  Baf2
//
//  Created by Mustafin Askar on 01.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ApiClient.h"

@interface PaymentsApi : ApiClient



+ (NSURLSessionTask *)paymentRegisterProviderId:(NSNumber *)providerId
                                     contractId:(NSNumber *)contractId
                                         amount:(NSNumber *)amount
                                     commission:(NSNumber *)commission
                           amountWithCommission:(NSNumber *)amountWithCommission
                                    bonusAmount:(NSNumber *)bonusAmount
                                      acquiring:(NSString *)acquiring
                                         cardId:(NSNumber *)cardId
                               additionalParams:(NSString *)additionalParams
                                    invoiceData:(NSString *)invoiceData
                                      invoiceId:(NSNumber *)invoiceId
                                    subContract:(NSString *)subContract
                                    entAttempts:(NSString *)entAttempts
                                        success:(success)success
                                        failure:(failure)failure;

+ (NSURLSessionTask *)paymentRegisterProviderId:(NSNumber *)providerId contractId:(NSNumber *)contractId amount:(NSNumber *)amount commission:(NSNumber *)commission amountWithCommission:(NSNumber *)amountWithCommission bonusAmount:(NSNumber *)bonusAmount acquiring:(NSString *)acquiring cardId:(NSNumber *)cardId additionalParams:(NSString *)additionalParams success:(success)success failure:(failure)failure;

+ (NSURLSessionTask *)processOrderId:(NSNumber *)orderId success:(success)success failure:(failure)failure;
+ (NSURLSessionTask *)checkContract:(NSString *)contract providerId:(NSNumber *)providerId success:(success)success failure:(failure)failure;
+ (NSURLSessionTask *)checkAccount:(NSNumber *)providerId contract:(NSString *)contract success:(success)success failure:(failure)failure;

+ (NSURLSessionTask *)checkAndCalcCommissionAccountId:(NSNumber *)accountId accountType:(NSString *)accountType providerCode:(NSString *)providerCode bank:(NSString *)bank card:(NSString *)card paymentAmount:(NSNumber *)paymentAmount contract:(id)contract acquiring:(NSString *)acquiring success:(success)success failure:(failure)failure;



+ (NSURLSessionTask *)getPaymentInfoByServiceLogUUID:(NSString *)uuid success:(success)success failure:(failure)failure;
+ (NSURLSessionTask *)getEpayProcessingLinkParamsCardId:(NSNumber *)cardId paymentId:(NSNumber *)paymentId cardType:(NSString *)cardType bank:(NSString *)bank processing:(NSString *)processing uuid:(NSString *)uuid success:(success)success failure:(failure)failure;
+ (NSURLSessionTask *)getPaymentInfoMakePaymentUUID:(NSString *)uuid payboxParams:(NSDictionary *)payboxParams success:(success)success failure:(failure)failure;
+ (NSURLSessionTask *)getPaymentInfoMakePaymentUUID:(NSString *)uuid success:(success)success failure:(failure)failure;
+ (NSURLSessionTask *)checkConfirmSMSPaymentOrderId:(NSNumber *)orderId code:(id)code success:(success)success failure:(failure)failure;


/**
 *  Register
 *
 */
+ (NSURLSessionTask *)paymentForInsuranceWithAmount:(NSNumber *)amount
                               amountWithCommission:(NSNumber *)amountWithCommission
                                             cardId:(NSNumber *)cardId
                                          acquiring:(NSString *)acquiring
                                         commission:(NSNumber *)commission
                                        bonusAmount:(NSNumber *)bonusAmount
                                          payOnline:(BOOL)payOnline
                                         providerId:(NSNumber *)providerId
                                         bonusMalus:(NSNumber *)bonusMalus
                                       deliveryDate:(NSString *)deliveryDate
                                          firstName:(NSString *)firstName
                                           lastName:(NSString *)lastName
                                         middleName:(NSString *)middleName
                                              email:(NSString *)email
                                              phone:(NSString *)phone
                                             cityId:(NSNumber *)cityId
                                           cityName:(NSString *)cityName
                                        ogpoRequest:(NSDictionary *)ogpoRequest
                                             street:(NSString *)street
                                               home:(NSString *)home
                                               flat:(NSString *)flat
                                            success:(success)success
                                            failure:(failure)failure;
@end