//
//  ContractsApi.m
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ContractsApi.h"
#import "Contract.h"

@implementation ContractsApi

+ (NSURLSessionDataTask *)getContractsWithSuccess:(void (^)(NSArray *contracts))onSuccess failure:(failure)failure {
    return [self POST:@"contracts/get_contracts_by_session" parameters:nil success:^(id response) {
        if (onSuccess) {
            NSMutableArray *contracts = [NSMutableArray new];
            
            NSArray *contractList = response[@"contracts"];
            for (NSDictionary *contractData in contractList) {
                Contract *ppbc = [Contract instanceFromDictionary:contractData];
                [contracts addObject:ppbc];
            }
                        
            onSuccess(contracts);
        }
    } failure:failure];
}

+ (NSURLSessionDataTask *)addContract:(NSNumber *)providerId contract:(NSString *)contract contractAlias:(NSString *)contractAlias success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"providerId"] = providerId;
    params[@"contract"] = contract;
    params[@"contractAlias"] = contractAlias;
    
    return [self POST:@"contracts/add_contract" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)deleteContract:(NSNumber *)contractId success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"contractId"] = contractId;
    
    return [self POST:@"contracts/delete_contract" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)updateContractAlias:(NSNumber *)contractId alias:(NSString *)alias success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"contractId"] = contractId;
    params[@"alias"] = alias;
    
    return [self POST:@"contracts/update_contract_alias" parameters:params success:success failure:failure];
}

@end
