//
//  ProvidersApi.h
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ApiClient.h"

@class ProviderCategory;

@interface ProvidersApi : ApiClient

+ (NSURLSessionDataTask *)getProvidersWithCategoriesWithSuccess:(void (^)(NSArray *paymentProvidersByCategories))onSuccess failure:(failure)failure;

+ (NSURLSessionDataTask *)getProvidersWithCategoriesByLocation:(NSNumber *)country city:(NSNumber *)city success:(void (^)(NSArray *paymentProvidersByCategories))onSuccess failure:(failure)failure;

+ (void)getProviderByCode:(NSString *)code success:(success)success failure:(failure)failure;

#pragma mark - Helpers

+ (ProviderCategory *)categoryByName:(NSString *)categoryName;

+ (NSArray *)enabledProvidersByCategories;
+ (NSArray *)paymentProvidersByCategories;
+ (void)setPaymentProvidersByCategories:(NSArray *)val;

@end
