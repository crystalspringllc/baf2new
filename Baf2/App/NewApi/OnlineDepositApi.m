//
// Created by Askar Mustafin on 10/18/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositApi.h"


@implementation OnlineDepositApi {}

+ (void)getDepositInfo:(success)success failure:(failure)failure {
    [self GET:@"deposit/info" parameters:nil success:success failure:failure];
}

+ (void)isBafClient:(success)success failure:(failure)failure {
    [self GET:@"clients/is_baf_client" parameters:nil success:success failure:failure];
}

+ (void)getClientEkdInfo:(success)success failure:(failure)failure {
    [self GET:@"clients/get_client_ekd_info" parameters:nil success:success failure:failure];
}

+ (void)getProvider:(success)success failure:(failure)failure {
    [self GET:@"deposit/provider" parameters:nil success:success failure:failure];
}

+ (void)getBankRequisites:(success)success failure:(failure)failure {
    [self GET:@"info/bank_requisites" parameters:nil success:success failure:failure];
}

+ (void)getBankFilials:(success)success failure:(failure)failure {
    [self GET:@"catalogs/bank/filials" parameters:nil success:success failure:failure];
}

+ (void)openInit:(NSDictionary *)params success:(success)success failure:(failure)failure {
    [self POST:@"deposit/open/init" parameters:params success:success failure:failure];
}

+ (void)openRun:(NSString *)serviceLogId smsCode:(NSString *)smsCode success:(success)success failure:(failure)failure {
    [self POST:@"deposit/open/run" parameters:@{@"servicelogId" : serviceLogId ? : [NSNull null], @"confirmCode" : smsCode ? : [NSNull null]}
       success:success failure:failure];
}

+ (void)checkSms:(NSString *)smsCode serviceLogId:(NSString *)serviceLogId success:(success)success failure:(failure)failure {
    [self POST:@"deposit/check_sms" parameters:@{@"servicelogId" : serviceLogId, @"code" : smsCode} showErrorAlert:NO success:success failure:failure];
}

+ (void)sendSmsAgain:(NSString *)serviceLogId success:(success)success failure:(failure)failure {
    [self POST:@"deposit/resend_sms" parameters:@{@"servicelogId" : serviceLogId} success:success failure:failure];
}

@end
