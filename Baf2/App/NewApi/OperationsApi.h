//
//  OperationsApi.h
//  Baf2
//
//  Created by Shyngys Kassymov on 21.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ApiClient.h"
#import "OperationHistory.h"
#import "OperationHistoryDetails.h"
#import "OperationHistoryFilter.h"

/*typedef NS_ENUM(NSNumber, PaymentServiceVersions) {
    PaymentServiceOld = 1,
    PaymentServiceNew
};
*/
typedef void(^LastOperationsResponse)(NSArray *operationStatusList, NSArray *operationList, NSString *startDate, NSString *endDate);
typedef void(^GetServiceVersionResponse)(NSNumber *version);
typedef void(^LastOperationDetailResponse)(OperationHistoryDetails *details);

@interface OperationsApi : ApiClient

+ (NSURLSessionDataTask *)getLastOperationsWithFilter:(OperationHistoryFilter *)filter
                                              success:(LastOperationsResponse)onSuccess
                                              failure:(failure)failure;

+ (NSURLSessionDataTask *)getInfoForDetailedOrdering:(NSNumber *)orderId
                                             success:(LastOperationDetailResponse)onSuccess
                                             failure:(failure)failure;

+ (NSURLSessionDataTask *)getPaymentsVersion:(NSNumber *)paymentServiceId
                                     success:(GetServiceVersionResponse)paymentService
                                     failure:(failure)failure;

@end
