//
//  CatalogApi.h
//  Baf2
//
//  Created by Shyngys Kassymov on 03.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ApiClient.h"

typedef enum CatalogType:NSInteger {
    CatalogTypeKnp
} CatalogType;

@interface CatalogApi : ApiClient

/**
 * @brief getting knp list
 * @param onSuccess - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)getKnpListWithSuccess:(void (^)(NSArray *knpList))onSuccess failure:(failure)failure;

/**
 * @brief getting economic sector list
 * @param onSuccess - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)getEconomicSectorsWithSuccess:(void (^)(NSArray *economicSectorList))onSuccess failure:(failure)failure;

+ (void)getDocumentIssuers:(success)success failure:(failure)failure;
+ (void)getDocumentTypes:(success)success failure:(failure)failure;

@end
