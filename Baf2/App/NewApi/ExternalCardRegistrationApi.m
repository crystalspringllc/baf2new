//
// Created by Askar Mustafin on 6/1/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "ExternalCardRegistrationApi.h"


@implementation ExternalCardRegistrationApi {

}

+ (void)cardTypesWithSuccess:(success)success failure:(failure)failure {
    [self POST:@"card_registration/card_types" parameters:nil success:success failure:failure];
}

+ (void)banksWithSuccess:(success)success failure:(failure)failure {
    [self POST:@"card_registration/banks" parameters:nil success:success failure:failure];
}

+ (void)registerExtCardWithCardType:(NSString *)cardType
                               bank:(NSString *)bank
                         processing:(NSString *)processing
                           cardName:(NSString *)cardName
                            success:(success)success
                            failure:(failure)failure {

    NSDictionary *params = @{
            @"cardType" : cardType,
            @"bank" : bank,
            @"processing" : processing,
            @"cardName" : cardName

    };
    [self POST:@"card_registration/register_ext_card" parameters:params success:success failure:failure];

}

+ (void)getEpayRegCardLinkParamsWithCardType:(NSString *)cardType
                                        bank:(NSString *)bank
                                      cardId:(NSNumber *)cardId
                                     success:(success)success
                                     failure:(failure)failure {

    NSDictionary *params = @{
            @"cardType" : cardType,
            @"bank" : bank,
            @"cardId" : cardId
    };
    [self POST:@"card_registration/get_epay_reg_card_link_params" parameters:params success:success failure:failure];
}

+ (void)saveCardRegWithUuid:(NSString *)uuid success:(success)success failure:(failure)failure {
    [self POST:@"card_registration/save_card_reg" parameters:@{@"uuid" : uuid} success:success failure:failure];
}

+ (void)registerCardByUuid:(NSString *)uuid
                   success:(success)success
                   failure:(failure)failure {
    [self POST:@"card_registration/registered_card_by_uuid" parameters:@{@"uuid" : uuid} success:success failure:failure];
}

+ (void)updateEpayCardInfoWithCardAlias:(NSString *)cardAlias
                             processing:(NSString *)processing
                               mainCard:(BOOL)maincard
                                 cardId:(NSNumber *)cardId
                                success:(success)success
                                failure:(failure)failure {

    [self POST:@"card_registration/update_epay_card_info" parameters:@{

            @"cardAlias":cardAlias,
            @"processing":processing,
            @"mainCard":maincard?@"1":@"0",
            @"cardId":cardId

    } success:^(id response) {
        success(response);
    } failure:failure];

}

+ (void)deleteCardWithCardId:(NSNumber *)cardId success:(success)success failure:(failure)failure {
    [self POST:@"card_registration/delete_card" parameters:@{@"cardId" : cardId} success:success failure:failure];
}

+ (void)updateEpayCardApproveStateWithCardId:(NSNumber *)cardId
                                     approve:(BOOL)approve
                                     success:(success)success
                                     failure:(failure)failure {
    [self POST:@"card_registration/update_epay_card_approve_state" parameters:@{

            @"cardId" : cardId,
            @"approve" : approve?@1:@0

    } success:success failure:failure];
}

+ (void)actionOnCardRegFailureWithCardId:(NSNumber *)cardId
                                 success:(success)success
                                 failure:(failure)failure {
    [self POST:@"card_registration/action_on_card_reg_failure" parameters:@{

            @"cardId" : cardId

    } success:success failure:failure];
}

@end