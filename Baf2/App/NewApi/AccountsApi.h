//
// Created by Askar Mustafin on 5/30/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiClient.h"
#import "Account.h"
#import "Beneficiary.h"
#import "Card.h"
#import "CardLimit.h"

@class OperationAccounts;


// Этот класс предназначен для запросов с счетами
//
@interface AccountsApi : ApiClient


/**
 * @brief Проверка на закешированность счетов на сервере
 * @param success вызывается после успешной загрузки данных
 * @param failure вызывается после не успешной загрузки данных
 */
+ (void)isUpdating:(success)success failure:(failure)failure;

/**
 * @brief Загрузка уже закешированных счетов да данный период (картсчетов, карт, карт другого банка, ткущие счета, депозитов, кредитов)
 * @param success вызывается после успешной загрузки данных
 * @param failure вызывается после не успешной загрузки данных
 */
+ (void)getAccountsSuccess:(success)success failure:(failure)failure;

/**
 * @brief Загрузка счетов с принудительной загрузкой на сервер (картсчетов, карт, карт другого банка, ткущие счета, депозитов, кредитов)
 * @param isForce если YES то принудительно загружать последние состояние на сервер
 *                     NO  то принудительно загружать последнее состояние только если прошло больше 5 минут
 * @param success вызывается после успешной загрузки данных
 * @param failure вызывается после не успешной загрузки данных
 */
+ (void)getAccountsWithUpdateForce:(BOOL)isForce withSuccess:(success)success failure:(failure)failure;

/**
 * @brief Загрузка выписки счетов
 * @param success вызывается после успешной загрузки данных
 * @param failure вызывается после не успешной загрузки данных
 */
+ (void)getAccountStatementsWithAccountId:(NSNumber *)accountId
                              accountType:(NSString *)accountType
                                 dateFrom:(NSDate *)dateFrom
                                   dateTo:(NSDate *)dateTo
                                  success:(success)success
                                  failure:(failure)failure;
/**
 * @brief Загрузка счета по его id
 * @param onSuccess вызывается после успешной загрузки данных
 * @param failure вызывается после не успешной загрузки данных
 * @param NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)getAccount:(NSNumber *)accountId
                         accountType:(NSString *)accountType
                             success:(void (^)(id account))onSuccess
                             failure:(failure)failure;

/**
 * @brief Get operation accounts (debet and credit accounts with cards, ext cards, currents, deposits, loans and beneficiars)
 * @param onSuccess only session needed
 * @param failure
 * @returns NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)getOperationAccountsWithSuccess:(void (^)(OperationAccounts *operationAccounts))onSuccess
                                                  failure:(failure)failure;

/**
 * @brief searching beneficiary account
 * @param iin
 * @param accountType
 * @param lastDigits
 * @param onSuccess only session needed
 * @param failure
 * @returns NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)findBeneficiaryWithIIN:(NSString *)iin
                                     accountType:(AccountType)accountType
                                      lastDigits:(NSString *)lastDigits
                                         success:(void (^)(Beneficiary *beneficiary))onSuccess
                                         failure:(failure)failure;

/**
 * @brief disabling beneficiary account
 * @param beneficiaryId
 * @param success only session needed
 * @param failure
 * @returns NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)disableBeneficiary:(BOOL)disable
                               beneficiaryId:(NSNumber *)beneficiaryId
                                     success:(success)success
                                     failure:(failure)failure;

/**
 * @brief saving interbank beneficiary to use after
 * @param iin - beneficiary iin
 * @param iban - beneficiary iban
 * @param alias - short account nickname
 * @param firstName
 * @param lastName
 * @param companyName
 * @param isResident - is bank resident
 * @param economicSector
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)saveInterbankBeneficiary:(NSString *)iin iban:(NSString *)iban alias:(NSString *)alias firstName:(NSString *)firstName lastName:(NSString *)lastName companyName:(NSString *)companyName isResident:(BOOL)isResident economicSector:(NSNumber *)economicSector success:(success)success failure:(failure)failure;

/**
 * @brief set alias to account
 * @param accountId - account id
 * @param accountType - account type
 * @param alias - alias text
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)setAccount:(NSNumber *)accountId
                         accountType:(NSString *)accountType
                               alias:(NSString *)alias
                             success:(success)success
                             failure:(failure)failure;
/**
 * @brief add account to favourites
 * @param accountId - account id
 * @param accountType - account type
 * @param isFavorite - bool is favourite
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)setAccount:(NSNumber *)accountId
                         accountType:(NSString *)accountType
                          isFavorite:(BOOL)isFavorite
                             success:(success)success
                             failure:(failure)failure;

/**
 * @brief serach account by iin and tel
 * @param iin - iin
 * @param tel - tel
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)checkMyAccountsWithIin:(NSString *)iin
                                             tel:(NSString *)tel
                                         success:(success)success
                                         failure:(failure)failure;

/**
 * @brief Check confirmation sms
 * @param code - sms code
 * @param tel - phone number
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)checkConfirmationSmsAbisWithCode:(NSString *)code
                                                       tel:(NSString *)tel
                                                   success:(success)success
                                                   failure:(failure)failure;

/**
 * @brief resned confirmation sms
 * @param phone - phone number
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)sendConfirmationSmsAbisWithPhone:(NSString *)phone
                                                   success:(success)success
                                                   failure:(failure)failure;


#pragma mark - Limits

/**
 * @brief get all limits in card
 * @param cardId - card id
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)getCardLimits:(NSNumber *)cardId
                                success:(void(^)(NSArray *limits))success
                                failure:(failure)failure;

/**
 * @brief set card limit
 * @discussion every card have limit on atms, internet transactions
 * @param cardId - card id
 * @param limitCode - limit code (type of limit)
 * @param limitAmount - amount of limit
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)setCardLimit:(NSNumber *)cardId
                             limitCode:(NSString *)limitCode
                           limitAmount:(NSNumber *)limitAmount
                               success:(success)success
                               failure:(failure)failure;

#pragma mark - getting cashback

/**
 * @brief getting cashbash amount
 * @discussion card account have cashback that come every month
 * @param accountId - account id
 * @param accountType - account type
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)getCashbackAccountId:(NSNumber *)accountId
                 accountType:(NSString *)accountType
                     success:(success)success
                     failure:(failure)failure;

#pragma mark - block and unblock

/**
 * @brief block card by id
 * @discussion card can be blocked by several reasons
 * @param cardId - card id
 * @param status - status: "TEMPORARILY_BLOCKED_IBFL", "LOST", "STOLEN"
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)blockAccount:(NSNumber *)cardId
                                status:(NSString *)status
                               success:(success)success
                               failure:(failure)failure;

/**
 * @brief register to unblock card
 * @param cardId - card id
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)unblockAccount:(NSNumber *)cardId
                                 success:(success)success
                                 failure:(failure)failure;

/**
 * @brief process to unblock card
 * @param cardId - card id
 * @param code - sms code
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)unblockProcessAccount:(NSNumber *)cardId
                                           code:(NSString *)code
                                        success:(success)success
                                        failure:(failure)failure;

/**
 * @brief resend sms before unblocking card
 * @param cardId - card id
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)resendSmsWithCardId:(NSNumber *)cardId
                                      success:(success)success
                                      failure:(failure)failure;

@end
