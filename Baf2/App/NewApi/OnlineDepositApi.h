//
// Created by Askar Mustafin on 10/18/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiClient.h"


@interface OnlineDepositApi : ApiClient

+ (void)getDepositInfo:(success)sucecss failure:(failure)failure;
+ (void)isBafClient:(success)success failure:(failure)failure;
+ (void)getClientEkdInfo:(success)success failure:(failure)failure;
+ (void)getProvider:(success)success failure:(failure)failure;
+ (void)getBankRequisites:(success)success failure:(failure)failure;
+ (void)getBankFilials:(success)success failure:(failure)failure;

+ (void)checkSms:(NSString *)smsCode serviceLogId:(NSString *)serviceLogId success:(success)success failure:(failure)failure;
+ (void)sendSmsAgain:(NSString *)serviceLogId success:(success)success failure:(failure)failure;

+ (void)openInit:(NSDictionary *)params success:(success)success failure:(failure)failure;
+ (void)openRun:(NSString *)serviceLogId smsCode:(NSString *)smsCode success:(success)success failure:(failure)failure;

@end
