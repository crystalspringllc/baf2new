//
//  ExchangeRatesApi.m
//  BAF
//
//  Created by Almas Adilbek on 10/6/15.
//
//

#import "ExchangeRatesApi.h"
#import "ExchangeRate.h"

@implementation ExchangeRatesApi

+ (NSURLSessionDataTask *)getCourses:(success)success failure:(failure)failure {
    return [self GET:@"catalogs/exchange/rates" parameters:nil success:^(id response) {
        NSLog(@"Exchange Response %@", response);
        success([self responseToExchangeRates:response[@"rates"]]);
    } failure:failure];
}

+ (NSURLSessionDataTask *)getCoursesWithUpdate:(success)success failure:(failure)failure {
    return [self POST:@"catalogs/exchange/rates/update" parameters:nil success:^(id response) {
        success([self responseToExchangeRates:response[@"rates"]]);
    } failure:failure];
}

#pragma mark -
#pragma mark Helper

+ (NSArray *)responseToExchangeRates:(id)response {
    NSMutableArray * array = [NSMutableArray array];
    for(NSDictionary *item in response) {
        NSLog(@"item exchange %@", item);
        [array addObject:[ExchangeRate instanceFromDictionary:item]];
    }
    return array;
}

@end
