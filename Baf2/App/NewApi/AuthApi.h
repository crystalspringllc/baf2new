//
// Created by Askar Mustafin on 5/30/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiClient.h"


@interface AuthApi : ApiClient

/**
 * @brief check for available updates
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)checkUpdatesSuccess:(success)success failure:(failure)failure;

#pragma mark -

/**
 * @brief authorize user
 * @param login - user login
 * @param password - user password
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)authByLogin:(NSString *)login password:(NSString *)password success:(success)success failure:(failure)failure;

/**
 * @brief get client information by session
 * @param session - session token
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)getClientBySession:(NSString *)session success:(success)success failure:(failure)failure;

/**
 * @brief recover email
 * @discussion sends message to email
 * @param email - email to send message
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)recoverEmail:(NSString *)email success:(success)success failure:(failure)failure;

#pragma mark - change lang

/**
 * @brief change languege
 * @discussion language of every text that return from rerver
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)changeLangSuccess:(success)success failure:(failure)failure;

#pragma mark - Registration

/**
 * @brief register user
 * @param firstName
 * @param lastName
 * @param middleName
 * @param phone
 * @param email
 * @param passwd
 * @param passwdConfirm
 * @param questionId
 * @param answer
 * @param phoneCountry
 * @param locale
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)registerUserWithFirstName:(NSString *)firstName
                                           lastName:(NSString *)lastName
                                         middleName:(NSString *)middleName
                                              phone:(NSString *)phone
                                              email:(NSString *)email
                                             passwd:(NSString *)passwd
                                      passwdConfirm:(NSString *)passwdConfirm
                                         questionId:(NSNumber *)questionId
                                             answer:(NSString *)answer
                                       phoneCountry:(NSNumber *)phoneCountry
                                             locale:(NSString *)locale
                                            success:(success)success
                                            failure:(failure)failure;

/**
 * @brief check if email already exists
 * @param email
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)isEmailExists:(NSString *)email
                                 success:(success)success
                                failure:(failure)failure;

/**
 * @brief check if phone already exists
 * @param phone
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)isPhoneExists:(NSString *)phone
                                success:(success)success
                                failure:(failure)failure;

/**
 * @brief check email code before registration
 * @param code - sms code
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)checkEmailCode:(NSString *)code
                                 success:(success)success
                                 failure:(failure)failure;

/**
 * @brief send sms code to phone pointed while fill registration form
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)sendEmailCodeWithSuccess:(success)success
                                           failure:(failure)failure;

/**
 * @brief check sms code
 * @param code - sms code
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)checkSMSCode:(NSString *)code
                                 success:(success)success
                               failure:(failure)failure;

/**
 * @brief sends sms code
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)sendSMSCodeWithSuccess:(success)success
                                         failure:(failure)failure;

/**
 * @brief getting all available secret questions
 * @discussion need to filled while registration process
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)getSecretQuestionsWithSuccess:(void (^)(NSArray *secretQuestions))success
                                                failure:(failure)failure;

#pragma mark - Password Update

/**
 * @brief getting client by username
 * @param username
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)clientByUsername:(NSString *)username
                                   success:(success)success
                                   failure:(failure)failure;

/**
 * @brief update password by sid
 * @param clientSid
 * @param oldPassword
 * @param newPassword
 * @param retypeNewPassword
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)updatePasswordBySid:(NSString *)clientSid
                                  oldPassword:(NSString *)oldPassword
                                  newPassword:(NSString *)newPassword
                            retypeNewPassword:(NSString *)retypeNewPassword
                                      success:(success)success
                                      failure:(failure)failure;

/**
 * @brief update password
 * @param oldPassword
 * @param newPassword
 * @param retypeNewPassword
 * @param success
 * @param failure
 * @return
 */
+ (NSURLSessionDataTask *)updatePassword:(NSString *)oldPassword
                             newPassword:(NSString *)newPassword
                       retypeNewPassword:(NSString *)retypeNewPassword
                                 success:(success)success
                                 failure:(failure)failure;

/**
 * @brief postpone password
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)postponePasswordChangeWithSuccess:(success)success
                                                    failure:(failure)failure;


@end
