//
//  ContractsApi.h
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ApiClient.h"

@interface ContractsApi : ApiClient

/**
 * @brieg getting contract list
 * @param onSuccess - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)getContractsWithSuccess:(void (^)(NSArray *contracts))onSuccess failure:(failure)failure;

/**
 * @brief adding new contract
 * @param providerId - provider id
 * @param contract
 * @param contractAlias
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)addContract:(NSNumber *)providerId contract:(NSString *)contract contractAlias:(NSString *)contractAlias success:(success)success failure:(failure)failure;

/**
 * @brief remove contract
 * @param contractId
 * @param onSuccess - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)deleteContract:(NSNumber *)contractId success:(success)success failure:(failure)failure;

/**
 * @brief update contract alias
 * @param contractId - contract id
 * @param alias - alias text
 * @param onSuccess - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)updateContractAlias:(NSNumber *)contractId alias:(NSString *)alias success:(success)success failure:(failure)failure;

@end
