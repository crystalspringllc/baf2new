//
// Created by Askar Mustafin on 5/30/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "User.h"
#import "NSObject+Json.h"
#import "Constants.h"

//base production url
#define kProdBaseURL @"https://24.bankastana.kz/BankAstanaApi/v1/"
//#define kTestLocalBaseUrl @"http://192.168.4.71:8080/BankAstanaApi/v1/" // MAIN LOCAL TEST

//base test url
#define kTestLocalBaseUrl @"http://test.myth.kz/BankAstanaApi/v1/" //THIS IS TEST FOR GLOBAL
//#define kTestLocalBaseUrl @"http://test.24.bankastana.kz/BankAstanaApi/v1/"
//#define kTestLocalBaseUrl @"http://pp-rest.cs.kz:8080/ServiceSystem/" //PREPROD

/**
 * @brief block to handle success response object
 */
typedef void (^success)(id response);

/**
 * @brief block to handle failure code and message
 */
typedef void (^failure)(NSString *code, NSString *message);

/**
 * @brief block to handle success message
 */
typedef void (^successMessage)(NSString *message);

/**
 * @brief block to handle cached success response object
 */
typedef void (^cacheSuccess)(id response);

/**
 * @brief block to handle success status
 */
typedef void (^successStatus)(BOOL isSuccess);

/**
 * @brief block to handle success status
 */
typedef void (^finally)(BOOL isSuccess);



@interface ApiClient : AFHTTPSessionManager

/**
 * @brief getting session manager singleton
 */
+ (AFHTTPSessionManager *)sharedManager;

/**
 * @brief Send GET request
 * @return NSURLSessionDataTask
 */

+ (NSURLSessionDataTask *)GET:(NSString *)path
                    parameters:(NSDictionary *)parameters
                       success:(success)success
                       failure:(failure)failure;

+ (NSURLSessionDataTask *)GET:(NSString *)path
                   parameters:(NSDictionary *)parameters
               showErrorAlert:(BOOL)showErrorAlert
                      success:(success)success
                      failure:(failure)failure;


/**
 * @brief Send POST request
 * @param showErrorAlert - default NO
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)POST:(NSString *)path
                    parameters:(NSDictionary *)parameters
                showErrorAlert:(BOOL)showErrorAlert
                       success:(success)success
                       failure:(failure)failure;


/**
 * @brief Send POST request with handling error and showinf them in alertView
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)POST:(NSString *)path
                    parameters:(NSDictionary *)parameters
                       success:(success)success
                       failure:(failure)failure;

/**
 * @brief cancel all operations
 */
+ (void)cancelAllOperations;

/**
 * @brief getting base url
 * @return NSString base url
 */
+ (NSString *)BASE_URL;

@end
