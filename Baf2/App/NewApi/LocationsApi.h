//
//  LocationsApi.h
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ApiClient.h"

@interface LocationsApi : ApiClient

+ (NSURLSessionDataTask *)getCountriesWithSuccess:(void (^)(NSArray *countries))onSuccess failure:(failure)failure;
+ (NSURLSessionDataTask *)getCities:(NSString *)countryId success:(void (^)(NSArray *cities))onSuccess failure:(failure)failure;

@end
