//
//  InfoApi.h
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ApiClient.h"

@interface InfoApi : ApiClient

+ (NSURLSessionDataTask *)clientInfo:(NSString *)idn success:(success)success failure:(failure)failure;
+ (NSURLSessionDataTask *)bankInfo:(NSString *)iban success:(success)success failure:(failure)failure;
+ (NSURLSessionDataTask *)sendRevieReviewText:(NSString *)reviewText Success:(success)success failure:(failure)failure;
+ (NSURLSessionDataTask *)getIpoInfoSuccess:(success)success failure:(failure)failure;

@end
