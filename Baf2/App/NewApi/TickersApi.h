//
// Created by Askar Mustafin on 7/18/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiClient.h"

@interface TickersApi : ApiClient

+ (void)getTickersSuccess:(success)success failure:(failure)failure;

@end