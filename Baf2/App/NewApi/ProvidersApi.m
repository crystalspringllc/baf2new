//
//  ProvidersApi.m
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ProvidersApi.h"
#import "PaymentProvidersByCategory.h"
#import "ProviderCategory.h"

@implementation ProvidersApi

+ (NSURLSessionDataTask *)getProvidersWithCategoriesWithSuccess:(void (^)(NSArray *paymentProvidersByCategories))onSuccess failure:(failure)failure {
    return [self POST:@"providers/get_providers_with_categories" parameters:nil success:^(id response) {
        if (onSuccess) {
            NSMutableArray *paymentProvidersByCategories = [NSMutableArray new];
            
            NSArray *ppbcList = response[@"paymentProvidersByCategories"];
            for (NSDictionary *ppbcData in ppbcList) {
                PaymentProvidersByCategory *ppbc = [PaymentProvidersByCategory instanceFromDictionary:ppbcData];
                [paymentProvidersByCategories addObject:ppbc];
            }
            
            ProvidersApi.paymentProvidersByCategories = [NSMutableArray arrayWithArray:paymentProvidersByCategories];
            
            onSuccess(paymentProvidersByCategories);
        }
    } failure:failure];
}

+ (NSURLSessionDataTask *)getProvidersWithCategoriesByLocation:(NSNumber *)country city:(NSNumber *)city success:(void (^)(NSArray *paymentProvidersByCategories))onSuccess failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"selectedCountry"] = country;
    params[@"selectedCity"] = city;
    
    return [self POST:@"providers/get_providers_with_categories_by_location" parameters:params success:^(id response) {
        if (onSuccess) {
            NSMutableArray *paymentProvidersByCategories = [NSMutableArray new];
            
            NSArray *ppbcList = response[@"paymentProvidersByCategories"];
            for (NSDictionary *ppbcData in ppbcList) {
                PaymentProvidersByCategory *ppbc = [PaymentProvidersByCategory instanceFromDictionary:ppbcData];
                [paymentProvidersByCategories addObject:ppbc];
            }
            
            // !!!: temp solution to hold all paymentProvidersByCategories
            //Caching paymentProvidersByCategories
            ProvidersApi.paymentProvidersByCategories = [NSMutableArray arrayWithArray:paymentProvidersByCategories];
            
            onSuccess(paymentProvidersByCategories);
        }
    } failure:failure];
}

+ (void)getProviderByCode:(NSString *)code success:(success)success failure:(failure)failure {
    [self POST:@"providers/get_provider_by_code" parameters:@{@"providerCode" : code} success:success failure:failure];
}

#pragma mark - Helpers

+ (ProviderCategory *)categoryByName:(NSString *)categoryName {
    ProviderCategory *providerCategory;
    
    for (PaymentProvidersByCategory *paymentProvidersByCategory in ProvidersApi.paymentProvidersByCategories) {
        ProviderCategory *pc = paymentProvidersByCategory.category;
        if ([pc.categoryName isEqualToString:categoryName]) {
            providerCategory = pc;
            break;
        }
    }
    
    return providerCategory;
}

#pragma mark - Static

+ (NSArray *)enabledProvidersByCategories {
    NSArray *paymentProvidersByCategories = ProvidersApi.paymentProvidersByCategories;
    for (PaymentProvidersByCategory *ppbc in paymentProvidersByCategories) {
        NSPredicate *filterEnabledProviders = [NSPredicate predicateWithFormat:@"disabled == NO"];
        ppbc.paymentProviderListWS = [ppbc.paymentProviderListWS filteredArrayUsingPredicate:filterEnabledProviders];
    }
    return paymentProvidersByCategories;
}

+ (NSArray *)paymentProvidersByCategories {
    NSArray *provCats = [[RequestsCache sharedCache] objectForKey:@"paymentProvidersByCategories"];
    NSMutableArray *arr = [NSMutableArray new];
    for (PaymentProvidersByCategory *p in provCats) {
        if (p.paymentProviderListWS.count > 0) {
            [arr addObject:p];
        }
    }

    return arr;
}

+ (void)setPaymentProvidersByCategories:(NSArray *)val {
    [[RequestsCache sharedCache] setObject:val forKey:@"paymentProvidersByCategories"];
}

@end
