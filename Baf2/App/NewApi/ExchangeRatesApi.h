//
//  ExchangeRatesApi.h
//  BAF
//
//  Created by Almas Adilbek on 10/6/15.
//
//



#import "ApiClient.h"

@interface ExchangeRatesApi : ApiClient

/**
 * @brief Getting cources catalog
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)getCourses:(success)success failure:(failure)failure;

/**
 * @brief Not response catalog, but just updates data from bank servers
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (NSURLSessionDataTask *)getCoursesWithUpdate:(success)success failure:(failure)failure;

@end
