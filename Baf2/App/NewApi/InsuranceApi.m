//
// Created by Askar Mustafin on 11/16/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceApi.h"
#import "NSData+Base64.h"

@implementation InsuranceApi {

}

+ (void)citiesSuccess:(success)success failure:(failure)failure {
    [self GET:@"insurance/cities" parameters:nil success:success failure:failure];
}

+ (void)catalogsByCategoryType:(CatalogType)catalogType
                                success:(success)success
                                failure:(failure)failure {

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"registration_territory"] = catalogType == 0 || catalogType == 4 ? @YES : @NO;
    params[@"transport_type"] = catalogType == 1 || catalogType == 4 ? @YES : @NO;
    params[@"age25_driving_experience"] = catalogType == 2 || catalogType == 4 ? @YES : @NO;
    params[@"transport_usage_time"] = catalogType == 3 || catalogType == 4 ? @YES : @NO;
    params[@"insurance_type"] = catalogType == 5 || catalogType == 4 ? @YES : @NO;

    [self POST:@"insurance/catalogs/by/category" parameters:params success:success failure:failure];
}

+ (void)bonusmalusByIIN:(NSNumber *)iin
                success:(success)success
                failure:(failure)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"iin"] = iin;

    [self POST:@"insurance/bonusmalus/by/iin" parameters:params success:success failure:failure];
}

+ (void)calcCars:(NSArray *)cars
        insureds:(NSArray *)insureds
 benefitDiscount:(BOOL)benefitDiscount
     vovDiscount:(BOOL)vovDiscount
         success:(success)success
         failure:(failure)failure {

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"step"] = @1;
    params[@"cars"] = [cars JSONRepresentation];
    params[@"insureds"] = [insureds JSONRepresentation];
    params[@"benefitDiscount"] = benefitDiscount ? @"true" : @"false";
    params[@"vovDiscount"] = vovDiscount ? @"true" : @"false";

    [self POST:@"insurance/calc" parameters:params success:success failure:failure];
}

+ (void)scandocsStore:(UIImage *)image
              success:(success)success
              failure:(failure)failure {

    NSData *data = UIImageJPEGRepresentation(image, 0);
    NSString *base64 = [data base64EncodedString];

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"imageBase64"] = base64;
    [self POST:@"insurance/scandocs/store" parameters:params success:success failure:failure];
}

+ (void)saveWithCars:(NSArray *)cars
            insureds:(NSArray *)insureds
     benefitDiscount:(BOOL)benefitDiscount
         vovDiscount:(BOOL)vovDiscount
 benefitDiscountDocs:(NSArray *)benefitDiscountDocs
     vovDiscountDocs:(NSArray *)vovDiscountDocs
             success:(success)success
             failure:(failure)failure {

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"step"] = @1;
    params[@"cars"] = [cars JSONRepresentation];
    params[@"insureds"] = [insureds JSONRepresentation];
    params[@"benefitDiscount"] = benefitDiscount ? @"true" : @"false";
    params[@"vovDiscount"] = vovDiscount ? @"true" : @"false";
    if (benefitDiscountDocs) params[@"benefitDiscountDocs"] = [benefitDiscountDocs JSONRepresentation];
    if (vovDiscountDocs) params[@"vovDiscountDocs"] = [vovDiscountDocs JSONRepresentation];


    [self POST:@"insurance/request/save" parameters:params success:success failure:failure];
}

+ (void)cashRun:(NSNumber *)serviceLogId success:(success)success failure:(failure)failure {
    [self POST:@"insurance/request/run" parameters:@{@"serviceLogId" : serviceLogId} success:success failure:failure];

}

@end