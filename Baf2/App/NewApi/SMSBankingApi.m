//
// Created by Askar Mustafin on 6/16/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "SMSBankingApi.h"

@implementation SMSBankingApi

#pragma mark -
#pragma mark - Sms banking

+ (NSURLSessionDataTask *)getSmsbankingStatusWithCardId:(NSNumber *)cardId
                                                success:(success)success
                                                failure:(failure)failure
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"cardId"] = cardId;
    return [self GET:@"accounts/card/smsbanking"
          parameters:params
             success:success
             failure:failure];
}

+ (NSURLSessionDataTask *)initSmsbankingWithCardId:(NSNumber *)cardId
                                             phone:(NSString *)phone
                                          isEnable:(BOOL)isEnable
                                           success:(success)success
                                           failure:(failure)failure
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"cardId"] = cardId;
    params[@"phone"] = phone;
    params[@"enable"] = @(isEnable);
    return [self POST:@"accounts/card/smsbanking/init"
           parameters:params
              success:success
              failure:failure];
}

+ (NSURLSessionDataTask *)checkSmsBankingSmsWithCode:(NSNumber *)code
                                        serviceLogId:(NSNumber *)serviceLogId
                                             success:(success)success
                                             failure:(failure)failure
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"servicelogId"] = serviceLogId;
    params[@"code"] = code;
    return [self POST:@"accounts/card/smsbanking/check_sms/run"
           parameters:params
              success:success
              failure:failure];
}

+ (NSURLSessionDataTask *)sendSmsAgainWithServiceLogId:(NSNumber *)serviceLogId
                                               success:(success)success
                                               failure:(failure)failure
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"servicelogId"] = serviceLogId;
    return [self POST:@"accounts/card/smsbanking/send_sms"
           parameters:params
              success:success
              failure:failure];
}

#pragma mark -
#pragma mark - change number for sms banking

+ (NSURLSessionDataTask *)initPhoneChangeWithCardId:(NSNumber *)cardId
                                           newPhone:(NSString *)phone
                                            success:(success)success
                                            failure:(failure)failure
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"cardId"] = cardId;
    params[@"newPhone"] = phone;
    return [self POST:@"accounts/card/smsbanking/update_phone/init"
           parameters:params
       showErrorAlert:NO
              success:success
              failure:failure];
}

+ (NSURLSessionDataTask *)sendSmsWithServiceLogId:(NSNumber *)serviceLogId
                                          success:(success)success
                                          failure:(failure)failure
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"servicelogId"] = serviceLogId;
    return [self POST:@"accounts/card/smsbanking/update_phone/send_sms"
           parameters:params
       showErrorAlert:NO
              success:success
              failure:failure];
}

+ (NSURLSessionDataTask *)runPhoneChangeWithServiceLogId:(NSNumber *)serviceLogId
                                                    code:(NSString *)code
                                                password:(NSString *)password
                                                 success:(success)success
                                                 failure:(failure)failure
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"servicelogId"] = serviceLogId;
    params[@"code"] = code;
    params[@"password"] = password;
    return [self POST:@"accounts/card/smsbanking/update_phone/run"
           parameters:params
       showErrorAlert:NO
              success:success
              failure:failure];

}

@end