//
//  CashBackApi.h
//  Baf2
//
//  Created by Dulatheo on 30.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiClient.h"

@interface CashBackApi : ApiClient
+ (NSURLSessionDataTask *)getCashBackDisclaimer:(NSNumber *)cardId
                                                success:(success)success
                                                failure:(failure)failure;

+ (NSURLSessionDataTask *)changeCashBackInit:(NSNumber *)cardId
                                  success:(success)success
                                  failure:(failure)failure;

+ (NSURLSessionDataTask *)changeCashBackSmsConfirm:(NSNumber *)serviceLogId
                                               code:(NSNumber *)code
                                            success:(success)success
                                            failure:(failure)failure;

+ (NSURLSessionDataTask *)changeCashBackSmsResend:(NSNumber *)serviceLogId
                                  success:(success)success
                                  failure:(failure)failure;

+ (NSURLSessionDataTask *)changeCashBackRun:(NSNumber *)serviceLogId
                                           success:(success)success
                                           failure:(failure)failure;
@end
