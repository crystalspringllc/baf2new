//
//  CatalogApi.m
//  Baf2
//
//  Created by Shyngys Kassymov on 03.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "CatalogApi.h"
#import "EconomicSector.h"
#import "Knp.h"
#import "DocumentModel.h"

@implementation CatalogApi


+ (void)getDocumentTypes:(success)success failure:(failure)failure {
    
    NSMutableArray *docTypeArray = [NSMutableArray array];
    [self GET:@"catalogs/document/type" parameters:nil success:^(id response) {
        
        for (NSDictionary *dict in response)
        {
        DocumentModel *document = [DocumentModel instanceFromDictionary:dict];
            document.definition = DocumentType;
        [docTypeArray addObject:document];
        }
        success(docTypeArray);
    
    } failure:failure];
}

+ (void)getDocumentIssuers:(success)success failure:(failure)failure {
    
    NSMutableArray *docIssuerArray = [NSMutableArray array];
    [self GET:@"catalogs/document/issuer" parameters:nil success:^(id response) {
        
        for (NSDictionary *dict in response)
        {
            DocumentModel *document = [DocumentModel instanceFromDictionary:dict];
            document.definition = DocumentIssued;
            [docIssuerArray addObject:document];
        }
        success(docIssuerArray);
        
    } failure:failure];
    
}

+ (NSURLSessionDataTask *)getKnpListWithSuccess:(void (^)(NSArray *knpList))onSuccess failure:(failure)failure {
    return [self POST:@"catalogs/knp_list" parameters:nil success:^(id response) {
        if (onSuccess) {
            NSMutableArray *knpList = [NSMutableArray new];

            
            NSArray *legalData = response[@"list"];
            for (NSDictionary *knpData in legalData) {
                Knp *knp = [Knp instanceFromDictionary:knpData];
                [knpList addObject:knp];
            }

            onSuccess(knpList);
        }
    } failure:failure];
}

+ (NSURLSessionDataTask *)getEconomicSectorsWithSuccess:(void (^)(NSArray *economicSectorList))onSuccess failure:(failure)failure {
    return [self POST:@"catalogs/economic_sectors" parameters:nil success:^(id response) {
        if (onSuccess) {
            NSMutableArray *economicSectorList = [NSMutableArray new];
            
            for (NSDictionary *economicSectorData in response[@"economicSectorList"]) {
                EconomicSector *economicSector = [EconomicSector instanceFromDictionary:economicSectorData];
                [economicSectorList addObject:economicSector];
            }
            
            onSuccess(economicSectorList);
        }
    } failure:failure];
}

@end
