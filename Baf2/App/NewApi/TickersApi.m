//
// Created by Askar Mustafin on 7/18/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "TickersApi.h"
#import "Ticker.h"


@implementation TickersApi {}

+ (void)getTickersSuccess:(success)success failure:(failure)failure {
    [self GET:@"tickers/info" parameters:nil showErrorAlert:NO success:^(id response) {
        success([Ticker instanceFromDictionary:response]);
    } failure:failure];
}

@end