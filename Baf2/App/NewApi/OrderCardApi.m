//
// Created by Askar Mustafin on 1/6/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "OrderCardApi.h"
#import "OCCategory.h"


@implementation OrderCardApi {}

+ (void)getProducts:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    params[@"channel"] = @"web";
    params[@"clientInfo"] = [AppUtils deviceInfoJsonString];

    [self.sharedManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

    [self GET:@"products/by" parameters:params success:^(id response) {
        NSMutableArray *categories = [[NSMutableArray alloc] init];
        if ([response[@"categories"] isKindOfClass:[NSArray class]]) {
            for (NSDictionary *d in response[@"categories"]) {
                OCCategory *category = [OCCategory instanceFromDictionary:d];
                [categories addObject:category];
            }
        }
        success(categories);
    } failure:failure];
}

+ (void)initWithIIN:(NSString *)iin
              phone:(NSString *)phone
           cityCode:(NSString *)cityCode
           codeWord:(NSString *)codeWord
        productCode:(NSString *)productCode
    virtualCurrency:(NSString *)virtualCurrency
            isMulti:(BOOL)isMulti
           isCredit:(BOOL)isCredit
          isVirtual:(BOOL)isVirtual
         isResident:(BOOL)isResident
        creditLimit:(NSNumber *)creditLimit
             income:(NSNumber *)income
            docType:(NSString *)docType
          docNumber:(NSString *)docNumber
       docIssueDate:(NSString *)docIssueDate
       categoryCode:(NSString *)categoryCode
           isAgreed:(BOOL)isAgreed
     childBirthDate:(NSString *)childBirthDate
           childIin:(NSString *)childIin
      childLastname:(NSString *)childLastname
     childFirstname:(NSString *)childFirstname
    childMiddlename:(NSString *)childMiddlename
    isChildResident:(BOOL)isisChildResident
            success:(success)success
            failure:(failure)failure
{

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"iin"] = iin;
    params[@"phone"] = phone;
    params[@"city"] = cityCode;
    params[@"codeWord"] = codeWord;
    params[@"productCode"] = productCode;
    params[@"isMulti"] = isMulti ? @"true" : @"false";
    params[@"isCredit"] = isCredit ? @"true" : @"false";
    params[@"isVirtual"] = isVirtual ? @"true" : @"false";
    params[@"isResident"] = isResident ? @"true" : @"false";
    params[@"channel"] = @"mob";
    params[@"categoryCode"] = categoryCode;

    if (virtualCurrency) params[@"virtualCurrency"] = virtualCurrency;
    if (creditLimit) params[@"creditLimit"] = creditLimit;
    if (income) params[@"income"] = income;
    if (docType) params[@"docType"] = docType;
    if (docNumber) params[@"docNumber"] = docNumber;
    if (docIssueDate) params[@"docIssueDate"] = docIssueDate;
    params[@"agree"] = isAgreed ? @"true" : @"false";

    if (childBirthDate) params[@"childBirthDate"] = childBirthDate;
    if (childIin) params[@"childIin"] = childIin;
    if (childLastname) params[@"childLastname"] = childLastname;
    if (childFirstname) params[@"childFirstname"] = childFirstname;
    if (childMiddlename) params[@"childMiddlename"] = childMiddlename;
    params[@"isChildResident"] = isisChildResident ? @"true" : @"false";

    [self POST:@"products/issuance/init/json" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

+ (void)runWithServiceLogId:(NSNumber *)serviceLogId success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"serviceLogId"] = serviceLogId;
    [self POST:@"products/issuance/run" parameters:params success:success failure:failure];
}

+ (void)getClientInfoByIIN:(NSString *)iin success:(success)success failure:(failure)failure {
    [self POST:@"clients/get_client_info_by_idn" parameters:@{@"idn" : iin} showErrorAlert:NO success:success failure:failure];
}

@end