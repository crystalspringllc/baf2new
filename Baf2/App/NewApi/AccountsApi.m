//
// Created by Askar Mustafin on 5/30/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "AccountsApi.h"
#import "NSDate+Ext.h"
#import "OperationAccounts.h"
#import "Deposit.h"
#import "Loan.h"
#import "Current.h"

@implementation AccountsApi {}

+ (void)isUpdating:(success)success failure:(failure)failure {
    if (!IS_API_URL_TEST)[self GET:@"accounts/is_updating" parameters:nil success:success failure:failure];
}

/**
 * Getting all account, cards, deposits and loans
 */
+ (void)getAccountsSuccess:(success)success failure:(failure)failure {
    [self POST:@"accounts/get_accounts" parameters:nil success:success failure:failure];
}

+ (void)getAccountsWithUpdateForce:(BOOL)isForce withSuccess:(success)success failure:(failure)failure {
    NSDictionary *param = @{@"force" : @(isForce)};
    [self POST:@"accounts/get_accounts_with_update" parameters:param success:success failure:failure];
}


/**
 * Getting statement
 */
+ (void)getAccountStatementsWithAccountId:(NSNumber *)accountId
                              accountType:(NSString *)accountType
                                 dateFrom:(NSDate *)dateFrom
                                   dateTo:(NSDate *)dateTo
                                  success:(success)success
                                  failure:(failure)failure {
    NSDictionary *param = @{
            @"accountId" : accountId,
            @"accountType" : accountType,
            @"dateFrom" : [dateFrom stringWithDateFormat:@"dd.MM.yyyy"],
            @"dateTo" : [dateTo stringWithDateFormat:@"dd.MM.yyyy"]};
    [self POST:@"accounts/get_account_statements" parameters:param success:success failure:failure];
}

+ (NSURLSessionDataTask *)getAccount:(NSNumber *)accountId
                         accountType:(NSString *)accountType
                             success:(void (^)(id account))onSuccess
                             failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"accountId"] = accountId;
    params[@"accountType"] = accountType;
    
    return [self POST:@"accounts/get_account" parameters:params success:^(id response) {
        if (onSuccess) {
            id account;
            
            if ([accountType isEqualToString:@"account"]) {
                account = [Account instanceFromDictionary:response];
            } else if ([accountType isEqualToString:@"card"]) {
                account = [Card instanceFromDictionary:response];
            } else if ([accountType isEqualToString:@"current_individuals"]) {
                account = [Current instanceFromDictionary:response];
            } else if ([accountType isEqualToString:@"benificiary"]) {
                account = [Beneficiary instanceFromDictionary:response];
            } else if ([accountType isEqualToString:@"deposit_individuals"]) {
                account = [Deposit instanceFromDictionary:response];
            } else if ([accountType isEqualToString:@"loan"]) {
                account = [Loan instanceFromDictionary:response];
            }
            
            onSuccess(account);
        }
    } failure:failure];
}

+ (NSURLSessionDataTask *)getOperationAccountsWithSuccess:(void (^)(OperationAccounts *))onSuccess failure:(failure)failure {
    return [self POST:@"accounts/get_operation_accounts" parameters:nil success:^(id response) {
        if (onSuccess) {
            OperationAccounts *operationAccounts = [OperationAccounts instanceFromDictionary:response];
            onSuccess(operationAccounts);
        }
    } failure:failure];
}

+ (NSURLSessionDataTask *)findBeneficiaryWithIIN:(NSString *)iin accountType:(AccountType)accountType lastDigits:(NSString *)lastDigits success:(void (^)(Beneficiary *beneficiary))onSuccess failure:(failure)failure {
    NSAssert(iin && (id)iin != [NSNull null], @"IIN should not be nil or null");
    NSAssert(lastDigits && (id)lastDigits != [NSNull null], @"lastDigits should not be nil or null");
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"iin"] = iin;
    params[@"accountType"] = [Account accountTypeToString:accountType];
    params[@"lastDigits"] = lastDigits;
    params[@"expireDate"] = @"14.04.2016";
    
    return [self POST:@"accounts/find_beneficiar" parameters:params success:^(id response) {
        if (onSuccess) {
            Beneficiary *beneficiary = [Beneficiary instanceFromDictionary:response];
            onSuccess(beneficiary);
        }
    } failure:failure];
}

+ (NSURLSessionDataTask *)disableBeneficiary:(BOOL)disable beneficiaryId:(NSNumber *)beneficiaryId success:(success)success failure:(failure)failure {
    NSAssert(beneficiaryId && (id)beneficiaryId != [NSNull null], @"beneficiaryId should not be nil or null");
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"beneficiarId"] = beneficiaryId;
    params[@"disable"] = (disable ? @"true" : @"false");
    
    return [self POST:@"accounts/disable_beneficiar" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)saveInterbankBeneficiary:(NSString *)iin iban:(NSString *)iban alias:(NSString *)alias firstName:(NSString *)firstName lastName:(NSString *)lastName companyName:(NSString *)companyName isResident:(BOOL)isResident economicSector:(NSNumber *)economicSector success:(success)success failure:(failure)failure {
    NSAssert(iin && (id)iin != [NSNull null], @"iin should not be nil or null");
    NSAssert(iban && (id)iban != [NSNull null], @"iban should not be nil or null");
    NSAssert(alias && (id)alias != [NSNull null], @"alias should not be nil or null");
//    NSAssert(firstName && (id)firstName != [NSNull null], @"firstName should not be nil or null");
//    NSAssert(lastName && (id)lastName != [NSNull null], @"lastName should not be nil or null");
//    NSAssert(companyName && (id)companyName != [NSNull null], @"companyName should not be nil or null");
//    NSAssert(economicSector && (id)economicSector != [NSNull null], @"economicSector should not be nil or null");
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"iin"] = iin;
    params[@"iban"] = iban;
    params[@"alias"] = alias;
    if (firstName) params[@"firstName"] = firstName;
    if (lastName) params[@"lastName"] = lastName;
    if (companyName) params[@"companyName"] = companyName;
    params[@"isResident"] = (isResident ? @"true" : @"false");
    if (economicSector) params[@"economicSector"] = economicSector;
    
    return [self POST:@"accounts/save_interbank_beneficiar" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)setAccount:(NSNumber *)accountId
                         accountType:(NSString *)accountType
                               alias:(NSString *)alias
                             success:(success)success
                             failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"accountId"] = accountId;
    params[@"accountType"] = accountType;
    params[@"alias"] = alias;
    
    return [self POST:@"accounts/set_alias" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)setAccount:(NSNumber *)accountId
                         accountType:(NSString *)accountType
                          isFavorite:(BOOL)isFavorite
                             success:(success)success
                             failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"accountId"] = accountId;
    params[@"accountType"] = accountType;
    params[@"isFavorite"] = isFavorite ? @"true" : @"false";
    
    return [self POST:@"accounts/set_favorite" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)checkMyAccountsWithIin:(NSString *)iin tel:(NSString *)tel success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"iin"] = iin;
    params[@"phone"] = tel;
    
    return [self POST:@"accounts_registration/check_my_accounts" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

+ (NSURLSessionDataTask *)checkConfirmationSmsAbisWithCode:(NSString *)code
                                                       tel:(NSString *)tel
                                                   success:(success)success
                                                   failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    params[@"code"] = code;
    params[@"phone"] = tel;
    
    return [self POST:@"accounts_registration/check_confirmation_sms" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

+ (NSURLSessionDataTask *)sendConfirmationSmsAbisWithPhone:(NSString *)phone success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"phone"] = phone;
    
    return [self POST:@"accounts_registration/resend_confirmation_sms" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}


#pragma mark - Limits

+ (NSURLSessionDataTask *)getCardLimits:(NSNumber *)cardId success:(void(^)(NSArray *limits))success failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"cardId"] = cardId;
    
    return [self GET:@"accounts/card/limits" parameters:params success:^(id response) {
        NSArray *limits = [CardLimit instancesFromArray:response[@"limits"]];
        success(limits);
    } failure:failure];
}

+ (NSURLSessionDataTask *)setCardLimit:(NSNumber *)cardId limitCode:(NSString *)limitCode limitAmount:(NSNumber *)limitAmount success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"cardId"] = cardId;
    params[@"limitCode"] = limitCode;
    params[@"limitAmount"] = limitAmount;
    
    return [self POST:@"accounts/card/limit/set" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

#pragma mark - getting cashback

+ (void)getCashbackAccountId:(NSNumber *)accountId accountType:(NSString *)accountType success:(success)success failure:(failure)failure {

    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"accountId"] = accountId;
    params[@"accountType"] = accountType;
    [self GET:@"accounts/get_cashback" parameters:params success:success failure:failure];

}


#pragma mark - block and unblock

+ (NSURLSessionDataTask *)blockAccount:(NSNumber *)cardId
                                status:(NSString *)status
                               success:(success)success
                               failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"cardId"] = cardId;
    params[@"status"] = status;

    return [self POST:@"accounts/block_card" parameters:params success:success failure:failure];
}
+ (NSURLSessionDataTask *)unblockAccount:(NSNumber *)cardId
                                 success:(success)success
                                 failure:(failure)failure {

    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"cardId"] = cardId;
    return [self POST:@"accounts/card/unblock/register" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)unblockProcessAccount:(NSNumber *)cardId
                                           code:(NSString *)code
                                 success:(success)success
                                 failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"cardId"] = cardId;
    params[@"code"] = code;
    return [self POST:@"accounts/card/unblock/process" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)resendSmsWithCardId:(NSNumber *)cardId success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"cardId"] = cardId;
    return [self POST:@"accounts/card/unblock/resend_sms" parameters:params success:success failure:failure];
}

@end