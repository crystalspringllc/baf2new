//
//  PinApi.h
//  BAF
//
//  Created by Almas Adilbek on 11/29/14.
//
//

#import "ApiClient.h"

@interface PinApi : ApiClient

+ (void)openClientSessionByPin:(NSString *)pin pinToken:(NSString *)pinToken success:(success)success failure:(failure)failure;
+ (void)setPin:(NSString *)pin login:(NSString *)login success:(success)success failure:(failure)failure;
+ (void)deletePin:(success)success failure:(failure)failure;

@end
