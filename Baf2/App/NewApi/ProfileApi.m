//
// Created by Askar Mustafin on 6/21/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "ProfileApi.h"


@implementation ProfileApi {}

+ (void)updateProfileWithParams:(NSDictionary *)params
                         password:(NSString *)password
                          success:(success)success
                          failure:(failure)failure
{
    [self POST:@"profile/update" parameters:params showErrorAlert:NO success:success failure:failure];
}


//EMAIL

+ (void)updateEmail:(NSString *)newEmail success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"newEmail"] = newEmail;
    [self POST:@"profile/update/email/init" parameters:params showErrorAlert:NO success:success failure:failure];
}

+ (void)checkEmailCode:(NSString *)code email:(NSString *)email serviceLogId:(NSNumber *)serviceLogId success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"code"] = code;
    params[@"email"] = email;
    params[@"serviceLogId"] = serviceLogId;
    [self POST:@"profile/update/check_email_code" parameters:params showErrorAlert:NO success:success failure:failure];
}

+ (void)checkSmsCode:(NSString *)code phone:(NSString *)phone serviceLogId:(NSNumber *)serviceLogId success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"code"] = code;
    params[@"phone"] = phone;
    params[@"serviceLogId"] = serviceLogId;
    [self POST:@"profile/update/check_sms_code" parameters:params showErrorAlert:NO success:success failure:failure];
}

+ (void)sendSmsAgain:(NSString *)phone serviceLogId:(NSNumber *)serviceLogId success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"serviceLogId"] = serviceLogId;
    params[@"phone"] = phone;
    [self POST:@"profile/update/send_email" parameters:params showErrorAlert:NO success:success failure:failure];
}

//PHONE

+ (void)updatePhone:(NSString *)newPhone isSmsToEmail:(BOOL)isSmsToEmail success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"newPhone"] = newPhone;
    params[@"isSmsToEmail"] = isSmsToEmail ? @"true" : @"false";
    [self POST:@"profile/update/phone/init" parameters:params showErrorAlert:NO success:success failure:failure];
}

//FINAL

//FOR EMAIL
+ (void)runEmailWith:(NSString *)newEmail
                smsCode:(NSString *)smsCode
           serviceLogId:(NSNumber *)serviceLogId
                success:(success)success
                failure:(failure)failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"smsCode"] = smsCode;
    params[@"newEmail"] = newEmail;
    params[@"serviceLogId"] = serviceLogId;
    [self POST:@"profile/update/email/run" parameters:params showErrorAlert:NO success:success failure:failure];
}

//FOR PHONE
+ (void)runPhoneWith:(NSString *)newPhone
             smsCodeNew:(NSString *)smsCodeNew
             smsCodeOld:(NSString *)smsCodeOld
              emailCode:(NSString *)emailCode
           serviceLogId:(NSNumber *)serviceLogId
                success:(success)success
                failure:(failure)failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"newPhone"] = newPhone;
    params[@"smsCodeNew"] = smsCodeNew;
    params[@"smsCodeOld"] = smsCodeOld;
    params[@"emailCode"] = emailCode;
    params[@"serviceLogId"] = serviceLogId;
    [self POST:@"profile/update/phone/run" parameters:params showErrorAlert:NO success:success failure:failure];
}

@end