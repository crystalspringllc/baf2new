//
//  PaymentsApi.m
//  Baf2
//
//  Created by Mustafin Askar on 01.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "PaymentsApi.h"

@implementation PaymentsApi


+ (NSURLSessionTask *)paymentRegisterProviderId:(NSNumber *)providerId
                                     contractId:(NSNumber *)contractId
                                         amount:(NSNumber *)amount
                                     commission:(NSNumber *)commission
                           amountWithCommission:(NSNumber *)amountWithCommission
                                    bonusAmount:(NSNumber *)bonusAmount
                                      acquiring:(NSString *)acquiring
                                         cardId:(NSNumber *)cardId
                               additionalParams:(NSString *)additionalParams
                                    invoiceData:(NSString *)invoiceData
                                      invoiceId:(NSNumber *)invoiceId
                                    subContract:(NSString *)subContract
                                    entAttempts:(NSString *)entAttempts
                                        success:(success)success
                                        failure:(failure)failure {

    NSMutableDictionary *p = [NSMutableDictionary new];

    NSString *urlCommand;
    if (invoiceData && invoiceId) {
        urlCommand = @"payments/invoice/simple_payment_register";
        p[@"invoiceData"]      = invoiceData;
        p[@"invoiceId"]        = invoiceId;
    } else {
        urlCommand = @"payments/simple_payment_register";
    }

    if (subContract) {
        p[@"sub_contract"]     = subContract;
    }

    if (additionalParams) {
        p[@"guid"]             = additionalParams;
    }

    p[@"providerId"]           = providerId;
    p[@"contractId"]           = contractId;
    p[@"amount"]               = amount;
    p[@"commission"]           = commission;
    p[@"amountWithCommission"] = amountWithCommission;
    p[@"bonusAmount"]          = bonusAmount;
    p[@"acquiring"]            = acquiring;
    p[@"cardId"]               = cardId;

    if (entAttempts) {
        p[@"entAttempts"]  = entAttempts;
    }

    return [self POST:urlCommand parameters:p success:success failure:failure];
};

+ (NSURLSessionTask *)paymentRegisterProviderId:(NSNumber *)providerId
                                     contractId:(NSNumber *)contractId
                                         amount:(NSNumber *)amount
                                     commission:(NSNumber *)commission
                           amountWithCommission:(NSNumber *)amountWithCommission
                                    bonusAmount:(NSNumber *)bonusAmount
                                      acquiring:(NSString *)acquiring
                                         cardId:(NSNumber *)cardId
                               additionalParams:(NSString *)additionalParams
                                        success:(success)success
                                        failure:(failure)failure {

    return [self paymentRegisterProviderId:providerId
                                contractId:contractId
                                    amount:amount
                                commission:commission
                      amountWithCommission:amountWithCommission
                               bonusAmount:bonusAmount
                                 acquiring:acquiring
                                    cardId:cardId
                          additionalParams:additionalParams
                               invoiceData:nil
                                invoiceId:nil
                               subContract:nil
                                    entAttempts:nil
                                   success:success failure:failure];
}

+ (NSURLSessionTask *)processOrderId:(NSNumber *)orderId
                             success:(success)success
                             failure:(failure)failure {
    NSMutableDictionary *p = [NSMutableDictionary new];
    p[@"orderId"] = orderId;
    
    return [self POST:@"payments/simple_payment_ow_process" parameters:p success:success failure:failure];
 
}

+ (NSURLSessionTask *)getPaymentInfoByServiceLogUUID:(NSString *)uuid
                                             success:(success)success
                                             failure:(failure)failure {
    NSMutableDictionary *p = [NSMutableDictionary new];
    p[@"uuid"] = uuid;
    
    return [self POST:@"payments/get_payment_info_by_servicelog_uid" parameters:p success:success failure:failure];
}

+ (NSURLSessionTask *)checkConfirmSMSPaymentOrderId:(NSNumber *)orderId
                                               code:(id)code
                                            success:(success)success
                                            failure:(failure)failure {
    NSMutableDictionary *p = [NSMutableDictionary new];
    p[@"orderId"] = orderId;
    p[@"code"] = code;
    
    return [self POST:@"payments/check_confirm_sms_payment" parameters:p success:success failure:failure];
}

+ (NSURLSessionTask *)checkAccount:(NSNumber *)providerId
                          contract:(NSString *)contract
                           success:(success)success
                           failure:(failure)failure {
    NSMutableDictionary *p = [NSMutableDictionary new];
    p[@"providerId"] = providerId;
    p[@"contract"] = contract;

    return [self POST:@"payments/check_account" parameters:p success:success failure:failure];
}

+ (NSURLSessionTask *)checkContract:(NSString *)contract providerId:(NSNumber *)providerId success:(success)success failure:(failure)failure {
    NSMutableDictionary *p = [NSMutableDictionary new];
    p[@"providerId"] = providerId;
    p[@"contract"] = contract;

    return [self POST:@"payments/chec_contract" parameters:p success:success failure:failure];
}

+ (NSURLSessionTask *)checkAndCalcCommissionAccountId:(NSNumber *)accountId
                                          accountType:(NSString *)accountType
                                         providerCode:(NSString *)providerCode
                                                 bank:(NSString *)bank
                                                 card:(NSString *)card
                                        paymentAmount:(NSNumber *)paymentAmount
                                             contract:(id)contract
                                            acquiring:(NSString *)acquiring
                                              success:(success)success
                                              failure:(failure)failure {
    NSMutableDictionary *p = [NSMutableDictionary new];
    p[@"providerCode"]  = providerCode;
    p[@"accountId"]     = accountId;
    p[@"accountType"]   = accountType;
    p[@"bank"]          = bank;
    if (card) p[@"card"]          = card;
    p[@"paymentAmount"] = paymentAmount;
    p[@"contract"]      = contract;
    p[@"acquiring"]     = acquiring;
    return [self POST:@"payments/check_and_calc_commission" parameters:p success:success failure:failure];
}

+ (NSURLSessionTask *)getEpayProcessingLinkParamsCardId:(NSNumber *)cardId
                                              paymentId:(NSNumber *)paymentId
                                               cardType:(NSString *)cardType
                                                   bank:(NSString *)bank
                                             processing:(NSString *)processing
                                                   uuid:(NSString *)uuid
                                                success:(success)success
                                                failure:(failure)failure {
    NSMutableDictionary *p = [NSMutableDictionary new];
    p[@"cardId"]     = cardId;
    p[@"paymentId"]  = paymentId;
    p[@"cardType"]   = cardType;
    p[@"bank"]       = bank;
    p[@"processing"] = processing;
    p[@"uuid"]       = uuid;
    return [self POST:@"payments/get_epay_processing_link_params" parameters:p success:success failure:failure];
}

+ (NSURLSessionTask *)getPaymentInfoMakePaymentUUID:(NSString *)uuid payboxParams:(NSDictionary *)payboxParams success:(success)success failure:(failure)failure {
    NSMutableDictionary *p = [NSMutableDictionary new];
    p[@"uuid"] = uuid;
    p[@"order_id"] = payboxParams[@"pg_order_id"];
    p[@"payment_id"] = payboxParams[@"pg_payment_id"];
    p[@"reference"] = payboxParams[@"pg_reference"];
    p[@"pan"] = payboxParams[@"pg_pan"];
    p[@"payment_to"] = payboxParams[@"pg_payment_to"];
    p[@"amount"] = payboxParams[@"pg_amount"];
    p[@"comission"] = payboxParams[@"pg_commission"];
    p[@"terminal"] = payboxParams[@"pg_terminal"];
    return [self POST:@"payments/get_payment_info_make_payment_paybox" parameters:p success:success failure:failure];
}

+ (NSURLSessionTask *)getPaymentInfoMakePaymentUUID:(NSString *)uuid success:(success)success failure:(failure)failure {
    NSMutableDictionary *p = [NSMutableDictionary new];
    p[@"uuid"] = uuid;
    return [self POST:@"payments/get_payment_info_make_payment" parameters:p success:success failure:failure];
}

+ (NSURLSessionTask *)paymentForInsuranceWithAmount:(NSNumber *)amount
                               amountWithCommission:(NSNumber *)amountWithCommission
                                             cardId:(NSNumber *)cardId
                                          acquiring:(NSString *)acquiring
                                         commission:(NSNumber *)commission
                                        bonusAmount:(NSNumber *)bonusAmount
                                          payOnline:(BOOL)payOnline
                                         providerId:(NSNumber *)providerId
                                         bonusMalus:(NSNumber *)bonusMalus
                                       deliveryDate:(NSString *)deliveryDate
                                          firstName:(NSString *)firstName
                                           lastName:(NSString *)lastName
                                         middleName:(NSString *)middleName
                                              email:(NSString *)email
                                              phone:(NSString *)phone
                                             cityId:(NSNumber *)cityId
                                           cityName:(NSString *)cityName
                                        ogpoRequest:(NSDictionary *)ogpoRequest
                                             street:(NSString *)street
                                               home:(NSString *)home
                                               flat:(NSString *)flat
                                            success:(success)success
                                            failure:(failure)failure
{
    NSMutableDictionary *p = [NSMutableDictionary new];
    p[@"amount"] = amount;
    p[@"amountWithCommission"] = amountWithCommission;
    if (cardId) p[@"cardId"] = cardId;
    p[@"acquiring"] = @"any";
    if (commission) p[@"commission"] = commission;
    p[@"bonusAmount"] = bonusAmount;
    p[@"payOnline"] = payOnline ? @"true" : @"false";
    p[@"providerId"] = providerId;
    p[@"bonusMalus"] = bonusMalus;
    p[@"deliveryDate"] = deliveryDate;
    p[@"firstName"] = firstName;
    p[@"lastName"] = lastName;
    p[@"middleName"] = middleName;
    p[@"email"] = email;
    p[@"phone"] = phone;
    p[@"city"] = cityId;
    p[@"cityName"] = cityName;
    p[@"ogpoRequest"] = [ogpoRequest JSONRepresentation];
    p[@"street"] = street;
    p[@"home"] = home;
    p[@"flat"] = flat;

    return [self POST:@"payments/insurance/simple_payment_register"
           parameters:p success:success failure:failure];
}

@end