//
// Created by Askar Mustafin on 5/30/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "AuthApi.h"
#import "SecretQuestion.h"
#import "AppUtils.h"
#import "User.h"
#import "NotificationCenterHelper.h"
#import "LocalizationManager.h"

@implementation AuthApi {}

+ (void)checkUpdatesSuccess:(success)success failure:(failure)failure {
    [self POST:@"general/updates" parameters:@{@"os" : @"ios", @"version" : [AppUtils appVersion]} showErrorAlert:NO success:success failure:failure];
}

+ (void)authByLogin:(NSString *)login
           password:(NSString *)password
            success:(success)success
            failure:(failure)failure {

    NSDictionary *params = @{
            @"login" : login,
            @"password" : password,
    };
    [self POST:@"auth/auth_by_login" parameters:params success:success failure:failure];

}

+ (void)getClientBySession:(NSString *)session success:(success)success failure:(failure)failure {
    NSDictionary *params = @{@"session" : session};
    [self POST:@"clients/get_client_by_session" parameters:params success:^(id response) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserLoggedIn object:nil];
        if (success) {
            success(response);
        }
    } failure:failure];
}

+ (void)recoverEmail:(NSString *)email success:(success)success failure:(failure)failure {
    NSDictionary *params = @{@"emailPhone" : email};
    [self POST:@"auth/restore_pswd" parameters:params success:success failure:failure];
}

#pragma mark - change lang

+ (void)changeLangSuccess:(success)success failure:(failure)failure {
    [self POST:@"security/change/locale"
    parameters:@{@"locale" : @"ru"/*TODO:[LocalizationManager sharedManager].currentLanguage.code*/}
       success:success
       failure:failure];
}

#pragma mark - Registration

+ (NSURLSessionDataTask *)registerUserWithFirstName:(NSString *)firstName
                                           lastName:(NSString *)lastName
                                         middleName:(NSString *)middleName
                                              phone:(NSString *)phone
                                              email:(NSString *)email
                                             passwd:(NSString *)passwd
                                      passwdConfirm:(NSString *)passwdConfirm
                                         questionId:(NSNumber *)questionId
                                             answer:(NSString *)answer
                                       phoneCountry:(NSNumber *)phoneCountry
                                             locale:(NSString *)locale
                                            success:(success)success
                                            failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"firstName"] = firstName;
    params[@"lastName"] = lastName;
    params[@"middleName"] = middleName;
    params[@"phone"] = phone;
    params[@"email"] = email;
    params[@"passwd"] = passwd;
    params[@"passwdConfirm"] = passwdConfirm;
    params[@"questionId"] = questionId;
    params[@"answer"] = answer;
    params[@"ipAddress"] = [AppUtils ipAddress];
    params[@"phoneCountry"] = phoneCountry;
    params[@"locale"] = locale;

    NSString *firstNameFirstLetter = [firstName substringToIndex:1];
    firstNameFirstLetter = [firstNameFirstLetter uppercaseString];
    params[@"nickName"] = [NSString stringWithFormat:@"%@ %@", lastName, firstNameFirstLetter];
    
    return [self POST:@"clients/json/register" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)isEmailExists:(NSString *)email
                                success:(success)success
                                failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"email"] = email;
    
    return [self POST:@"registration/is_email_exist" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)isPhoneExists:(NSString *)phone
                                success:(success)success
                                failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"phone"] = phone;
    
    return [self POST:@"registration/is_phone_exist" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)checkEmailCode:(NSString *)code
                                 success:(success)success
                                 failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"code"] = code;
    params[@"phone"] = [User sharedInstance].phoneNumber;
    return [self POST:@"registration/check_email_code" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)sendEmailCodeWithSuccess:(success)success
                                           failure:(failure)failure {
    return [self POST:@"registration/send_email_code" parameters:nil success:success failure:failure];
}

+ (NSURLSessionDataTask *)checkSMSCode:(NSString *)code
                               success:(success)success
                               failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"code"] = code;
    params[@"phone"] = [User sharedInstance].phoneNumber;
    return [self POST:@"registration/check_sms_code" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)sendSMSCodeWithSuccess:(success)success
                                         failure:(failure)failure {
    return [self POST:@"registration/send_sms_code" parameters:nil success:success failure:failure];
}

+ (NSURLSessionDataTask *)getSecretQuestionsWithSuccess:(void (^)(NSArray *secretQuestions))success
                                                failure:(failure)failure {
    return [self POST:@"registration/get_secret_questions" parameters:nil success:^(id response) {
        if (success) {
            NSMutableArray *secretQuestions = [NSMutableArray new];
            
            NSArray *questionList = response[@"questionList"];
            for (NSDictionary *questionData in questionList) {
                SecretQuestion *secretQuestion = [SecretQuestion instanceFromDictionary:questionData];
                [secretQuestions addObject:secretQuestion];
            }
            
            success(secretQuestions);
        }
    } failure:failure];
}

#pragma mark - Password Update

+ (NSURLSessionDataTask *)clientByUsername:(NSString *)username
                                    success:(success)success
                                    failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"username"] = username;
    return [self POST:@"clients/client/by/username" parameters:params success:^(id response) {
        if (success) {
            success(response);
        }
    } failure:failure];
}

+ (NSURLSessionDataTask *)updatePasswordBySid:(NSString *)clientSid
                                  oldPassword:(NSString *)oldPassword
                                  newPassword:(NSString *)newPassword
                            retypeNewPassword:(NSString *)retypeNewPassword
                                 success:(success)success
                                 failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"clientSid"] = clientSid;
    params[@"oldPassword"] = oldPassword;
    params[@"newPassword"] = newPassword;
    params[@"retypeNewPassword"] = retypeNewPassword;
    return [self POST:@"clients/password/update/by/sid" parameters:params showErrorAlert:false success:^(id response) {
        if (success) {
            success(response);
        }
    } failure:failure];
}

+ (NSURLSessionDataTask *)updatePassword:(NSString *)oldPassword
                             newPassword:(NSString *)newPassword
                       retypeNewPassword:(NSString *)retypeNewPassword
                                 success:(success)success
                                 failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"oldPassword"] = oldPassword;
    params[@"newPassword"] = newPassword;
    params[@"retypeNewPassword"] = retypeNewPassword;
    return [self POST:@"clients/password/update" parameters:params showErrorAlert:false success:^(id response) {
        if (success) {
            success(response);
        }
    } failure:failure];
}

+ (NSURLSessionDataTask *)postponePasswordChangeWithSuccess:(success)success
                                                    failure:(failure)failure {
    return [self POST:@"clients/password/update/postpone" parameters:nil showErrorAlert:false success:^(id response) {
        if (success) {
            success(response);
        }
    } failure:failure];
}

@end
