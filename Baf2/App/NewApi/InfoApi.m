
//
//  InfoApi.m
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "InfoApi.h"

@implementation InfoApi

+ (NSURLSessionDataTask *)clientInfo:(NSString *)idn success:(success)success failure:(failure)failure {
    NSAssert(idn && (id)idn != [NSNull null], @"idn should not be nil or null");
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"idn"] = idn;
    
    return [self POST:@"info/client_info" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)bankInfo:(NSString *)iban success:(success)success failure:(failure)failure {
    NSAssert(iban && (id)iban != [NSNull null], @"iban should not be nil or null");
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"iban"] = iban;
    
    return [self POST:@"info/bank_info" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)sendRevieReviewText:(NSString *)reviewText Success:(success)success failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"os"] = @"iOS";
    params[@"app_version"] = [AppUtils appVersion];
    params[@"client"] = [User sharedInstance].phoneNumber;
    params[@"review"] = reviewText;

    return [self POST:@"general/review/send" parameters:params success:success failure:failure];
}

+ (NSURLSessionDataTask *)getIpoInfoSuccess:(success)success failure:(failure)failure {
    return [self GET:@"general/ipo/info" parameters:nil success:success failure:failure];
}

@end
