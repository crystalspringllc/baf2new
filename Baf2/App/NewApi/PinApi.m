//
//  PinApi.m
//  BAF
//
//  Created by Almas Adilbek on 11/29/14.
//
//

#import "PinApi.h"
#import "AppUtils.h"
#import "PinHelper.h"
#import "DataCache.h"

@implementation PinApi

+ (void)openClientSessionByPin:(NSString *)pin pinToken:(NSString *)pinToken success:(success)success failure:(failure)failure
{
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    params[@"pin"] = pin;
    
#ifdef WATCH_OS
    params[@"deviceId"] = [DataCache instance].deviceID;
#else
    params[@"deviceId"] = [AppUtils deviceID];
#endif
    
    params[@"pinToken"] = pinToken;
    params[@"withData"] = @"NO";


    [self POST:@"auth/pin" parameters:params showErrorAlert:NO success:success failure:failure];
}

+ (void)setPin:(NSString *)pin login:(NSString *)login success:(success)success failure:(failure)failure
{
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    params[@"pin"] = pin;
    params[@"deviceId"] = [AppUtils deviceID];
    params[@"login"] = login;

    [self POST:@"auth/pin/set" parameters:params success:success failure:failure];
}

+ (void)deletePin:(success)success failure:(failure)failure
{
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    params[@"deviceId"] = [AppUtils deviceID];
    params[@"pinToken"] = [PinHelper pinToken];

    [self POST:@"auth/pin/delete" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

@end
