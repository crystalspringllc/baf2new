//
//  NewTransfersApi.m
//  Baf2
//
//  Created by Shyngys Kassymov on 31.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NewTransfersApi.h"
#import "TransferTemplate.h"
#import "FromExternalToAccountButtonCheckResponse.h"
#import "OperationAccounts.h"
#import "NewCheckAndInitTransferResponse.h"
#import "CVVAddResponse.h"
//#import "Knp.h"
//#import "NewTransferType.h"

@implementation NewTransfersApi

#pragma mark - Main

+ (NSURLSessionDataTask *)initAviaTransfer:(TransferType)transferType
                               requestorId:(NSNumber *)requestorId
                           requestorAmount:(NSNumber *)requestorAmount
                             destinationId:(NSNumber *)destinationId
                         destinationAmount:(NSNumber *)destinationAmount
                             requestorType:(NSString *)requestorType
                         requestorCurrency:(NSString *)requestorCurrency
                           destinationType:(NSString *)destinationType
                       destinationCurrency:(NSString *)destinationCurrency
                                 direction:(BOOL)direction
                                      save:(BOOL)save
                                     alias:(NSString *)alias
                                 reference:(NSString *)reference
                                   success:(success)success
                                   failure:(failure)failure {

    NSMutableDictionary *params = [NSMutableDictionary new];

    NSString *transferTypeToString = [self transferTypeToString:transferType];
    params[@"type"] = transferTypeToString;

    if (requestorId) {
        params[@"requestorId"] = requestorId;
    }

    params[@"requestorAmount"] = requestorAmount;
    params[@"destinationAmount"] = destinationAmount;

    if (destinationId) {
        params[@"destinationId"] = destinationId;
    }
    if (requestorType) {
        params[@"requestorType"] = requestorType;
    }
    if (destinationType) {
        params[@"destinationType"] = destinationType;
    }
    if (requestorType) {
        params[@"requestorCurrency"] = requestorCurrency;
    }
    if (destinationType) {
        params[@"destinationCurrency"] = destinationCurrency;
    }

    params[@"direction"] = (direction ? @"true" : @"false");

    params[@"save"] = save ? @"true" : @"false";
    if (alias && save) {
        params[@"alias"] = alias;
    }

    if (reference) {
        params[@"reference"] = reference; //operationId
    }
    NSLog(@"Params to API %@", params);
    return [self POST:@"transfers/json/init/avia" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

+ (NSURLSessionDataTask *)checkTransfer:(TransferType)transferType
                            requestorId:(NSNumber *)requestorId
                        requestorAmount:(NSNumber *)requestorAmount
                          destinationId:(NSNumber *)destinationId
                          requestorType:(NSString *)requestorType
                      requestorCurrency:(NSString *)requestorCurrency
                        destinationType:(NSString *)destinationType
                    destinationCurrency:(NSString *)destinationCurrency
                                knpCode:(NSString *)knpCode
                                knpName:(NSString *)knpName
                              direction:(BOOL)direction
                                   save:(BOOL)save
                                  alias:(NSString *)alias
                                success:(success)success
                                failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    NSString *transferTypeToString = [self transferTypeToString:transferType];
    params[@"type"] = transferTypeToString;
    
    if (requestorId) {
        params[@"requestorId"] = requestorId;
    }
    if (requestorAmount) {
        if (direction) {
            params[@"requestorAmount"] = requestorAmount;
        } else {
            params[@"destinationAmount"] = requestorAmount;
        }
    } else {
        if (direction) {
            params[@"requestorAmount"] = @(0);
        } else {
            params[@"destinationAmount"] = @(0);
        }
    }
    if (destinationId) {
        params[@"destinationId"] = destinationId;
    }
    if (requestorType) {
        params[@"requestorType"] = requestorType;
    }
    if (destinationType) {
        params[@"destinationType"] = destinationType;
    }
    if (requestorCurrency) {
        params[@"requestorCurrency"] = requestorCurrency;
    }
    if (destinationCurrency) {
        params[@"destinationCurrency"] = destinationCurrency;
    }
    if (knpCode) {
        params[@"knpCode"] = knpCode;
    }
    if (knpName) {
        params[@"knpName"] = knpName;
    }
    
    params[@"direction"] = (direction ? @"true" : @"false");
    
    params[@"save"] = save ? @"true" : @"false";
    if (alias && save) {
        params[@"alias"] = alias;
    }

    return [self POST:@"transfers/json/check" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

+ (NSURLSessionDataTask *)initTransfer:(TransferType)transferType
                           requestorId:(NSNumber *)requestorId
                       requestorAmount:(NSNumber *)requestorAmount
                         destinationId:(NSNumber *)destinationId
                         requestorType:(NSString *)requestorType
                     requestorCurrency:(NSString *)requestorCurrency
                       destinationType:(NSString *)destinationType
                   destinationCurrency:(NSString *)destinationCurrency
                               knpCode:(NSString *)knpCode
                               knpName:(NSString *)knpName
                             direction:(BOOL)direction
                                  save:(BOOL)save
                                 alias:(NSString *)alias
                            clientDesc:(NSString *)clientDesc
                               success:(success)success
                               failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    NSString *transferTypeToString = [self transferTypeToString:transferType];
    params[@"type"] = transferTypeToString;
    
    if (requestorId) {
        params[@"requestorId"] = requestorId;
    }
    if (requestorAmount) {
        if (direction) {
            params[@"requestorAmount"] = requestorAmount;
        } else {
            params[@"destinationAmount"] = requestorAmount;
        }
    } else {
        if (direction) {
            params[@"requestorAmount"] = @(0);
        } else {
            params[@"destinationAmount"] = @(0);
        }
    }
    if (destinationId) {
        params[@"destinationId"] = destinationId;
    }
    if (requestorType) {
        params[@"requestorType"] = requestorType;
    }
    if (destinationType) {
        params[@"destinationType"] = destinationType;
    }
    if (requestorType) {
        params[@"requestorCurrency"] = requestorCurrency;
    }
    if (destinationType) {
        params[@"destinationCurrency"] = destinationCurrency;
    }
    if (knpCode) {
        params[@"knpCode"] = knpCode;
    }
    if (knpName) {
        params[@"knpName"] = knpName;
    }
    if (clientDesc) {
        params[@"clientDesc"] = clientDesc;
    }
    
    params[@"direction"] = (direction ? @"true" : @"false");
    
    params[@"save"] = save ? @"true" : @"false";
    if (alias && save) {
        params[@"alias"] = alias;
    }
    
    return [self POST:@"transfers/json/init" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

+ (NSURLSessionDataTask *)getTransfer:(NSNumber *)servicelogId
                              success:(success)success
                              failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"servicelogId"] = servicelogId;

    return [self POST:@"transfers/get" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}



+ (NSURLSessionDataTask *)runTransfer:(NSNumber *)servicelogId
                              success:(success)success
                              failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"servicelogId"] = servicelogId;
    
    return [self POST:@"transfers/run" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

+ (NSURLSessionDataTask *)sendSMS:(NSNumber *)servicelogId
                          success:(success)success
                          failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"servicelogId"] = servicelogId;
    
    return [self POST:@"transfers/send_sms" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

+ (NSURLSessionDataTask *)checkSMS:(NSString *)code
                      servicelogId:(NSNumber *)servicelogId
                           success:(success)success
                           failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"code"] = code;
    params[@"servicelogId"] = servicelogId;
    
    return [self POST:@"transfers/check_sms" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

#pragma mark - transfer run avia

+(NSURLSessionDataTask *)runTransferAvia:(NSNumber *)servicelogId success:(success)success failure:(failure)failure{
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"servicelogId"] = servicelogId;
    
    return [self POST:@"transfers/run/avia" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

#pragma mark - P2P

+ (NSURLSessionTask *)initP2PWithSuccess:(success)success
                                 failure:(failure)failure {
    NSDictionary *p = @{@"type" : @"P2P_TRANSFERS"};
    return [self POST:@"transfers/json/init/cnp" parameters:p success:success failure:failure];
}

+ (NSURLSessionTask *)initPayboxWithSuccess:(success)success failure:(failure)failure {
    NSDictionary *p = @{@"type" : @"P2P_TRANSFERS"};
    return [self POST:@"transfers/json/init/paybox" parameters:p success:success failure:failure];
}

#pragma mark - from external card to owen account

+ (NSURLSessionTask *)checkTransferButtonDestinationId:(NSNumber *)destinationId
                                       destinationType:(NSString *)destinationType
                                     destinationAmount:(NSNumber *)destinationAmount
                                   destinationCurrency:(NSString *)destinationCurrency
                                                succes:(success)success
                                               failure:(failure)failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"destinationId"] = destinationId;
    params[@"destinationType"] = destinationType;
    params[@"destinationAmount"] = destinationAmount;
    params[@"destinationCurrency"] = destinationCurrency;
    return [self POST:@"transfers/check/button" parameters:params showErrorAlert:NO success:success failure:failure];
}

+ (NSURLSessionTask *)initPayboxButtonDestinationId:(NSNumber *)destinationId
                                    destinationType:(NSString *)destinationType
                                  destinationAmount:(NSNumber *)destinationAmount
                                destinationCurrency:(NSString *)destinationCurrency
                                             succes:(success)success
                                            failure:(failure)failure {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"destinationId"] = destinationId;
    params[@"destinationType"] = destinationType;
    params[@"destinationAmount"] = destinationAmount;
    params[@"destinationCurrency"] = destinationCurrency;
    return [self POST:@"transfers/init/paybox/button" parameters:params showErrorAlert:NO success:success failure:failure];
}

#pragma mark - Templates

+ (NSURLSessionTask *)getTemplatesWithSuccess:(void (^)(NSArray *transferTemplates))success
                                      failure:(failure)failure {
    return [self POST:@"transfers/templates" parameters:nil success:^(id response) {
        NSMutableArray *transferTemplates = [NSMutableArray new];
        
        for (NSDictionary *ttData in response[@"allTransfers"]) {
            TransferTemplate *tt = [TransferTemplate instanceFromDictionary:ttData];
            [transferTemplates addObject:tt];
        }
        
        success(transferTemplates);
    } failure:failure];
}

+ (NSURLSessionTask *)setTemplatesAlias:(NSString *)alias
                             templateId:(NSNumber *)templateId
                                success:(success)success
                                failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"alias"] = alias;
    params[@"templateId"] = templateId;
    
    return [self POST:@"transfers/template/alias" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

+ (NSURLSessionTask *)setTemplatesDelete:(BOOL)del
                              templateId:(NSNumber *)templateId
                                 success:(success)success
                                 failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"delete"] = del ? @"true" : @"false";
    params[@"templateId"] = templateId;
    
    return [self POST:@"transfers/template/delete" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

#pragma mark - New transferApi TEST!!!!!!!

+ (void)isUpdating:(success)success failure:(failure)failure {
    if (!IS_API_URL_TEST)[self POST:@"transfers_ref/getAccounts" parameters:nil success:success failure:failure];
}

+ (NSURLSessionTask *)getKnpWithParams:(NSDictionary *)params
                               Success:(success)success
                               failure:(failure)failure
{
    return [self POST:@"transfers_ref/getKnp" parameters:params
              success:^(id response) {
                  Knp* knpObject = [Knp instanceFromDictionary:response];
                  success(knpObject);
              } failure:failure];
}


+ (NSURLSessionTask *)getTypesWithSucces:(success)success
                                 failure:(failure)failure
{
    NSMutableArray *transfers = [NSMutableArray array];
    
    return [self GET:@"transfers_ref/getTypes" parameters:nil success:^(id response) {
        for(NSDictionary *typeItem in response){
            NewTransferType *type = [NewTransferType instanceFromDictionary:typeItem];
            [transfers addObject:type];
        }
        if(transfers.count > 0) success(transfers);
    } failure:failure];
}

+ (NSURLSessionTask *)getAccountsWithParametrs:(NSDictionary *)params
                                     success:(void (^)(OperationAccounts *))onSuccess
                                     failure:(failure)failure
{

    
    return [self POST:@"transfers_ref/getAccounts" parameters:params showErrorAlert:YES success:^(id response) {
        if(onSuccess){
        OperationAccounts *operationAccounts = [OperationAccounts instanceFromDictionary:response];
        onSuccess(operationAccounts);
        }
    } failure:failure];
}

+ (NSURLSessionDataTask *)newCheckTransfer:(TransferType)transferType
                                    params:(NSMutableDictionary *) params
                               requestorId:(NSNumber *)requestorId
                                    amount:(NSNumber *)amount
                             destinationId:(NSNumber *)destinationId
                             requestorType:(NSString *)requestorType
                         requestorCurrency:(NSString *)requestorCurrency
                           destinationType:(NSString *)destinationType
                       destinationCurrency:(NSString *)destinationCurrency
                                   knpCode:(NSString *)knpCode
                                   knpName:(NSString *)knpName
                                 direction:(BOOL)direction
                                      save:(BOOL)save
                                     alias:(NSString *)alias
                                   success:(void (^)(NewCheckAndInitTransferResponse *))onSuccess
                                   failure:(failure)failure {
    //NSMutableDictionary *params = [NSMutableDictionary new];
    
   // NSString *transferTypeToString = [self transferTypeToString:transferType];
   // params[@"type"] = @"I";
   // params[@"dstOwner"] = @"S";
    //params[@"dir"] = @"true";
    
    if (requestorId) {
        params[@"reqId"] = requestorId;
    }
    
    //params[@"reqType"] = requestorType;
    
    if (amount) {
        if (direction) {
            params[@"reqAmount"] = amount;
        } else {
            params[@"dstAmount"] = amount;
        }
    } else {
        if (direction) {
            params[@"reqAmount"] = @(0);
        } else {
            params[@"dstAmount"] = @(0);
        }
    }
    if (destinationId) {
        params[@"dstId"] = destinationId;
    }
    if (requestorType) {
        params[@"reqType"] = requestorType;
    }
    if (destinationType) {
        params[@"dstType"] = destinationType;
    }
    if (requestorCurrency) {
        params[@"reqCurrency"] = requestorCurrency;
    }
    if (destinationCurrency) {
        params[@"dstCurrency"] = destinationCurrency;
    }
    if (knpCode) {
        params[@"knp"] = knpCode;
    }
    if (knpName) {
        params[@"knpName"] = knpName;
    }
    
    params[@"dir"] = (direction ? @"true" : @"false");
    
    params[@"save"] = save ? @"true" : @"false";
    if (alias && save) {
        params[@"alias"] = alias;
    }
    
    return [self POST:@"transfers_ref/check" parameters:params success:^(id response) {
        NewCheckAndInitTransferResponse *checkTransferResponse = [NewCheckAndInitTransferResponse instanceFromDictionary:response];
        onSuccess(checkTransferResponse);
    } failure:failure];
}

+ (NSURLSessionDataTask *)newInitTransferWithParams:(NSMutableDictionary *)params
                                        requestorId:(NSNumber *)requestorId
                                    requsetorAmount:(NSNumber *)reqAmount
                                  destinationAmount:(NSNumber *)desAmount
                                      destinationId:(NSNumber *)destinationId
                                      requestorType:(NSString *)requestorType
                                  requestorCurrency:(NSString *)requestorCurrency
                                    destinationType:(NSString *)destinationType
                                destinationCurrency:(NSString *)destinationCurrency
                                            knpCode:(NSString *)knpCode
                                            knpName:(NSString *)knpName
                                          direction:(BOOL)direction
                                               save:(BOOL)save
                                              alias:(NSString *)alias
                                            success:(void (^)(NewCheckAndInitTransferResponse *))onSuccess
                                            failure:(failure)failure {
    
    params[@"reqAmount"] = reqAmount;
    params[@"dstAmount"] = desAmount;
    
   /* if (requestorId) {
        params[@"reqId"] = requestorId;
    }
    if (reqAmount)
    {
            params[@"reqAmount"] = reqAmount;
        
    } else if(desAmount)
        {
            
            params[@"dstAmount"] = desAmount;
        
        }else{*/
        
           /* if (direction) {
           
                params[@"reqAmount"] = @(0);
       
            } else {
            
                params[@"dstAmount"] = @(0);
        }*/
  //  }
    if (destinationId) {
        params[@"dstId"] = destinationId;
    }
    if (requestorType) {
        params[@"reqType"] = requestorType;
    }
    if (destinationType) {
        params[@"dstType"] = destinationType;
    }
    if (requestorType) {
        params[@"reqCurrency"] = requestorCurrency;
    }
    if (destinationType) {
        params[@"dstCurrency"] = destinationCurrency;
    }
    if (knpCode) {
        params[@"knp"] = knpCode;
    }
    
    params[@"direction"] = (direction ? @"true" : @"false");
    
    params[@"save"] = save ? @"true" : @"false";
    if (alias && save) {
        params[@"alias"] = alias;
    }
    
    
    return [self POST:@"transfers_ref/init" parameters:params success:^(id response) {
        NewCheckAndInitTransferResponse *checkTransferResponse = [NewCheckAndInitTransferResponse instanceFromDictionary:response];
        onSuccess(checkTransferResponse);
        NSLog(@"Response from api: %@", response);
    } failure:failure];
}

+ (NSURLSessionDataTask *)newCheckSMS:(NSString *)code
                      servicelogId:(NSNumber *)servicelogId
                           success:(success)success
                           failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"code"] = code;
    params[@"servicelog"] = servicelogId;
    
    return [self POST:@"transfers_ref/sms/check" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

+ (NSURLSessionDataTask *)newSendSMS:(NSNumber *)servicelogId
                          success:(success)success
                          failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"servicelog"] = servicelogId;
    
    return [self POST:@"transfers_ref/sms/send" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}



+ (NSURLSessionDataTask *)newRunTransfer:(NSNumber *)servicelogId
                              success:(success)success
                              failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"servicelog"] = servicelogId;
    
    return [self POST:@"transfers_ref/run" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

+ (NSURLSessionDataTask *)sendEncryptedCVV:(NSString *)cvv
                             withAccountId:(NSNumber *)cardId
                                   success:(success)success
                                   failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"cvv"] = cvv;
    params[@"id"]  = cardId;
    
    return [self POST:@"transfers_ref/card/cvv" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}

+ (NSURLSessionDataTask *)getTransferResultFromWebWithServiceLogID:(NSNumber *)serviceLogId
                                                           success:(success)success
                                                           failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"servicelog"] = serviceLogId;
    
    return [self POST:@"transfers_ref/get" parameters:params success:^(id response) {
        success(response);
    } failure:failure];
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Helpers

+ (NSString *)transferTypeToString:(TransferType)transferType {
    NSString *result = nil;
    
    switch(transferType) {
        case MY_TRANSFERS:
            result = @"MY_TRANSFERS";
            break;
        case MY_CONVERT:
            result = @"MY_CONVERT";
            break;
        case TRANSFERS_TO_OTHER:
            result = @"TRANSFERS_TO_OTHER";
            break;
        case INTERBANK_TRANSFERS:
            result = @"INTERBANK_TRANSFERS";
            break;
        case P2P_TRANSFERS:
            result = @"P2P_TRANSFERS";
            break;
        case INTERNATIONAL_TRANSFERS:
            result = @"INTERNATIONAL_TRANSFERS";
            break;
        case MY_BONUS_COMPENSATION:
            result = @"MY_BONUS_COMPENSATION";
            break;
        default:
            [NSException raise:NSGenericException format:@"Unexpected TransferType."];
    }
    
    return result;
}

+ (TransferType)transferTypeForString:(NSString *)transferTypeString {
    if ([transferTypeString isEqualToString:@"MY_CONVERT"]) {
        return MY_CONVERT;
    } else if ([transferTypeString isEqualToString:@"TRANSFERS_TO_OTHER"]) {
        return TRANSFERS_TO_OTHER;
    } else if ([transferTypeString isEqualToString:@"INTERBANK_TRANSFERS"]) {
        return INTERBANK_TRANSFERS;
    } else if ([transferTypeString isEqualToString:@"P2P_TRANSFERS"]) {
        return P2P_TRANSFERS;
    } else if ([transferTypeString isEqualToString:@"INTERNATIONAL_TRANSFERS"]) {
        return INTERNATIONAL_TRANSFERS;
    } else if ([transferTypeString isEqualToString:@"MY_BONUS_COMPENSATION"]) {
        return MY_BONUS_COMPENSATION;
    }
    
    return MY_TRANSFERS;
}

+ (NSString *)transferTypeToDescriptionString:(TransferType)transferType {
    NSString *result = nil;
    
    switch(transferType) {
        case MY_TRANSFERS:
            result = @"Между своими счетами";
            break;
        case MY_CONVERT:
            result = @"Конвертация валют";
            break;
        case TRANSFERS_TO_OTHER:
            result = @"В пользу третьих лиц";
            break;
        case INTERBANK_TRANSFERS:
            result = @"Межбанковский перевод";
            break;
        case P2P_TRANSFERS:
            result = @"С карты на карту";
            break;
        case INTERNATIONAL_TRANSFERS:
            result = @"Международный перевод";
            break;
        case MY_BONUS_COMPENSATION:
            result = @"MY_BONUS_COMPENSATION";
            break;
        default:
            [NSException raise:NSGenericException format:@"Unexpected TransferType."];
    }
    
    return result;
}

+ (void)cancelAllOperations {
    [[[self sharedManager] operationQueue] cancelAllOperations];
}


@end
