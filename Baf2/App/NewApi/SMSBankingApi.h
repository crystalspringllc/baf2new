//
// Created by Askar Mustafin on 6/16/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiClient.h"


@interface SMSBankingApi : ApiClient

#pragma mark -
#pragma mark - Sms banking

+ (NSURLSessionDataTask *)getSmsbankingStatusWithCardId:(NSNumber *)cardId
                                                success:(success)success
                                                failure:(failure)failure;

+ (NSURLSessionDataTask *)initSmsbankingWithCardId:(NSNumber *)cardId
                                             phone:(NSString *)phone
                                          isEnable:(BOOL)isEnable
                                           success:(success)success
                                           failure:(failure)failure;

+ (NSURLSessionDataTask *)checkSmsBankingSmsWithCode:(NSNumber *)code
                                        serviceLogId:(NSNumber *)serviceLogId
                                             success:(success)success
                                             failure:(failure)failure;

+ (NSURLSessionDataTask *)sendSmsAgainWithServiceLogId:(NSNumber *)serviceLogId
                                               success:(success)success
                                               failure:(failure)failure;

#pragma mark -
#pragma mark - change number for sms banking

+ (NSURLSessionDataTask *)initPhoneChangeWithCardId:(NSNumber *)cardId
                                           newPhone:(NSString *)phone
                                            success:(success)success
                                            failure:(failure)failure;

+ (NSURLSessionDataTask *)sendSmsWithServiceLogId:(NSNumber *)serviceLogId
                                          success:(success)success
                                          failure:(failure)failure;

+ (NSURLSessionDataTask *)runPhoneChangeWithServiceLogId:(NSNumber *)serviceLogId
                                                    code:(NSString *)code
                                                password:(NSString *)password
                                                 success:(success)success
                                                 failure:(failure)failure;

@end