//
//  OperationsApi.m
//  Baf2
//
//  Created by Shyngys Kassymov on 21.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "OperationsApi.h"

@implementation OperationsApi

+ (NSURLSessionDataTask *)getLastOperationsWithFilter:(OperationHistoryFilter *)filter
                                         success:(LastOperationsResponse)onSuccess
                                         failure:(failure)failure {
    NSMutableDictionary *params = [filter toJSON];



    return [self POST:@"operations/get_last_operations" parameters:[params mutableCopy] success:^(id response) {
        if (onSuccess) {
            NSMutableArray *operationStatusList = [NSMutableArray new];
            if (response[@"operationStatusList"]) {
                NSArray *list = response[@"operationStatusList"];
                [operationStatusList addObjectsFromArray:list];
            }
            
            NSMutableArray *operationList = [NSMutableArray new];
            if (response[@"operationList"]) {
                NSArray *list = [OperationHistory instancesFromArray:response[@"operationList"]];
                [operationList addObjectsFromArray:list];
            }
            
            NSString *startDate = response[@"startDate"];
            NSString *endDate = response[@"endDate"];
            
            onSuccess(operationStatusList, operationList, startDate, endDate);
        }
    } failure:failure];
}

+ (NSURLSessionDataTask *)getPaymentsVersion:(NSNumber *)paymentServiceId
                                     success:(GetServiceVersionResponse)paymentService
                                     failure:(failure)failure {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    params[@"serviceLogId"] = paymentServiceId;
    NSLog(@"serviceLogId %@", paymentServiceId);
    return [self GET:@"v1/payments/get_version" parameters:[params mutableCopy] success:^(id response) {
        NSLog(@"Response from service ver.: %@", response);
        if(paymentService)
        {
            paymentService([NSNumber numberWithInt:1]);
        }
       /* NSNumber *version = respons[@"version"];
        switch (version) {
            case 1:
                paymentService = PaymentServiceNew;
                break;
            case 2:
                paymentService = PaymentServiceOld;
                break;
            default:
                break;
        }*/
    } failure:failure];
}


+ (NSURLSessionDataTask *)getInfoForDetailedOrdering:(NSNumber *)orderId
                                             success:(LastOperationDetailResponse)onSuccess
                                             failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    if (orderId) params[@"orderId"] = orderId;
    
    return [self POST:@"operations/get_info_for_detailed_ordering" parameters:params success:^(id response) {
        if (onSuccess) {
            OperationHistoryDetails *details = [OperationHistoryDetails instanceFromDictionary:response];
            
            onSuccess(details);
        }
    } failure:failure];
}

@end
