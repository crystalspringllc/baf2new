//
// Created by Askar Mustafin on 5/30/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "ApiClient.h"
#import "Connection.h"
#import "NSString+HTML.h"
#import "AppUtils.h"


@implementation ApiClient {}

+ (AFHTTPSessionManager *)sharedManager {
    static AFHTTPSessionManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [AFHTTPSessionManager manager];
        sharedManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        sharedManager.responseSerializer.acceptableContentTypes =
                [sharedManager.responseSerializer.acceptableContentTypes
                        setByAddingObject:@"application/x-www-form-urlencoded"];

//        AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
//        NSString *certPath_bank = [[NSBundle mainBundle] pathForResource:@"365.bankastana.kz" ofType:@"cer"];//*.myth.kz
////        NSString *certPath_root = [[NSBundle mainBundle] pathForResource:@"COMODO High-Assurance Secure Server CA" ofType:@"cer"];
////        NSString *certPath_validation = [[NSBundle mainBundle] pathForResource:@"AddTrust External CA Root" ofType:@"cer"];
//        securityPolicy.pinnedCertificates = [[NSSet alloc] initWithArray:@[[NSData dataWithContentsOfFile:certPath_bank]]];
////                [NSData dataWithContentsOfFile:certPath_root],
////                [NSData dataWithContentsOfFile:certPath_validation]]];
//        sharedManager.securityPolicy = securityPolicy;

    });

    return sharedManager;
}

+ (NSURLSessionDataTask *)GET:(NSString *)path
                   parameters:(NSDictionary *)parameters
                      success:(success)success
                      failure:(failure)failure {
    return [self GET:path parameters:parameters showErrorAlert:YES success:success failure:failure];
}

+ (NSURLSessionDataTask *)GET:(NSString *)path
                   parameters:(NSDictionary *)parameters
               showErrorAlert:(BOOL)showErrorAlert
                      success:(success)success
                      failure:(failure)failure {

#if !(WATCH_OS)
    if (![Connection isReachableM]) {
        return nil;
    }
#endif

    NSString *API_BASE_URL;
    NSString *URLString;
    
    API_BASE_URL = [self BASE_URL];
    
    URLString = [API_BASE_URL stringByAppendingString:path];
    
    if (!parameters) parameters = [NSMutableDictionary dictionary];
    NSMutableDictionary *fullParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    
    [self setSessionHeader];

    if (!fullParameters[@"session"] && [User sharedInstance].sessionID) {
        fullParameters[@"session"] = [User sharedInstance].sessionID;
        [[self sharedManager].requestSerializer setValue:[User sharedInstance].sessionID forHTTPHeaderField:@"session"];
        [[self sharedManager].requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    }
   
    
    NSURLSessionDataTask *requestOperation = [[self sharedManager]
            GET:URLString
     parameters:fullParameters
       progress:nil
        success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

            if (IS_API_RESPONSE_LOG) {
                NSLog(@"------------------------------------------------------------");
                NSLog(@"NEW API METHOD: %@", URLString);
                NSLog(@" ");
                NSLog(@"NEW API PARAMETERS (%@) : %@", path, [fullParameters JSONRepresentation]);
                NSLog(@" ");
                NSLog(@"NEW API RESPONSE (%@) : %@", path, [responseObject JSONRepresentationPretyPrinted:YES]);
                NSLog(@"------------------------------------------------------------");
            }
            if (showErrorAlert) {
                [self handleResponse:responseObject success:success failure:failure];
            } else {
                NSString *status = responseObject[@"status"];
                if ([status isEqualToString:@"error"]) {
                    if (failure) {
                        NSString *errorCode = responseObject[@"error_code"];
                        NSString *message = responseObject[@"message"];
                        if (responseObject[@"error_description"]) {
                            message = responseObject[@"error_description"];
                        }
                        failure(errorCode, message);
                    }
                } else if ([status isEqualToString:@"success"]) {
                    id data = responseObject[@"data"];
                    if (success) {
                        success(data);
                    }
                }
            }

        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

                NSString *errorCode = [NSString stringWithFormat:@"%ld", (long)error.code];
#if !(WATCH_OS)
                [UIHelper showAlertWithMessageTitle:@"" message:NSLocalizedString(@"not_possible_to_connect", nil)];
#endif
                if (IS_API_RESPONSE_LOG) {
                    NSLog(@"------------------------------------------------------------");
                    NSLog(@"NEW API METHOD: %@", URLString);
                    NSLog(@" ");
                    NSLog(@"NEW API PARAMETERS (%@) : %@", path, [fullParameters JSONRepresentation]);
                    NSLog(@" ");
                    NSLog(@"NEW API ERROR (%@) : %@", path, error.userInfo);
                    NSLog(@"------------------------------------------------------------");
                }

                if (failure) {
                    failure(errorCode, error.description);
                }

            }];
    
    return requestOperation;
}

+ (NSURLSessionDataTask *)POST:(NSString *)path
                    parameters:(NSDictionary *)parameters
                       success:(success)success
                       failure:(failure)failure {
    
    return [self POST:path parameters:parameters showErrorAlert:YES success:success failure:failure];
}

+ (NSURLSessionDataTask *)POST:(NSString *)path
                    parameters:(NSDictionary *)parameters
                showErrorAlert:(BOOL)showErrorAlert
                       success:(success)success
                       failure:(failure)failure {

#if !(WATCH_OS)
    if (![Connection isReachableM]) {
        return nil;
    }
#endif
    
    NSString *API_BASE_URL;
    NSString *URLString;
    
    API_BASE_URL = [self BASE_URL];
    
    URLString = [API_BASE_URL stringByAppendingString:path];
    
    if (!parameters) parameters = [NSMutableDictionary dictionary];
    NSMutableDictionary *fullParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    
    [self setSessionHeader];
    NSLog(@"Sessions ID %@", [User sharedInstance].sessionID);
    if (!fullParameters[@"session"] && [User sharedInstance].sessionID) {
        fullParameters[@"session"] = [User sharedInstance].sessionID;
        // add session header
        [[self sharedManager].requestSerializer setValue:[User sharedInstance].sessionID forHTTPHeaderField:@"session"];
    }

    fullParameters[@"portalId"] = @"2";
    fullParameters[@"brandId"] = @"2";
    fullParameters[@"clientInfo"] = [AppUtils deviceInfoJsonString];


    NSURLSessionDataTask *requestOperation = [[self sharedManager]
            POST:URLString
      parameters:fullParameters
        progress:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {

             if (IS_API_RESPONSE_LOG) {

                 NSLog(@"--------------------------------------------------------");
                 NSLog(@"NEW API METHOD: %@", URLString);
                 NSLog(@" ");
                 NSLog(@"NEW API PARAMETERS (%@) : %@", path, [fullParameters JSONRepresentation]);
                 NSLog(@" ");
                 NSLog(@"NEW API RESPONSE (%@) : %@", path, [responseObject JSONRepresentation]);
                 NSLog(@"--------------------------------------------------------");
             }

             if (showErrorAlert) {
                 [self handleResponse:responseObject success:success failure:failure];
             } else {
               
                 NSString *status = responseObject[@"status"];
                 if ([status isEqualToString:@"error"]) {
                     if (failure) {
                         NSString *errorCode = responseObject[@"error_code"];
                         NSString *message = responseObject[@"message"];
                         if (responseObject[@"error_description"]) {
                             message = responseObject[@"error_description"];
                         }
                         failure(errorCode, message);
                     }
                 } else if ([status isEqualToString:@"success"]) {
                     id data = responseObject[@"data"];
                     if (success) {
                         success(data);
                     }
                 }
             }
         } failure:^(NSURLSessionDataTask *task, NSError *error) {
                NSString *errorCode = [NSString stringWithFormat:@"%ld", (long)error.code];
#if !(WATCH_OS)
                [UIHelper showAlertWithMessageTitle:@"" message:NSLocalizedString(@"not_possible_to_connect", nil)];
#endif

                if (IS_API_RESPONSE_LOG) {
                    NSLog(@"------------------------------------------------------------");
                    NSLog(@"NEW API METHOD: %@", URLString);
                    NSLog(@" ");
                    NSLog(@"NEW API PARAMETERS (%@) : %@", path, [fullParameters JSONRepresentation]);
                    NSLog(@" ");
                    NSLog(@"NEW API ERROR (%@) : %@", path, error.userInfo);
                    NSLog(@"------------------------------------------------------------");
                }

                if (failure) {
                    failure(errorCode, error.description);
                }
            }];
    return requestOperation;
}

#pragma mark - handling response

+ (void)handleResponse:(id)responseObject success:(success)success failure:(failure)failure {
    if (!responseObject || responseObject == (id)[NSNull null]) {
        if (failure) {
            failure(@"", 0);
        }
        return;
    }

    NSString *status = responseObject[@"status"];

    //Error
    if ([status isEqualToString:@"error"]) {

        NSString *errorCode = responseObject[@"error_code"];
        NSString *errorDescription = [responseObject[@"error_description"] stringByConvertingHTMLToPlainText];
        
        // !!!: temp till backenders didn't add description for errors
        if (!errorDescription || errorDescription.length == 0) {
            errorDescription = errorCode;
        }
        
        // Uncomment if errorDescription is unreadable for users
        NSString *message = responseObject[@"message"];



        if (message) {
#if !(WATCH_OS)
            [UIHelper showAlertWithMessage:message];
#endif
            if (failure) {
                failure(errorCode, message);
            }
            
#if !(WATCH_OS)
            if ([errorCode isEqualToString:@"SESSION_EXPIRED_OR_CLOSED"]) {
                [((AppDelegate *)[UIApplication sharedApplication].delegate) logoutUser];
            }
#endif
            
            return;
        }
        
#if !(WATCH_OS)
        [UIHelper showAlertWithMessage:errorDescription];
#endif
        if (failure) {
            failure(errorCode, errorDescription);
        }
        
#if !(WATCH_OS)
        if ([errorCode isEqualToString:@"SESSION_EXPIRED_OR_CLOSED"]) {
            [self cancelAllOperations];
            [((AppDelegate *)[UIApplication sharedApplication].delegate) logoutUser];
        }
#endif
    }
    //Success
    else if ([status isEqualToString:@"success"]) {

        id data = responseObject[@"data"];

        if (success) {
            success(data);
        }
    }
}

#pragma mark - set user session in http header

+ (void)setSessionHeader {
    if ([User sharedInstance].sessionID) {
        [self.sharedManager.requestSerializer setValue:[User sharedInstance].sessionID forHTTPHeaderField:@"session"];
    }
}

#pragma mark - cancel request

+ (void)cancelAllOperations {
    [[[self sharedManager] operationQueue] cancelAllOperations];
}

#pragma mark -

+ (NSString *)BASE_URL {
    if (IS_API_URL_TEST) {
        return kTestLocalBaseUrl;
    } else {
        return kProdBaseURL;
    }
}

@end
