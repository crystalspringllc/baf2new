//
//  LocationsApi.m
//  Baf2
//
//  Created by Shyngys Kassymov on 07.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "LocationsApi.h"
#import "Location.h"

@implementation LocationsApi

+ (NSURLSessionDataTask *)getCountriesWithSuccess:(void (^)(NSArray *countries))onSuccess failure:(failure)failure {
    return [self POST:@"locations/get_countries" parameters:nil success:^(id response) {
        if (onSuccess) {
            NSMutableArray *countries = [NSMutableArray new];
            
            NSArray *locationList = response[@"locations"];
            for (NSDictionary *locationData in locationList) {
                Location *location = [Location instanceFromDictionary:locationData];
                [countries addObject:location];
            }
            
            onSuccess(countries);
        }
    } failure:failure];
}

+ (NSURLSessionDataTask *)getCities:(NSString *)countryId success:(void (^)(NSArray *cities))onSuccess failure:(failure)failure {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"countryId"] = countryId;
    
    return [self POST:@"locations/get_cities" parameters:params success:^(id response) {
        if (onSuccess) {
            NSMutableArray *cities = [NSMutableArray new];
            
            NSArray *locationList = response[@"locations"];
            for (NSDictionary *locationData in locationList) {
                Location *location = [Location instanceFromDictionary:locationData];
                [cities addObject:location];
            }
            
            onSuccess(cities);
        }
    } failure:failure];
}

@end
