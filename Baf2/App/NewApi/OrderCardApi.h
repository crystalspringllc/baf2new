//
// Created by Askar Mustafin on 1/6/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiClient.h"


@interface OrderCardApi : ApiClient

+ (void)getProducts:(success)success failure:(failure)failure;

+ (void)initWithIIN:(NSString *)iin
              phone:(NSString *)phone
           cityCode:(NSString *)cityCode
           codeWord:(NSString *)codeWord
        productCode:(NSString *)productCode
    virtualCurrency:(NSString *)virtualCurrency
            isMulti:(BOOL)isMulti
           isCredit:(BOOL)isCredit
          isVirtual:(BOOL)isVirtual
         isResident:(BOOL)isResident
        creditLimit:(NSNumber *)creditLimit
             income:(NSNumber *)income
            docType:(NSString *)docType
          docNumber:(NSString *)docNumber
       docIssueDate:(NSString *)docIssueDate
       categoryCode:(NSString *)categoryCode
           isAgreed:(BOOL)isAgreed
     childBirthDate:(NSString *)childBirthDate
           childIin:(NSString *)childIin
      childLastname:(NSString *)childLastname
     childFirstname:(NSString *)childFirstname
    childMiddlename:(NSString *)childMiddlename
    isChildResident:(BOOL)isisChildResident
            success:(success)success
            failure:(failure)failure;


+ (void)runWithServiceLogId:(NSNumber *)serviceLogId success:(success)success failure:(failure)failure;

+ (void)getClientInfoByIIN:(NSString *)iin success:(success)success failure:(failure)failure;

@end