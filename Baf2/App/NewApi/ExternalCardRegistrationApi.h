//
// Created by Askar Mustafin on 6/1/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiClient.h"


@interface ExternalCardRegistrationApi : ApiClient


/**
 * @brief getting all card type
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)cardTypesWithSuccess:(success)success failure:(failure)failure;

/**
 * @brief getting banks available to register their plastic card
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)banksWithSuccess:(success)success failure:(failure)failure;

/**
 * @brief - register external card
 * @param cardType
 * @param bank
 * @param processing
 * @param cardName
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)registerExtCardWithCardType:(NSString *)cardType
                               bank:(NSString *)bank
                         processing:(NSString *)processing
                           cardName:(NSString *)cardName
                            success:(success)success
                            failure:(failure)failure;

/**
 * @brief geting epay link
 * @param cardType
 * @param bank
 * @param cardId
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)getEpayRegCardLinkParamsWithCardType:(NSString *)cardType
                                        bank:(NSString *)bank
                                      cardId:(NSNumber *)cardId
                                     success:(success)success
                                     failure:(failure)failure;

/**
 * @brief save card
 * @param uuid
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)saveCardRegWithUuid:(NSString *)uuid
                    success:(success)success
                    failure:(failure)failure;

/**
 * @brief register card by uuid
 * @param uuid
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)registerCardByUuid:(NSString *)uuid
                   success:(success)success
                   failure:(failure)failure;


//+ (void)getCardRegUuidByOrderWithCardId:(NSNumber *)orderId
//                                success:(success)success
//                                failure:(failure)failure;

/**
 * @brief
 * @param cardId
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)actionOnCardRegFailureWithCardId:(NSNumber *)cardId
                                 success:(success)success
                                 failure:(failure)failure;

/**
 * @brief update epay card approve state
 * @param cardId
 * @param approve
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)updateEpayCardApproveStateWithCardId:(NSNumber *)cardId
                                     approve:(BOOL)approve
                                     success:(success)success
                                     failure:(failure)failure;

/**
 * @brief Update external bank card info
 * @param cardAlias - name of card entered by user
 * @param processing - hardcoded with from #define kProcessingBankKkb @"epay_kkb" from Constants.h
 * @param maincard - Bool like favorites
 * @param cardId - ExtCard.accountId
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 */
+ (void)updateEpayCardInfoWithCardAlias:(NSString *)cardAlias
                          processing:(NSString *)processing
                            mainCard:(BOOL)maincard
                              cardId:(NSNumber *)cardId
                             success:(success)success
                             failure:(failure)failure;

/**
 * @brief Remove external bank card
 * @param cardId - ExtCard.accountId
 * @param success - block to handle success result object
 * @param failure - block to handle failure code nad message
 * @return NSURLSessionDataTask
 */
+ (void)deleteCardWithCardId:(NSNumber *)cardId
                     success:(success)success
                     failure:(failure)failure;

@end