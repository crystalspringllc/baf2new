//
//  CashBackApi.m
//  Baf2
//
//  Created by Dulatheo on 30.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "CashBackApi.h"

@implementation CashBackApi
+(NSURLSessionDataTask *)getCashBackDisclaimer:(NSNumber *)cardId success:(success)success failure:(failure)failure{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"cardId"] = cardId;
    return [self POST:@"accounts/card/cashback/disclaimer" parameters:params success:success failure:failure];
}

+(NSURLSessionDataTask *)changeCashBackInit:(NSNumber *)cardId success:(success)success failure:(failure)failure{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"cardId"] = cardId;
    return [self POST:@"accounts/card/cashback/change/init" parameters:params success:success failure:failure];
}

+(NSURLSessionDataTask *)changeCashBackSmsResend:(NSNumber *)serviceLogId success:(success)success failure:(failure)failure{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"serviceLogId"] = serviceLogId;
    return [self POST:@"accounts/card/cashback/change/sms/resend" parameters:params success:success failure:failure];
}

+(NSURLSessionDataTask *)changeCashBackSmsConfirm:(NSNumber *)serviceLogId code:(NSNumber *)code success:(success)success failure:(failure)failure{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"serviceLogId"] = serviceLogId;
    params[@"code"] = code;
    return [self POST:@"accounts/card/cashback/change/sms/confirm" parameters:params success:success failure:failure];
}

+(NSURLSessionDataTask *)changeCashBackRun:(NSNumber *)serviceLogId success:(success)success failure:(failure)failure{
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"serviceLogId"] = serviceLogId;
    return [self POST:@"accounts/card/cashback/change/run" parameters:params success:success failure:failure];
}
@end
