//
//  NewTransfersApi.h
//  Baf2
//
//  Created by Shyngys Kassymov on 31.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ApiClient.h"
#import "Knp.h"
#import "NewTransferType.h"
#import "OperationAccounts.h"
#import "NewCheckAndInitTransferResponse.h"


typedef enum TransferType:NSInteger {
    MY_TRANSFERS,
    MY_CONVERT,
    TRANSFERS_TO_OTHER,
    INTERBANK_TRANSFERS,
    P2P_TRANSFERS,
    INTERNATIONAL_TRANSFERS,
    MY_BONUS_COMPENSATION,
} TransferType;

@class TransferTemplate, NewCheckAndInitTransferResponse;

@interface NewTransfersApi : ApiClient

#pragma mark - Main

+ (NSURLSessionDataTask *)initAviaTransfer:(TransferType)transferType
                               requestorId:(NSNumber *)requestorId
                           requestorAmount:(NSNumber *)requestorAmount
                             destinationId:(NSNumber *)destinationId
                         destinationAmount:(NSNumber *)destinationAmount
                             requestorType:(NSString *)requestorType
                         requestorCurrency:(NSString *)requestorCurrency
                           destinationType:(NSString *)destinationType
                       destinationCurrency:(NSString *)destinationCurrency
                                 direction:(BOOL)direction
                                      save:(BOOL)save
                                     alias:(NSString *)alias
                                 reference:(NSString *)reference
                                   success:(success)success
                                   failure:(failure)failure;

+ (NSURLSessionDataTask *)checkTransfer:(TransferType)transferType
                            requestorId:(NSNumber *)requestorId
                        requestorAmount:(NSNumber *)requestorAmount
                          destinationId:(NSNumber *)destinationId
                          requestorType:(NSString *)requestorType
                      requestorCurrency:(NSString *)requestorCurrency
                        destinationType:(NSString *)destinationType
                    destinationCurrency:(NSString *)destinationCurrency
                                knpCode:(NSString *)knpCode
                                knpName:(NSString *)knpName
                              direction:(BOOL)direction
                                   save:(BOOL)save
                                  alias:(NSString *)alias
                                success:(success)success
                                failure:(failure)failure;

+ (NSURLSessionDataTask *)initTransfer:(TransferType)transferType
                           requestorId:(NSNumber *)requestorId
                       requestorAmount:(NSNumber *)requestorAmount
                         destinationId:(NSNumber *)destinationId
                         requestorType:(NSString *)requestorType
                     requestorCurrency:(NSString *)requestorCurrency
                       destinationType:(NSString *)destinationType
                   destinationCurrency:(NSString *)destinationCurrency
                               knpCode:(NSString *)knpCode
                               knpName:(NSString *)knpName
                             direction:(BOOL)direction
                                  save:(BOOL)save
                                 alias:(NSString *)alias
                            clientDesc:(NSString *)clientDesc
                               success:(success)success
                               failure:(failure)failure;

+ (NSURLSessionDataTask *)getTransfer:(NSNumber *)servicelogId
                               success:(success)success
                              failure:(failure)failure;

+ (NSURLSessionDataTask *)runTransfer:(NSNumber *)servicelogId
                              success:(success)success
                              failure:(failure)failure;

+ (NSURLSessionDataTask *)sendSMS:(NSNumber *)servicelogId
                           success:(success)success
                           failure:(failure)failure;

+ (NSURLSessionDataTask *)checkSMS:(NSString *)code
                      servicelogId:(NSNumber *)servicelogId
                           success:(success)success
                           failure:(failure)failure;

#pragma mark - P2P

+ (NSURLSessionTask *)initP2PWithSuccess:(success)success
                                 failure:(failure)failure;

+ (NSURLSessionTask *)initPayboxWithSuccess:(success)success
                                    failure:(failure)failure;

#pragma mark - from external card to owen account

+ (NSURLSessionTask *)checkTransferButtonDestinationId:(NSNumber *)destinationId
                                       destinationType:(NSString *)destinationType
                                     destinationAmount:(NSNumber *)destinationAmount
                                   destinationCurrency:(NSString *)destinationCurrency
                                                succes:(success)success
                                               failure:(failure)failure;

+ (NSURLSessionTask *)initPayboxButtonDestinationId:(NSNumber *)destinationId
                                    destinationType:(NSString *)destinationType
                                  destinationAmount:(NSNumber *)destinationAmount
                                destinationCurrency:(NSString *)destinationCurrency
                                             succes:(success)success
                                            failure:(failure)failure;

#pragma mark - Templates

+ (NSURLSessionTask *)getTemplatesWithSuccess:(void (^)(NSArray *transferTemplates))success
                                      failure:(failure)failure;

+ (NSURLSessionTask *)setTemplatesAlias:(NSString *)alias
                             templateId:(NSNumber *)templateId
                                success:(success)success
                                failure:(failure)failure;

+ (NSURLSessionTask *)setTemplatesDelete:(BOOL)del
                             templateId:(NSNumber *)templateId
                                success:(success)success
                                failure:(failure)failure;

#pragma mark - Helpers

+ (NSString *)transferTypeToString:(TransferType)transferType;
+ (TransferType)transferTypeForString:(NSString *)transferTypeString;

+ (NSString *)transferTypeToDescriptionString:(TransferType)transferType;

#pragma mark new trans. api

+ (NSURLSessionTask *)getKnpWithParams:(NSDictionary *)params
                               Success:(success)success
                               failure:(failure)failure;

+ (NSURLSessionTask *)getTypesWithSucces:(success)success
                                 failure:(failure)failure;

+ (NSURLSessionTask *)getAccountsWithParametrs:(NSDictionary *)params
                                       success:(void (^)(OperationAccounts *))onSuccess
                                       failure:(failure)failure;

+ (void)isUpdating:(success)success failure:(failure)failure;

+ (void)cancelAllOperations;

+ (NSURLSessionDataTask *)newCheckTransfer:(TransferType)transferType
                                    params:(NSMutableDictionary *) params
                               requestorId:(NSNumber *)requestorId
                                    amount:(NSNumber *)amount
                             destinationId:(NSNumber *)destinationId
                             requestorType:(NSString *)requestorType
                         requestorCurrency:(NSString *)requestorCurrency
                           destinationType:(NSString *)destinationType
                       destinationCurrency:(NSString *)destinationCurrency
                                   knpCode:(NSString *)knpCode
                                   knpName:(NSString *)knpName
                                 direction:(BOOL)direction
                                      save:(BOOL)save
                                     alias:(NSString *)alias
                                   success:(void (^)(NewCheckAndInitTransferResponse *))onSuccess
                                   failure:(failure)failure;

+ (NSURLSessionDataTask *)newInitTransferWithParams:(NSMutableDictionary *)params
                                        requestorId:(NSNumber *)requestorId
                                    requsetorAmount:(NSNumber *)reqAmount
                                  destinationAmount:(NSNumber *)desAmount
                                      destinationId:(NSNumber *)destinationId
                                      requestorType:(NSString *)requestorType
                                  requestorCurrency:(NSString *)requestorCurrency
                                    destinationType:(NSString *)destinationType
                                destinationCurrency:(NSString *)destinationCurrency
                                            knpCode:(NSString *)knpCode
                                            knpName:(NSString *)knpName
                                          direction:(BOOL)direction
                                               save:(BOOL)save
                                              alias:(NSString *)alias
                                            success:(void (^)(NewCheckAndInitTransferResponse *))onSuccess
                                            failure:(failure)failure;



+ (NSURLSessionDataTask *)newSendSMS:(NSNumber *)servicelogId
                             success:(success)success
                             failure:(failure)failure;

+ (NSURLSessionDataTask *)newCheckSMS:(NSString *)code
                         servicelogId:(NSNumber *)servicelogId
                              success:(success)success
                              failure:(failure)failure;

+ (NSURLSessionDataTask *)newRunTransfer:(NSNumber *)servicelogId
                                 success:(success)success
                                 failure:(failure)failure;

+ (NSURLSessionDataTask *)sendEncryptedCVV:(NSString *)cvv
                             withAccountId:(NSNumber *)cardId
                                   success:(success)success
                                   failure:(failure)failure;

+ (NSURLSessionDataTask *)getTransferResultFromWebWithServiceLogID:(NSNumber *)serviceLogId
                                                           success:(success)success
                                                           failure:(failure)failure ;

#pragma mark - avia mil run
+ (NSURLSessionDataTask *)runTransferAvia:(NSNumber *)servicelogId
                              success:(success)success
                              failure:(failure)failure;
@end
