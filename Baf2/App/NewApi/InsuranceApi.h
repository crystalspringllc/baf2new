//
// Created by Askar Mustafin on 11/16/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiClient.h"

typedef NS_ENUM(NSInteger, CatalogType) {
    CatalogTypeRegistrationTerritory = 0,
    CatalogTypeTransportType = 1,
    CatalogTypeDrivingExperience = 2,
    CatalogTypeTransportUsageTime = 3,
    CatalogTypeAllTypes = 4,
    CatalogTypeInsuranceType = 5

};

@interface InsuranceApi : ApiClient


/**
 * Getting list of cities
 *
 * @param success
 * @param failure
 */
+ (void)citiesSuccess:(success)success
              failure:(failure)failure;

/**
 *
 *
 *
 * @param insuranceType
 * @param age25DrivingExperience
 * @param registration_territory
 * @param transportType
 * @param transportUsageTime
 * @param success
 * @param failure
 */
+ (void)catalogsByCategoryType:(CatalogType)catalogType
                       success:(success)success
                       failure:(failure)failure;

+ (void)bonusmalusByIIN:(NSString *)iin
                success:(success)success
                failure:(failure)failure;

+ (void)calcCars:(NSArray *)cars
        insureds:(NSArray *)insureds
 benefitDiscount:(BOOL)benefitDiscount
     vovDiscount:(BOOL)vovDiscount
         success:(success)success
         failure:(failure)failure;

+ (void)scandocsStore:(UIImage *)image
              success:(success)success
              failure:(failure)failure;

+ (void)saveWithCars:(NSArray *)cars
            insureds:(NSArray *)insureds
     benefitDiscount:(BOOL)benefitDiscount
         vovDiscount:(BOOL)vovDiscount
 benefitDiscountDocs:(NSArray *)benefitDiscountDocs
     vovDiscountDocs:(NSArray *)vovDiscountDocs
             success:(success)success
             failure:(failure)failure;

+ (void)cashRun:(NSNumber *)serviceLogId success:(success)success failure:(failure)failure;


@end