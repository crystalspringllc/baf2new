//
// Created by Askar Mustafin on 6/21/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiClient.h"


@interface ProfileApi : ApiClient

/**
 * Updates profile with parameters (see in used viewcontroller)
 * @param params    - parameters
 * @param password  - password
 * @param success   - success callback
 * @param failure   - failure callback
 */
+ (void)updateProfileWithParams:(NSDictionary *)params
                       password:(NSString *)password
                        success:(success)success
                        failure:(failure)failure;

/**
 * Initializes email changing, sends sms code to referenced phone
 * @param newEmail  - email to change
 * @param oldEmail  - old email
 * @param phone     - phone to send sms code
 * @param success   - success callback
 * @param failure   - failure callback
 */
+ (void)updateEmail:(NSString *)newEmail
            success:(success)success
            failure:(failure)failure;


/**
 * Check sms code than was send to after calling updateEmail method
 * @param code      - sms code
 * @param phone     - phone where sms code was sent
 * @param serviceLogId - service log id from method update email
 * @param success
 * @param failure
 */
+ (void)checkEmailCode:(NSString *)code
               email:(NSString *)email
        serviceLogId:(NSNumber *)serviceLogId
             success:(success)success
             failure:(failure)failure;

+ (void)checkSmsCode:(NSString *)code
               phone:(NSString *)phone
        serviceLogId:(NSNumber *)serviceLogId
             success:(success)success
             failure:(failure)failure;


+ (void)sendSmsAgain:(NSString *)phone
        serviceLogId:(NSNumber *)serviceLogId
             success:(success)success
             failure:(failure)failure;


/**
 * Initialize updating phone number
 * @param newPhone      - new phone number
 * @param isSmsToEmail  - is also send code to email instead of old phone
 * @param success       - success callback
 * @param failure       - failure callback
 */
+ (void)updatePhone:(NSString *)newPhone
       isSmsToEmail:(BOOL)isSmsToEmail
            success:(success)success
            failure:(failure)failure;



+ (void)sendEmailCodeAgain:(NSString *)email
              serviceLogId:(NSNumber *)serviceLogId
                   success:(success)success
                   failure:(failure)failure;

//FINAL

//FOR EMAIL
+ (void)runEmailWith:(NSString *)newEmail
                smsCode:(NSString *)smsCode
           serviceLogId:(NSNumber *)serviceLogId
                success:(success)success
                failure:(failure)failure;

//FOR PHONE
+ (void)runPhoneWith:(NSString *)newPhone
             smsCodeNew:(NSString *)smsCodeNew
             smsCodeOld:(NSString *)smsCodeOld
              emailCode:(NSString *)emailCode
           serviceLogId:(NSNumber *)serviceLogId
                success:(success)success
                failure:(failure)failure;


@end