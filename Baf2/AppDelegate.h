//
//  AppDelegate.h
//  Baf2
//
//  Created by Askar Mustafin on 3/28/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGSideMenuController.h"
#import "ViewController.h"
@class MenuViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

#define kSideMenuController (LGSideMenuController *)[UIApplication sharedApplication].delegate.window.rootViewController

@property (strong, nonatomic) UIWindow *window;

/*!
	@brief It logouts User
*/
- (void)logoutUser;

/*!
	@brief Creates root navigation controller
	@return LGSideMenuController Instance of LGSideMenuController class
*/
- (LGSideMenuController *)createRootNavigationController;

@end
