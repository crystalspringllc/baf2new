//
//  CashBackConfigRouter.h
//  Baf2
//
//  Created by Dulatheo on 29.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CashBackConfigInteractor.h"
#import "Account.h"
@class CashBackConfigPresenter;

@interface CashBackConfigRouter : NSObject

@property (nonatomic, strong) CashBackConfigPresenter *cashBackConfigPresenter;
- (void)showCashBackConfigModuleInVC:(UIViewController *)viewController withAccount:(Account *)account;
- (void)close;

@end
