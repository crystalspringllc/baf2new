//
//  CashBackConfigRouter.m
//  Baf2
//
//  Created by Dulatheo on 29.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "CashBackConfigRouter.h"
#import "CashBackConfigViewController.h"
#import "CashBackConfigPresenter.h"
#import <STPopup/STPopupController.h>
#import "STPopupController+Extensions.h"
#import "Constants.h"

@interface CashBackConfigRouter()
@property (nonatomic, strong) STPopupController *popUpController;
@end

@implementation CashBackConfigRouter

-(void)showCashBackConfigModuleInVC:(UIViewController *)viewController withAccount:(Account*)account{
    CashBackConfigViewController *cashBackConfigViewController = [CashBackConfigViewController createVC];
    cashBackConfigViewController.output = self.cashBackConfigPresenter;
    cashBackConfigViewController.account = account;
    self.cashBackConfigPresenter.viewInput = cashBackConfigViewController;
    
    self.popUpController = [[STPopupController alloc] initWithRootViewController:cashBackConfigViewController];
    self.popUpController.dismissOnBackgroundTap = true;
    self.popUpController.style = STPopupStyleFormSheet;
    self.popUpController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [self.popUpController presentInViewController:viewController];
}

-(void)close{
    [self.popUpController dismiss];
}
@end
