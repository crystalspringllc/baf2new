//
//  PoliticsDisclaimerCashBackViewController.m
//  Baf2
//
//  Created by Dulatheo on 30.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "PoliticsDisclaimerCashBackViewController.h"
#import <STPopup/STPopup.h>

@implementation PoliticsDisclaimerCashBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - configUI

- (void)configUI{
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], [ASize screenHeight] - 100);
    self.title = NSLocalizedString(@"agreement", nil);
    CGRect viewFrame = self.view.frame;
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(viewFrame.origin.x, viewFrame.origin.y, self.contentSizeInPopup.width, self.contentSizeInPopup.height)];
    webView.contentMode = UIViewContentModeScaleAspectFit;
    webView.scrollView.bounces = NO;
    webView.delegate = self;
    CGPoint scrollPoint = CGPointMake(0, webView.scrollView.contentSize.height - self.contentSizeInPopup.height);
    [webView.scrollView setContentOffset:scrollPoint];
    NSMutableString * str;
    str = [NSMutableString new];
    [str appendString:self.htmlString];
    [webView loadHTMLString:str baseURL:nil];
    [self.view addSubview:webView];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{

}

@end
