//
//  CashBackConfigPresenter.h
//  Baf2
//
//  Created by Dulatheo on 29.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CashBackConfigViewIO.h"
#import "CashBackConfigInteractorIO.h"
#import "CashBackConfigRouter.h"

@interface CashBackConfigPresenter : NSObject<CashBackConfigInteractorOutput, CashBackConfigViewOutput>

@property (nonatomic, strong) id <CashBackConfigInteractorInput> interactorInput;

@property (nonatomic, strong) id <CashBackConfigViewInput> viewInput;

@property (nonatomic, strong) CashBackConfigRouter *cashBackConfigRouter;

@end
