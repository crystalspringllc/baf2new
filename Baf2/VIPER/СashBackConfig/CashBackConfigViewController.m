//
//  CashBackConfigViewController.m
//  Baf2
//
//  Created by Dulatheo on 29.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "CashBackConfigViewController.h"
#import <STPopup/STPopup.h>
#import "MainButton.h"
#import "PoliticsDisclaimerCashBackViewController.h"
#import "SMSConfirmationPopupViewController.h"

@interface CashBackConfigViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *agreementTappableView;
@property (weak, nonatomic) IBOutlet UISwitch *agreementSwitch;
@property (weak, nonatomic) IBOutlet UILabel *switchContainer;
@property (weak, nonatomic) IBOutlet MainButton *mainButton;

@property (nonatomic, strong) SMSConfirmationPopupViewController *v;


@end

@implementation CashBackConfigViewController

+(instancetype)createVC{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CashBackConfig" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([CashBackConfigViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - actions
- (void)agreementTapped:(UITapGestureRecognizer *)sender{
    [self.output getPolitics:_account.accountId];
}

- (IBAction)mianButtonClicked:(MainButton *)sender {
    [self.output changeCashBackInit:_account.accountId];
}

- (IBAction)switchValueChanged:(UISwitch *)sender {
    if (sender.isOn) {
        [self.mainButton setMainButtonStyle:MainButtonStyleOrange];
    }else{
        [self.mainButton setMainButtonStyle:MainButtonStyleDisable];
    }
}

#pragma mark - view input protocol
-(void)inputMainButtonTitle:(NSString *)title{
    [self.mainButton setTitle:title];
}

-(void)inputTitleLabel:(NSString *)title{
    [self.titleLabel setText:title];
}

-(void)inputIconImage:(UIImage *)image{
    [self.iconImageView setImage:image];
}

- (void)openHtmlDisclaimer:(NSString *)data{
    PoliticsDisclaimerCashBackViewController *vc = [[PoliticsDisclaimerCashBackViewController alloc]init];
    vc.htmlString = data;
    [self.popupController pushViewController:vc animated:YES];
}

-(void)openSmsViewController:(NSNumber *)serviceLogId{
    CashBackConfigViewController *wSelf = self;
    self.v = [[SMSConfirmationPopupViewController alloc] init];
    self.v.verifySmsCode = ^(SMSConfirmationPopupViewController *_self, NSString *smsCode) {
        NSNumber *code = @([smsCode floatValue]);
        [wSelf.output confirmSmsCodeCashBack:serviceLogId code:code];
    };
    self.v.sendSmsCodeAgain = ^(SMSConfirmationPopupViewController *_self) {
        [wSelf.output resendSmsCashBack:serviceLogId];
    };
    [self.popupController pushViewController:self.v animated:YES];
}

- (void)checkSmsCodeSuccess {
    [self.v smsTextFieldStopLoading];
    [self.v showSuccessIcon];
    [Toast showToast:@"Процесс изменения настроек «режима добра» запущен, ожидайте"];
    CashBackConfigViewController *wSelf = self;
    [self performBlock:^{
        [wSelf.v dismiss];
    } afterDelay:1];
    
    
}

- (void)checkSmsCodeFailure {
    [self.v smsTextFieldStopLoading];
}

- (void)smsSentAgain {
    [self.v smsTextFieldStopLoading];
    [UIAlertView showWithTitle:nil message:NSLocalizedString(@"new_code_send", nil) cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
}

#pragma mark - hud things
//
// show and hide baf hud
//
- (void)showBafHUDWithTitle:(NSString *)title {
    [MBProgressHUD showAstanaHUDWithTitle:title animated:YES];
}

- (void)hideBafHUD {
    [MBProgressHUD hideAstanaHUDAnimated:YES];
}

#pragma mark - config ui

- (void)configUI {
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = NSLocalizedString(@"config_charity", nil);
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 420 - 50);
    
    [self.agreementSwitch setOn:NO];
    [self.mainButton setMainButtonStyle:MainButtonStyleDisable];
    
    [self.mainButton setTitle: !self.account.cashbackOn ? @"ОТКЛЮЧИТЬ" : @"ВКЛЮЧИТЬ"];
    [self.titleLabel setText: !self.account.cashbackOn ? @"Отключить режим добра\n (начислять CashBack на карту)" : @"Включить «режим добра» \n(переводить Cashback на \nблаготворительность)"];
    [self.iconImageView setImage:[UIImage imageNamed:!self.account.cashbackOn ? @"bankcards" : @"charity"]];
    
    UITapGestureRecognizer *tapAgreementView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(agreementTapped:)];
    [_agreementTappableView addGestureRecognizer:tapAgreementView];
}

@end
