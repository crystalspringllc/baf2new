//
//  CashBackConfigInteractor.h
//  Baf2
//
//  Created by Dulatheo on 29.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CashBackConfigInteractorIO.h"
#import "ModelObject.h"
#import "Account.h"

@protocol CashBackConfigInteractorOutput;

@interface CashBackConfigInteractor : NSObject<CashBackConfigInteractorInput>

@property (nonatomic, weak) id<CashBackConfigInteractorOutput> output;

-(void)changeCashBackRun:(NSNumber *)serviceLogId;
@end
