//
//  CashBackConfigViewIO.h
//  Baf2
//
//  Created by Dulatheo on 29.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CashBackConfigViewOutput <NSObject>
- (void)getPolitics:(NSNumber *)cardId;
- (void)changeCashBackInit:(NSNumber *)cardId;
- (void)resendSmsCashBack:(NSNumber *)serviceLogId;
- (void)confirmSmsCodeCashBack:(NSNumber *)serviceLogId code:(NSNumber *)code;
@end

@protocol CashBackConfigViewInput <NSObject>
- (void)inputMainButtonTitle:(NSString *)title;
- (void)inputTitleLabel:(NSString *)title;
- (void)inputIconImage:(UIImage *)image;

- (void)showBafHUDWithTitle:(NSString *)title;
- (void)hideBafHUD;

- (void)openHtmlDisclaimer:(NSString *)data;
- (void)openSmsViewController:(NSNumber *)serviceLogId;

- (void)checkSmsCodeSuccess;
- (void)checkSmsCodeFailure;
- (void)smsSentAgain;
@end

