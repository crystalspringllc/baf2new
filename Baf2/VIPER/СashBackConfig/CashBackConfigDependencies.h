//
//  CashBackConfigDependencies.h
//  Baf2
//
//  Created by Dulatheo on 29.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Account.h"
#import "CashBackConfigViewController.h"
#import "CashBackConfigViewIO.h"

@class CashBackConfigInteractor;

@interface CashBackConfigDependencies : NSObject

@property (nonatomic, strong) CashBackConfigInteractor *cashBackConfigInteractor;
@property (nonatomic, strong) CashBackConfigViewController *cashBackConfigViewController;

@property (nonatomic, strong) UIViewController *parentVC;
@property (nonatomic, strong) Account *account;
- (void)installRootViewController;

@end
