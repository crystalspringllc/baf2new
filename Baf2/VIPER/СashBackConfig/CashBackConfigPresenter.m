//
//  CashBackConfigPresenter.m
//  Baf2
//
//  Created by Dulatheo on 29.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "CashBackConfigPresenter.h"

@implementation CashBackConfigPresenter

#pragma mark - view output protocol

-(void)getPolitics:(NSNumber*)cardId{
    [self.viewInput showBafHUDWithTitle:NSLocalizedString(@"loading", nil)];
    [self.interactorInput getCashBackDisclaimer:cardId];
}

-(void)changeCashBackInit:(NSNumber *)cardId{
    [self.viewInput showBafHUDWithTitle:NSLocalizedString(@"loading", nil)];
    [self.interactorInput cashBackChangeInit:cardId];
}

-(void)resendSmsCashBack:(NSNumber *)serviceLogId{
    [self.viewInput showBafHUDWithTitle:NSLocalizedString(@"loading", nil)];
    [self.interactorInput cashBackSmsResend:serviceLogId];
}

-(void)confirmSmsCodeCashBack:(NSNumber *)serviceLogId code:(NSNumber *)code{
    [self.viewInput showBafHUDWithTitle:NSLocalizedString(@"loading", nil)];
    [self.interactorInput cashBackSmsConfirm:serviceLogId code:code];
}

#pragma mark - interactor output protocol

-(void)didCashBackDisclaimerReceived:(NSString *)data{
    [self.viewInput hideBafHUD];
    [self.viewInput openHtmlDisclaimer:data];
}

-(void)didCashBackDisclaimerFailured{
    [self.viewInput hideBafHUD];
}

-(void)changeCashBackInitReceived:(NSNumber *)data{
    [self.viewInput hideBafHUD];
    [self.viewInput openSmsViewController:data];
}

-(void)changeCashBackInitFailured{
    [self.viewInput hideBafHUD];
}

-(void)smsResendCashBackReceived{
    [self.viewInput hideBafHUD];
    [self.viewInput smsSentAgain];
}

-(void)smsResendCashBackFailured{
    [self.viewInput hideBafHUD];
}

-(void)smsConfirmCashBackReceived{
    [self.viewInput hideBafHUD];
    [self.viewInput checkSmsCodeSuccess];
}

-(void)smsConfirmCashBackFailured{
    [self.viewInput hideBafHUD];
    [self.viewInput checkSmsCodeFailure];
}

@end
