//
//  CashBackConfigInteractor.m
//  Baf2
//
//  Created by Dulatheo on 29.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "CashBackConfigInteractor.h"
#import "CashBackApi.h"

@implementation CashBackConfigInteractor

#pragma mark - interactor input protocol

-(void)getCashBackDisclaimer:(NSNumber*)cardId{
    __weak CashBackConfigInteractor *wSelf = self;
    [self getDisclaimer:cardId completion:^(NSString *response) {
        [wSelf.output didCashBackDisclaimerReceived:response];
    }];
}

-(void)cashBackChangeInit:(NSNumber *)cardId{
    __weak CashBackConfigInteractor *wSelf = self;
    [self cashBackInit:cardId completion:^(NSNumber *response) {
        [wSelf.output changeCashBackInitReceived:response];
    }];
}

-(void)cashBackSmsResend:(NSNumber *)serviceLogId{
    [self cashBackSmsResendRequest:serviceLogId];
}

-(void)cashBackSmsConfirm:(NSNumber *)serviceLogId code:(NSNumber *)code{
    [self cashBackSmsConfirmRequest:serviceLogId code:code];
}


-(void)changeCashBackRun:(NSNumber *)serviceLogId{
    [self cashBackRunRequest:serviceLogId];
}

#pragma mark - api requests

-(void)getDisclaimer:(NSNumber*)cardId completion:(void (^)(NSString *response))completion{
    [CashBackApi getCashBackDisclaimer:cardId success:^(id response) {
        NSString *data = (NSString *)response;        
        if(completion){
            completion(data);
        }
    } failure:^(NSString *code, NSString *message) {
        [self.output didCashBackDisclaimerFailured];
    }];
}

-(void)cashBackInit:(NSNumber *)cardId completion:(void (^)(NSNumber *response))completion{
    [CashBackApi changeCashBackInit:cardId success:^(id response) {
        NSNumber *data = (NSNumber *)response;
        if(completion){
            completion(data);
        }
    } failure:^(NSString *code, NSString *message) {
        [self.output changeCashBackInitFailured];
    }];
}

-(void)cashBackSmsResendRequest:(NSNumber *)serviceLogId{
    [CashBackApi changeCashBackSmsResend:serviceLogId success:^(id response) {
        [self.output smsResendCashBackReceived];
    } failure:^(NSString *code, NSString *message) {
        [self.output smsResendCashBackFailured];
    }];
}

-(void)cashBackSmsConfirmRequest:(NSNumber *)serviceLogId code:(NSNumber *)code{
    [CashBackApi changeCashBackSmsConfirm:serviceLogId code:code success:^(id response) {
        [self changeCashBackRun:serviceLogId];
    } failure:^(NSString *code, NSString *message) {
        [self.output smsConfirmCashBackFailured];
    }];
}

-(void)cashBackRunRequest:(NSNumber *)serviceLogId{
    [CashBackApi changeCashBackRun:serviceLogId success:^(id response) {
        [self.output smsConfirmCashBackReceived];
    } failure:^(NSString *code, NSString *message) {
        [self.output smsConfirmCashBackFailured];
    }];
}

@end
