//
//  CashBackConfigInteractorIO.h
//  Baf2
//
//  Created by Dulatheo on 29.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CashBackConfigInteractorInput <NSObject>
- (void)getCashBackDisclaimer:(NSNumber *)cardId;
- (void)cashBackChangeInit:(NSNumber *)cardId;
- (void)cashBackSmsResend:(NSNumber *)serviceLogId;
- (void)cashBackSmsConfirm:(NSNumber *)serviceLogId code:(NSNumber *)code;
@end

@protocol CashBackConfigInteractorOutput <NSObject>
- (void)didCashBackDisclaimerReceived:(NSString *)data;
- (void)didCashBackDisclaimerFailured;

- (void)changeCashBackInitReceived:(NSNumber *)data;
- (void)changeCashBackInitFailured;

- (void)smsResendCashBackReceived;
- (void)smsResendCashBackFailured;

- (void)smsConfirmCashBackReceived;
- (void)smsConfirmCashBackFailured;
@end
