//
//  CashBackConfigViewController.h
//  Baf2
//
//  Created by Dulatheo on 29.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CashBackConfigViewIO.h"
#import "Account.h"

@interface CashBackConfigViewController : UIViewController<CashBackConfigViewInput>
+ (instancetype)createVC;

@property (nonatomic, strong) id<CashBackConfigViewOutput> output;

@property (nonatomic, strong) Account *account;

@end
