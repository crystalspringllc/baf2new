//
//  CashBackConfigDependencies.m
//  Baf2
//
//  Created by Dulatheo on 29.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "CashBackConfigDependencies.h"
#import "CashBackConfigRouter.h"
#import "CashBackConfigPresenter.h"



@interface CashBackConfigDependencies()
@property (nonatomic, strong) CashBackConfigRouter *cashBackConfigRouter;
@end
@implementation CashBackConfigDependencies{}

-(id)init {
    self = [super init];
    if (self) {
        [self configureDependencies];
    }
    return self;
}

-(void)installRootViewController{
    [self.cashBackConfigRouter showCashBackConfigModuleInVC:self.parentVC withAccount:self.account];
}

-(void)configureDependencies{
    CashBackConfigRouter *cashBackConfigRouter = [[CashBackConfigRouter alloc] init];
    CashBackConfigPresenter *cashBackConfigPresenter = [[CashBackConfigPresenter alloc] init];
    CashBackConfigInteractor *cashBackConfigInteractor = [[CashBackConfigInteractor alloc] init];
    
    cashBackConfigInteractor.output = cashBackConfigPresenter;
    cashBackConfigPresenter.interactorInput = cashBackConfigInteractor;
    
    cashBackConfigRouter.cashBackConfigPresenter = cashBackConfigPresenter;
    cashBackConfigPresenter.cashBackConfigRouter = cashBackConfigRouter;
    
    self.cashBackConfigRouter = cashBackConfigRouter;
    self.cashBackConfigInteractor = cashBackConfigInteractor;
}
- (void)dealloc {
    NSLog(@"CashBackConfigDependencies DEALLOC");
}

@end
