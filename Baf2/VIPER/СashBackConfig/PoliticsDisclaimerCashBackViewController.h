//
//  PoliticsDisclaimerCashBackViewController.h
//  Baf2
//
//  Created by Dulatheo on 30.03.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PoliticsDisclaimerCashBackViewController : UIViewController<UIWebViewDelegate>

@property (nonatomic, strong) NSString* htmlString;

@end
