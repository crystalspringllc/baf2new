//
// Created by Askar Mustafin on 11/4/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "RateAppDialogViewController.h"
#import "RateAppFeedbackViewController.h"
#import <STPopup/STPopup.h>
#import "MainButton.h"

#define kEnterCounts @"enterCounts"

@interface RateAppDialogViewController()
@property (weak, nonatomic) IBOutlet MainButton *yesButton;
@property (weak, nonatomic) IBOutlet MainButton *laterButton;
@property (weak, nonatomic) IBOutlet MainButton *noButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@end

@implementation RateAppDialogViewController {}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"RateAppDialog" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([RateAppDialogViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth] - 40, 300);
    [self configUI];
}

#pragma mark - config actions

- (IBAction)clickYesButton:(id)sender {
    [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:kEnterCounts];
    [[NSUserDefaults standardUserDefaults] synchronize];

    int appId = 877622886;
    NSString *templateReviewURL = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=APP_ID";
    NSString *templateReviewURLiOS7 = @"itms-apps://itunes.apple.com/app/idAPP_ID";
    NSString *templateReviewURLiOS8 = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=APP_ID&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software";

    //ios7 before
    NSString *reviewURL = [templateReviewURL stringByReplacingOccurrencesOfString:@"APP_ID" withString:[NSString stringWithFormat:@"%d", appId]];

    // iOS 7 needs a different templateReviewURL @see https://github.com/arashpayan/appirater/issues/131
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 && [[[UIDevice currentDevice] systemVersion] floatValue] < 7.1)
    {
        reviewURL = [templateReviewURLiOS7 stringByReplacingOccurrencesOfString:@"APP_ID" withString:[NSString stringWithFormat:@"%d", appId]];
    }
        // iOS 8 needs a different templateReviewURL also @see https://github.com/arashpayan/appirater/issues/182
    else if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        reviewURL = [templateReviewURLiOS8 stringByReplacingOccurrencesOfString:@"APP_ID" withString:[NSString stringWithFormat:@"%d", appId]];
    }

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL] options:@{} completionHandler:nil];
    [self.popupController dismiss];
}

- (IBAction)clickLaterButton:(id)sender {
    //after 3 enters open again
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:kEnterCounts];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.popupController dismiss];
}

- (IBAction)clickNoButton:(id)sender {
    [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:kEnterCounts];
    [[NSUserDefaults standardUserDefaults] synchronize];
    RateAppFeedbackViewController *v = [RateAppFeedbackViewController createVC];
    [self.popupController pushViewController:v animated:YES];
}

#pragma mark - config ui

- (void)configUI {
    [self.yesButton setMainButtonStyle:MainButtonStyleOrange];
    [self.laterButton setMainButtonStyle:MainButtonStyleOrange];
    [self.noButton setMainButtonStyle:MainButtonStyleOrange];
    [self.logoImageView setTintColor:[UIColor blackColor]];
}

@end
