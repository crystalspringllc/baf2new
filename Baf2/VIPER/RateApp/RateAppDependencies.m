//
// Created by Askar Mustafin on 11/4/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "RateAppDependencies.h"
#import "RateAppRouter.h"

@interface RateAppDependencies()

@property (nonatomic, strong) RateAppRouter *router;

@end

@implementation RateAppDependencies {

}

- (id)init {
    self = [super init];
    if (self) {
        [self configureDependencies];
    }
    return self;
}

- (void)startRouter {
    [self.router startRouterIn:self.parentVC];
}

- (void)configureDependencies {
    RateAppRouter *router = [[RateAppRouter alloc] init];
    self.router = router;
}

@end