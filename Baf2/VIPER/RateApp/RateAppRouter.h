//
// Created by Askar Mustafin on 11/4/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RateAppRouter : NSObject

- (void)startRouterIn:(UIViewController *)viewController;

@end