//
// Created by Askar Mustafin on 11/4/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "RateAppRouter.h"
#import "NSString+Ext.h"
#import "NSDate+Ext.h"
#import "RateAppDialogViewController.h"
#import "STPopupController.h"
#import "STPopupController+Extensions.h"

#define kAppOpenTime @"appOpenTime"
#define kRateDidOpened @"rateDidOpened"
#define kEnterCounts @"enterCounts"

#define kAppOpenTimeDateFormat @"dd.MM.yyyy"

@interface RateAppRouter()
@property (nonatomic, strong) STPopupController *popupController;
@end

@implementation RateAppRouter {}

- (void)startRouterIn:(UIViewController *)viewController {
    NSString *appOpenTimeString = [[NSUserDefaults standardUserDefaults] objectForKey:kAppOpenTime];
    NSDate *currentDate = [NSDate date];

    if (appOpenTimeString) {
        NSDate *appOpenTimeDate = [appOpenTimeString dateWithDateFormat:kAppOpenTimeDateFormat];

        int daysBetween = (int) [NSDate daysBetweenDate:appOpenTimeDate andDate:currentDate];
        int enterCount = (int) [[NSUserDefaults standardUserDefaults] integerForKey:kEnterCounts];

        if (daysBetween > 10 && enterCount < 3) {
            if (![[NSUserDefaults standardUserDefaults] boolForKey:kRateDidOpened]) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kRateDidOpened];
                [self showDialogView:viewController];
            } else {
                if (enterCount == 3) {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kRateDidOpened];
                    [self showDialogView:viewController];
                } else {
                    enterCount = enterCount + 1;
                    [[NSUserDefaults standardUserDefaults] setInteger:enterCount forKey:kEnterCounts];
                }
            }
        }
    } else {
        NSString *currentDataString = [currentDate stringWithDateFormat:kAppOpenTimeDateFormat];
        [[NSUserDefaults standardUserDefaults] setObject:currentDataString forKey:kAppOpenTime];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)showDialogView:(UIViewController *)viewController {
    RateAppDialogViewController *smsBankingViewController = [RateAppDialogViewController createVC];

    self.popupController = [[STPopupController alloc] initWithRootViewController:smsBankingViewController];
    self.popupController.dismissOnBackgroundTap = true;
    self.popupController.style = STPopupStyleFormSheet;
    self.popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    self.popupController.navigationBarHidden = YES;
    [self.popupController presentInViewController:viewController];
}

@end