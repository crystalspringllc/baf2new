//
// Created by Askar Mustafin on 11/4/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RateAppDialogViewController : UIViewController

+ (instancetype)createVC;

@end
