//
// Created by Askar Mustafin on 11/4/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "RateAppFeedbackViewController.h"
#import <STPopup/STPopup.h>
#import "MainButton.h"
#import "ApiClient.h"
#import "InfoApi.h"

#define kEnterCounts @"enterCounts"

#define kMaxTextLenght 2000

@interface RateAppFeedbackViewController()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet MainButton *leaveFeedback;
@property (weak, nonatomic) IBOutlet MainButton *close;

@end

@implementation RateAppFeedbackViewController {}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"RateAppDialog" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([RateAppFeedbackViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 300);
    [self configUI];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.textView becomeFirstResponder];
}

#pragma mark - config actions

- (IBAction)clickCloseButton:(id)sender {
    [self.popupController dismiss];
}

- (IBAction)clickLeaceFeedbackButton:(id)sender {
    if (self.textView.text.length > 0 ) {

        //request
        [MBProgressHUD showAstanaHUDWithTitle:@"Отправка отзыва" animated:YES];
        [InfoApi sendRevieReviewText:self.textView.text Success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            [Toast showToast:@"Спасибо за Ваш отзыв. Мы обязательно примем его во внимание!"];

            [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:kEnterCounts];
            [[NSUserDefaults standardUserDefaults] synchronize];

            [self.popupController dismiss];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            [self.popupController dismiss];
        }];

    } else {
        [UIHelper showAlertWithMessage:@"Напишите отзыв"];
    }
}

#pragma mark - text view delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return textView.text.length + (text.length - range.length) <= kMaxTextLenght;
}

#pragma mark - config ui

- (void)configUI {
    [self.leaveFeedback setMainButtonStyle:MainButtonStyleOrange];
    [self.close setMainButtonStyle:MainButtonStyleOrange];

    self.textView.delegate = self;
}

@end
