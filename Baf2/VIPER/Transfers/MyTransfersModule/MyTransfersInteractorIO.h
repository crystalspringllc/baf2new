//
// Created by Askar Mustafin on 5/23/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Account.h"

@class CheckAndInitTransferResponse;


@protocol MyTransfersInteractorInput <NSObject>

- (void)prepareAccounts;

- (void)checkTransferSourceAccount:(Account *)sourceAccount
           selectedSourceCurrrency:(NSString *)selectedSourceCurrency
                     targetAccount:(Account *)targetAccount
            selectedTargetCurrency:(NSString *)selectedTargetCurrency
                            amount:(NSNumber *)amount
                    isSaveTemplate:(BOOL)isSaveTemplate
                     templateAlias:(NSString *)templateAlias
                         direction:(BOOL)direction;

- (void)initTransferSourceAccount:(Account *)sourceAccount
          selectedSourceCurrrency:(NSString *)selectedSourceCurrency
                    targetAccount:(Account *)targetAccount
           selectedTargetCurrency:(NSString *)selectedTargetCurrency
                           amount:(NSNumber *)amount
                   isSaveTemplate:(BOOL)isSaveTemplate
                    templateAlias:(NSString *)templateAlias
                        direction:(BOOL)direction;

@end


@protocol MyTransfersInteractorOutput <NSObject>

- (void)handleLoadedAccounts:(id)loadedAccounts;
- (void)handleCheckTransferResponse:(CheckAndInitTransferResponse *)checkTransferResponse;
- (void)handleCheckTransferFailure:(NSString *)message;
- (void)handleInitTransferResponse:(CheckAndInitTransferResponse *)checkTransferResponse;

@end