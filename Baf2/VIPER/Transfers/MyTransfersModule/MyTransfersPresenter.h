//
// Created by Askar Mustafin on 5/23/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyTransfersViewIO.h"
#import "MyTransfersInteractorIO.h"

@class MyTransfersRouter;
@class TransferTemplate;


@interface MyTransfersPresenter : NSObject<MyTransfersInteractorOutput, MyTransfersViewOutput>

@property (nonatomic, strong) id <MyTransfersInteractorInput> interactorInput;
@property (nonatomic, strong) id <MyTransfersViewInput> viewInput;

@property (nonatomic, strong) MyTransfersRouter *myTransfersRouter;

@property (nonatomic, strong) TransferTemplate *transferTemplate;
@property (nonatomic, strong) Account *targetChargeAccount;

@end