#import <Foundation/Foundation.h>
#import "ModelObject.h"

@class Account;
@class TransferCurrencyRates;

@interface CheckAndInitTransferResponse : ModelObject {

    NSNumber *amount;
    NSNumber *clientId;
    NSString *currency;
    Account *destinationAccount;
    NSNumber *destinationAmount;
    NSString *destinationCurrency;
    NSNumber *destinationId;
    NSString *destinationType;
    BOOL direction;
    NSString *errorMessage;
    Account *requestorAccount;
    NSString *requestorCurrency;
    NSNumber *requestorId;
    NSString *requestorType;
    NSString *session;
    NSString *type;

    NSNumber *comission;
    NSNumber *comissionCurrency;
    TransferCurrencyRates *exchangeResponse;
    BOOL isValid;
    NSArray *knpList;
    NSString *operdate;
    NSNumber *requestorAmount;
    NSString *safetyLevel;
    NSArray *warningMessages;

    BOOL isPinVerifyRequired;
    NSString *uuid;
    NSNumber *servicelogId;
    BOOL save;
}

@property (nonatomic, copy) NSNumber *amount;
@property (nonatomic, copy) NSNumber *clientId;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, strong) Account *destinationAccount;
@property (nonatomic, copy) NSNumber *destinationAmount;
@property (nonatomic, copy) NSString *destinationCurrency;
@property (nonatomic, copy) NSNumber *destinationId;
@property (nonatomic, copy) NSString *destinationType;
@property (nonatomic, assign) BOOL direction;
@property (nonatomic, copy) NSString *errorMessage;
@property (nonatomic, strong) Account *requestorAccount;
@property (nonatomic, copy) NSString *requestorCurrency;
@property (nonatomic, copy) NSNumber *requestorId;
@property (nonatomic, copy) NSString *requestorType;
@property (nonatomic, copy) NSString *session;
@property (nonatomic, copy) NSString *type;

@property (nonatomic, strong) NSNumber *comission;
@property (nonatomic, strong) NSNumber *comissionCurrency;
@property (nonatomic, strong) TransferCurrencyRates *exchangeResponse;
@property (nonatomic, assign) BOOL isValid;
@property (nonatomic, strong) NSArray *knpList;
@property (nonatomic, strong) NSString *operdate;
@property (nonatomic, strong) NSNumber *requestorAmount;
@property (nonatomic, strong) NSString *safetyLevel;
@property (nonatomic, strong) NSArray *warningMessages;

@property (nonatomic, assign) BOOL isPinVerifyRequired;
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSNumber *servicelogId;
@property (nonatomic, assign) BOOL save;


+ (CheckAndInitTransferResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
