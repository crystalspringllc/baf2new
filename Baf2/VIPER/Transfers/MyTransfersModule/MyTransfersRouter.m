//
// Created by Askar Mustafin on 5/23/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "MyTransfersRouter.h"
#import "MyTransfersPresenter.h"
#import "MyTransfersViewController.h"


@interface MyTransfersRouter() {}
@property (nonatomic, strong) MyTransfersViewController *myTransfersViewController;
@end

@implementation MyTransfersRouter {}

- (void)pushMyTransfersModule:(UINavigationController *)navigationController {
    MyTransfersViewController *myTransfersViewController = [MyTransfersViewController createVC];

    myTransfersViewController.output = self.myTransfersPresenter;
    self.myTransfersPresenter.viewInput = myTransfersViewController;

    self.myTransfersViewController = myTransfersViewController;
    [navigationController pushViewController:myTransfersViewController animated:YES];
}

@end