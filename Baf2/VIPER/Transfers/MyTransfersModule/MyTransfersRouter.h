//
// Created by Askar Mustafin on 5/23/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MyTransfersPresenter;


@interface MyTransfersRouter : NSObject

@property (nonatomic, strong) MyTransfersPresenter *myTransfersPresenter;

- (void)pushMyTransfersModule:(UINavigationController *)navigationController;

@end