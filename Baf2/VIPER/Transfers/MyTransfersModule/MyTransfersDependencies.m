//
// Created by Askar Mustafin on 5/23/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "MyTransfersDependencies.h"
#import "MyTransfersRouter.h"
#import "MyTransfersPresenter.h"
#import "MyTransfersInteractor.h"
#import "TransferTemplate.h"
#import "Account.h"

@interface MyTransfersDependencies()
@property (nonatomic, strong) MyTransfersRouter *myTransfersRouter;
@end

@implementation MyTransfersDependencies {

}

- (id)init {
    self = [super init];
    if (self) {
        [self configureDependencies];
    }
    return self;
}

- (void)configureDependencies {

    //Transfer Modules Classes
    MyTransfersRouter *myTransfersRouter = [[MyTransfersRouter alloc] init];
    MyTransfersPresenter *myTransfersPresenter = [[MyTransfersPresenter alloc] init];
    MyTransfersInteractor *myTransfersInteractor = [[MyTransfersInteractor alloc] init];

    //contections
    //interactor's OUTPUT with presenter
    //presenter's INPUT with interactor
    myTransfersInteractor.output = myTransfersPresenter;
    myTransfersPresenter.interactorInput = myTransfersInteractor;

    myTransfersRouter.myTransfersPresenter = myTransfersPresenter;
    myTransfersPresenter.myTransfersRouter = myTransfersRouter;

    self.myTransfersRouter = myTransfersRouter;
}

- (void)setTransferTemplate:(TransferTemplate *)transferTemplate {
    _transferTemplate = transferTemplate;
    self.myTransfersRouter.myTransfersPresenter.transferTemplate = transferTemplate;
}

- (void)setTargetChargeAccount:(Account *)targetChargeAccount {
    _targetChargeAccount = targetChargeAccount;
    self.myTransfersRouter.myTransfersPresenter.targetChargeAccount = targetChargeAccount;
}

- (void)installRootViewController {
    [self.myTransfersRouter pushMyTransfersModule:[UIHelper rootNavigationController]];
}

#pragma mark -

- (void)dealloc {
    NSLog(@"DEPENDENCY DEALLOC");
}

@end