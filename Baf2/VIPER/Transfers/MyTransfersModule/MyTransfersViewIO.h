//
// Created by Askar Mustafin on 5/23/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Account;
@class CheckAndInitTransferResponse;

@protocol MyTransfersViewOutput <NSObject>

/**
 * Check if template setted
 */
- (BOOL)isTemplate;

/**
 * Run transfer init
 */
- (void)runInitTransferWithAmount:(NSString *)amountString isSave:(BOOL)isSave templateName:(NSString *)templateName;

/**
 * Run transfer check
 */
- (void)runCheckTransferWithAmount:(NSString *)amountString direction:(BOOL)direction;

/**
 * load accounts at start
 */
- (void)configureStart;

/**
 * configure from template
 */
- (void)configureWithTemplate;

/**
 * configure with target account to charge
 */
- (void)configureWithTargetAccount;

/**
 * Open picker popup
 */
- (void)openSourceAccountSelection;
- (void)openTargetAccountSelection;

- (void)openSourceBalanceCurrencySelection;
- (void)openTargetBalanceCurrencySelection;

/**
 * Selected accounts
 */
- (void)setSelectedSource:(Account *)sourceAccount;
- (void)setSelectedTarget:(Account *)targetAccount;

/**
 * Selected balances
 */
- (void)setSelectedSourceBalance:(id)sourceBalance;
- (void)setSelectedTargetBalance:(id)targetBalance;

@end

@protocol MyTransfersViewInput <NSObject>

/**
 * Go to transfer confirmation
 */
- (void)goToConfirmation:(CheckAndInitTransferResponse *)checkAndInitTransferResponse;

/**
 * Enable and disable pay button
 */
- (void)setTransferButtonEnabled;
- (void)setTransferButtonDisabled;

/**
 * Show and hide progress view
 */
- (void)showPregressHUD:(NSString *)message;
- (void)hideProgressHUD;

/**
 * prepare accoutns for popup
 * @param accounts
 */
- (void)sourceAccountSheet:(NSArray *)accounts;
- (void)targetAccountSheet:(NSArray *)accounts;

/**
 * prepare accounts for tableview
 */
- (void)preparedSourceAccount:(NSDictionary *)source;
- (void)preparedTargetAccount:(NSDictionary *)target;

/**
 * prepare balance and currency for popup
 */
- (void)sourceBalanceSheet:(NSArray *)sourceBalanceAndCurrencies;
- (void)targetBalanceSheet:(NSArray *)targetBalanceAndCurrencies;

/**
 * prepare balance and currency for tableview
 */
- (void)preparedSourceBalance:(NSString *)sourceBalance;
- (void)preparedTargetBalance:(NSString *)targetBalance;

/**
 * Show check transfer's error message
 */
- (void)showErrorMessage:(NSString *)errorMessage;

/**
 * Show currency rate and update time in disclamer
 * @param message
 * @param time
 */
- (void)showCurrencyRate:(NSString *)message andUpdateTime:(NSString *)time;

/**
 * Show warning messages
 */
- (void)showWarningMessages:(NSArray *)warningMessages;

/**
 * Show amounts
 * @param amount
 */
- (void)showAmount:(NSString *)amount andDestinationAmount:(NSString *)destinationAmount;

@end