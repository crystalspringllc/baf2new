//
// Created by Askar Mustafin on 5/23/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TransferTemplate;
@class Account;


@interface MyTransfersDependencies : NSObject

@property (nonatomic, strong) TransferTemplate *transferTemplate;
@property (nonatomic, strong) Account *targetChargeAccount;

- (void)installRootViewController;

@end