//
// Created by Askar Mustafin on 5/23/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "MyTransfersViewController.h"
#import "MyTransfersViewIO.h"
#import "UIViewController+Extension.h"
#import "TransfersFormDisclaimerCell.h"
#import "ActionSheetListPopupViewController.h"
#import "AccountsActionSheetViewController.h"
#import "STPopupController.h"
#import "STPopupController+Extensions.h"
#import "TransfersFormCell.h"
#import "AccountBalanceSheetViewController.h"
#import "MyTransferBalanceCell.h"
#import "MainButton.h"
#import "BankTransferConfirmViewController.h"
#import "CheckAndInitTransferResponse.h"
#import "NewTransfersApi.h"
#import "TransferCurrencyRates.h"
#import "JVFloatLabeledTextField.h"


#define DEF_SOURCE_SUMM_TEXTFIELD_TAG 13
#define DEF_TARGET_SUMM_TEXTFIELD_TAG 14

@interface MyTransfersViewController() {}
@property (weak, nonatomic) IBOutlet TransfersFormDisclaimerCell *convertationDisclamer;
@property (weak, nonatomic) IBOutlet MyTransferBalanceCell *sourceAmountCell;
@property (weak, nonatomic) IBOutlet MyTransferBalanceCell *targetAmountCell;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *templateNameTextField;
@property (weak, nonatomic) IBOutlet UISwitch *saveTemplateSwitch;
@property (weak, nonatomic) IBOutlet MainButton *transferButton;
@end

@implementation MyTransfersViewController {}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MyTransfersModule" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([MyTransfersViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self.output configureStart];

    //When starts from template
    [self.output configureWithTemplate];

    //When starts from charging accounts
    [self.output configureWithTargetAccount];
}

#pragma mark - calls from presenter

/**
 * Go to confirmation
 */
- (void)goToConfirmation:(CheckAndInitTransferResponse *)checkAndInitTransferResponse {
    [self showConfirmationWith:checkAndInitTransferResponse transferType:@"MY_TRANSFERS"];
}

/**
 * Enable and disable transfer button
 */
- (void)setTransferButtonEnabled {
    [self.transferButton setMainButtonStyle:MainButtonStyleOrange];
}

- (void)setTransferButtonDisabled {
    [self.transferButton setMainButtonStyle:MainButtonStyleDisable];
}

/**
 * Show and hide progress HUD
 */
- (void)showPregressHUD:(NSString *)message {
    [super showProgressHUDWithTtitle:message];
}

- (void)hideProgressHUD {
    [super hideProgressHUD];
}

/**
 * Show amount and destination amount
 */
- (void)showAmount:(NSString *)amount andDestinationAmount:(NSString *)destinationAmount {
    self.sourceAmountCell.summTextField.text = amount;
    self.targetAmountCell.summTextField.text = destinationAmount;
}

/**
 * Show warning messages
 */
- (void)showWarningMessages:(NSArray *)warningMessages {
    self.warningMessages = warningMessages;
    [self.tableView reloadData];
}

/**
 * Show currency rate and update time message
 */
- (void)showCurrencyRate:(NSString *)message andUpdateTime:(NSString *)time {
    self.convertationDisclamer.leftDescriptionTextLabel.text = message;
    self.convertationDisclamer.rightDescriptionTextLabel.text = time;

    if (message && time) {
        self.convertationDisclamer.hidden = NO;
    } else {
        self.convertationDisclamer.hidden = YES;
    }

    [self.tableView reloadData];
}

/**
 * Show check transfer's error message
 */
- (void)showErrorMessage:(NSString *)errorMessage {
    [Toast showToast:errorMessage];
}

/**
 * Open account action sheet
 * @param accounts
 */
- (void)sourceAccountSheet:(NSArray *)accounts {
    [self openAccountsActionSheet:accounts tag:0];
}

- (void)targetAccountSheet:(NSArray *)accounts {
    [self openAccountsActionSheet:accounts tag:1];
}

/**
 * Set selected Accounts
 * @param source
 */
- (void)preparedSourceAccount:(NSDictionary *)source {
    self.selectedSourceAccounts = source;
    [self.tableView reloadData];
}

- (void)preparedTargetAccount:(NSDictionary *)target {
    self.selectedTargetAccounts = target;
    [self.tableView reloadData];
}

/**
 * Open balance and currency action sheet
 * @param sourceBalanceAndCurrencies
 */
- (void)sourceBalanceSheet:(NSArray *)sourceBalanceAndCurrencies {
    [self openBalanceAndCurrencyActionSheet:sourceBalanceAndCurrencies tag:0];
}

- (void)targetBalanceSheet:(NSArray *)targetBalanceAndCurrencies {
    [self openBalanceAndCurrencyActionSheet:targetBalanceAndCurrencies tag:1];
}

/**
 * Set selected balance and currency
 */
- (void)preparedSourceBalance:(NSString *)sourceBalance {
    self.selectedSourceBalance = sourceBalance;
    [self.tableView reloadData];
}

- (void)preparedTargetBalance:(NSString *)targetBalance {
    self.selectedTargetBalance = targetBalance;
    [self.tableView reloadData];
}

#pragma mark - config actions

- (IBAction)clickTransferButton:(id)sender {
    if (self.saveTemplateSwitch.isOn && self.templateNameTextField.text.length == 0) {
            return [Toast showToast:@"Для сохранения введите название шаблона"];
    }
    [self.output runInitTransferWithAmount:self.sourceAmountCell.summTextField.text
                                    isSave:self.saveTemplateSwitch.isOn
                              templateName:self.templateNameTextField.text];
}

#pragma mark - TextField delegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == DEF_SOURCE_SUMM_TEXTFIELD_TAG) {
        [self.output runCheckTransferWithAmount:textField.text direction:YES];
    } else if (textField.tag == DEF_TARGET_SUMM_TEXTFIELD_TAG) {
        [self.output runCheckTransferWithAmount:textField.text direction:NO];
    }
}

#pragma mark - Action sheet delegate

- (void)actionSheet:(AccountsActionSheetViewController *)actionSheet didSelectedAccount:(Account *)account {
    //send accounts to presented for preparation
    if (actionSheet.view.tag == 0) {

        [self.output setSelectedSource:account];
        [self.output runCheckTransferWithAmount:self.sourceAmountCell.summTextField.text direction:YES];

    } else if (actionSheet.view.tag == 1) {
        [self.output setSelectedTarget:account];

        if (self.targetAmountCell.summTextField.text.length > 0) {
            [self.output runCheckTransferWithAmount:self.targetAmountCell.summTextField.text direction:NO];
        } else {
            [self.output runCheckTransferWithAmount:self.sourceAmountCell.summTextField.text direction:YES];
        }

    }

    [actionSheet dismissViewControllerAnimated:YES completion:nil];
}

- (void)accountBalanceSheet:(AccountBalanceSheetViewController *)balanceSheet didSelectedBalance:(id)selectedBalance {
    if (balanceSheet.view.tag == 0) {
        [self.output setSelectedSourceBalance:selectedBalance];
    } else if (balanceSheet.view.tag == 1) {
        [self.output setSelectedTargetBalance:selectedBalance];
    }
    [balanceSheet dismissViewControllerAnimated:YES completion:nil];
    if (self.sourceAmountCell.summTextField.text.length > 0) {
        [self.output runCheckTransferWithAmount:self.sourceAmountCell.summTextField.text direction:YES];
    }
}

#pragma mark - on table selection tap

- (void)openSourceAccountSelection {
    [self.output openSourceAccountSelection];
}
- (void)openSourceBalanceSelection {
    [self.output openSourceBalanceCurrencySelection];
}
- (void)openTargetAccountSelection {
    [self.output openTargetAccountSelection];
}
- (void)openTargetBalanceSelection {
    [self.output openTargetBalanceCurrencySelection];
}

#pragma mark - config ui

- (void)configUI {
    [self setNavigationTitle:@"Между своими счетами"];
    [self setNavigationBackButton];
    [self setBafBackground];
    self.tableView.estimatedRowHeight = 100;
    
    //Default transferButton's state is DISABLED
    self.transferButton.mainButtonStyle = MainButtonStyleDisable;

    //Default convertationDisclamer state is hidden
    self.isConvertationDisclamerHidden = self.convertationDisclamer.hidden = YES;
 
}

#pragma mark - config ui

/**
 * Open Accounts Action Sheet popup
 * @param accounts
 * @param tag
 */
- (void)openAccountsActionSheet:(NSArray *)accounts tag:(NSInteger)tag {
    [super openAccountsActionSheet:accounts tag:tag inController:self];
}

/**
 * Open Balance and Currencies Sheet popup
 * @param balanceAndCurrencies
 * @param tag
 */
- (void)openBalanceAndCurrencyActionSheet:(NSArray *)balanceAndCurrencies tag:(NSInteger)tag {
    [super openBalanceAndCurrencyActionSheet:balanceAndCurrencies tag:tag inController:self];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
