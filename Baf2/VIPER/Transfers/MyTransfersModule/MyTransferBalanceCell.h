//
// Created by Askar Mustafin on 5/26/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MyTransferBalanceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *summTextField;
@property (weak, nonatomic) IBOutlet UILabel *summLabel;


@end
