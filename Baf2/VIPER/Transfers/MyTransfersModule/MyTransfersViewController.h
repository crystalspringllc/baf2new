//
// Created by Askar Mustafin on 5/23/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTransfersViewIO.h"
#import "AccountsActionSheetViewController.h"
#import "AccountBalanceSheetViewController.h"
#import "TransfersBaseViewController.h"

@protocol MyTransfersViewOutput;


@interface MyTransfersViewController : TransfersBaseViewController<MyTransfersViewInput,
        AccountsActionSheetDelegate,
        AccountBalanceSheetDelegate, UITextFieldDelegate>

@property (nonatomic, strong) id<MyTransfersViewOutput> output;

+ (instancetype)createVC;

@end