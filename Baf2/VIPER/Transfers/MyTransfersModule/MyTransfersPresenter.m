//
// Created by Askar Mustafin on 5/23/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "MyTransfersPresenter.h"
#import "MyTransfersRouter.h"
#import "OperationAccounts.h"
#import "Accounts.h"
#import "Card.h"
#import "Current.h"
#import "Deposit.h"
#import "NSString+Ext.h"
#import "CheckAndInitTransferResponse.h"
#import "TransferCurrencyRates.h"
#import "TransferTemplate.h"

@interface MyTransfersPresenter() {}
@property (nonatomic, strong) OperationAccounts *operationAccounts;
@property (nonatomic, strong) Account *source;
@property (nonatomic, strong) Account *target;
@property (nonatomic, strong) id sourceBalance;
@property (nonatomic, strong) id targetBalance;
@end


@implementation MyTransfersPresenter {}


#pragma mark - protocol methods

/**
 * Check is tempalte setted
 * @return
 */
- (BOOL)isTemplate {
    return self.transferTemplate ? YES : NO;
}

/**
 * configure from template
 */
- (void)configureWithTemplate {
    if (self.transferTemplate) {
        self.source = self.transferTemplate.requestorAccount;
        self.target = self.transferTemplate.destinationAccount;

        [self setSelectedSource:self.transferTemplate.requestorAccount];
        [self setSelectedTarget:self.transferTemplate.destinationAccount];
        [self.viewInput showAmount:[self.transferTemplate.requestorAmount decimalFormatString]
              andDestinationAmount:[self.transferTemplate.destinationAmount decimalFormatString]];

        [self runCheckTransferWithAmount:[self.transferTemplate.requestorAmount decimalFormatString] direction:YES];
    }
}

/**
 * Configure with account to charge
 */

- (BOOL)isAccountToCharge {
    return self.targetChargeAccount ? YES : NO;
}

- (void)configureWithTargetAccount {
    self.target = self.targetChargeAccount;
    [self setSelectedTarget:self.targetChargeAccount];
}

/**
 * call interactor to make get operation accounts request
 */
- (void)configureStart {
    [self.viewInput showPregressHUD:@"Загрузка счетов"];
    [self.interactorInput prepareAccounts];
}

/**
 * hadlndle loaded operation accounts
 * @param loadedAccounts
 */
- (void)handleLoadedAccounts:(id)loadedAccounts {
    self.operationAccounts = (OperationAccounts *)loadedAccounts;
    [self.viewInput hideProgressHUD];
}

/*
 * Open account picker popup
 */
- (void)openSourceAccountSelection {
    [self.viewInput sourceAccountSheet:[self sourceAccounts]];
}

- (void)openTargetAccountSelection {
    [self.viewInput targetAccountSheet:[self targetAccounts]];
}

/**
 * selected accounts
 */
- (void)setSelectedSource:(Account *)sourceAccount {
    self.source = sourceAccount;
    NSDictionary *preparedAccount = [self prepareSelectedAccounts:sourceAccount];
    [self.viewInput preparedSourceAccount:preparedAccount];

    NSArray *balances = [self getBalancesFromAccaunt:sourceAccount isSource:YES];
    if (balances.count > 0) {
        self.sourceBalance = [balances firstObject];
        NSString *balance = [self prepareBalance:self.sourceBalance];
        [self.viewInput preparedSourceBalance:balance];
    }
}

- (void)setSelectedTarget:(Account *)targetAccount {
    self.target = targetAccount;
    NSDictionary *preparedAccount = [self prepareSelectedAccounts:targetAccount];
    [self.viewInput preparedTargetAccount:preparedAccount];

    NSArray *balances = [self getBalancesFromAccaunt:targetAccount isSource:NO];
    if (balances.count > 0) {
        self.targetBalance = [balances firstObject];
        NSString *balance = [self prepareBalance:self.targetBalance];
        [self.viewInput preparedTargetBalance:balance];
    }
}

/**
 * Open balance picker popup
 */
- (void)openSourceBalanceCurrencySelection {
    NSArray *balances = [self getBalancesFromAccaunt:self.source isSource:YES];
    if (balances) {
        [self.viewInput sourceBalanceSheet:balances];
    }
}

- (void)openTargetBalanceCurrencySelection {
    NSArray *balances = [self getBalancesFromAccaunt:self.target isSource:NO];
    if (balances) {
        [self.viewInput targetBalanceSheet:balances];
    }
}

/**
 * selected balances
 */
- (void)setSelectedSourceBalance:(id)sourceBalance {
    self.sourceBalance = sourceBalance;
    NSString *balance = [self prepareBalance:sourceBalance];
    [self.viewInput preparedSourceBalance:balance];
}

- (void)setSelectedTargetBalance:(id)targetBalance {
    self.targetBalance = targetBalance;
    NSString *balance = [self prepareBalance:targetBalance];
    [self.viewInput preparedTargetBalance:balance];

}

/**
 * Run transfer init
 */
- (void)runInitTransferWithAmount:(NSString *)amountString isSave:(BOOL)isSave templateName:(NSString *)templateName {
    if (self.source && self.sourceBalance[@"currency"] && self.target && self.targetBalance[@"currency"] && amountString.length > 0) {

        [self.viewInput showPregressHUD:@"Подготовка"];

        [self.interactorInput initTransferSourceAccount:self.source
                                selectedSourceCurrrency:self.sourceBalance[@"currency"]
                                          targetAccount:self.target
                                 selectedTargetCurrency:self.targetBalance[@"currency"]
                                                 amount:[amountString numberDecimalFormat]
                                         isSaveTemplate:isSave
                                          templateAlias:templateName
                                              direction:YES];
    }
}

/**
 * Run transfer check
 */
- (void)runCheckTransferWithAmount:(NSString *)amountString direction:(BOOL)direction {
    if (self.source && self.sourceBalance[@"currency"] && self.target && self.targetBalance[@"currency"] && amountString.length > 0) {

        [self.viewInput showPregressHUD:@"Проверка"];

        [self.interactorInput checkTransferSourceAccount:self.source
                                 selectedSourceCurrrency:self.sourceBalance[@"currency"]//todo: create model better
                                           targetAccount:self.target
                                  selectedTargetCurrency:self.targetBalance[@"currency"]
                                                  amount:[amountString numberDecimalFormat]
                                          isSaveTemplate:NO
                                           templateAlias:nil
                                               direction:direction];
    }
}

/**
 * handle init transfer response
 */

- (void)handleInitTransferResponse:(CheckAndInitTransferResponse *)checkTransferResponse {
    [self.viewInput goToConfirmation:checkTransferResponse];
    [self.viewInput hideProgressHUD];
}

/**
 * handle check transfer response & failure
 */
- (void)handleCheckTransferResponse:(CheckAndInitTransferResponse *)checkTransferResponse {
    if (checkTransferResponse.errorMessage.length > 0) {
        [self.viewInput showErrorMessage:checkTransferResponse.errorMessage];
    }

    [self.viewInput showAmount:[checkTransferResponse.requestorAmount decimalFormatString]
          andDestinationAmount:[checkTransferResponse.destinationAmount decimalFormatString]];

    [self.viewInput showCurrencyRate:[checkTransferResponse.exchangeResponse composeCurrencyRateMessage]
                       andUpdateTime:[checkTransferResponse.exchangeResponse composeUpdateTime]];

    [self.viewInput showWarningMessages:checkTransferResponse.warningMessages];


    if (checkTransferResponse.isValid && checkTransferResponse.errorMessage.length == 0) {
        [self.viewInput setTransferButtonEnabled];
    } else {
        [self.viewInput setTransferButtonDisabled];
    }


    [self.viewInput hideProgressHUD];
}

- (void)handleCheckTransferFailure:(NSString *)message {
    [self.viewInput hideProgressHUD];
}

#pragma mark - private methods

- (NSArray *)getBalancesFromAccaunt:(Account *)account isSource:(BOOL)isSource {
    NSMutableArray *balances = [[NSMutableArray alloc] init];
    if (account.isMulty) {
        if (isSource) {
            if ([account.balanceKzt doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceKzt decimalFormatString], @"currency" : @"KZT"}];
            }
            if ([account.balanceUsd doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceUsd decimalFormatString], @"currency" : @"USD"}];
            }
            if ([account.balanceEur doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceEur decimalFormatString], @"currency" : @"EUR"}];
            }
            if ([account.balanceRub doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceRub decimalFormatString], @"currency" : @"RUB"}];
            }
            if ([account.balanceGbp doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceGbp decimalFormatString], @"currency" : @"GBP"}];
            }
        } else {
            [balances addObject:@{@"balance" : [account.balanceKzt decimalFormatString], @"currency" : @"KZT"}];
            [balances addObject:@{@"balance" : [account.balanceUsd decimalFormatString], @"currency" : @"USD"}];
            [balances addObject:@{@"balance" : [account.balanceEur decimalFormatString], @"currency" : @"EUR"}];
            [balances addObject:@{@"balance" : [account.balanceRub decimalFormatString], @"currency" : @"RUB"}];
            [balances addObject:@{@"balance" : [account.balanceGbp decimalFormatString], @"currency" : @"GBP"}];
        }
        return balances;
    } else {
        if (account.balance) {
            [balances addObject:@{@"balance" : [account.balance decimalFormatString], @"currency" : account.currency}];
            return balances;
        } else {
            return nil;
        }
    }
    return nil;
}

- (NSString *)prepareBalance:(id)balance {
    return [NSString stringWithFormat:@"%@ %@", balance[@"balance"], balance[@"currency"]];
}

- (NSDictionary *)prepareSelectedAccounts:(Account *)account {
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    dictionary[@"alias"] = account.alias;
    dictionary[@"number"] = account.number;
    dictionary[@"amount"] = [account balanceAvailableWithCurrency];
    dictionary[@"logo"] = account.logo;
    return dictionary;
}

- (NSArray *)sourceAccounts {
    return [self filterAccounts:self.operationAccounts.sourceAccounts];
}

- (NSArray *)targetAccounts {
    return [self filterAccounts:self.operationAccounts.targetAccounts];
}

- (NSArray *)filterAccounts:(Accounts *)accounts {
    NSMutableArray *filteredSourceAccounts = [[NSMutableArray alloc] init];
    if (accounts.cards.count) {
        NSMutableDictionary *cardsSection = [[NSMutableDictionary alloc] init];
        cardsSection[@"title"] = @"Карты";
        cardsSection[@"rows"] = accounts.cards;
        [filteredSourceAccounts addObject:cardsSection];
    }
    if (accounts.currents.count) {
        NSMutableDictionary *currentsSection = [[NSMutableDictionary alloc] init];
        currentsSection[@"title"] = @"Счета";
        currentsSection[@"rows"] = accounts.currents;
        [filteredSourceAccounts addObject:currentsSection];
    }
    if (accounts.deposits.count) {
        NSMutableDictionary *depositSection = [[NSMutableDictionary alloc] init];
        depositSection[@"title"] = @"Депозиты";
        depositSection[@"rows"] = accounts.deposits;
        [filteredSourceAccounts addObject:depositSection];
    }
    return filteredSourceAccounts;
}

@end
