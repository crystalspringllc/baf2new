//
// Created by Askar Mustafin on 5/23/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyTransfersInteractorIO.h"


@interface MyTransfersInteractor : NSObject<MyTransfersInteractorInput>

@property (nonatomic, weak) id<MyTransfersInteractorOutput> output;

@end