//
// Created by Askar Mustafin on 5/23/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "MyTransfersInteractor.h"
#import "AccountsApi.h"

#import "Accounts.h"
#import "Current.h"
#import "Deposit.h"
#import "NewTransfersApi.h"
#import "OperationAccounts.h"
#import "CheckAndInitTransferResponse.h"


@interface MyTransfersInteractor(){}
@end

@implementation MyTransfersInteractor {}

- (void)prepareAccounts
{
    [AccountsApi getOperationAccountsWithSuccess:^(OperationAccounts *operationAccounts)
    {
        NSMutableArray *sourceCards = operationAccounts.sourceAccounts.cards;
        operationAccounts.sourceAccounts.cards = sourceCards;
        [self.output handleLoadedAccounts:operationAccounts];

    } failure:^(NSString *code, NSString *message) {
        [self.output handleCheckTransferFailure:message];
    }];
}

- (void)checkTransferSourceAccount:(Account *)sourceAccount
        selectedSourceCurrrency:(NSString *)selectedSourceCurrency
                     targetAccount:(Account *)targetAccount
        selectedTargetCurrency:(NSString *)selectedTargetCurrency
                            amount:(NSNumber *)amount
                    isSaveTemplate:(BOOL)isSaveTemplate
                             templateAlias:(NSString *)templateAlias
                         direction:(BOOL)direction
{
    [NewTransfersApi checkTransfer:MY_TRANSFERS
                       requestorId:sourceAccount.accountId
                   requestorAmount:amount
                     destinationId:targetAccount.accountId
                     requestorType:sourceAccount.type
                 requestorCurrency:selectedSourceCurrency
                   destinationType:targetAccount.type
               destinationCurrency:selectedTargetCurrency
                           knpCode:nil
                           knpName:nil
                         direction:direction
                              save:isSaveTemplate
                             alias:templateAlias
                           success:^(id response) {

                                CheckAndInitTransferResponse *checkTransferResponse = [CheckAndInitTransferResponse instanceFromDictionary:response];
                                [self.output handleCheckTransferResponse:checkTransferResponse];

                           } failure:^(NSString *code, NSString *message) {
                                [self.output handleCheckTransferFailure:message];
            }];
}

- (void)initTransferSourceAccount:(Account *)sourceAccount
        selectedSourceCurrrency:(NSString *)selectedSourceCurrency
                  targetAccount:(Account *)targetAccount
         selectedTargetCurrency:(NSString *)selectedTargetCurrency
                         amount:(NSNumber *)amount
                 isSaveTemplate:(BOOL)isSaveTemplate
                  templateAlias:(NSString *)templateAlias
                      direction:(BOOL)direction
{
    [NewTransfersApi initTransfer:MY_TRANSFERS
                      requestorId:sourceAccount.accountId
                  requestorAmount:amount
                    destinationId:targetAccount.accountId
                    requestorType:sourceAccount.type
                requestorCurrency:selectedSourceCurrency
                  destinationType:targetAccount.type
              destinationCurrency:selectedTargetCurrency
                          knpCode:nil
                          knpName:nil
                        direction:direction
                             save:isSaveTemplate
                            alias:templateAlias
                       clientDesc:nil
                          success:^(id response) {

                              CheckAndInitTransferResponse *checkTransferResponse = [CheckAndInitTransferResponse instanceFromDictionary:response];
                              [self.output handleInitTransferResponse:checkTransferResponse];

                          } failure:^(NSString *code, NSString *message) {
                [self.output handleCheckTransferFailure:message];
            }];
}

@end
