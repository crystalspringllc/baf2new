#import "CheckAndInitTransferResponse.h"
#import "Account.h"
#import "CheckTransferWarningMessages.h"
#import "TransferCurrencyRates.h"

@implementation CheckAndInitTransferResponse

@synthesize amount;
@synthesize clientId;
@synthesize currency;
@synthesize destinationAccount;
@synthesize destinationAmount;
@synthesize destinationCurrency;
@synthesize destinationId;
@synthesize destinationType;
@synthesize direction;
@synthesize errorMessage;
@synthesize requestorAccount;
@synthesize requestorCurrency;
@synthesize requestorId;
@synthesize requestorType;
@synthesize session;
@synthesize type;
@synthesize comission;
@synthesize comissionCurrency;
@synthesize exchangeResponse;
@synthesize isValid;
@synthesize knpList;
@synthesize operdate;
@synthesize requestorAmount;
@synthesize safetyLevel;
@synthesize warningMessages;
@synthesize isPinVerifyRequired;
@synthesize uuid;
@synthesize servicelogId;
@synthesize save;

+ (CheckAndInitTransferResponse *)instanceFromDictionary:(NSDictionary *)aDictionary {

    CheckAndInitTransferResponse *instance = [[CheckAndInitTransferResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.amount = [self numberValueForKey:@"amount"];
    self.clientId = [self numberValueForKey:@"clientId"];
    self.currency = [self stringValueForKey:@"currency"];
    self.destinationAccount = [Account instanceFromDictionary:objectData[@"destinationAccount"]];
    self.destinationAmount = [self numberValueForKey:@"destinationAmount"];
    self.destinationCurrency = [self stringValueForKey:@"destinationCurrency"];
    self.destinationId = [self numberValueForKey:@"destinationId"];
    self.destinationType = [self stringValueForKey:@"destinationType"];
    self.direction = [self stringValueForKey:@"direction"];
    self.errorMessage = [self stringValueForKey:@"errorMessage"];
    self.requestorAccount = [Account instanceFromDictionary:objectData[@"requestorAccount"]];
    self.requestorCurrency = [self stringValueForKey:@"requestorCurrency"];
    self.requestorId = [self numberValueForKey:@"requestorId"];
    self.requestorType = [self stringValueForKey:@"requestorType"];
    self.session = [self stringValueForKey:@"session"];
    self.type = [self stringValueForKey:@"type"];

    self.comission = [self numberValueForKey:@"comission"];
    self.comissionCurrency = [self numberValueForKey:@"comissionCurrency"];
    self.exchangeResponse = [TransferCurrencyRates instanceFromDictionary:objectData[@"exchange"]];
    self.isValid = [self boolValueForKey:@"isValid"];
    self.knpList = [self stringValueForKey:@"knpList"];//array
    self.operdate = [self stringValueForKey:@"operdate"];
    self.requestorAmount = [self numberValueForKey:@"requestorAmount"];
    self.safetyLevel = [self stringValueForKey:@"safetyLevel"];
    self.warningMessages = [self arrayValueForKey:@"warningMessages"];
    self.isPinVerifyRequired = [self boolValueForKey:@"isPinVerifyRequired"];
    self.uuid = [self stringValueForKey:@"uuid"];
    self.servicelogId = [self numberValueForKey:@"servicelogId"];
    self.save = [self boolValueForKey:@"save"];

    objectData = nil;
}

@end