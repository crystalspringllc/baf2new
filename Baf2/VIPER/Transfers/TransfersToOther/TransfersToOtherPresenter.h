//
// Created by Askar Mustafin on 5/31/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransfersToOtherInteractorIO.h"
#import "TransfersToOtherViewIO.h"
@class TransfersToOtherRouter;
@class TransferTemplate;


@interface TransfersToOtherPresenter : NSObject<TransfersToOtherInteractorOutput, TransfersToOtherViewOutput>

@property (nonatomic, strong) id <TransfersToOtherInteractorInput> interactorInput;
@property (nonatomic, strong) id <TransfersToOtherViewInput> viewInput;

@property (nonatomic, strong) TransfersToOtherRouter *transfersToOtherRouter;

@property (nonatomic, strong) TransferTemplate *transferTemplate;
@end