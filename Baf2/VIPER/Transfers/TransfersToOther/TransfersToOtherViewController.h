//
// Created by Askar Mustafin on 5/31/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransfersToOtherViewIO.h"
#import "TransfersBaseViewController.h"


@interface TransfersToOtherViewController : TransfersBaseViewController<TransfersToOtherViewInput>

@property (nonatomic, weak) id <TransfersToOtherViewOutput> output;

+ (instancetype)createVC;

@end