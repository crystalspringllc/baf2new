//
// Created by Askar Mustafin on 5/31/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TransfersToOtherPresenter;


@interface TransfersToOtherRouter : NSObject

@property (nonatomic, strong) TransfersToOtherPresenter *transfersToOtherPresenter;

- (void)pushTransfersToOtherModule:(UINavigationController *)navigationController;

@end