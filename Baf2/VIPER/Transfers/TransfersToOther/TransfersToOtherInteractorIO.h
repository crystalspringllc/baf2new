//
// Created by Askar Mustafin on 5/31/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Account;
@class CheckAndInitTransferResponse;
@class OperationAccounts;


@protocol TransfersToOtherInteractorOutput <NSObject>


/**
 * Operation accounts loaded
 */
- (void)accountsPrepared;

/**
 * Accounts list prepared
 * @param sourceAccounts
 */
- (void)sourceAccountsPrepared:(NSArray *)sourceAccounts;
- (void)targetAccountsPrepared:(NSArray *)targetAccounts;

/**
 * Balances list prepared
 */
- (void)sourceBalancesPrepared:(NSArray *)sourceBalances;
- (void)targetBalancesPrepared:(NSArray *)sourceBalances;

/**
 * Selected accounts prepared
 */
- (void)selectedSourceAccountPrepared:(NSDictionary *)sourceAccount;
- (void)selectedTargetAccountPrepared:(NSDictionary *)sourceAccount;

/**
 * Selected balance prepared
 */
- (void)sourceBalancePrepared:(NSString *)balance;
- (void)targetBalancePrepared:(NSString *)balance;

/**
 * Handle check response
 */
- (void)handleCheckResponseWithSourceAmount:(NSString *)sourceAmount
                               targetAmount:(NSString *)targetAmount
                                currMessage:(NSString *)currMessage
                             currUpdateTime:(NSString *)currUpdateTime
                            warningMessages:(NSString *)waringMessages
                            isValidTransfer:(BOOL)isValidTransfer;

- (void)handleCheckTransferFailure:(NSString *)message;

- (void)showCheckResponseErrorMessage:(NSString *)errorMessage;

- (void)showCheckTransferProgressHUD;

/**
 * Handle init transfer response
 */
- (void)handleInitTransferResponse:(CheckAndInitTransferResponse *)checkTransferResponse;

/**
 * Send operation account to view
 */
- (void)sendOpeationAccount:(OperationAccounts *)operationAccounts;

@end

@protocol TransfersToOtherInteractorInput <NSObject>

/**
 * Load all accounts
 */
- (void)prepareAccounts;

/**
 * Check transfers
 */
- (void)checkTransferWithAmount:(NSNumber *)amount
                      direction:(BOOL)direction
                 isSaveTemplate:(BOOL)isSaveTemplate
                  templateAlias:(NSString *)templateAlias;

- (void)initTransferAmount:(NSNumber *)amount
            isSaveTemplate:(BOOL)isSaveTemplate
             templateAlias:(NSString *)templateAlias;

/**
 * Prepare lists for picker popup
 */
- (void)prepareSourceAccountList;
- (void)prepareSourceBalanceList;
- (void)prepareTargetAccountList;
- (void)prepareTargetBalanceList;

/**
 * Prepare selected account
 */
- (void)prepareSelectedSource:(Account *)account;
- (void)prepareSelectedTarget:(Account *)account;

/**
 * Prepare selected balance
 */
- (void)prepareSelectedSourceBalance:(id)balance;
- (void)prepareSelectedTargetBalance:(id)balance;

/**
 * View asking for operationAccaounts
 */
- (id)askOperationAccounts;

@end