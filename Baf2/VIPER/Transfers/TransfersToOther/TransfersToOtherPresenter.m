//
// Created by Askar Mustafin on 5/31/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "TransfersToOtherPresenter.h"
#import "TransfersToOtherRouter.h"
#import "NSString+Ext.h"
#import "TransferTemplate.h"


@implementation TransfersToOtherPresenter {}

#pragma mark - output from view

- (void)closeModule {
    self.transfersToOtherRouter = nil;
}

- (void)configureStart {
    [self.viewInput showProgressHUD:@"Загрузка счетов"];
    [self.interactorInput prepareAccounts];
}

- (void)configureWithTemplate {
    if (self.transferTemplate) {
        [self.interactorInput prepareSelectedSource:self.transferTemplate.requestorAccount];
        [self.interactorInput prepareSelectedTarget:self.transferTemplate.destinationAccount];
        [self setSelectedSource:self.transferTemplate.requestorAccount];
        [self setSelectedTarget:self.transferTemplate.destinationAccount];
        [self.viewInput showSourceAmount:[self.transferTemplate.requestorAmount decimalFormatString]];
        [self.viewInput showTargetAmount:[self.transferTemplate.destinationAmount decimalFormatString]];
        [self runCheckTransferWithAmount:[self.transferTemplate.requestorAmount decimalFormatString] direction:YES];
    }
}

/**
 * Check transfer
 */
- (void)runCheckTransferWithAmount:(NSString *)amount direction:(BOOL)direction {
    [self.interactorInput checkTransferWithAmount:[amount numberDecimalFormat]
                                            direction:direction
                                       isSaveTemplate:NO
                                        templateAlias:nil];
}

- (void)runInitTransferWithAmount:(NSString *)amount
                           isSave:(BOOL)isSave
                     templateName:(NSString *)templateName
{
    [self.viewInput showProgressHUD:@"Подготовка"];
    [self.interactorInput initTransferAmount:[amount numberDecimalFormat]
                              isSaveTemplate:isSave
                               templateAlias:templateName];
}

/**
 * On open source/target account and balance
 */
- (void)openSourceAccountPopup {
    NSLog(@"open source account");
    [self.interactorInput prepareSourceAccountList];
}

- (void)openSourceBalancePopup {
    NSLog(@"open source balance");
    [self.interactorInput prepareSourceBalanceList];
}

- (void)openTargetAccountPopup {
    NSLog(@"open target account");
    [self.interactorInput prepareTargetAccountList];
}

- (void)openTargetBalancePopup {
    NSLog(@"open target balance");
    [self.interactorInput prepareTargetBalanceList];
}

/**
 * Set Account to Interactor for handling
 */
- (void)setSelectedSource:(Accounts *)account {
    [self.interactorInput prepareSelectedSource:account];
}

- (void)setSelectedTarget:(Accounts *)account {
    [self.interactorInput prepareSelectedTarget:account];
}

/**
 * Set selected balance
 */
- (void)setSelectedSourceBalance:(id)balance {
    [self.interactorInput prepareSelectedSourceBalance:balance];
}

- (void)setSelectedTargetBalance:(id)balance {
    [self.interactorInput prepareSelectedTargetBalance:balance];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark - output from interactor

- (void)accountsPrepared {
    [self.viewInput hideProgressHUD];
}

#pragma mark - HANDLE CHECK TRANSFER RESPONSE

- (void)handleCheckResponseWithSourceAmount:(NSString *)sourceAmount
                               targetAmount:(NSString *)targetAmount
                                currMessage:(NSString *)currMessage
                             currUpdateTime:(NSString *)currUpdateTime
                            warningMessages:(NSString *)waringMessages
                            isValidTransfer:(BOOL)isValidTransfer
{
    [self.viewInput showSourceAmount:sourceAmount];
    [self.viewInput showTargetAmount:targetAmount];

    [self.viewInput showCurrencyMessage:currMessage];
    [self.viewInput showCurrencyUpdateTime:currUpdateTime];
    [self.viewInput showWarningMessages:waringMessages];

    if (isValidTransfer) {
        [self.viewInput setTransferButtonEnabled];
    } else {
        [self.viewInput setTransferButtonDisabled];
    }

    if (currMessage.length > 0 && currUpdateTime) {
        [self.viewInput hideConvertationDisclamer:NO];
    } else {
        [self.viewInput hideConvertationDisclamer:YES];
    }

    [self.viewInput hideProgressHUD];
}

- (void)handleCheckTransferFailure:(NSString *)message {
    [self.viewInput hideProgressHUD];
}

- (void)showCheckResponseErrorMessage:(NSString *)errorMessage {
    [self.viewInput showErrorMessage:errorMessage];
}

- (void)showCheckTransferProgressHUD {
    [self.viewInput showProgressHUD:@"Проверка"];
}

/**
 * Handle init transfer response
 */

- (void)handleInitTransferResponse:(CheckAndInitTransferResponse *)checkTransferResponse {
    [self.viewInput goToConfirmation:checkTransferResponse];
    [self.viewInput hideProgressHUD];
}

#pragma mark - LIST FOR SHOW IN POPUP SHEET
/**
 * Accounts list prepared to show on Action sheet
 * @param sourceAccounts
 */
- (void)sourceAccountsPrepared:(NSArray *)sourceAccounts {
    [self.viewInput presentSourceAccounts:sourceAccounts];
}

- (void)targetAccountsPrepared:(NSArray *)targetAccounts {
    [self.viewInput presentTargetAccounts:targetAccounts];
}

- (void)sourceBalancesPrepared:(NSArray *)sourceBalances {
    [self.viewInput presentSourceBalances:sourceBalances];
}

- (void)targetBalancesPrepared:(NSArray *)targetBalances {
    [self.viewInput presentTargetBalances:targetBalances];
}

#pragma mark - SELECTED ACCOUNT OR BALANCE
/**
 * Selected account prepared to show in tableview
 * @param sourceAccount
 */
- (void)selectedSourceAccountPrepared:(NSDictionary *)sourceAccount {
    [self.viewInput preparedSourceAccount:sourceAccount];
}

- (void)selectedTargetAccountPrepared:(NSDictionary *)sourceAccount {
    [self.viewInput preparedTargetAccount:sourceAccount];
}

/**
 *
 * @param show balance after selection
 */
- (void)sourceBalancePrepared:(NSString *)balance {
    [self.viewInput preparedSourceBalance:balance];
}

- (void)targetBalancePrepared:(NSString *)balance {
    [self.viewInput preparedTargetBalance:balance];
}

/**
 * VIEW ASKING INTERACTOR TO PROVIDE OPERATION ACCOUNTS
 */
- (id)requestOperationAccounts {
    return [self.interactorInput askOperationAccounts];
}

#pragma mark - dealloc

- (void)dealloc {
    NSLog(@"TransfersToOtherPresenter dealloced");
}


@end
