//
// Created by Askar Mustafin on 5/31/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TransferTemplate;


@interface TransfersToOtherDependencies : NSObject

@property (nonatomic, strong) TransferTemplate *transferTemplate;

- (void)installRootViewController;

@end