//
// Created by Askar Mustafin on 5/31/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "TransfersToOtherInteractor.h"
#import "TransfersToOtherViewIO.h"
#import "AccountsApi.h"
#import "NSString+Ext.h"
#import "NSDate+Ext.h"
#import "Accounts.h"
#import "OperationAccounts.h"
#import "NewTransfersApi.h"
#import "CheckAndInitTransferResponse.h"
#import "TransferCurrencyRates.h"

@interface TransfersToOtherInteractor() {}
@property (nonatomic, strong) OperationAccounts *operationAccounts;
@property (nonatomic, strong) Account *selectedSourceAccount;
@property (nonatomic, strong) Account *selectedTargetAccount;
@property (nonatomic, strong) NSDictionary *sourceBalance;
@property (nonatomic, strong) NSDictionary *targetBalance;
@end

@implementation TransfersToOtherInteractor {}

#pragma mark - api requests

- (void)prepareAccounts
{
    [AccountsApi getOperationAccountsWithSuccess:^(OperationAccounts *operationAccounts)
    {
        self.operationAccounts = operationAccounts;
        [self.output accountsPrepared];

    } failure:^(NSString *code, NSString *message) {
        [self.output accountsPrepared];
    }];
}

- (void)checkTransferWithAmount:(NSNumber *)amount
                      direction:(BOOL)direction
                 isSaveTemplate:(BOOL)isSaveTemplate
                  templateAlias:(NSString *)templateAlias
{
    if (self.selectedSourceAccount
            && self.selectedTargetAccount
            && self.sourceBalance
            && amount
            && self.targetBalance)
    {

        [self.output showCheckTransferProgressHUD];

        [NewTransfersApi checkTransfer:TRANSFERS_TO_OTHER
                           requestorId:self.selectedSourceAccount.accountId
                       requestorAmount:amount
                         destinationId:self.selectedTargetAccount.accountId
                         requestorType:self.selectedSourceAccount.type
                     requestorCurrency:self.sourceBalance[@"currency"]
                       destinationType:self.selectedTargetAccount.type
                   destinationCurrency:self.targetBalance[@"currency"]
                               knpCode:nil
                               knpName:nil
                             direction:direction
                                  save:isSaveTemplate
                                 alias:templateAlias
                               success:^(id response) {

                                   CheckAndInitTransferResponse *checkTransferResponse = [CheckAndInitTransferResponse instanceFromDictionary:response];
                                    [self prepareCheckTransferResponse:checkTransferResponse];

                               } failure:^(NSString *code, NSString *message) {
                    [self.output handleCheckTransferFailure:message];
                }];

    }
}

- (void)initTransferAmount:(NSNumber *)amount
            isSaveTemplate:(BOOL)isSaveTemplate
             templateAlias:(NSString *)templateAlias

{
    [NewTransfersApi initTransfer:TRANSFERS_TO_OTHER
                      requestorId:self.selectedSourceAccount.accountId
                  requestorAmount:amount
                    destinationId:self.selectedTargetAccount.accountId
                    requestorType:self.selectedSourceAccount.type
                requestorCurrency:self.sourceBalance[@"currency"]
                  destinationType:self.selectedTargetAccount.type
              destinationCurrency:self.targetBalance[@"currency"]
                          knpCode:nil
                          knpName:nil
                        direction:YES
                             save:isSaveTemplate
                            alias:templateAlias
                       clientDesc:nil
                          success:^(id response) {

                              CheckAndInitTransferResponse *checkTransferResponse = [CheckAndInitTransferResponse instanceFromDictionary:response];
                              [self.output handleInitTransferResponse:checkTransferResponse];

                          } failure:^(NSString *code, NSString *message) {
                [self.output handleCheckTransferFailure:message];
            }];
}

/**
 * On account and balance selection
 */

- (void)prepareSourceAccountList {
    NSArray *sourceAccountPrimitives = [self filterAccounts:self.operationAccounts.sourceAccounts isSource:YES];
    [self.output sourceAccountsPrepared:sourceAccountPrimitives];
}

- (void)prepareSourceBalanceList {
    if (!self.selectedSourceAccount) {
        return;
    }
    NSArray *balances = [self getBalancesFromAccaunt:self.selectedSourceAccount isSource:YES];
    [self.output sourceBalancesPrepared:balances];
}

- (void)prepareTargetAccountList {
    NSArray *targetAccountPrimitives = [self filterAccounts:self.operationAccounts.targetAccounts isSource:NO];
    [self.output targetAccountsPrepared:targetAccountPrimitives];
}

- (void)prepareTargetBalanceList {
    if (!self.selectedTargetAccount) {
        return;
    }
    NSArray *balances = [self getBalancesFromAccaunt:self.selectedTargetAccount isSource:NO];
    [self.output targetBalancesPrepared:balances];
}

/**
 * Prepare selected account and balance
 */
- (void)prepareSelectedSource:(Account *)account {
    self.selectedSourceAccount = account;
    NSDictionary *preparedAccount = [self prepareSelectedAccounts:account];
    [self.output selectedSourceAccountPrepared:preparedAccount];

    NSArray *balances = [self getBalancesFromAccaunt:account isSource:YES];
    if (balances.count > 0) {
        self.sourceBalance = [balances firstObject];
        NSString *balance = [self prepareBalance:self.sourceBalance];
        [self.output sourceBalancePrepared:balance];
    }
}

- (void)prepareSelectedTarget:(Account *)account {
    self.selectedTargetAccount = account;
    NSDictionary *preparedAccount = [self prepareSelectedAccounts:account];
    [self.output selectedTargetAccountPrepared:preparedAccount];

    NSArray *balances = [self getBalancesFromAccaunt:account isSource:NO];
    if (balances.count > 0) {
        self.targetBalance = [balances firstObject];
        NSString *balance = [self prepareBalance:self.targetBalance];
        [self.output targetBalancePrepared:balance];
    }
}

/**
 * Prepare selected balance
 */
- (void)prepareSelectedSourceBalance:(id)balance {
    self.sourceBalance = balance;
    [self.output sourceBalancePrepared:[self prepareBalance:self.sourceBalance]];
}

- (void)prepareSelectedTargetBalance:(id)balance {
    self.targetBalance = balance;
    [self.output targetBalancePrepared:[self prepareBalance:self.targetBalance]];
}

#pragma mark - private helper methods

- (void)prepareCheckTransferResponse:(CheckAndInitTransferResponse *)checkTransferResponse {

    NSString *sourceAmount = [checkTransferResponse.requestorAmount decimalFormatString];
    NSString *targetAmount = [checkTransferResponse.destinationAmount decimalFormatString];
    NSString *currMessage = [checkTransferResponse.exchangeResponse composeCurrencyRateMessage];
    NSString *currUpdateTime = [checkTransferResponse.exchangeResponse composeUpdateTime];
    NSArray *warningMessages = checkTransferResponse.warningMessages;

    [self.output handleCheckResponseWithSourceAmount:sourceAmount
                                        targetAmount:targetAmount
                                         currMessage:currMessage
                                      currUpdateTime:currUpdateTime
                                     warningMessages:warningMessages
                                     isValidTransfer:checkTransferResponse.isValid];

    if (checkTransferResponse.errorMessage.length > 0) {
        [self.output showCheckResponseErrorMessage:checkTransferResponse.errorMessage];
    }
}


/**
 * Prepare operation accounts to be sent to presenter
 * @param operationAccounts
 */

- (id)askOperationAccounts {
    return self.operationAccounts;
}

/**
 * Get balances from account
 * @param account
 * @param isSource
 * @return
 */
- (NSArray *)getBalancesFromAccaunt:(Account *)account isSource:(BOOL)isSource {
    NSMutableArray *balances = [[NSMutableArray alloc] init];
    if (account.isMulty) {
        if (isSource) {
            if ([account.balanceKzt doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceKzt decimalFormatString], @"currency" : @"KZT"}];
            }
            if ([account.balanceUsd doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceUsd decimalFormatString], @"currency" : @"USD"}];
            }
            if ([account.balanceEur doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceEur decimalFormatString], @"currency" : @"EUR"}];
            }
            if ([account.balanceRub doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceRub decimalFormatString], @"currency" : @"RUB"}];
            }
            if ([account.balanceGbp doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceGbp decimalFormatString], @"currency" : @"GBP"}];
            }
        } else {
            [balances addObject:@{@"balance" : @"", @"currency" : @"KZT"}];
            [balances addObject:@{@"balance" : @"", @"currency" : @"USD"}];
            [balances addObject:@{@"balance" : @"", @"currency" : @"EUR"}];
            [balances addObject:@{@"balance" : @"", @"currency" : @"RUB"}];
            [balances addObject:@{@"balance" : @"", @"currency" : @"GBP"}];
        }
        return balances;
    } else {
        if (account.balance) {
            [balances addObject:@{@"balance" : [account.balance decimalFormatString], @"currency" : account.currency}];
            return balances;
        } else {
            return nil;
        }
    }
    return nil;
}

/**
 * Prepare Balance
 * @param balance
 * @return
 */
- (NSString *)prepareBalance:(id)balance {
    return [NSString stringWithFormat:@"%@ %@", balance[@"balance"], balance[@"currency"]];
}

/**
 * Prepare Selected Accounts
 * @param account
 * @return
 */
- (NSDictionary *)prepareSelectedAccounts:(Account *)account {
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    dictionary[@"alias"] = account.alias;
    dictionary[@"number"] = account.number;
    dictionary[@"amount"] = [account balanceAvailableWithCurrency];
    dictionary[@"logo"] = account.logo;
    return dictionary;
}


/**
 * Filter accounts for presenter
 * @param accounts
 * @return array of account primitive data
 */
/**
 * !!! CHECKING ON CLIENT. IN FUTURE MIGTH BE ON SERVER
 */
- (NSArray *)filterAccounts:(Accounts *)accounts isSource:(BOOL)isSource {
    NSMutableArray *filteredSourceAccounts = [[NSMutableArray alloc] init];
    if (isSource) {
        if (accounts.cards.count) {
            NSMutableDictionary *cardsSection = [[NSMutableDictionary alloc] init];
            cardsSection[@"title"] = @"Карты";
            cardsSection[@"rows"] = accounts.cards;
            [filteredSourceAccounts addObject:cardsSection];
        }
        if (accounts.currents.count) {
            NSMutableDictionary *currentsSection = [[NSMutableDictionary alloc] init];
            currentsSection[@"title"] = @"Счета";
            currentsSection[@"rows"] = accounts.currents;
            [filteredSourceAccounts addObject:currentsSection];
        }
        if (accounts.deposits.count) {
            NSMutableDictionary *depositSection = [[NSMutableDictionary alloc] init];
            depositSection[@"title"] = @"Депозиты";
            depositSection[@"rows"] = accounts.deposits;
            [filteredSourceAccounts addObject:depositSection];
        }
    } else {
        if (accounts.beneficiars.count) {

            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isInterbank == NO"];
            NSArray *beneficiarsForTransfersToOther = [accounts.beneficiars filteredArrayUsingPredicate:predicate];

            NSMutableDictionary *beneficiarsSection = [[NSMutableDictionary alloc] init];
            beneficiarsSection[@"title"] = @"Счет зачисления";
            beneficiarsSection[@"rows"] = beneficiarsForTransfersToOther;
            [filteredSourceAccounts addObject:beneficiarsSection];
        }
    }
    return filteredSourceAccounts;
}

#pragma mark - dealloc

- (void)dealloc {
    NSLog(@"TransfersToOtherInteractor dealloced");
}

@end
