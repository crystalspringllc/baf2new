//
// Created by Askar Mustafin on 5/31/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Accounts;
@class CheckAndInitTransferResponse;


@protocol TransfersToOtherViewOutput <NSObject>

- (void)closeModule;

- (void)configureWithTemplate;

- (void)configureStart;

- (void)runCheckTransferWithAmount:(NSString *)amount
                         direction:(BOOL)direction;

- (void)runInitTransferWithAmount:(NSString *)text
                           isSave:(BOOL)isSave
                     templateName:(NSString *)templateName;

/**
 * Check if template setted
 */
- (BOOL)isTemplate;


/**
 * Open account/balance popups
 */
- (void)openSourceAccountPopup;
- (void)openSourceBalancePopup;
- (void)openTargetAccountPopup;
- (void)openTargetBalancePopup;


/**
 * breaking viper rule : View and Presenter knows abount Entity
 * @param account
 */
- (void)setSelectedSource:(Accounts *)account;
- (void)setSelectedTarget:(Accounts *)account;


/**
 * set selected balance
 */
- (void)setSelectedSourceBalance:(id)balance;
- (void)setSelectedTargetBalance:(id)balance;


/**
 * ASK FOR OPERATION ACCOUNTS
 * !!VIOLATION OF VIREP RULES!!
 */
- (id)requestOperationAccounts;

@end

////////////////////////////////////////////////////////////////////////////

@protocol TransfersToOtherViewInput <NSObject>

/**
 * Go to transfer confirmation
 */
- (void)goToConfirmation:(CheckAndInitTransferResponse *)checkAndInitTransferResponse;

#pragma mark - TRANSFER BUTTON STATE

- (void)setTransferButtonEnabled;
- (void)setTransferButtonDisabled;

#pragma mark - ERROR message

- (void)showErrorMessage:(NSString *)showErrorMessage;

#pragma mark - HANDLE CHECK TRANSFER RESPONSE

/**
 * Handle check transfer resposne
 */
- (void)showSourceAmount:(NSString *)sourceAmount;
- (void)showTargetAmount:(NSString *)targetAmount;
- (void)showCurrencyMessage:(NSString *)currencyMessage;
- (void)showCurrencyUpdateTime:(NSString *)currencyUpateTime;
- (void)showWarningMessages:(NSString *)warningMessages;
- (void)hideConvertationDisclamer:(BOOL)isHideDisclamer;

#pragma mark - PROGRESS HUD SHOW / HIDE

/**
 * Show and hide progress view
 */
- (void)showProgressHUD:(NSString *)message;
- (void)hideProgressHUD;

#pragma mark - LIST FOR SHOW IN POPUP SHEET

/**
 * Present source and target accounts
 */
- (void)presentSourceAccounts:(NSArray *)sourceAccounts;
- (void)presentTargetAccounts:(NSArray *)targetAccounts;

/**
 * Present source and target balance
 */
- (void)presentSourceBalances:(NSArray *)sourceBalances;
- (void)presentTargetBalances:(NSArray *)targetBalances;


#pragma mark - SELECTED ACCOUNT OR BALANCE
/**
 *
 * @param source
 */
- (void)preparedSourceAccount:(NSDictionary *)source;
- (void)preparedTargetAccount:(NSDictionary *)target;

/**
 * prepare balance and currency for tableview
 */
- (void)preparedSourceBalance:(NSString *)sourceBalance;
- (void)preparedTargetBalance:(NSString *)targetBalance;

@end