//
// Created by Askar Mustafin on 5/31/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransfersToOtherInteractorIO.h"


@interface TransfersToOtherInteractor : NSObject<TransfersToOtherInteractorInput>
@property (nonatomic, weak) id<TransfersToOtherInteractorOutput> output;
@end