//
// Created by Askar Mustafin on 5/31/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "TransfersToOtherViewController.h"
#import "TransfersFormDisclaimerCell.h"
#import "TransfersFormCell.h"
#import "MyTransferBalanceCell.h"
#import "AccountsActionSheetViewController.h"
#import "AccountBalanceSheetViewController.h"
#import "UIViewController+NavigationController.h"
#import "MainButton.h"
#import "JVFloatLabeledTextField.h"
#import "SearchBeneficiaryViewController.h"
#import "EditBankTransferBeneficiaryAccountsViewController.h"
#import "OperationAccounts.h"
#import "Accounts.h"
#import "NotificationCenterHelper.h"

#define DEF_SOURCE_SUMM_TEXTFIELD_TAG 13
#define DEF_TARGET_SUMM_TEXTFIELD_TAG 14

@interface TransfersToOtherViewController()<UITextFieldDelegate> {}
@property (weak, nonatomic) IBOutlet TransfersFormDisclaimerCell *convertationDisclamer;
@property (weak, nonatomic) IBOutlet MyTransferBalanceCell *sourceAmountCell;
@property (weak, nonatomic) IBOutlet MyTransferBalanceCell *targetAmountCell;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *templateNameTextField;
@property (weak, nonatomic) IBOutlet UISwitch *saveTemplateSwitch;
@property (weak, nonatomic) IBOutlet MainButton *transferButton;
@end

@implementation TransfersToOtherViewController {}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TransfersToOther" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([TransfersToOtherViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

    [self.output configureWithTemplate];

    [self.output configureStart];
    
}

#pragma mark - calls from presenter

/**
 * Go to confirmation
 */
- (void)goToConfirmation:(CheckAndInitTransferResponse *)checkAndInitTransferResponse {
    [self showConfirmationWith:checkAndInitTransferResponse
                  transferType:@"TRANSFERS_TO_OTHER"];
}

/**
 * Toggle transfer button state
 */
- (void)setTransferButtonEnabled
{
    [self.transferButton setMainButtonStyle:MainButtonStyleOrange];
}

- (void)setTransferButtonDisabled
{
    [self.transferButton setMainButtonStyle:MainButtonStyleDisable];
}

/**
 * Show error message
 * @param showErrorMessage
 */
- (void)showErrorMessage:(NSString *)errorMessage
{
    [Toast showToast:errorMessage];
}

/**
 * Handle check transfer resposne
 */
- (void)showSourceAmount:(NSString *)sourceAmount
{
    self.sourceAmountCell.summTextField.text = sourceAmount;
}

- (void)showTargetAmount:(NSString *)targetAmount
{
    self.targetAmountCell.summTextField.text = targetAmount;
}

- (void)showCurrencyMessage:(NSString *)currencyMessage
{
    self.convertationDisclamer.leftDescriptionTextLabel.text = currencyMessage;
}

- (void)showCurrencyUpdateTime:(NSString *)currencyUpateTime
{
    self.convertationDisclamer.rightDescriptionTextLabel.text = currencyUpateTime;
}

- (void)hideConvertationDisclamer:(BOOL)isHideDisclamer {
    self.isConvertationDisclamerHidden = isHideDisclamer;
    self.convertationDisclamer.hidden = isHideDisclamer;
    [self.tableView reloadData];
}

- (void)showWarningMessages:(NSString *)warningMessages
{
    self.warningMessages = warningMessages;
    [self.tableView reloadData];
}

/**
 * Show and hide progress HUD
 */
- (void)showProgressHUD:(NSString *)message {
    [super showProgressHUDWithTtitle:message];
}

- (void)hideProgressHUD {
    [super hideProgressHUD];
}

#pragma mark - LIST FOR SHOW IN POPUP SHEET
/**
 * Show source and target account list in action sheet popup
 */
- (void)presentSourceAccounts:(NSArray *)sourceAccounts {
    [self openAccountsActionSheet:sourceAccounts tag:0];
}

- (void)presentTargetAccounts:(NSArray *)targetAccounts {
    [self openAccountsActionSheet:targetAccounts tag:1];
}

/**
 * Show balance in action sheet popup
 */
- (void)presentSourceBalances:(NSArray *)sourceBalances {
    [self openBalanceAndCurrencyActionSheet:sourceBalances
                                        tag:0
                               inController:self];
}

- (void)presentTargetBalances:(NSArray *)targetBalances {
    [self openBalanceAndCurrencyActionSheet:targetBalances
                                        tag:1
                               inController:self];
}

#pragma mark - SELECTED ACCOUNT OR BALANCE

/**
 * Prepared source and target accounts for tableView
 * @param source
 */
- (void)preparedSourceAccount:(NSDictionary *)source {
    self.selectedSourceAccounts = source;
    [self.tableView reloadData];
}

- (void)preparedTargetAccount:(NSDictionary *)target {
    self.selectedTargetAccounts = target;
    [self.tableView reloadData];
}

/**
 * Prepared balance and currency for tableView
 */
- (void)preparedSourceBalance:(NSString *)sourceBalance {
    self.selectedSourceBalance = sourceBalance;
    [self.tableView reloadData];
}

- (void)preparedTargetBalance:(NSString *)targetBalance {
    self.selectedTargetBalance = targetBalance;
    [self.tableView reloadData];
}

#pragma mark - Action sheet delegate

- (void)actionSheet:(AccountsActionSheetViewController *)actionSheet didSelectedAccount:(Account *)account {
    //send accounts to presented for preparation
    if (actionSheet.view.tag == 0)
    {

        [self.output setSelectedSource:account];
        [self.output runCheckTransferWithAmount:self.sourceAmountCell.summTextField.text
                                      direction:YES];

    }
    else if (actionSheet.view.tag == 1)
    {
        [self.output setSelectedTarget:account];

        if (self.targetAmountCell.summTextField.text.length > 0) {
            [self.output runCheckTransferWithAmount:self.targetAmountCell.summTextField.text direction:NO];
        } else {
            [self.output runCheckTransferWithAmount:self.sourceAmountCell.summTextField.text direction:YES];
        }
    }

    [actionSheet dismissViewControllerAnimated:YES completion:nil];
}

- (void)accountBalanceSheet:(AccountBalanceSheetViewController *)balanceSheet didSelectedBalance:(id)selectedBalance {
    if (balanceSheet.view.tag == 0) {
        [self.output setSelectedSourceBalance:selectedBalance];
    } else if (balanceSheet.view.tag == 1) {
        [self.output setSelectedTargetBalance:selectedBalance];
    }
    [balanceSheet dismissViewControllerAnimated:YES completion:nil];
    if (self.sourceAmountCell.summTextField.text.length > 0) {
        [self.output runCheckTransferWithAmount:self.sourceAmountCell.summTextField.text direction:YES];
    }
}

#pragma mark - config actions

- (IBAction)clickTransferButton:(id)sender {
    if (self.saveTemplateSwitch.isOn && self.templateNameTextField.text.length == 0) {
        return [Toast showToast:@"Для сохранения введите название шаблона"];
    }
    [self.output runInitTransferWithAmount:self.sourceAmountCell.summTextField.text
                                    isSave:self.saveTemplateSwitch.isOn
                              templateName:self.templateNameTextField.text];
}

#pragma mark - table user actions

/**
 * Open source account selection popup
 */
- (void)openSourceAccountSelection
{
    [self.output openSourceAccountPopup];
}

/**
 * Open source balance selection popup
 */
- (void)openSourceBalanceSelection
{
    [self.output openSourceBalancePopup];
}

/**
 * Open target account selection popup
 */
- (void)openTargetAccountSelection
{
    [self.output openTargetAccountPopup];
}

/**
 * Open target balance selection popup
 */
- (void)openTargetBalanceSelection
{
    [self.output openTargetBalancePopup];
}

/**
 * Open select beneficiary account vc
 */
- (void)openSelectBeneficiary {
    UINavigationController *nc = [UIHelper defaultNavigationController];
    nc.navigationBar.barStyle = UIBarStyleBlack;
    SearchBeneficiaryViewController *v = [[SearchBeneficiaryViewController alloc] init];
    v.delegate = self;
    [nc setViewControllers:@[v]];
    [self presentViewController:nc animated:YES completion:nil];
}

/**
 * Open remove beneficiary account vc
 */
- (void)openRemoveBeneficiary {
    EditBankTransferBeneficiaryAccountsViewController *vc = [EditBankTransferBeneficiaryAccountsViewController new];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"isInterbank == NO"];
    vc.beneficiaryAccounts = [[[self.output requestOperationAccounts] targetAccounts].beneficiars filteredArrayUsingPredicate:filter];
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - reloading operation account

- (void)reloadOperationsAccount:(NSNotification *)notification {
    [self.output configureStart];
}

#pragma mark - search beneficiary delegate

- (void)searchBeneficiaryViewControllerRecipientAddedWithId:(NSNumber *)beneficialAccountId {
    [self reloadOperationsAccount:nil];
}

#pragma mark - textfield delegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == DEF_SOURCE_SUMM_TEXTFIELD_TAG) {
        [self.output runCheckTransferWithAmount:textField.text direction:YES];
    } else if (textField.tag == DEF_TARGET_SUMM_TEXTFIELD_TAG) {
        [self.output runCheckTransferWithAmount:textField.text direction:NO];
    }
}

#pragma mark - config ui

/**
 * Open Accounts Action Sheet popup
 * @param accounts
 * @param tag
 */
- (void)openAccountsActionSheet:(NSArray *)accounts tag:(NSInteger)tag {
    [super openAccountsActionSheet:accounts tag:tag inController:self];
}

/**
 * Main config ui
 */
- (void)configUI
{
    [self setNavigationTitle:@"Переводы в пользу третьих лиц"];
    [self setNavigationBackButton];
    [self setBafBackground];

    self.tableView.estimatedRowHeight = 100;

    self.transferButton.mainButtonStyle = MainButtonStyleDisable;

    self.isConvertationDisclamerHidden = self.convertationDisclamer.hidden = YES;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadOperationsAccount:) name:kNotificationUserUpdatedBeneficiaryAccount object:nil];
}

#pragma mark -

//dealloc from memory

- (void)goBack {
    [self.output closeModule];
    self.output = nil;
    [self popToRoot];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"TransfersToOtherViewController delloced");
}

@end
