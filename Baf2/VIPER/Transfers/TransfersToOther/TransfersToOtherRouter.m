//
// Created by Askar Mustafin on 5/31/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "TransfersToOtherRouter.h"
#import "TransfersToOtherViewController.h"
#import "TransfersToOtherPresenter.h"


@interface TransfersToOtherRouter() {}
@property (nonatomic, strong) TransfersToOtherViewController *transfersToOtherViewController;
@end

@implementation TransfersToOtherRouter {}

- (void)pushTransfersToOtherModule:(UINavigationController *)navigationController {
    TransfersToOtherViewController *transfersToOtherViewController = [TransfersToOtherViewController createVC];
    
    transfersToOtherViewController.output = self.transfersToOtherPresenter;
    self.transfersToOtherPresenter.viewInput = transfersToOtherViewController;

    self.transfersToOtherViewController = transfersToOtherViewController;
    [navigationController pushViewController:transfersToOtherViewController animated:YES];
}

- (void)dealloc {
    NSLog(@"TransfersToOtherRouter dealloc");
}

@end
