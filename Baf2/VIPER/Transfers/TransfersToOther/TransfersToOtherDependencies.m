//
// Created by Askar Mustafin on 5/31/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "TransfersToOtherDependencies.h"
#import "TransfersToOtherRouter.h"
#import "TransfersToOtherInteractor.h"
#import "TransfersToOtherPresenter.h"
#import "TransferTemplate.h"


@interface TransfersToOtherDependencies()
@property (nonatomic, strong) TransfersToOtherRouter *transfersToOtherRouter;
@end

@implementation TransfersToOtherDependencies {}

- (id)init {
    self = [super init];
    if (self) {
        [self configureDependencies];
    }
    return self;
}

- (void)configureDependencies {

    //Transfer Modules Classes
    TransfersToOtherRouter *transfersToOtherRouter = [[TransfersToOtherRouter alloc] init];
    TransfersToOtherPresenter *transfersToOtherPresenter = [[TransfersToOtherPresenter alloc] init];
    TransfersToOtherInteractor *transfersToOtherInteractor = [[TransfersToOtherInteractor alloc] init];

    //contections
    //interactor's OUTPUT with presenter
    //presenter's INPUT with interactor
    transfersToOtherInteractor.output = transfersToOtherPresenter;
    transfersToOtherPresenter.interactorInput = transfersToOtherInteractor;

    transfersToOtherRouter.transfersToOtherPresenter = transfersToOtherPresenter;
    transfersToOtherPresenter.transfersToOtherRouter = transfersToOtherRouter;

    self.transfersToOtherRouter = transfersToOtherRouter;
}

- (void)installRootViewController {
    [self.transfersToOtherRouter pushTransfersToOtherModule:[UIHelper rootNavigationController]];
}

- (void)setTransferTemplate:(TransferTemplate *)transferTemplate {
    _transferTemplate = transferTemplate;
    self.transfersToOtherRouter.transfersToOtherPresenter.transferTemplate = transferTemplate;
}

- (void)dealloc {
    NSLog(@"TransfersToOtherDependencies dealloc");
}

@end