//
// Created by Askar Mustafin on 6/2/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "TransfersBaseViewController.h"
#import "TransfersFormCell.h"
#import "MyTransferBalanceCell.h"
#import "TransfersFormDisclaimerCell.h"
#import "AccountBalanceSheetViewController.h"
#import "STPopupController+Extensions.h"
#import "AccountsActionSheetViewController.h"
#import "CheckAndInitTransferResponse.h"
#import "BankTransferConfirmViewController.h"
#import "TransferCurrencyRates.h"
#import <STPopup/STPopupController.h>


@implementation TransfersBaseViewController {}

#pragma mark - transition

- (void)showConfirmationWith:(CheckAndInitTransferResponse *)checkAndInitTransferResponse
                transferType:(NSString *)transferType
{
    BankTransferConfirmViewController *v = [[BankTransferConfirmViewController alloc] init];
    v.isOldVersion = YES;
    v.servicelogId = checkAndInitTransferResponse.servicelogId;
    v.safetyLevel = checkAndInitTransferResponse.safetyLevel;
    v.commission = checkAndInitTransferResponse.comission;
    v.commissionCurrency = [checkAndInitTransferResponse.comissionCurrency decimalFormatString];
    v.fromAccount = checkAndInitTransferResponse.requestorAccount;
    v.toAccount = checkAndInitTransferResponse.destinationAccount;
    v.currency = checkAndInitTransferResponse.requestorCurrency;
    v.amount = checkAndInitTransferResponse.requestorAmount;
    v.transferType = transferType;
    v.saveAsTemplate = checkAndInitTransferResponse;
    v.destinationAmount = checkAndInitTransferResponse.destinationAmount;
    v.destinationCurrency = checkAndInitTransferResponse.destinationCurrency;
    v.rate = checkAndInitTransferResponse.exchangeResponse.presentation;
    [self.navigationController pushViewController:v animated:YES];
}

#pragma mark - ui helpers

- (void)openBalanceAndCurrencyActionSheet:(NSArray *)array tag:(NSInteger)tag inController:(UIViewController *)controller {
    AccountBalanceSheetViewController *p = [[AccountBalanceSheetViewController alloc] initWithBalanceAndCurrencies:array];
    p.delegate = controller;
    p.view.tag = tag;
    p.titleString = tag == 0 ? @"Выберите валюту списания" : @"Выберите валюту зачисления";
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:p];
    popupController.dismissOnBackgroundTap = true;
    popupController.style = STPopupStyleBottomSheet;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [popupController presentInViewController:controller];
}

- (void)openAccountsActionSheet:(NSArray *)array tag:(NSInteger)tag inController:(UIViewController *)controller {
    AccountsActionSheetViewController *p = [[AccountsActionSheetViewController alloc] initWithAccounts:array];
    p.delegate = controller;
    p.view.tag = tag;
    p.titleString = tag == 0 ? @"Выберите счет списания" : @"Выберите счет зачисления";
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:p];
    popupController.dismissOnBackgroundTap = true;
    popupController.style = STPopupStyleBottomSheet;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [popupController presentInViewController:controller];
}

- (void)showProgressHUDWithTtitle:(NSString *)title {
    [MBProgressHUD showAstanaHUDWithTitle:title animated:YES];
}

- (void)hideProgressHUD {
    [MBProgressHUD hideAstanaHUDAnimated:YES];
}

#pragma mark - tableview delegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case ConvertationDisclamerSection:
        {
            if (self.isConvertationDisclamerHidden) {
                return 0;
            } else {
                return UITableViewAutomaticDimension;
            }
        }
        case OtherDisclamersSection:
        {
            if (indexPath.row == 0 && (!self.warningMessages || self.warningMessages.count == 0)) {
                return 0;
            } else if (indexPath.row == 1 && (!self.warningMessages || self.warningMessages.count < 2)) {
                return 0;
            } else if (indexPath.row == 2 && (!self.warningMessages || self.warningMessages.count < 3)) {
                return 0;
            } else if (indexPath.row == 3 && (!self.warningMessages || self.warningMessages.count < 4)) {
                return 0;
            } else if (indexPath.row == 4 && (!self.warningMessages || self.warningMessages.count < 5)) {
                return 0;
            } else if (indexPath.row == 5 && (!self.warningMessages || self.warningMessages.count < 6)) {
                return 0;
            }
        }
        case TemplateSection:
        {
            if (self.isTemplate) {
                return 0;
            } else {
                return UITableViewAutomaticDimension;
            }
        }
        default:
        {
            return UITableViewAutomaticDimension;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];

    switch (indexPath.section)
    {
        case ConvertationDisclamerSection:
        {
            break;
        }
        case SourceAccountSection:
        {
            //SELECT SOURCE ACCOUNT
            if (indexPath.row == 0)
            {
                TransfersFormCell *sourceCell = (TransfersFormCell *)cell;
                if (self.selectedSourceAccounts)
                {
                    [sourceCell.iconImageView
                            sd_setImageWithURL:self.selectedSourceAccounts[@"logo"]
                              placeholderImage:[UIImage imageNamed:@"icon-icon-placeholder"]];
                    sourceCell.mainTitleLabel.text = self.selectedSourceAccounts[@"alias"];
                    sourceCell.leftDescriptionLabel.text = self.selectedSourceAccounts[@"number"];
                    sourceCell.rightDescriptionLabel.text = self.selectedSourceAccounts[@"amount"];
                    return sourceCell;
                }
            }
                //ENTER SOURCE AMOUNT
            else if (indexPath.row == 1)
            {
                MyTransferBalanceCell *sourceBalanceCell = (MyTransferBalanceCell *)cell;
                if (self.selectedSourceBalance)
                {
                    sourceBalanceCell.summLabel.text = self.selectedSourceBalance;
                } else {
                    sourceBalanceCell.summLabel.text = @"Валюта";
                }
            }

            break;
        }
        case TargetAccountSection:
        {
            //SELECT TARGET ACCOUNT
            if (indexPath.row == 0)
            {
                TransfersFormCell *sourceCell = (TransfersFormCell *)cell;
                if (self.selectedTargetAccounts)
                {
                    [sourceCell.iconImageView
                            sd_setImageWithURL:self.selectedTargetAccounts[@"logo"]
                              placeholderImage:[UIImage imageNamed:@"icon-icon-placeholder"]];
                    sourceCell.mainTitleLabel.text = self.selectedTargetAccounts[@"alias"];
                    sourceCell.leftDescriptionLabel.text = self.selectedTargetAccounts[@"number"];
                    sourceCell.rightDescriptionLabel.text = self.selectedTargetAccounts[@"amount"];
                    return sourceCell;
                }
            }
                //ENTER TARGET AMOUNT
            else if (indexPath.row == 1)
            {
                MyTransferBalanceCell *targetBalanceCell = (MyTransferBalanceCell *)cell;
                if (self.selectedSourceBalance)
                {
                    targetBalanceCell.summLabel.text = self.selectedTargetBalance;
                } else {
                    targetBalanceCell.summLabel.text = @"Валюта";
                }
            }

            break;
        }
        case OtherDisclamersSection:
        {

            TransfersFormDisclaimerCell *disclaimerCell = (TransfersFormDisclaimerCell *) cell;

            if (self.warningMessages && self.warningMessages.count > indexPath.row) {
                [disclaimerCell setHTML:self.warningMessages[(NSUInteger) indexPath.row]];
            }

            return disclaimerCell;
        }

        default:
        {
            break;
        }
    }

    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"indexPath section: %i row: %i", indexPath.section, indexPath.row);

    switch (indexPath.section)
    {
        case ConvertationDisclamerSection:
        {
            break;
        }
        case SourceAccountSection:
        {
            //SELECT SOURCE ACCOUNT
            if (indexPath.row == 0)
            {
                [self openSourceAccountSelection];
            }
                //ENTER SOURCE AMOUNT
            else if (indexPath.row == 1)
            {
                [self openSourceBalanceSelection];
            }

            break;
        }
        case TargetAccountSection:
        {
            //SELECT TARGET ACCOUNT
            if (indexPath.row == 0)
            {
                [self openTargetAccountSelection];

            }
            //SELECT TARGET BALANCE
            else if (indexPath.row == 1)
            {
                [self openTargetBalanceSelection];
            }

            //ADD BENEFICIARY ACCOUNT
            else if (indexPath.row == 2)
            {
                [self openSelectBeneficiary];
            }

            //REMOVE BENEFICIARY ACCOUNT
            else if (indexPath.row == 3)
            {
                [self openRemoveBeneficiary];
            }

            break;
        }
        case OtherDisclamersSection:
        {
            break;
        }

        default:
        {
            break;
        }
    }
}

@end
