//
// Created by Askar Mustafin on 6/2/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class CheckAndInitTransferResponse;

/**
 * Enum that represents TableView sections
 */
typedef NS_ENUM(NSInteger, SectionType) {
    ConvertationDisclamerSection = 0,
    SourceAccountSection = 1,
    TargetAccountSection = 2,
    TemplateSection = 3,
    OtherDisclamersSection = 4
};

@interface TransfersBaseViewController : UITableViewController

@property (nonatomic, strong) NSArray *warningMessages;
@property (nonatomic, strong) NSDictionary *selectedSourceAccounts;
@property (nonatomic, strong) NSDictionary *selectedTargetAccounts;
@property (nonatomic, strong) NSString *selectedSourceBalance;
@property (nonatomic, strong) NSString *selectedTargetBalance;
@property (nonatomic, assign) BOOL isConvertationDisclamerHidden;
@property (nonatomic, assign) BOOL isTemplate;

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;


- (void)openSourceAccountSelection;
- (void)openSourceBalanceSelection;
- (void)openTargetAccountSelection;
- (void)openTargetBalanceSelection;

/**
 * Used in transfer third party and interbank
 */
- (void)openSelectBeneficiary;
- (void)openRemoveBeneficiary;

/**
 * show and hide progress hud
 * @param title
 */
- (void)showProgressHUDWithTtitle:(NSString *)title;
- (void)hideProgressHUD;


- (void)openBalanceAndCurrencyActionSheet:(NSArray *)array tag:(NSInteger)tag inController:(UIViewController *)controller;
- (void)openAccountsActionSheet:(NSArray *)array tag:(NSInteger)tag inController:(UIViewController *)controller;

/**
 * Confirmation vc
 */
- (void)showConfirmationWith:(CheckAndInitTransferResponse *)checkAndInitTransferResponse
                transferType:(NSString *)transferType;

@end