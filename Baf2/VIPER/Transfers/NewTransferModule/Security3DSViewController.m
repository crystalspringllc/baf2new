//
//  Security3DSViewController.m
//  Baf2
//
//  Created by developer on 27.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "Security3DSViewController.h"
#import "ASize.h"
#import "NewTransfersApi.h"
#import "BankTransferResultViewController.h"
#import "LastOperationDetailsViewController.h"

@interface Security3DSViewController () <UIWebViewDelegate>
@end

@implementation Security3DSViewController
{
    UIWebView *secWebView;
    NSMutableURLRequest *request;
    BOOL initFromNil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *string = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('html')[0].innerHTML"];
    NSLog(@"Script string %@", string);
    if ([string rangeOfString:@"window.close();"].location == NSNotFound)
    {
        if ([string rangeOfString:@"document.forms[0].submit()"].location == NSNotFound && !initFromNil) [UIHelper showErrorAlertWithMessage:@"Введенный вами пароль неверный.\nПопробуйте еще раз"];
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }else{
        [NewTransfersApi getTransferResultFromWebWithServiceLogID:self.serviceLogID
                                                          success:^(id response) {
                                                              NewCheckAndInitTransferResponse *checkTarnsferResponse = [NewCheckAndInitTransferResponse instanceFromDictionary:response];
                                                              
                                                              if(![checkTarnsferResponse.transferInfo.status isEqualToString:@"BEGIN"])
                                                              {
                                                                  __weak Security3DSViewController *weakSelf = self;
                                                                 
                                                                  // Goto result view controller
                                                                 /* BankTransferResultViewController *v = [[BankTransferResultViewController alloc] init];
                                                                  v.servicelogId = checkTarnsferResponse.transferInfo.serviceId;
                                                                  v.operationId = response;
                                                                  v.fromAccount = checkTarnsferResponse.requestor.account;
                                                                  v.toAccount = checkTarnsferResponse.destination.account;
                                                                  v.amount = checkTarnsferResponse.amountInfo.finalAmount;
                                                                  v.currency = checkTarnsferResponse.amountInfo.currency;
                                                                  v.convertedAmount = [checkTarnsferResponse.amountInfo.exchangeResponse.convertedAmount decimalFormatString];
                                                                  v.comissionAmount = checkTarnsferResponse.amountInfo.commission.commissionAmount;
                                                                  v.comissionCurrency = checkTarnsferResponse.amountInfo.commission.commissionCurrency;
                                                                  v.saveAsTemplate = checkTarnsferResponse.save;*/
                                                                  LastOperationDetailsViewController *v = [[LastOperationDetailsViewController alloc] init];
                                                                  v.operationId = checkTarnsferResponse.transferInfo.serviceId;
                                                                  v.newVersion = YES;
                                                                  [weakSelf.navigationController pushViewController:v animated:YES];
                                                              }
                                                              else{
                                                                  [self goBack];
                                                              }
                                                              
                                                              [MBProgressHUD hideAstanaHUDAnimated:YES];
                                                          }
                                                          failure:^(NSString *code, NSString *message) {
                                                            [MBProgressHUD hideAstanaHUDAnimated:YES];
                                                          }];
    }
     initFromNil = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
  [MBProgressHUD hideAstanaHUDAnimated:YES];
  [UIHelper showErrorAlertWithMessage:@"Ошибка при загрузски веб части"];
}


- (void)startRequest
{
    if(!self.securityParams) return;
        request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.securityLink]];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[self httpBodyForParameters:self.securityParams]];
        [secWebView loadRequest:request];
}

- (NSData *)httpBodyForParameters:(NSDictionary *)parameters
{
    NSMutableArray *parameterArray = [NSMutableArray array];
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
        NSString *param = [NSString stringWithFormat:@"%@=%@", [self percentEscapeString:key], [self percentEscapeString:obj]];
        [parameterArray addObject:param];
    }];
    
    NSString *string = [parameterArray componentsJoinedByString:@"&"];
    
    return [string dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)percentEscapeString:(NSString *)string
{
    NSCharacterSet *allowed = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~"];
    return [string stringByAddingPercentEncodingWithAllowedCharacters:allowed];
}

- (void)configUI
{
    [self setNavigationTitle:@"Ввод кода безопастности"];
    [self setNavigationBackButton];
    secWebView = [UIWebView newAutoLayoutView];
    secWebView.delegate = self;
    secWebView.scrollView.scrollEnabled = YES;
    [self.view addSubview:secWebView];
    [secWebView autoPinEdgesToSuperviewEdges];
    [self startRequest];
    initFromNil = YES;
}


@end
