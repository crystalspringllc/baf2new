//
//  CVVEnterViewController.h
//  Baf2
//
//  Created by developer on 03.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Account.h"
#import "JVFloatLabeledTextField.h"

@protocol CVVEnterViewControllerDelegate<NSObject>
- (void)enterCVCViewController:(id)viewController didEnterCVC:(NSString *)cvc forAccount:(Account *)account;
@end

@interface CVVEnterViewController : UIViewController <UITextFieldDelegate>

@property(nonatomic, weak) id <CVVEnterViewControllerDelegate> delegate;
@property(nonatomic, strong) NSString *cardNumber;
@property(nonatomic, strong) NSString *warningMessage;
@property(nonatomic, strong) NSString *publicKey;

@property(nonatomic, assign) BOOL promoEnabled;
@property(nonatomic, strong) Account *account;

+ (instancetype)createVC;

- (NSString *)cardBindingId;


@end
