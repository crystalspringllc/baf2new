//
//  TransferViewController.m
//  Baf2
//
//  Created by developer on 23.01.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "TransferViewController.h"
#import "AccountFormView.h"
#import "JVFloatLabeledTextField.h"
#import "STPopupController+Extensions.h"
#import "AccountsApi.h"
#import "User.h"
#import "AccountsResponse.h"
#import "NewTransfersApi.h"
#import "MBProgressHUD+AstanaView.h"
#import "OperationAccounts.h"
#import "Accounts.h"
#import "UIImage+Extensions.h"
#import "NSString+Ext.h"
#import "NewCheckAndInitTransferResponse.h"
#import "NSError+Extensions.h"
#import "MainButton.h"
#import "SearchBeneficiaryViewController.h"
#import "EditBankTransferBeneficiaryAccountsViewController.h"
#import "NotificationCenterHelper.h"
#import "AddJuridicalBeneficiaryViewController.h"
#import "AddPhysicalBeneficiaryViewController.h"
#import "ASize.h"
#import "TransfersFormViewController.h"
#import "UIView+ConcisePureLayout.h"
#import "BankTransferConfirmViewController.h"
#import "KnpView.h"
#import "CVVEnterViewController.h"
#import <STPopup/STPopupController.h>
#import "STPopupController+Extensions.h"
#import "BankTransferSmsConfirmationViewController.h"
#import "BankTransferResultViewController.h"
#import "P2PBrowserViewController.h"
#import "CurrencyRateDisclamerView.h"
#import "DisclamerView.h"
#import "CVVAddResponse.h"
#import "LastOperationDetailsViewController.h"



@interface TransferViewController () <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, CVVEnterViewControllerDelegate, SearchBeneficiaryViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *transferTypeSelector;
@property (weak, nonatomic) IBOutlet UISegmentedControl *destinationTypeSelector;

@property (weak, nonatomic) IBOutlet UIView *accountFromView;
@property (weak, nonatomic) IBOutlet UIView *destinationAccountView;

@property (weak, nonatomic) IBOutlet UIView *TransferFromSummView;
@property (weak, nonatomic) IBOutlet UIView *transferDestinationSummView;

@property (weak, nonatomic) IBOutlet UILabel *fromCardlabel;
@property (weak, nonatomic) IBOutlet UILabel *fromCardBalance;
@property (weak, nonatomic) IBOutlet UIImageView *fromCardImage;
@property (weak, nonatomic) IBOutlet UILabel *fromCardNumber;
@property (weak, nonatomic) IBOutlet UILabel *emptyFormRequestorLabel;
@property (weak, nonatomic) IBOutlet UILabel *emptyFormOrExtCardLabel;

@property (weak, nonatomic) IBOutlet UILabel *destinationCardLabel;
@property (weak, nonatomic) IBOutlet UIImageView *destinationCardImage;
@property (weak, nonatomic) IBOutlet UILabel *destinationCardBalance;
@property (weak, nonatomic) IBOutlet UILabel *destinationCardNumber;

@property (weak, nonatomic) IBOutlet UILabel *fromCurrencyLabel;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *fromSummTextField;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *destinationSummTextField;
@property (weak, nonatomic) IBOutlet UILabel *destinationCurrencyLabel;
@property (weak, nonatomic) IBOutlet UIView *destinationCurrencyTapView;
@property (weak, nonatomic) IBOutlet UIView *fromCurrencyTapView;

@property (weak, nonatomic) IBOutlet UILabel *addSourceCardLabel;
@property (weak, nonatomic) IBOutlet UILabel *addCardLabel;
@property (weak, nonatomic) IBOutlet UILabel *addAccountLabel;
@property (weak, nonatomic) IBOutlet UILabel *removeAccountLabel;

@property (weak, nonatomic) IBOutlet MainButton *transferButton;
@property (weak, nonatomic) IBOutlet UIView *contentView;


@property (weak, nonatomic) IBOutlet KnpView *knpView;

@property (weak, nonatomic) IBOutlet CurrencyRateDisclamerView *currencyDisclamer;
@property (weak, nonatomic) IBOutlet DisclamerView *disclaimerView;
@property (weak, nonatomic) IBOutlet DisclamerView *disclamerView1;
@property (weak, nonatomic) IBOutlet DisclamerView *disclamerView3;

@property (nonatomic, strong) OperationAccounts *operationAccounts;

@property (nonatomic, strong) AccountsResponse *accountsResponse;
@property (nonatomic, strong) Account *source;
@property (nonatomic, strong) Account *target;

@property (nonatomic, strong) NewCheckAndInitTransferResponse *checkTarnsferResponse;

@property (nonatomic, strong) NSString *transferSumm;

@property (nonatomic, strong) id sourceBalanceProperty;
@property (nonatomic, strong) id targetBalanceProperty;

@property (nonatomic, strong) NSMutableDictionary *accountParams;
@property (nonatomic, strong) NSMutableDictionary *transferParams;

@property (nonatomic, strong) NSString *selectedKnpCode;
@property (nonatomic, strong) NSString *selectedKnpName;
@property (nonatomic, strong) NSArray *knpList;
@property (nonatomic, strong) NSArray *allKnpList;

///////////Constaraints

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addAcoountsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *removeAccountsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addAccountTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *knpViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *transferButtonTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *transferTypeTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *targetTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addAcccountTopConstraint;


@end
@implementation TransferViewController
{
    NSArray *accountArray;
    NSMutableArray *balanceArray;
    BOOL didLoadAccounts;
    BOOL direction;
}

+ (instancetype)createVC
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TransferView" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([TransferViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initVars];
    [self configUI];
}

- (void)initVars
{
   // self.knpCode = @"119";
    direction = NO;
    accountArray = [NSArray array];
    _accountParams = [NSMutableDictionary dictionary];
    _transferParams = [NSMutableDictionary dictionary];
    _transferSumm = [NSString string];
}
- (IBAction)transferButtonAction:(id)sender {
    
    [MBProgressHUD showAstanaHUDWithTitle:@"Инициализация трансфера" animated:YES];
    [NewTransfersApi newInitTransferWithParams:self.transferParams
                                   requestorId:self.source.accountId
                               requsetorAmount:[self.fromSummTextField.text numberDecimalFormat]
                             destinationAmount:[self.destinationSummTextField.text numberDecimalFormat]
                                 destinationId:self.target.accountId
                                 requestorType:self.source.type
                             requestorCurrency:self.sourceBalanceProperty[@"currency"]
                               destinationType:self.target.type
                           destinationCurrency:self.targetBalanceProperty[@"currency"]
                                       knpCode:self.selectedKnpCode
                                       knpName:self.selectedKnpName
                                     direction:direction
                                          save:NO
                                         alias:nil
                                       success:^(NewCheckAndInitTransferResponse *checkTarnsfer) {
                                           [MBProgressHUD hideAstanaHUDAnimated:YES];
                                          // self.checkTarnsferResponse = [NewCheckAndInitTransferResponse instanceFromDictionary:response];
                                           self.checkTarnsferResponse = checkTarnsfer;
                                           [self checkInitResponse:checkTarnsfer];
                                       }
                                       failure:^(NSString *code, NSString *message) {
                                            [MBProgressHUD hideAstanaHUDAnimated:YES];
                                           NSLog(@"Error");
                                       }];
}

- (void)checkInitResponse:(NewCheckAndInitTransferResponse *)checkAndInitTransfer
{
    if(!_checkTarnsferResponse.isPayBox){
    if([_checkTarnsferResponse.nextAction isEqualToString:@"SMS_CONFIRM"]){
        
      [self showSmsConfirmationWith:checkAndInitTransfer transferType:nil];
        
    }else if([checkAndInitTransfer.nextAction isEqualToString:@"INIT"])
    {
        return;
    }else if([_checkTarnsferResponse.nextAction isEqualToString:@"RUN"])
    {
        [self showConfirmationWith:checkAndInitTransfer transferType:nil];
      //[self runTransfer:checkAndInitTransfer];
    }else if([_checkTarnsferResponse.nextAction isEqualToString:@"EXECUTE"])
    {
        if([checkAndInitTransfer smsIsNeed])
        {
          [self showSmsConfirmationWith:checkAndInitTransfer transferType:nil];
               } else {
            [self runTransfer:checkAndInitTransfer];
        }
    }else if([_checkTarnsferResponse.transferInfo.level isEqualToString:@"SMS"]){
        if([_checkTarnsferResponse.transferInfo.status isEqualToString:@"BEGIN"])
        {
            [self showSmsConfirmationWith:checkAndInitTransfer transferType:nil];
        }
    } else if([_checkTarnsferResponse.transferInfo.level isEqualToString:@"3DS"]){
        if([_checkTarnsferResponse.transferInfo.status isEqualToString:@"BEGIN"])
        {
            [self showConfirmationWith:checkAndInitTransfer transferType:nil];
            // [self showP2PWebViewWith:self.checkTarnsferResponse];
        }else{
            [self showErrorMessage:_checkTarnsferResponse.transferInfo.statusText];
        }
    }
    }else{
        if([_checkTarnsferResponse.transferInfo.level isEqualToString:@"SMS"]){
        if([_checkTarnsferResponse.transferInfo.status isEqualToString:@"BEGIN"])
        {
          [self showSmsConfirmationWith:checkAndInitTransfer transferType:nil];
        }
        }else if([_checkTarnsferResponse.transferInfo.level isEqualToString:@"FRAME"]){
        if([_checkTarnsferResponse.transferInfo.status isEqualToString:@"BEGIN"])
        {
           [self showP2PWebViewWith:self.checkTarnsferResponse];
   }else{
           [self showErrorMessage:_checkTarnsferResponse.transferInfo.statusText];
        }
    }else if([_checkTarnsferResponse.transferInfo.level isEqualToString:@"3DS"]){
        if([_checkTarnsferResponse.transferInfo.status isEqualToString:@"BEGIN"])
        {
             [self showConfirmationWith:checkAndInitTransfer transferType:nil];
           // [self showP2PWebViewWith:self.checkTarnsferResponse];
        }else{
            [self showErrorMessage:_checkTarnsferResponse.transferInfo.statusText];
        }
    }
}
}



- (void)showConfirmationWith:(NewCheckAndInitTransferResponse *)checkAndInitTransferResponse
                transferType:(NSString *)transferType
{
    BankTransferConfirmViewController *v = [[BankTransferConfirmViewController alloc] init];
    v.isOldVersion = NO;
    v.servicelogId = self.checkTarnsferResponse.transferInfo.serviceId;
    v.safetyLevel = self.checkTarnsferResponse.transferInfo.level;
    v.levelStatus = self.checkTarnsferResponse.transferInfo.status;
    v.statusText = self.checkTarnsferResponse.transferInfo.statusText;
    v.knpDescription = [self.checkTarnsferResponse.knpObject objectForKey:@"name"];
    v.commissionCurrency = self.checkTarnsferResponse.amountInfo.commission.commissionCurrency;
    v.commission = self.checkTarnsferResponse.amountInfo.commission.commissionAmount;
    v.convertedAmount = [self.checkTarnsferResponse.amountInfo.exchangeResponse.convertedAmount decimalFormatString];
    v.fromAccount = self.checkTarnsferResponse.requestor.account;
    v.toAccount = self.checkTarnsferResponse.destination.account;
    v.currency = self.checkTarnsferResponse.amountInfo.currency;
    v.requsetorCurrency = self.checkTarnsferResponse.requestor.currency;
    v.amount = self.checkTarnsferResponse.requestor.accountAmount;
    v.transferType = nil;
    v.operationDate = self.checkTarnsferResponse.transferInfo.date;
    v.saveAsTemplate = self.checkTarnsferResponse;
    v.destinationAmount = self.checkTarnsferResponse.destination.accountAmount;
    v.destinationCurrency = self.checkTarnsferResponse.destination.currency;
    v.rate = self.checkTarnsferResponse.amountInfo.exchangeResponse.presentation;
    v.operationAmount = [self.checkTarnsferResponse.amountInfo.finalAmount decimalFormatString];
    v.operationCurrency = self.checkTarnsferResponse.amountInfo.currency;
    v.isPayboxType = self.checkTarnsferResponse.isPayBox;
    [self.navigationController pushViewController:v animated:YES];
}

- (void)showSmsConfirmationWith:(NewCheckAndInitTransferResponse *)checkAndInitTransferResponse
                transferType:(NSString *)transferType
{
    BankTransferSmsConfirmationViewController *v = [[BankTransferSmsConfirmationViewController alloc] init];
    v.servicelogId = self.checkTarnsferResponse.transferInfo.serviceId;
    v.commissionCurrency = self.checkTarnsferResponse.amountInfo.commission.commissionCurrency;
    v.commissionAmount = self.checkTarnsferResponse.amountInfo.commission.commissionAmount;
    v.fromAccount = self.checkTarnsferResponse.requestor.account;
    v.toAccount = self.checkTarnsferResponse.destination.account;
    v.currency = self.checkTarnsferResponse.amountInfo.currency;
    v.amount = self.checkTarnsferResponse.amountInfo.finalAmount;
    v.saveAsTemplate = self.checkTarnsferResponse;
    v.checkTarnsferResponse = checkAndInitTransferResponse;
    [self.navigationController pushViewController:v animated:YES];
}

- (void)showP2PWebViewWith:(NewCheckAndInitTransferResponse *)checkAndInitTransferResponse
{
    P2PBrowserViewController *v = [[P2PBrowserViewController alloc] init];
    v.postLink = checkAndInitTransferResponse.gateWay.postLink;
    v.backLink = checkAndInitTransferResponse.gateWay.backLink;
    v.processLink = checkAndInitTransferResponse.gateWay.processLink;
    v.terminalId = checkAndInitTransferResponse.gateWay.terminalId;
    v.servicelogId = checkAndInitTransferResponse.transferInfo.serviceId;
    v.isNew = YES;
    
    [self.navigationController pushViewController:v animated:YES];
}

-(void)goToAppointmentPayment
{
    __weak TransferViewController *wSelf = self;
    TransfersFormViewController *vc = [TransfersFormViewController createVC];
    vc.operationAccounts = [self.operationAccounts copy];
    vc.transfersType = INTERBANK_TRANSFERS;
    vc.title = [NewTransfersApi transferTypeToDescriptionString:INTERBANK_TRANSFERS];
    [wSelf.navigationController pushViewController:vc animated:true];
}

- (void)runTransfer:(NewCheckAndInitTransferResponse *)transfer
{
    __weak TransferViewController *weakSelf = self;
    weakSelf.checkTarnsferResponse = transfer;
    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
    
    [NewTransfersApi newRunTransfer:transfer.transferInfo.serviceId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        // Send notification that should update accounts
       // [[NSNotificationCenter defaultCenter] postNotificationName:kMyCardsAndAccountsViewControllerUpdateAccountsNotification object:nil];
        
        // Goto result view controller
        //BankTransferResultViewController *v = [[BankTransferResultViewController alloc] init];
        LastOperationDetailsViewController *v = [[LastOperationDetailsViewController alloc] init];
        v.operationId = weakSelf.checkTarnsferResponse.transferInfo.serviceId;
        v.newVersion = YES;
       /* v.operationId = response;
        v.fromAccount = weakSelf.checkTarnsferResponse.requestor.account;
        v.toAccount = weakSelf.checkTarnsferResponse.destination.account;
        v.amount = weakSelf.checkTarnsferResponse.amountInfo.finalAmount;
        v.currency = weakSelf.checkTarnsferResponse.amountInfo.currency;
        v.convertedAmount = [weakSelf.checkTarnsferResponse.amountInfo.exchangeResponse.convertedAmount decimalFormatString];
        v.comissionAmount = weakSelf.checkTarnsferResponse.amountInfo.commission.commissionAmount;
        v.comissionCurrency = weakSelf.checkTarnsferResponse.amountInfo.commission.commissionCurrency;
        v.saveAsTemplate = weakSelf.checkTarnsferResponse.save;*/
        [weakSelf.navigationController pushViewController:v animated:YES];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

-(void)sourceExtCardSelected
{
    Account *account = self.operationAccounts.sourceAccounts.extCards.lastObject;
    AccountsActionSheetViewController *p = [self getAccountsActionSheet:nil tag:0];
    p.view.tag = 1;
    [self actionSheet:p didSelectedAccount:account];
}

-(void)targetExtCardSelected
{
    Account *account = self.operationAccounts.targetAccounts.extCards.lastObject;
    AccountsActionSheetViewController *p = [self getAccountsActionSheet:nil tag:1];
    p.view.tag = 2;
    [self actionSheet:p didSelectedAccount:account];
}


- (void)openSelectBeneficiary
{
    
    if(![[self.transferParams objectForKey:@"dstOwner"] isEqualToString:@"S"] && [[self.transferParams objectForKey:@"type"] isEqualToString:@"I"] && ![[self.transferParams objectForKey:@"dstOwner"] isEqualToString:@"L"])
    {
       UINavigationController *nc = [UIHelper defaultNavigationController];
       nc.navigationBar.barStyle = UIBarStyleBlack;
       SearchBeneficiaryViewController *v = [[SearchBeneficiaryViewController alloc] init];
       v.delegate = self;
       [nc setViewControllers:@[v]];
       [self presentViewController:nc animated:YES completion:nil];
    }
    else if([[self.transferParams objectForKey:@"dstOwner"] isEqualToString:@"L"] /*&& ![[self.transferParams objectForKey:@"type"] isEqualToString:@"I"]*/)
    {
        AddJuridicalBeneficiaryViewController *v = [AddJuridicalBeneficiaryViewController createVC];
        [self.navigationController pushViewController:v animated:YES];
    }else{
        AddPhysicalBeneficiaryViewController *v = [AddPhysicalBeneficiaryViewController createVC];
        [self.navigationController pushViewController:v animated:YES];
    }
    
}

- (void)searchBeneficiaryViewControllerRecipientAddedWithId:(NSNumber *)beneficialAccountId
{
    [self clearRequestorForm];
    [self refreshUserAccount];
}

/**
 * Open remove beneficiary account vc
 */
- (void)openRemoveBeneficiary {
    EditBankTransferBeneficiaryAccountsViewController *vc = [EditBankTransferBeneficiaryAccountsViewController new];
    NSPredicate *filter;
    if([[_transferParams objectForKey:@"type"] isEqualToString:@"K"]){
    filter = [NSPredicate predicateWithFormat:@"isInterbank == YES"];
    }else{
    filter = [NSPredicate predicateWithFormat:@"isInterbank == NO"];
    }
    vc.beneficiaryAccounts = [self.operationAccounts.targetAccounts.beneficiars filteredArrayUsingPredicate:filter];
    [self.navigationController pushViewController:vc animated:true];
}

- (void)refreshUserAccount
{
    if(self.operationAccounts) self.operationAccounts = nil;
    [MBProgressHUD showAstanaHUDWithTitle:@"Получение данных" animated:YES];
    [NewTransfersApi getAccountsWithParametrs:_accountParams success:^(OperationAccounts *operationAccounts) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        NSMutableArray *sourceCards = operationAccounts.sourceAccounts.cards;
        operationAccounts.sourceAccounts.cards = sourceCards;
        self.operationAccounts = operationAccounts;
        [self clearDestinationForm];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

///////////// ------------ EnterCVVDelegate -----------------//////////////////

- (void)enterCVCViewController:(id)viewController didEnterCVC:(NSString *)cvc forAccount:(Account *)account
{
    [MBProgressHUD showAstanaHUDWithTitle:nil animated:YES];
    [NewTransfersApi sendEncryptedCVV:cvc
                        withAccountId:account.accountId
                              success:^(id response) {
                                  [MBProgressHUD hideAstanaHUDAnimated:YES];
                                  CVVAddResponse *cvvResponse = [CVVAddResponse instanceFromDictionary:response];
                                  if(cvvResponse.isNeed == false){
                                      [self runCheckTransferWithAmount:self.transferSumm direction:direction];
                                  }else{
                                      [UIHelper showErrorAlertWithMessage:cvvResponse.errorMessage];
                                  }
                             // [self checkInitResponse:self.checkTarnsferResponse];
                              } failure:^(NSString *code, NSString *message){
                                [MBProgressHUD hideAstanaHUDAnimated:YES];
                              }];
    
 
}

//////////// ------------ UISegmentSelector -----------------//////////////////

- (void)transferSelectorButton:(UISegmentedControl *)control
{
    if (control.selectedSegmentIndex == 0) {
        [_accountParams setValue:@"I" forKey:@"transferType"];
        [_transferParams setValue:@"I" forKey:@"type"];
        
        self.addSourceCardLabel.hidden = YES;
        self.targetTopConstraint.constant = 25;
        
    } else if(control.selectedSegmentIndex == 1) {
        [_accountParams setValue:@"K" forKey:@"transferType"];
        [_transferParams setValue:@"K" forKey:@"type"];
        
        self.addSourceCardLabel.hidden = NO;
        self.targetTopConstraint.constant = 50;
        
    }
    [self clearRequestorForm];
    [self destinationSelectorButton:self.destinationTypeSelector];
}

- (void)destinationSelectorButton:(UISegmentedControl *)control
{
    if (control.selectedSegmentIndex == 0) {
        [_transferParams setValue:@"S" forKey:@"dstOwner"];
        [_accountParams setValue:@"S" forKey:@"dstOwner"];
         self.knpView.hidden = YES;
        if([[self.transferParams objectForKey:@"type"] isEqualToString:@"I"])
        {
            self.addCardLabel.hidden = YES;
            self.addAccountLabel.hidden = YES;
            self.removeAccountLabel.hidden = YES;
            self.addAccountTop.constant = 0;
            self.addAcoountsHeight.constant = 0;
            self.removeAccountsHeight.constant = 0;
            self.knpViewTop.constant = 0;
            self.transferButtonTop.constant = -10;
        }else{
            self.knpView.hidden = YES;
            self.addCardLabel.hidden = YES;
            self.addAcccountTopConstraint.constant = 10;
            self.addAccountLabel.hidden = NO;
            self.removeAccountLabel.hidden = NO;
            self.addAccountTop.constant = 25;
            self.addAcoountsHeight.constant = 20;
            self.removeAccountsHeight.constant = 20;
            self.knpViewTop.constant = 0;
            self.transferButtonTop.constant = -15;
        }
    } else if(control.selectedSegmentIndex == 1) {
        [_transferParams setValue:@"O" forKey:@"dstOwner"];
        [_accountParams setValue:@"O" forKey:@"dstOwner"];
        self.knpView.hidden = YES;
        if([[self.transferParams objectForKey:@"type"] isEqualToString:@"I"]) {
            self.addCardLabel.hidden = YES;
            self.addAcccountTopConstraint.constant = 10;
        }
        else {
            self.addCardLabel.hidden = NO;
            self.addAcccountTopConstraint.constant = 40;
        }
        self.addAccountLabel.hidden = NO;
        self.removeAccountLabel.hidden = NO;
        self.addAccountTop.constant = 25;
        self.addAcoountsHeight.constant = 20;
        self.removeAccountsHeight.constant = 20;
        self.knpViewTop.constant = 0;
        self.transferButtonTop.constant = -15;
    } else if(control.selectedSegmentIndex == 2) {
        [_transferParams setValue:@"L" forKey:@"dstOwner"];
        [_accountParams setValue:@"L" forKey:@"dstOwner"];
        self.knpView.hidden = NO;
        self.addCardLabel.hidden = YES;
        self.addAcccountTopConstraint.constant = 10;
        self.addAccountLabel.hidden = NO;
        self.removeAccountLabel.hidden = NO;
        self.addAccountTop.constant = 15;
        self.addAcoountsHeight.constant = 20;
        self.removeAccountsHeight.constant = 20;
        self.knpViewTop.constant = 25;
        self.transferButtonTop.constant = 25;
    }
    [self clearAllDisclamerView];
    [self.currencyDisclamer makeDefault];
    self.transferTypeTopConstraint.constant = 0;
    [self refreshUserAccount];

    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
}

///////// -------- UITableView -----------//////////



   // UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"KnpCellTarnsfer"];
   // cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
   // cell.textLabel.backgroundColor = [UIColor clearColor];
   // TransfersFormViewController
   /* __weak TransferViewController *wSelf = self;
    KnpCell *knpCell = (KnpCell *) cell;
    knpCell.knps = self.checkTarnsferResponse.knpList;
    knpCell.allKnpList = self.checkTarnsferResponse.knpList;
    knpCell.parentVC = self;
    knpCell.sourceAccount = self.source;
    knpCell.targetAccount = self.target;
    knpCell.onKnpSelect = ^(NSString *knpCode, NSString *knpName) {
        if (knpCode && knpName) {
            wSelf.selectedKnpCode = knpCode;
            wSelf.selectedKnpName = knpName;
            [wSelf runCheckTransferWithAmount:self.fromSummTextField.text direction:YES];
        }
    };*/
    


//////// ------- PopupActionList --------/////////

- (NSArray *)sourceAccounts {
    return [self filterAccounts:self.operationAccounts.sourceAccounts isSource:YES];
}

- (NSArray *)targetAccounts {
    if(self.destinationTypeSelector != 0){
    return [self filterAccounts:self.operationAccounts.targetAccounts isSource:NO];
    }else{
     return [self filterAccounts:self.operationAccounts.targetAccounts isSource:YES];
    }
}

- (NSArray *)sourceBalance {
    return [self getBalancesFromAccaunt:self.source isSource:YES];
}

- (NSArray *)targetBalance {
    return [self getBalancesFromAccaunt:self.target isSource:NO];
}

- (void)tapToAccountAction:(id)sender
{
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
    if([tapRecognizer.view tag] == 1){
        [self openAccountsActionSheet:[self sourceAccounts] tag:[tapRecognizer.view tag]];
    }else{
        [self openAccountsActionSheet:[self targetAccounts] tag:[tapRecognizer.view tag]];
    }
}

- (void)tapToBalanceAction:(id)sender
{
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
    if([tapRecognizer.view tag] == 0){
        [self openBalanceAndCurrencyActionSheet:[self sourceBalance] tag:[tapRecognizer.view tag]];
    }else if([tapRecognizer.view tag] == 1){
        [self openBalanceAndCurrencyActionSheet:[self targetBalance] tag:[tapRecognizer.view tag]];
    }
}

- (NSArray *)filterAccounts:(Accounts *)accounts isSource:(BOOL)isSource{
    NSMutableArray *filteredSourceAccounts = [[NSMutableArray alloc] init];
    //if(isSource){
    if (accounts.cards.count) {
        NSMutableDictionary *cardsSection = [[NSMutableDictionary alloc] init];
        cardsSection[@"title"] = @"Карты";
        cardsSection[@"rows"] = accounts.cards;
        [filteredSourceAccounts addObject:cardsSection];
    }
    if (accounts.currents.count) {
        NSMutableDictionary *currentsSection = [[NSMutableDictionary alloc] init];
        currentsSection[@"title"] = @"Счета";
        currentsSection[@"rows"] = accounts.currents;
        [filteredSourceAccounts addObject:currentsSection];
    }
    if (accounts.deposits.count) {
        NSMutableDictionary *depositSection = [[NSMutableDictionary alloc] init];
        depositSection[@"title"] = @"Депозиты";
        depositSection[@"rows"] = accounts.deposits;
        [filteredSourceAccounts addObject:depositSection];
    }
    if (accounts.extCards.count) {
        NSMutableDictionary *extCardSection = [[NSMutableDictionary alloc] init];
        extCardSection[@"title"] = @"Карты других банков";
        extCardSection[@"rows"] = accounts.extCards;
        [filteredSourceAccounts addObject:extCardSection];
    }
        if (accounts.beneficiars.count) {
            
            NSArray *beneficiarsForTransfersToOther = accounts.beneficiars;
            
            NSMutableDictionary *beneficiarsSection = [[NSMutableDictionary alloc] init];
            beneficiarsSection[@"title"] = @"Счет зачисления";
            beneficiarsSection[@"rows"] = beneficiarsForTransfersToOther;
            [filteredSourceAccounts addObject:beneficiarsSection];
        }
    return filteredSourceAccounts;
}


- (void)openBalanceAndCurrencyActionSheet:(NSArray *)array tag:(NSInteger)tag {
    AccountBalanceSheetViewController *p = [[AccountBalanceSheetViewController alloc] initWithBalanceAndCurrencies:array];
    p.delegate = self;
    p.view.tag = tag;
    p.titleString = tag == 0 ? @"Выберите валюту списания" : @"Выберите валюту зачисления";
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:p];
    popupController.dismissOnBackgroundTap = true;
    popupController.style = STPopupStyleBottomSheet;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [popupController presentInViewController:self];
}

- (void)openAccountsActionSheet:(NSArray *)array tag:(NSInteger)tag
{
    AccountsActionSheetViewController *p = [self getAccountsActionSheet:array tag:tag];
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:p];
    popupController.dismissOnBackgroundTap = true;
    popupController.style = STPopupStyleBottomSheet;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [popupController presentInViewController:self];
}

-(AccountsActionSheetViewController *)getAccountsActionSheet:(NSArray *)array tag:(NSInteger)tag
{
    AccountsActionSheetViewController *p = [[AccountsActionSheetViewController alloc] initWithAccounts:array];
    p.delegate = self;
    p.view.tag = tag;
    p.titleString = tag == 0 ? @"Выберите счет списания" : @"Выберите счет зачисления";
    return p;
}

- (void)actionSheet:(AccountsActionSheetViewController *)actionSheet didSelectedAccount:(Account *)account
{
    if(actionSheet.view.tag == 1)
    {
        [self setSelectedSource:account];
        [self.fromCardImage sd_setImageWithURL:[[NSURL alloc] initWithString:account.logo] placeholderImage:[UIImage imageNamed:@"img_placeholder.png"]];
        self.fromCardlabel.text = account.alias;
        [self emptyFormHiddenForRequestor:YES];
        if(![account.type isEqualToString:@"ext_card"]){
            self.fromCardBalance.text = [account balanceAvailableWithCurrency];
        }else{
            self.fromCardBalance.text = @"";
        }
        self.fromCardNumber.text = account.number;

    }else if(actionSheet.view.tag == 2)
    {
       [self setSelectedTarget:account];
       [self.destinationCardImage sd_setImageWithURL:[[NSURL alloc] initWithString:account.logo] placeholderImage:[UIImage imageNamed:@"img_placeholder.png"]];
        
        if(![[_accountParams objectForKey:@"dstOwner"] isEqualToString:@"S"]){
            if((account.number.length == 0))
            {
                self.emptyFormOrExtCardLabel.text = account.alias;
                [self emptyFormHiddenForDestination:NO];
            }else{
                self.destinationCardLabel.text = account.alias;
                self.destinationCardNumber.text = account.number;
                [self emptyFormHiddenForDestination:YES];
            }
        }else{
             self.destinationCardLabel.text = account.alias;
            self.destinationCardBalance.text = [account balanceAvailableWithCurrency];
            self.destinationCardNumber.text = account.number;
            [self emptyFormHiddenForDestination:YES];
        }
        
       /*     self.destinationCardLabel.text = account.alias;
       if([[_accountParams objectForKey:@"dstOwner"] isEqualToString:@"S"]) self.destinationCardBalance.text = [account balanceAvailableWithCurrency];
        self.destinationCardNumber.text = account.number;*/
    }
    [actionSheet dismissViewControllerAnimated:YES completion:nil];
    if(self.transferButton.mainButtonStyle == MainButtonStyleOrange) self.transferButton.mainButtonStyle = MainButtonStyleDisable;
    /*if([[_transferParams objectForKey:@"dstOwner"] isEqualToString:@"L"] && (!self.selectedKnpCode || !self.selectedKnpName))
    {
        return;
    }else */if(self.source && self.target && self.sourceBalanceProperty && self.transferSumm.length > 0)
    {
        [self runCheckTransferWithAmount:self.transferSumm direction:direction];
    }

}

- (void)accountBalanceSheet:(AccountBalanceSheetViewController *)balanceSheet didSelectedBalance:(id)selectedBalance
{
    if (balanceSheet.view.tag == 0) {
        self.sourceBalanceProperty = selectedBalance;
        self.fromCurrencyLabel.text = selectedBalance[@"currency"];
    } else if (balanceSheet.view.tag == 1) {
        self.targetBalanceProperty = selectedBalance;
        self.destinationCurrencyLabel.text = selectedBalance[@"currency"];
    }
    [balanceSheet dismissViewControllerAnimated:YES completion:nil];
   /* if([[_transferParams objectForKey:@"dstOwner"] isEqualToString:@"L"] && (!self.selectedKnpCode || !self.selectedKnpName))
    {
        return;
    }else */if (self.fromSummTextField.text.length > 0 || self.destinationSummTextField.text.length > 0) {
        [self runCheckTransferWithAmount:self.transferSumm direction:direction];
    }
}

- (void)emptyFormHiddenForRequestor:(BOOL)hidden
{
        self.emptyFormRequestorLabel.hidden = hidden;
        self.fromCardlabel.hidden = !hidden;
}

- (void)emptyFormHiddenForDestination:(BOOL)hidden
{
    self.emptyFormOrExtCardLabel.hidden = hidden;
    self.destinationCardLabel.hidden = !hidden;
}

- (void)clearRequestorForm
{
    self.fromCardImage.image = [UIImage imageNamed:@"icn_exp_baf_card"];
    self.fromCardlabel.text = @"Выберите счет для списания";
    self.fromCardNumber.text = nil;
    self.fromCardBalance.text = nil;
    self.fromCurrencyLabel.text = nil;
    self.fromSummTextField.text = nil;
    self.destinationSummTextField.text = nil;
    self.source = nil;
    self.transferSumm = nil;
    self.sourceBalanceProperty = nil;
    [self emptyFormHiddenForRequestor:NO];
    [self clearDestinationForm];
}

- (void)clearDestinationForm
{
    self.destinationCardImage.image = [UIImage imageNamed:@"icn_exp_baf_card"];
    self.destinationCardLabel.text = @"Выберите счет для пополнения";
    self.destinationCardNumber.text = nil;
    self.destinationCardBalance.text = nil;
    self.destinationCurrencyLabel.text = nil;
    self.destinationSummTextField.text = nil;
  //  self.fromSummTextField.text = nil;
    self.target = nil;
    self.emptyFormOrExtCardLabel.text = @"Выберите счет для пополнения";
    [self emptyFormHiddenForDestination:NO];
    self.targetBalanceProperty = nil;
    if(!self.knpView.hidden) self.knpView.knpChooseLabel.text = @"Выберите код назначения платежа";
}

- (void)runCheckTransferWithAmount:(NSString *)amountString direction:(BOOL)direction {
    if (self.source && self.sourceBalanceProperty[@"currency"] && self.target && self.targetBalanceProperty[@"currency"] && amountString.length > 0) {
       
        [MBProgressHUD showAstanaHUDWithTitle:@"Проверка" animated:YES];
        
        [self newCheckTransferSourceAccount:self.source
                                 selectedSourceCurrrency:self.sourceBalanceProperty[@"currency"]//todo: create model better
                                           targetAccount:self.target
                                  selectedTargetCurrency:self.targetBalanceProperty[@"currency"]
                                                  amount:[amountString numberDecimalFormat]
                                          isSaveTemplate:NO
                                           templateAlias:nil
                                               direction:direction];
    }
}

- (void)initTransfer
{
    [MBProgressHUD showAstanaHUDWithTitle:@"Инициализация трансфера" animated:YES];
    
    
}

- (void)setSelectedSource:(Account *)sourceAccount {
    self.source = sourceAccount;
    [self.fromCurrencyTapView setUserInteractionEnabled:YES];
    self.fromCurrencyLabel.text = sourceAccount.currency;
    
    NSArray *balances = [self getBalancesFromAccaunt:sourceAccount isSource:YES];
    if (balances.count > 0) {
        self.sourceBalanceProperty = [balances firstObject];
    } else {
        self.sourceBalanceProperty = @{@"currency" : sourceAccount.currency};
    }
}

- (void)setSelectedTarget:(Account *)targetAccount {
    self.target = targetAccount;
    [self.destinationCurrencyTapView setUserInteractionEnabled:YES];
    self.destinationCurrencyLabel.text = targetAccount.currency;
    
    NSArray *balances = [self getBalancesFromAccaunt:targetAccount isSource:NO];
    if (balances.count > 0) {
        self.targetBalanceProperty = [balances firstObject];
    } else {
        self.targetBalanceProperty = @{@"currency" : targetAccount.currency};
    }
}

- (NSArray *)getBalancesFromAccaunt:(Account *)account isSource:(BOOL)isSource {
    NSMutableArray *balances = [[NSMutableArray alloc] init];
    if (account.isMulty) {
        if (isSource) {
            if ([account.balanceKzt doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceKzt decimalFormatString], @"currency" : @"KZT"}];
            }
            if ([account.balanceUsd doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceUsd decimalFormatString], @"currency" : @"USD"}];
            }
            if ([account.balanceEur doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceEur decimalFormatString], @"currency" : @"EUR"}];
            }
            if ([account.balanceRub doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceRub decimalFormatString], @"currency" : @"RUB"}];
            }
            if ([account.balanceGbp doubleValue] > 0) {
                [balances addObject:@{@"balance" : [account.balanceGbp decimalFormatString], @"currency" : @"GBP"}];
            }
        } else {
            [balances addObject:@{@"balance" : [account.balanceKzt decimalFormatString], @"currency" : @"KZT"}];
            [balances addObject:@{@"balance" : [account.balanceUsd decimalFormatString], @"currency" : @"USD"}];
            [balances addObject:@{@"balance" : [account.balanceEur decimalFormatString], @"currency" : @"EUR"}];
            [balances addObject:@{@"balance" : [account.balanceRub decimalFormatString], @"currency" : @"RUB"}];
            [balances addObject:@{@"balance" : [account.balanceGbp decimalFormatString], @"currency" : @"GBP"}];
        }
        return balances;
    } else {
        if (account.balance) {
            [balances addObject:@{@"balance" : [account.balance decimalFormatString], @"currency" : account.currency}];
            return balances;
        } else {
            return nil;
        }
    }
    return nil;
}

#pragma mark Cheking //////////////////////////////////

- (void)newCheckTransferSourceAccount:(Account *)sourceAccount
           selectedSourceCurrrency:(NSString *)selectedSourceCurrency
                     targetAccount:(Account *)targetAccount
            selectedTargetCurrency:(NSString *)selectedTargetCurrency
                            amount:(NSNumber *)amount
                    isSaveTemplate:(BOOL)isSaveTemplate
                     templateAlias:(NSString *)templateAlias
                         direction:(BOOL)direction
{
    [NewTransfersApi newCheckTransfer:nil
                               params:_transferParams
                          requestorId:sourceAccount.accountId
                               amount:amount
                        destinationId:targetAccount.accountId
                        requestorType:sourceAccount.type
                    requestorCurrency:selectedSourceCurrency
                      destinationType:targetAccount.type
                  destinationCurrency:selectedTargetCurrency
                              knpCode:self.selectedKnpCode
                              knpName:self.selectedKnpName
                            direction:direction
                                 save:isSaveTemplate
                                alias:templateAlias
                              success:^(NewCheckAndInitTransferResponse *checkTarnsfer) {
                                [MBProgressHUD hideAstanaHUDAnimated:YES];
                                  self.checkTarnsferResponse = checkTarnsfer;
                                  [self setAmountAndCurrency];
                                  self.fromSummTextField.text = [self.checkTarnsferResponse.requestor.accountAmount decimalFormatString];
                                  self.destinationSummTextField.text = [self.checkTarnsferResponse.destination.accountAmount decimalFormatString];
                                  [self setResponseKnpWithDisclamerText];
                                  [self showErrorMessage:self.checkTarnsferResponse.errorMessage];
                            if(self.checkTarnsferResponse.isValid)
                            {
                              if([self.checkTarnsferResponse nextActionTypeIsInit])
                              {
                                  self.transferButton.mainButtonStyle = MainButtonStyleOrange;
                              }
                            }else{
                                if([self.checkTarnsferResponse isIntraBank])
                                {
                                    [self processingForIntraBankTransaction];
                                }else{
                                    [self processingForInterBankTransaction];
                                }
                               /* if(![[_transferParams objectForKey:@"dstOwner"] isEqualToString:@"L"])
                                {
                                    [self clearRequestorForm];
                                }*/
                               // [self clearRequestorForm];
                               // [self showErrorMessage:self.checkTarnsferResponse.errorMessage];
                            }
                           } failure:^(NSString *code, NSString *message) {
                              // [self.output handleCheckTransferFailure:message];
                               [MBProgressHUD hideAstanaHUDAnimated:YES];
                           }];
}

- (void)setResponseKnpWithDisclamerText
{
    self.knpView.knps = [self.checkTarnsferResponse.knpObject objectForKey:@"all"];
    self.knpView.allKnpList = [self.checkTarnsferResponse.knpObject objectForKey:@"all"];
    self.knpView.transferModel = self.checkTarnsferResponse;
    self.knpView.sourceAccount = self.source;
    self.knpView.targetAccount = self.target;
    if(self.knpView.allKnpList.count > 0){
    self.knpView.hidden = NO;
    }
    if(self.knpView.allKnpList.count == 1)
    {
        self.selectedKnpCode = [self.checkTarnsferResponse.knpObject objectForKey:@"code"];
        self.selectedKnpName = [self.checkTarnsferResponse.knpObject objectForKey:@"name"];
        Knp *knp = [self.checkTarnsferResponse.knpList firstObject];
        self.knpView.knpChooseLabel.text = knp.name;
        self.knpViewTop.constant = 25;
        self.transferButtonTop.constant = 25;
    }
    
    [self clearAllDisclamerView];

    if(self.checkTarnsferResponse.warningMessages.count > 0){
    for(int i = 0; i < self.checkTarnsferResponse.warningMessages.count; i++)
    {
        NSString *warningStr = self.checkTarnsferResponse.warningMessages[i];
        DisclamerView *view = [[self disclamerViewArray] objectAtIndex:i];
        [view setText:warningStr];
    }}
   
    if(self.checkTarnsferResponse.amountInfo.exchangeResponse.presentation.length > 0)
    {
        [self.currencyDisclamer setTextFromCurrencyRate:self.checkTarnsferResponse.amountInfo.exchangeResponse];
        self.transferTypeTopConstraint.constant = 15;
    }else{
        [self.currencyDisclamer makeDefault];
        self.transferTypeTopConstraint.constant = 0;
    }
    
}

- (void)clearAllDisclamerView
{
    for(DisclamerView *view in [self disclamerViewArray])
    {
        [view makeDefault];
    }
   
}


- (void)setAmountAndCurrency
{
    //self.fromSummTextField.text = [self.checkTarnsferResponse.requestor.accountAmount decimalFormatString];
    self.fromCurrencyLabel.text = self.checkTarnsferResponse.requestor.currency;
    
    //self.destinationSummTextField.text = [self.checkTarnsferResponse.destination.accountAmount decimalFormatString];
    self.destinationCurrencyLabel.text = self.checkTarnsferResponse.destination.currency;
}

- (void)loadKnpObjects
{
    [MBProgressHUD showAstanaHUDWithTitle:@"Получение данных" animated:YES];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    //params[@"code"] = @"119";
    //params[@"dstOwner"] = @"S";
    [NewTransfersApi getKnpWithParams:params Success:^(Knp *knpResponse)
    {
         [MBProgressHUD hideAstanaHUDAnimated:YES];
        //self.selectedKnpCode = knpResponse.code;
    } failure:^(NSString *code, NSString *message) {
        // [self.output handleCheckTransferFailure:message];
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

/*- (IBAction)textFieldDidEndChange:(id)sender {
    /*if([[_transferParams objectForKey:@"dstOwner"] isEqualToString:@"L"] && (!self.selectedKnpCode || !self.selectedKnpName))
    {
        return;
    }else if(_fromSummTextField.text.length > 0)
    {
      [self runCheckTransferWithAmount:self.fromSummTextField.text direction:YES];
    }
}*/

////// Обработка внутри банка
- (void)processingForIntraBankTransaction
{
    
}

////// Обработка меж. банка
- (void)processingForInterBankTransaction
{
    self.source = self.checkTarnsferResponse.requestor.account;
    self.target = self.checkTarnsferResponse.destination.account;
   if(self.checkTarnsferResponse.isPayBox)
   {
       if([self.checkTarnsferResponse selfRequestorAccountCVV])
       {
           CVVEnterViewController *v = [CVVEnterViewController createVC];
           v.delegate = self;
           v.warningMessage = self.checkTarnsferResponse.requestor.cvcModel.messageString;
           v.account = self.checkTarnsferResponse.requestor.account;
           v.publicKey = self.checkTarnsferResponse.gateWay.publicKey;
           STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:v];
           popupController.dismissOnBackgroundTap = true;
           popupController.transitionStyle = STPopupTransitionStyleFade;
           popupController.style = STPopupStyleFormSheet;
           popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
           [popupController presentInViewController:self];

       }else if([self.checkTarnsferResponse selfDestinationAccountCVV])
       {
           CVVEnterViewController *v = [CVVEnterViewController createVC];
           v.delegate = self;
           v.warningMessage = self.checkTarnsferResponse.destination.cvcModel.messageString;
           v.account = self.checkTarnsferResponse.destination.account;
           v.publicKey = self.checkTarnsferResponse.gateWay.publicKey;
           STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:v];
           popupController.dismissOnBackgroundTap = true;
           popupController.transitionStyle = STPopupTransitionStyleFade;
           popupController.style = STPopupStyleFormSheet;
           popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
           [popupController presentInViewController:self];
       }
   }
}
- (IBAction)destinationTextFieldExit:(id)sender
{
    if(self.destinationSummTextField.text.length > 0)
    {
        self.fromSummTextField.text = nil;//self.destinationSummTextField.text;
        direction = NO;
        self.transferSumm = self.destinationSummTextField.text;
        [self runCheckTransferWithAmount:self.transferSumm direction:direction];
    }
}

- (NSArray *)disclamerViewArray
{
    return @[self.disclaimerView, self.disclamerView1, self.disclamerView3];
}

- (IBAction)textFieldExit:(id)sender
{
    if(_fromSummTextField.text.length > 0)
    {
        self.destinationSummTextField.text = nil;//_fromSummTextField.text;
        direction = YES;
        self.transferSumm = self.fromSummTextField.text;
        [self runCheckTransferWithAmount:self.transferSumm direction:direction];
    }
}

- (void)showErrorMessage:(NSString *)errorMessage {
    [Toast showToast:errorMessage];
}

///////////////////////////////////////////////////////
- (void)configUI {
    [self setNavigationTitle:NSLocalizedString(@"ga_transfers", nil)];
    [self setNavigationBackButton];
    [self setBafBackground];
    
    self.scrollView.delegate = self;
    [self showMenuButton];
//    UITapGestureRecognizer *addedKnpToTransfer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToAppointmentPayment)];
    
    UITapGestureRecognizer *accountListTapFrom =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(tapToAccountAction:)];
    
    UITapGestureRecognizer *accountListTapDestination =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(tapToAccountAction:)];
    
    UITapGestureRecognizer *balanceListTapFrom =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(tapToBalanceAction:)];
    
    UITapGestureRecognizer *balanceListTapDestination =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(tapToBalanceAction:)];
    
    UITapGestureRecognizer *addSourceCardGR =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(sourceExtCardSelected)];
    
    UITapGestureRecognizer *addTargetCardGR =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(targetExtCardSelected)];
    
    UITapGestureRecognizer *addAccountGR =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(openSelectBeneficiary)];
    
    UITapGestureRecognizer *removeAccountGR =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(openRemoveBeneficiary)];
    
    self.accountFromView.layer.cornerRadius = 2;
    self.accountFromView.layer.borderWidth = 0.5;
    self.accountFromView.layer.borderColor = [UIColor colorWithRed:152.0/255.0 green:152.0/255.0 blue:152.0/255.0 alpha:1].CGColor;
    self.accountFromView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.accountFromView.layer.shadowOffset = CGSizeMake(0, 2);
    self.accountFromView.layer.shadowOpacity = 0.3;
    self.accountFromView.tag = 1;
    [self.accountFromView addGestureRecognizer:accountListTapFrom];
    
   // self.fromCardlabel.hidden = YES;
   // self.emptyFormRequestorLabel.hidden = NO;
    [self emptyFormHiddenForRequestor:NO];
    [self emptyFormHiddenForDestination:NO];
    self.fromCardNumber.text = nil;
    self.fromCardBalance.text = nil;
    self.destinationCardNumber.text = nil;
    self.destinationCardBalance.text = nil;
    
    self.TransferFromSummView.layer.cornerRadius = 2;
    self.TransferFromSummView.layer.borderWidth = 0.5;
    self.TransferFromSummView.layer.borderColor = [UIColor colorWithRed:152.0/255.0 green:152.0/255.0 blue:152.0/255.0 alpha:1].CGColor;
    self.TransferFromSummView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.TransferFromSummView.layer.shadowOffset = CGSizeMake(0, 2);
    self.TransferFromSummView.layer.shadowOpacity = 0.3;
    
    self.destinationAccountView.layer.cornerRadius = 2;
    self.destinationAccountView.layer.borderWidth = 0.5;
    self.destinationAccountView.layer.borderColor = [UIColor colorWithRed:152/255.0 green:152/255.0 blue:152/255.0 alpha:1].CGColor;
    self.destinationAccountView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.destinationAccountView.layer.shadowOffset = CGSizeMake(0, 2);
    self.destinationAccountView.layer.shadowOpacity = 0.3;
    self.destinationAccountView.tag = 2;
    [self.destinationAccountView addGestureRecognizer:accountListTapDestination];
    
    self.transferDestinationSummView.layer.cornerRadius = 2;
    self.transferDestinationSummView.layer.borderWidth = 0.5;
    self.transferDestinationSummView.layer.borderColor = [UIColor colorWithRed:152/255.0 green:152/255.0 blue:152/255.0 alpha:1].CGColor;
    self.transferDestinationSummView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.transferDestinationSummView.layer.shadowOffset = CGSizeMake(0, 2);
    self.transferDestinationSummView.layer.shadowOpacity = 0.3;
    
    self.fromSummTextField.placeholder = @"Сумма списания";
    self.fromSummTextField.keyboardType = UIKeyboardTypeDecimalPad;
   // self.fromSummTextField.tag = 1;
    self.destinationSummTextField.placeholder = @"Сумма зачисления";
    self.destinationSummTextField.keyboardType = UIKeyboardTypeDecimalPad;
   // self.destinationSummTextField.tag = 2;
    
    self.fromCurrencyLabel.text = @"Валюта";
    self.fromCurrencyTapView.tag = 0;
    self.destinationCurrencyLabel.text = @"Валюта";
    self.destinationCurrencyTapView.tag = 1;
    [self.destinationCurrencyTapView addGestureRecognizer:balanceListTapDestination];
    [self.destinationCurrencyTapView setUserInteractionEnabled:NO];
    [self.fromCurrencyTapView addGestureRecognizer:balanceListTapFrom];
    [self.fromCurrencyTapView setUserInteractionEnabled:NO];
    
    [self.transferTypeSelector addTarget:self action:@selector(transferSelectorButton:) forControlEvents:UIControlEventValueChanged];
    self.transferTypeSelector.selectedSegmentIndex = 0;
    [self.destinationTypeSelector addTarget:self action:@selector(destinationSelectorButton:) forControlEvents:UIControlEventValueChanged];
    self.destinationTypeSelector.selectedSegmentIndex = 0;
    self.transferTypeTopConstraint.constant = 0;
    [self transferSelectorButton:self.transferTypeSelector];
    self.transferButton.mainButtonStyle = MainButtonStyleDisable;
   
    [self.addSourceCardLabel setUserInteractionEnabled:YES];
    [self.addSourceCardLabel addGestureRecognizer:addSourceCardGR];
    
    [self.addCardLabel setUserInteractionEnabled:YES];
    [self.addCardLabel addGestureRecognizer:addTargetCardGR];
    
    [self.addAccountLabel setUserInteractionEnabled:YES];
    [self.addAccountLabel addGestureRecognizer:addAccountGR];
    
    [self.removeAccountLabel setUserInteractionEnabled:YES];
    [self.removeAccountLabel addGestureRecognizer:removeAccountGR];
    
    __weak TransferViewController *wSelf = self;
    self.knpView.transferModel = self.checkTarnsferResponse;
    self.knpView.knps = [self.checkTarnsferResponse.knpObject objectForKey:@"all"];
    self.knpView.allKnpList = [self.checkTarnsferResponse.knpObject objectForKey:@"all"];
    self.knpView.parentVC = self;
    self.knpView.sourceAccount = self.source;
    self.knpView.targetAccount = self.target;
    self.knpView.layer.borderWidth = 0.5;
    self.knpView.layer.borderColor = [UIColor grayColor].CGColor;
    self.knpView.onKnpSelect = ^(NSString *knpCode, NSString *knpName) {
        if (knpCode && knpName) {
            self.selectedKnpCode = knpCode;
            self.selectedKnpName = knpName;
            if(knpCode.length > 0 && knpName.length > 0)  [wSelf runCheckTransferWithAmount:self.transferSumm direction:direction];
        }
    };
    
    self.knpView.hidden = YES;
    [self.currencyDisclamer makeDefault];
    [self.disclaimerView makeDefault];
    [self.disclamerView1 makeDefault];
    [self.disclamerView3 makeDefault];
    
    /////////////Disclamer
    
    //self.currencyDisclamer.heightConstraint.constant = 0;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshUserAccount) name:kNotificationTransferRetailBeneficiarAdded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshUserAccount) name:kNotificationTransferCorporateBeneficiarAdded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshUserAccount) name:kNotificationUserUpdatedBeneficiaryAccount object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
     [[NSNotificationCenter defaultCenter] removeObserver:self];
    // Dispose of any resources that can be recreated.
}



@end
