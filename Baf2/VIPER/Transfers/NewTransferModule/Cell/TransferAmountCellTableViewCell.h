//
//  TransferAmountCellTableViewCell.h
//  Baf2
//
//  Created by developer on 22.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransferAmountCellTableViewCell : UITableViewCell

-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

- (void)setTitleForTransferSumm:(NSString *)title comissionSumm:(NSString *)commisionTitle finalSumm:(NSString *)finalTitle;
- (void)setSubtitleForTransferSumm:(NSString *)transferSubtitle commissionSubtitle:(NSString *)commissionTitle finalSubtitle:(NSString *)finalSubtitle;

- (void)setTitleTextColor:(UIColor *)color;
- (void)setSubtitleTextColor:(UIColor *)color;

+ (CGFloat)cellHeightWithTitle:(NSString *)title;
+ (CGFloat)cellHeightWithTitle:(NSString *)title subtitle:(NSString *)subtitle;
@end
