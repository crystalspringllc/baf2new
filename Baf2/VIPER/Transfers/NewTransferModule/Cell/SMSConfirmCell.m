//
//  SMSConfirmCell.m
//  Baf2
//
//  Created by developer on 22.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "SMSConfirmCell.h"
#import "InputTextField.h"
#import "UIView+AAPureLayout.h"
#import "NSString+Validators.h"
#import "MainButton.h"

@implementation SMSConfirmCell
{
    UITableView *list;
    UIView *footerButtonView;
    InputTextField *codeField;
    BOOL viewDidAppear;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self configUI];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configUI{
    codeField = [[InputTextField alloc] initWithFrame:CGRectMake(15, 8, [ASize screenWidth] - 2 * 15, 44)];
    [codeField.textField setKeyboardType:UIKeyboardTypeNumberPad];
    [codeField setPlaceholder:NSLocalizedString(@"enter_codee", nil)];
    [codeField setIcon:@"icon-hash"];
    [self.contentView addSubview:codeField];
    
    // Resend button
    UIButton *resendSmsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 240, 30)];
    [resendSmsButton addTarget:self action:@selector(resendSmsTapped) forControlEvents:UIControlEventTouchUpInside];
    NSAttributedString *buttonTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"send_code_again", nil)
                                                                      attributes:@{
                                                                                   NSForegroundColorAttributeName: [UIColor fromRGB:0x146888],
                                                                                   NSFontAttributeName: [UIFont systemFontOfSize:14],
                                                                                   NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                                                                                   }];
    [resendSmsButton setAttributedTitle:buttonTitle forState:UIControlStateNormal];
    resendSmsButton.centerX = (CGFloat) ([ASize screenWidth] * 0.5);
    resendSmsButton.y = codeField.bottom + 8;
    [self.contentView addSubview:resendSmsButton];
}

@end
