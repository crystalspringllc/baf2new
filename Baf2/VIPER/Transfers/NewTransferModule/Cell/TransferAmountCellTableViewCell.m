//
//  TransferAmountCellTableViewCell.m
//  Baf2
//
//  Created by developer on 22.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "TransferAmountCellTableViewCell.h"
#import "NSString+Ext.h"
#import "UIColor+Extensions.h"

#define kCellPadding 15
#define kCellTopPadding 8
#define kCellTitleSubtitleSpacing 3

#define kTitleFont [UIFont systemFontOfSize:11]
#define kSubtitleFont [UIFont boldSystemFontOfSize:14]

@implementation TransferAmountCellTableViewCell
{
UILabel *transferSummTitleLabel;
UILabel *subtitleTransferSummLabel;
UILabel *commissionSummTitleLabel;
UILabel *subtitleCommissionSummLabel;
UILabel *finalSummTitleLabel;
UILabel *subtitleFinalSummLabel;
}

-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if(self) {
        
        transferSummTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        transferSummTitleLabel.x = kCellPadding;
        transferSummTitleLabel.y = kCellTopPadding;
        transferSummTitleLabel.backgroundColor = [UIColor clearColor];
        transferSummTitleLabel.font = kTitleFont;
        transferSummTitleLabel.textColor = [UIColor grayColor];
        transferSummTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        transferSummTitleLabel.numberOfLines = 0;
        [self addSubview:transferSummTitleLabel];
        
        subtitleTransferSummLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        subtitleTransferSummLabel.x = kCellPadding;
        subtitleTransferSummLabel.backgroundColor = [UIColor clearColor];
        subtitleTransferSummLabel.font = kSubtitleFont;
        subtitleTransferSummLabel.textColor = [UIColor blackColor];
        subtitleTransferSummLabel.lineBreakMode = NSLineBreakByWordWrapping;
        subtitleTransferSummLabel.numberOfLines = 0;
        [self addSubview:subtitleTransferSummLabel];
        
        commissionSummTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        commissionSummTitleLabel.x = 2*kCellPadding+[TransferAmountCellTableViewCell contentWidth]/3;
        commissionSummTitleLabel.y = kCellTopPadding;
        commissionSummTitleLabel.backgroundColor = [UIColor clearColor];
        commissionSummTitleLabel.font = kTitleFont;
        commissionSummTitleLabel.textColor = [UIColor grayColor];
        commissionSummTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        commissionSummTitleLabel.numberOfLines = 0;
        [self addSubview:commissionSummTitleLabel];
        
        subtitleCommissionSummLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        subtitleCommissionSummLabel.x = 2*kCellPadding+[TransferAmountCellTableViewCell contentWidth]/3;
        subtitleCommissionSummLabel.backgroundColor = [UIColor clearColor];
        subtitleCommissionSummLabel.font = kSubtitleFont;
        subtitleCommissionSummLabel.textColor = [UIColor blackColor];
        subtitleCommissionSummLabel.lineBreakMode = NSLineBreakByWordWrapping;
        subtitleCommissionSummLabel.numberOfLines = 0;
        [self addSubview:subtitleCommissionSummLabel];
        
        finalSummTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        finalSummTitleLabel.x = kCellPadding*2+[TransferAmountCellTableViewCell contentWidth] * 2/3;
        finalSummTitleLabel.y = kCellTopPadding;
        finalSummTitleLabel.backgroundColor = [UIColor clearColor];
        finalSummTitleLabel.font = kTitleFont;
        finalSummTitleLabel.textColor = [UIColor grayColor];
        finalSummTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        finalSummTitleLabel.numberOfLines = 0;
        [self addSubview:finalSummTitleLabel];
        
        subtitleFinalSummLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        subtitleFinalSummLabel.x = kCellPadding*2+[TransferAmountCellTableViewCell contentWidth] * 2/3;
        subtitleFinalSummLabel.backgroundColor = [UIColor clearColor];
        subtitleFinalSummLabel.font = kSubtitleFont;
        subtitleFinalSummLabel.textColor = [UIColor blackColor];
        subtitleFinalSummLabel.lineBreakMode = NSLineBreakByWordWrapping;
        subtitleFinalSummLabel.numberOfLines = 0;
        [self addSubview:subtitleFinalSummLabel];
        
    }
    return self;
}

- (void)setTitleForTransferSumm:(NSString *)title comissionSumm:(NSString *)commisionTitle finalSumm:(NSString *)finalTitle
{
   // transferSummTitleLabel.width = [TransferAmountCellTableViewCell contentWidth]/3;
    transferSummTitleLabel.text = title;
    [transferSummTitleLabel sizeToFit];
    
   // commissionSummTitleLabel.width = [TransferAmountCellTableViewCell contentWidth]/3;
    commissionSummTitleLabel.text = commisionTitle;
    [commissionSummTitleLabel sizeToFit];
    
   // finalSummTitleLabel.width = [TransferAmountCellTableViewCell contentWidth]/3;
    finalSummTitleLabel.text = finalTitle;
    [finalSummTitleLabel sizeToFit];
    
    if(subtitleTransferSummLabel.text.length > 0) {
        subtitleTransferSummLabel.y = transferSummTitleLabel.bottom + kCellTitleSubtitleSpacing;
    }
    
    if(subtitleCommissionSummLabel.text.length > 0) {
        subtitleCommissionSummLabel.y = commissionSummTitleLabel.bottom + kCellTitleSubtitleSpacing;
    }
    
    if(subtitleFinalSummLabel.text.length > 0) {
        subtitleFinalSummLabel.y = finalSummTitleLabel.bottom + kCellTitleSubtitleSpacing;
    }
}

- (void)setSubtitleForTransferSumm:(NSString *)transferSubtitle commissionSubtitle:(NSString *)commissionTitle finalSubtitle:(NSString *)finalSubtitle
{
    subtitleTransferSummLabel.width = [TransferAmountCellTableViewCell contentWidth]/3;
    subtitleTransferSummLabel.text = transferSubtitle;
    subtitleTransferSummLabel.textColor = [UIColor deleteColor];
    [subtitleTransferSummLabel sizeToFit];
    
    if(subtitleTransferSummLabel.text.length > 0) {
        subtitleTransferSummLabel.y = transferSummTitleLabel.bottom + kCellTitleSubtitleSpacing;
    } else {
        subtitleTransferSummLabel.y = kCellTopPadding;
    }
    
    subtitleCommissionSummLabel.width = [TransferAmountCellTableViewCell contentWidth]/3;
    subtitleCommissionSummLabel.textColor = [UIColor deleteColor];
    subtitleCommissionSummLabel.text = commissionTitle;
    [subtitleCommissionSummLabel sizeToFit];
    
    if(subtitleCommissionSummLabel.text.length > 0) {
        subtitleCommissionSummLabel.y = commissionSummTitleLabel.bottom + kCellTitleSubtitleSpacing;
    } else {
        subtitleCommissionSummLabel.y = kCellTopPadding;
    }
    
    subtitleFinalSummLabel.width = [TransferAmountCellTableViewCell contentWidth]/3;
    subtitleFinalSummLabel.text = finalSubtitle;
    subtitleFinalSummLabel.textColor = [UIColor deleteColor];
    [subtitleFinalSummLabel sizeToFit];
    
    if(subtitleFinalSummLabel.text.length > 0) {
        subtitleFinalSummLabel.y = finalSummTitleLabel.bottom + kCellTitleSubtitleSpacing;
    } else {
        subtitleFinalSummLabel.y = kCellTopPadding;
    }


}


- (void)setTitleTextColor:(UIColor *)color
{
    for (UILabel* label in [self arrayForTitle]) {
       if(label) label.textColor = color;
    }
}

- (void)setSubtitleTextColor:(UIColor *)color
{
    for (UILabel* label in [self arrayForSubtitle]) {
        if(label) label.textColor = color;
    }
}


+ (CGFloat)cellHeightWithTitle:(NSString *)title
{
    return [self cellHeightWithTitle:title subtitle:nil];
}

- (NSArray *)arrayForTitle
{
    return @[transferSummTitleLabel, commissionSummTitleLabel, finalSummTitleLabel];
}

- (NSArray *)arrayForSubtitle
{
    return @[subtitleTransferSummLabel, subtitleCommissionSummLabel, subtitleFinalSummLabel];
}

+ (CGFloat)cellHeightWithTitle:(NSString *)title subtitle:(NSString *)subtitle
{
    if (![title isKindOfClass:[NSString class]]) {
        title = [NSString stringWithFormat:@"%@", title];
    }
    
    if (![subtitle isKindOfClass:[NSString class]]) {
        subtitle = [NSString stringWithFormat:@"%@", subtitle];
    }
    
    NSDictionary *attributes = @{NSFontAttributeName:kTitleFont};
    CGRect rect = [title boundingRectWithSize:CGSizeMake([self contentWidth], CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributes
                                      context:nil];
    
    CGFloat h = rect.size.height + 2 * kCellTopPadding;
    
    if(subtitle && [subtitle trim].length > 0) {
        NSDictionary *attributes2 = @{NSFontAttributeName: kSubtitleFont};
        CGRect rect2 = [subtitle boundingRectWithSize:CGSizeMake([self contentWidth], CGFLOAT_MAX)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:attributes2
                                              context:nil];
        
        h = rect.size.height + kCellTitleSubtitleSpacing + rect2.size.height + 2 * kCellTopPadding;
    }
    
    return h;
}

+ (CGFloat)contentWidth {
    return [ASize screenWidth] - 2 * kCellPadding;
}

@end
