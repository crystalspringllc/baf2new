//
//  KnpView.h
//  Baf2
//
//  Created by developer on 01.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FloatingTextField.h"
#import "NewCheckAndInitTransferResponse.h"

@class Account;

@interface KnpView : UIView <UITextFieldDelegate>
@property (nonatomic, strong) NSArray *knps;
@property (nonatomic, strong) NSArray *allKnpList;
@property (nonatomic, weak) UIViewController *parentVC;

@property (nonatomic, weak) NewCheckAndInitTransferResponse *transferModel;

@property (nonatomic, weak) IBOutlet UIView *knpChooseContainerView;

@property (nonatomic, weak) IBOutlet UILabel *knpChooseLabel;
@property (nonatomic, weak) IBOutlet UIButton *chooseKnpButton;


@property (nonatomic, weak) IBOutlet UILabel *knpInfoLabel;
@property (nonatomic, weak) IBOutlet FloatingTextField *knpTextField;
@property (nonatomic, weak) IBOutlet UITextField *knpTargetField;

@property (nonatomic, copy) void (^onKnpSelect)(NSString *knpCode, NSString *knpName);
//@property (nonatomic, copy) void (^onConstraintUpdate)();

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *chooseKnpViewBottomConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *knpTargetFieldBottomConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *knpCodeBottomConstreint;

@property (nonatomic, strong) Account *sourceAccount;
@property (nonatomic, strong) Account *targetAccount;

@end
