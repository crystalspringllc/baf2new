//
//  CurrencyRateDisclamerView.m
//  Baf2
//
//  Created by developer on 21.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "CurrencyRateDisclamerView.h"
@interface CurrencyRateDisclamerView  ()
@property(weak, nonatomic) IBOutlet UILabel *currencyLabel;
@property(weak, nonatomic) IBOutlet UILabel *dateLabel;
@end

@implementation CurrencyRateDisclamerView

-(void)setTextFromCurrencyRate:(TransferCurrencyRates *)currencyRate
{
    self.currencyLabel.text = [currencyRate composeCurrencyRateMessage];
    self.dateLabel.text = [currencyRate composeUpdateTime];
    self.viewHeightConstraint.constant = 80;
    self.hidden = NO;
}

-(void)makeDefault
{
    self.hidden = YES;
    self.viewHeightConstraint.constant = 0;
}

@end
