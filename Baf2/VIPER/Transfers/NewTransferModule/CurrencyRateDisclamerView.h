//
//  CurrencyRateDisclamerView.h
//  Baf2
//
//  Created by developer on 21.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransferCurrencyRates.h"

@interface CurrencyRateDisclamerView : UIView

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeightConstraint;

-(void)setTextFromCurrencyRate:(TransferCurrencyRates *)currencyRate;
-(void)makeDefault;
@end
