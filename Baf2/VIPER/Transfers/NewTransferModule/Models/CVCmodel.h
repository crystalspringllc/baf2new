//
//  CVCmodel.h
//  Baf2
//
//  Created by developer on 04.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface CVCmodel : ModelObject 

@property (nonatomic, assign) BOOL isNeed;
@property (nonatomic, strong) NSString *messageString;
@property (nonatomic, strong) NSString *errorString;

+ (CVCmodel *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;
- (id)copyWithZone:(NSZone *)zone;

@end
