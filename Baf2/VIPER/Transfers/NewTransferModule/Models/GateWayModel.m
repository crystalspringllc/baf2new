//
//  GateAwayModel.m
//  Baf2
//
//  Created by developer on 06.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "GateWayModel.h"

@implementation GateWayModel

+ (GateWayModel *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    GateWayModel *instance = [[GateWayModel alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.publicKey = [self stringValueForKey:@"publicKey"];
    self.backLink = [self stringValueForKey:@"backLink"];
    self.postLink = [self stringValueForKey:@"postLink"];
    self.processLink = [self stringValueForKey:@"processLink"];
    self.terminalId = [self numberValueForKey:@"terminalId"];
    self.securitylink = [self stringValueForKey:@"securityLink"];
    self.securityParams = [self idValueForKey:@"securityParams"];
    
    objectData = nil;
}

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setSecuritylink:[self.securitylink copyWithZone:zone]];
        [copy setSecurityParams:[self.securityParams copyWithZone:zone]];
        [copy setSecurityParams:[self.securityParams copyWithZone:zone]];
        [copy setPublicKey:[self.publicKey copyWithZone:zone]];
        [copy setPostLink:[self.postLink copyWithZone:zone]];
        [copy setBackLink:[self.backLink copyWithZone:zone]];
        [copy setProcessLink:[self.processLink copyWithZone:zone]];
        [copy setTerminalId:[self.terminalId copyWithZone:zone]];
     
    }
    
    return copy;
}

@end
