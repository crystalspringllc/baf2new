//
//  CVVAddResponse.h
//  Baf2
//
//  Created by developer on 23.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface CVVAddResponse : ModelObject

@property (weak, nonatomic) NSString* errorMessage;
@property (weak, nonatomic) NSString* message;
@property (assign, nonatomic) BOOL isNeed;


+ (CVVAddResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;
- (id)copyWithZone:(NSZone *)zone;

@end
