//
//  TransferInfo.h
//  Baf2
//
//  Created by developer on 07.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface TransferInfo : ModelObject

@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *operation;
@property (nonatomic, copy) NSString *provider;
@property (nonatomic, assign) BOOL disabled;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *level;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *statusText;
@property (nonatomic, copy) NSNumber *serviceId;
@property (nonatomic, copy) NSString *reference;


+ (TransferInfo *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;
- (id)copyWithZone:(NSZone *)zone;

@end
