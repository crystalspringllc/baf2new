//
//  AccountInfoModel.h
//  Baf2
//
//  Created by developer on 06.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@class Account;
@class CVCmodel;

@interface AccountInfoModel : ModelObject

@property (nonatomic, copy) Account *account;
@property (nonatomic, copy) CVCmodel *cvcModel;
@property (nonatomic, copy) NSString *accountOwner;
@property (nonatomic, copy) NSNumber *accountAmount;
@property (nonatomic, copy) NSString *currency;

+ (AccountInfoModel *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;
- (id)copyWithZone:(NSZone *)zone;
@end
