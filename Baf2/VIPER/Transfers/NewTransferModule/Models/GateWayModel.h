//
//  GateAwayModel.h
//  Baf2
//
//  Created by developer on 06.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface GateWayModel : ModelObject
@property (nonatomic, copy) NSString *publicKey;
@property (nonatomic, copy) NSString *postLink;
@property (nonatomic, copy) NSString *backLink;
@property (nonatomic, copy) NSString *processLink;
@property (nonatomic, copy) NSNumber *terminalId;
@property (nonatomic, copy) NSString *securitylink;
@property (nonatomic, copy) NSDictionary *securityParams;


+ (GateWayModel *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;
- (id)copyWithZone:(NSZone *)zone;
@end
