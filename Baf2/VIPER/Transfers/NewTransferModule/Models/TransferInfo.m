//
//  TransferInfo.m
//  Baf2
//
//  Created by developer on 07.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "TransferInfo.h"

@implementation TransferInfo

+ (TransferInfo *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    TransferInfo *instance = [[TransferInfo alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.code = [self stringValueForKey:@"code"];
    self.name = [self stringValueForKey:@"name"];
    self.operation = [self stringValueForKey:@"operation"];
    self.provider = [self stringValueForKey:@"provider"];
    self.date = [self stringValueForKey:@"date"];
    self.level = [self stringValueForKey:@"level"];
    self.status = [self stringValueForKey:@"status"];
    self.statusText = [self stringValueForKey:@"statusText"];
    self.serviceId = [self numberValueForKey:@"serviceLogId"];
    self.reference = [self stringValueForKey:@"reference"];
    objectData = nil;
}

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setReference:[self.reference copyWithZone:zone]];
        [copy setCode:[self.code copyWithZone:zone]];
        [copy setName:[self.name copyWithZone:zone]];
        [copy setOperation:[self.operation copyWithZone:zone]];
        [copy setProvider:[self.provider copyWithZone:zone]];
        [copy setDate:[self.date copyWithZone:zone]];
        [copy setLevel:[self.level copyWithZone:zone]];
        [copy setStatus:[self.status copyWithZone:zone]];
        [copy setStatusText:[self.statusText copyWithZone:zone]];
        [copy setServiceId:[self.serviceId copyWithZone:zone]];
    }
    
    return copy;
}

@end
