//
//  CVVAddResponse.m
//  Baf2
//
//  Created by developer on 23.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "CVVAddResponse.h"

@implementation CVVAddResponse

+ (CVVAddResponse *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    CVVAddResponse *instance = [[CVVAddResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.errorMessage = [self stringValueForKey:@"error"];
    self.isNeed = [self boolValueForKey:@"isNeed"];
    self.message = [self stringValueForKey:@"message"];
    
    objectData = nil;
}

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setErrorMessage:[self.errorMessage copyWithZone:zone]];
        [copy setMessage:[self.message copyWithZone:zone]];
    }
    
    return copy;
}

@end
