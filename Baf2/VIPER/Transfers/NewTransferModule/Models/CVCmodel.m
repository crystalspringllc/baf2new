//
//  CVCmodel.m
//  Baf2
//
//  Created by developer on 04.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "CVCmodel.h"

@implementation CVCmodel
{
    
}

+ (CVCmodel *)instanceFromDictionary:(NSDictionary *)aDictionary {
    CVCmodel *instance = [[CVCmodel alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.isNeed = [self boolValueForKey:@"isNeed"];
    self.messageString = [self stringValueForKey:@"message"];
    self.errorString = [self stringValueForKey:@"error"];
    
    objectData = nil;
}

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setIsNeed:self.isNeed];
        [copy setMessageString:[self.messageString copyWithZone:zone]];
        [copy setErrorString:[self.errorString copyWithZone:zone]];
    }
    
    return copy;
}

@end
