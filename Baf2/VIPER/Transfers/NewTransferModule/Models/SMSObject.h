//
//  SMSObject.h
//  Baf2
//
//  Created by developer on 06.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

@interface SMSObject : ModelObject
@property (nonatomic, assign) BOOL isNeed;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *errorMessage;

+ (SMSObject *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;
- (id)copyWithZone:(NSZone *)zone;
@end
