//
//  NewCheckAndInitTransferResponse.m
//  Baf2
//
//  Created by developer on 26.01.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "NewCheckAndInitTransferResponse.h"
#import "Knp.h"


@implementation NewCheckAndInitTransferResponse

@synthesize amountInfo;
@synthesize requestor;
@synthesize destination;
@synthesize amount;
@synthesize clientId;
@synthesize currency;
@synthesize destinationAmount;
@synthesize destinationCurrency;
@synthesize destinationId;
@synthesize destinationType;
@synthesize direction;
@synthesize errorMessage;
@synthesize requestorCurrency;
@synthesize requestorId;
@synthesize requestorType;
@synthesize session;
@synthesize type;
@synthesize comission;
@synthesize comissionCurrency;
//@synthesize exchangeResponse;
@synthesize isValid;
@synthesize knpList;
@synthesize knpObject;
@synthesize operdate;
@synthesize requestorAmount;
@synthesize safetyLevel;
@synthesize warningMessages;
@synthesize isPinVerifyRequired;
@synthesize uuid;
@synthesize save;
@synthesize messages;
@synthesize nextAction;
@synthesize isPayBox;
@synthesize transferInfo;

+ (NewCheckAndInitTransferResponse *)instanceFromDictionary:(NSDictionary *)aDictionary {
    
    NewCheckAndInitTransferResponse *instance = [[NewCheckAndInitTransferResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
    
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    self.nextAction = [self stringValueForKey:@"nextAction"];
    self.amount = [self numberValueForKey:@"amount"];
    self.clientId = [self numberValueForKey:@"clientId"];
    self.currency = [self stringValueForKey:@"currency"];
    self.destinationAmount = [self numberValueForKey:@"destinationAmount"];
    self.destinationCurrency = [self stringValueForKey:@"destinationCurrency"];
    self.destinationId = [self numberValueForKey:@"destinationId"];
    self.destinationType = [self stringValueForKey:@"destinationType"];
    self.direction = [self stringValueForKey:@"direction"];
    //self.warningMessages = [self arrayValueForKey:@"messages"];
    //self.cvcModel = [CVCmodel instanceFromDictionary:[objectData[@"requestor"] objectForKey:@"account"]];
    self.requestorCurrency = [self stringValueForKey:@"requestorCurrency"];
    self.requestorId = [self numberValueForKey:@"requestorId"];
    self.requestorType = [self stringValueForKey:@"requestorType"];
    self.session = [self stringValueForKey:@"session"];
    self.type = [self stringValueForKey:@"type"];
    
    self.comission = [self numberValueForKey:@"comission"];
    self.comissionCurrency = [self numberValueForKey:@"comissionCurrency"];
 //   self.exchangeResponse = [TransferCurrencyRates instanceFromDictionary:[self idValueForKey:@"exchangeRates"]];
    self.isValid = [self boolValueForKey:@"isValid"];
    self.knpObject = [self idValueForKey:@"knp"];//array
   // self.knpList = [self.knpObject arrayValueForKey:@"all"];
    self.operdate = [self stringValueForKey:@"operdate"];
    self.requestorAmount = [self numberValueForKey:@"requestorAmount"];
    self.safetyLevel = [self stringValueForKey:@"safetyLevel"];
    self.isPinVerifyRequired = [self boolValueForKey:@"isPinVerifyRequired"];
    self.uuid = [self stringValueForKey:@"uuid"];
    self.save = [self boolValueForKey:@"save"];
    self.isPayBox = [self boolValueForKey:@"isPaypox"];
    
    self.messages = [self idValueForKey:@"messages"];
    
    self.errorMessage = [self.messages objectForKey:@"error"];
    self.warningMessages = [self.messages mutableArrayValueForKey:@"warning"];
    
    self.requestor = [AccountInfoModel instanceFromDictionary:[self idValueForKey:@"requestor"]];
    self.destination = [AccountInfoModel instanceFromDictionary:[self idValueForKey:@"destination"]];
    self.amountInfo = [AmountInfo instanceFromDictionary:[self idValueForKey:@"amountInfo"]];
    self.gateWay = [GateWayModel instanceFromDictionary:[self idValueForKey:@"gateway"]];
    self.smsObject = [SMSObject instanceFromDictionary:[self idValueForKey:@"sms"]];
    self.transferInfo = [TransferInfo instanceFromDictionary:[self idValueForKey:@"info"]];
    
    self.knpList = [NSMutableArray array];
    for(NSDictionary *dict in [self.knpObject objectForKey:@"all"])
    {
        Knp *knp = [Knp instanceFromDictionary:dict];
        [self.knpList addObject:knp];
    }
    
    objectData = nil;
}

- (BOOL)nextActionTypeIsInit
{
    return [self.nextAction isEqualToString:@"INIT"];
}

- (BOOL)nextActionTypeIsCheck
{
    return [self.nextAction isEqualToString:@"CHECK"];
}

- (BOOL)isIntraBank
{
    return [self.type isEqualToString:@"I"];
}

- (BOOL)selfRequestorAccountCVV
{
    return self.requestor.cvcModel.isNeed;
}

- (BOOL)selfDestinationAccountCVV
{
    return self.destination.cvcModel.isNeed;
}

- (BOOL)smsIsNeed
{
    return self.smsObject.isNeed;
}


@end
