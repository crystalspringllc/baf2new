//
//  NewCheckAndInitTransferResponse.h
//  Baf2
//
//  Created by developer on 26.01.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"
#import "Account.h"
#import "TransferCurrencyRates.h"
#import "AccountInfoModel.h"
#import "AmountInfo.h"
#import "GateWayModel.h"
#import "ExchangeRate.h"
#import "TransferInfo.h"
#import "SMSObject.h"

@class Account;
@class TransferCurrencyRates;
@class CVCmodel;
@class AccountInfoModel;
@class AmountInfo;
@class GateWayModel;
@class SMSObject;
@class TarnsferInfo;


@interface NewCheckAndInitTransferResponse : ModelObject{
    
    TransferInfo *transferInfo;
    SMSObject *smsObject;
    GateWayModel *gateWay;
    AmountInfo *amountInfo;
    AccountInfoModel *requestor;
    AccountInfoModel *destination;
    NSNumber *amount;
    NSNumber *clientId;
    NSString *currency;
    NSNumber *destinationAmount;
    NSString *destinationCurrency;
    NSNumber *destinationId;
    NSString *destinationType;
    BOOL direction;
    NSString *errorMessage;
    NSString *requestorCurrency;
    NSNumber *requestorId;
    NSString *requestorType;
    NSString *session;
    NSString *type;
    
    NSNumber *comission;
    NSNumber *comissionCurrency;
   // TransferCurrencyRates *exchangeResponse;
    BOOL isValid;
    id knpObjects;
    NSMutableArray *knpList;
    NSString *operdate;
    NSNumber *requestorAmount;
    NSString *safetyLevel;
    NSArray *warningMessages;
    NSDictionary *messages;
    
    BOOL isPinVerifyRequired;
    NSString *uuid;
    NSNumber *servicelogId;
    NSString *nextAction;
    BOOL save;
    BOOL isPayBox;
}

@property (nonatomic, copy) TransferInfo *transferInfo;
@property (nonatomic, copy) SMSObject *smsObject;
@property (nonatomic, copy) GateWayModel *gateWay;
@property (nonatomic, copy) AmountInfo *amountInfo;
@property (nonatomic, copy) AccountInfoModel *requestor;
@property (nonatomic, copy) AccountInfoModel *destination;
@property (nonatomic, copy) NSString *nextAction;
@property (nonatomic, copy) NSNumber *amount;
@property (nonatomic, copy) NSNumber *clientId;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, copy) NSNumber *destinationAmount;
@property (nonatomic, copy) NSString *destinationCurrency;
@property (nonatomic, copy) NSNumber *destinationId;
@property (nonatomic, copy) NSString *destinationType;
@property (nonatomic, assign) BOOL direction;
@property (nonatomic, copy) NSString *errorMessage;
@property (nonatomic, copy) NSString *requestorCurrency;
@property (nonatomic, copy) NSNumber *requestorId;
@property (nonatomic, copy) NSString *requestorType;
@property (nonatomic, copy) NSString *session;
@property (nonatomic, copy) NSString *type;

@property (nonatomic, strong) NSNumber *comission;
@property (nonatomic, strong) NSNumber *comissionCurrency;
//@property (nonatomic, strong) TransferCurrencyRates *exchangeResponse;
@property (nonatomic, assign) BOOL isValid;
@property (nonatomic, copy) id knpObject;
@property (nonatomic, strong) NSMutableArray *knpList;
@property (nonatomic, copy) NSString *operdate;
@property (nonatomic, strong) NSNumber *requestorAmount;
@property (nonatomic, strong) NSString *safetyLevel;
@property (nonatomic, strong) NSArray *warningMessages;
@property (nonatomic, strong) NSDictionary *messages;

@property (nonatomic, assign) BOOL isPinVerifyRequired;
@property (nonatomic, copy) NSString *uuid;
@property (nonatomic, assign) BOOL save;
@property (nonatomic, assign) BOOL isPayBox;

+ (NewCheckAndInitTransferResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (BOOL)nextActionTypeIsCheck;
- (BOOL)nextActionTypeIsInit;
- (BOOL)isIntraBank;
- (BOOL)isCvvInput;
- (BOOL)selfRequestorAccountCVV;
- (BOOL)selfDestinationAccountCVV;
- (BOOL)smsIsNeed;


@end
