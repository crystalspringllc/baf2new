//
//  SMSObject.m
//  Baf2
//
//  Created by developer on 06.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "SMSObject.h"

@implementation SMSObject

+ (SMSObject *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    SMSObject *instance = [[SMSObject alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.isNeed = [self boolValueForKey:@"isNeed"];
    self.message = [self stringValueForKey:@"message"];
    self.errorMessage = [self stringValueForKey:@"error"];
    
    objectData = nil;
}

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setIsNeed:self.isNeed];
        [copy setMessage:[self.message copyWithZone:zone]];
        [copy setErrorMessage:[self.errorMessage copyWithZone:zone]];
    }
    
    return copy;
}



@end
