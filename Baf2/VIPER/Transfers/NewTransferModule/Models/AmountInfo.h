//
//  AmountInfo.h
//  Baf2
//
//  Created by developer on 06.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"
#import "ExchangeRate.h"
#import "TransferCurrencyRates.h"

@class ExchangeRate;
@class TransferCurrencyRates;

@interface Commission : NSObject
@property(nonatomic, strong) NSNumber *commissionAmount;
@property(nonatomic, strong) NSString *commissionCurrency;
@end

@interface AmountInfo : ModelObject
{
NSNumber *finalAmount;
NSString *currency;
ExchangeRate *rate;
TransferCurrencyRates *exchangeResponse;
Commission *commission;
NSDictionary *commissionJson;
}

@property(nonatomic, copy) TransferCurrencyRates *exchangeResponse;
@property(nonatomic, copy) NSNumber *finalAmount;
@property(nonatomic, copy) NSString *currency;
@property(nonatomic, strong) ExchangeRate *rate;
@property(nonatomic, strong) Commission *commission;
@property(nonatomic, copy) NSDictionary *commissionJson;

+ (AmountInfo *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;
- (id)copyWithZone:(NSZone *)zone;

@end
