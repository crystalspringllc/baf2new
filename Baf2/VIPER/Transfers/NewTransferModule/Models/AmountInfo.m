//
//  AmountInfo.m
//  Baf2
//
//  Created by developer on 06.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "AmountInfo.h"
#import "ExchangeRate.h"

@implementation Commission

- (instancetype)initWithJson:(NSDictionary *)json {
    self = [super init];
    if (self) {
        self.commissionAmount = json[@"amount"];
        self.commissionCurrency = json[@"currency"];
    }
    return self;
}

@end

@implementation AmountInfo

@synthesize finalAmount;
@synthesize currency;
@synthesize rate;
@synthesize commission;
@synthesize commissionJson;


+ (AmountInfo *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    AmountInfo *instance = [[AmountInfo alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    NSLog(@"amount %@", [self stringValueForKey:@"amount"]);
    self.finalAmount = [self numberValueForKey:@"amount"];
    self.currency = [self stringValueForKey:@"currency"];
    self.rate = [ExchangeRate instanceFromDictionary:[self idValueForKey:@"exchangeRates"]];
    self.commission = [[Commission alloc] initWithJson:[self idValueForKey:@"commission"]];
    self.commissionJson = [self idValueForKey:@"commission"];
    self.exchangeResponse = [TransferCurrencyRates instanceFromDictionary:[self idValueForKey:@"exchangeRates"]];
    
    objectData = nil;
}

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setExchangeResponse:[self.exchangeResponse copyWithZone:zone]];
        [copy setFinalAmount:[self.finalAmount copyWithZone:zone]];
        [copy setCurrency:[self.currency copyWithZone:zone]];
        [copy setRate:self.rate];
        [copy setCommission:self.commission];
        [copy setCommissionJson:[self.commissionJson copyWithZone:zone]];
    }
    
    return copy;
}

@end
