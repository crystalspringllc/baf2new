//
//  AccountInfoModel.m
//  Baf2
//
//  Created by developer on 06.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "AccountInfoModel.h"
#import "Account.h"
#import "CVCmodel.h"

@implementation AccountInfoModel

+ (AccountInfoModel *)instanceFromDictionary:(NSDictionary *)aDictionary {
    AccountInfoModel *instance = [[AccountInfoModel alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.account = [Account instanceFromDictionary:[self idValueForKey:@"account"]];
    self.cvcModel = [CVCmodel instanceFromDictionary:[self idValueForKey:@"cvv"]];
    self.currency = [self stringValueForKey:@"currency"];
    self.accountAmount = [self numberValueForKey:@"amount"];
    
    
    objectData = nil;
}

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setAccount:[self.account copyWithZone:zone]];
        [copy setCurrency:[self.currency copyWithZone:zone]];
        [copy setCvcModel:[self.cvcModel copyWithZone:zone]];
        [copy setAccountAmount:[self.accountAmount copy]];
    }
    
    return copy;
}

@end
