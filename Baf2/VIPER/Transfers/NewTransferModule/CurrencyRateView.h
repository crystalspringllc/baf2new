//
//  CurrencyRateView.h
//  Baf2
//
//  Created by developer on 20.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrencyRateView : UIView
@property (weak, nonatomic) IBOutlet UILabel *currencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@end
