//
//  DisclamerView.m
//  Baf2
//
//  Created by developer on 21.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "DisclamerView.h"
@interface DisclamerView ()

@property (nonatomic, weak) IBOutlet UILabel *desclamerLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineViewHeight;
@end

@implementation DisclamerView

- (void)setText:(NSString *)desclamerText
{
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[desclamerText dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.desclamerLabel.attributedText = attrStr;
    self.desclamerLabel.numberOfLines = 0;
    self.topConstraint.constant = 15;
    self.bottomConstraint.constant = 15;
    self.lineViewHeight.constant = 5;
    self.hidden = NO;
}

- (void)makeDefault
{
    self.hidden = NO;
    self.desclamerLabel.numberOfLines = 0;
    self.desclamerLabel.text = nil;
    self.topConstraint.constant = 0;
    self.bottomConstraint.constant = 0;
    self.lineViewHeight.constant = 0;
}

@end
