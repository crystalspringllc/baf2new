//
//  DisclamerView.h
//  Baf2
//
//  Created by developer on 21.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisclamerView : UIView

- (void)setText:(NSString *)desclamerText;
- (void)makeDefault;


@end
