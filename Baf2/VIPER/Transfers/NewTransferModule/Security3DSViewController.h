//
//  Security3DSViewController.h
//  Baf2
//
//  Created by developer on 27.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Security3DSViewController : UIViewController
@property (nonatomic, strong) NSString *securityLink;
@property (nonatomic, strong) NSDictionary *securityParams;
@property (nonatomic, strong) NSNumber *serviceLogID;
@end
