//
//  CVVEnterViewController.m
//  Baf2
//
//  Created by developer on 03.02.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "CVVEnterViewController.h"
#import "JVFloatLabeledTextField.h"
#import "MainButton.h"
#import "ASize.h"
#import "UIView+ConcisePureLayout.h"
#import "Account.h"
#import "UIColor+Extensions.h"
#import <STPopup/STPopup.h>
#import "NSObject+PWObject.h"
#import "NSString+Ext.h"
#import "Version.h"
#import "UIView+Gestures.h"
#import "JVFloatLabeledTextField.h"
#import "RSA.h"
#import "NewTransfersApi.h"

@interface CVVEnterViewController()<UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UIView *wrapperView;
@property (weak, nonatomic) IBOutlet UILabel *warningMessageLabel;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *cvvTextField1;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *cvvTextField2;
@property (weak, nonatomic) IBOutlet MainButton *sumbitButton;
@property (nonatomic, strong) NSString *encryptedCVV;

@end

@implementation CVVEnterViewController
{
    NSString *publicKey;
}

+ (instancetype)createVC
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TransferView" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([CVVEnterViewController class])];
}

- (void)awakeFromNib
{
   // [super awakeFromNib];
    CGFloat w = [ASize screenWidth] - [Version iphone5:50 iphone6:70 iphone6HD:90];
    self.contentSizeInPopup = CGSizeMake(w, 300);
    self.landscapeContentSizeInPopup = CGSizeMake(w, 300);
}

-(void)initVars {}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
    
    [self performBlock:^{
        [self.popupController.backgroundView tapGesture:self selector:@selector(hide)];
    } afterDelay:0.5];
    
    [self.cvvTextField1 addTarget:self action:@selector(cvcDidChanged) forControlEvents:UIControlEventEditingChanged];
    
    
}

#pragma mark -
#pragma mark Actions


- (IBAction)hide {
    [self.view endEditing:YES];
    [self.popupController dismiss];
}

- (void)cvcDidChanged {
    self.sumbitButton.enabled = [self.cvvTextField2.text trim].length == 3;
}



#pragma mark -
#pragma mark UITextField

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if([string isEmpty]) {
        return YES;
    }
    return textField.text.length < 3;
}

/*- (void)textFieldDidEndEditing:(UITextField *)textField
{
    /*switch (textField.tag) {
        case 1:
            if(textField.text.length >= 3)
            {
                [textField resignFirstResponder];
                [self.cvvTextField2 setUserInteractionEnabled:YES];
                [self.cvvTextField2 becomeFirstResponder];
            }
            break;
        case 2:
            if(textField.text.length == _cvvTextField1.text.length)
            {
                [self.cvvTextField2 resignFirstResponder];
                [self checkCVVCodeFirstText:_cvvTextField1.text andSecondText:textField.text];
            }else{
                [self.cvvTextField2 becomeFirstResponder];
                [UIHelper showAlertWithMessage:@"Введенные данные должны совпадать"];
            }
            return;
            break;
        default:
            break;
    
     if(textField.tag == 1 && textField.text.length >= 3)
     {
        // [textField resignFirstResponder];
         [self.cvvTextField2 setUserInteractionEnabled:YES];
        // [self.cvvTextField2 becomeFirstResponder];
     }else if(textField.tag == 2)
     {
         //if(textField.text.length == _cvvTextField1.text.length)
        // {
            // [self.cvvTextField2 resignFirstResponder];
             [self checkCVVCodeFirstText:_cvvTextField1.text andSecondText:textField.text];
         //}else{
            // [self.cvvTextField2 becomeFirstResponder];
            // [UIHelper showAlertWithMessage:@"Введенные данные должны совпадать"];
         }
    
}*/

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    if(textField.tag == 1 && textField.text.length >= 3)
    {
        // [textField resignFirstResponder];
        [self.cvvTextField2 setUserInteractionEnabled:YES];
       // [self.cvvTextField2 becomeFirstResponder];
        //[self textFieldShouldBeginEditing:self.cvvTextField2];
        return YES;
    }else if(textField.tag == 2)
    {
        if(textField.text.length == _cvvTextField1.text.length && textField.text.length > 0)
         {
         [self.cvvTextField2 resignFirstResponder];
         [self checkCVVCodeFirstText:_cvvTextField1.text andSecondText:textField.text];
             return YES;
        }else{
         [self.cvvTextField2 becomeFirstResponder];
         [UIHelper showAlertWithMessage:@"Введенные данные должны совпадать"];
            return NO;
        }
    }
    return YES;
}

/*
 -----BEGIN PUBLIC KEY-----
 MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDjtB/GEP/g94lt1gV0JCqG7XgR
 rdiW2qXjmtr88J03jveUDQUdomq0NWqKHeZDUy/wwlJImgNvnr4j61SjznhfLMV1
 es9Guyrh4IA1xsmK3u4sI7MzbKQRywx2GYWuEXE6Z+rsEaIXkC62o2ieWz/y37is
 AU3erD2TDt0w0PpaYwIDAQAB
 -----END PUBLIC KEY-----
 */

- (void)checkCVVCodeFirstText:(NSString *)textFieldText1 andSecondText:(NSString *)textFieldText2
{
    if([textFieldText1 isEqualToString:textFieldText2])
    {
        NSLog(@"public key:%@", self.publicKey);
       self.encryptedCVV = [RSA encryptString:textFieldText1 publicKey:self.publicKey];
       NSLog(@"encrypyed CVV: %@", self.encryptedCVV);
        [self.sumbitButton setMainButtonStyle:MainButtonStyleOrange];
        [self.sumbitButton setEnabled:YES];
    
    }else{
       [UIHelper showAlertWithMessage:@"Введенные данные должны совпадать"];
    }
}

- (IBAction)sendCVV:(id)sender {
    [self.delegate enterCVCViewController:self didEnterCVC:self.encryptedCVV forAccount:self.account];
    [self hide];
}

#pragma mark -

- (void)updatePopup {
    [_wrapperView setNeedsLayout];
    [_wrapperView layoutIfNeeded];
    self.contentSizeInPopup = CGSizeMake(_wrapperView.width, _wrapperView.height);
}

-(void)configUI
{
    self.warningMessageLabel.text = self.warningMessage;
    [self.sumbitButton setTitle:NSLocalizedString(@"Отправить", nil) forState:UIControlStateNormal];
    
    [self.sumbitButton setMainButtonStyle:MainButtonStyleDisable];
    self.sumbitButton.title = @"Отправить";
    
    self.cvvTextField1.delegate = self;
    self.cvvTextField2.delegate = self;
    self.cvvTextField1.tag = 1;
    self.cvvTextField2.tag = 2;
    [self.cvvTextField2 setUserInteractionEnabled:NO];
    self.cvvTextField1.keyboardType = UIKeyboardTypeNumberPad;
    self.cvvTextField2.keyboardType = UIKeyboardTypeNumberPad;
    self.cvvTextField1.returnKeyType = UIReturnKeyNext;
     [self.cvvTextField2 setClearsOnBeginEditing:YES];
     [self.cvvTextField1 setClearsOnBeginEditing:YES];
    
    publicKey = @"-----BEGIN PUBLIC KEY-----MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDjtB/GEP/g94lt1gV0JCqG7XgRrdiW2qXjmtr88J03jveUDQUdomq0NWqKHeZDUy/wwlJImgNvnr4j61SjznhfLMV1es9Guyrh4IA1xsmK3u4sI7MzbKQRywx2GYWuEXE6Z+rsEaIXkC62o2ieWz/y37isAU3erD2TDt0w0PpaYwIDAQAB-----END PUBLIC KEY-----";
    
    [self updatePopup];
}


/*-(NSString *)cardUserName {
 PaymentCard *card = [PaymentCard objectForPrimaryKey:self.cardNumber];
 if(card) {
 return card.cardHolder;
 }
 return nil;
 }*/



-(void)dealloc {
    
}


@end
