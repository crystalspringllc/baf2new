//
// Created by Askar Mustafin on 6/7/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "IPOViewController.h"
#import <STPopup/STPopup.h>
#import "MainButton.h"
#import "ASize.h"
#import "IPORouter.h"

@interface IPOViewController() {}
@property (weak, nonatomic) IBOutlet MainButton *yesButton;
@property (weak, nonatomic) IBOutlet MainButton *laterButton;
@property (weak, nonatomic) IBOutlet MainButton *noButton;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@end

@implementation IPOViewController

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"IPO" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([IPOViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth] - 40, 300);
    [self configUI];

    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsNotShowIPO];
    [[NSUserDefaults standardUserDefaults] synchronize];

    if (self.text) {
        self.textLabel.text = self.text;
    }

}

#pragma mark -

- (IBAction)clickYesButton:(id)sender {

    NSString *urlString = @"https://ipobankastana.kz/?utm_source=Mobile&utm_medium=cpc&utm_campaign=ipo";

    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [GAN sendEventWithCategory:@"IPO" action:@"перешел по ссылке"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString] options:@{} completionHandler:nil];
    } else {
        [[UIApplication sharedApplication] openURL:urlString];
    }

    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsNotShowIPO];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [self.popupController dismiss];
}

- (IBAction)clickLaterButton:(id)sender {
    [GAN sendEventWithCategory:@"IPO" action:@"позже"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kIsNotShowIPO];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.popupController dismiss];
}

- (IBAction)clickNoButton:(id)sender {
    [GAN sendEventWithCategory:@"IPO" action:@"отказал"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsNotShowIPO];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.popupController dismiss];
}

#pragma mark - config ui

- (void)configUI {
    [self.yesButton setMainButtonStyle:MainButtonStyleOrange];
    [self.laterButton setMainButtonStyle:MainButtonStyleOrange];
    [self.noButton setMainButtonStyle:MainButtonStyleOrange];
}

@end
