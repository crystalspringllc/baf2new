//
// Created by Askar Mustafin on 6/7/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface IPODependencies : NSObject

- (void)startRouter;
@property (nonatomic, strong) UIViewController *parentVC;

@end