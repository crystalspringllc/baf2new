//
// Created by Askar Mustafin on 6/7/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "IPORouter.h"
#import "NSString+Ext.h"
#import "NSDate+Ext.h"
#import <STPopup/STPopup.h>
#import "STPopupController+Extensions.h"
#import "IPOViewController.h"
#import "InfoApi.h"

#define kIPOAppOpenTimeDateFormat @"dd.MM.yyyy"

@interface IPORouter()
@property (nonatomic, strong) STPopupController *popupController;
@end

@implementation IPORouter {}

- (void)startRouterIn:(UIViewController *)viewController
{
    BOOL isNotShowIPO = [[NSUserDefaults standardUserDefaults] boolForKey:kIsNotShowIPO];
    BOOL isShownPerSession = [[NSUserDefaults standardUserDefaults] boolForKey:kIsShownPerSession];

    if (isNotShowIPO == NO && isShownPerSession == NO) {

        [InfoApi getIpoInfoSuccess:^(id response) {
            if ([response[@"enabled"] boolValue]) {
                if (response[@"text"] && [response[@"text"] length] > 0) {
                    [self showDialogView:viewController text:response[@"text"]];
                } else {
                    [self showDialogView:viewController text:nil];
                }
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsShownPerSession];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        } failure:^(NSString *code, NSString *message) {
        }];
    }
}

- (void)showDialogView:(UIViewController *)viewController text:(NSString *)text
{
    IPOViewController *vc = [IPOViewController createVC];
    vc.text = text;
    self.popupController = [[STPopupController alloc] initWithRootViewController:vc];
    self.popupController.dismissOnBackgroundTap = true;
    self.popupController.style = STPopupStyleFormSheet;
    self.popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    self.popupController.navigationBarHidden = YES;
    [self.popupController presentInViewController:viewController];
}

@end