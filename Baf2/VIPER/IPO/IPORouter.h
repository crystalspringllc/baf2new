//
// Created by Askar Mustafin on 6/7/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kIsNotShowIPO @"isNotShowIPO"
#define kIsShownPerSession @"isShowPerSession"

@interface IPORouter : NSObject

- (void)startRouterIn:(UIViewController *)viewController;

@end