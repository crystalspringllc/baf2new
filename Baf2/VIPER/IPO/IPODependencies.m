//
// Created by Askar Mustafin on 6/7/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "IPODependencies.h"
#import "IPORouter.h"


@interface IPODependencies()

@property (nonatomic, strong) IPORouter *router;

@end


@implementation IPODependencies {

}

- (id)init {
    self = [super init];
    if (self) {
        [self configureDependencies];
    }
    return self;
}

- (void)startRouter {
    [self.router startRouterIn:self.parentVC];
}

- (void)configureDependencies {
    IPORouter *router = [[IPORouter alloc] init];
    self.router = router;
}

@end