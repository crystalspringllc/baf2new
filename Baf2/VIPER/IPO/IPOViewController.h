//
// Created by Askar Mustafin on 6/7/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IPOViewController : UIViewController

+ (instancetype)createVC;

@property (nonatomic, strong) NSString *text;

@end
