//
// Created by Askar Mustafin on 10/4/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SmsBankingInteractorInput <NSObject>
- (void)getSmsState;
- (void)enableSmsBanking;
- (void)runWithSmsCode:(NSString *)smsCode;
- (void)sendSmsCodeAgain;
- (void)prepareToChangeNumber;
@end


@protocol SmsBankingInteractorOutput <NSObject>
- (void)smsBankingStateDateDidReceived:(id)smsBankingStateDate;
- (void)smsBankingStateDateDidFailured;

- (void)initSmsBankingDidReceived;
- (void)checkSmsCodeSuccess;
- (void)checkSmsCodeFailure;
- (void)smsSentAgain;

- (void)preparedToChangeNumber:(NSNumber *)cardId;

@end