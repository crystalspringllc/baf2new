//
// Created by Askar Mustafin on 6/15/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

/**
 * !
 * THIS MODULE DO NOT SUPPORTS VIPER
 * JUST OLD PLAIN MVC ONLY
 */

#import "SmsBankingChangePhoneNumberViewController.h"
#import "OCMaskedTextFieldView.h"
#import "MainButton.h"
#import "IQUIView+IQKeyboardToolbar.h"
#import "SMSBankingApi.h"
#import "SmsBankingSmsCodeAndPasswordViewController.h"
#import <STPopup/STPopup.h>
#import "MBProgressHUD+AstanaView.h"
#import "ASize.h"

@interface SmsBankingChangePhoneNumberViewController() {}
@property (weak, nonatomic) IBOutlet UILabel *currentNumber;
@property (weak, nonatomic) IBOutlet OCMaskedTextFieldView *numberTextField;
@property (weak, nonatomic) IBOutlet MainButton *changeNumberButton;
@end

@implementation SmsBankingChangePhoneNumberViewController {}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SmsBanking" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SmsBankingChangePhoneNumberViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - config actions

- (IBAction)clickChangeButton:(id)sender
{
    if ([self isChangingValid])
    {
        [MBProgressHUD showAstanaHUDWithTitle:@"Подготовка" animated:YES];
        [SMSBankingApi initPhoneChangeWithCardId:self.cardId newPhone:self.numberTextField.getRawInputText success:^(id response)
        {
            [MBProgressHUD hideAstanaHUDAnimated:YES];

            //id response success
            //show enter sms and password screen
            if (response) {

                SmsBankingSmsCodeAndPasswordViewController *vc = [SmsBankingSmsCodeAndPasswordViewController createVC];
                //todo:set servicelog id
                vc.serviceLogId = response;
                [self.popupController pushViewController:vc animated:YES];

            }

        } failure:^(NSString *code, NSString *message)
        {
            [MBProgressHUD hideAstanaHUDAnimated:YES];

        }];
    }
}

#pragma mark - helpers

- (BOOL)isChangingValid {
    if (self.currentNumber.text.length > 0 &&
            self.numberTextField.getRawInputText.length > 0 &&
            self.cardId) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - config ui

- (void)configUI {
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = NSLocalizedString(@"manage_sms_banking", nil);

    self.changeNumberButton.mainButtonStyle = MainButtonStyleOrange;
    [self.numberTextField setMask:@"+7 (###) ### ## ##"];
    [self.numberTextField showMask];
    self.numberTextField.maskedTextField.textAlignment = NSTextAlignmentRight;
    
    self.currentNumber.text = self.currencyPhoneString;

    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 200);
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
