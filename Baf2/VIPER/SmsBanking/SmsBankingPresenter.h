//
//  SmsBankingPresenter.h
//  Baf2
//
//  Created by Askar Mustafin on 10/3/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SmsBankingViewIO.h"
#import "SmsBankingInteractorIO.h"
#import "SmsBankingRouter.h"



@interface SmsBankingPresenter : NSObject<SmsBankingInteractorOutput, SmsBankingViewOutput>

@property (nonatomic, strong) id <SmsBankingInteractorInput> interactorInput;
@property (nonatomic, strong) id <SmsBankingViewInput> viewInput;

@property (nonatomic, strong) SmsBankingRouter *smsBankingRouter;

@end
