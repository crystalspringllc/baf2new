//
//  SmsBankingSmsCodeAndPasswordViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 6/16/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SmsBankingSmsCodeAndPasswordViewController : UIViewController

@property (nonatomic, strong) NSNumber *serviceLogId;

+ (instancetype)createVC;

@end
