//
// Created by Askar Mustafin on 10/3/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "SmsBankingInteractor.h"
#import "AccountsApi.h"
#import "SMSBankingApi.h"


@interface SmsBankingInteractor()
@property (nonatomic, strong) SmsBankingResponse *smsBankingResponse;
@property (nonatomic, strong) NSNumber *serviceLogId;
@end

@implementation SmsBankingInteractor {}

#pragma mark - interactor input protocol

- (void)getSmsState {
    __weak SmsBankingInteractor *wSelf = self;
    [self getSmsBankingInfoCompletion:^(SmsBankingResponse *response) {
        wSelf.smsBankingResponse = response;
        [wSelf.output smsBankingStateDateDidReceived:response];
    }];
}

- (void)enableSmsBanking {
    __weak SmsBankingInteractor *wSelf = self;
    [self initSmsBankingCompletion:^(NSNumber *serviceLogId) {
        wSelf.serviceLogId = serviceLogId;
        [wSelf.output initSmsBankingDidReceived];
    }];
}

- (void)runWithSmsCode:(NSString *)smsCode {
    __weak SmsBankingInteractor *wSelf = self;
    [self checkSmsBankingSmsCode:@([smsCode intValue]) completion:^(BOOL isSuccess) {
        if (isSuccess) {
            [wSelf.output checkSmsCodeSuccess];
        } else {
            [wSelf.output checkSmsCodeFailure];
        }

    }];
}

- (void)sendSmsCodeAgain {
    __weak SmsBankingInteractor *wSelf = self;
    [self resendSmsCodeCompletion:^(id response) {
        [wSelf.output smsSentAgain];
    }];
}

- (void)prepareToChangeNumber {
    [self.output preparedToChangeNumber:self.cardId];
}

#pragma mark - api requests

- (void)getSmsBankingInfoCompletion:(void (^)(SmsBankingResponse *response))completion {
    __weak SmsBankingInteractor *wSelf = self;

    [SMSBankingApi getSmsbankingStatusWithCardId:self.cardId success:^(id response) {
        SmsBankingResponse *smsBankingResponse = [SmsBankingResponse instanceFromDictionary:response];
        if (completion) {
            completion(smsBankingResponse);
        }
    } failure:^(NSString *code, NSString *message) {
        [wSelf.output smsBankingStateDateDidFailured];
    }];
}

- (void)initSmsBankingCompletion:(void (^)(NSNumber *serviceLogId))completion {
    [SMSBankingApi initSmsbankingWithCardId:self.cardId phone:self.smsBankingResponse.phone isEnable:!self.smsBankingResponse.isConnected success:^(id response) {
        if (completion) {
            completion(response);
        }
    } failure:^(NSString *code, NSString *message) {}];
}

- (void)checkSmsBankingSmsCode:(NSNumber *)smsCode completion:(void (^)(BOOL isSuccess))completion {
    [SMSBankingApi checkSmsBankingSmsWithCode:smsCode serviceLogId:self.serviceLogId success:^(id response) {
        if (completion) {
            completion(YES);
        }
    } failure:^(NSString *code, NSString *message) {
        if (completion) {
            completion(NO);
        }
    }];
}

- (void)resendSmsCodeCompletion:(void (^)(id response))completion {
    [SMSBankingApi sendSmsAgainWithServiceLogId:self.serviceLogId success:^(id response) {
        if (completion) {
            completion(response);
        }
    } failure:^(NSString *code, NSString *message) {}];
}

@end

#pragma mark - SmsBankingResponse Model

@implementation SmsBankingResponse

+ (SmsBankingResponse *)instanceFromDictionary:(NSDictionary *)aDictionary {
    SmsBankingResponse *instance = [[SmsBankingResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.accountId = [self numberValueForKey:@"accountId"];
    self.accountType = [self stringValueForKey:@"accountType"];
    self.amount = [self numberValueForKey:@"amount"];
    self.cardNumber = [self stringValueForKey:@"cardNumber"];
    self.currency = [self stringValueForKey:@"currency"];
    self.iban = [self stringValueForKey:@"iban"];
    self.iin = [self stringValueForKey:@"iin"];
    self.isAvailable = [self boolValueForKey:@"isAvailable"];
    self.isConnected = [self boolValueForKey:@"isConnected"];
    self.phone = [self stringValueForKey:@"phone"];

    objectData = nil;
}

@end
