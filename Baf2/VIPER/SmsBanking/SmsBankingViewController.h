//
//  SmsBankingViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 10/3/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SmsBankingViewIO.h"


@interface SmsBankingViewController : UIViewController<SmsBankingViewInput>
+ (instancetype)createVC;

@property (nonatomic, strong) id<SmsBankingViewOutput> output;
@end
