//
// Created by Askar Mustafin on 10/3/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SmsBankingViewOutput <NSObject>
- (void)firstConfigure;
- (void)changeState;
- (void)runWithSmsCode:(NSString *)smsCode;
- (void)sendSmsCodeAgain;
- (void)prepareToChangeNumber;
@end

@protocol SmsBankingViewInput <NSObject>

- (void)moveToChangeNumber:(NSNumber *)cardId;

- (void)inputMainButtonTitle:(NSString *)title;
- (void)inputStateString:(NSString *)stateString color:(UIColor *)stateColor;
- (void)inputPhoneNumber:(NSString *)phoneNumber;
- (void)inputAmountDiscamer:(NSString *)amountDisclamer;
- (void)inputProcessDisclamer:(NSString *)processDisclamer;
- (void)inputRuleDisclamer:(NSString *)ruleDisclamer;
- (void)inputStateIsEnabled:(BOOL)isEnabled;
- (void)showBafHUDWithTitle:(NSString *)title;

- (void)hideBafHUD;
- (void)showEnterSmsCodeView;
- (void)checkSmsCodeSuccess;
- (void)checkSmsCodeFailure;
- (void)smsSentAgain;
@end