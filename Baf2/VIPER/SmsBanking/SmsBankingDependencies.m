//
// Created by Askar Mustafin on 11/1/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "SmsBankingDependencies.h"
#import "SmsBankingRouter.h"
#import "SmsBankingPresenter.h"


@interface SmsBankingDependencies()
@property (nonatomic, strong) SmsBankingRouter *smsBankingRouter;
@end

@implementation SmsBankingDependencies {}

- (id)init {
    self = [super init];
    if (self) {
        [self configureDependencies];
    }
    return self;
}

- (void)installRootViewController {
    [self.smsBankingRouter showSmsBankingModuleInVC:self.parentVC];
}

- (void)configureDependencies {
    //Transfer Modules Classes
    SmsBankingRouter *smsBankingRouter = [[SmsBankingRouter alloc] init];
    SmsBankingPresenter *smsBankingPresenter = [[SmsBankingPresenter alloc] init];
    SmsBankingInteractor *smsBankingInteractor = [[SmsBankingInteractor alloc] init];

    //contections
    //interactor's OUTPUT with presenter
    //presenter's INPUT with interactor
    smsBankingInteractor.output = smsBankingPresenter;
    smsBankingPresenter.interactorInput = smsBankingInteractor;

    smsBankingRouter.smsBankingPresenter = smsBankingPresenter;
    smsBankingPresenter.smsBankingRouter = smsBankingRouter;

    self.smsBankingRouter = smsBankingRouter;
    self.smsBankingInteractor = smsBankingInteractor;
}

- (void)dealloc {
    NSLog(@"SmsBankingDependencies DEALLOC");
}

@end
