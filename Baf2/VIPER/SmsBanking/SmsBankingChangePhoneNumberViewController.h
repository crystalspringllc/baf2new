//
// Created by Askar Mustafin on 6/15/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SmsBankingChangePhoneNumberViewController : UIViewController

@property (nonatomic, strong) NSNumber *cardId;
@property (nonatomic, strong) NSString *currencyPhoneString;

+ (instancetype)createVC;

@end
