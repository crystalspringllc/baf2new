//
//  SmsBankingRouter.m
//  Baf2
//
//  Created by Askar Mustafin on 10/3/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "SmsBankingRouter.h"
#import "SmsBankingViewIO.h"
#import "SmsBankingViewController.h"
#import "SmsBankingPresenter.h"
#import <STPopup/STPopupController.h>
#import "STPopupController+Extensions.h"
#import "Constants.h"


@interface SmsBankingRouter()
@property (nonatomic, strong) STPopupController *popupController;
@end

@implementation SmsBankingRouter

- (void)showSmsBankingModuleInVC:(UIViewController *)viewController {
    SmsBankingViewController *smsBankingViewController = [SmsBankingViewController createVC];
    smsBankingViewController.output = self.smsBankingPresenter;
    self.smsBankingPresenter.viewInput = smsBankingViewController;

    self.popupController = [[STPopupController alloc] initWithRootViewController:smsBankingViewController];
    self.popupController.dismissOnBackgroundTap = true;
    self.popupController.style = STPopupStyleFormSheet;
    self.popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [self.popupController presentInViewController:viewController];
}

- (void)close {
    [self.popupController dismiss];
}

@end
