//
//  SmsBankingSmsCodeAndPasswordViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 6/16/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "SmsBankingSmsCodeAndPasswordViewController.h"
#import "SuperTextFieldView.h"
#import "MainButton.h"
#import "SMSBankingApi.h"
#import <STPopup/STPopup.h>

@interface SmsBankingSmsCodeAndPasswordViewController ()
@property (weak, nonatomic) IBOutlet SuperTextFieldView *smsCodeTextField;
@property (weak, nonatomic) IBOutlet SuperTextFieldView *passwordTextField;
@property (weak, nonatomic) IBOutlet MainButton *proceedButton;
@end

@implementation SmsBankingSmsCodeAndPasswordViewController

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SMSConfirmViewController" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SmsBankingSmsCodeAndPasswordViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self configActions];
}

#pragma mark - config actions

- (IBAction)clickProceedButton:(id)sender {
    [MBProgressHUD showAstanaHUDWithTitle:@"Изменение номера" animated:YES];

    NSString *code = self.smsCodeTextField.value;
    NSString *password = self.passwordTextField.value;

    [SMSBankingApi runPhoneChangeWithServiceLogId:self.serviceLogId code:code password:password success:^(id response)
    {
       [MBProgressHUD hideAstanaHUDAnimated:YES];
        [Toast showToast:@"Номер телефона успешно изменен"];
        [self.popupController dismissWithCompletion:nil];
    } failure:^(NSString *code, NSString *message) {
        if (message) {
            [Toast showToast:message];
        } else {
            [Toast showToast:@"Неправильный код или пароль"];
        }
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

- (IBAction)clickResendSmsCodeButton:(id)sender {
    if (!self.serviceLogId) {
        return;
    }

    [MBProgressHUD showAstanaHUDWithTitle:@"Переотправка смс кода" animated:YES];
    [SMSBankingApi sendSmsWithServiceLogId:self.serviceLogId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [Toast showToast:@"Смс повторно отправлен"];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

/**
 * This validates proceed button
 */
- (void)configActions {
    __weak SmsBankingSmsCodeAndPasswordViewController *wSelf = self;

    [self.smsCodeTextField onValueChanged:^(NSString *value) {
        if (wSelf.smsCodeTextField.value.length > 0 &&
                wSelf.passwordTextField.value.length > 0 &&
                wSelf.serviceLogId)
        {
            wSelf.proceedButton.mainButtonStyle = MainButtonStyleOrange;
        }
    }];

    [self.passwordTextField onValueChanged:^(NSString *value) {
        if (wSelf.smsCodeTextField.value.length > 0 &&
                wSelf.passwordTextField.value.length > 0
                        && wSelf.serviceLogId)
        {
            wSelf.proceedButton.mainButtonStyle = MainButtonStyleOrange;
        }
    }];
}

#pragma mark - config ui

- (void)configUI {
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = NSLocalizedString(@"manage_sms_banking", nil);
    self.proceedButton.mainButtonStyle = MainButtonStyleDisable;
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 350);
    self.passwordTextField.textField.secureTextEntry = YES;
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
