//
//  SmsBankingPresenter.m
//  Baf2
//
//  Created by Askar Mustafin on 10/3/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "SmsBankingPresenter.h"

@implementation SmsBankingPresenter

#pragma mark - view output protocol

- (void)firstConfigure {
    [self.viewInput showBafHUDWithTitle:@"Получение статуса"];
    [self.interactorInput getSmsState];
}

- (void)changeState {
    [self.viewInput showBafHUDWithTitle:@""];
    [self.interactorInput enableSmsBanking];
}

- (void)runWithSmsCode:(NSString *)smsCode {
    [self.interactorInput runWithSmsCode:smsCode];
}

- (void)sendSmsCodeAgain {
    [self.interactorInput sendSmsCodeAgain];
}

- (void)prepareToChangeNumber {
    [self.interactorInput prepareToChangeNumber];
}

#pragma mark - interactor output protocol

- (void)preparedToChangeNumber:(NSNumber *)cardId {
    [self.viewInput moveToChangeNumber:cardId];
}

- (void)smsBankingStateDateDidReceived:(id)smsBankingStateDate {
    [self.viewInput hideBafHUD];

    SmsBankingResponse *smsBankingResponse = smsBankingStateDate;

    [self.viewInput inputMainButtonTitle:smsBankingResponse.isConnected ? @"Отключить" : @"Включить"];
    [self.viewInput inputStateString:smsBankingResponse.isConnected ? @"Включен" : @"Выключен"
                               color:smsBankingResponse.isConnected ? [UIColor fromRGB:0x4B8153] : [UIColor fromRGB:0xEF6B7C]];
    [self.viewInput inputPhoneNumber:smsBankingResponse.phone];
    [self.viewInput inputAmountDiscamer:[self amountDisclamer:smsBankingResponse.amount currency:smsBankingResponse.currency phone:smsBankingResponse.phone]];
    [self.viewInput inputProcessDisclamer:@"Процесс обрабатывается в течение 30 минут"];
    [self.viewInput inputStateIsEnabled:smsBankingResponse.isConnected];

    if (!smsBankingResponse.isConnected) {
        [self.viewInput inputRuleDisclamer:[self ruleDisclamer:smsBankingResponse.phone]];
    }
}

- (void)smsBankingStateDateDidFailured {
    [self.viewInput hideBafHUD];
    [self.smsBankingRouter close];
}

- (void)initSmsBankingDidReceived {
    [self.viewInput hideBafHUD];
    [self.viewInput showEnterSmsCodeView];
}

- (void)checkSmsCodeSuccess {
    [self.viewInput checkSmsCodeSuccess];
}

- (void)checkSmsCodeFailure {
    [self.viewInput checkSmsCodeFailure];
}

- (void)smsSentAgain {
    [self.viewInput smsSentAgain];
}

#pragma mark - helpers

- (NSString *)amountDisclamer:(NSNumber *)amount currency:(NSString *)currency phone:(NSString *)phone {
    return [NSString stringWithFormat:@""
            "Стоимость данной услуги - %@ %@ в месяц. "
            "SMS-банкинг будет подключен к мобильному номеру: %@. "
            "Для изменения номера телефона просим обратиться в "
            "call-центр по номеру 2555, либо в отделение Банка.", [amount decimalFormatString], currency, phone];
}

- (NSString *)ruleDisclamer:(NSString *)phone {
    return [NSString stringWithFormat:@""
            "Настоящим подтверждаю согласие на получение "
            "смс-сообщений и корректность указанного номера "
            "телефона %@ для подключения услуги «SMS-банкинг». "
            "С тарифом за абонентскую плату ознакомлен. "
            "Ответственность за предоставление недостоверных "
            "данных, я полностью беру на себя.", phone];
}

@end