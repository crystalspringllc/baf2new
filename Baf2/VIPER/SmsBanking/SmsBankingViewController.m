//
//  SmsBankingViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 10/3/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//


#import "SmsBankingViewController.h"
#import "MainButton.h"
#import "SMSConfirmationPopupViewController.h"
#import "SmsBankingChangePhoneNumberViewController.h"
#import <STPopup/STPopup.h>

@interface SmsBankingViewController ()
@property (weak, nonatomic) IBOutlet UILabel *phoneTitle;
@property (weak, nonatomic) IBOutlet UILabel *stateTitle;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *processLabel;
@property (weak, nonatomic) IBOutlet UILabel *rulesLabel;
@property (weak, nonatomic) IBOutlet UISwitch *rulesSwitch;
@property (weak, nonatomic) IBOutlet MainButton *mainButton;
@property (weak, nonatomic) IBOutlet UIView *switchContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *switchContainerHeight;
@property (weak, nonatomic) IBOutlet MainButton *changeNumberButton;

@property (nonatomic, strong) SMSConfirmationPopupViewController *v;
@end

@implementation SmsBankingViewController

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SmsBanking" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SmsBankingViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

    //ask output protocol send data via input protocol
    [self.output firstConfigure];
}

#pragma mark - config ib actions
- (IBAction)swipeSwitch:(UISwitch *)sender {
    if (sender.isOn) {
        [self.mainButton setMainButtonStyle:MainButtonStyleOrange];
    } else {
        [self.mainButton setMainButtonStyle:MainButtonStyleDisable];
    }
}

- (IBAction)clickMainButton:(id)sender {
    [self.output changeState];
}
- (IBAction)changeNumber:(id)sender {
    [self.output prepareToChangeNumber];
}

#pragma mark - view input protocol

- (void)moveToChangeNumber:(NSNumber *)cardId {
    SmsBankingChangePhoneNumberViewController *vc = [SmsBankingChangePhoneNumberViewController createVC];
    vc.cardId = cardId;
    vc.currencyPhoneString = self.phoneLabel.text ? : @"";
    [self.popupController pushViewController:vc animated:YES];
}

- (void)inputMainButtonTitle:(NSString *)title {
    [self.mainButton setTitle:title];
}

- (void)inputStateString:(NSString *)stateString color:(UIColor *)stateColor {
    self.stateLabel.text = stateString;
    self.stateLabel.textColor = stateColor;
}

- (void)inputPhoneNumber:(NSString *)phoneNumber {
    self.phoneLabel.text = phoneNumber;
}

- (void)inputAmountDiscamer:(NSString *)amountDisclamer {
    self.amountLabel.text = amountDisclamer;
}

- (void)inputProcessDisclamer:(NSString *)processDisclamer {
    self.processLabel.text = processDisclamer;
}

- (void)inputRuleDisclamer:(NSString *)ruleDisclamer {
    self.rulesLabel.text = ruleDisclamer;
    self.switchContainerHeight.constant = 120;
    self.switchContainer.hidden = NO;
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 500);
}

- (void)inputStateIsEnabled:(BOOL)isEnabled {
    if (isEnabled) {
        [self.mainButton setMainButtonStyle:MainButtonStyleOrange];
        [self.changeNumberButton setMainButtonStyle:MainButtonStyleOrange];
    } else {
        [self.mainButton setMainButtonStyle:MainButtonStyleDisable];
        [self.changeNumberButton setMainButtonStyle:MainButtonStyleDisable];
    }
}

- (void)showEnterSmsCodeView {
    SmsBankingViewController *wSelf = self;
    self.v = [[SMSConfirmationPopupViewController alloc] init];
    self.v.didDismissWithSuccessBlock = ^{};
    self.v.verifySmsCode = ^(SMSConfirmationPopupViewController *smsConfirmationPopup, NSString *smsCode) {
        [wSelf.output runWithSmsCode:smsCode];
    };
    self.v.sendSmsCodeAgain = ^(SMSConfirmationPopupViewController *smsConfirmationPopup) {
        [wSelf.output sendSmsCodeAgain];
    };
    [self.popupController pushViewController:self.v animated:YES];
}

 - (void)checkSmsCodeSuccess {
     [self.v smsTextFieldStopLoading];
     [self.v showSuccessIcon];
     SmsBankingViewController *wSelf = self;
     [self performBlock:^{
         [wSelf.v dismiss];
     } afterDelay:1];
 }

- (void)checkSmsCodeFailure {
    [self.v smsTextFieldStopLoading];
}

- (void)smsSentAgain {
    [self.v smsTextFieldStopLoading];
    [UIAlertView showWithTitle:nil message:NSLocalizedString(@"new_code_send", nil) cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
}

#pragma mark - hud things
//
// show and hide baf hud
//
- (void)showBafHUDWithTitle:(NSString *)title {
    [MBProgressHUD showAstanaHUDWithTitle:title animated:YES];
}

- (void)hideBafHUD {
    [MBProgressHUD hideAstanaHUDAnimated:YES];
}

#pragma mark - config ui

- (void)configUI {
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = NSLocalizedString(@"manage_sms_banking", nil);

    [self.mainButton setMainButtonStyle:MainButtonStyleOrange];
    [self.changeNumberButton setMainButtonStyle:MainButtonStyleWhite];

    self.switchContainerHeight.constant = 0;
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 420 - 50);
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"SmsBankingViewController DEALLOC");
}

@end
