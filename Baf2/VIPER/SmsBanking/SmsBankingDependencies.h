//
// Created by Askar Mustafin on 11/1/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class SmsBankingInteractor;


@interface SmsBankingDependencies : NSObject

@property (nonatomic, strong) SmsBankingInteractor *smsBankingInteractor; //interactor for assign cardId
@property (nonatomic, strong) UIViewController *parentVC;

- (void)installRootViewController;

@end
