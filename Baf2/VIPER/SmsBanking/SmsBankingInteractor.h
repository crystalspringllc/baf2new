//
// Created by Askar Mustafin on 10/3/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SmsBankingInteractorIO.h"
#import "ModelObject.h"

@protocol SmsBankingInteractorOutput;


@interface SmsBankingInteractor : NSObject<SmsBankingInteractorInput>

@property (nonatomic, strong) NSNumber *cardId;
@property (nonatomic, weak) id<SmsBankingInteractorOutput> output;

@end


@interface SmsBankingResponse : ModelObject

@property (nonatomic, strong) NSNumber *accountId;
@property (nonatomic, strong) NSString *accountType;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSString *cardNumber;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *iban;
@property (nonatomic, strong) NSString *iin;
@property (nonatomic, assign) BOOL isAvailable;
@property (nonatomic, assign) BOOL isConnected;
@property (nonatomic, strong) NSString *phone;


+ (SmsBankingResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;

@end