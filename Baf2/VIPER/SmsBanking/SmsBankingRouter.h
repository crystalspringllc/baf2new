//
//  SmsBankingRouter.h
//  Baf2
//
//  Created by Askar Mustafin on 10/3/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SmsBankingInteractor.h"

@class SmsBankingPresenter;

@interface SmsBankingRouter : NSObject

@property (nonatomic, strong) SmsBankingPresenter *smsBankingPresenter;

- (void)showSmsBankingModuleInVC:(UIViewController *)viewController;
- (void)close;

@end