//
//  AviaMilesRouter.m
//  Baf2
//
//  Created by Askar Mustafin on 10/3/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "AviaMilesRouter.h"
#import "AviaMilesViewController.h"
#import "AviaMilesPresenter.h"

@interface AviaMilesRouter() {}
@property (nonatomic, strong) AviaMilesViewController *aviaMilesViewController;
@end

@implementation AviaMilesRouter

- (void)pushAviaMilesModule:(UINavigationController *)navigationController
{
    AviaMilesViewController *aviaMilesViewController = [[AviaMilesViewController alloc] init];

    aviaMilesViewController.output = self.aviaMilesPresenter;
    self.aviaMilesPresenter.viewInput = aviaMilesViewController;

    self.aviaMilesViewController = aviaMilesViewController;
    [navigationController pushViewController:aviaMilesViewController animated:YES];
}

/**
 * Pushing to result view controller
 */
- (void)pushAviaMilesResultModuleWithData:(NSDictionary *)resultData {
    [self.aviaMilesResultRouter pushAviaMilesResultModule:self.aviaMilesViewController.navigationController];
    [self.aviaMilesResultRouter passPresenterResultData:resultData];
}

@end