//
//  AviaMilesPresenter.h
//  Baf2
//
//  Created by Askar Mustafin on 10/3/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AviaMilesViewIO.h"
#import "AviaMilesInteractorIO.h"
#import "Card.h"
#import "Statement.h"
#import "AviaMilesRouter.h"



@interface AviaMilesPresenter : NSObject<AviaMilesInteractorOutput, AviaMilesViewOutput>

@property (nonatomic, strong) id <AviaMilesInteractorInput> interactorInput;
@property (nonatomic, strong) id <AviaMilesViewInput> viewInput;

@property (nonatomic, strong) AviaMilesRouter *aviaMilesRouter;

@end