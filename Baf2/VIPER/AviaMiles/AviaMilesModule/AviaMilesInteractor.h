//
// Created by Askar Mustafin on 10/3/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AviaMilesInteractorIO.h"
#import "Card.h"
#import "Statement.h"

@protocol AviaMilesInteractorOutput;


@interface AviaMilesInteractor : NSObject<AviaMilesInteractorInput>

@property (nonatomic, weak) id<AviaMilesInteractorOutput> output;

@property (nonatomic, strong) Card *card;
@property (nonatomic, strong) Statement *stament;

@end