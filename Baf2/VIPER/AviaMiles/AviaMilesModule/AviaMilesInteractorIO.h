//
// Created by Askar Mustafin on 10/4/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"
#import "Statement.h"

@protocol AviaMilesInteractorInput <NSObject>

- (void)prepareCardForPresent;
- (void)initializeTransfer;
- (void)makeTransfer;

@end


@protocol AviaMilesInteractorOutput <NSObject>

- (void)didReceivedCard:(Card *)card;
- (void)didReceivedStatement:(Statement *)statement;

- (void)didInitTransferWithSuccess:(NSDictionary *)response;
- (void)didInitTransferWithFailure:(NSString *)message;

- (void)didTransferWithSuccess:(NSDictionary *)response;
- (void)didTransferWithFailure;

@end