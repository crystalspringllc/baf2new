//
//  AviaMilesViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 10/3/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Card.h"
#import "AviaMilesViewIO.h"


@interface AviaMilesViewController : UITableViewController<AviaMilesViewInput>

@property (nonatomic, strong) id<AviaMilesViewOutput> output;

@end
