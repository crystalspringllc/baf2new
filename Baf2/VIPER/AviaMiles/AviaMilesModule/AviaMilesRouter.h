//
//  AviaMilesRouter.h
//  Baf2
//
//  Created by Askar Mustafin on 10/3/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AviaMilesInteractor.h"
#import "AviaMilesResultRouter.h"

@class AviaMilesPresenter;

@interface AviaMilesRouter : NSObject

@property (nonatomic, strong) AviaMilesPresenter *aviaMilesPresenter;
@property (nonatomic, strong) AviaMilesResultRouter *aviaMilesResultRouter;


- (void)pushAviaMilesModule:(UINavigationController *)navigationController;

- (void)pushAviaMilesResultModuleWithData:(NSDictionary *)resultData;

@end
