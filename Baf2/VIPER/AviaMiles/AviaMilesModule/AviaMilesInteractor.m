//
// Created by Askar Mustafin on 10/3/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "AviaMilesInteractor.h"
#import "NewTransfersApi.h"


@interface AviaMilesInteractor()
@property (nonatomic, strong) NSDictionary *initializeTransferResponse;
@end

@implementation AviaMilesInteractor {}

#pragma mark - AviaMilesInteractorInput protocol implementations

- (void)prepareCardForPresent {
    [self.output didReceivedCard:self.card];
    [self.output didReceivedStatement:self.stament];
}

- (void)initializeTransfer {
    __weak AviaMilesInteractor *wSelf = self;
    [self initTransferWithSuccess:^(id response) {
        wSelf.initializeTransferResponse = response;
        [wSelf.output didInitTransferWithSuccess:response];
    } failure:^(NSString *code, NSString *message) {
        [wSelf.output didInitTransferWithFailure:message];
    }];
}

- (void)makeTransfer {
    __weak AviaMilesInteractor *wSelf = self;
    [self runTransferWithServiceLogId:self.initializeTransferResponse[@"servicelogId"] success:^(id response) {
        [wSelf.output didTransferWithSuccess:wSelf.initializeTransferResponse];
    } failure:^(NSString *code, NSString *message) {
        [wSelf.output didTransferWithFailure];
    }];
}

#pragma mark - data base request

- (void)initTransferWithSuccess:(success)success failure:(failure)failure {
    [NewTransfersApi initAviaTransfer:MY_BONUS_COMPENSATION
                          requestorId:self.card.cardId
                      requestorAmount:self.stament.aviaBonusMil
                        destinationId:self.card.cardId
                    destinationAmount:self.stament.aviaBonusKzt
                        requestorType:self.card.type
                    requestorCurrency:@"MIL"
                      destinationType:self.card.type
                  destinationCurrency:self.card.currency
                            direction:true
                                 save:false
                                alias:@""
                            reference:self.stament.operationId
                              success:success failure:failure];
}

- (void)runTransferWithServiceLogId:(NSNumber *)serviceLogId success:(success)success failure:(failure)failure {
    [NewTransfersApi runTransferAvia:serviceLogId success:success failure:failure];
}

@end
