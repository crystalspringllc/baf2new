//
//  AviaMilesViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 10/3/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

// USED VIPER PATTERN
//
// All data RECEIVE to this VIEW from PRESENTER
// Data RECEIVE via AviaMilesViewInterface PROTOCOL
//
// Data SENT to PRESENTER via  <AviaMilesModuleInterface>output PROTOCOL
//
// VIEW know NOTHING about ENTITIES (MODELS)

#import "AviaMilesViewController.h"
#import "TransfersFormCell.h"
#import "UITableViewController+Extension.h"
#import "MainButton.h"
#import "UITableViewController+Extension.h"

@interface AviaMilesViewController ()
@property (weak, nonatomic) IBOutlet TransfersFormCell *transferSenderCell;
@property (weak, nonatomic) IBOutlet TransfersFormCell *transferAccepterCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *totalTransferSumCell;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property (weak, nonatomic) IBOutlet MainButton *transferButton;
@end

@implementation AviaMilesViewController

- (id)init {
    self = [super init];
    if(self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"AviaMilesCompensation" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AviaMilesViewController class])];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

    //tell Presenter to configure staring data
    //it will call protocol method that
    //AviaMilesCompensationViewInterface
    //implemented here
    [self.output configureStart];
}

#pragma mark - config actions

- (IBAction)clickTransferButton:(id)sender {

    //CALL TO PRESENTER VIA <AviaMilesModuleInterface> output
    [self.output clickTransferButton];
}

#pragma mark - AviaMilesCompensationViewInterface (calls from presenter)

- (void)didIniTransferDone {

}

- (void)didCheckTransferDone {

}

//
// sender and accepter card cells
//

- (void)setSenderCellBalance:(NSString *)balance {
    self.transferSenderCell.rightDescriptionLabel.text = balance;
}

- (void)setSenderCellAlias:(NSString *)alias {
    self.transferSenderCell.mainTitleLabel.text = alias;
}

- (void)setSenderCellDescription:(NSString *)descriptionText {
    self.transferSenderCell.leftDescriptionLabel.text = descriptionText;
}

- (void)setAccepterCellBalance:(NSString *)balance {
    self.transferAccepterCell.rightDescriptionLabel.text = balance;
}

- (void)setAccepterCellAlias:(NSString *)alias {
    self.transferAccepterCell.mainTitleLabel.text = alias;
}

- (void)setAccepterCellDescription:(NSString *)descriptionText {
    self.transferAccepterCell.leftDescriptionLabel.text = descriptionText;
}

- (void)setBalanceDisclamerText:(NSString *)balanceDisclamerText {
    self.balanceLabel.text = balanceDisclamerText;
}

//
// total transfer sum
//

- (void)setTotalTransferSum:(NSAttributedString *)sum {
    self.totalTransferSumCell.textLabel.attributedText = sum;
}


//
// show and hide baf hud
//

- (void)showBafHUDWithTitle:(NSString *)title {
    [MBProgressHUD showAstanaHUDWithTitle:title animated:YES];
}

- (void)hideBafHUD {
    [MBProgressHUD hideAstanaHUDAnimated:YES];
}

#pragma mark - table view

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return NSLocalizedString(@"enroll_from", nil);
    } else if (section == 2) {
        return NSLocalizedString(@"enroll_on", nil);
    } else if (section == 3) {
        return NSLocalizedString(@"total_transfer_for_sum", nil);
    } else {
        return @"";
    }
}

#pragma mark - config ui

- (void)configUI {
    [self setNavigationTitle:NSLocalizedString(@"compesation_miles", nil)];

    [self setNavigationBackButton];
    [self setBAFTableViewBackground];
    [self configCells];
    [self.transferButton setTitle:NSLocalizedString(@"accept_transfer", nil)];
}

- (void)configCells {
    self.transferAccepterCell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.transferSenderCell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.transferAccepterCell.arrowImageView.hidden = YES;
    self.transferSenderCell.arrowImageView.hidden = YES;

    self.totalTransferSumCell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    self.totalTransferSumCell.textLabel.numberOfLines = 0;
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
