//
// Created by Askar Mustafin on 10/3/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AviaMilesViewOutput <NSObject>

- (void)configureStart;
- (void)clickTransferButton;

@end

@protocol AviaMilesViewInput <NSObject>


- (void)showBafHUDWithTitle:(NSString *)title;
- (void)hideBafHUD;

/**
 * Set statements.aviaBonusmil with currency string as card
 *
 * @param balance
 */
- (void)setSenderCellBalance:(NSString *)balance;

/**
 * Set card.alias with currency string as card
 *
 * @param alias
 */
- (void)setSenderCellAlias:(NSString *)alias;

/**
 * Set card.number as descriptionLabel in SenderCell
 *
 * @param descriptionText
 */
- (void)setSenderCellDescription:(NSString *)descriptionText;

/**
 * Set card.balanceWithCurrency as rightDescriptionLabel in AccepterCell
 *
 * @param balance
 */
- (void)setAccepterCellBalance:(NSString *)balance;

/**
 * Set card.alias as mainTitleLabel text in AccepterCell
 *
 * @param alias
 */
- (void)setAccepterCellAlias:(NSString *)alias;

/**
 * Set card.number as descriptionLabel in AccepterCell
 *
 * @param descriptionText
 */
- (void)setAccepterCellDescription:(NSString *)descriptionText;

/**
 * Disclamer that show avia miles balance in AccepterCell
 *
 * @param balanceDisclamerText
 */
- (void)setBalanceDisclamerText:(NSString *)balanceDisclamerText;

/**
 * Set total transfer sum
 *
 * @param sum
 */
- (void)setTotalTransferSum:(NSAttributedString *)sum;

@end