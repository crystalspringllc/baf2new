//
//  AviaMilesPresenter.m
//  Baf2
//
//  Created by Askar Mustafin on 10/3/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "AviaMilesPresenter.h"

@implementation AviaMilesPresenter
{
    NSDictionary *responseData;
}



#pragma mark - config this first from router

- (void)configureStart {
    [self.viewInput setSenderCellBalance:@"299.52 MIL"];

    //
    // Avia exchanges rate may be loaded from server
    [self.viewInput setBalanceDisclamerText:@"Загрузка обменного курса..."];

    //ask interactor to give card's data
    [self.interactorInput prepareCardForPresent];

    //prepate for transfer
    [self.viewInput showBafHUDWithTitle:@"Подготовка перевода"];
    [self.interactorInput initializeTransfer];
}

#pragma mark - action from interface

- (void)clickTransferButton {
    NSLog(@"clickTransferButton");
    //call interactor to prepare for transfer and
    //bring data that needed to ne shown in view
    [self.viewInput showBafHUDWithTitle:@"Перевод"];
    [self.interactorInput makeTransfer];
}

#pragma mark - implement AviaMilesInteractorIO outputs

//
// preparing for transfer
//
- (void)didInitTransferWithSuccess:(NSDictionary *)response {
    [self.viewInput hideBafHUD];
    responseData = [[NSDictionary alloc] initWithDictionary:response];
    NSDictionary *exchange = responseData[@"exchange"];
    NSString *currencyValue = exchange[@"presentation"];
    NSString *balanceDisclamer = [@"Курс валюты: " stringByAppendingString:currencyValue];
    [self.viewInput setBalanceDisclamerText:balanceDisclamer];
}
- (void)didInitTransferWithFailure:(NSString *)message {
    //implement showing failure message
}

//
// making transfer
//
- (void)didTransferWithSuccess:(NSDictionary *)response {
    [self.viewInput hideBafHUD];
    [self.aviaMilesRouter pushAviaMilesResultModuleWithData:response];
}

- (void)didTransferWithFailure {
    //implement showing failure message
}

//
// card and statement
//
- (void)didReceivedStatement:(Statement *)statement {
    [self.viewInput setTotalTransferSum:[self prepareFromStatement:statement]];
}

- (void)didReceivedCard:(Card *)card {
    //show balance alias and number in view
    [self.viewInput setSenderCellBalance:card.balanceAvailableWithCurrency];
    [self.viewInput setSenderCellAlias:card.alias];
    [self.viewInput setSenderCellDescription:card.number];

    [self.viewInput setAccepterCellBalance:card.balanceAvailableWithCurrency];
    [self.viewInput setAccepterCellAlias:card.alias];
    [self.viewInput setAccepterCellDescription:card.number];
}

#pragma mark - helpers

- (NSString *)addCurrencyToAviaBalance:(NSNumber *)balance {
    return [[balance decimalFormatString] stringByAppendingString:@" MIL"];
}

- (NSString *)addCurrencyToKztBalance:(NSNumber *)balance {
    return [[balance decimalFormatString] stringByAppendingString:@" KZT"];
}

- (NSNumber *)convertToKZTFromMIL:(NSNumber *)MIL{
    return @([MIL intValue] / 2);
}

- (NSAttributedString *)prepareFromStatement:(Statement *)statement {
    NSMutableAttributedString *totalString = [[NSMutableAttributedString alloc] init];

    NSString *aviaBonus = [self addCurrencyToAviaBalance:statement.aviaBonusMil];
    NSAttributedString *takeMiles = [self normalText:@"Списать:"
                                            boldText:[aviaBonus stringByAppendingString:@"\n"]];

    //NSString *kztBalance = [self addCurrencyToKztBalance:[self convertToKZTFromMIL:statement.aviaBonusMil]];
   // NSAttributedString *bringSum = [self normalText:@"Зачислить:"
                                         //  boldText:[kztBalance stringByAppendingString:@"\n"]];

   // NSAttributedString *totalSum = [self normalText:@"Итого:"
                                      //     boldText:kztBalance];

    NSString *kztBalance = [NSString stringWithFormat:@"%@ %@", statement.aviaBonusKzt, statement.currency];
   // NSLog(@"Response ");
    NSAttributedString *bringSum = [self normalText:@"Зачислить:"
                                            boldText:[kztBalance stringByAppendingString:@"\n"]];
                                    
    NSAttributedString *totalSum = [self normalText:@"Итого:"
                                          boldText:[kztBalance stringByAppendingString:@"\n"]];
    
    [totalString appendAttributedString:takeMiles];
    [totalString appendAttributedString:bringSum];
    [totalString appendAttributedString:totalSum];

    return totalString;
}

- (NSAttributedString *)normalText:(NSString *)normalText boldText:(NSString *)boldText {
    NSString *normalString = [normalText stringByAppendingString:@" "];

    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] init];

    UIFont *normalFont = [UIFont systemFontOfSize:14];
    UIFont *boldFont = [UIFont boldSystemFontOfSize:15];
    NSDictionary *normalAttributes = @{NSFontAttributeName : normalFont};
    NSDictionary *boldAttributes = @{NSFontAttributeName : boldFont};

    NSAttributedString *normalAttributedString = [[NSAttributedString alloc] initWithString:normalString attributes:normalAttributes];
    NSAttributedString *boldAttributedString = [[NSAttributedString alloc] initWithString:boldText attributes:boldAttributes];

    [attributedString appendAttributedString: normalAttributedString];
    [attributedString appendAttributedString: boldAttributedString];

    return [attributedString mutableCopy];
}

@end
