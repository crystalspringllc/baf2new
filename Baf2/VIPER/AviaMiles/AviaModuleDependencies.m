//
// Created by Askar Mustafin on 10/29/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "AviaModuleDependencies.h"
#import "AviaMilesRouter.h"
#import "AviaMilesPresenter.h"

@interface AviaModuleDependencies()
@property (nonatomic, strong) AviaMilesRouter *aviaMilesRouter;
@end

@implementation AviaModuleDependencies {}

- (id)init {
    self = [super init];
    if (self) {
        [self configureDependencies];
    }
    return self;
}

- (void)installRootViewController {
    [self.aviaMilesRouter pushAviaMilesModule:[UIHelper rootNavigationController]];
}

- (void)configureDependencies {
    //Transfer Modules Classes
    AviaMilesRouter *aviaMilesRouter = [[AviaMilesRouter alloc] init];
    AviaMilesPresenter *aviaMilesPresenter = [[AviaMilesPresenter alloc] init];
    AviaMilesInteractor *aviaMilesInteractor = [[AviaMilesInteractor alloc] init];

    //Result Modules Classes
    AviaMilesResultRouter *aviaMilesResultRouter = [[AviaMilesResultRouter alloc] init];
    AviaMilesResultPresenter *aviaMilesResultPresenter = [[AviaMilesResultPresenter alloc] init];

    //contections
    //interactor's OUTPUT with presenter
    //presenter's INPUT with interactor
    aviaMilesInteractor.output = aviaMilesPresenter;
    aviaMilesPresenter.interactorInput = aviaMilesInteractor;

    aviaMilesRouter.aviaMilesPresenter = aviaMilesPresenter;
    aviaMilesPresenter.aviaMilesRouter = aviaMilesRouter;

    self.aviaMilesRouter = aviaMilesRouter;
    self.aviaMilesInteractor = aviaMilesInteractor;


    //
    aviaMilesResultRouter.aviaMilesResultPresenter = aviaMilesResultPresenter;
    aviaMilesResultPresenter.aviaMilesResultRouter = aviaMilesResultRouter;


    aviaMilesRouter.aviaMilesResultRouter = aviaMilesResultRouter;
}

#pragma mark -

- (void)dealloc {
    NSLog(@"DEPENDENCY DEALLOC");
}

@end