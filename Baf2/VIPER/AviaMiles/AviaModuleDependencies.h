//
// Created by Askar Mustafin on 10/29/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AviaMilesInteractor.h"


@interface AviaModuleDependencies : NSObject

- (void)installRootViewController;

//For reference on card and statement
@property (nonatomic, strong) AviaMilesInteractor *aviaMilesInteractor;

@end