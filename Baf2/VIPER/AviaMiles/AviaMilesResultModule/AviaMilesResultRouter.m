//
// Created by Askar Mustafin on 10/28/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "AviaMilesResultRouter.h"
#import "AviaMilesResultViewController.h"
#import "NotificationCenterHelper.h"


@interface AviaMilesResultRouter() {}
@property (nonatomic, strong) AviaMilesResultViewController *aviaMilesResultViewController;
@property (nonatomic, strong) UINavigationController *nc;
@end

@implementation AviaMilesResultRouter {}

- (void)pushAviaMilesResultModule:(UINavigationController *)navigationController {
    self.nc = navigationController;

    AviaMilesResultViewController *aviaMilesViewController = [[AviaMilesResultViewController alloc] init];
    [aviaMilesViewController setNavigationTitle:NSLocalizedString(@"compesation_miles", nil)];

    aviaMilesViewController.output = self.aviaMilesResultPresenter;
    self.aviaMilesResultPresenter.viewInput = aviaMilesViewController;

    self.aviaMilesResultViewController = aviaMilesViewController;
    [navigationController pushViewController:aviaMilesViewController animated:YES];
}

- (void)passPresenterResultData:(NSDictionary *)resultData {
    self.aviaMilesResultPresenter.resultData = resultData;
}

- (void)goToHome {
    [self.nc popToRootViewControllerAnimated:NO];
}

- (void)goToHistory {
    [self.nc popToRootViewControllerAnimated:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferDoneGotoHistory object:nil];
}

@end