//
// Created by Askar Mustafin on 10/28/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AviaMilesResultPresenter.h"


@interface AviaMilesResultRouter : NSObject

@property (nonatomic, strong) AviaMilesResultPresenter *aviaMilesResultPresenter;

- (void)pushAviaMilesResultModule:(UINavigationController *)navigationController;
- (void)passPresenterResultData:(NSDictionary *)resultData;


- (void)goToHistory;
- (void)goToHome;

@end