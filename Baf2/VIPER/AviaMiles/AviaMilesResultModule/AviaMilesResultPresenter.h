//
// Created by Askar Mustafin on 10/29/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AviaMilesResultViewIO.h"

@class AviaMilesResultRouter;


@interface AviaMilesResultPresenter : NSObject<AviaMilesResultViewOutput>

@property (nonatomic, strong) id <AviaMilesResultViewInput> viewInput;

@property (nonatomic, strong) AviaMilesResultRouter *aviaMilesResultRouter;

@property (nonatomic, strong) NSDictionary *resultData;

@end