//
// Created by Askar Mustafin on 10/29/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AviaMilesResultViewInput <NSObject>
- (void)showResult:(NSArray *)array;
- (void)showTitle:(NSString *)title;
@end

@protocol AviaMilesResultViewOutput <NSObject>
- (void)clickGoHistoryButton;
- (void)clickGoHomeButton;
@end