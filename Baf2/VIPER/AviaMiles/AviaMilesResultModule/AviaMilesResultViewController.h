//
// Created by Askar Mustafin on 10/29/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AviaMilesResultViewIO.h"


@interface AviaMilesResultViewController : UIViewController<AviaMilesResultViewInput>
@property (nonatomic, strong) id<AviaMilesResultViewOutput> output;

- (void)showResult:(NSArray *)array;
- (void)showTitle:(NSString *)title;
@property (nonatomic, strong) NSString *tableTitle;
@property (nonatomic, strong) NSArray *dataForTable;

@end