//
// Created by Askar Mustafin on 10/29/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "AviaMilesResultViewController.h"
#import "AASimpleTableViewCell.h"
#import "MainButton.h"
#import "NotificationCenterHelper.h"

@interface AviaMilesResultViewController()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *footerButtonView;

@end

@implementation AviaMilesResultViewController {}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - config actions

- (void)goHistoryTapped {
    if (self.output) {
        [self.output clickGoHistoryButton];
    } else {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferDoneGotoHistory object:nil];
    }
}

- (void)goHomeTapped {
    if (self.output) {
        [self.output clickGoHomeButton];
    } else {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

#pragma mark - AviaMilesResultViewInput

- (void)showResult:(NSArray *)array {
    self.dataForTable = array;
    [self.tableView reloadData];
}

- (void)showTitle:(NSString *)title {
    self.tableTitle = title;
    [self.tableView reloadData];
}

#pragma mark - tableView

//header
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.tableTitle) {
        return 70;
    } else {
        return 0;
    }

}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [self tableView:tableView heightForHeaderInSection:0])];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (CGFloat) (headerView.width * 0.8), 1)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:16];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 0;
    titleLabel.text = self.tableTitle;
    [titleLabel sizeToFit];
    titleLabel.centerX = headerView.middleX;
    titleLabel.centerY = headerView.middleY;
    [headerView addSubview:titleLabel];
    return headerView;
}
//body
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *infoRowTitles = self.dataForTable.firstObject;
    return infoRowTitles.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *infoRowTitles = self.dataForTable[0];
    NSArray *infoRowSubtitles = self.dataForTable[1];
    NSString *title = infoRowTitles[(NSUInteger) indexPath.row];
    NSString *subtitle = infoRowSubtitles[(NSUInteger) indexPath.row];
    return [AASimpleTableViewCell cellHeightWithTitle:title subtitle:subtitle];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"cell";
    AASimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AASimpleTableViewCell alloc] initWithReuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSArray *infoRowTitles = self.dataForTable[0];
    NSArray *infoRowSubtitles = self.dataForTable[1];
    NSString *title = infoRowTitles[(NSUInteger) indexPath.row];
    NSString *subtitle = infoRowSubtitles[(NSUInteger) indexPath.row];
    [cell setTitle:title];
    [cell setSubtitle:subtitle];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    return cell;
}
//footer
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 122;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if(!self.footerButtonView)
    {
        self.footerButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [self tableView:tableView heightForFooterInSection:section])];

        // Goto main button
        MainButton *gohomeButton = [[MainButton alloc] initWithFrame:CGRectMake(0, 12, 220, 45)];
        gohomeButton.mainButtonStyle = MainButtonStyleOrange;
        gohomeButton.frame = CGRectMake(0, 20, 220, 40);
        gohomeButton.centerX = self.footerButtonView.middleX;
        gohomeButton.title = NSLocalizedString(@"go_to_main_activity", nil);
        [gohomeButton addTarget:self action:@selector(goHomeTapped) forControlEvents:UIControlEventTouchUpInside];
        [self.footerButtonView addSubview:gohomeButton];

        // Goto main button
        MainButton *goHistoryButton = [[MainButton alloc] initWithFrame:CGRectMake(0, gohomeButton.bottom + 8, 220, 45)];
        goHistoryButton.mainButtonStyle = MainButtonStyleOrange;
        goHistoryButton.frame = CGRectMake(0, gohomeButton.bottom + 8, 220, 45);
        goHistoryButton.centerX = self.footerButtonView.middleX;
        goHistoryButton.title = NSLocalizedString(@"ga_operation_history", nil);
        [goHistoryButton addTarget:self action:@selector(goHistoryTapped) forControlEvents:UIControlEventTouchUpInside];
        [self.footerButtonView addSubview:goHistoryButton];
    }
    return self.footerButtonView;
}

#pragma mark - config ui

- (void)configUI {
    //[self setNavigationTitle:NSLocalizedString(@"compesation_miles", nil)];
    [self setNavigationBackButton];
    [self setBafBackground];


    self.tableView = [[UITableView alloc]
            initWithFrame:CGRectMake(0,0,[ASize screenWidth],[ASize screenHeightWithoutStatusBarAndNavigationBar])
                    style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end