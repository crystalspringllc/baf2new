//
// Created by Askar Mustafin on 10/29/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "AviaMilesResultPresenter.h"
#import "AviaMilesResultRouter.h"


@implementation AviaMilesResultPresenter {}

#pragma mark - view ouput handlings

- (void)clickGoHistoryButton {
    [self.aviaMilesResultRouter goToHistory];
}

- (void)clickGoHomeButton {
    [self.aviaMilesResultRouter goToHome];
}

#pragma mark - view input callings

- (void)setResultData:(NSDictionary *)resultData {
    NSArray *dataForTable = [self prepareDataForTableView:resultData];
    [self.viewInput showResult:dataForTable];
    [self.viewInput showTitle:resultData[@"provider"][@"name"]];
}

#pragma mark - helpers

- (NSArray *)prepareDataForTableView:(NSDictionary *)resultData {
    NSMutableArray *infoRowTitles = [[NSMutableArray alloc] init];
    NSMutableArray *infoRowSubtitles = [[NSMutableArray alloc] init];

    //id операции
    [infoRowTitles addObject:NSLocalizedString(@"operation_id", nil)];
    NSNumber *servicelogId = resultData[@"servicelogId"];
    [infoRowSubtitles addObject:[servicelogId stringValue]];

//    [infoRowTitles addObject:NSLocalizedString(@"compensation_time", nil)];
//    [infoRowSubtitles addObject:[[NSDate date] stringWithDateFormat:@"dd.MM.yyyy"]];

    //карта списания
    [infoRowTitles addObject:NSLocalizedString(@"card_from_enroll", nil)];
    NSString *requestorCard = [NSString stringWithFormat:@"%@\n%@",
                    resultData[@"requestorAccount"][@"alias"],
                    resultData[@"requestorAccount"][@"key"]];
    [infoRowSubtitles addObject:requestorCard];

    //карта зачисления
    [infoRowTitles addObject:NSLocalizedString(@"card_to_enroll", nil)];
    NSString *destinationCard = [NSString stringWithFormat:@"%@\n%@",
                    resultData[@"destinationAccount"][@"alias"],
                    resultData[@"destinationAccount"][@"key"]];
    [infoRowSubtitles addObject:destinationCard];

    //Сумма зачисления
    [infoRowTitles addObject:NSLocalizedString(@"incoming_summ", nil)];
    NSNumber *destinationAmount = resultData[@"destinationAmount"];
    NSString *destinationCurrency = resultData[@"destinationCurrency"];
    NSString *dAmount = [NSString stringWithFormat:@"%@ %@",
                    [destinationAmount decimalFormatString],
                    destinationCurrency];
    [infoRowSubtitles addObject:dAmount];

    //Сумма списания
    [infoRowTitles addObject:NSLocalizedString(@"summ_to_entroll", nil)];
    NSNumber *requestorAmount = resultData[@"requestorAmount"];
    NSString *requestorCurrency = resultData[@"requestorCurrency"];
    NSString *rAmount = [NSString stringWithFormat:@"%@ %@",
                    [requestorAmount decimalFormatString],
                    requestorCurrency];
    [infoRowSubtitles addObject:rAmount];


    //Курс
    [infoRowTitles addObject:NSLocalizedString(@"convert_rate", nil)];
    NSNumber *currency = resultData[@"exchange"][@"presentation"];
    [infoRowSubtitles addObject:currency];


    //Комиссия
    [infoRowTitles addObject:NSLocalizedString(@"comission", nil)];
    NSNumber *comission = resultData[@"comission"];
    NSString *comissionCurrency = resultData[@"comissionCurrency"];
    NSString *сAmount = [NSString stringWithFormat:@"%@ %@",
                    [comission decimalFormatString],
                    comissionCurrency];
    [infoRowSubtitles addObject:сAmount];

    return @[infoRowTitles, infoRowSubtitles];
}

@end
