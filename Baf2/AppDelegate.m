//
//  AppDelegate.m
//  Baf2
//
//  Created by Askar Mustafin on 3/28/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ViewController.h"
#import "MenuViewController.h"
#import "PinHelper.h"
#import "User.h"
#import "CS.h"
#import "ATAppUpdater.h"
#import "AuthApi.h"
#import "MultilineLabel.h"
#import "MainButton.h"
#import "IPORouter.h"
#import "AFNetworkActivityLogger.h"
#import "AFNetworkActivityConsoleLogger.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <WatchConnectivity/WatchConnectivity.h>

@interface AppDelegate () <WCSessionDelegate>
@property(nonatomic, strong) MenuViewController *menu;
@end

@implementation AppDelegate


/*! 
    @brief calls when user tap button within version check view
    @disscussion Opens link to baf app in AppStore
*/
- (void)updateFromAppStore {
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"https://itunes.apple.com/kz/app/bank-astany-mobil-nyj-banking/id877622886?mt=8"]];
}

/*!
    @brief Checks version avilable by requesting method from server
    @discussion This method creates and shows UIView with message and button
                within it.
*/
- (void)checkNewVersionAvailable {
    [AuthApi checkUpdatesSuccess:^(id response) {

        if (response) {
            BOOL isNeedUpdate = [response[@"needUpdate"] boolValue];
            NSString *message = response[@"message"];
            if (isNeedUpdate && message && message.length > 0) {
                UIView *view = [[UIView alloc] initWithFrame:self.window.screen.bounds];
                view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];

                UIView *container = [[UIView alloc] initWithFrame:CGRectMake(20, 150, view.width - 40, 1)];
                container.backgroundColor = [UIColor whiteColor];
                container.layer.cornerRadius = 5;

                UILabel *messageLabel = [[UILabel alloc] init];
                messageLabel.width = container.width - 20;
                messageLabel.numberOfLines = 0;
                messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
                messageLabel.font = [UIFont systemFontOfSize:20];
                messageLabel.text = message;
                [messageLabel sizeToFit];
                messageLabel.y = 40;
                messageLabel.x = (container.width - messageLabel.width) * 0.5;
                [container addSubview:messageLabel];
                MainButton *button = [[MainButton alloc] init];
                button.mainButtonStyle = MainButtonStyleOrange;
                button.frame = CGRectMake(10, messageLabel.bottom + 20, container.width - 20, 44);
                [button setTitle:@"Обновить" forState:UIControlStateNormal];
                [button addTarget:self action:@selector(updateFromAppStore) forControlEvents:UIControlEventTouchUpInside];
                [container addSubview:button];

                container.height = button.bottom + 40;

                [view addSubview:container];
                [self.window addSubview:view];
            }
        }

    } failure:^(NSString *code, NSString *message) {

    }];
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    //Just prints documents directory folder path
    NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);

    // Check if new version is available
    [self checkNewVersionAvailable];

    //[[ATAppUpdater sharedUpdater] showUpdateWithForce];

    //Turnoff NSConstraints warning logs
    [[NSUserDefaults standardUserDefaults] setValue:@(NO) forKey:@"_UIConstraintBasedLayoutLogUnsatisfiable"];

    [Fabric with:@[[Crashlytics class]]];
    [GAN start];
    [CS startWithServerAppID:@"BAF_i"];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    NSUserDefaults *sud = [NSUserDefaults standardUserDefaults];
    BOOL userAlreadyLaunchedApp = [sud boolForKey:@"userAlreadyLaunchedApp"];
    if (userAlreadyLaunchedApp) {
        self.window.rootViewController = [self createRootNavigationController];
    } else {
        self.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OnboardingViewController"];
    }

    [self otherConfig];

    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];

    return YES;
}

#pragma mark -

/*!
    @brief Activates WCSeccion for watch os    
*/
- (void)otherConfig {
    // Watch
    if ([WCSession isSupported]) {
        WCSession *session = [WCSession defaultSession];
        session.delegate = self;
        [session activateSession];

        if (session.paired != true) {
            NSLog(@"Apple Watch is not paired");
        }
        if (session.watchAppInstalled != true) {
            NSLog(@"WatchKit app is not installed");
        }

    } else {
        NSLog(@"WatchConnectivity is not supported on this device");
    }
}

#pragma mark -

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[PINDiskCache sharedCache] setObject:[NSDate date] forKey:CacheAppEnterBackgroundDate];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    __weak AppDelegate *wSelf = self;

    [self performBlock:^{
        if([User sharedInstance].loggedIn) {
            NSDate *enterBackgroundDate = [[PINDiskCache sharedCache] objectForKey:CacheAppEnterBackgroundDate];
            if (enterBackgroundDate) {
                double secondsPassed = [[NSDate date] timeIntervalSinceDate:enterBackgroundDate];
                if (secondsPassed > 180) {
                    [wSelf logoutUser];
                    [Toast showToast:NSLocalizedString(@"session_expired", nil)];
                    [[PINDiskCache sharedCache] removeObjectForKey:CacheAppEnterBackgroundDate];
                }
            }
        }
    } afterDelay:0.75];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kIsShownPerSession];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark -

- (LGSideMenuController *)createRootNavigationController {
    UINavigationController *navigationController = [UIHelper defaultNavigationController];
    ViewController *viewController = [ViewController createVC];
    [navigationController setViewControllers:@[viewController]];

    self.menu = [[MenuViewController alloc] init];

    LGSideMenuController *side = [[LGSideMenuController alloc] initWithRootViewController:navigationController];
    //LGSideMenuPresentationStyleSlideAbove
    [side setRightViewEnabledWithWidth:[ASize screenWidth] - 80
                     presentationStyle:LGSideMenuPresentationStyleSlideAbove
                  alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];
    side.rightViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnAll;
    [side.rightView addSubview:self.menu.view];

    return side;
}

#pragma mark -

- (void)logoutUser {
    if([User sharedInstance].loggedIn) {
        [User logout];
        self.window.rootViewController = [self createRootNavigationController];
    }
}

#pragma mark - WCSessionDelegate

- (void)session:(WCSession *)session activationDidCompleteWithState:(WCSessionActivationState)activationState error:(NSError *)error {

}

- (void)sessionDidBecomeInactive:(WCSession *)session {

}

- (void)sessionDidDeactivate:(WCSession *)session {

}

- (void)session:(WCSession *)session didReceiveMessage:(nonnull NSDictionary<NSString *,id> *)message replyHandler:(nonnull void (^)(NSDictionary<NSString *,id> * _Nonnull))replyHandler {
    if (message[wcMessageType] && [message[wcMessageType] isEqualToString:wcMessageTypeGetPinToken]) {
        NSString *pinToken = @"";
        if ([PinHelper pinToken]) {
            pinToken = [PinHelper pinToken];
        }
        NSString *deviceID = [AppUtils deviceID];
        NSDictionary *messsageObject = @{@"pinToken": pinToken, @"deviceID": deviceID};

        NSDictionary *sendPinToken = @{wcMessageType: wcMessageTypeGetPinToken, wcMessageObject: messsageObject};

        replyHandler(sendPinToken);
    }
}

@end
