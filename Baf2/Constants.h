//л
//  Constants.h
//  Baf2
//
//  Created by Shyngys Kassymov on 14.04.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>


#define kAppID @"877622886"

// Test or prod
#define IS_API_URL_TEST YES
#define IS_API_RESPONSE_LOG YES

// Constants
#define kSupportEmail @"24@bankastana.kz"
#define kBankPrivacyUrl @"https://www.myth.kz/public/politics-baf/politics-ru.html"
#define kBankPriilegeUrl @"https://www.bankastana.kz/ru/benefits"
#define kBafLogoUrlSmall @"https://myth.kz/public/mob/providers/MOB_bankastana.png"

// Config
#define kAppGroupContainer @"group.kz.crystalspring.baf"
#define kCharityProviderId 10
#define kProcessingBankKkb @"epay_kkb"
#define kProcessingBankAlpha @"alpha"
#define kProcessingBankBAF @"ow_baf"
#define kBrandId @"2"
#define kPostDateFormat @"dd/MM/yyyy HH:mm:ss"
#define kScreenWidth [ASize screenWidth]
#define kViewSidePadding 15
#define kContentWidth kScreenWidth - 2 * kViewSidePadding
#define kCheckCardGoToOrderCallCode 940

// UserDefaults
#define kHideOnlineDepositeInfo @"kShowOnlineDepositeInfo"

// Assets
static NSString* const BASE_IMAGE_URL = @"https://24.bankastana.kz/assets/images/";
static NSString* const PROVIDER_IMAGE_URL = @"https://24.bankastana.kz/assets/images/providers/";

// Activities
#define ActivityOpenRequestCard @"kz.crystalspring.baf.activityOpenRequestCard"

// Watch
static NSString* const wcMessageType = @"wcMessageType";
static NSString* const wcMessageObject = @"wcMessageObject";
static NSString* const wcMessageTypeGetPinToken = @"wcMessageTypeGetPinToken";

// Providers
#define ONAY_INFO_REQUEST_TYPE 4
#define KAZAKHTELECOM_ID 5
#define CELLULAR_CATEGORY_ID 1

// Popup
#define POPUP_CORNER_RADIUS 6

// Error Codes
#define UPDATE_PASSWORD @"UPDATE_PASSWORD"


#pragma mark - TransferFormLocalization Constants

#define kTextStringError NSLocalizedString(@"error", nil)
#define kTextStringErrorInForm NSLocalizedString(@"error_in_form", nil)
#define kTextStringFillAllFields NSLocalizedString(@"fill_in_all_fields", nil)
#define kTextStringFirstEnterTransferSumm NSLocalizedString(@"first_enter_summ_of_transfer", nil)
#define kTextStringForSelectKnp NSLocalizedString(@"for_select_knp_first_select", nil)
#define kTextStringAddingReceiverAccout NSLocalizedString(@"add_beneficiar_dlg_title", nil)
#define kTextStringcCncel NSLocalizedString(@"cancel", nil)
#define kTextStringFizFace NSLocalizedString(@"individual", nil)
#define kTextStringJurFace NSLocalizedString(@"legal_entity", nil)
#define kTextStringDeletaTemplate NSLocalizedString(@"remove_template", nil)
#define kTextStringRemove NSLocalizedString(@"delete", nil)
#define kTextStringDeletingTemplate NSLocalizedString(@"deleting_template", nil)
#define kTextStringTemplateSuccesfullyRemoved NSLocalizedString(@"template_successfully_removed", nil)
#define kTextStringLoadingOperationalAccounts NSLocalizedString(@"loading_oparational_accounts", nil)
#define kTextStringChecking NSLocalizedString(@"checking", nil)
#define kTextStringProviderTemporaryUnavailable NSLocalizedString(@"provide_temporary_unavailable", nil)
#define kTextStringIncorrectKnpCode NSLocalizedString(@"incorrect_knp_code", nil)
#define kTextStringChooseAccountsFortransfer NSLocalizedString(@"choose_accounts_for_transfer", nil)
#define kTextStringEnterTemplateName NSLocalizedString(@"enter_template_name", nil)
#define kTextStringChekingTransfer NSLocalizedString(@"check_transfer", nil)
#define kTextStringPreparingToTransfer NSLocalizedString(@"prepare_transfer", nil)
#define kTextStringChooseAccount NSLocalizedString(@"choose_account", nil)
#define kTextStringNoAccountsForTransfer NSLocalizedString(@"empty_from_list", nil)
#define kTextStringYouHaveNoAccountForTransfer NSLocalizedString(@"you_have_no_accounts_for_transfer", nil)
#define kTextStringGoToCardAndAccountsForAdd NSLocalizedString(@"moveto_cards_and_account", nil)
#define kTextStringYouHaveNoAccountsForTransfer NSLocalizedString(@"you_have_no_accounts_for", nil)
#define kTextStringRateFor NSLocalizedString(@"course_for", nil)
#define kTextStringWillEnroll NSLocalizedString(@"will_be_enroll", nil)
#define kTextStringComission NSLocalizedString(@"comission", nil)
#define kTextStringWillWrittenOff NSLocalizedString(@"will_writen_off", nil)
#define kTextStringNoAcocuntForWrittenOff NSLocalizedString(@"no_account_for_write_off", nil)
#define kTextStringCards NSLocalizedString(@"_cards", nil)
#define kTextStringAcounts NSLocalizedString(@"accounts", nil)
#define kTextStringDeposits NSLocalizedString(@"_deposits", nil)
#define kTextStringWriteOffFrom NSLocalizedString(@"write_off_from", nil)
#define kTextStringActions NSLocalizedString(@"actions", nil)
#define kTextStringEditList NSLocalizedString(@"edit_key", nil)
#define kTextStringEnrollOn NSLocalizedString(@"enroll_on", nil)
#define kTextStringIncorrectKnpText NSLocalizedString(@"incorrect_knp_code", nil)
#define kTextStringTransfer NSLocalizedString(@"transfer", nil)
