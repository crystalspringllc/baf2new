//
//  NewsDetailViewController.h
//  Baf2
//
//  Created by nmaksut on 17.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"

@interface NewsDetailViewController : UITableViewController
@property (nonatomic, strong) News *news;
+ (id)createVC;
@end
