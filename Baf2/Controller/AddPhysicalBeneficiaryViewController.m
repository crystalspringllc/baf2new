//
//  AddPhysicalBeneficiaryViewController.mIinSuperTextField
//  BAF
//
//  Created by Almas Adilbek on 8/20/15.
//
//

#import <PureLayout/NSLayoutConstraint+PureLayout.h>
#import "AddPhysicalBeneficiaryViewController.h"
#import "UIViewController+Extension.h"
#import "DropdownSuperTextFieldView.h"
#import "IinSuperTextFieldView.h"
#import "IbanSuperTextFieldView.h"
#import "AppUtils.h"
#import "SuperTextFieldView+Validation.h"
#import "TcInfo.h"
#import "BankInfo.h"
#import "MainButton.h"
#import "InfoApi.h"
#import "AccountsApi.h"
#import "NotificationCenterHelper.h"
#import "UIView+AAPureLayout.h"
#import "NSString+Ext.h"
#import "MBProgressHUD+AstanaView.h"
#import "TwoCheckBoxesSegmentedControl.h"

@interface AddPhysicalBeneficiaryViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet IinSuperTextFieldView *iinField;
@property (weak, nonatomic) IBOutlet SuperTextFieldView *surnameField;
@property (weak, nonatomic) IBOutlet SuperTextFieldView *nameField;
@property (weak, nonatomic) IBOutlet IbanSuperTextFieldView *ibanField;
@property (weak, nonatomic) IBOutlet UILabel *bankBicLabel;
@property (weak, nonatomic) IBOutlet UILabel *bankNameLabel;
@property (weak, nonatomic) IBOutlet MainButton *submitButton;
@property (weak, nonatomic) IBOutlet UILabel *ibanErrorLabel;
@property (weak, nonatomic) IBOutlet TwoCheckBoxesSegmentedControl *residentSegmentedControl;

// Constraints.
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ibanErrorLabelTopConstraint;
@end

@implementation AddPhysicalBeneficiaryViewController {
    BOOL bankInfoLoading;

    TcInfo *_tcInfo;
    BankInfo *_bankInfo;

    CGFloat ibanErrorLabelTopConstraintConstant;
    NSLayoutConstraint *ibanErrorLabelHeightConstraint;
}

+ (instancetype)createVC
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"AmongBanksTransfer" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AddPhysicalBeneficiaryViewController class])];
}

-(void)initVars {
    ibanErrorLabelTopConstraintConstant = self.ibanErrorLabelTopConstraint.constant;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initVars];
    [self configUI];

    __weak AddPhysicalBeneficiaryViewController * weakSelf = self;
    [_iinField onValueChanged:^(NSString *value) {
        if([value isNotEmpty]) {
            [weakSelf loadBeneficiaryInfo];
        }
    }];

    [_ibanField onValueChanged:^(NSString *value) {
        if([value isNotEmpty]) {
            [weakSelf loadBankInfo];
        }
    }];
}

#pragma mark -
#pragma mark Actions

- (IBAction)tapConfirm:(id)sender
{
    if(!self.residentSegmentedControl.segmentItemsChecked)
    {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"is_resident", nil)];
        return;
    }
    
    if(![self.iinField validateIin]) return;

    if([[self.nameField value] isEmpty]) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_receiver_name", nil)];
        return;
    }
    if([[self.surnameField value] isEmpty]) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_receiver_surname", nil)];
        return;
    }
    
    if(![self.ibanField validateIBAN]) return;

    if(!_bankInfo) {
        if(bankInfoLoading) {
            [UIHelper showAlertWithMessage:NSLocalizedString(@"wait_for_loading", nil)];
        } else {
            [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_correct_iban", nil)];
        }
        return;
    }

    NSString *alias = [NSString stringWithFormat:@"%@ %@", [self.nameField value], [self.surnameField value]];

    __weak AddPhysicalBeneficiaryViewController *weakSelf = self;
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"adding", nil) animated:true];
    [AccountsApi saveInterbankBeneficiary:self.iinField.value
                                     iban:self.ibanField.value
                                    alias:alias
                                firstName:self.nameField.value
                                 lastName:self.surnameField.value
                              companyName:@""
                               isResident:self.residentSegmentedControl.selectedItemIndex == 0
                           economicSector:@(9)
                                  success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferRetailBeneficiarAdded object:response];
        [weakSelf goBack];

    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark -
#pragma mark Load

- (void)loadBeneficiaryInfo
{
    [self performBlock:^
    {
        if([_iinField validateIin])
        {
            [_nameField startLoading];
            [_surnameField startLoading];
            
            __weak AddPhysicalBeneficiaryViewController *weakSelf = self;
            
            [InfoApi clientInfo:self.iinField.value success:^(id response) {
                _tcInfo = response;
                [_nameField stopLoading];
                [_surnameField stopLoading];
                
                if (response[@"firstName"] && [response[@"firstName"] length] > 0) {
                    [_nameField setValue:response[@"firstName"]];
                    _nameField.textField.enabled = false;
                }
                if (response[@"lastName"] && [response[@"lastName"] length] > 0) {
                    [_surnameField setValue:response[@"lastName"]];
                    _surnameField.textField.enabled = false;
                }
                if (response[@"isResident"]) {
                    [weakSelf.residentSegmentedControl toggleCheckbox:[response[@"isResident"] boolValue] ? 0 : 1];
                }
                
                [weakSelf checkRequiredFilled];

            } failure:^(NSString *code, NSString *message) {
                _tcInfo = nil;
                [_nameField stopLoading];
                [_surnameField stopLoading];
                
                [_nameField clear];
                [_surnameField clear];

            }];
        }
    } afterDelay:0.5];
}

- (void)loadBankInfo
{
    [self hideIbanErrorLabel];

    [self performBlock:^
    {
        if([_ibanField validateIBAN])
        {
            bankInfoLoading = YES;

            self.bankNameLabel.text = NSLocalizedString(@"dialog_loading", nil);
            self.bankBicLabel.text = self.bankNameLabel.text;

            __weak AddPhysicalBeneficiaryViewController *weakSelf = self;
            
            [InfoApi bankInfo:self.ibanField.value success:^(id response) {
                _bankInfo = response;
                
                if (response[@"name"] && [response[@"name"] length] > 0) {
                    weakSelf.bankNameLabel.text = response[@"name"];
                }
                if (response[@"bic"] && [response[@"bic"] length] > 0) {
                    weakSelf.bankBicLabel.text = response[@"bic"];
                }
                
                bankInfoLoading = NO;
            } failure:^(NSString *code, NSString *message) {
                _bankInfo = nil;
                weakSelf.bankNameLabel.text = @"-";
                weakSelf.bankBicLabel.text = weakSelf.bankNameLabel.text;
                
                weakSelf.ibanErrorLabel.text = message;
                [weakSelf showIbanErrorLabel];
                
                bankInfoLoading = NO;
            }];
        }
    } afterDelay:0.5];
}

#pragma mark -
#pragma mark Helper

- (void)checkRequiredFilled {
//    self.submitButton.enabled = [_iinField isFilled] && [_ibanField isFilled] && _tcInfo && _bankInfo;
}

- (void)showIbanErrorLabel
{
    if(self.ibanErrorLabel.hidden) {
        [ibanErrorLabelHeightConstraint autoRemove];
        self.ibanErrorLabelTopConstraint.constant = ibanErrorLabelTopConstraintConstant;
        self.ibanErrorLabel.hidden = NO;
    }
}

- (void)hideIbanErrorLabel {
    if(!self.ibanErrorLabel.hidden) {
        ibanErrorLabelHeightConstraint = [_ibanErrorLabel aa_setHeight:0];
        self.ibanErrorLabelTopConstraint.constant = 0;
        self.ibanErrorLabel.hidden = YES;
    }
}

#pragma mark -

- (void)configUI
{
    [self setBafBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"add_beneficiar_dlg_title", nil)];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.scrollView.backgroundColor = [UIColor clearColor];
    
    [self.iinField setPlaceholder:NSLocalizedString(@"beneficiar_iin", nil)];
    [self.surnameField setPlaceholder:NSLocalizedString(@"last_name", nil)];
    [self.nameField setPlaceholder:NSLocalizedString(@"first_name_middle_name", nil)];
    [self.ibanField setPlaceholder:NSLocalizedString(@"recipient_iban", nil)];
    self.ibanField.textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;

    self.bankBicLabel.text = @"-";
    self.bankNameLabel.text = @"-";

    self.submitButton.mainButtonStyle = MainButtonStyleOrange;
    
    [self hideIbanErrorLabel];

    self.residentSegmentedControl.items = @[@"Резидент", @"Не резидент"];
    [self.residentSegmentedControl uncheckedItems];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
