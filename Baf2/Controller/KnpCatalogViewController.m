//
//  KnpCatalogViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 03.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "KnpCatalogViewController.h"
#import "CatalogTableViewCell.h"
#import "Knp.h"

@interface KnpCatalogViewController () <UISearchBarDelegate>

@property (nonatomic, strong) NSArray *knpList;
@property (nonatomic, strong) NSMutableArray *groupedKnpList;
@property (nonatomic, strong) NSMutableArray *filteredGroupedKnpList;

@end

@implementation KnpCatalogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
    [self getKnpList];
}

#pragma mark - API

- (void)getKnpList {
        NSMutableArray *individualKnpList = [NSMutableArray new];
        for (NSDictionary *knpData in self.preparedKnpList) {
            Knp *knp = [Knp instanceFromDictionary:knpData];
            [individualKnpList addObject:knp];
        }

        self.groupedKnpList = [NSMutableArray new];
        [self.groupedKnpList addObjectsFromArray:individualKnpList];
        NSSortDescriptor *sortKnpByCode = [[NSSortDescriptor alloc] initWithKey:@"code" ascending:true];
        [self.groupedKnpList sortUsingDescriptors:@[sortKnpByCode]];
        self.filteredGroupedKnpList = [NSMutableArray arrayWithArray:self.groupedKnpList];
        [self.tableView reloadData];
        [self checkForEmptiness];

        return;
}

- (void)onKnpListResponse {
    self.groupedKnpList = [NSMutableArray new];
    if (self.knpList && self.isLegal) {
        [self.groupedKnpList addObjectsFromArray:self.knpList];
    }
    
    NSSortDescriptor *sortKnpByCode = [[NSSortDescriptor alloc] initWithKey:@"code" ascending:true];
    [self.groupedKnpList sortUsingDescriptors:@[sortKnpByCode]];
    
    self.filteredGroupedKnpList = [NSMutableArray arrayWithArray:self.groupedKnpList];
    
    [self.tableView reloadData];
    [self checkForEmptiness];
}

#pragma mark - Methods

- (void)checkForEmptiness {
    if (!self.filteredGroupedKnpList || self.filteredGroupedKnpList.count == 0) {
        UILabel *emptyLabel = [[UILabel alloc] initWithFrame:self.tableView.bounds];
        emptyLabel.font = [UIFont boldSystemFontOfSize:16];
        emptyLabel.textAlignment = NSTextAlignmentCenter;
        emptyLabel.textColor = [UIColor lightGrayColor];
        emptyLabel.text = NSLocalizedString(@"no_results", nil);
        
        self.tableView.backgroundView = emptyLabel;
    } else {
        self.tableView.backgroundView = nil;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filteredGroupedKnpList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CatalogTableViewCell";

    CatalogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (!cell) {
        cell = [[CatalogTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    Knp *knp = self.filteredGroupedKnpList[(NSUInteger) indexPath.row];

    cell.nameLabel.text = knp.name;
    cell.descriptionLabel.text = @"";

    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    if (self.didSelectKnpItem) {
        self.didSelectKnpItem(self.filteredGroupedKnpList[(NSUInteger) indexPath.row]);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Config UI

- (void)configUI {
    [self setNavigationBackButton];
    
    self.title = NSLocalizedString(@"knp_list", nil);

    [self setNavigationTitle:self.title];

    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 83;
    self.tableView.tableFooterView = [UIView new];
}

- (void)dealloc {
    NSLog(@"knp catalog dealloc");
}

@end
