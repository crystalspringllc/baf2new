//
//  ChangePhoneConfirmViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 6/22/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePhoneConfirmViewController : UIViewController

@property (nonatomic, strong) NSString *neuPhoneStr;
@property (nonatomic, strong) NSNumber *serviceLogId;
@property (nonatomic, assign) BOOL isEmailInsteadOfPhone;
+ (instancetype)createVC;

@end
