//
//  CardMultiCurrencyConverterConfirmationViewController.m
//  BAF
//
//  Created by Almas Adilbek on 6/10/15.
//
//

#import "CardMultiCurrencyConverterConfirmationViewController.h"
#import "AASimpleTableViewCell.h"
#import "TransferCurrencyRates.h"
#import "AATableSection.h"
#import "CardsApi.h"
#import "CardMultiCurrencyConverterResultViewController.h"
#import "Card.h"
#import "MainButton.h"
#import "NewTransfersApi.h"

@interface CardMultiCurrencyConverterConfirmationViewController ()
@property (weak, nonatomic) IBOutlet UITableView *list;
@end

@implementation CardMultiCurrencyConverterConfirmationViewController {
    UIView *footerButtonView;
    AATableSection *tableSection;
}

- (id)init
{
    self = [super init];
    if(self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CardMultiCurrencyConverter" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([CardMultiCurrencyConverterConfirmationViewController class])];
        [self initVars];
    }
    return self;
}

- (void)initVars {}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];

    tableSection = [[AATableSection alloc] initWithSectionId:@"tableSection"];
    [tableSection addRow:@{@"title" : NSLocalizedString(@"multi_card", nil), @"value" : self.cardCurrencyAccount.number}];
    [tableSection addRow:@{@"title" : NSLocalizedString(@"from_curr_to_curr", nil), @"value" : [NSString stringWithFormat:@"%@ → %@", self.fromCurrency, self.toCurrency]}];
    [tableSection addRow:@{@"title" : NSLocalizedString(@"amount", nil), @"value" : [NSString stringWithFormat:@"%@ %@ (%@)", [_amount decimalFormatString], self.fromCurrency, self.convertedAmountWithCurrency]}];
    [tableSection addRow:@{@"title" : NSLocalizedString(@"operation_commission", nil), @"value" : [NSString stringWithFormat:@"%@ %@", [self.comission decimalFormatString], self.comissionCurrency]}];
//    [tableSection addRow:@{@"title" : @"На валюту:", @"value" : self.toCurrency}];

    NSString *formattedRatesInfo = [self.currencyRates presentation];
    formattedRatesInfo = [formattedRatesInfo stringByReplacingOccurrencesOfString:@"</br>" withString:@"\n"];
    formattedRatesInfo = [formattedRatesInfo stringByReplacingOccurrencesOfString:@"<b>" withString:@""];
    formattedRatesInfo = [formattedRatesInfo stringByReplacingOccurrencesOfString:@"</b>" withString:@""];
    [tableSection addRow:@{@"title" : NSLocalizedString(@"convert_rate", nil), @"value" : [NSString stringWithFormat:@"%@ %@", formattedRatesInfo, [self.currencyRates getUpdateTime]]}];

    [self.list reloadData];
}

#pragma mark -
#pragma mark Actions

- (void)confirmTapped {
    [self performTransfer];
}

- (void)performTransfer
{
    __weak CardMultiCurrencyConverterConfirmationViewController *weakSelf = self;
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"convert", nil) animated:true];
    [NewTransfersApi runTransfer:self.servicelogId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        [weakSelf onConvertationSuccess:[self.servicelogId stringValue]];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

- (void)onConvertationSuccess:(NSString *)operationId
{
    CardMultiCurrencyConverterResultViewController *v = [[CardMultiCurrencyConverterResultViewController alloc] init];
    v.servicelogId = self.servicelogId;
    v.operationId = operationId;
    v.amount = self.amount;
    v.cardCurrencyAccount = self.cardCurrencyAccount;
    v.currencyRates = self.currencyRates;
    v.fromCurrency = self.fromCurrency;
    v.toCurrency = self.toCurrency;
    v.comission = self.comission;
    v.comissionCurrency = self.comissionCurrency;
    v.saveAsTemplate = self.saveAsTemplate;
    [self.navigationController pushViewController:v animated:YES];
}

#pragma mark -
#pragma mark UITableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return tableSection.rows.count;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return NSLocalizedString(@"accept_convert_multi_card", nil);
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 100;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(!footerButtonView) {
        footerButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [self tableView:tableView heightForFooterInSection:section])];
        // Button
        MainButton *submitButton = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, 170, 40)];
        submitButton.mainButtonStyle = MainButtonStyleOrange;
        [submitButton addTarget:self action:@selector(confirmTapped) forControlEvents:UIControlEventTouchUpInside];
        submitButton.frame = CGRectMake(0, 0, 170, 40);
        submitButton.centerX = footerButtonView.middleX;
        submitButton.centerY = footerButtonView.middleY - 10;
        [submitButton setTitle:NSLocalizedString(@"accepting", nil)];
        [footerButtonView addSubview:submitButton];
    }
    return footerButtonView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *row = tableSection.rows[indexPath.row];
    NSString *title = row[@"title"];
    NSString *subtitle = row[@"value"];
    return [AASimpleTableViewCell cellHeightWithTitle:title subtitle:subtitle];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    AASimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AASimpleTableViewCell alloc] initWithReuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.font = [UIFont boldSystemFontOfSize:15];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    NSDictionary *row = tableSection.rows[indexPath.row];

    [cell setTitle:row[@"title"]];
    [cell setSubtitle:row[@"value"]];
    
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

    return cell;
}

#pragma mark -

- (void)configUI {
    [self setBafBackground];
    [self setNavigationTitle:NSLocalizedString(@"accepting_of_data", nil)];
    [self setNavigationBackButton];
    
    self.list.backgroundColor = [UIColor clearColor];
    self.list.backgroundView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc {}

@end
