//
//  ChangePhoneEmailViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 6/22/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePhoneEmailViewController : UIViewController

@property (nonatomic, assign) BOOL isPhone;

+ (instancetype)createVC;

@end
