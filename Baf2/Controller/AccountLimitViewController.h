//
//  AccountLimitViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 12.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Card, CardLimit;

@interface AccountLimitViewController : UIViewController

@property (nonatomic, weak) Card *card;
@property (nonatomic, weak) CardLimit *limit;

@end
