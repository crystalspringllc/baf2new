//
//  OnboardingViewController.swift
//  Baf2
//
//  Created by Shyngys Kassymov on 24.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {
    
    @IBOutlet weak var onboarding: PaperOnboarding!
    @IBOutlet weak var startButton: MainButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        onboarding.currentIndex(0, animated: true)
        
        startButton.mainButtonStyle = MainButtonStyle.orange
        startButton.title = "Приступить к работе"
        startButton.alpha = 0;
        startButton.translatesAutoresizingMaskIntoConstraints = false
        startButton.addTarget(self, action: #selector(OnboardingViewController.skipOnboarding(_:)), for: .touchUpInside)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

// MARK: Actions

extension OnboardingViewController {
    
    @IBAction func skipOnboarding(_ sender: AnyObject) {        
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor.white
        
        UIHelper.appDelegate().window.rootViewController = UIHelper.appDelegate().createRootNavigationController()
        
        let sud = UserDefaults.standard
        sud.set(true, forKey: "userAlreadyLaunchedApp")
        sud.synchronize()
        
    }
    
}

// MARK: PaperOnboardingDelegate

extension OnboardingViewController: PaperOnboardingDelegate {
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        if (index != 5) {
            startButton.alpha = 0
        } else {
            UIView.animate(withDuration: 0.745, animations: {
                self.startButton.alpha = 1
            })
        }
    }
    
    func onboardingDidTransitonToIndex(_ index: Int) {
        
    }
    
    func onboardingConfigurationItem(_ item: OnboardingContentViewItem, index: Int) {
        item.bottomConstraint?.constant = -100
        
        if index != 5 {
            item.imageView!.setNeedsLayout()
            item.imageView!.layoutIfNeeded()
            
            let circleView = UIView()
            circleView.translatesAutoresizingMaskIntoConstraints = false
            circleView.backgroundColor = UIColor.black
            circleView.layer.cornerRadius = (item.imageView?.frame.size.width)!/2.0
            circleView.alpha = 0.25
            item.insertSubview(circleView, at: 0)
            item.addConstraints([
                NSLayoutConstraint(item: circleView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: item.imageView, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0),
                NSLayoutConstraint(item: circleView, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: item.imageView, attribute: NSLayoutAttribute.left, multiplier: 1.0, constant: 0),
                NSLayoutConstraint(item: circleView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: item.imageView, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0),
                NSLayoutConstraint(item: circleView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: item.imageView, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: 0)
                ])
            
            let circleShadowView = UIImageView()
            circleShadowView.image = UIImage(named: "shadow")
            circleShadowView.translatesAutoresizingMaskIntoConstraints = false
            item.insertSubview(circleShadowView, at: 1)
            item.addConstraints([
                NSLayoutConstraint(item: circleShadowView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: circleView, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0),
                NSLayoutConstraint(item: circleShadowView, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: circleView, attribute: NSLayoutAttribute.left, multiplier: 1.0, constant: 0),
                NSLayoutConstraint(item: circleShadowView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: circleView, attribute: NSLayoutAttribute.height, multiplier: 364.0/612.0, constant: 0),
                NSLayoutConstraint(item: circleShadowView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: circleView, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: 0)
                ])
            
            let skipButton = UIButton()
            skipButton.translatesAutoresizingMaskIntoConstraints = false
            skipButton.addTarget(self, action: #selector(OnboardingViewController.skipOnboarding(_:)), for: .touchUpInside)
            skipButton.isHidden = true;
            let multipleAttributes = [
                NSForegroundColorAttributeName: UIColor.white,
                NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue,
                NSFontAttributeName: UIFont.systemFont(ofSize: 12)] as [String : Any]
            let attrString = NSAttributedString(string: "Пропустить", attributes: multipleAttributes)
            skipButton.setAttributedTitle(attrString, for: .normal)
            item.addSubview(skipButton)
            item.addConstraints([
                NSLayoutConstraint(item: skipButton, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant:44),
                NSLayoutConstraint(item: skipButton, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: item, attribute: NSLayoutAttribute.left, multiplier: 1.0, constant: 0),
                NSLayoutConstraint(item: skipButton, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: item, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: 0),
                NSLayoutConstraint(item: skipButton, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: item.descriptionLabel, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)
                ])
        }
    }
}

// MARK: PaperOnboardingDataSource

extension OnboardingViewController: PaperOnboardingDataSource {
    
    func onboardingItemAtIndex(_ index: Int) -> OnboardingItemInfo {
        let titleFont = UIFont(name: "HelveticaNeue", size: 16.0) ?? UIFont.boldSystemFont(ofSize: 16.0)
        let titleFont2 = UIFont(name: "HelveticaNeue", size: 22) ?? UIFont.boldSystemFont(ofSize: 22)
        let titleFont3 = UIFont(name: "HelveticaNeue-Thin", size: 22) ?? UIFont.boldSystemFont(ofSize: 22)
        let descriptionFont = UIFont(name: "HelveticaNeue", size: 16.0) ?? UIFont.systemFont(ofSize: 16.0)
        return [
            ("safe_pic", "", "Ваши данные надежно защищены", "keepass_filled", UIColor(red:255/255.0, green:128/255.0, blue:0, alpha:1.00), UIColor.white, UIColor.white, titleFont,descriptionFont),
            ("convert_pic", "", "Конвертируйте валюту на карте в одно касание", "shekel_filled", UIColor(red:18/255.0, green:128/255.0, blue:131/255.0, alpha:1.00), UIColor.white, UIColor.white, titleFont,descriptionFont),
            ("trans_pic", "", "Переводите деньги быстро и надёжно", "buy_upgrade_filled", UIColor(red:137/255.0, green:81/255.0, blue:176/255.0, alpha:1.00), UIColor.white, UIColor.white, titleFont,descriptionFont),
            ("pays_pic", "", "Оплачивайте более 100 видов услуг через мобильный банк", "US_dollar_fillled", UIColor(red:48/255.0, green:159/255.0, blue:126/255.0, alpha:1.00), UIColor.white, UIColor.white, titleFont,descriptionFont),
            ("accounts_pic", "", "Управляйте счетами с вашего мобильного устройства", "Wallet", UIColor(red:15/255.0, green:79/255.0, blue:116/255.0, alpha:1.00), UIColor.white, UIColor.white, titleFont,descriptionFont),
            ("logo_vert", "Добро пожаловать в ", "МОБИЛЬНЫЙ БАНК", "", UIColor(patternImage: UIImage(named: "bg_new")!), UIColor(red:3/255.0, green:70/255.0, blue:109/255.0, alpha:1.00), UIColor(red:3/255.0, green:70/255.0, blue:109/255.0, alpha:1.00), titleFont3,titleFont2)
            ][index]
    }
    
    func onboardingItemsCount() -> Int {
        return 6
    }
}
