//
//  DateIntervalPickerPopupViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 4/13/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateIntervalPickerPopupViewController : UIViewController

@property (nonatomic, strong) NSString *datePickerTitle;

@property (nonatomic, strong) NSDate *fromDate;
@property (nonatomic, strong) NSDate *toDate;

@property (nonatomic, copy) void (^completionBlock)(NSDate *fromDate, NSDate *toDate);


- (void)setFromDate:(NSDate *)fromDate andToDate:(NSDate *)toDate;

@end
