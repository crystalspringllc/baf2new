//
//  MenuViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 4/23/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <GPUImage/GPUImageGaussianBlurFilter.h>
#import "MenuViewController.h"
#import "MenuItem.h"
#import "NotificationCenterHelper.h"
#import "MenuTableCell.h"
#import "MenuProfileTableCell.h"
#import "User.h"
#import "IPORouter.h"
#import <CoreText/CoreText.h>

@interface MenuViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *userMenuItems;
@property (nonatomic, strong) NSArray *guestMenuItems;

@end

@implementation MenuViewController {}


- (id)init {
    self = [super init];
    if(self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Menu" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
        [self initVars];
    }
    return self;
}

- (void)initVars {
    self.userMenuItems = [MenuItem userMenuItems];
    self.guestMenuItems = [MenuItem guestMenuItems];
}

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didLoginNotification)
                                                 name:kNotificationUserLoggedIn
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gotoHistory)
                                                 name:kNotificationTransferDoneGotoHistory
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(openLoginVC)
                                                 name:kNotificationMenuOpenLogin
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(openBecomeClient)
                                                 name:kNotificationMenuOpenBecomeClient
                                               object:nil];


    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(openBankOffers)
                                                 name:kNotificationMenuOpenBankOffers
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateContent)
                                                 name:kNotificationUserUpdatedProfile
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateContent)
                                                 name:kNotificationLanguageDidChange
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(openOnlineDeposit)
                                                 name:kNotificationMenuOpenOnlineDeposit
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(openAtmPoints)
                                                 name:kNotificationOpenAtmPoints
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(openNewTransfer)
                                                 name:kNotificationOpenNewTransfers
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(openPayments)
                                                 name:kNotificationOpenPayments
                                               object:nil];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerNotifications];
    [self configUI];
    self.selectedIndexPath = [MenuItem rowIndexPathIn:self.menuItems byName:MENU_TITLE_LOGINREGISTER];
}

#pragma makr - config actions

- (void)openCorrespondingVC
{
    NSArray *items = [self menuItems];
    MenuItem *menuItem = items[(NSUInteger) self.selectedIndexPath.row];
    UINavigationController *nc = [UIHelper defaultNavigationController];
    UIViewController *selectedController = menuItem.correspondingVC;
    nc.viewControllers = @[selectedController];
    LGSideMenuController *sideMenuController = kSideMenuController;
    sideMenuController.rootViewController = nc;
    [sideMenuController hideRightViewAnimated:YES completionHandler:nil];
}

#pragma mark - notification actions

- (void)updateContent {
    [self initVars];
    [self.tableView reloadData];
}

- (void)didLoginNotification {
    self.selectedIndexPath = [MenuItem rowIndexPathIn:self.menuItems byName:MENU_TITLE_MAINVC];
    [self.tableView reloadData];
}

- (void)gotoHistory {
    self.selectedIndexPath = [MenuItem rowIndexPathIn:self.menuItems byName:MENU_TITLE_HISTORY];
    [self.tableView reloadData];
    [self openCorrespondingVC];
}

- (void)openLoginVC {
    self.selectedIndexPath = [MenuItem rowIndexPathIn:self.menuItems byName:MENU_TITLE_LOGINREGISTER];
    [self.tableView reloadData];
    [self openCorrespondingVC];
}

- (void)openBecomeClient {
    self.selectedIndexPath = [MenuItem rowIndexPathIn:self.menuItems byName:MENU_TITLE_BECOMECLIENT];
    [self.tableView reloadData];
    [self openCorrespondingVC];
}

- (void)openBankOffers {
    self.selectedIndexPath = [MenuItem rowIndexPathIn:self.menuItems byName:MENU_TITLE_BANKOFFERS];
    [self.tableView reloadData];
    [self openCorrespondingVC];
}

- (void)openOnlineDeposit {
    self.selectedIndexPath = [MenuItem rowIndexPathIn:self.menuItems byName:MENU_TITLE_DEPOSIT];
    [self.tableView reloadData];
    [self openCorrespondingVC];
}

- (void)openAtmPoints {
    self.selectedIndexPath = [MenuItem rowIndexPathIn:self.menuItems byName:MENU_TITLE_ATMANDDEP];
    [self.tableView reloadData];
    [self openCorrespondingVC];
}

- (void)openNewTransfer {
    self.selectedIndexPath = [MenuItem rowIndexPathIn:self.menuItems byName:MENU_TITLE_TRANSFERS];
    [self.tableView reloadData];
    [self openCorrespondingVC];
}

- (void)openPayments {
    self.selectedIndexPath = [MenuItem rowIndexPathIn:self.menuItems byName:MENU_TITLE_PAYMENTS];
    [self.tableView reloadData];
    [self openCorrespondingVC];
}

#pragma mark - tableview delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *items = [self menuItems];
    MenuItem *menuItem = items[(NSUInteger) indexPath.row];
    if ([menuItem.name isEqualToString:MENU_TITLE_PROFILE]) {
        return 100;
    }
    return 57;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self menuItems].count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *items = [self menuItems];
    MenuItem *menuItem = items[(NSUInteger) indexPath.row];
    MenuTableCell *menuTableCell = (MenuTableCell *)cell;

    if (self.selectedIndexPath.row == indexPath.row) {
        if (![menuItem.name isEqualToString:MENU_TITLE_PROFILE]) {
            menuTableCell.titleLabel.font = [UIFont boldHelveticaNeue:14];
            menuTableCell.innerView.backgroundColor = [UIColor fromRGB:0x303940];
        }
    } else {
        menuTableCell.titleLabel.font = [UIFont helveticaNeue:14];

        if ([menuItem.name isEqualToString:MENU_TITLE_PROFILE]) {
            menuTableCell.titleLabel.font = [UIFont boldHelveticaNeue:15];
        }

        if (menuTableCell.coloredBg) {
            menuTableCell.innerView.backgroundColor = [UIColor fromRGB:0x364047];
        } else {
            menuTableCell.innerView.backgroundColor = [UIColor clearColor];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSArray *items = [self menuItems];
    MenuItem *menuItem = items[(NSUInteger) indexPath.row];

    if ([menuItem.name isEqualToString:MENU_TITLE_PROFILE]) {

        static NSString *CellIdentifier = @"MenuProfileTableCell";
        MenuProfileTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

        if ([User sharedInstance].lastname && [User sharedInstance].firstname) {
            NSString *name = [NSString stringWithFormat:@"%@ %@", [User sharedInstance].lastname, [User sharedInstance].firstname];

            NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:name];
            [attString addAttribute:(NSString*)kCTUnderlineStyleAttributeName
                              value:[NSNumber numberWithInt:kCTUnderlineStyleSingle]
                              range:(NSRange){0,[attString length]}];
            cell.titleLabel.attributedText = attString;
            cell.titleLabel.textColor = [UIColor whiteColor];


            [cell.loader stopAnimating];
        } else {
            cell.titleLabel.text = @"";
            [cell.loader startAnimating];
        }

        cell.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
        cell.iconImageView.layer.cornerRadius = cell.iconImageView.width / 2;
        cell.iconImageView.layer.masksToBounds = true;

        if ([User sharedInstance].sessionID.length > 0) {
            if ([User sharedInstance].avatarImageUrlString) {
                [cell.iconImageView sd_setImageWithURL:[[NSURL alloc] initWithString:[User sharedInstance].avatarImageUrlString]
                                      placeholderImage:[UIImage imageNamed:@"icon-icon-placeholder"]
                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                 if (image) {
                                                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

                                                         GPUImageGaussianBlurFilter *gaussianBlur = [[GPUImageGaussianBlurFilter alloc] init];
                                                         gaussianBlur.blurRadiusInPixels = 5;
                                                         UIImage *blurredImage = [gaussianBlur imageByFilteringImage:image];

                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             cell.bgImageView.contentMode = UIViewContentModeScaleAspectFill;
                                                             cell.bgImageView.image = blurredImage;
                                                         });
                                                     });
                                                 }
                                             }];
            }

        }

        return cell;

    } else {

        static NSString *CellIdentifier = @"Cell";
        MenuTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];


        cell.iconImageView.image = [UIImage imageNamed:menuItem.icon];
        cell.titleLabel.text = menuItem.name;
        cell.iconImageView.layer.cornerRadius = 0;
        cell.iconImageView.contentMode = UIViewContentModeScaleAspectFit;
        cell.leftColorView.hidden = menuItem.color == nil;
        cell.leftColorView.backgroundColor = menuItem.color;
        cell.backgroundColor = menuItem.color ? [UIColor fromRGB:0x364047] : [UIColor clearColor];
        cell.innerView.backgroundColor = menuItem.color ? [UIColor fromRGB:0x364047] : [UIColor clearColor];
        cell.coloredBg = menuItem.color != nil;
        return cell;

    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSIndexPath *selIndexPath = [MenuItem rowIndexPathIn:self.menuItems byName:MENU_TITLE_EXIT];

    if ([User sharedInstance].sessionID.length > 0 && [indexPath compare:selIndexPath] == NSOrderedSame) {
        [[UIHelper appDelegate] logoutUser];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kIsShownPerSession];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }

    NSIndexPath *epinIndexPath = [MenuItem rowIndexPathIn:self.menuItems byName:MENU_TITLE_EPIN];
    if ([indexPath compare:epinIndexPath] == NSOrderedSame) {
        NSURL *url = [NSURL URLWithString:@"https://www.bankastana.kz/getpin/"];
        if (![[UIApplication sharedApplication] openURL:url]) {
            NSLog(@"%@%@",@"Failed to open url:",[url description]);
        }
        return;
    }
    
    NSIndexPath *easyTransferIndexPath = [MenuItem rowIndexPathIn:self.menuItems byName:MENU_TITLE_EASYTRANSFER];
    if ([indexPath compare:easyTransferIndexPath] == NSOrderedSame) {
        NSURL *url = [NSURL URLWithString:@"https://www.easytransfer.kz/"];
        if (![[UIApplication sharedApplication] openURL:url]) {
            NSLog(@"%@%@",@"Failed to open url:",[url description]);
        }
        return;
    }


    self.selectedIndexPath = indexPath;
    [tableView reloadData];

    NSArray *items = [self menuItems];
    MenuItem *menuItem = items[(NSUInteger) indexPath.row];

    if (menuItem && menuItem.name) {
        [GAN sendEventWithCategory:@"Меню" action:menuItem.name];
    }

    if ([menuItem.name isEqualToString:MENU_TITLE_PRIVILEGE]) {
        NSURL *url = [NSURL URLWithString:kBankPriilegeUrl];
        if (![[UIApplication sharedApplication] openURL:url]) {
            NSLog(@"%@%@",@"Failed to open url:",[url description]);
        }
        return;
    }


    UINavigationController *nc = [UIHelper defaultNavigationController];
    UIViewController *selectedController = menuItem.correspondingVC;
    nc.viewControllers = @[selectedController];
    LGSideMenuController *sideMenuController = kSideMenuController;
    sideMenuController.rootViewController = nc;
    [sideMenuController hideRightViewAnimated:YES completionHandler:nil];
}

#pragma mark - config ui

- (void)configUI {
    self.tableView.tableFooterView = [UIView new];
    UIImage *patternImage = [UIImage imageNamed:@"left_menu_bg"];
    UIImage *img = [patternImage resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeTile];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:img];
    self.tableView.backgroundColor = [UIColor clearColor];
}

#pragma mark - helpers

- (NSArray *)menuItems {
    if ([User sharedInstance].sessionID.length > 0) {
        return self.userMenuItems;
    } else {
        return self.guestMenuItems;
    }
}

- (void)selectLoginVC {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    [self.tableView selectRowAtIndexPath:indexPath
                                animated:YES
                          scrollPosition:UITableViewScrollPositionNone];
    [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
}

#pragma mark - dealloc

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
