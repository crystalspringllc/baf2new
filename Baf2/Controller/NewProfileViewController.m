//
// Created by Askar Mustafin on 6/20/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "NewProfileViewController.h"
#import "UserProfileHeaderView.h"
#import "User.h"
#import "MXParallaxHeader.h"
#import "UIViewController+Extension.h"
#import "UITableViewController+Extension.h"
#import "JVFloatLabeledTextField.h"
#import "MainButton.h"
#import "ProfileApiOLD.h"
#import "MBProgressHUD+AstanaView.h"
#import "OldPasswordPopupViewController.h"
#import "NotificationCenterHelper.h"
#import "PasswordExpireViewController.h"
#import "ProfileApi.h"
#import "ConfirmPasswordViewController.h"
#import "STPopupController+Extensions.h"
#import "ChangePhoneEmailViewController.h"
#import "AuthApi.h"


@interface NewProfileViewController()
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *lastnameTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *nameTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *middlenameTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *nicknameTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *iinTextField;
@property (weak, nonatomic) IBOutlet MainButton *changePasswordButton;
@property (weak, nonatomic) IBOutlet MainButton *changeButton;
////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *emailTextField;
@property (weak, nonatomic) IBOutlet MainButton *changeEmailButton;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet MainButton *changePhoneNumberButton;
@end

@implementation NewProfileViewController {}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self fillProfile];
}

#pragma mark - requests

/**
 * If user not confirmed email or phone we
 * then show him window for do that
 */
- (void)checkUserOnSmsEmailConfirmation {
    __weak NewProfileViewController *weakSelf = self;

    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"checking", nil) animated:true];


    [ProfileApiOLD getClientInfoWithSuccess:^(id response) {
        NSDictionary *userData = response[@"user"];

        [[User sharedInstance] setUserProfileData:userData];

        if (![User sharedInstance].isEmailConfirmed || ![User sharedInstance].isPhoneConfirmed) {
            [UIHelper showEmailPhoneConfirmationWindow:YES target:^{
                [weakSelf.tableView reloadData];
            }];
        } else {
            self.changeEmailButton.hidden = NO;
            self.changePhoneNumberButton.hidden = NO;
            self.changePasswordButton.hidden = NO;
            self.changeButton.hidden = NO;
        }
        [MBProgressHUD hideAstanaHUDAnimated:true];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserUpdatedProfile object:nil];
    } failure:^(NSInteger code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserUpdatedProfile object:nil];
    }];
}

#pragma mark - config actions

- (IBAction)clickChangePasswordButton:(id)sender {
    PasswordExpireViewController *v = [[PasswordExpireViewController alloc] init];
    v.disclamerText = @"Смена пароля";
    v.username = self.nameTextField.text;
    v.showCloseButton = YES;
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:v];
    [self.navigationController presentViewController:nc animated:YES completion:nil];
}

- (IBAction)clickChangeButton:(id)sender {
    __weak NewProfileViewController *wSelf = self;

    if (self.nicknameTextField.text.length == 0) {
        [Toast showToast:@"Введите никнейм"];
        return;
    }

    ConfirmPasswordViewController *v = [ConfirmPasswordViewController createVC];
    v.onCorfirmTapped = ^(NSString *password) {

        [MBProgressHUD showAstanaHUDWithTitle:@"Изменение профиля" animated:YES];

        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];

        params[@"nickname"] = self.nicknameTextField.text ? : @"";
        params[@"lastName"] = self.lastnameTextField.text ? : @"";
        params[@"firstName"] = self.nameTextField.text ? : @"";
        params[@"middleName"] = self.middlenameTextField.text ? : @"";
        params[@"email"] = self.emailTextField.text ? : @"";
        params[@"phone"] = self.phoneNumberTextField.text ? : @"";
        params[@"currentPass"] = password;


        [ProfileApi updateProfileWithParams:params password:password success:^(id response) {
            [Toast showToast:@"Профиль успешно изменен"];
            [MBProgressHUD hideAstanaHUDAnimated:YES];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
        }];

    };
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:v];
    popupController.dismissOnBackgroundTap = YES;
    popupController.navigationBarHidden = true;
    popupController.style = STPopupStyleFormSheet;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [popupController presentInViewController:self];
}


/**
 * Change EMAIL
 * @param sender - uibutton
 */
- (IBAction)clickChangeEmailButton:(id)sender {
    ChangePhoneEmailViewController *vc = [ChangePhoneEmailViewController createVC];
    vc.isPhone = NO;
    STPopupController *p = [[STPopupController alloc] initWithRootViewController:vc];
    p.dismissOnBackgroundTap = YES;
    p.style = STPopupStyleFormSheet;
    p.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [p presentInViewController:self];
}

/**
 * Change PHONE
 * @param sender - uibutton
 */
- (IBAction)clickChangePhoneButton:(id)sender {
    ChangePhoneEmailViewController *vc = [ChangePhoneEmailViewController createVC];
    vc.isPhone = YES;
    STPopupController *p = [[STPopupController alloc] initWithRootViewController:vc];
    p.dismissOnBackgroundTap = YES;
    p.style = STPopupStyleFormSheet;
    p.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [p presentInViewController:self];
}

#pragma mark - uitableview

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 1
            && indexPath.row == 2
            && ([User sharedInstance].isPhoneConfirmed
            || [User sharedInstance].isEmailConfirmed))
    {
        return 0;
    }
    
    if (indexPath.section == 0
            && (indexPath.row == 5
            || indexPath.row == 6)
            && (![User sharedInstance].isPhoneConfirmed
            || ![User sharedInstance].isEmailConfirmed))
    {
        return 0;
    }
    
    return UITableViewAutomaticDimension;
}

#pragma mark - config ui

- (void)fillProfile {

    self.lastnameTextField.text = [User sharedInstance].lastname ? : @"";
    self.nameTextField.text = [User sharedInstance].firstname ? : @"";
    self.middlenameTextField.text = [User sharedInstance].middlename ? : @"";
    self.nicknameTextField.text = [User sharedInstance].nickname ? : @"";
    self.iinTextField.text = [User sharedInstance].iin ? : @"";
    self.emailTextField.text = [User sharedInstance].email ? : @"";
    self.phoneNumberTextField.text = [User sharedInstance].phoneNumber ? : @"";

    self.lastnameTextField.enabled = NO;
    self.nameTextField.enabled = NO;
    self.middlenameTextField.enabled = NO;
    self.nicknameTextField.enabled = YES;
    self.iinTextField.enabled = NO;
    self.emailTextField.enabled = NO;
    self.phoneNumberTextField.enabled = NO;

    User *tempUser = [User sharedInstance];
    NSLog(tempUser.firstname);
    
    if (![User sharedInstance].isEmailConfirmed || ![User sharedInstance].isPhoneConfirmed)
    {
        self.changeEmailButton.hidden = YES;
        self.changePhoneNumberButton.hidden = YES;
        self.changePasswordButton.hidden = YES;
        self.changeButton.hidden = YES;

        __weak NewProfileViewController *wSelf = self;
        [UIHelper showEmailPhoneConfirmationWindow:YES target:^{
            [wSelf.tableView reloadData];
            [wSelf checkUserOnSmsEmailConfirmation];
        }];
    }
}

- (void)configUI {
    __weak NewProfileViewController *wSelf = self;

    [self showMenuButton];
    [self setNavigationTitle:NSLocalizedString(@"profile", nil)];
    [self setBAFTableViewBackground];

    //avatar
    UserProfileHeaderView *userProfileHeaderView = [NSBundle.mainBundle loadNibNamed:@"UserProfileHeaderView" owner:self options:nil].firstObject;
    userProfileHeaderView.parentNavigationController = self.navigationController;
    if ([User sharedInstance].avatarImageUrlString) {
        [userProfileHeaderView setAvatarImageFromUrlString:[User sharedInstance].avatarImageUrlString];
    }
    userProfileHeaderView.didCropImage = ^(UIImage *croppedImage) {
        //wSelf.avatar = croppedImage;
    };
    self.tableView.parallaxHeader.view = userProfileHeaderView;
    self.tableView.parallaxHeader.height = 200;
    self.tableView.parallaxHeader.mode = MXParallaxHeaderModeFill;
    self.tableView.parallaxHeader.minimumHeight = 0;
    self.tableView.estimatedRowHeight = 60;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    //buttons
    self.changePasswordButton.mainButtonStyle = MainButtonStyleOrange;
    self.changeButton.mainButtonStyle = MainButtonStyleOrange;
    self.changeEmailButton.mainButtonStyle = MainButtonStyleOrange;
    self.changePhoneNumberButton.mainButtonStyle = MainButtonStyleOrange;

    self.nameTextField.textColor = [UIColor lightGrayColor];
    self.lastnameTextField.textColor = [UIColor lightGrayColor];
    self.middlenameTextField.textColor = [UIColor lightGrayColor];
    self.iinTextField.textColor = [UIColor lightGrayColor];


    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadProfile) name:kNotificationUserUpdatedProfile object:nil];
}

- (void)reloadProfile {
    [MBProgressHUD showAstanaHUDWithTitle:@"Обновление профиля" animated:YES];
    [AuthApi getClientBySession:[User sharedInstance].sessionID success:^(id response2) {
        [[User sharedInstance] setUserProfileData:response2];
        [self fillProfile];
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
