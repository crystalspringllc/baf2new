//
//  CategoryProvidersListViewController.m
//  Myth
//
//  Created by Almas Adilbek on 8/27/12.
//
//

#import "CategoryProvidersListViewController.h"
#import "ProviderTableCell.h"
#import "ContractsAPIOLD.h"
#import "NoResultMessageView.h"
#import "PaymentFormViewController.h"

@interface CategoryProvidersListViewController ()

@end

@implementation CategoryProvidersListViewController

@synthesize paymentProviders, showMenuButton;

-(id)init {
    self = [super init];
    if(self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];

    if(paymentProviders.count == 0) {
        NoResultMessageView *nov = [[NoResultMessageView alloc] init];
        [nov setText:NSLocalizedString(@"no_providers", nil)];
        [self.view addSubview:nov];
    }
}

#pragma mark -
#pragma mark UITableViewDelegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return paymentProviders.count;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(paymentProviders.count == 0) return @"";
    return NSLocalizedString(@"choose_service", nil);
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PaymentProvider *provider = paymentProviders[(NSUInteger) indexPath.row];
    return [ProviderTableCell height:provider.name];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    ProviderTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ProviderTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

    PaymentProvider *provider = paymentProviders[(NSUInteger) indexPath.row];
    [cell setIcon:provider.logo];
    [cell setTitle:provider.name];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PaymentProvider *provider = paymentProviders[(NSUInteger) indexPath.row];

    
    PaymentFormViewController *vc = [PaymentFormViewController createVC];
    vc.actionType = ActionTypeNewPayment;
    vc.paymentProvider = provider;
    vc.providerCategoryId = self.providerCategoryId;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -

- (void)configUI {
    [self setBafBackground];
    
    [self setNavigationTitle:self.title];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITableView *list = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeightWithoutStatusBarAndNavigationBar]) style:UITableViewStyleGrouped];
    list.backgroundView = nil;
    list.backgroundColor = [UIColor clearColor];
    list.delegate = self;
    list.dataSource = self;
    [self.view addSubview:list];

    if(!showMenuButton) {
        [self setNavigationBackButton];
    } else {
        [self showMenuButton];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
