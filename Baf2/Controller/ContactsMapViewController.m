//
//  ContactsMapViewController.m
//  BAF
//
//  Created by Almas Adilbek on 5/18/15.
//
//

#import "ContactsMapViewController.h"
#import "ContactsSQLiteManager.h"
#import "OCAnnotation.h"
#import "OCMapViewSampleHelpAnnotation.h"
#import "OCMapView.h"

@interface ContactsMapViewController () <MKMapViewDelegate>
@property(nonatomic, strong) CLLocationManager *locationManager;
@end

@implementation ContactsMapViewController {
    ObjectsMapView *objectsMapView;
    OCMapView *mv;
}

-(id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

-(void)initVars {}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
        
    if (self.annotationToShow) {
        MKUserTrackingBarButtonItem *buttonItem = [[MKUserTrackingBarButtonItem alloc] initWithMapView:mv];
        self.navigationItem.leftBarButtonItem = buttonItem;
    } else {
        MKUserTrackingBarButtonItem *buttonItem = [[MKUserTrackingBarButtonItem alloc] initWithMapView:objectsMapView.mv];
        self.navigationItem.leftBarButtonItem = buttonItem;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark -
#pragma mark CLLocationManager

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", locations);
}

#pragma mark -

-(void)configUI
{
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNavigationTitle:NSLocalizedString(@"card", nil)];
    [self setNavigationCloseButtonAtLeft:NO];

    if (self.annotationToShow) {
        mv = [[OCMapView alloc] initWithFrame:self.view.bounds];
        mv.minimumAnnotationCountPerCluster = 20;
        mv.delegate = self;
        mv.translatesAutoresizingMaskIntoConstraints = NO;
        mv.clusteringEnabled = YES;
        mv.showsUserLocation = YES;
        [self.view addSubview:mv];
        
        [mv autoPinEdgeToSuperviewEdge:ALEdgeTop];
        [mv autoPinEdgeToSuperviewEdge:ALEdgeLeft];
        [mv autoPinEdgeToSuperviewEdge:ALEdgeRight];
        [mv autoSetDimension:ALDimensionHeight toSize:[ASize screenHeightWithoutStatusBarAndNavigationBar]];
        
        [self showAnnotationToShow];
    } else {
        objectsMapView = [[ObjectsMapView alloc] initWithHeight:[ASize screenHeightWithoutStatusBarAndNavigationBar]];
        objectsMapView.parentVC = self;
        objectsMapView.delegate = self;
        objectsMapView.fullscreen = YES;
        [self.view addSubview:objectsMapView];
        [objectsMapView autoPinEdgeToSuperviewEdge:ALEdgeTop];
        [objectsMapView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    }
}

- (void)showAnnotationToShow {
    [mv addAnnotation:self.annotationToShow];
    
    MKCoordinateRegion mapRegion;
    mapRegion.center = self.annotationToShow.coordinate;
    mapRegion.span.latitudeDelta = 0.009;
    mapRegion.span.longitudeDelta = 0.019;
    
    [mv setRegion:mapRegion animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc {

}

#pragma mark -
#pragma mark MKMapView

- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *annotationView;
    
    // if it's a cluster
    if ([annotation isKindOfClass:[OCAnnotation class]])
    {
        OCAnnotation *clusterAnnotation = (OCAnnotation *)annotation;
        
        annotationView = [aMapView dequeueReusableAnnotationViewWithIdentifier:@"ClusterView"];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"ClusterView"];
            annotationView.canShowCallout = YES;
            //            annotationView.centerOffset = CGPointMake(0, -20);
        }
        
        // set its image
        UILabel *numberLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        numberLabel.backgroundColor = [UIColor clearColor];
        numberLabel.font = [UIFont boldSystemFontOfSize:12];
        numberLabel.textColor = [UIColor whiteColor];
        numberLabel.text = [NSString stringWithFormat:@"%zd", [clusterAnnotation.annotationsInCluster count]];
        [numberLabel sizeToFit];
        
        CGFloat clusterSize = MAX(numberLabel.width + 12, 30);
        UIView *clusterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, clusterSize , clusterSize)];
        clusterView.layer.borderColor = [UIColor whiteColor].CGColor;
        clusterView.layer.borderWidth = 2;
        clusterView.layer.cornerRadius = (CGFloat) (clusterSize * 0.5);
        clusterView.clipsToBounds = YES;
        clusterView.opaque = NO;
        clusterView.backgroundColor = [UIColor darkGreen];
        
        numberLabel.center = clusterView.middlePoint;
        [clusterView addSubview:numberLabel];
        
        annotationView.image = [UIImage imageWithView:clusterView];
    }
    // If it's a single annotation
    else if([annotation isKindOfClass:[OCMapViewSampleHelpAnnotation class]])
    {
        OCMapViewSampleHelpAnnotation *singleAnnotation = (OCMapViewSampleHelpAnnotation *)annotation;
        annotationView = [aMapView dequeueReusableAnnotationViewWithIdentifier:@"singleAnnotationView"];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:singleAnnotation reuseIdentifier:@"singleAnnotationView"];
            annotationView.canShowCallout = YES;
            annotationView.centerOffset = CGPointMake(0, -22);
        }
        if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms) {
            annotationView.image = [UIImage imageNamed:@"btn_pin_normal"];
        } else {
            annotationView.image = [UIImage imageNamed:@"pin-baf.png"];
        }
    }
    else if (annotation == mv.userLocation)
    {
        return nil;
    }
    // Error
    else{
        annotationView = (MKPinAnnotationView *)[aMapView dequeueReusableAnnotationViewWithIdentifier:@"errorAnnotationView"];
        if (!annotationView) {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"errorAnnotationView"];
            annotationView.canShowCallout = NO;
            ((MKPinAnnotationView *)annotationView).pinColor = MKPinAnnotationColorRed;
        }
    }
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if([view.annotation isKindOfClass:[OCMapViewSampleHelpAnnotation class]]) {
        if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms) {
            view.image = [UIImage imageNamed:@"btn_pin_pressed"];
        }
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if([view.annotation isKindOfClass:[OCMapViewSampleHelpAnnotation class]]) {
        if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms) {
            view.image = [UIImage imageNamed:@"btn_pin_normal"];
        }
    }
}

- (void)mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)animated {
    [mv doClustering];
}

#pragma mark - Helpers

- (void)deselectAllOtherAnnotations:(MKAnnotationView *)view {
    NSArray *selectedAnnotations = mv.selectedAnnotations;
    for (id<MKAnnotation> annotation in selectedAnnotations) {
        if ([annotation isKindOfClass:[OCMapViewSampleHelpAnnotation class]]) {
            if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms) {
                if (annotation != view.annotation) {
                    [mv deselectAnnotation:annotation animated:YES];
                }
            }
        }
    }
}

@end