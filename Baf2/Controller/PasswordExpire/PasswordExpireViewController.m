//
//  PasswordExpireViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 15.02.17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "PasswordExpireViewController.h"
#import "AuthApi.h"
#import "PasswordExpireView.h"

@interface PasswordExpireViewController ()
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) PasswordExpireView *passwordExpireView;
@end

@implementation PasswordExpireViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
    
    if (!self.forceUpdate) {
        [self showDisclaimer:[NSString stringWithFormat:@"Срок действия Вашего пароля истек.\nКоличество оставшихся входов: %ld.", (long)self.graces]];
    } else {
        [self showDisclaimer:@"Срок действия Вашего пароля истек."];
    }

    if (self.disclamerText) {
        [self showDisclaimer:self.disclamerText];
    }

    [self showError:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self.passwordExpireView.oldPasswordTextField becomeFirstResponder];
}

#pragma mark - Actions

- (void)updatePasswordTapped {
    [self.view endEditing:true];
    
    if (self.forceUpdate) {
        [self getClientByUsername];
        return;
    }
    
    [self updatePassword];
}
    
#pragma mark - Methods
    
- (void)getClientByUsername {
    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
    [AuthApi clientByUsername:self.username
                      success:^(id response) {
                          [self updatePasswordUsingUsername:response[@"sid"]];
                      } failure:^(NSString *code, NSString *message) {
                          [MBProgressHUD hideAstanaHUDAnimated:true];
                          [self showError:message];
                      }];
}

- (void)updatePassword {
    [self.view endEditing:true];

    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
    [AuthApi updatePassword:self.passwordExpireView.oldPasswordText
                newPassword:self.passwordExpireView.updatedPasswordText
          retypeNewPassword:self.passwordExpireView.repeatedUpdatedPasswordText
                    success:^(id response) {
                        [MBProgressHUD hideAstanaHUDAnimated:true];

                        [User sharedInstance].graces = 1000;
                        [User sharedInstance].isPasswordExpired = false;

                        [self showError:nil];
                        [self.navigationController popViewControllerAnimated:true];

                        if (self.didUpdatePassword) {
                            self.didUpdatePassword();
                        }

                        if (self.showCloseButton) {
                            [self dismissViewControllerAnimated:YES completion:^{
                                [Toast showToast:@"Пароль успешно сменен"];
                            }];
                        }

                    } failure:^(NSString *code, NSString *message) {
                        [MBProgressHUD hideAstanaHUDAnimated:true];
                        [self showError:message];
                    }];
}

- (void)updatePasswordUsingUsername:(NSString *)clientSid {
    [self.view endEditing:true];

    [AuthApi updatePasswordBySid:clientSid
                     oldPassword:self.passwordExpireView.oldPasswordText
                     newPassword:self.passwordExpireView.updatedPasswordText
               retypeNewPassword:self.passwordExpireView.repeatedUpdatedPasswordText
                         success:^(id response) {
                             [MBProgressHUD hideAstanaHUDAnimated:true];

                             [User sharedInstance].graces = 1000;
                             [User sharedInstance].isPasswordExpired = false;

                             [self showError:nil];
                             [self.navigationController popViewControllerAnimated:true];

                             if (self.didUpdatePassword) {
                                 self.didUpdatePassword();
                             }
                         } failure:^(NSString *code, NSString *message) {
                             [MBProgressHUD hideAstanaHUDAnimated:true];
                             [self showError:message];
                         }];
}

- (void)showDisclaimer:(NSString *)message {
    self.passwordExpireView.disclaimerText = message;
}

- (void)showError:(NSString *)message {
    self.passwordExpireView.errorText = message;
}

#pragma mark - Config UI

- (void)configUI {
    [self setBafBackground];
    [self setNavigationBackButton];

    if (self.showCloseButton) {
        [self setNavigationCloseButtonAtLeft:YES];
    }

    [self configNavigationBar];
    [self configureScrollView];
    [self configureContainerView];
    [self configPasswordExpireView];
}

- (void)configNavigationBar {
    [self setNavigationTitle:@"Обновление пароля"];
}

- (void)configureScrollView {
    self.scrollView = [UIScrollView newAutoLayoutView];
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.bounces = true;
    self.scrollView.alwaysBounceVertical = true;
    [self.view addSubview:self.scrollView];
    [self.scrollView autoPinEdgesToSuperviewEdges];
}

- (void)configureContainerView {
    self.containerView = [UIView newAutoLayoutView];
    self.containerView.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:self.containerView];
    [self.containerView autoPinEdgesToSuperviewEdges];
    [self.containerView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.scrollView];
}

- (void)configPasswordExpireView {
    self.passwordExpireView = [PasswordExpireView new];
    __weak PasswordExpireViewController *weakSelf = self;
    self.passwordExpireView.didTapSubmit = ^{
        [weakSelf updatePasswordTapped];
    };
    [self.containerView addSubview:self.passwordExpireView];
    [self.passwordExpireView autoPinEdgesToSuperviewEdges];
}

@end
