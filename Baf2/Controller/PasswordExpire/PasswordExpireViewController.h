//
//  PasswordExpireViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 15.02.17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PasswordExpireViewController : UIViewController

@property (nonatomic, copy) NSString *username;
@property (nonatomic) NSInteger graces;
@property (nonatomic) NSInteger expireDays;
@property (nonatomic) BOOL forceUpdate;
@property (nonatomic, strong) NSString *disclamerText;
@property (nonatomic, assign) BOOL showCloseButton;

@property (nonatomic, copy) void (^didUpdatePassword)();

@end