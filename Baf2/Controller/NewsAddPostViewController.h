//
//  NewsAddPostViewController.h
//  Baf2
//
//  Created by nmaksut on 18.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsAddPostViewController : UITableViewController

+ (id)createVC;

@end
