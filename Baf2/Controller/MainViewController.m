//
// Created by Askar Mustafin on 4/8/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "MainViewController.h"
#import "CardsViewController.h"
#import "PaymentsQuickViewController.h"
#import "BankOffersViewController.h"
#import "TransfersQuickViewController.h"
#import "RateAppDependencies.h"
#import "IPODependencies.h"
#import "OnlineDepositPopupViewController.h"
#import "STPopupController+Extensions.h"
#import "Constants.h"
#import "NotificationCenterHelper.h"
#import "TransferViewController.h"


@interface MainViewController() {}
@property (nonatomic, strong) NSArray *controllers;
@end


@implementation MainViewController {}

- (id)init {
    self = [super init];
    if (self) {}
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tabImageNames = @[@"icon_cards_normal1", @"icon_payments_normal1", @"icon_transfers_normal1", @"icon_offers_normal1"];
    self.tabLabelNames = @[NSLocalizedString(@"cards", nil), NSLocalizedString(@"pay", nil), NSLocalizedString(@"transfer", nil), NSLocalizedString(@"offers", nil)];

    CardsViewController *v1 = [CardsViewController createVC];
    PaymentsQuickViewController *v2 = [PaymentsQuickViewController createVC];
//    TransfersQuickViewController *v3 = [TransfersQuickViewController createVC];
    TransferViewController *v3 = [TransferViewController createVC];
    BankOffersViewController *v4 = [BankOffersViewController createVC];

    self.controllers = @[v1, v2, v3, v4];

    [self setDataSource:self];
    self.delegate = self;
    [self reloadData];
    [self selectTabbarIndex:0];

    [self configUI];

    //Rate app
    RateAppDependencies *rateAppDependencies = [[RateAppDependencies alloc] init];
    rateAppDependencies.parentVC = self;
    [rateAppDependencies startRouter];

    IPODependencies *ipo = [[IPODependencies alloc] init];
    ipo.parentVC = self;
    [ipo startRouter];
    
    //OnlineDeposit PopUp
    [self openOnlineDepositPopup];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar setShadowImage:nil];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - data source

- (NSInteger)numberOfViewControllers {
    return self.controllers.count;
}

- (UIViewController *)viewControllerForIndex:(NSInteger)index {
    return self.controllers[(NSUInteger) index];
}

#pragma mark - methods

- (void)openOnlineDepositPopup {
    BOOL hide = [[NSUserDefaults standardUserDefaults] boolForKey:kHideOnlineDepositeInfo];
    if(!hide) {
        [self setupOnlineDepositPopup];
    }
}

-(void)setupOnlineDepositPopup {
    __weak MainViewController *wSelf = self;
    
    OnlineDepositPopupViewController *depositSheetVC = [OnlineDepositPopupViewController new];
    depositSheetVC.onInterestingButtonClick = ^() {
        [wSelf onlineDepositeInterestingTapped];
    };
    STPopupController *vc = [[STPopupController alloc] initWithRootViewController:depositSheetVC];
    vc.style = STPopupStyleFormSheet;
    vc.dismissOnBackgroundTap = false;
    vc.navigationBarHidden = true;
    [vc presentInViewController:self];
}

- (void)onlineDepositeInterestingTapped {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMenuOpenOnlineDeposit object:nil];
}

#pragma mark - config ui

- (void)configUI {
    [self hideBackButton];
    [self showBafLogo];
    [self showMenuButton];
    [self showPhoneButton];
}


#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"MainVC dealloc");
}

@end
