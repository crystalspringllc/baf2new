//
//  SettingsTableViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 28.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <ChameleonFramework/ChameleonMacros.h>
#import "SettingsTableViewController.h"
#import "AMTouchIdAuth.h"
#import "PinHelper.h"
#import "PinApi.h"
#import "UITableViewController+Extension.h"
#import "UIActionSheet+Blocks.h"
#import "LocalizationManager.h"
#import "AuthApi.h"
#import "NotificationCenterHelper.h"

@interface SettingsTableViewController ()
@property (nonatomic, strong) UISwitch *usePinSwitch;
@property (nonatomic, strong) UISwitch *useTouchIDSwitch;
@property (weak, nonatomic) IBOutlet UILabel *enterByPinLabel;
@property (weak, nonatomic) IBOutlet UILabel *userTouchIdButton;
@property (weak, nonatomic) IBOutlet UILabel *languageLabel;
@property(nonatomic, strong) UILabel *version;
@end

@implementation SettingsTableViewController

+ (id)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SettingsTableViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
    //[self setInitialLang];
}

#pragma mark - Actions


- (void)changeLanguage:(NSString *)languageCode {
    [MBProgressHUD showAstanaHUDWithTitle:@"" animated:YES];
    [AuthApi changeLangSuccess:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [[LocalizationManager sharedManager] changeCurrentLanguageTo:languageCode];
        [self setInitialLang];
        [NSNotificationCenter.defaultCenter postNotificationName:kNotificationLanguageDidChange object:nil];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

- (void)setInitialLang {
    if ([[LocalizationManager sharedManager].currentLanguage.code isEqualToString:@"ru"]) {
        self.languageLabel.text = @"Русский";
    } else if ([[LocalizationManager sharedManager].currentLanguage.code isEqualToString:@"en"]) {
        self.languageLabel.text = @"English";
    }

    [self setNavigationTitle:NSLocalizedString(@"settings", nil)];
    self.enterByPinLabel.text = NSLocalizedString(@"pin_login", nil);
    self.userTouchIdButton.text = NSLocalizedString(@"user_touch_id", nil);
    self.version.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"version", nil), [AppUtils appVersion]];
    [self.tableView reloadData];
}

- (void)switchUsePin:(UISwitch *)sw {
    __weak SettingsTableViewController *wSelf = self;

    if (sw.isOn) {
        PinAuthView *pinAuthView = nil;
        pinAuthView = [[PinAuthView alloc] initWithPinType:PinAuthViewPinTypeNew];
        [pinAuthView setCancelButtonTitle:NSLocalizedString(@"cancellation", nil)];
        pinAuthView.pinAuthViewWillDismissAfterCancel = ^(PinAuthView *pinAuthView) {
            [wSelf pinAuthViewWillDismissAfterCancel:pinAuthView];
        };
        pinAuthView.pinAuthViewPinEntered = ^(PinAuthView *pinAuthView) {
            [wSelf pinAuthViewPinEntered:pinAuthView];
        };
        [pinAuthView show];
        [pinAuthView hideRateBannerView];


    } else {
        [MBProgressHUD showAstanaHUDWithTitle:@"" animated:YES];
        [PinApi deletePin:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
            [wSelf.tableView reloadData];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];
        [PinHelper deletePinToken];
    }
    [self.tableView reloadData];
}

- (void)switchUseTouchID:(UISwitch *)sw {
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    
    if (![context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
          switch (error.code) {
              case LAErrorPasscodeNotSet: {
                  NSLog(@"Authentication could not start, because passcode is not set on the device.");
                  [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"block_password_not_turrnedon", nil)];

                  break;
              }
              case LAErrorTouchIDNotAvailable: {
                  NSLog(@"Authentication could not start, because Touch ID is not available on the device.");
                  [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"touchid_not_available", nil)];
                  
                  break;
              }
              case LAErrorTouchIDNotEnrolled: {
                  NSLog(@"Authentication could not start, because Touch ID has no enrolled fingers.");
                  [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"not_fingerprints", nil)];

                  break;
              }
              default:
                  NSLog(@"Touch ID configuration");
                  [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"touchid_not_available", nil)];
                  break;
          }
    } else {
        NSUserDefaults *sud = [NSUserDefaults standardUserDefaults];
        if (sw.isOn) {
            [sud setBool:true forKey:kUserUseTouchID];
        } else {
            [sud setBool:false forKey:kUserUseTouchID];
        }
    }
    [self.tableView reloadData];
}


#pragma mark - PinAuthView

/**
 * Cancel pin
 */
- (void)pinAuthViewWillDismissAfterCancel:(PinAuthView *)pinAuthView {
    [self.tableView reloadData];
}

/**
 *  Authorization with pin
 */
- (void)pinAuthViewPinEntered:(PinAuthView *)pinAuthView {
    __weak SettingsTableViewController *wSelf = self;
    
    // Set new pin
    [MBProgressHUD showAstanaHUDWithTitle:@"" animated:YES];
    [PinApi setPin:pinAuthView.pinCode login:[User sharedInstance].phoneNumber success:^(id response) {
        // Save pin token
        [PinHelper savePinToken:response withPin:pinAuthView.pinCode];
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        
        // Hide pin view
        [pinAuthView hide];
        
        [wSelf.tableView reloadData];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [pinAuthView reset];
    }];
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
        } else if (indexPath.row == 1) {
            // hide touch id switch if no pin set
            if (![PinHelper pinToken]) {
                return 0;
            }
            
            // hide touch id switch if not available
            LAContext *context = [[LAContext alloc] init];
            NSError *error = nil;
            if (![context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
                switch (error.code) {
                    case LAErrorTouchIDNotAvailable: {
                        return 0;
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
    
    return UITableViewAutomaticDimension;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return NSLocalizedString(@"security", nil);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [self.usePinSwitch setOn:([PinHelper pinToken] ? true : false)];
            cell.accessoryView = self.usePinSwitch;
        } else if (indexPath.row == 1) {
            [self.useTouchIDSwitch setOn:[[NSUserDefaults standardUserDefaults] boolForKey:kUserUseTouchID]];
            cell.accessoryView = self.useTouchIDSwitch;
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak SettingsTableViewController *wSelf = self;

    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    if (indexPath.row == 2) {
        [UIActionSheet showInView:self.view
                        withTitle:NSLocalizedString(@"choose_language", nil)
                cancelButtonTitle:NSLocalizedString(@"cancel", nil)
           destructiveButtonTitle:nil
                otherButtonTitles:@[@"Русский", @"English"]
                         tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
                            if (actionSheet.cancelButtonIndex != buttonIndex) {
                                
                                if (buttonIndex == 0) {

                                    [wSelf changeLanguage:@"ru"];

                                } else if (buttonIndex == 1) {

                                    [wSelf changeLanguage:@"en"];

                                }
                            }
                         }];
    }
}

#pragma mark - Config UI

- (void)configUI {
    [self setBAFTableViewBackground];
    [self showMenuButton];
    [self setNavigationTitle:NSLocalizedString(@"settings", nil)];
    self.enterByPinLabel.text = NSLocalizedString(@"pin_login", nil);
    self.userTouchIdButton.text = NSLocalizedString(@"user_touch_id", nil);
    
    self.usePinSwitch = [UISwitch new];
    self.usePinSwitch.onTintColor = [UIColor flatSkyBlueColorDark];
    [self.usePinSwitch addTarget:self action:@selector(switchUsePin:) forControlEvents:UIControlEventValueChanged];
    
    self.useTouchIDSwitch = [UISwitch new];
    self.useTouchIDSwitch.onTintColor = [UIColor flatSkyBlueColorDark];
    [self.useTouchIDSwitch addTarget:self action:@selector(switchUseTouchID:) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 50;

    self.version = [[UILabel alloc] initWithFrame:CGRectMake(0,0,[ASize screenWidth], 70)];
    self.version.textAlignment = NSTextAlignmentCenter;
    self.version.font = [UIFont boldSystemFontOfSize:10];
    self.version.textColor = [UIColor flatGrayColorDark];
    self.version.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"version", nil), [AppUtils appVersion]];
    self.tableView.tableFooterView = self.version;
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    NSLog(@"Settings dealloc");
}

@end
