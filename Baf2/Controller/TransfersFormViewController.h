//
//  TransfersFormViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 31.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewTransfersApi.h"

@class Account, Deposit, OperationAccounts;
@class Card;

@interface TransfersFormViewController : UITableViewController

@property (nonatomic) TransferType transfersType;
@property (nonatomic, copy) OperationAccounts *operationAccounts;

// for currency convert
@property (nonatomic, weak) Account *currencyConvertAccount;
@property (nonatomic, weak) Deposit *depositToRefill;
@property (nonatomic, weak) TransferTemplate *transferTemplate;

@property (nonatomic, strong) Card *cardToRefill;

+ (instancetype)createVC;

@end