//
//  TransfersTemplatesViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 15.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "TransfersTemplatesViewController.h"
#import "NewTransfersApi.h"
#import "Account.h"
#import "TransferTemplate.h"

@interface TransfersTemplatesViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation TransfersTemplatesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.transferTemplates count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    TransferTemplate *tt = self.transferTemplates[indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)", tt.alias, [NewTransfersApi transferTypeToDescriptionString:[NewTransfersApi transferTypeForString:tt.type]]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (%@)", tt.requestorAccount.alias, tt.requestorAccount.currency];;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

#pragma mark - Config UI

- (void)configUI {
    [self setNavigationBackButton];
    self.title = NSLocalizedString(@"translation_templates", nil);
    
    self.tableView.tableFooterView = [UIView new];
}

@end
