//
//  ExternalCardRegistrationSuccessPopupViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 6/9/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ExternalCardRegistrationSuccessPopupViewController.h"
#import "MainButton.h"
#import "ExtCard.h"
#import "SaveCardResponse.h"
#import <STPopup/STPopup.h>

@interface ExternalCardRegistrationSuccessPopupViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *maskLabel;
@property (weak, nonatomic) IBOutlet UILabel *instructionLabel;
@property (weak, nonatomic) IBOutlet MainButton *activateNowButton;
@property (weak, nonatomic) IBOutlet MainButton *activateLaterButton;
@end

@implementation ExternalCardRegistrationSuccessPopupViewController

- (id)init {
    self = [super init];
    if (self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ExternalCardRegisterSuccessPopup" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ExternalCardRegistrationSuccessPopupViewController class])];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth] - 20, 450);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - config action

- (IBAction)clickActivateNowButton:(id)sender {
    __weak ExternalCardRegistrationSuccessPopupViewController *wSelf = self;

    [self dismissViewControllerAnimated:YES completion:^{
        if (wSelf.onActivateNow) {
            self.onActivateNow();
        }
    }];
}

- (IBAction)clickActivateLatelButton:(id)sender {
    __weak ExternalCardRegistrationSuccessPopupViewController *wSelf = self;

    [self dismissViewControllerAnimated:YES completion:^{
        if (wSelf.onActivateLabel) {
            wSelf.onActivateLabel();
        }
    }];
}

#pragma mark - config ui

- (void)configUI {
    self.activateLaterButton.mainButtonStyle = MainButtonStyleOrange;
    self.activateLaterButton.title = NSLocalizedString(@"enter_activation_code_later", nil);
    
    self.activateNowButton.mainButtonStyle = MainButtonStyleOrange;
    self.activateNowButton.title = NSLocalizedString(@"enter_activation_code_now", nil);

    self.orderNumberLabel.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"order_number_i", nil), self.saveCardResponse.orderId];
    self.maskLabel.text = self.saveCardResponse.extCard.mask;
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
