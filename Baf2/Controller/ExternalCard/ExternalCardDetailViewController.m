//
// Created by Askar Mustafin on 6/15/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <ChameleonFramework/ChameleonMacros.h>
#import "ExternalCardDetailViewController.h"
#import "ExtCard.h"
#import "ExternalCardDetailMainInfoCell.h"
#import "UIKit+AFNetworking.h"
#import "UIView+ConcisePureLayout.h"
#import "MainButton.h"
#import "FloatingTextField.h"
#import "ExternalCardRegistrationApi.h"
#import "MBProgressHUD.h"
#import "MBProgressHUD+AstanaView.h"
#import "EpayRegCardLinkParam.h"
#import "ExternalCardRegistrationBrowserViewController.h"
#import "NotificationCenterHelper.h"
#import "UITableViewController+Extension.h"

@interface ExternalCardDetailViewController() {}

@property (weak, nonatomic) IBOutlet ExternalCardDetailMainInfoCell *mainInfoCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *bankInfoCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *mainCardCell;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activateButtonTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *getNewActivationCodeButtonTopConstrain;

@property (weak, nonatomic) IBOutlet MainButton *activateButton;
@property (weak, nonatomic) IBOutlet MainButton *getNewActivationCodeButton;
@property (weak, nonatomic) IBOutlet MainButton *removeCardButton;
@property (weak, nonatomic) IBOutlet MainButton *saveCardButton;

@property (weak, nonatomic) IBOutlet FloatingTextField *aliasTextField;
@property (weak, nonatomic) IBOutlet UISwitch *mainCardSwitch;

@end

@implementation ExternalCardDetailViewController

- (id)init {
    self = [super init];
    if(self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ExternalCardDetails" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ExternalCardDetailViewController class])];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - config actions

- (IBAction)clickActivateButton:(id)sender {
    [self activateCardWithCardId:self.externalCard.accountId];
}

- (IBAction)clickGetNewActivationCodeButton:(id)sender {
    __weak ExternalCardDetailViewController *wSelf = self;

    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
    
    [ExternalCardRegistrationApi getEpayRegCardLinkParamsWithCardType:self.externalCard.cardType
                                                                 bank:self.externalCard.bank
                                                               cardId:self.externalCard.accountId
                                                              success:^(id response) {
                                                                  [MBProgressHUD hideAstanaHUDAnimated:true];
                                                                  
                                                                  EpayRegCardLinkParam *linkParam = [EpayRegCardLinkParam instanceFromDictionary:response];
                                                                  ExternalCardRegistrationBrowserViewController *v = [[ExternalCardRegistrationBrowserViewController alloc] initWithType:ExternalCardRegistrationBrowserTypeRegister];
                                                                  v.linkParam = linkParam;
                                                                  v.cardForActivation = self.externalCard;
                                                                  UINavigationController *nc = [UIHelper defaultNavigationController];
                                                                  [nc setViewControllers:@[v]];                                                                      [self.navigationController presentViewController:nc animated:YES completion:nil];
                                                                  v.onGotoCardActivation = ^(ExtCard *createdCard) {
                                                                      [wSelf activateCardWithCardId:createdCard.accountId];
                                                                  };
                                                                  
                                                              } failure:^(NSString *code, NSString *message) {
                                                                  [MBProgressHUD hideAstanaHUDAnimated:true];
                                                              }];
}

- (IBAction)clickSaveCardButton:(id)sender {
    [self.aliasTextField resignFirstResponder];
    
    __weak ExternalCardDetailViewController *weakSelf = self;
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"saving", nil) animated:YES];
    [ExternalCardRegistrationApi updateEpayCardInfoWithCardAlias:self.aliasTextField.text processing:kProcessingBankKkb mainCard:self.mainCardSwitch.isOn cardId:self.externalCard.accountId success:^(id response) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"information_about_card_successfully_blocked", nil)];
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        
        weakSelf.externalCard.alias = weakSelf.aliasTextField.text;
        weakSelf.externalCard.isMain = weakSelf.mainCardSwitch.isOn;
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCardInfoChanged object:nil];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

- (IBAction)clickRemoveCardButton:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"delete_key", nil) preferredStyle:UIAlertControllerStyleAlert];
    alertController.popoverPresentationController.sourceView = sender;
    alertController.popoverPresentationController.sourceRect = ((MainButton *)sender).frame;
    
    __weak ExternalCardDetailViewController *weakSelf = self;
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *_Nonnull action) {
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"deletion_key", nil) animated:YES];
        [ExternalCardRegistrationApi deleteCardWithCardId:weakSelf.externalCard.accountId success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            [UIHelper showAlertWithMessage:NSLocalizedString(@"card_successfully_deleted", nil)];

            [weakSelf goBack];

            if (weakSelf.onDeleteCardSuccess) {
                weakSelf.onDeleteCardSuccess();
            }
        }                                         failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
        }];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancellation", nil) style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:alertController animated:true completion:nil];
}


- (void)activateCardWithCardId:(NSNumber *)cardId {
    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
    [ExternalCardRegistrationApi getEpayRegCardLinkParamsWithCardType:self.externalCard.cardType
                                                                 bank:self.externalCard.bank
                                                               cardId:cardId
                                                              success:^(id response) {
                                                                  [MBProgressHUD hideAstanaHUDAnimated:true];
                                                                  
                                                                  EpayRegCardLinkParam *linkParam = [EpayRegCardLinkParam instanceFromDictionary:response];
                                                                  ExternalCardRegistrationBrowserViewController *v = [[ExternalCardRegistrationBrowserViewController alloc] initWithType:ExternalCardRegistrationBrowserTypeActivate];
                                                                  UINavigationController *nc = [UIHelper defaultNavigationController];
                                                                  [nc setViewControllers:@[v]];                                                                                                                                        v.linkParam = linkParam;
                                                                  v.cardForActivation = self.externalCard;
                                                                  [self.navigationController presentViewController:nc animated:YES completion:nil];

                                                              } failure:^(NSString *code, NSString *message) {
                                                                  [MBProgressHUD hideAstanaHUDAnimated:true];
                                                              }];
}

#pragma mark - tableview delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 1 || indexPath.row == 2) {
            return 50;
        }
    }
    
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
//    if (indexPath.section == 0) {
//        if (indexPath.row == 0) {
//            cell.textLabel.text = self.externalCard.alias;
//        } else if (indexPath.row == 1) {
//            cell.textLabel.font = [UIFont systemFontOfSize:12];
//            cell.textLabel.text = [self.externalCard getMaskOrEmpty];
//        }
//    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

#pragma mark - config ui

- (void)configUI {
    [self setNavigationTitle:self.externalCard.alias];
    [self setNavigationBackButton];
    [self setBAFTableViewBackground];

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 50;

    [self configFooterButtons];
    [self fillCells];

    self.mainCardSwitch.onTintColor = [UIColor flatSkyBlueColorDark];
}

- (void)configFooterButtons {
    self.activateButton.mainButtonStyle = MainButtonStyleOrange;
    self.getNewActivationCodeButton.mainButtonStyle = MainButtonStyleOrange;
    self.removeCardButton.mainButtonStyle = MainButtonStyleRed;
    self.saveCardButton.mainButtonStyle = MainButtonStyleWhite;

    if ([self.externalCard.status isEqualToString:@"epay_activation_time_expired"] ||
            [self.externalCard.status isEqualToString:@"epay_activation_failed"]) {
        [self.activateButton aa_setHeight:0];
        self.activateButtonTopConstrain.constant = 0;
        [self.getNewActivationCodeButton aa_setHeight:45];
        self.getNewActivationCodeButtonTopConstrain.constant = 15;
    } else if ([self.externalCard.status isEqualToString:@"epay_card_activation_valid"]) {
        [self.activateButton aa_setHeight:45];
        self.activateButtonTopConstrain.constant = 15;
        [self.getNewActivationCodeButton aa_setHeight:0];
        self.getNewActivationCodeButtonTopConstrain.constant = 0;
    } else {
        [self.activateButton aa_setHeight:0];
        self.activateButtonTopConstrain.constant = 0;
        [self.getNewActivationCodeButton aa_setHeight:0];
        self.getNewActivationCodeButtonTopConstrain.constant = 0;
    }
}

- (void)fillCells {
    //main detail cell
    [self.mainInfoCell.logo setImageWithURL:[[NSURL alloc] initWithString:self.externalCard.logo] placeholderImage:[UIImage imageNamed:@"icon-icon-placeholder"]];
    self.mainInfoCell.title.text = self.externalCard.alias;
    self.mainInfoCell.subTitle.text = [self.externalCard getMaskOrEmpty];

    if ([self.externalCard getDateStatusText].length > 0) {
        self.mainInfoCell.details.text = [self.externalCard getDateStatusText];
    } else {
        [self.mainInfoCell.details aa_setHeight:0];
        self.mainInfoCell.detailsVerticalConstraint.constant = 0;
    }
    
    if (self.externalCard.expMonth && self.externalCard.expMonth.length > 0 && self.externalCard.expYear && self.externalCard.expYear.length > 0) {
        self.mainInfoCell.expirationDateLabel.text = [NSString stringWithFormat:@"%@/%@", self.externalCard.expMonth, self.externalCard.expYear];
    } else {
        self.mainInfoCell.expirationDateLabel.text = @"";
    }
    
    self.aliasTextField.text = self.externalCard.alias;

    //bank info
    [self.bankInfoCell.imageView setImageWithURL:[[NSURL alloc] initWithString:self.externalCard.logo] placeholderImage:[UIImage imageNamed:@"icon-icon-placeholder"]];
    self.bankInfoCell.textLabel.text = self.externalCard.descriptionText;
}

#pragma mark - setter

- (void)setExternalCard:(ExtCard *)externalCard {
    _externalCard = externalCard;
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {}

@end