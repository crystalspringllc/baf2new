//
// Created by Askar Mustafin on 6/15/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ExtCard;


@interface ExternalCardDetailViewController : UITableViewController

@property (nonatomic, weak) ExtCard *externalCard;

@property (nonatomic, copy) void (^onDeleteCardSuccess)();

@end