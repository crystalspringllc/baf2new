//
//  ExternalCardRegistrationSuccessPopupViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 6/9/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SaveCardResponse;

@interface ExternalCardRegistrationSuccessPopupViewController : UIViewController

@property (nonatomic, strong) SaveCardResponse *saveCardResponse;

@property (nonatomic, copy) void (^onActivateNow)();
@property (nonatomic, copy) void (^onActivateLabel)();

@end
