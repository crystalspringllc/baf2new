//
//  ExternalCardRegistrationBrowserViewController.h
//  Myth
//
//  Created by Almas Adilbek on 12/26/13.
//
//

#import <UIKit/UIKit.h>

@class EpayRegCardLinkParam;
@class ExtCard;

typedef NS_ENUM(NSInteger, ExternalCardRegistrationBrowserType) {
    ExternalCardRegistrationBrowserTypeRegister,
    ExternalCardRegistrationBrowserTypeActivate
};

@interface ExternalCardRegistrationBrowserViewController : UIViewController <UIWebViewDelegate> {}


- (id)initWithType:(ExternalCardRegistrationBrowserType)type;


@property (nonatomic, strong) EpayRegCardLinkParam *linkParam;
@property (nonatomic, assign) ExternalCardRegistrationBrowserType type;

@property (nonatomic, strong) ExtCard *cardForActivation;


//after register
@property (nonatomic, copy) void (^onGotoCardActivation)(ExtCard *createdCard);
@property (nonatomic, copy) void (^onCloseTapped)();

//after activation
@property (nonatomic, copy) void (^onCardActivationSuccess)();
@property (nonatomic, copy) void (^onCardActivationFailure)();



@end