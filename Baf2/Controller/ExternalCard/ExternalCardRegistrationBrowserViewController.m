//
//  ExternalCardRegistrationBrowserViewController.m
//  Myth
//
//  Created by Almas Adilbek on 12/26/13.
//
//

#import "ExternalCardRegistrationBrowserViewController.h"
#import "EpayRegCardLinkParam.h"
#import "MBProgressHUD.h"
#import "MBProgressHUD+AstanaView.h"
#import "ExternalCardRegistrationApi.h"
#import "SaveCardResponse.h"
#import "ExternalCardRegistrationSuccessPopupViewController.h"
#import "STPopup.h"
#import "ChameleonMacros.h"
#import "ExtCard.h"
#import <UIView+Toast.h>

@interface ExternalCardRegistrationBrowserViewController() {}
@property (nonatomic, strong) UIWebView *webview;
@property (nonatomic, strong) SaveCardResponse *saveCardResponse;
@end

@implementation ExternalCardRegistrationBrowserViewController {
    BOOL webViewInitLoaded;
}

- (id)initWithType:(ExternalCardRegistrationBrowserType)type {
    self = [super init];
    if (self) {
        self.type = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

    NSString *html;

    if (self.type == ExternalCardRegistrationBrowserTypeRegister) {

        html = @"<html><head><title>KKB Add Card</title></head><body style='background-color:#e5e4e1;'>";
        NSString *form = [NSString stringWithFormat:@"<form id='epay' name='SendOrder' method='post' action='%@'>", self.linkParam.url];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='Signed_Order_B64' value='%@'/>", self.linkParam.base64Content]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='BackLink' value='%@'/>", self.linkParam.backLink]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='PostLink' value='%@'/>", self.linkParam.postlink]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='FailurePostLink' value='%@'/>", self.linkParam.failBackLink]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='FailureBackLink' value='%@'/>", self.linkParam.failPostLink]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='template' value='%@'/>", self.linkParam.template]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='appendix' value='%@'/>", self.linkParam.appendix]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='Language' value='%@'/>", self.linkParam.language]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='email' value='%@'/>", @""]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"</form>"]];
        html = [html stringByAppendingString:form];
        html = [html stringByAppendingString:@"<script type='text/javascript'> window.onload(document.getElementById('epay').submit()); </script>"];
        html = [html stringByAppendingString:@"</body></html>"];

    } else if (self.type == ExternalCardRegistrationBrowserTypeActivate) {

        html = @"<html><head><title>KKB Add Card</title></head><body style='background-color:#e5e4e1;'>";
        NSString *form = [NSString stringWithFormat:@"<form id='epayForm' name='SendOrder' method='post' action='%@'>", self.linkParam.approveLink];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='backlink' value='%@'/>", self.linkParam.backLink]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='failurebacklink' value='%@'/>", self.linkParam.failBackLink]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='reference' value='%@'/>", self.cardForActivation.reference]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='template' value='%@'/>", self.linkParam.template]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='lang' value='%@'/>", self.linkParam.language]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"</form>"]];
        html = [html stringByAppendingString:form];
        html = [html stringByAppendingString:@"<script type='text/javascript'> window.onload(document.getElementById('epayForm').submit()); </script>"];
        html = [html stringByAppendingString:@"</body></html>"];

    }

    [self.webview loadHTMLString:html baseURL:[NSURL URLWithString:self.linkParam.url]];

    webViewInitLoaded = NO;
}

#pragma mark - WebView delegates

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [MBProgressHUD showAstanaHUDWithTitle:@"Загрузка" animated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [MBProgressHUD hideAstanaHUDAnimated:YES];
    if(!webViewInitLoaded) {
        webViewInitLoaded = YES;
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    [MBProgressHUD hideAstanaHUDAnimated:YES];

    NSString *urlString = [[request URL] absoluteString];

    if (webViewInitLoaded) {

        if (self.type == ExternalCardRegistrationBrowserTypeRegister) {

            if ([urlString rangeOfString:self.linkParam.backLink].location != NSNotFound) {
                [self showCardAddedSuccess];
                return NO;
            } else if([urlString rangeOfString:self.linkParam.failBackLink].location != NSNotFound) {
                [Toast showToast:@"Ошибка при регистрации карты"];

                [self close:nil];
                return NO;
            } else if([urlString rangeOfString:@"err_process"].location != NSNotFound) {
                [GAN sendEventWithCategory:kGanCategoryCards action:@"Не успешная регистрация (данные карты не верны)"];
                
                [Toast showToast:@"Не успешная регистрация (данные карты не верны)"];
                
                [self close:nil];
            }

        } else if (self.type == ExternalCardRegistrationBrowserTypeActivate) {
            if (self.linkParam) {
                if ([urlString rangeOfString:[self.linkParam.backLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]].location != NSNotFound) {
                    [self onActivationSuccess];
                    return NO;
                } else if ([urlString rangeOfString:[self.linkParam.failBackLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]].location != NSNotFound) {
                    [self onActivationFailure];
                    return NO;
                }
            }
        }
    }

    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [UIHelper showAlertTryAgain];
    [self performBlock:^{
        [self popViewController];
    } afterDelay:0.75];
}

#pragma mark - card aproved

- (void)onActivationSuccess {
    if (self.onCardActivationSuccess) {
        self.onCardActivationSuccess();
    }
    [self close:nil];
}

- (void)onActivationFailure {
    if (self.onCardActivationFailure) {
        self.onCardActivationFailure();
    }
    [self close:nil];
}

#pragma mark - add card success

- (void)showCardAddedSuccess {
    __weak ExternalCardRegistrationBrowserViewController *wSelf = self;

    [self.webview removeFromSuperview];

    [MBProgressHUD showAstanaHUDWithTitle:@"Пожалуйста ждите" animated:YES];
    [ExternalCardRegistrationApi saveCardRegWithUuid:self.linkParam.uuid success:^(id response) {

        self.saveCardResponse = [SaveCardResponse instanceFromDictionary:response];
        ExternalCardRegistrationSuccessPopupViewController *v = [[ExternalCardRegistrationSuccessPopupViewController alloc] init];
        v.saveCardResponse = self.saveCardResponse;
        STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:v];
        popupController.navigationBarHidden = YES;
        popupController.style = STPopupStyleFormSheet;
        popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
        [popupController presentInViewController:self];

        v.onActivateNow = ^{

            [MBProgressHUD showAstanaHUDWithTitle:@"Пожалуйста ждите" animated:YES];
            [ExternalCardRegistrationApi registerCardByUuid:self.saveCardResponse.uuid success:^(id response1) {


                self.saveCardResponse.extCard.reference = response1[@"reference"];
                [wSelf gotoCardActivationTappedWithReference];


                [MBProgressHUD hideAstanaHUDAnimated:YES];
            } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
            }];

        };

        v.onActivateLabel = ^{
            [wSelf activateCardLaterTapped];
        };

        [MBProgressHUD hideAstanaHUDAnimated:YES];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

- (void)close:(void (^)())completionBlock {
    [self dismissViewControllerAnimated:YES completion:^{
        if (completionBlock) {
            completionBlock();
        }
    }];
}

- (void)gotoCardActivationTappedWithReference {
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.onGotoCardActivation) {
            self.onGotoCardActivation(self.saveCardResponse.extCard);
        }
    }];
}

- (void)activateCardLaterTapped {
    [self close:nil];
}

#pragma mark - config ui

- (void)configUI {
    [self setNavigationTitle:@"Регистрация карты"];
    [self setNavigationCloseButtonAtLeft:NO];
    self.view.backgroundColor = [UIColor whiteColor];

    ///
    self.webview = [[UIWebView alloc] initWithFrame:CGRectMake(0,0, [ASize screenWidth], [ASize screenHeight])];
    self.webview.backgroundColor = [UIColor whiteColor];
    self.webview.delegate = self;
    self.webview.scalesPageToFit = NO;
    self.webview.scrollView.showsVerticalScrollIndicator = NO;
    self.webview.scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.webview];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {}

@end
