//
//  ExternalCardRegistrationViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 6/8/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ExternalCardRegistrationViewController.h"
#import "ActionSheetListPopupViewController.h"
#import "ExternalCardRegistrationApi.h"
#import "CardType.h"
#import "Bank.h"
#import "MainButton.h"
#import "FloatingTextField.h"
#import "EpayRegCardLinkParam.h"
#import "ExternalCardRegistrationBrowserViewController.h"
#import "ChameleonMacros.h"
#import "ExtCard.h"
#import "STPopupController+Extensions.h"
#import "UITableViewController+Extension.h"

typedef enum RowType:NSInteger {
    RowTypeCards,
    RowTypeBanks
} RowType;

@interface ExternalCardRegistrationViewController ()

//UI
@property (weak, nonatomic) IBOutlet MainButton *addCardButton;
@property (weak, nonatomic) IBOutlet FloatingTextField *cardNameTextField;

//Vars
@property (nonatomic, strong) NSMutableArray *cardTypes;
@property (nonatomic, strong) NSMutableArray *banks;
@property (nonatomic, strong) CardType *selectedCardType;
@property (nonatomic, strong) Bank *selectedBank;
@end

@implementation ExternalCardRegistrationViewController

- (id)init {
    self = [super init];
    if(self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CardRegistration" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ExternalCardRegistrationViewController class])];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
    // Do any additional setup after loading the view.
    [self loadCardTypes];
}

#pragma mark - loadings

- (void)loadCardTypes {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"data_locading", nil) animated:YES];
    [ExternalCardRegistrationApi cardTypesWithSuccess:^(id response) {

        for (NSDictionary *cardTypeDict in response[@"cardTypeList"]) {
            CardType *cardType = [CardType instanceFromDictionary:cardTypeDict];
            if (!self.cardTypes) {
                self.cardTypes = [[NSMutableArray alloc] init];
            }
            [self.cardTypes addObject:cardType];
        }

        [self loadBanks];


    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

- (void)loadBanks {
    [ExternalCardRegistrationApi banksWithSuccess:^(id response) {

        for (NSDictionary *bankDict in response[@"bankList"]) {
            Bank *bank = [Bank instanceFromDictionary:bankDict];
            if (!self.banks) {
                self.banks = [[NSMutableArray alloc] init];
            }
            [self.banks addObject:bank];
        }

        [self.tableView reloadData];

        [MBProgressHUD hideAstanaHUDAnimated:YES];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

#pragma mark - config actions

- (IBAction)clickAddCardButton:(id)sender {
    if ([self validateForm]) {
        [self registerExtcardCard];
    }
}

- (BOOL)validateForm {
    if (self.selectedBank && self.selectedCardType && self.cardNameTextField.text.length > 0) {
        return YES;
    } else {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"fill_the_fields", nil)];
        return NO;
    }
}

- (void)registerExtcardCard {
    [MBProgressHUD showAstanaHUDWithTitle:nil animated:YES];
    [ExternalCardRegistrationApi registerExtCardWithCardType:self.selectedCardType.name
                                                bank:self.selectedBank.name
                                          processing:kProcessingBankKkb
                                            cardName:self.cardNameTextField.text
                                             success:^(id response) {

                                                 if ([response isKindOfClass:[NSNumber class]]) {
                                                     [self getEpayRegLinkWithCardId:response];
                                                 } else {
                                                     [UIHelper showAlertWithMessage:NSLocalizedString(@"not_able_not_register_card", nil)];
                                                 }

                                             } failure:^(NSString *code, NSString *message) {
                                                 [MBProgressHUD hideAstanaHUDAnimated:true];
                                             }];
}

- (void)getEpayRegLinkWithCardId:(NSNumber *)cardId {
    __weak ExternalCardRegistrationViewController *wSelf = self;

    [ExternalCardRegistrationApi getEpayRegCardLinkParamsWithCardType:self.selectedCardType.name
                                                         bank:self.selectedBank.name
                                                       cardId:cardId
                                                      success:^(id response) {
                                                          [MBProgressHUD hideAstanaHUDAnimated:true];
                                                          
                                                          EpayRegCardLinkParam *linkParam = [EpayRegCardLinkParam instanceFromDictionary:response];
                                                          ExternalCardRegistrationBrowserViewController *v = [[ExternalCardRegistrationBrowserViewController alloc] initWithType:ExternalCardRegistrationBrowserTypeRegister];
                                                          v.linkParam = linkParam;
                                                          UINavigationController *navigationController = [UIHelper defaultNavigationController];
                                                          [navigationController setViewControllers:@[v]];
                                                          [self.navigationController presentViewController:navigationController animated:YES completion:nil];
                                                          v.onGotoCardActivation = ^(ExtCard *createdCard) {
                                                              [wSelf activateCardWithCard:createdCard];
                                                          };
                                                          v.onCardActivationFailure = ^() {
                                                              
                                                          };

                                                      } failure:^(NSString *code, NSString *message) {
                                                          [MBProgressHUD hideAstanaHUDAnimated:true];
            }];
}

- (void)activateCardWithCard:(ExtCard *)extCard {
    [ExternalCardRegistrationApi getEpayRegCardLinkParamsWithCardType:self.selectedCardType.name
                                                         bank:self.selectedBank.name
                                                       cardId:extCard.accountId
                                                      success:^(id response) {

                                                          EpayRegCardLinkParam *linkParam = [EpayRegCardLinkParam instanceFromDictionary:response];
                                                          ExternalCardRegistrationBrowserViewController *v = [[ExternalCardRegistrationBrowserViewController alloc] initWithType:ExternalCardRegistrationBrowserTypeActivate];
                                                          UINavigationController *navigationController = [UIHelper defaultNavigationController];
                                                          [navigationController setViewControllers:@[v]];
                                                          v.linkParam = linkParam;
                                                          v.cardForActivation = extCard;
                                                          v.onCardActivationSuccess = ^{
                                                              [self updateEpayCardApproveStateWithCard:extCard];
                                                          };
                                                          v.onCardActivationFailure = ^{
                                                              [self actionOnCardRegFailureWithCard:extCard];
                                                          };
                                                          [self.navigationController presentViewController:navigationController animated:YES completion:nil];

                                                      } failure:^(NSString *code, NSString *message) {}];
}

- (void)updateEpayCardApproveStateWithCard:(ExtCard *)extCard {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"ending_registration", nil) animated:YES];
    [ExternalCardRegistrationApi updateEpayCardApproveStateWithCardId:extCard.accountId approve:YES success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [UIHelper showAlertWithMessage:NSLocalizedString(@"card_activation_successfully_ended", nil)];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

- (void)actionOnCardRegFailureWithCard:(ExtCard *)extCard {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"ending_registration", nil) animated:YES];
    [ExternalCardRegistrationApi actionOnCardRegFailureWithCardId:extCard.accountId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [UIHelper showAlertWithMessage:NSLocalizedString(@"not_able_to_activate_card", nil)];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

#pragma mark - tableview delegates

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    if (indexPath.section == 0) {
        
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"expand_arrow"]];

        if (indexPath.row == 0) {


            if (self.selectedCardType) {
                CardType *displayCardType = self.selectedCardType;
                cell.textLabel.text = displayCardType.descr;
                cell.textLabel.textColor = [UIColor flatBlackColor];
            } else {
                cell.textLabel.textColor = [UIColor flatGrayColor];
                cell.textLabel.text = NSLocalizedString(@"choose_card_type", nil);
            }

        }

    } else if (indexPath.section == 1) {
        
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"expand_arrow"]];

        if (indexPath.row == 0) {

            if (self.selectedBank) {
                Bank *displayBank = self.selectedBank;
                cell.textLabel.text = displayBank.descr;
                cell.textLabel.textColor = [UIColor flatBlackColor];
            } else {
                cell.textLabel.textColor = [UIColor flatGrayColor];
                cell.textLabel.text = NSLocalizedString(@"choose_bank", nil);

            }
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0) {

        [self showActionSheetWithList:self.cardTypes rowType:RowTypeCards];

    } else if (indexPath.section == 1) {

        [self showActionSheetWithList:self.banks rowType:RowTypeBanks];

    } else {

    }
}

#pragma mark - helpers

- (void)showActionSheetWithList:(NSArray *)list rowType:(RowType)rowType {
    __weak ExternalCardRegistrationViewController *wSelf = self;


    NSMutableArray *rawObjects = [[NSMutableArray alloc] init];
    NSIndexPath *selectedIndexPath = nil;

    int i = 0;
    for (id object in list) {
        ActionSheetListRowObject *rowObject = [[ActionSheetListRowObject alloc] init];

        if (rowType == RowTypeCards) {

            CardType *cardType = object;
            rowObject.keepObject = cardType;
            rowObject.title = cardType.descr;

            if (self.selectedCardType == object) {
                selectedIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
            }
        } else if (rowType == RowTypeBanks) {

            Bank *cardType = object;
            rowObject.keepObject = cardType;
            rowObject.title = cardType.descr;
            
            if (self.selectedBank == object) {
                selectedIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
            }
        }

        [rawObjects addObject:rowObject];
        i++;
    }

    ActionSheetListPopupViewController *v = [[ActionSheetListPopupViewController alloc] init];
    v.selectedIndexPath = selectedIndexPath;
    v.numberOfRowsInSection = ^NSInteger(NSInteger section) {
        return rawObjects.count;
    };
    v.rowForIndexPath = ^ActionSheetListRowObject*(NSIndexPath *indexPath) {
        ActionSheetListRowObject *rowObject = rawObjects[(NSUInteger) indexPath.row];
        
        return rowObject;
    };
    v.onSelectObject = ^(id selectedObject, NSIndexPath *indexPath) {
        if (rowType == RowTypeCards) {
            wSelf.selectedCardType = selectedObject;
        } else if (rowType == RowTypeBanks) {
            wSelf.selectedBank = selectedObject;
        }
        [wSelf.tableView reloadData];
    };
    
    if (rowType == RowTypeCards) {
        v.title = NSLocalizedString(@"choose_card_type", nil);
    } else if (rowType == RowTypeBanks) {
        v.title = NSLocalizedString(@"choose_bank", nil);
    }
    
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:v];
    popupController.dismissOnBackgroundTap = true;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    popupController.style = STPopupStyleBottomSheet;
    [popupController presentInViewController:self];
}

#pragma mark - config ui

- (void)configUI {
    [self setBAFTableViewBackground];
    self.addCardButton.title = NSLocalizedString(@"add_cardd", nil);
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"card_registration", nil)];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
