//
//  ViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 3/28/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
// icon_touchID

//#import "ViewController.h"
#import "MainViewController.h"
#import "RegistrationViewController.h"
#import "ForgotPasswordViewController.h"
#import "PasswordExpireViewController.h"

//helpers
#import "NSUserDefaults+sharedDefaults.h"
#import "UIView+ConcisePureLayout.h"
#import "PinHelper.h"
#import <WatchConnectivity/WatchConnectivity.h>
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "ChameleonMacros.h"
#import "STPopupController+Extensions.h"

//networking
#import "PinApi.h"
#import "AuthApi.h"
#import "AccountsApi.h"
#import "AMTouchIdAuth.h"
#import "NotificationCenterHelper.h"

//views
#import "FloatingTextField.h"
#import "MainButton.h"
#import "UnderlinedTitleButton.h"
#import "STPopupNavigationBar.h"
#import "ExchangeRatesApi.h"
#import "ExchangeRate.h"
#import "RatesBannerView.h"
#import "TickersApi.h"
#import "TickerBannerView.h"
#import "TransfersFormViewController.h"


@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *topTitle;
@property (weak, nonatomic) IBOutlet UnderlinedTitleButton *registerButton;
@property (weak, nonatomic) IBOutlet UnderlinedTitleButton *fogetPasswordButton;

@property (weak, nonatomic) IBOutlet UIView *loginContainer;
@property (weak, nonatomic) IBOutlet UIView *saveLoginContainer;
@property (weak, nonatomic) IBOutlet UIView *passwordContainer;
@property (weak, nonatomic) IBOutlet UIView *loginWithPinContainer;
@property (weak, nonatomic) IBOutlet UIView *enterButtonContainer;

@property (nonatomic, strong) FloatingTextField *loginTextField;
@property (nonatomic, strong) FloatingTextField *passwordTextField;
@property (nonatomic, strong) TitledSwitchView *saveLoginSwitch;
@property (nonatomic, strong) TitledSwitchView *loginWithPinSwitch;

@property (nonatomic, strong) RatesBannerView *ratesBannerView;
@property (nonatomic, strong) id ratesResponse;
@property (nonatomic, strong) Ticker *ticker;

@property (nonatomic) NSInteger graces;
@property (nonatomic) NSInteger expireDays;
@property (nonatomic) BOOL isPasswordExpired;

@end

@implementation ViewController {
    PinAuthView *pinAuthView;
}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

    [GAN sendView:@"Логин"];

    if ([PinHelper pinToken]) {
        [self showPinViewAnimated:false];
        //[self showEnterWithTouchIdButton];
    } else {
        self.view.userInteractionEnabled = YES;
    }

    [self configMainStyle];

    [self loadRates];
    [self loadTickers];
}

- (void)configMainStyle {

    // STPopup
    [STPopupNavigationBar appearance].tintColor = [UIColor fromRGB:0x444444];
    [STPopupNavigationBar appearance].barStyle = UIBarStyleDefault;
    [STPopupNavigationBar appearance].barTintColor = [UIColor flatWhiteColor];
    [STPopupNavigationBar appearance].translucent = false;
    [STPopupNavigationBar appearance].titleTextAttributes = @{ NSForegroundColorAttributeName: [UIColor fromRGB:0x444444] };

    [[UIBarButtonItem appearanceWhenContainedIn:[STPopupNavigationBar class], nil]
            setTitleTextAttributes:@{ NSForegroundColorAttributeName:[UIColor fromRGB:0x444444] }
                          forState:UIControlStateNormal];

    // Search bars
    if ([UIBarButtonItem instancesRespondToSelector:@selector(appearanceWhenContainedIn:)]) {
        [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor appDarkBlueColor]];
    }
    if ([UIBarButtonItem instancesRespondToSelector:@selector(appearanceWhenContainedInInstancesOfClasses:)]) {
        [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTintColor:[UIColor appDarkBlueColor]];
    }

    // Keyboard
    [IQKeyboardManager sharedManager].enable = true;
    [IQKeyboardManager sharedManager].enableAutoToolbar = true;
}

#pragma mark -

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // remove cache on each new session
    [self cleanCache];
}

#pragma mark - config actions

- (IBAction)openRegistration:(id)sender {
    RegistrationViewController *vc = [RegistrationViewController new];
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)forgotPassword:(id)sender {
    ForgotPasswordViewController *vc = [[ForgotPasswordViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
    
- (void)updatePassword {
    PasswordExpireViewController *vc = [[PasswordExpireViewController alloc] init];
    vc.expireDays = self.expireDays;
    vc.forceUpdate = true;
    vc.username = self.loginTextField.value;
    [self.navigationController pushViewController:vc animated:true];
}

- (void)saveLogin {
    if (self.saveLoginSwitch.isOn) {
        [[NSUserDefaults shared] setValue:self.loginTextField.value forKey:kKeyUsername];
    } else {
        [[NSUserDefaults shared] setValue:@"" forKey:kKeyUsername];
    }
}

- (void)loadTickers {
    if (!self.showAsModal) {
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:spinner];
        [spinner startAnimating];
    }

    [TickersApi getTickersSuccess:^(id response) {
        if (!self.showAsModal) {
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadRates)];
        }

        if (response != nil && [response isKindOfClass:[Ticker class]]) {

            self.ticker = response;

            self.ratesBannerView.tickerBannerView = [TickerBannerView configuredView];
            [self.ratesBannerView.tickerBannerView setLightStyle];
            self.ratesBannerView.tickerBannerView.ticker = self.ticker;

            if (pinAuthView) {
                pinAuthView.ticker = self.ticker;
            }
        }

    } failure:^(NSString *code, NSString *message) {
        if (!self.showAsModal) {
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadRates)];
        }
    }];
}

- (void)loadRates {
    if (!self.showAsModal) {
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:spinner];
        [spinner startAnimating];
    }

    [ExchangeRatesApi getCourses:^(id response) {
        if (!self.showAsModal) {
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadRates)];
        }
        //NSLog(@"Exchange Response %@", response);
        self.ratesResponse = response;
        [self onRatesLoaded:self.ratesResponse];
    } failure:^(NSString *code, NSString *message) {
        if (!self.showAsModal) {
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadRates)];
        }
    }];
}

- (void)onRatesLoaded:(NSArray *)rates {
    if (!self.ratesBannerView) {
        self.ratesBannerView = [[RatesBannerView alloc] initWithFrame:CGRectMake(0, 0, RATE_VIEW_PREFERRED_SIZE.width, RATE_VIEW_PREFERRED_SIZE.height)];
        self.navigationItem.titleView = self.ratesBannerView;
        [self.ratesBannerView startFlipTimer];
    }

    for (ExchangeRate *rate in rates) {
        [self.ratesBannerView setRateType:[RateView rateTypeForCurrency:rate.currency]
                                      buy:rate.getBuyDecimal
                                     sell:rate.getSellDecimal];
        if (pinAuthView) {
            [pinAuthView setRateType:[RateView rateTypeForCurrency:rate.currency]
                                 buy:rate.getBuyDecimal
                                sell:rate.getSellDecimal];
        }
    }
}

#pragma mark - Authorization

- (void)clickLoginButton {    
    [self authByLogin];
}

- (void)cleanKeyChainWithUsername:(NSString *)username {
    NSString *keyChainUsername = [[NSUserDefaults shared] objectForKey:kKeyUsername];

    if (![username isEqualToString:keyChainUsername]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUserUseTouchID];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUseTouchIDAsked];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

/**
 * Authorize by login and passwor were entered from keyboard
 *
 */
- (void)authByLogin {
    [self.view endEditing:YES];

    __weak ViewController *weakSelf = self;

//validation
    NSString *username = weakSelf.loginTextField.value;
    NSString *password = weakSelf.passwordTextField.value;

    if (username.length == 0) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"please", nil) message:NSLocalizedString(@"enter_username_email_or_phone_number", nil)];
        [weakSelf.loginTextField focus];
        return;
    }
    if (password.length == 0) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"please", nil) message:NSLocalizedString(@"enter_password", nil)];
        [weakSelf.passwordTextField focus];
        return;
    }

    // clean preferences if other user logged in
    [weakSelf cleanKeyChainWithUsername:username];

    //save login if needed
    [weakSelf saveLogin];

    // if login without pin turn on astanahud
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"entrance", nil) animated:true];

    //login
    [AuthApi authByLogin:username password:password success:^(id response) {

        self.graces = [response[@"graces"] integerValue];
        [User sharedInstance].graces = self.graces;
        self.expireDays = [response[@"expireDays"] integerValue];
        [User sharedInstance].expireDays = self.expireDays;
        self.isPasswordExpired = [response[@"isPasswordExpired"] boolValue];
        [User sharedInstance].isPasswordExpired = self.isPasswordExpired;
        
        // Сессия присваивается к обьекту User только так и только здесь
        [User sharedInstance].sessionID = response[@"reference"];

        [AccountsApi getAccountsWithUpdateForce:YES withSuccess:nil failure:nil];

        [AuthApi getClientBySession:[User sharedInstance].sessionID success:^(id response2) {
            [[User sharedInstance] setUserProfileData:response2];
            [AuthApi changeLangSuccess:nil failure:nil];


            [MBProgressHUD hideAstanaHUDAnimated:true];

            //if login with pin and touch id is set
            if (weakSelf.loginWithPinSwitch.isOn) {
                [weakSelf showPinViewAnimated:true];
            } else {
                //continue enter the bank
                [weakSelf enterTheBank];
            }
        } failure:^(NSString *code, NSString *message) {}];

    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        if ([code isEqualToString:UPDATE_PASSWORD]) {
            [self updatePassword];
        }
    }];
}


/**
 * Authorize using touch id that was activated throw PinAuthView
 *
 * @param pinAuthView
 */
- (void)showPinViewAnimated:(BOOL)animated {
    self.view.userInteractionEnabled = false;

    __weak ViewController *wSelf = self;

    if([PinHelper pinToken]) {
        //Login with existing pin and pin token
        pinAuthView = [[PinAuthView alloc] initWithPinType:PinAuthViewPinTypeAuth];
    } else  {
        // Set new pin
        pinAuthView = [[PinAuthView alloc] initWithPinType:PinAuthViewPinTypeNew];
    }

    if (pinAuthView) {
        pinAuthView.ticker = self.ticker;
    }

    for (ExchangeRate *rate in self.ratesResponse) {
        if (pinAuthView) {
            [pinAuthView setRateType:[RateView rateTypeForCurrency:rate.currency]
                                 buy:rate.getBuyDecimal
                                sell:rate.getSellDecimal];
        }
    }

    __weak PinAuthView *weakPinAuthView = pinAuthView;
    pinAuthView.onLoginWithTouchIDSuccess = ^(NSString *pinCode) {
        //If login by TouchID is Success
        //Authorize via "auth/pin" method
        NSString *pinToken = [PinHelper pinToken];

        [MBProgressHUD showAstanaHUDWithTitle:@"" animated:YES];
        [PinApi openClientSessionByPin:pinCode pinToken:pinToken success:^(id response) {

            [User sharedInstance].sessionID = response[@"reference"];

            [AccountsApi getAccountsWithUpdateForce:YES withSuccess:nil failure:nil];

            [AuthApi getClientBySession:[User sharedInstance].sessionID success:^(id response2) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
                [[User sharedInstance] setUserProfileData:response2];
                [AuthApi changeLangSuccess:nil failure:nil];
                [weakPinAuthView hide];
                [wSelf enterTheBank];
            } failure:^(NSString *code, NSString *message) {}];

        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            [weakPinAuthView reset];
            // Hide pin view
            [weakPinAuthView hide];
            //[MBProgressHUD hideAstanaHUDAnimated:true];
            if ([code isEqualToString:UPDATE_PASSWORD]) {
                
                [wSelf updatePassword];
            }
            
            
        }];
        
    };


    pinAuthView.pinAuthViewWillDismissAfterCancel = ^(PinAuthView *_pinAuthView) {
        [wSelf pinAuthViewWillDismissAfterCancel:_pinAuthView];
    };
    pinAuthView.pinAuthViewPinEntered = ^(PinAuthView *_pinAuthView) {
        [wSelf pinAuthViewPinEntered:_pinAuthView];
    };
    [pinAuthView show:animated];

    [self performBlock:^{
        wSelf.view.userInteractionEnabled = true;
    } afterDelay:0.25];
}


/**
 * Authorize using pincode that was entered in PinAuthView
 *
 * @param pinAuthView
 */
- (void)pinAuthViewPinEntered:(PinAuthView *)pinAuthView {
    __weak ViewController *wSelf = self;

    if (pinAuthView.pinType == PinAuthViewPinTypeAuth) {
        // Authorization with pin
        NSString *pinToken = [PinHelper pinToken];

        [MBProgressHUD showAstanaHUDWithTitle:@"" animated:YES];
        [PinApi openClientSessionByPin:pinAuthView.pinCode pinToken:pinToken success:^(id response) {
            //
            // Сессия присваивается к обьекту User только так и толь здесь
            //
            [User sharedInstance].sessionID = response[@"reference"];

            [AccountsApi getAccountsWithUpdateForce:YES withSuccess:nil failure:nil];

            [AuthApi getClientBySession:[User sharedInstance].sessionID success:^(id response2) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
                [[User sharedInstance] setUserProfileData:response2];
                [wSelf enterTheBank];
                [pinAuthView hide];

            } failure:^(NSString *code, NSString *message) {}];

        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];

            [pinAuthView shakePinsView];
            
            [pinAuthView reset];
            
            //[MBProgressHUD hideAstanaHUDAnimated:true];
            if ([code isEqualToString:UPDATE_PASSWORD]) {
                
                [self updatePassword];
            }
            
            

            // If pin entered N times incorrectly
            if ([code isEqualToString:@"PIN_BLOCKED"]) {
                [UIHelper showAlertWithMessage:NSLocalizedString(@"your_pin_blocked", nil)];
                // Delete pin token from local
                [PinHelper deletePinToken];
                [wSelf.loginWithPinSwitch setIsOn:NO];

                // Hide pin view
                [pinAuthView hide];
            }
        }];

    } else if(pinAuthView.pinType == PinAuthViewPinTypeNew) {
        // Set new pin
        [MBProgressHUD showAstanaHUDWithTitle:@"" animated:YES];
        [PinApi setPin:pinAuthView.pinCode login:self.loginTextField.value success:^(id response) {
            // Save pin token
            [PinHelper savePinToken:response withPin:pinAuthView.pinCode];
            [MBProgressHUD hideAstanaHUDAnimated:YES];

            // Ask to use touch id if not asked before
            if (![[NSUserDefaults standardUserDefaults] boolForKey:kUseTouchIDAsked]) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUseTouchIDAsked];
                [[NSUserDefaults standardUserDefaults] synchronize];

                AMTouchIdAuth *touchIdAuth = [[AMTouchIdAuth alloc] init];
                [touchIdAuth askUseTouchIdCompletion:^(BOOL success) {
                    [[NSUserDefaults standardUserDefaults] setBool:success forKey:kUserUseTouchID];
                    [[NSUserDefaults standardUserDefaults] synchronize];

                    [AuthApi changeLangSuccess:nil failure:nil];
                    // Hide pin view
                    [pinAuthView hide];

                    // User logged in! Go back!
                    [wSelf enterTheBank];

                    [wSelf sendPinTokenToWatchApp];
                }];
            } else { // login without touch id
                [MBProgressHUD hideAstanaHUDAnimated:YES];
                // Hide pin view
                [pinAuthView hide];

                [AuthApi changeLangSuccess:nil failure:nil];
                // User logged in! Go back!
                [wSelf enterTheBank];

                [self sendPinTokenToWatchApp];
            }
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            [pinAuthView reset];
        }];
    }
}

/**
 * Switch off pin code and remove them
 *
 * @param pinAuthView
 */
- (void)pinAuthViewWillDismissAfterCancel:(PinAuthView *)pinAuthView {
    if(pinAuthView.pinType == PinAuthViewPinTypeNew) {
        // User logged in! Go back!
        [self enterTheBank];
    } else if(pinAuthView.pinType == PinAuthViewPinTypeAuth) {
        [MBProgressHUD showAstanaHUDWithTitle:@"" animated:YES];
        [PinApi deletePin:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];

        [PinHelper deletePinToken];

        // Uncheck the pin auth checkbox
        [self.loginWithPinSwitch setIsOn:NO];
    }
}


/**
 * Proceed transition to MainViewController (Enter to the bank)
 *
 */
- (void)enterTheBank {

    [GAN sendEventWithCategory:@"Вход" action:@"Вход в систему"];

    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserLoggedIn object:nil];

    if (self.showAsModal) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }

    MainViewController *main = [[MainViewController alloc] init];
    main.graces = self.graces;
    main.isPasswordExpired = self.isPasswordExpired;
    UINavigationController *nc = [UIHelper defaultNavigationController];
    nc.viewControllers = @[main];
    LGSideMenuController *sideMenuController = kSideMenuController;

    sideMenuController.rootViewController = nc;
    [sideMenuController hideRightViewAnimated:YES completionHandler:nil];

}

/**
 * Remove cached login, passwords and reset city in userdefaults
 *
 */

- (void)cleanCache {
    // remove data from cache
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestsCache sharedCache] removeAllObjects];
    });

    // reset user preferences
    NSUserDefaults *sud = [NSUserDefaults standardUserDefaults];
    [sud setObject:@(110) forKey:kUserSelectedCountry]; // kz
    [sud setObject:NSLocalizedString(@"kazakhstan", nil) forKey:kUserSelectedCountryName]; // kz
    [sud setObject:@(-2) forKey:kUserSelectedCity]; // all cities
    [sud setObject:NSLocalizedString(@"all", nil) forKey:kUserSelectedCityName]; // all cities
    [sud synchronize];
}

#pragma mark - Watch App

- (void)sendPinTokenToWatchApp {
    if (WCSession.defaultSession.reachable == true) {
        NSString *pinToken = @"";
        if ([PinHelper pinToken]) {
            pinToken = [PinHelper pinToken];
        }
        NSString *deviceID = [AppUtils deviceID];
        NSDictionary *messsageObject = @{@"pinToken": pinToken, @"deviceID": deviceID};

        NSDictionary *sendPinToken = @{wcMessageType: wcMessageTypeGetPinToken, wcMessageObject: messsageObject};

        [[WCSession defaultSession] sendMessage:sendPinToken
                                   replyHandler:^(NSDictionary *reply) {
                                       NSLog(@"Sent message to watch app with reply - %@", reply);
                                   }
                                   errorHandler:^(NSError *error) {
                                       NSLog(@"Failed to send message to watch app with error - %@", error);
                                   }
         ];
    } else {
        NSLog(@"Watch app not reachable");
    }
}

#pragma mark - config ui

- (void)configUI {
    if(!self.showAsModal) [self showMenuButton];

    [self setBafBackground];
    [self showBafLogo];
    [self hideBackButtonTitle];
    
    self.topTitle.text = NSLocalizedString(@"login_to_banking", nil);
    [self.registerButton setTitle:NSLocalizedString(@"sign_up", nil) forState:UIControlStateNormal];
    [self.fogetPasswordButton setTitle:NSLocalizedString(@"forget_password", nil) forState:UIControlStateNormal];

    if ([[UIApplication sharedApplication] respondsToSelector:@selector(setStatusBarHidden:)]) {
        [[UIApplication sharedApplication] setStatusBarHidden:false];
    }

    if (self.showAsModal) {
        [self setNavigationCloseButtonAtLeft:YES];
    } else {
        [self showPhoneButton];
    }

    self.scrollView.backgroundColor = [UIColor clearColor];

    self.loginContainer.backgroundColor = [UIColor clearColor];
    self.saveLoginContainer.backgroundColor = [UIColor clearColor];
    self.passwordContainer.backgroundColor = [UIColor clearColor];
    self.loginWithPinContainer.backgroundColor = [UIColor clearColor];
    self.enterButtonContainer.backgroundColor = [UIColor clearColor];

    //login textfiled
    self.loginTextField = [FloatingTextField newAutoLayoutView];
    self.loginTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.loginTextField.separatorHidden = false;
    self.loginTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.loginTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.loginTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.loginTextField.placeholder = NSLocalizedString(@"email_or_phone_number", nil);
    [self.loginContainer addSubview:self.loginTextField];
    [self.loginTextField aa_superviewFillWithInset:0];

    //save login switch
    self.saveLoginSwitch = [[TitledSwitchView alloc] init];
    self.saveLoginSwitch.backgroundColor = [UIColor clearColor];
    self.saveLoginSwitch.title = NSLocalizedString(@"save_login", nil);
    [self.saveLoginContainer addSubview:self.saveLoginSwitch];


    //password textfield
    self.passwordTextField = [FloatingTextField newAutoLayoutView];
    self.passwordTextField.separatorHidden = false;
    self.passwordTextField.secureTextEntry = true;
    self.passwordTextField.placeholder = NSLocalizedString(@"enter_password", nil);
    [self.passwordContainer addSubview:self.passwordTextField];
    [self.passwordTextField aa_superviewFillWithInset:0];

    //enter with pin and touchid switch
    self.loginWithPinSwitch = [[TitledSwitchView alloc] init];
    self.loginWithPinSwitch.backgroundColor = [UIColor clearColor];
    NSString *loginWithPinTitle;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL isTouchIDOn = [userDefaults boolForKey:kUserUseTouchID];
    if (isTouchIDOn) {
        loginWithPinTitle = NSLocalizedString(@"enter_by_pin_or_touch", nil);
    } else {
        loginWithPinTitle = NSLocalizedString(@"enter_by_pin", nil);
    }
    self.loginWithPinSwitch.title = loginWithPinTitle;
    self.loginWithPinSwitch.isOn = NO;
    self.loginWithPinSwitch.onSwitchValueChanged = ^(BOOL isON) {

    };
    [self.loginWithPinContainer addSubview:self.loginWithPinSwitch];
    [self.loginWithPinSwitch aa_superviewFillWithInset:0];

    //enter button
    MainButton *enterButton = [[MainButton alloc] init];
    enterButton.mainButtonStyle = MainButtonStyleOrange;
    enterButton.title = NSLocalizedString(@"enter", nil);
    [enterButton addTarget:self action:@selector(clickLoginButton) forControlEvents:UIControlEventTouchUpInside];
    [self.enterButtonContainer addSubview:enterButton];
    [enterButton aa_superviewFillWithInset:0];

    //if saved username exists set into logintextfield
    NSString *savedUsername = [[NSUserDefaults shared] valueForKey:kKeyUsername];
    [self.loginTextField setValue:(savedUsername?savedUsername:@"")];
    [self.saveLoginSwitch setIsOn:savedUsername != nil];


    //if login with pin and touchid on turn switch
    if ([PinHelper pinToken]) {
        self.loginWithPinSwitch.isOn = YES;
    }
}

- (BOOL)prefersStatusBarHidden {
    return false;
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
