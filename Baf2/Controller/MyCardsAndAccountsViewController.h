//
//  MyCardsAndAccountsViewController.h
//  BAF
//
//  Created by Almas Adilbek on 8/12/14.
//
//



#import "ProductRowView.h"
#import "DepositsListView.h"
#import "AAccordionView.h"
#import "RegisteredCardsListView.h"
#import "ConnectAccountsMessageBoxView.h"
#import "ConnectAccountsSearchUserViewController.h"
#import "BankCardsListView.h"
#import "ScrollViewController.h"

@class AccountsResponse;

static NSString * const kMyCardsAndAccountsViewControllerUpdateAccountsNotification = @"MyCardsAndAccountsViewControllerUpdateAccountsNotification";

@interface MyCardsAndAccountsViewController : ScrollViewController <AAccordionViewDelegate, ProductRowViewDelegate, DepositsListViewDelegate, BankCardsListViewDelegate, RegisteredCardsListViewDelegate, ConnectAccountsMessageBoxViewDelegate>

@property (nonatomic, strong) AccountsResponse *accountsResponse;
@property (nonatomic, assign) BOOL isOpenedFromMenu;

@end