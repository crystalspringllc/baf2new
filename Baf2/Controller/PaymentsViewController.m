//
//  PaymentsViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 4/25/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "PaymentsViewController.h"
#import "PaymentFormViewController.h"
#import "ContractPaymentCell.h"
#import "ContractsApi.h"
#import "ProvidersApi.h"
#import "LocationsApi.h"
#import "Location.h"
#import "ProviderCategory.h"
#import "PaymentsSearchViewController.h"
#import "CategoryProvidersListViewController.h"
#import "NotificationCenterHelper.h"
#import "PaymentProvidersByCategory.h"
#import "ActionSheetListPopupViewController.h"
#import "AstanaRefreshControl.h"
#import <STPopup.h>
#import "STPopupController+Extensions.h"
#import "ContractModelView.h"
#import "AlsekoReceiptViewController.h"
#import "PaymentContractTableLIst.h"

@interface PaymentsViewController ()<UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;

@property (weak, nonatomic) IBOutlet UIButton *countryButton;
@property (weak, nonatomic) IBOutlet UIButton *cityButton;

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) NSLayoutConstraint *paymentsSearchViewControllerBottomConstraint;
@property (nonatomic, strong) PaymentsSearchViewController *paymentsSearchViewController;
@property (weak, nonatomic) IBOutlet UILabel *paymentsLabel;

@property (nonatomic) CGFloat currentKeyboardHeight;

@property (nonatomic, strong) STPopupController *actionSheetPopupController;

@property (nonatomic, strong) AstanaRefreshControl *astanaRefreshControl;

@end

@implementation PaymentsViewController {
    NSArray *paymentProvidersByCategories;
}

- (id)init {
    self = [super init];
    if(self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Payments" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PaymentsViewController class])];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self loadContracts:true];
    [self reloadButtonTitles];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    if (self != [self.navigationController.viewControllers objectAtIndex:0]) {
        [self setNavigationBackButton];
    } else {
        self.navigationItem.leftBarButtonItem = nil;
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.emptyView layoutIfNeeded];
    self.emptyLabel.preferredMaxLayoutWidth = self.emptyLabel.frame.size.width;
}

#pragma mark - load contracts

- (void)loadContracts:(BOOL)isNotRefresh {
    __weak PaymentsViewController *weakSelf = self;
    
    self.emptyView.hidden = true;
    
    dispatch_group_t group_queue = dispatch_group_create();
    
    if (!self.contracts || self.contracts.count == 0 || !isNotRefresh || [[ContractModelView sharedInstance] needsReload]) {
        [[ContractModelView sharedInstance] setNeedsReload:false];
        
        if (isNotRefresh) {
            [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading_payment_template_and_providers", nil) animated:YES];
        }

        dispatch_group_enter(group_queue);
        [[ContractModelView sharedInstance] loadContracts:^(BOOL success, NSError *error) {
            dispatch_group_leave(group_queue);
        }];
    } else if (isNotRefresh) {
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"provider_loading", nil) animated:YES];
    }
    
    if (!paymentProvidersByCategories || paymentProvidersByCategories.count == 0 || !isNotRefresh) {
        dispatch_group_enter(group_queue);
        NSUserDefaults *sud = [NSUserDefaults standardUserDefaults];
        [ProvidersApi getProvidersWithCategoriesByLocation:[sud objectForKey:kUserSelectedCountry] city:[sud objectForKey:kUserSelectedCity] success:^(NSArray *paymentProvidersByCategories) {
            dispatch_group_leave(group_queue);
        } failure:^(NSString *code, NSString *message) {
            dispatch_group_leave(group_queue);
        }];
    }
    
    dispatch_group_notify(group_queue, dispatch_get_main_queue(), ^{
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        
        if (!isNotRefresh) {
            [weakSelf.astanaRefreshControl finishingLoading];
        }
        
        [weakSelf reloadData];
    });
}

- (void)reloadContracts {
    [self loadContracts:true];
}

- (void)reloadData {
    self.contracts = [ContractModelView sharedInstance].contracts.mutableCopy;
    
    paymentProvidersByCategories = ProvidersApi.enabledProvidersByCategories;
    
    if (self.contracts.count == 0) {
        self.emptyView.hidden = false;
        self.collectionView.hidden = true;
    } else {
        self.emptyView.hidden = true;
        self.collectionView.hidden = false;
    }
    
    [self.tableView reloadData];
    [self.collectionView reloadData];
    [self reloadButtonTitles];
}

- (void)deleteContract:(NSNotification *)notification {
    self.contracts = [ContractModelView sharedInstance].contracts.mutableCopy;
    
    if (self.contracts.count == 0) {
        self.emptyView.hidden = false;
        self.collectionView.hidden = true;
    } else {
        self.emptyView.hidden = true;
        self.collectionView.hidden = false;
    }
    
    [self.collectionView reloadData];
}

- (void)chooseCountry {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"country_loading", nil) animated:true];
    
    __weak PaymentsViewController *weakSelf = self;
    [LocationsApi getCountriesWithSuccess:^(NSArray *countries) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        NSUserDefaults *sud = [NSUserDefaults standardUserDefaults];
        NSInteger selectedIndex = -1;

        NSMutableArray *options = [NSMutableArray new];
        for (int i = 0; i < countries.count; i++) {
            Location *location = countries[i];
            ActionSheetListRowObject *option = [ActionSheetListRowObject new];
            option.title = location.name;
            option.keepObject = location;
            [options addObject:option];
            
            if ([location.locationId isEqualToString:[NSString stringWithFormat:@"%@", [sud objectForKey:kUserSelectedCountry]]]) {
                selectedIndex = i;
            }
        }
        
        ActionSheetListPopupViewController *vc = [ActionSheetListPopupViewController new];
        vc.numberOfRowsInSection = ^NSInteger(NSInteger section) {
            return options.count;
        };
        vc.rowForIndexPath = ^ActionSheetListRowObject*(NSIndexPath *indexPath) {
            ActionSheetListRowObject *rowObject = options[(NSUInteger) indexPath.row];
            
            return rowObject;
        };
        vc.selectedIndexPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
        vc.onSelectObject = ^(Location *selectedObject, NSIndexPath *indexPath) {
            [sud setObject:selectedObject.locationId forKey:kUserSelectedCountry];
            [sud setObject:selectedObject.name forKey:kUserSelectedCountryName];
            [sud setObject:@(-2) forKey:kUserSelectedCity]; // all cities
            [sud setObject:NSLocalizedString(@"all", nil) forKey:kUserSelectedCityName]; // all cities
            [sud synchronize];
            
            paymentProvidersByCategories = @[]; // reset
            [weakSelf loadContracts:true];
        };
        vc.title = NSLocalizedString(@"select_contry", nil);
        weakSelf.actionSheetPopupController = [[STPopupController alloc] initWithRootViewController:vc];
        weakSelf.actionSheetPopupController.style = STPopupStyleBottomSheet;
        weakSelf.actionSheetPopupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
        weakSelf.actionSheetPopupController.dismissOnBackgroundTap = true;
        [weakSelf.actionSheetPopupController presentInViewController:weakSelf];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

- (void)chooseCity {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"city_loading", nil) animated:true];
    
    __weak PaymentsViewController *weakSelf = self;
    
    NSUserDefaults *sud = [NSUserDefaults standardUserDefaults];
    [LocationsApi getCities:[sud objectForKey:kUserSelectedCountry] success:^(NSArray *cities) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        NSInteger selectedIndex = -1;
        
        NSMutableArray *options = [NSMutableArray new];
        for (int i = 0; i < cities.count; i++) {
            Location *location = cities[i];
            ActionSheetListRowObject *option = [ActionSheetListRowObject new];
            option.title = location.name;
            option.keepObject = location;
            [options addObject:option];
            
            if ([location.locationId isEqualToString:[NSString stringWithFormat:@"%@", [sud objectForKey:kUserSelectedCity]]]) {
                selectedIndex = i;
            }
        }
        
        ActionSheetListPopupViewController *vc = [ActionSheetListPopupViewController new];
        vc.numberOfRowsInSection = ^NSInteger(NSInteger section) {
            return options.count;
        };
        vc.rowForIndexPath = ^ActionSheetListRowObject*(NSIndexPath *indexPath) {
            ActionSheetListRowObject *rowObject = options[(NSUInteger) indexPath.row];
            
            return rowObject;
        };
        vc.selectedIndexPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
        vc.onSelectObject = ^(Location *selectedObject, NSIndexPath *indexPath) {
            [sud setObject:selectedObject.locationId forKey:kUserSelectedCity];
            [sud setObject:selectedObject.name forKey:kUserSelectedCityName];
            [sud synchronize];
            
            paymentProvidersByCategories = @[]; // reset
            [weakSelf loadContracts:true];
        };
        vc.title = NSLocalizedString(@"choose_city", nil);
        weakSelf.actionSheetPopupController = [[STPopupController alloc] initWithRootViewController:vc];
        weakSelf.actionSheetPopupController.dismissOnBackgroundTap = true;
        weakSelf.actionSheetPopupController.style = STPopupStyleBottomSheet;
        weakSelf.actionSheetPopupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
        [weakSelf.actionSheetPopupController presentInViewController:weakSelf];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

- (void)reloadButtonTitles {
    NSUserDefaults *sud = [NSUserDefaults standardUserDefaults];
    
    NSString *countryName = [sud objectForKey:kUserSelectedCountryName];
    [self.countryButton setTitle:countryName forState:UIControlStateNormal];
    
    NSString *cityName = [sud objectForKey:kUserSelectedCityName];
    [self.cityButton setTitle:cityName forState:UIControlStateNormal];
}

#pragma mark - search delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self searchOnlyProviderCategories:searchText];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    if (searchBar.text && searchBar.text.length > 0) {
        [self searchOnlyProviderCategories:searchBar.text];
    }

    self.tableView.scrollEnabled = false;
    self.tableView.contentInset = UIEdgeInsetsMake(-236, 0, 0, 0);
    [self.searchBar setShowsCancelButton:true animated:true];
    
    [self showSearchViewController];
    
    return true;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchOnlyProviderCategories:searchBar.text];

    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    
    self.tableView.scrollEnabled = true;
    self.tableView.contentInset = UIEdgeInsetsZero;
    [self.searchBar setShowsCancelButton:false animated:true];
    
    searchBar.text = nil;
    
    [self hideSearchViewController];
}

#pragma mark - collection delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.contracts.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ContractPaymentCell *cell = (ContractPaymentCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"ContractPaymentCell" forIndexPath:indexPath];
    Contract *contract = self.contracts[(NSUInteger) indexPath.row];
    cell.contract = contract;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Contract *contract = self.contracts[(NSUInteger) indexPath.row];
    [self payForContract:contract];
}

#pragma mark - tableview delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return paymentProvidersByCategories.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.textLabel.textColor = [UIColor fromRGB:0x146888];
    cell.textLabel.font = [UIFont systemFontOfSize:15];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PaymentsCell"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    PaymentProvidersByCategory *providers = paymentProvidersByCategories[(NSUInteger) indexPath.row];
    NSArray *paymentProviders = providers.paymentProviderListWS;
    
    if (providers.category.categoryName) {
     cell.textLabel.text = [NSString stringWithFormat:@"%@ (%ld)", providers.category.categoryName, (long)paymentProviders.count];
    } else {
     cell.textLabel.text = @"";
    }
    
    
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    PaymentProvidersByCategory *providers = paymentProvidersByCategories[(NSUInteger) indexPath.row];
    ProviderCategory *category = providers.category;
    if(category) {
        CategoryProvidersListViewController *v = [[CategoryProvidersListViewController alloc] init];
        v.title = category.categoryName;
        v.paymentProviders = providers.paymentProviderListWS;
        v.providerCategoryId = category.providerCategoryId.integerValue;
        [self.navigationController pushViewController:v animated:true];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.tableView) {
        [self.astanaRefreshControl scrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.tableView) {
        [self.astanaRefreshControl scrollViewWillEndDragging];
    }
}

#pragma mark - Search Animation & Other

- (void)showSearchViewController {
    if (!self.paymentsSearchViewController) {
        self.contentView = [UIView newAutoLayoutView];
        self.contentView.backgroundColor = [UIColor fromRGB:0xF4F4F4];
        self.contentView.alpha = 0;
        [self.view addSubview:self.contentView];
        [self.contentView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(44, 0, 0, 0)];
        
        self.paymentsSearchViewController = [PaymentsSearchViewController new];
        self.paymentsSearchViewController.paymentsViewController = self;
        [self addChildViewController:self.paymentsSearchViewController];
        self.paymentsSearchViewController.view.translatesAutoresizingMaskIntoConstraints = false;
        [self.contentView addSubview:self.paymentsSearchViewController.view];
        [self.paymentsSearchViewController.view autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero excludingEdge:ALEdgeBottom];
        if (self.paymentsSearchViewControllerBottomConstraint) {
            [self.paymentsSearchViewControllerBottomConstraint autoRemove];
        }
        self.paymentsSearchViewControllerBottomConstraint = [self.paymentsSearchViewController.view autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:self.currentKeyboardHeight];
        [self.paymentsSearchViewController didMoveToParentViewController:self];
        
        [UIView animateWithDuration:0.275 animations:^{
            self.contentView.alpha = 1;
        }];
    }
}

- (void)hideSearchViewController {
    if (self.paymentsSearchViewController) {
        [UIView animateWithDuration:0.275 animations:^{
            self.contentView.alpha = 0;
        } completion:^(BOOL finished) {
            [self.paymentsSearchViewController willMoveToParentViewController:nil];
            [self.paymentsSearchViewController.view removeFromSuperview];
            [self.paymentsSearchViewController removeFromParentViewController];
            self.paymentsSearchViewController = nil;
            
            [self.contentView removeFromSuperview];
            self.contentView = nil;;
        }];
    }
}

#pragma mark - Keyboard

- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    
    [self updateConstraintsAccordingKeyboardInfo:info];
}

- (void)keyboardWillHide:(NSNotification*)notification {
    self.currentKeyboardHeight = 0;
    
    if (self.paymentsSearchViewControllerBottomConstraint) {
        [self.paymentsSearchViewControllerBottomConstraint autoRemove];
    }
    self.paymentsSearchViewControllerBottomConstraint = [self.paymentsSearchViewController.view autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:self.currentKeyboardHeight];
}

- (void)updateConstraintsAccordingKeyboardInfo:(NSDictionary *)info {
    NSValue *keyboardFrameBegin = [info valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [keyboardFrameBegin CGRectValue];
    self.currentKeyboardHeight = keyboardFrame.size.height;
    
    if (self.paymentsSearchViewControllerBottomConstraint) {
        [self.paymentsSearchViewControllerBottomConstraint autoRemove];
    }
    self.paymentsSearchViewControllerBottomConstraint = [self.paymentsSearchViewController.view autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:self.currentKeyboardHeight];
}

-(void)tapShowPaymnetsList
{
    PaymentContractTableLIst *vc = [PaymentContractTableLIst createVC];
    vc.contracts = self.contracts;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - config ui

- (void)configUI {
    [self setBafBackground];
    [self setNavigationTitle:NSLocalizedString(@"ga_payments", nil)];
    [self hideBackButtonTitle];
    
    [self showMenuButton];
    
    self.searchBar.placeholder = NSLocalizedString(@"search", nil);
    self.searchBar.tintColor = [UIColor appBlueColor];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self]; // sometimes dealloc not called, therefore remove previous observers if exist

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadContracts) name:kNotificationPaymentTemplatesUpdated object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteContract:) name:kNotificationPaymentTemplateDeleted object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:kNotificationPaymentTemplatesNeedsReload object:nil];
    
    [self.countryButton addTarget:self action:@selector(chooseCountry) forControlEvents:UIControlEventTouchUpInside];
    [self.cityButton addTarget:self action:@selector(chooseCity) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *recongniser = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapShowPaymnetsList)];
   
    [self.paymentsLabel setUserInteractionEnabled:YES];
    [self.paymentsLabel addGestureRecognizer:recongniser];
    
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.rowHeight = 50;
    self.tableView.tableFooterView = [UIView new];
    
    self.astanaRefreshControl = [AstanaRefreshControl new];
    self.astanaRefreshControl.target = self;
    self.astanaRefreshControl.action = @selector(loadContracts:);
    [self.tableView insertSubview:self.astanaRefreshControl atIndex:0];
}

#pragma mark -

- (void)payForContract:(Contract *)contract {
    UIViewController *v;

    if ([contract.provider.infoRequestType isEqualToNumber:@2]) {
        AlsekoReceiptViewController *alsekoVC = [AlsekoReceiptViewController createVC];
        alsekoVC.contract = contract;
        v = alsekoVC;
    } else {
        PaymentFormViewController *paymentVC = [PaymentFormViewController createVC];
        paymentVC.actionType = ActionTypeMakePayment;
        paymentVC.contract = contract;
        v = paymentVC;
    }

    [self.navigationController pushViewController:v animated:YES];
}

- (void)searchOnlyProviderCategories:(NSString *)text {
    if (self.didChangeSearchText) {
        self.didChangeSearchText(text);
    }
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
