//
//  RegistrationStep1ViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 26.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "RegistrationStep1ViewController.h"
#import "MainButton.h"
#import "AuthApi.h"
#import "UIS.h"
#import "NSString+Validators.h"
#import <JVFloatLabeledTextField.h>
#import "UITableViewController+Extension.h"
#import "ChameleonMacros.h"

typedef void (^isValid)(BOOL valid);

@interface RegistrationStep1ViewController () <UITextFieldDelegate>

@property (nonatomic, strong) UISwitch *privacySwitchView;
@property (nonatomic) BOOL privacyAccepted;

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *middleNameTextField;

@property (weak, nonatomic) IBOutlet MainButton *nextButton;


@property (weak, nonatomic) IBOutlet UILabel *policyLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *aceptPolicyLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;

@end

@implementation RegistrationStep1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
}

#pragma mark - Actions

- (void)tappedNext:(MainButton *)sender {
    [self.view endEditing:true];
    
    __weak RegistrationStep1ViewController *weakSelf = self;

    if (!self.privacySwitchView.isOn) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"you_must_accept_rules", nil) message:@""];
        return;
    }
    
    if (![self.phoneTextField.text.validPhoneNumber validate]) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"error_in_form", nil) message:NSLocalizedString(@"phone_number_filled_incorrect", nil)];
        return;
    }
    
    if (![weakSelf.emailTextField.text validate] && ![weakSelf.emailTextField.text validateEmail]) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"error_in_form", nil) message:NSLocalizedString(@"format_email_incorrect", nil)];
        return;
    }
    
    if (![weakSelf.lastNameTextField.text validate]) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"error_in_form", nil) message:NSLocalizedString(@"surname_not_entered", nil)];
        return;
    }
    if (![weakSelf.firstNameTextField.text validate]) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"error_in_form", nil) message:NSLocalizedString(@"name_not_entered", nil)];
        return;
    }
//    if (![weakSelf.middleNameTextField.text validate]) {
//        [UIHelper showAlertWithMessageTitle:@"Ошибка в форме" message:@"Отчество не введено"];
//        return;
//    }
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"cheking_phone_number", nil) animated:true];
    [AuthApi isPhoneExists:self.phoneTextField.text.validPhoneNumber success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];

        BOOL phoneExist = [response boolValue];
        
        if (phoneExist) {
            [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"this_number_is_already_registered", nil)];
            return;
        }
        
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"cheking_email", nil) animated:true];
        [AuthApi isEmailExists:weakSelf.emailTextField.text success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
            
            BOOL emailExist = [response boolValue];
            
            if (emailExist) {
                [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"email", nil) message:NSLocalizedString(@"this_email_is_already_registered", nil)];
                return;
            }
            
            weakSelf.phone = weakSelf.phoneTextField.text.validPhoneNumber;
            weakSelf.email = weakSelf.emailTextField.text;
            weakSelf.lastName = weakSelf.lastNameTextField.text;
            weakSelf.firstName = weakSelf.firstNameTextField.text;
            weakSelf.middleName = weakSelf.middleNameTextField.text;
            
            if (weakSelf.didValidate) {
                weakSelf.didValidate();
            }
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

- (void)privacyTapped {
    [UIS presentTermsAndConditionsWithTarget:self];
}

- (void)privacyChanged:(UISwitch *)sender {
    self.privacyAccepted = sender.isOn;
}

#pragma mark - Validation

NSString *filteredPhoneStringFromStringWithFilter(NSString *string, NSString *filter) {
    NSUInteger onOriginal = 0, onFilter = 0, onOutput = 0;
    char outputString[([filter length])];
    BOOL done = NO;
    
    while(onFilter < [filter length] && !done)
    {
        char filterChar = [filter characterAtIndex:onFilter];
        char originalChar = onOriginal >= string.length ? '\0' : [string characterAtIndex:onOriginal];
        switch (filterChar) {
            case '#':
                if(originalChar=='\0')
                {
                    // We have no more input numbers for the filter.  We're done.
                    done = YES;
                    break;
                }
                if(isdigit(originalChar))
                {
                    outputString[onOutput] = originalChar;
                    onOriginal++;
                    onFilter++;
                    onOutput++;
                }
                else
                {
                    onOriginal++;
                }
                break;
            default:
                // Any other character will automatically be inserted for the user as they type (spaces, - etc..) or deleted as they delete if there are more numbers to come.
                outputString[onOutput] = filterChar;
                onOutput++;
                onFilter++;
                if(originalChar == filterChar)
                    onOriginal++;
                break;
        }
    }
    outputString[onOutput] = '\0'; // Cap the output string
    return [NSString stringWithUTF8String:outputString];
}

#pragma mark - UITableViewDataSource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return NSLocalizedString(@"policy", nil);
    } else if (section == 1) {
        return NSLocalizedString(@"contact_datas", nil);
    } else if (section == 3) {
        return NSLocalizedString(@"select_mobile_operator_number", nil);
    }
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section == 1) {
        return NSLocalizedString(@"ankets_datas", nil);
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            UIImageView *disclosureIndicator = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
            disclosureIndicator.contentMode = UIViewContentModeScaleAspectFit;
            disclosureIndicator.image = [UIImage imageNamed:@"next"];
            cell.accessoryView = disclosureIndicator;
        } else if (indexPath.row == 1) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            self.privacySwitchView = [UISwitch new];
            self.privacySwitchView.onTintColor = [UIColor flatSkyBlueColorDark];
            [self.privacySwitchView setOn:self.privacyAccepted];
            [self.privacySwitchView addTarget:self action:@selector(privacyChanged:) forControlEvents:UIControlEventValueChanged];
            cell.accessoryView = self.privacySwitchView;
        }
    } else if (indexPath.section == 3) {
        if (indexPath.row == 3) {
            UIImageView *disclosureIndicator = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
            disclosureIndicator.contentMode = UIViewContentModeScaleAspectFit;
            disclosureIndicator.image = [UIImage imageNamed:@"expand_arrow"];
            cell.accessoryView = disclosureIndicator;
        } else {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    } else  {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [self privacyTapped];
        }
    } else if (indexPath.section == 1) {
        [self.phoneTextField becomeFirstResponder];
    } else if (indexPath.section == 2) {
        [self.emailTextField becomeFirstResponder];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.phoneTextField) {
        NSString *filter = @"+7 (7##) ### ## ##";
        
        if (!filter) return YES; // No filter provided, allow anything
        
        NSString *changedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if (range.length == 1 && // Only do for single deletes
           string.length < range.length &&
           [[textField.text substringWithRange:range] rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]].location == NSNotFound) {
            // Something was deleted.  Delete past the previous number
            NSInteger location = changedString.length-1;
            if (location > 0) {
                for (; location > 0; location--) {
                    if (isdigit([changedString characterAtIndex:location])) {
                        break;
                    }
                }
                changedString = [changedString substringToIndex:location];
            }
        }
        
        textField.text = filteredPhoneStringFromStringWithFilter(changedString, filter);
        
        return false;
    }
    
    return true;
}

#pragma mark - Config UI

- (void)configUI {
    [self setNavigationBackButton];
    [self setBAFTableViewBackground];

    self.privacySwitchView.onTintColor = [UIColor flatSkyBlueColorDark];
    self.privacySwitchView.thumbTintColor = [UIColor fromRGB:0x209A7C];
    self.privacySwitchView.tintColor = [UIColor fromRGB:0x1F987C];

    self.phoneTextField.delegate = self;
    self.phoneTextField.keyboardType = UIKeyboardTypePhonePad;
    self.phoneTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.phoneTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    if (self.phone) {
        self.phoneTextField.text = self.phone;
    }
    
    self.emailTextField.delegate = self;
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.lastNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.firstNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.middleNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.nextButton.mainButtonStyle = MainButtonStyleOrange;
    self.nextButton.title = NSLocalizedString(@"next", nil);
    [self.nextButton addTarget:self action:@selector(tappedNext:) forControlEvents:UIControlEventTouchUpInside];
    
    self.policyLabel.text = NSLocalizedString(@"confidentional_policy", nil);
    self.phoneNumberLabel.text = NSLocalizedString(@"phone_number", nil);
    self.aceptPolicyLabel.text = NSLocalizedString(@"acept_with_policy", nil);
    self.emailLabel.text = NSLocalizedString(@"your_mail", nil);
    
    self.lastNameTextField.placeholder = NSLocalizedString(@"last_name", nil);
    self.firstNameTextField.placeholder = NSLocalizedString(@"first_name", nil);
    self.middleNameTextField.placeholder = NSLocalizedString(@"middle_name", nil);
    
}

@end
