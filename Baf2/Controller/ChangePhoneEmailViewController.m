//
//  ChangePhoneEmailViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 6/22/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "ChangePhoneEmailViewController.h"
#import "FloatingTextField.h"
#import "MainButton.h"
#import "User.h"
#import "STPopup.h"
#import "NSString+Validators.h"
#import "ProfileApi.h"
#import "ChangeEmailConfirmViewController.h"
#import "ChangePhoneConfirmViewController.h"


@interface ChangePhoneEmailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *oldLoginLabel;
@property (weak, nonatomic) IBOutlet FloatingTextField *neuloginTextField;
@property (weak, nonatomic) IBOutlet UILabel *sendToEmailInstedOfOldNumberLabel;
@property (weak, nonatomic) IBOutlet UISwitch *sendToEmailInsteadOfOldNumberSwitch;
@property (weak, nonatomic) IBOutlet UILabel *disclamerLabel;
@property (weak, nonatomic) IBOutlet UILabel *confirmLabel;
@property (weak, nonatomic) IBOutlet UISwitch *confirmSwitch;
@property (weak, nonatomic) IBOutlet MainButton *nextButton;

@end

@implementation ChangePhoneEmailViewController

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ChangePhoneEmail" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 470);
}

#pragma mark - config actions

- (IBAction)switchConfirm:(UISwitch *)sender {
    if (sender.isOn) {
        self.nextButton.mainButtonStyle = MainButtonStyleOrange;
    } else {
        self.nextButton.mainButtonStyle = MainButtonStyleDisable;
    }
}

- (IBAction)clickNextButton:(id)sender {
    if (!self.isPhone) {
        [self proceedWithEmail:self.neuloginTextField.text];
    } else {
        [self proceedWithPhone:self.neuloginTextField.text];
    }
}

- (void)proceedWithEmail:(NSString *)email {
    if ([[User sharedInstance].email isEqualToString:self.neuloginTextField.text]) {
        [Toast showToast:@"Введите новый email"];
        return;
    }

    if (![email validateEmail]) {
        [Toast showToast:@"Введите правильный email"];
        return;
    }

    if (!self.confirmSwitch.isOn) {
        [Toast showToast:@"Согласитесь с правилами"];
        return;
    }

    //email validated
    [MBProgressHUD showAstanaHUDWithTitle:@"Подготовка к изменению" animated:YES];
    [ProfileApi updateEmail:email
                    success:^(id response) {
                        [MBProgressHUD hideAstanaHUDAnimated:YES];

                        if (response[@"serviceLogId"]) {
                            ChangeEmailConfirmViewController *v = [ChangeEmailConfirmViewController createVC];
                            v.serviceLogId = response[@"serviceLogId"];
                            v.neuEmailStr = email;
                            [self.popupController pushViewController:v animated:YES];
                        }

                    } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
            }];
}

- (void)proceedWithPhone:(NSString *)phone {
    //validate phone
    if ([[User sharedInstance].phoneNumber isEqualToString:phone]) {
        [Toast showToast:@"Введите новый номер"];
    }

    if (![phone validatePhoneNumber]) {
        [Toast showToast:@"Введите правильный номер"];
        return;
    }

    if (!self.confirmSwitch.isOn) {
        [Toast showToast:@"Согласитесь с правилами"];
        return;
    }

    [MBProgressHUD showAstanaHUDWithTitle:@"Подготовка к изменению" animated:YES];
    [ProfileApi updatePhone:phone
               isSmsToEmail:self.sendToEmailInsteadOfOldNumberSwitch.isOn
                    success:^(id response) {
                        [MBProgressHUD hideAstanaHUDAnimated:YES];

                        if (response[@"serviceLogId"]) {
                            ChangePhoneConfirmViewController *v = [ChangePhoneConfirmViewController createVC];
                            v.serviceLogId = response[@"serviceLogId"];
                            v.neuPhoneStr = phone;
                            v.isEmailInsteadOfPhone = self.sendToEmailInsteadOfOldNumberSwitch.isOn;
                            [self.popupController pushViewController:v animated:YES];
                        }

            } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
            }];
}

#pragma mark - config ui

- (void)configUI {
    self.nextButton.mainButtonStyle = MainButtonStyleDisable;
    self.neuloginTextField.separatorHidden = NO;
    
    if (self.isPhone) {
        self.neuloginTextField.maxCharacters = 10;
    }


    if (!self.isPhone) {
        self.titleLabel.text = @"Текущая почта";
        self.sendToEmailInstedOfOldNumberLabel.hidden = YES;
        self.sendToEmailInsteadOfOldNumberSwitch.hidden = YES;
        
        self.oldLoginLabel.text = [User sharedInstance].email;
        
        self.neuloginTextField.keyboardType = UIKeyboardTypeEmailAddress;
        self.neuloginTextField.placeholder = @"Новая почта";

        self.disclamerLabel.text = @"Внимание!\n"
                "При смене адреса электронной почты изменится:\n"
                "• Логин для входа;\n"
                "• адрес получения уведомлений в виде ссылок на восстановление пароля, активации учетной записи, коды подтверждения;\n"
                "• Идентификация клиента при обращении в Контактный центр Банка, а также обратная связь с вами.";
    } else {
        self.oldLoginLabel.text = [User sharedInstance].phoneNumber;
        self.neuloginTextField.keyboardType = UIKeyboardTypePhonePad;
        self.neuloginTextField.placeholder = @"Новый номер";

        self.disclamerLabel.text = @"Внимание!\n"
                "При смене номера телефона изменится:\n"
                "• Логин для входа;\n"
                "• Номер получения уведомлений в виде смс-кодов, 3D secure и установка ПИН-кода;\n"
                "• Идентификация клиента при обращении в Контактный центр Банка, а также обратная связь с вами.";
    }
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    NSLog(@"ChangePhoneEmailViewController dealloc");
}

@end
