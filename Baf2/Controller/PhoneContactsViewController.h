//
//  PhoneContactsViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 14.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneContactsViewController : UIViewController

@property (nonatomic, copy) void (^didSelectContact)(NSString *contactFullName, NSString *contactNumber);

@end
