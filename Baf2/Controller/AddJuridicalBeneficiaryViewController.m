//
//  AddJuridicalBeneficiaryViewController.m
//  BAF
//
//  Created by Almas Adilbek on 8/20/15.
//
//

#import "AddJuridicalBeneficiaryViewController.h"
#import "DropdownSuperTextFieldView.h"
#import "IbanSuperTextFieldView.h"
#import "MainButton.h"
#import "SuperTextFieldView+Validation.h"
#import "BankInfo.h"
#import "TcInfo.h"
#import "NotificationCenterHelper.h"
#import "UIView+Ext.h"
#import "UIView+AAPureLayout.h"
#import "IinSuperTextFieldView.h"
#import "NSString+Ext.h"
#import "AccountsApi.h"
#import "CatalogApi.h"
#import "EconomicSector.h"
#import "InfoApi.h"
#import "TwoCheckBoxesSegmentedControl.h"

@interface AddJuridicalBeneficiaryViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet IinSuperTextFieldView *binField;
@property (weak, nonatomic) IBOutlet DropdownSuperTextFieldView *economicSectorDropdown;
@property (weak, nonatomic) IBOutlet IbanSuperTextFieldView *ibanField;
@property (weak, nonatomic) IBOutlet UILabel *bankNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *bankBiсLabel;
@property (weak, nonatomic) IBOutlet MainButton *submitButton;
@property (weak, nonatomic) IBOutlet SuperTextFieldView *companyNameSuperTextView;
@property (weak, nonatomic) IBOutlet UILabel *ibanErrorLabel;
@property (weak, nonatomic) IBOutlet TwoCheckBoxesSegmentedControl *residentSegmentedControl;

// Constraints.
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ibanErrorLabelTopConstraint;

@end

@implementation AddJuridicalBeneficiaryViewController {
    BOOL companyNameLoading;
    BOOL bankInfoLoading;

    BankInfo *_bankInfo;
    TcInfo *_tcInfo;

    CGFloat ibanErrorLabelTopConstraintConstant;
    NSLayoutConstraint *ibanErrorLabelHeightConstraint;
}

+ (instancetype)createVC
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"AmongBanksTransfer" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AddJuridicalBeneficiaryViewController class])];
}

-(void)initVars {
    ibanErrorLabelTopConstraintConstant = self.ibanErrorLabelTopConstraint.constant;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initVars];
    [self configUI];

    [self loadEconomySectors];

    __weak AddJuridicalBeneficiaryViewController * weakSelf = self;
    [_binField onValueChanged:^(NSString *value) {
        [weakSelf loadCompanyInfo];
    }];

    [_ibanField onValueChanged:^(NSString *value) {
        [weakSelf loadBankInfo];
    }];

    [_companyNameSuperTextView onValueChange:^(NSString *value) {
        if (value.length > 0 && value.length < 50) {
            weakSelf.companyNameSuperTextView.hintLabel.text = @"";
            weakSelf.submitButton.mainButtonStyle = MainButtonStyleOrange;
        } else {
            weakSelf.companyNameSuperTextView.hintLabel.text = NSLocalizedString(@"specify_short_name", nil);
            weakSelf.submitButton.mainButtonStyle = MainButtonStyleDisable;
        }
    }];
}

#pragma mark -
#pragma mark Load

- (void)loadCompanyInfo {
    __weak AddJuridicalBeneficiaryViewController *weakSelf = self;

    if ([[self.binField value] isEqualToString:@"080540014021"]) {
        [Toast showToast:NSLocalizedString(@"incorrect_bin", nil)];
        return;
    }

    if([[_binField value] isEmpty]) {
        [_companyNameSuperTextView clear];
        return;
    }

    [self performBlock:^
    {
        if([weakSelf.binField validateBIN:NO])
        {
            companyNameLoading = YES;

            [weakSelf.companyNameSuperTextView setValue:NSLocalizedString(@"loading", nil)];

            [InfoApi clientInfo:self.binField.value success:^(id response) {
                _tcInfo = response;

                [weakSelf.companyNameSuperTextView setValue:response[@"companyName"]];

                if (response[@"companyName"] &&
                        (([response[@"companyName"] length] > 0) && ([response[@"companyName"] length] < 50))) {
                    [weakSelf.companyNameSuperTextView disabled];
                    [weakSelf.companyNameSuperTextView removeHint];
                    weakSelf.submitButton.mainButtonStyle = MainButtonStyleOrange;
                } else {
                    [weakSelf.companyNameSuperTextView makeEnabled];
                    [weakSelf.companyNameSuperTextView setError:NSLocalizedString(@"specify_short_name", nil)];
                    weakSelf.submitButton.mainButtonStyle = MainButtonStyleDisable;
                }

                /*if (response[@"isResident"]) {
                    [self.residentSegmentedControl toggleCheckbox:[response[@"isResident"] boolValue] ? 0 : 1];
                }*/

                companyNameLoading = NO;
            } failure:^(NSString *code, NSString *message) {
                _tcInfo = nil;
                [weakSelf.companyNameSuperTextView clear];
                companyNameLoading = NO;
                [weakSelf.companyNameSuperTextView makeEnabled];
            }];
        } else {
            _tcInfo = nil;
            [weakSelf.companyNameSuperTextView clear];
        }
    } afterDelay:0.5];
}

- (void)loadBankInfo
{
    [self hideIbanErrorLabel];
    if([[_ibanField value] isEmpty]) return;

    [self performBlock:^
    {
        if([_ibanField validateIBAN:NO])
        {
            bankInfoLoading = YES;

            self.bankNameLabel.text = NSLocalizedString(@"dialog_loading", nil);
            self.bankBiсLabel.text = self.bankNameLabel.text;

            __weak AddJuridicalBeneficiaryViewController *weakSelf = self;
            
            [InfoApi bankInfo:self.ibanField.value success:^(id response) {
                _bankInfo = response;
                if (response[@"name"] && [response[@"name"] length] > 0) {
                    weakSelf.bankNameLabel.text = response[@"name"];
                } else {
                    weakSelf.bankNameLabel.text = @"-";
                    weakSelf.bankBiсLabel.text = self.bankNameLabel.text;
                }
                if (response[@"bic"] && [response[@"bic"] length] > 0) {
                    weakSelf.bankBiсLabel.text = response[@"bic"];
                } else {
                    weakSelf.bankNameLabel.text = @"-";
                    weakSelf.bankBiсLabel.text = self.bankNameLabel.text;
                }
                bankInfoLoading = NO;
            } failure:^(NSString *code, NSString *message) {
                _bankInfo = nil;
                weakSelf.bankNameLabel.text = @"-";
                weakSelf.bankBiсLabel.text = self.bankNameLabel.text;
                
                weakSelf.ibanErrorLabel.text = message;
                [weakSelf showIbanErrorLabel];
                
                bankInfoLoading = NO;
            }];
        }
    } afterDelay:0.5];
}

- (void)loadEconomySectors
{
    [_economicSectorDropdown startLoading];
    
    [CatalogApi getEconomicSectorsWithSuccess:^(NSArray *economicSectorList) {
        [_economicSectorDropdown stopLoading];
        
        for(EconomicSector *economicSector in economicSectorList) {
            [_economicSectorDropdown addOptionWithId:economicSector.code title:economicSector.economicSectorDescription];
        }
    } failure:^(NSString *code, NSString *message) {
        [_economicSectorDropdown stopLoading];
    }];
}

#pragma mark -
#pragma mark Actions

- (IBAction)tapConfirm:(id)sender
{
    
    if(!self.residentSegmentedControl.segmentItemsChecked)
    {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"is_resident", nil)];
        return;
    }
    
    if(![_binField validateBIN]) return;

    if(companyNameLoading || bankInfoLoading) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"wait_for_loading", nil)];
        return;
    }

    if(![_companyNameSuperTextView isset]) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_organozation_name", nil)];
        [_companyNameSuperTextView focus];
        return;
    }

    if(![_economicSectorDropdown isset]) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"choose_economy_sector", nil)];
        return;
    }
    
    if(!self.residentSegmentedControl.segmentItemsChecked)
    {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"is_resident", nil)];
        return;
    }

    if(![_ibanField validateIBAN]) return;

    if(!_tcInfo) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_correct_bin", nil)];
        return;
    }

    if(!_bankInfo) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_correct_iban", nil)];
        return;
    }


    __weak AddJuridicalBeneficiaryViewController *weakSelf = self;
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"adding", nil) animated:true];
    [AccountsApi saveInterbankBeneficiary:self.binField.value
                                     iban:self.ibanField.value
                                    alias:_companyNameSuperTextView.value
                                firstName:nil
                                 lastName:nil
                              companyName:_companyNameSuperTextView.value
                               isResident:self.residentSegmentedControl.selectedItemIndex == 0
                           economicSector:self.economicSectorDropdown.optionId
                                  success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferCorporateBeneficiarAdded object:response];
        [weakSelf goBack];
        
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark -
#pragma mark Helper

- (void)showIbanErrorLabel
{
    if(self.ibanErrorLabel.hidden) {
        [ibanErrorLabelHeightConstraint autoRemove];
        self.ibanErrorLabelTopConstraint.constant = ibanErrorLabelTopConstraintConstant;
        self.ibanErrorLabel.hidden = NO;
    }
}

- (void)hideIbanErrorLabel {
    if(!self.ibanErrorLabel.hidden) {
        ibanErrorLabelHeightConstraint = [_ibanErrorLabel aa_setHeight:0];
        self.ibanErrorLabelTopConstraint.constant = 0;
        self.ibanErrorLabel.hidden = YES;
    }
}

#pragma mark -

-(void)configUI
{
    [self setBafBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"add_beneficiar_dlg_title", nil)];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.scrollView.backgroundColor = [UIColor clearColor];

    [self.binField setPlaceholder:NSLocalizedString(@"recipient_bin", nil)];
    [self.companyNameSuperTextView setPlaceholder:NSLocalizedString(@"company_name", nil)];
    [self.economicSectorDropdown setPlaceholder:NSLocalizedString(@"cl_j_economy_sector", nil)];

    [self.ibanField setPlaceholder:NSLocalizedString(@"recipient_iban", nil)];
    self.ibanField.textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;

    _companyNameSuperTextView.maxCharacters = 60;
    _companyNameSuperTextView.placeholder = NSLocalizedString(@"organization_name", nil);

    self.bankBiсLabel.text = @"-";
    self.bankNameLabel.text = @"-";

    self.submitButton.mainButtonStyle = MainButtonStyleOrange;
    
    // Initially hide iban error label.
    [self hideIbanErrorLabel];

    self.residentSegmentedControl.items = @[@"Резидент", @"Не резидент"];
    [self.residentSegmentedControl uncheckedItems];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
