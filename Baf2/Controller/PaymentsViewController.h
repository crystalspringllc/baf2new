//
//  PaymentsViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 4/25/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, copy) void (^didChangeSearchText)(NSString *searchText);

@property (nonatomic, strong) NSMutableArray *contracts;

@end