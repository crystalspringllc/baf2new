//
// Created by Mustafin Askar on 08/05/15.
//

#import "SearchContractModalViewController.h"
#import "PaymentProvider.h"
#import "PaymentFormViewController.h"
#import "InputTextField.h"

@interface SearchContractModalViewController() {}
@property (nonatomic, strong) SearchResultsView *searchResultsView;
@end

@implementation SearchContractModalViewController {

}

- (id)init {
    self = [super init];
    if (self) {
        [self initVars];
    }
    return self;
}

- (void)initVars {

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchField focus];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

}

#pragma mark - ----------------------------------
#pragma mark - SearchResultView delegate


-(void)searchResultsView:(SearchResultsView *)searchResultsView providerTapped:(PaymentProvider *)provider
{
    [self searchResultsViewHideKeyboard];

    PaymentFormViewController *vc = [PaymentFormViewController createVC];
    vc.actionType = ActionTypeNewPayment;
    vc.paymentProvider = provider;
//    v.showMenuButton = NO;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)searchResultsViewHideKeyboard {
    [self.searchField.textField resignFirstResponder];
}


#pragma mark - config ui

- (void)configUI {
    __weak SearchContractModalViewController *wSelf = self;
    self.view.backgroundColor = [UIColor whiteColor];

    self.searchField = [[InputTextField alloc] init];
    self.searchField.y = 20;
    self.searchField.x = 10;
    self.searchField.textField.clearButtonMode = UITextFieldViewModeNever;
    [self.searchField setPlaceholder:NSLocalizedString(@"search", nil)];
    [self.searchField onValueChange:^{
        [wSelf.searchResultsView searchOnlyProviderCategories:self.searchField.value];
    }];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.searchField];

    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.height = 40;
    closeButton.width = 40;
    closeButton.backgroundColor = [UIColor clearColor];
    [closeButton setImage:[UIImage imageNamed:@"btn_close_normal"] forState:UIControlStateNormal];
    [closeButton setImage:[UIImage imageNamed:@"btn_close_pressed"] forState:UIControlStateHighlighted];
    [closeButton addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];

    self.searchResultsView = [[SearchResultsView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeightWithoutStatusBarAndNavigationBar])];
    self.searchResultsView.delegate = self;
    [self.view addSubview:self.searchResultsView];
}

- (void)closeView {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {

}

@end
