//
//  TransfersTemplatesViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 15.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransfersTemplatesViewController : UIViewController

@property (nonatomic, weak) NSArray *transferTemplates;

@end
