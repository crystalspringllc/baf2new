//
// Created by Askar Mustafin on 4/8/16.
// Copyright (c) 2016 Банк Астаны. All rights reserved.
//

#import "AlertPopupViewController.h"
#import "AppDelegate.h"
#import <STPopup/STPopup.h>

@interface AlertPopupViewController () {}

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIView *wrapper;

@end

@implementation AlertPopupViewController {}

- (id)init {
    self = [super init];
    if(self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PaymentCardRegistrationPopup" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AlertPopupViewController class])];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth] - 10, 250);
    self.landscapeContentSizeInPopup = CGSizeMake([ASize screenWidth], 250);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
//    [[UIHelper appDelegate] hideTabbar:NO];
}

#pragma mark - config ui

- (void)configUI {
    self.titleLabel.text = self.titleString;
    self.imageView.image = self.image;
    self.messageLabel.text = self.textString;
    [self.nextButton setTitle:self.buttonTitle forState:UIControlStateNormal];
    [self.nextButton addTarget:self action:@selector(clickOnNextButton) forControlEvents:UIControlEventTouchUpInside];

    [self.wrapper setNeedsLayout];
    [self.wrapper layoutIfNeeded];

    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], self.wrapper.height);
}

#pragma mark - config action

- (void)clickOnNextButton {
    if (self.OnButtonClick) {
        [self.popupController dismissWithCompletion:^{
            self.OnButtonClick();
        }];
    }
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
//    [[UIHelper appDelegate] showTabbar:NO];
}

@end
