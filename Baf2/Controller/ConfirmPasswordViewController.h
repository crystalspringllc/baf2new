//
// Created by Askar Mustafin on 6/21/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ConfirmPasswordViewController : UIViewController

+ (instancetype)createVC;

@property (nonatomic, copy) void (^onCorfirmTapped)(NSString *password);

@end