//
//  CategoryProvidersListViewController.h
//  Myth
//
//  Created by Almas Adilbek on 8/27/12.
//
//

#import <UIKit/UIKit.h>

@interface CategoryProvidersListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
    NSArray *paymentProviders;
    BOOL showMenuButton;
}

@property (nonatomic, strong) NSArray *paymentProviders;
@property (nonatomic, assign) BOOL showMenuButton;
@property (nonatomic) NSInteger providerCategoryId;

@end
