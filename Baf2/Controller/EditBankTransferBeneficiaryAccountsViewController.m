//
//  EditBankTransferBeneficiaryAccountsViewController.m
//  BAF
//
//  Created by Almas Adilbek on 7/23/15.
//
//

#import "EditBankTransferBeneficiaryAccountsViewController.h"
#import "UIViewController+Extension.h"
#import "Beneficiary.h"
#import "AccountsApi.h"
#import "NotificationCenterHelper.h"
#import "MBProgressHUD+AstanaView.h"
#import <UIScrollView+EmptyDataSet.h>

@interface EditBankTransferBeneficiaryAccountsViewController () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (weak, nonatomic) IBOutlet UITableView *list;
@end

@implementation EditBankTransferBeneficiaryAccountsViewController {
    BOOL edited;
}

- (id)init
{
    self = [super init];
    if(self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Transfers" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([EditBankTransferBeneficiaryAccountsViewController class])];

        [self initVars];
    }
    return self;
}


-(void)initVars {}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.list setEditing:YES animated:YES];
}

- (void)goBack {
    [super goBack];
    
    if (edited) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserUpdatedBeneficiaryAccount object:nil];
    }
}

#pragma mark -
#pragma mark UITableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _beneficiaryAccounts.count;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (_beneficiaryAccounts.count == 0) {
        return nil;
    }
    return NSLocalizedString(@"accounts_receivers", nil);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont helveticaNeue:14];
    }
    
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    cell.textLabel.backgroundColor = [UIColor clearColor];

    Beneficiary *account = _beneficiaryAccounts[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", account.alias, account.currency];

    return cell;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    Beneficiary *account = _beneficiaryAccounts[indexPath.row];

    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"deletion", nil) animated:true];

    [AccountsApi disableBeneficiary:true beneficiaryId:account.accountId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        NSMutableArray * mutableAccounts = [NSMutableArray arrayWithArray:_beneficiaryAccounts];
        [mutableAccounts removeObject:account];
        self.beneficiaryAccounts = mutableAccounts;
        
        [self.list deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
        
        edited = true;
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"no_accounts", nil) attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14], NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
}

#pragma mark -

-(void)configUI
{
    [self setBafBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"edit_accounts", nil)];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.list.backgroundColor = [UIColor clearColor];
    self.list.backgroundView = nil;
    self.list.emptyDataSetSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"EditBankTransferBeneficiaryAccountsVC dealloc");
}

@end