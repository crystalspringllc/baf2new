//
//  OpportunitiesViewController.m
//  Baf2
//
//  Created by nmaksut on 27.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "OpportunitiesViewController.h"
#import "OpportunitiesCollectionCell.h"
#import "MainButton.h"
#import "NotificationCenterHelper.h"

@interface OpportunitiesViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet MainButton *becomeClientButton;

@end

@implementation OpportunitiesViewController

#pragma mark - init

- (instancetype)init
{
    self = [super init];
    if (self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Opportunities" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    }
    return self;
}

#pragma mark - lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - actions

- (IBAction)becomeClient:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMenuOpenBecomeClient object:nil];
}

- (IBAction)loginTapped:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMenuOpenLogin object:nil];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    OpportunitiesCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    switch (indexPath.item) {
        case 0: {
            cell.titleLabel.text = NSLocalizedString(@"organize_your_accounts_from_your_mobile_device", nil);
            cell.backgroundImageView.image = [UIImage imageNamed:@"banner-accounts"];
            break;
        }
        case 1: {
            cell.titleLabel.text = NSLocalizedString(@"opportunity_2", nil);
            cell.backgroundImageView.image = [UIImage imageNamed:@"banner-pays"];
            break;
        }
        case 2: {
            cell.titleLabel.text = NSLocalizedString(@"opportunity_3", nil);
            cell.backgroundImageView.image = [UIImage imageNamed:@"banner-transfers"];
            break;
        }
        default:
            break;
    }
    
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize cellSize = CGSizeMake([ASize screenWidth], collectionView.height);
//    CGSize cellSize = CGSizeMake(145, 180);
    return cellSize;
}

#pragma mark - configUI

- (void)configUI {
    [self showMenuButton];
    [self showBafLogo];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;

    [self.loginButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [self.loginButton setTitleColor:[UIColor fromRGB:0x146888] forState:UIControlStateNormal];
    [self.loginButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [self.loginButton addTarget:self action:@selector(loginTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    self.loginButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"login_registration", nil) attributes:underlineAttribute];
        
    // Default
    self.loginButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.loginButton setTitleColor:[UIColor fromRGB:0x58648e] forState:UIControlStateNormal];
    
    [self.becomeClientButton setMainButtonStyle:MainButtonStyleOrange];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
