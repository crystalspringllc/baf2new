//
//  ActionSheetListPopupViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 4/28/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <AFNetworking/UIImageView+AFNetworking.h>
#import "ActionSheetListPopupViewController.h"
#import "ActionSheetListTableViewCell.h"
#import "Card.h"
#import <STPopup/STPopup.h>
#import <UIScrollView+EmptyDataSet.h>

@implementation ActionSheetListRowObject

@end


@interface ActionSheetListPopupViewController ()<UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource>
@end

@implementation ActionSheetListPopupViewController

- (id)init {
    self = [super init];
    if (self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ActionSheetListPopup" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ActionSheetListPopupViewController class])];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 308);
    self.landscapeContentSizeInPopup = CGSizeMake([ASize screenWidth], 144);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    return [[NSAttributedString alloc] initWithString:(self.emptyDataText ?: NSLocalizedString(@"no_data", nil))
                                           attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                                   NSFontAttributeName: [UIFont systemFontOfSize:14]}];
}

#pragma mark - config table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!self.numberOfSections) {
        return 1;
    }
    
    return self.numberOfSections();
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.numberOfRowsInSection(section);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.imageView.width = 30;
    cell.imageView.height = 30;
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ActionSheetList";
    ActionSheetListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.subtitle2Label.text = @"";
    cell.subtitleLabel.text = @"";

    ActionSheetListRowObject *actionSheetListRawObject = self.rowForIndexPath(indexPath);

    
    if ([actionSheetListRawObject.keepObject isKindOfClass:[Card class]] && !actionSheetListRawObject.title) {
        Card *bankCard = actionSheetListRawObject.keepObject;
        cell.titleLabel.text = bankCard.number;
        cell.subtitleLabel.text = [bankCard balanceAvailableWithCurrency];
    } else if ([actionSheetListRawObject.keepObject isKindOfClass:[NSDictionary class]]) {
        cell.titleLabel.text = actionSheetListRawObject.title;
        NSDictionary *d = actionSheetListRawObject.keepObject;
        if ([d[@"id"] isKindOfClass:[Card class]]) {
            Card *bankCard = d[@"id"];
            cell.subtitle2Label.text = bankCard.number;
        }
    } else {
        cell.titleLabel.text = actionSheetListRawObject.title;

        if ([actionSheetListRawObject.keepObject isKindOfClass:[Card class]]) {
            Card *bankCard = actionSheetListRawObject.keepObject;
            cell.subtitle2Label.text = bankCard.number;
        } else {
            cell.subtitleLabel.text = @"";
        }
    }

    if (actionSheetListRawObject.imageName) {

        [cell.logoImageView setImage:[[UIImage imageNamed:actionSheetListRawObject.imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    } else if (actionSheetListRawObject.imageUrl) {
        [cell.logoImageView setImageWithURL:[NSURL URLWithString:actionSheetListRawObject.imageUrl] placeholderImage:nil];
    } else {
        [cell hideImageView];
    }
    cell.logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    if (actionSheetListRawObject.tintColor) {
        cell.logoImageView.tintColor = actionSheetListRawObject.tintColor;
    }
    
    if (![self.checkmarkDisabledSections containsIndex:indexPath.section]) {
        if (self.selectedIndexPath && self.selectedIndexPath.section == indexPath.section && self.selectedIndexPath.row == indexPath.row) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }

    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (!self.titleForSection) {
        return nil;
    }
    
    return self.titleForSection(section);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (!self.titleForSection || !self.titleForSection(section)) {
        return nil;
    }
    
    UIView *headerView = [UIView new];
    headerView.backgroundColor = [[UIColor fromRGB:0xffffff] colorWithAlphaComponent:0.8];
    
    NSString *headerTitle = self.titleForSection(section);
    
    UILabel *headerTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 8, [ASize screenWidth] - 2 * 15, 20)];
    headerTitleLabel.font = [UIFont boldSystemFontOfSize:14];
    headerTitleLabel.textColor = [UIColor fromRGB:0x146888];
    headerTitleLabel.text = headerTitle;
    [headerView addSubview:headerTitleLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (!self.titleForSection || !self.titleForSection(section)) {
        return 0.001;
    }
    
    return kActionSheetListHeaderHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    __weak ActionSheetListPopupViewController *wSelf = self;

    if (![self.checkmarkDisabledSections containsIndex:indexPath.section]) {
        for (int i = 0; i < [self.tableView numberOfSections]; i++) {
            for (int j = 0; j < [self.tableView numberOfRowsInSection:i]; j++) {
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:j inSection:i]];
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        self.selectedIndexPath = indexPath;
        
        [self.tableView reloadData];
    }
    
    [self dismissViewControllerAnimated:YES completion:^{

        if (wSelf.onSelectObject) {
            ActionSheetListRowObject *actionSheetListRawObject = wSelf.rowForIndexPath(indexPath);
            wSelf.onSelectObject(actionSheetListRawObject.keepObject, indexPath);
        }

    }];
}

#pragma mark - config ui

- (void)configUI {
    self.tableView.tableFooterView = [UIView new];
    self.tableView.emptyDataSetSource = self;
    self.tableView.estimatedRowHeight = 66;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    CGFloat headerHeights = 0;
    
    NSInteger numberOfRows = 0;
    for (int i = 0; i < (self.numberOfSections ? self.numberOfSections() : 1); i++) {
        for (int j = 0; j < self.numberOfRowsInSection(i); j++) {
            numberOfRows++;
        }
        
        if (self.titleForSection && self.titleForSection(i)) {
            headerHeights += kActionSheetListHeaderHeight;
        }
    }
    
    if (numberOfRows < 7) {
        CGFloat viewHeight = numberOfRows * kActionSheetListCellHeight + headerHeights;
        if (viewHeight == 0) {
            viewHeight = 80;
        }
        self.contentSizeInPopup = CGSizeMake([ASize screenWidth], viewHeight);
        self.landscapeContentSizeInPopup = CGSizeMake([ASize screenWidth], 144);
    } else {
        self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 308);
        self.landscapeContentSizeInPopup = CGSizeMake([ASize screenWidth], 144);
    }
    
    [self.tableView reloadData];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end