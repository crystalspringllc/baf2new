//
//  RegistrationViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 11.04.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationViewController : UIViewController

@property (nonatomic, strong) NSString *phone;
@property (nonatomic, assign) BOOL fromNews;

@end
