//
//  TickerViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 7/19/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "TickerViewController.h"
#import "TickersApi.h"
#import "Ticker.h"
#import "TickerBannerView.h"
#import "PaymentReceiptRowView.h"
#import "StatusLabel.h"

@interface TickerViewController ()
@end

@implementation TickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadTickers];

    self.view.backgroundColor = [UIColor whiteColor];
    [self setBafBackground];
    [self setNavigationTitle:@"Акции Банка"];
    [self hideBackButtonTitle];
    [self showMenuButton];
}

#pragma mark - config requests

- (void)loadTickers {
    [MBProgressHUD showAstanaHUDWithTitle:@"Загрузка котировок" animated:YES];
    [TickersApi getTickersSuccess:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        if (response != nil && [response isKindOfClass:[Ticker class]]) {
            Ticker *ticker = response;
            [self configUI:ticker];
        }
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [self showNoDataLabel];
    }];
}

#pragma mark - config ui

- (void)showNoDataLabel {
    UILabel *noDataLabel = [[UILabel alloc] init];
    noDataLabel.width = [ASize screenWidth] - 40;
    noDataLabel.numberOfLines = 0;
    noDataLabel.lineBreakMode = NSLineBreakByWordWrapping;
    noDataLabel.text = @"Нет данных";
    noDataLabel.textColor = [UIColor grayColor];
    [noDataLabel sizeToFit];
    noDataLabel.centerX = self.view.centerX;
    [self.view addSubview:noDataLabel];
}

- (void)configUI:(Ticker *)ticker {
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.height = ASize.screenHeight - 64;
    scrollView.width = ASize.screenWidth;
    [self.view addSubview:scrollView];

    TickerBannerView *tbv = [TickerBannerView configuredView];
    [tbv setLightStyle];
    tbv.centerX = self.view.centerX;
    tbv.y = 10;
    [scrollView addSubview:tbv];
    tbv.ticker = ticker;

    CGFloat yOffset = tbv.bottom + 30;

    if (ticker.infokeys.count > 0) {
        for (int i = 0; i < ticker.infokeys.count ; i++) {
            PaymentReceiptRowView *receiptRowView = [[NSBundle mainBundle] loadNibNamed:@"PaymentReceiptRowView" owner:self options:nil].firstObject;
            receiptRowView.backgroundColor = [UIColor clearColor];
            receiptRowView.titleLabel.text = ticker.infokeys[i];
            receiptRowView.valueLabel.text = ticker.info[ticker.infokeys[i]];
            receiptRowView.y = yOffset;
            receiptRowView.x = 15;
            receiptRowView.width = [ASize screenWidth] - 30;
            receiptRowView.height = 44;
            [scrollView addSubview:receiptRowView];
            yOffset = receiptRowView.bottom + 10;
        }
    }

    StatusLabel *warning = [[StatusLabel alloc] initWithType:Warning];
    warning.text = ticker.warning;
    warning.y = yOffset + 20;
    warning.x = 15;
    warning.width = [ASize screenWidth] - 30;
    warning.height = 60;
    warning.textInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    [scrollView addSubview:warning];

    StatusLabel *message = [[StatusLabel alloc] initWithType:Info];
    message.text = ticker.message;
    message.y = warning.bottom + 10;
    message.x = 15;
    message.width = [ASize screenWidth] - 30;
    message.height = 80;
    message.textInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    [scrollView addSubview:message];

    scrollView.contentSizeHeight = message.bottom + 50;
    scrollView.contentSizeWidth = ASize.screenWidth;
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
