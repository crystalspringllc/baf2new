//
//  ScrollViewController.m
//  AstanaKzNationalControl
//
//  Created by Almas Adilbek on 5/15/13.
//  Copyright (c) 2013 Astana.kz. All rights reserved.
//

#import <PureLayout/ALView+PureLayout.h>
#import "ScrollViewController.h"
#import "TitleInputTextField.h"
#import "DatePickerInputTextField.h"
#import "UIView+AAPureLayout.h"

@interface ScrollViewController()
@end

@implementation ScrollViewController {
    float bottomTopY;
    NSMutableArray *textFields;
}

@synthesize contentScrollView;

-(void)initVars {
    bottomTopY = contentScrollView.bounds.size.height - 15;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];

    if(_useAutolayout) {
        self.contentScrollView = [UIScrollView newAutoLayoutView];
        [self.view addSubview:contentScrollView];
        [self.contentScrollView aa_superviewTop:0];
        [self.contentScrollView aa_superviewLeft:0];
        [self.contentScrollView aa_superviewRight:0];
        [self.contentScrollView aa_superviewBottom:0];
    } else {
        CGFloat navHeight = [self.navigationController isNavigationBarHidden]?0:44;
        self.contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeightWithoutStatusBar] - navHeight)];
        [self.view addSubview:contentScrollView];
    }


    contentScrollView.delegate = self;
    contentScrollView.alwaysBounceVertical = YES;
    contentScrollView.alwaysBounceHorizontal = NO;
    contentScrollView.showsVerticalScrollIndicator = NO;
    contentScrollView.showsHorizontalScrollIndicator = NO;
    contentScrollView.backgroundColor = [UIColor whiteColor];
    
    [self initVars];
}

-(void)setContentScrollViewContentHeight:(float)height
{
    CGSize contentSize = contentScrollView.contentSize;
    contentSize.height = height;
    contentScrollView.contentSize = contentSize;
}

-(void)clearContent
{
    if(contentScrollView) {
        for (id subview in contentScrollView.subviews) {
            [subview removeFromSuperview];
        }
    }
    [self initVars];
}

-(float)contentScrollHeight {
    return contentScrollView.frame.size.height;
}

#pragma mark -
#pragma mark UI

-(void)pushViewToBottom:(UIView *)insertView
{
    [self pushViewToBottom:insertView paddingBottom:10];
}

-(void)pushViewToBottom:(UIView *)insertView paddingBottom:(int)paddingBottom
{
    CGRect f = insertView.frame;
    f.origin.y = bottomTopY - f.size.height - paddingBottom;
    insertView.frame = f;
    [contentScrollView addSubview:insertView];
    
    bottomTopY = f.origin.y;
}

-(void)pushButtonToBottom:(UIView *)button {
    [self pushButtonToBottom:button paddingBottom:10];
}

-(void)pushButtonToBottom:(UIView *)button paddingBottom:(int)paddingBottom {
    [self pushViewToBottom:button paddingBottom:paddingBottom];
    
    CGRect f = button.frame;
    f.origin.x = (CGFloat) ((contentScrollView.bounds.size.width - f.size.width) * 0.5);
    button.frame = f;
}

#pragma mark -
#pragma mark Keyboard

- (void)addKeyboardControlFields:(NSArray *)fields
{
//    __weak ScrollViewController *wSelf = self;
    textFields = [[NSMutableArray alloc] init];

    for(id field in fields)
    {
        if([field isKindOfClass:[InputTextField class]]) {
            __weak InputTextField *textField1 = (InputTextField *)field;
            textField1.textFieldDidBeginEditingBlock = ^(UITextField *textField) {
//                [wSelf.keyboardControls setActiveField:textField];
            };
            UITextField *tf = [(InputTextField *)field textField];
            [textFields addObject:tf];
        } else if([field isKindOfClass:[TitleInputTextField class]]) {
            __weak TitleInputTextField *textField1 = (TitleInputTextField *)field;
            textField1.textFieldDidBeginEditingBlock = ^(UITextField *textField) {
//                [wSelf.keyboardControls setActiveField:textField];
            };
            UITextField *tf = [(TitleInputTextField *)field textField];
            [textFields addObject:tf];
        } if([field isKindOfClass:[DatePickerInputTextField class]]) {
            __weak DatePickerInputTextField *textField1 = (DatePickerInputTextField *)field;
            textField1.textFieldDidBeginEditingBlock = ^(UITextField *textField) {
//                [wSelf.keyboardControls setActiveField:textField];
            };
            UITextField *tf = [(DatePickerInputTextField *)field textField];
            [textFields addObject:tf];
        }  else if([field isKindOfClass:[UITextField class]]) {
            UITextField *tf = field;
            tf.delegate = self;
            [textFields addObject:tf];
        } else {
            continue;
        }
    }

//    if(self.keyboardControls) {
//        self.keyboardControls = nil;
//    }
//    self.keyboardControls = [[BSKeyboardControls alloc] initWithFields:textFields];
//    self.keyboardControls.delegate = self;
}

#pragma mark -
#pragma mark UITextField
//
- (void)textFieldDidBeginEditing:(UITextField *)textField {
//    [self.keyboardControls setActiveField:textField];
}


#pragma mark -
#pragma mark UIScrollView

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setContentScrollView:nil];
}
@end
