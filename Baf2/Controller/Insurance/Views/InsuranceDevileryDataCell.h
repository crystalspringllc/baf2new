//
// Created by Askar Mustafin on 11/29/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FloatingTextField;


@interface InsuranceDevileryDataCell : UITableViewCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@property (nonatomic, strong) FloatingTextField *firstname;
@property (nonatomic, strong) FloatingTextField *lastname;
@property (nonatomic, strong) FloatingTextField *middlename;
@property (nonatomic, strong) FloatingTextField *email;
@property (nonatomic, strong) FloatingTextField *phone;
@property (nonatomic, strong) FloatingTextField *street;
@property (nonatomic, strong) FloatingTextField *house;
@property (nonatomic, strong) FloatingTextField *flat;

- (BOOL)isValid;

@end