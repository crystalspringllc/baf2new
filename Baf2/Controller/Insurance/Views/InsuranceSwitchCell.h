//
// Created by Askar Mustafin on 11/21/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface InsuranceSwitchCell : UITableViewCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;
@property (nonatomic, copy) void (^onSwitchHasChanged)(BOOL isOn);


@end