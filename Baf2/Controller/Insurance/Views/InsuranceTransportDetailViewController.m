//
// Created by Askar Mustafin on 11/23/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceTransportDetailViewController.h"
#import "UITableViewController+Extension.h"


@implementation InsuranceTransportDetailViewController {}

- (id)init {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - config action

- (void)deleteContract:(id)sender {
    if (self.onTransportDelete) {
        self.onTransportDelete(self.transport);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Заполните все поля";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"newFriendCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }

    if (indexPath.row == 0) {
        cell.textLabel.text = self.transport.area.title ? self.transport.area.title : @"Регион регистрации";
        cell.textLabel.textColor = self.transport.area.title ?  [UIColor blackColor] : [UIColor grayColor];
    } else if (indexPath.row == 1) {
        cell.textLabel.text = self.transport.type.title ? self.transport.type.title : @"Тип";
        cell.textLabel.textColor = self.transport.type.title ?  [UIColor blackColor] : [UIColor grayColor];
    } else if (indexPath.row == 2) {
        cell.textLabel.text = self.transport.lifeTime.title ? self.transport.lifeTime.title : @"Срок эксплуатации";
        cell.textLabel.textColor = self.transport.lifeTime.title ?  [UIColor blackColor] : [UIColor grayColor];
    }

    //etc.
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    InsuranceSelectCatalogViewController *v;
    if (indexPath.row == 0) {
        v = [[InsuranceSelectCatalogViewController alloc] initWithType:CatalogTypeRegistrationTerritory];
    } else if (indexPath.row == 1) {
        v = [[InsuranceSelectCatalogViewController alloc] initWithType:CatalogTypeTransportType];
    } else if (indexPath.row == 2) {
        v = [[InsuranceSelectCatalogViewController alloc] initWithType:CatalogTypeTransportUsageTime];
    }

    v.delegate = self;

    [self.navigationController pushViewController:v animated:YES];
}

#pragma mark - InsuranceSelectCatalogDelegate

- (void)didSelectCatalogEntity:(InsuranceCatalogEntity *)catalogEntity catalogType:(CatalogType)catalogType {
    if (catalogType == CatalogTypeRegistrationTerritory) {
        self.transport.area = catalogEntity;
    } else if (catalogType == CatalogTypeTransportType) {
        self.transport.type = catalogEntity;
    } else if (catalogType == CatalogTypeTransportUsageTime) {
        self.transport.lifeTime = catalogEntity;
    }
    [self.tableView reloadData];
}

#pragma mark - config ui

- (void)configUI {
    [self setNavigationTitle:@"Cтрахование"];
    [self setBAFTableViewBackground];
    [self setNavigationBackButton];
    
    if (self.isAbleDelete) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteContract:)];
    }
}

@end
