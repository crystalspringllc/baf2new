//
// Created by Askar Mustafin on 11/22/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class FloatingTextField;

@interface FloatingTextFieldCell : UITableViewCell
- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;
@property(nonatomic, strong) FloatingTextField *iin;
@end