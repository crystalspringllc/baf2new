//
// Created by Askar Mustafin on 11/22/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceDriverDetailViewController.h"
#import "FloatingTextFieldCell.h"
#import "FloatingTextField.h"
#import "BonusMalus.h"
#import "InsuranceDriver.h"
#import "UITableViewController+Extension.h"

@implementation InsuranceDriverDetailViewController {}

- (id)init {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - config actions

- (void)onIINHasEntered:(NSString *)iin {
    self.driver.iin = iin;
    if (iin.length == 12) {
        [MBProgressHUD showAstanaHUDWithTitle:@"Загрузка" animated:YES];
        [InsuranceApi bonusmalusByIIN:iin success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            self.driver.bonusMalus = [BonusMalus instanceFromDictionary:response];
            [self.tableView reloadData];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
        }];
    }
}

- (void)deleteContract:(id)sender {
    if (self.onDriverDelete) {
        self.onDriverDelete(self.driver);
    }
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - tableview

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak InsuranceDriverDetailViewController *wSelf = self;

    id cellToReturn = nil;

    if (indexPath.row == 0) {
        static NSString *CellIdentifier = @"IINCell";
        FloatingTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[FloatingTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.iin.placeholder = @"Введите ИИН";
        cell.iin.keyboardType = UIKeyboardTypeNumberPad;
        [cell.iin onValueChanged:^(NSString *value) {
             [wSelf onIINHasEntered:value];
        }];
        cell.iin.value = self.driver.iin;

        if ([AppUtils isAskar] && IS_API_URL_TEST) {
            cell.iin.value = @"910918300477";
        }

        cellToReturn = cell;
    }

    else if (indexPath.row == 1) {
        static NSString *CellIdentifier = @"BonusMalus";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }

        cell.textLabel.text = self.driver.bonusMalus ? self.driver.bonusMalus.bonusMalus : @"Класс \"бонус-малус\"";

        cellToReturn = cell;
    }

    else if (indexPath.row == 2) {
        static NSString *CellIdentifier = @"BonusMalus";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }

        cell.textLabel.text = self.driver.ageExperience.title ? self.driver.ageExperience.title : @"Возраст и стаж вождения";
        cell.textLabel.textColor = self.driver.ageExperience.title ?  [UIColor blackColor] : [UIColor grayColor];

        cellToReturn = cell;
    }

    return cellToReturn;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 2) {
        InsuranceSelectCatalogViewController *v = [[InsuranceSelectCatalogViewController alloc] initWithType:CatalogTypeDrivingExperience];
        v.delegate = self;
        [self.navigationController pushViewController:v animated:YES];
    }
}

#pragma mark - InsuranceSelectCatalogDelegate

- (void)didSelectCatalogEntity:(InsuranceCatalogEntity *)catalogEntity catalogType:(CatalogType)catalogType {
    if (catalogType == CatalogTypeDrivingExperience) {
        self.driver.ageExperience = catalogEntity;
    }
    [self.tableView reloadData];
}

#pragma mark - config

- (void)configUI {
    [self setNavigationTitle:@"Cтрахование"];
    [self setBAFTableViewBackground];
    [self setNavigationBackButton];
    
    if (self.isAbleDelete) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteContract:)];
    }

    if ([AppUtils isAskar] && IS_API_URL_TEST) {
        [self.tableView reloadData];
        [self onIINHasEntered:@"910918300477"];
    }

}

#pragma mark -

- (void)dealloc {
    NSLog(@"InsuranceDriverDetailVC delloc");
}

@end
