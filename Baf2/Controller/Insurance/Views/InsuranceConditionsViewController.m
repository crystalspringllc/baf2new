//
// Created by Askar Mustafin on 11/28/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceConditionsViewController.h"


@implementation InsuranceConditionsViewController {

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

- (void)configUI {
    [self setBafBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:@"Условия страховки"];

    UIScrollView *v = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0,[ASize screenWidth], [ASize screenHeightWithoutStatusBarAndNavigationBar])];
    v.backgroundColor = [UIColor clearColor];
    v.alwaysBounceVertical = YES;
    [self.view addSubview:v];


    UILabel *cond = [[UILabel alloc] init];
    cond.frame = CGRectMake(20,20, [ASize screenWidth] - 40, 1);
    cond.numberOfLines = 0;
    cond.lineBreakMode = NSLineBreakByWordWrapping;
    cond.text = @"Что можно застраховать по данному виду страхования?\n\n"
            "Легковые машины, грузовые автомобили, автобусы, микроавтобусы, мототранспорт, прицепы (полуприцепы), трамваи, троллейбусы;\n"
            "Транспортное средство, временно въехавшее на территорию Республики Казахстан\n"
            "\t\n"
            "От каких рисков можно застраховать?\n\n"
            "Вы страхуете свою Гражданско-правовую ответственность перед участниками дорожного движения, в случае вашей вины.\n"
            "\t\n"
            "На какую сумму можно застраховать ответственность автовладельца?\n\n"
            "На сумму фактического ущерба, но не более 600 МРП (согласно Законодательству Республики Казахстан).\n"
            "\t\n"
            "Что не принимается на страхование?\n\n"
            "Автомобиль, незарегистрированный в органах дорожной полиции РК (за исключением временного въезда)";
    [cond sizeToFit];
    [v addSubview:cond];

    v.contentSize = CGSizeMake([ASize screenWidth], cond.bottom + 40);
}

@end