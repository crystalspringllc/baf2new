//
// Created by on 11/24/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InsuranceCalcResponse.h"
#import "InsuranceSelectCatalogViewController.h"
#import "InsuranceSelectCityViewController.h"

@class MainButton;


@interface InsuranceLoadDocumentsViewController : UITableViewController<UITableViewDelegate, UITableViewDataSource, InsuranceSelectCityDelegate>

@property (nonatomic, strong) InsuranceCalcResponse *insuranceCalcResponse;
@property (nonatomic, strong) NSMutableArray *drivers;
@property (nonatomic, strong) NSMutableArray *transports;
@property (nonatomic, assign) BOOL isVov;
@property (nonatomic, assign) BOOL isPen;
@end