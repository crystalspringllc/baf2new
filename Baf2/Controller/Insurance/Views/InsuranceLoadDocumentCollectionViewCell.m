//
//  InsuranceLoadDocumentCollectionViewCell.m
//  Baf2
//
//  Created by Askar Mustafin on 11/25/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceLoadDocumentCollectionViewCell.h"

@implementation InsuranceLoadDocumentCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (IBAction)clickDeleteButton:(id)sender {
    if (_deleteBlock) self.deleteBlock();
}

@end
