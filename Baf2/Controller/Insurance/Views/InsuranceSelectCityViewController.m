//
// Created by Askar Mustafin on 11/26/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceSelectCityViewController.h"
#import "InsuranceApi.h"
#import "UITableViewController+Extension.h"


@interface InsuranceSelectCityViewController()
@property (nonatomic, strong) NSArray *cities;
@end

@implementation InsuranceSelectCityViewController {}

- (id)init {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {}
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self loadCitiesList];
}

#pragma mark -

- (void)loadCitiesList {
    NSMutableArray *cities = [[NSMutableArray alloc] init];

    NSArray *citiesList = [self parseFromJsonFile:@"cities"];
    for (NSDictionary *c in citiesList) {
        City *city = [City instanceFromDictionary:c];
        [cities addObject:city];
    }
    self.cities = cities;
    [self.tableView reloadData];

//    [MBProgressHUD showAstanaHUDWithTitle:@"Список городов" animated:YES];
//    [InsuranceApi citiesSuccess:^(id response) {
//        [MBProgressHUD hideAstanaHUDAnimated:YES];
//        for (NSDictionary *c in response[@"locations"]) {
//            City *city = [City instanceFromDictionary:c];
//            [cities addObject:city];
//        }
//        self.cities = cities;
//        [self.tableView reloadData];
//    } failure:^(NSString *code, NSString *message) {
//        [MBProgressHUD hideAstanaHUDAnimated:YES];
//    }];
}

#pragma mark - UITableView delegate & datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"newFriendCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    City *city = self.cities[(NSUInteger) indexPath.row];
    cell.textLabel.text = city.name;

    //etc.
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate) {
        City *city = self.cities[(NSUInteger) indexPath.row];
        [self.delegate didSelectCity:city];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -

- (void)configUI {
    [self setNavigationTitle:@"Cтрахование"];
    [self setBAFTableViewBackground];
    [self setNavigationBackButton];
}

@end