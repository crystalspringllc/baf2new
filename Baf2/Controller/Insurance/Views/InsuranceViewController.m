//
// Created by Askar Mustafin on 11/15/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "ChameleonMacros.h"
#import "InsuranceViewController.h"
#import "InsuranceDriverDetailViewController.h"
#import "InsuranceSwitchCell.h"
#import "InsuranceDriver.h"
#import "BonusMalus.h"
#import "InsuranceTransportDetailViewController.h"
#import "InsuranceFooterView.h"
#import "InsuranceCalcResponse.h"
#import "UITableViewController+Extension.h"
#import "InsuranceLoadDocumentsViewController.h"
#import "InsuranceConditionsViewController.h"
#import "NotificationCenterHelper.h"

@interface InsuranceViewController()
@property (nonatomic, strong) NSMutableArray *drivers;
@property (nonatomic, strong) NSMutableArray *transports;
@property (nonatomic, assign) BOOL isVov;
@property (nonatomic, assign) BOOL isPen;

@property (nonatomic, strong) InsuranceCatalogEntity *selecterInsuranceType;
@property (nonatomic, strong) InsuranceCatalogs *insuranceTypes;

@property (nonatomic, strong) InsuranceCalcResponse *insuranceCalcResponse;
@property (nonatomic, strong) InsuranceFooterView *footer;

@end

@implementation InsuranceViewController {}

- (id)init {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        //initially one driver
        self.drivers = [[NSMutableArray alloc] init];
        InsuranceDriver *driver = [[InsuranceDriver alloc] init];
        [self.drivers addObject:driver];

        self.transports = [[NSMutableArray alloc] init];
        InsuranceTransport *transport = [[InsuranceTransport alloc] init];
        [self.transports addObject:transport];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self loadInsuranceTypeCatalog];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.tableView) {
        [self.tableView reloadData];
    }
}

#pragma mark - http requests

- (void)calculateAmount {
    __weak InsuranceViewController *wSelf = self;

    NSMutableArray *insureds = [[NSMutableArray alloc] init];
    NSMutableArray *cars = [[NSMutableArray alloc] init];

    for (InsuranceTransport *c in wSelf.transports) {
        if (c.jsonPrep) {
            [cars addObject:c.jsonPrep];
        }
    }
    for (InsuranceDriver *c in wSelf.drivers) {
        if (c.jsonPrep) {
            [insureds addObject:c.jsonPrep];
        }
    }
    if (insureds.count == 0) {
        [UIHelper showAlertWithMessage:@"Заполните поля о водителе"];
        return;
    }
    if (cars.count == 0) {
        [UIHelper showAlertWithMessage:@"Добавьте"];
    }

    [MBProgressHUD showAstanaHUDWithTitle:@"Расчет суммы" animated:YES];
    [InsuranceApi calcCars:cars insureds:insureds benefitDiscount:wSelf.isPen vovDiscount:wSelf.isVov success:^(id response) {

        wSelf.insuranceCalcResponse = [InsuranceCalcResponse instanceFromDictionary:response];
        [wSelf.footer setAmount:wSelf.insuranceCalcResponse.insuranceAmount];
        [wSelf.tableView reloadData];

        [MBProgressHUD hideAstanaHUDAnimated:YES];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

- (void)loadInsuranceTypeCatalog {
    [MBProgressHUD showAstanaHUDWithTitle:@"Подождите" animated:YES];
    [InsuranceApi catalogsByCategoryType:CatalogTypeInsuranceType success:^(id response) {
        self.insuranceTypes = [InsuranceCatalogs instanceFromDictionary:response[@"catalogsMap"]];
        self.selecterInsuranceType = self.insuranceTypes.insuranceType.firstObject;
        [self.tableView reloadData];
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

#pragma mark - config actions

- (void)onExecuteClick {
    if ([User sharedInstance].loggedIn) {
        InsuranceLoadDocumentsViewController *v = [[InsuranceLoadDocumentsViewController alloc] init];
        v.insuranceCalcResponse = self.insuranceCalcResponse;
        v.drivers = self.drivers;
        v.transports = self.transports;
        v.isPen = self.isPen;
        v.isVov = self.isVov;
        [self.navigationController pushViewController:v animated:YES];
    } else {
        [UIAlertView showWithTitle:@"" message:@"Для оформление полиса, нужно авторизоваться" cancelButtonTitle:@"Отмена"
                 otherButtonTitles:@[@"Аторизоваться"]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                             if (alertView.cancelButtonIndex != buttonIndex) {
                                 UIViewController *rootViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
                                 LGSideMenuController *l = (LGSideMenuController *)rootViewController;
                                 UINavigationController *navigationController = [UIHelper defaultNavigationController];
                                 ViewController *viewController = [ViewController createVC];
                                 [navigationController setViewControllers:@[viewController]];
                                 [l setRootViewController:navigationController];
                                 [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMenuOpenLogin object:nil];
                             }
                          }];
    }
}

- (void)addDriver {
    if (self.drivers.count < 4) {
        InsuranceDriver *driver = [[InsuranceDriver alloc] init];
        [self.drivers addObject:driver];
    }
    [self.tableView reloadData];
}

- (void)addTransport {
    if (self.drivers.count == 1) {
        if (self.transports.count == 1) {
            InsuranceTransport *transport = [[InsuranceTransport alloc] init];
            [self.transports addObject:transport];
        }
    }
    [self.tableView reloadData];
}

- (void)showConditions {
    InsuranceConditionsViewController *v = [[InsuranceConditionsViewController alloc]init];
    [self.navigationController pushViewController:v animated:YES];
}

#pragma mark - UITableview protocol methods

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 5) {
        return 250;
    } else {
        return 1;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    __weak InsuranceViewController *wSelf = self;
    if (section == 5) {
        if (!wSelf.footer) {
            wSelf.footer = [[InsuranceFooterView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.width, 250)];
            wSelf.footer.onCalculateClick = ^() {
                [wSelf calculateAmount];
            };
            wSelf.footer.onExecuteClick = ^{
                [wSelf onExecuteClick];
            };
        }
        return wSelf.footer;
    } else {
        return nil;
    }
}

- (CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 2) {
        return 1;
    } else if (section == 4) {
        return 1;
    }
    return 40.0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"Выбор страховки и условия";
    } else if (section == 1) {
        return @"Водительские данные";
    } else if (section == 3) {
        return @"Транспортные данные";
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 88;
    }
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 2;
    } else if (section == 1) {
        return self.drivers.count; //number of drivers
    } else if (section == 2) {
        return 1; //add driver button
    } else if (section == 3) {
        return self.transports.count;
    } else if (section == 4) {
        return 1; //add transfer button
    } else if (section == 5) {
        return 2; //vov and pension switchers
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak InsuranceViewController *wSelf = self;

    id cellToReturn = nil;

    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"ConditionsCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.numberOfLines = 0;
            cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        }

        if (indexPath.row == 0) {
            cell.textLabel.text = self.selecterInsuranceType.title;
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"Условия страхования";
            cell.textLabel.numberOfLines = 1;
        }

        cellToReturn = cell;

    } else if (indexPath.section == 1) {
        //number of drivers
        static NSString *CellIdentifier = @"DriverCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.detailTextLabel.font = [UIFont systemFontOfSize:10];
            cell.detailTextLabel.textColor = [UIColor grayColor];
        }

        InsuranceDriver *driver = self.drivers[(NSUInteger) indexPath.row];

        if (driver.iin && driver.ageExperience && driver.bonusMalus) {
            cell.textLabel.text = [NSString stringWithFormat:@"ИИН: %@, Бонус-Малус: %@, Cтаж: %@", driver.iin, driver.bonusMalus.bonusMalus, driver.ageExperience.title];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"Данные %iго водителя", indexPath.row + 1];
        } else {
            cell.textLabel.text = [NSString stringWithFormat:@"Данные %iго водителя", indexPath.row + 1];
            cell.detailTextLabel.text = nil;
        }

        cellToReturn = cell;

    } else if (indexPath.section == 2) {
        //add driver button
        static NSString *CellIdentifier = @"AddDriverCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.textColor = [UIColor flatBlueColor];
        cell.textLabel.text = @"Добавить водителя";
        cellToReturn = cell;

    } else if (indexPath.section == 3) {
        //number of transports
        static NSString *CellIdentifier = @"TransportCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.detailTextLabel.font = [UIFont systemFontOfSize:10];
            cell.detailTextLabel.textColor = [UIColor grayColor];
        }

        InsuranceTransport *transport = self.transports[(NSUInteger) indexPath.row];

        if (transport.area && transport.lifeTime && transport.type) {
            cell.textLabel.text = [NSString stringWithFormat:@"Регион: %@, Тип: %@, Срок: %@", transport.area.title, transport.type.title, transport.lifeTime.title];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"Данные %iго транспорта", indexPath.row + 1];
        } else {
            cell.textLabel.text = [NSString stringWithFormat:@"Данные %iго транспорта", indexPath.row + 1];
            cell.detailTextLabel.text = nil;
        }

        cellToReturn = cell;

    } else if (indexPath.section == 4) {
        //add transport button
        static NSString *CellIdentifier = @"AddTransportCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.textColor = [UIColor flatBlueColor];
        cell.textLabel.text = @"Добавить транспорт";
        cellToReturn = cell;
    } else if (indexPath.section == 5) {
        //vov and pension switchers
        static NSString *CellIdentifier = @"VovCell";
        InsuranceSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[InsuranceSwitchCell alloc] initWithReuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont systemFontOfSize:13];
        }

        cell.onSwitchHasChanged = ^(BOOL isOn) {
              if (indexPath.row == 0) {
                  wSelf.isVov = isOn;
              } else {
                  wSelf.isPen = isOn;
              }
        };

        if (indexPath.row == 0) {
            cell.textLabel.text = @"Являетесь ли Вы участником ВОВ?";
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"Являетесь ли Вы пенсионером?";
        }
        cellToReturn = cell;
    }
    return cellToReturn;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak InsuranceViewController *wSelf = self;
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            InsuranceSelectCatalogViewController *v = [[InsuranceSelectCatalogViewController alloc] initWithCatalogs:self.insuranceTypes type:CatalogTypeInsuranceType];
            v.delegate = self;
            [self.navigationController pushViewController:v animated:YES];
        } else {
            [self showConditions];
        }
    } else if (indexPath.section == 1) {
        InsuranceDriver *driver = self.drivers[(NSUInteger) indexPath.row];
        InsuranceDriverDetailViewController *v = [[InsuranceDriverDetailViewController alloc] init];
        v.driver = driver;
        v.isAbleDelete = self.drivers.count > 1;
        v.onDriverDelete = ^void(InsuranceDriver *insuranceDriver) {
            [wSelf removeObject:insuranceDriver inArray:wSelf.drivers];
            [wSelf.tableView reloadData];
        };
        [self.navigationController pushViewController:v animated:YES];
    } else if (indexPath.section == 2) {
        [self addDriver];
    } else if (indexPath.section == 3) {
        InsuranceTransport *transport = self.transports[(NSUInteger) indexPath.row];
        InsuranceTransportDetailViewController *v = [[InsuranceTransportDetailViewController alloc] init];
        v.transport = transport;
        v.isAbleDelete = self.transports.count > 1;
        v.onTransportDelete = ^void(InsuranceTransport *insuranceTransport) {
            [wSelf removeObject:insuranceTransport inArray:wSelf.transports];
            [wSelf.tableView reloadData];
        };
        [self.navigationController pushViewController:v animated:YES];
    } else if (indexPath.section == 4) {
        [self addTransport];
    }
}

#pragma mark - helper func

- (void)removeObject:(id)object inArray:(NSMutableArray *)array {
    id objectToRemove;
    for (id objIterate in array) {
        if ([object isEqual:objIterate]) {
            objectToRemove = objIterate;
        }
    }
    [array removeObject:objectToRemove];
}

#pragma mark - InsuranceSelectCatalogDelegate

- (void)didSelectCatalogEntity:(InsuranceCatalogEntity *)catalogEntity catalogType:(CatalogType)catalogType {
    self.selecterInsuranceType = catalogEntity;
    [self.tableView reloadData];
}

#pragma mark - config ui

- (void)configUI {
    [self setNavigationTitle:@"Cтрахование"];
    [self setBAFTableViewBackground];
    [self setNavigationBackButton];
}

#pragma mark - memmory warnings

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"InsuranceViewController dealloc");
}

@end
