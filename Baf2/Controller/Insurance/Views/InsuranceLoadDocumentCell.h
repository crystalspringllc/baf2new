//
// Created by Askar Mustafin on 11/24/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TOCropViewController.h"


@interface InsuranceLoadDocumentCell : UITableViewCell
        <UICollectionViewDelegate,
        UICollectionViewDataSource,
        UINavigationControllerDelegate,
        UIImagePickerControllerDelegate,
        TOCropViewControllerDelegate>

/**
 * loaded image's urls
 */
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIViewController *vcOwner;

@property (nonatomic, copy) void (^onImageUploaded)(NSArray *uploadedUrls);

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@end