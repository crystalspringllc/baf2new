//
// Created by Askar Mustafin on 11/26/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "City.h"


@protocol InsuranceSelectCityDelegate
- (void)didSelectCity:(City *)city;
@end

@interface InsuranceSelectCityViewController : UITableViewController

@property (nonatomic, weak) id <InsuranceSelectCityDelegate> delegate;

@end