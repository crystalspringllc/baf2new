//
// Created by Askar Mustafin on 11/22/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InsuranceSelectCatalogViewController.h"

@class InsuranceDriver;

@interface InsuranceDriverDetailViewController : UITableViewController <InsuranceSelectCatalogDelegate>

@property (nonatomic, strong) InsuranceDriver *driver;

@property (nonatomic, assign) BOOL isAbleDelete;

@property (nonatomic, copy) void (^onDriverDelete)(InsuranceDriver *driver);

@end
