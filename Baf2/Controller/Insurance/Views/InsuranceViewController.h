//
// Created by Askar Mustafin on 11/15/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "InsuranceSelectCatalogViewController.h"

@interface InsuranceViewController : UITableViewController<UITableViewDelegate, UITableViewDataSource, InsuranceSelectCatalogDelegate>
@end
