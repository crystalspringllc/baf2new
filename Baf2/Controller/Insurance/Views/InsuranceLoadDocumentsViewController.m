//
// Created by on 11/24/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "ChameleonMacros.h"
#import "InsuranceLoadDocumentsViewController.h"
#import "InsuranceLoadDocumentCell.h"
#import "FloatingTextFieldCell.h"
#import "FloatingTextField.h"
#import "MainButton.h"
#import "NSDate+Ext.h"
#import "InsuranceTransport.h"
#import "InsuranceDriver.h"
#import "PaymentFormViewController.h"
#import "ProvidersApi.h"
#import "PaymentProvider.h"
#import "UITableViewController+Extension.h"
#import "PaymentsApi.h"
#import "LastOperationDetailsViewController.h"
#import "InsuranceDevileryDataCell.h"


@interface InsuranceLoadDocumentsViewController()
@property (nonatomic, strong) City *selectedCity;
@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) NSArray *vovDiscountDocs;
@property (nonatomic, strong) NSArray *benefitDiscountDocs;
@property (nonatomic, strong) PaymentProvider *correspondingProvider;
@property (nonatomic, assign) BOOL isPayOnline;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) MainButton *toPaymentButton;
@property (nonatomic, strong) NSNumber *rerviceLogId;
@property (nonatomic, strong) InsuranceDevileryDataCell *deliveryCell;
@property(nonatomic, strong) UISegmentedControl *payTypeSegment;
@end

@implementation InsuranceLoadDocumentsViewController {}

- (id)init {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

    self.isPayOnline = YES;
}

#pragma mark - config actions

- (void)clickPaymentButton {
    __weak InsuranceLoadDocumentsViewController *wSelf = self;

    NSMutableArray *driverJSON = [[NSMutableArray alloc] init];
    NSMutableArray *transportJSON = [[NSMutableArray alloc] init];

    for (InsuranceDriver *d in self.drivers) {
        if (d.jsonPrep) {
            [driverJSON addObject:d.jsonPrep];
        }
    }
    for (InsuranceTransport *t in self.transports) {
        if (t.jsonPrep) {
            [transportJSON addObject:t.jsonPrep];
        }
    }
    NSString *providerCode;
    if (self.isPayOnline) {
        providerCode = @"STD_INSURANCE";
    } else {
        providerCode = @"STD_INSURANCE_BY_CASH";
    }

    if (!self.deliveryCell.isValid) {
        [UIHelper showAlertWithMessage:@"Заполните все поля"];
        return;
    }

    BOOL notValid = NO;
    for (NSDictionary *d in driverJSON) {
        NSArray *drivingLicenseScanDocs = d[@"drivingLicenseScanDocs"];
        NSArray *identityScanDocs = d[@"identityScanDocs"];
        if ( drivingLicenseScanDocs.count == 0 || identityScanDocs.count == 0) {
            notValid = YES;
            break;
        }
    }
    for (NSDictionary *t in transportJSON) {
        NSArray *vehicleIdentityDocs = t[@"vehicleIdentityDocs"];
        if (vehicleIdentityDocs.count == 0) {
            notValid = YES;
            break;
        }
    }
    if (notValid) {
        [UIHelper showAlertWithMessage:@"Загрузите фото документов"];
        return;
    }

    if (!self.selectedDate) {
        [UIHelper showAlertWithMessage:@"Выберите дату доставки"];
        return;
    }

    if (!self.selectedCity) {
        [UIHelper showAlertWithMessage:@"Выберите город"];
        return;
    }


    NSMutableDictionary *ogpoRequest = [[NSMutableDictionary alloc] init];
    ogpoRequest[@"benefitDiscount"] = @(self.isPen);
    ogpoRequest[@"vovDiscount"] = @(self.isVov);
    ogpoRequest[@"cars"] = transportJSON;
    ogpoRequest[@"insureds"] = driverJSON;
    if (self.benefitDiscountDocs.count > 0) ogpoRequest[@"benefitDiscountDocs"] = self.benefitDiscountDocs;
    if (self.vovDiscountDocs.count > 0) ogpoRequest[@"vovDiscountDocs"] = self.vovDiscountDocs;

    [MBProgressHUD showAstanaHUDWithTitle:@"Сохранение" animated:YES];
    [ProvidersApi getProviderByCode:providerCode success:^(id response) {

        wSelf.correspondingProvider = [PaymentProvider instanceFromDictionary:response];

        [InsuranceApi saveWithCars:transportJSON insureds:driverJSON benefitDiscount:self.isPen vovDiscount:self.isVov benefitDiscountDocs:self.benefitDiscountDocs vovDiscountDocs:self.vovDiscountDocs success:^(id response) {


            if (!self.isPayOnline) {
                [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"register_payment", nil) animated:true];

                [PaymentsApi paymentForInsuranceWithAmount:self.insuranceCalcResponse.insuranceAmount
                                      amountWithCommission:self.insuranceCalcResponse.insuranceAmount
                                                    cardId:nil
                                                 acquiring:@""
                                                commission:@0
                                               bonusAmount:@0
                                                 payOnline:NO
                                                providerId:wSelf.correspondingProvider.paymentProviderId
                                                bonusMalus:@([self.insuranceCalcResponse.bonusMalus intValue])
                                              deliveryDate:[self.selectedDate stringWithDateFormat:@"dd.MM.yyyy"]
                                                 firstName:self.deliveryCell.firstname.value
                                                  lastName:self.deliveryCell.lastname.value
                                                middleName:self.deliveryCell.middlename.value
                                                     email:self.deliveryCell.email.value
                                                     phone:self.deliveryCell.phone.value
                                                    cityId: @([self.selectedCity.cityId intValue])
                                                  cityName:self.selectedCity.name
                                               ogpoRequest:ogpoRequest
                                                    street:self.deliveryCell.street.value
                                                      home:self.deliveryCell.house.value
                                                      flat:self.deliveryCell.flat.value
                                                   success:^(id response2) {
                                                       self.rerviceLogId = response2[@"serviceLogId"];

                                                       [InsuranceApi cashRun:self.rerviceLogId
                                                                     success:^(id cashRunResponse) {
                                                                         [MBProgressHUD hideAstanaHUDAnimated:true];

                                                                         [Toast showToast:@"Ваш заказ успешно оформлен, мы свяжемся с Вами"];

                                                                         LastOperationDetailsViewController *v = [[LastOperationDetailsViewController alloc] init];
                                                                         v.operationId = self.rerviceLogId;
                                                                         v.newVersion = NO;
                                                                         [self.navigationController pushViewController:v animated:YES];
                                                                     } failure:^(NSString *code, NSString *message) {
                                                                   [MBProgressHUD hideAstanaHUDAnimated:true];
                                                               }];



                                                   } failure:^(NSString *code, NSString *message) {
                            [MBProgressHUD hideAstanaHUDAnimated:true];
                        }];

            } else {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
                PaymentFormViewController *v = [PaymentFormViewController createVC];
                v.actionType = ActionTypeInsurancePayment;
                v.paymentProvider = wSelf.correspondingProvider;
                v.inputAmount = self.insuranceCalcResponse.insuranceAmount;
                v.bonusMalus = @([self.insuranceCalcResponse.bonusMalus intValue]);
                v.ogpoRequest = ogpoRequest;
                v.deliveryDate = [self.selectedDate stringWithDateFormat:@"dd.MM.yyyy"];
                v.fName = self.deliveryCell.firstname.value;
                v.lName = self.deliveryCell.lastname.value;
                v.mName = self.deliveryCell.middlename.value;
                v.email = self.deliveryCell.email.value;
                v.phone = self.deliveryCell.phone.value;
                v.street = self.deliveryCell.street.value;
                v.home = self.deliveryCell.house.value;
                v.flat = self.deliveryCell.flat.value;
                v.cityId = @([self.selectedCity.cityId intValue]);
                v.cityName = self.selectedCity.name;

                [self.navigationController pushViewController:v animated:YES];
            }

        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
        }];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

- (void)changePayTypeValue:(UISegmentedControl *)payTypeSegment {
    if (payTypeSegment.selectedSegmentIndex == 0) {
        self.isPayOnline = YES;
        self.toPaymentButton.title = @"К оплате";
    } else {
        self.isPayOnline = NO;
        self.toPaymentButton.title = @"Подтвердить заказ";
    }
}

#pragma mark - tableview delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"Удостоверение личности";
    } else if (section == 1) {
        return @"Водительское удостоверение";
    } else if (section == 2) {
        return @"Технический паспорт";
    } else if (section == 5) {
        return @"Заполните данные о доставке";
    } else if (section == 3) {
        if (self.isVov) {
            return @"Удостоверение участника ВОВ";
        } else {
            return nil;
        }
    } else if (section == 4) {
        if (self.isPen) {
            return @"Пенсионное удостоверение";
        } else {
            return nil;
        }
    }
//
// else if (section == 5) {
//    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.drivers.count;
    } else if (section == 1) {
        return self.drivers.count;
    } else if (section == 2) {
        return self.transports.count;
    } else if (section == 3) {
        if (self.isVov) {
            return 1;
        }
        return 0;
    } else if (section == 4) {
        if (self.isPen) {
            return 1;
        }
        return 0;
    } else if (section == 5) {
        return 4;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section < 5) {
        return 80;
    } else if (indexPath.section == 5) {
        if (indexPath.row == 0) {
            return 370;
        }
        return 44;
    }
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak InsuranceLoadDocumentsViewController *wSelf = self;

    id cellToReturn;

    if (indexPath.section < 5) {
        if (indexPath.section == 0) {
            static NSString *CellIdentifier = @"UdoCell";
            InsuranceLoadDocumentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[InsuranceLoadDocumentCell alloc] initWithReuseIdentifier:CellIdentifier];
            }
            cell.vcOwner = self;
            cell.onImageUploaded = ^(NSArray *uploadedUrls) {
                InsuranceDriver *d = wSelf.drivers[(NSUInteger) indexPath.row];
                d.identityScanDocs = uploadedUrls;
            };
            cellToReturn = cell;

        } else if (indexPath.section == 1) {
            static NSString *CellIdentifier = @"PravaCell";
            InsuranceLoadDocumentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[InsuranceLoadDocumentCell alloc] initWithReuseIdentifier:CellIdentifier];
            }
            cell.vcOwner = self;
            cell.onImageUploaded = ^(NSArray *uploadedUrls) {
                InsuranceDriver *d = wSelf.drivers[(NSUInteger) indexPath.row];
                d.drivingLicenseScanDocs = uploadedUrls;
            };
            cellToReturn = cell;

        } else if (indexPath.section == 2) {
            static NSString *CellIdentifier = @"TechnicalCell";
            InsuranceLoadDocumentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[InsuranceLoadDocumentCell alloc] initWithReuseIdentifier:CellIdentifier];
            }
            cell.vcOwner = self;
            cell.onImageUploaded = ^(NSArray *uploadedUrls) {
                InsuranceTransport *t = wSelf.transports[(NSUInteger) indexPath.row];
                t.vehicleIdentityDocs = uploadedUrls;
            };
            cellToReturn = cell;

        } else if (indexPath.section == 3) {
            static NSString *CellIdentifier = @"VovCell";
            InsuranceLoadDocumentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[InsuranceLoadDocumentCell alloc] initWithReuseIdentifier:CellIdentifier];
            }
            cell.vcOwner = self;
            cell.onImageUploaded = ^(NSArray *uploadedUrls) {
                wSelf.vovDiscountDocs = uploadedUrls;
            };
            cellToReturn = cell;

        } else if (indexPath.section == 4) {
            static NSString *CellIdentifier = @"PenCell";
            InsuranceLoadDocumentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[InsuranceLoadDocumentCell alloc] initWithReuseIdentifier:CellIdentifier];
            }
            cell.vcOwner = self;
            cell.onImageUploaded = ^(NSArray *uploadedUrls) {
                wSelf.benefitDiscountDocs = uploadedUrls;
            };
            cellToReturn = cell;
        }

    } else if (indexPath.section == 5) {

        if (indexPath.row == 0) {

            static NSString *CellIdentifier = @"DeliveryData";

            if (!self.deliveryCell) {
                self.deliveryCell = [[InsuranceDevileryDataCell alloc] initWithReuseIdentifier:CellIdentifier];
                self.deliveryCell.selectionStyle = UITableViewCellSelectionStyleNone;
                self.deliveryCell.accessoryType = UITableViewCellAccessoryNone;
            }
            cellToReturn = wSelf.deliveryCell;

        } else if (indexPath.row  == 1) {

            static NSString *CellIdentifier = @"SelectCity";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
                cell.selectionStyle = UITableViewCellSelectionStyleGray;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            cell.textLabel.text = self.selectedCity ? self.selectedCity.name : @"Выберите город";
            cellToReturn = cell;

        } else if (indexPath.row == 2) {

            static NSString *CellIdentifier = @"DatePicker";
            FloatingTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[FloatingTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }

            UIDatePicker *datePicker = [[UIDatePicker alloc] init];
            datePicker.datePickerMode = UIDatePickerModeDate;
            //[datePicker addTarget:self action:@selector(updateTextFieldDate:) forControlEvents:UIControlEventValueChanged];
            [cell.iin addTarget:self action:@selector(textFieldWithDateDidEnd:) forControlEvents:UIControlEventEditingDidEnd];
            cell.iin.inputView = datePicker;
            cell.iin.value = self.selectedDate ? [self.selectedDate stringWithDateFormat:@"dd.MM.yyyy"] : @"Выберите дату доставки";
            cellToReturn = cell;

        } else if (indexPath.row == 3) {

            static NSString *CellIdentifier = @"PayTypeSwitch";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
                cell.selectionStyle = UITableViewCellSelectionStyleGray;
            }

            if (!self.payTypeSegment) {
                self.payTypeSegment = [[UISegmentedControl alloc] initWithItems:@[@"Оплатить онлайн", @"Наличными курьеру"]];
                [self.payTypeSegment setTintColor:[UIColor flatBlackColorDark]];
                [self.payTypeSegment setSelectedSegmentIndex:0];
                [self.payTypeSegment addTarget:self action:@selector(changePayTypeValue:) forControlEvents:UIControlEventValueChanged];
                [cell addSubview:self.payTypeSegment];
            }

            self.payTypeSegment.x = (CGFloat) (( cell.width - self.payTypeSegment.width ) * 0.5);
            self.payTypeSegment.y = (CGFloat) (( cell.height - self.payTypeSegment.height ) * 0.5);

            cellToReturn = cell;
        }
    }
    return cellToReturn;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 5) {
        if (indexPath.row == 1) {
            //select City
            InsuranceSelectCityViewController *v = [[InsuranceSelectCityViewController alloc] init];
            v.delegate = self;
            [self.navigationController pushViewController:v animated:YES];
        }
    }
}

//choose city

- (void)didSelectCity:(City *)city {
    self.selectedCity = city;
    [self.tableView reloadData];
}

//choose date

- (void)textFieldWithDateDidEnd:(UITextField *)textField {
    UIDatePicker *datePicker = (UIDatePicker *) textField.inputView;
    self.selectedDate = datePicker.date;
    [self.tableView reloadData];
}

#pragma mark - config ui

- (void)configUI {
    [self setNavigationTitle:@"Cтрахование"];
    [self setBAFTableViewBackground];
    [self setNavigationBackButton];

    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.tableView.width, 120)];
    footerView.backgroundColor = [UIColor whiteColor];
    self.toPaymentButton = [[MainButton alloc] initWithFrame:CGRectMake(0,0,250,44)];
    self.toPaymentButton.title = @"К оплате";
    self.toPaymentButton.mainButtonStyle = MainButtonStyleOrange;
    self.toPaymentButton.x = (CGFloat) ((footerView.width - self.toPaymentButton.width) * 0.5);
    self.toPaymentButton.y = 20;
    [self.toPaymentButton addTarget:self action:@selector(clickPaymentButton) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:self.toPaymentButton];
    self.tableView.tableFooterView = footerView;
}

@end
