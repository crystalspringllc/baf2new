//
// Created by Askar Mustafin on 11/23/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class MainButton;

@interface InsuranceFooterView : UIView
@property (nonatomic, copy) void (^onCalculateClick)();
@property (nonatomic, copy) void (^onExecuteClick)();

- (void)setAmount:(NSNumber *)amount;

@end