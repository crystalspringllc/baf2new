//
// Created by Askar Mustafin on 11/29/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceDevileryDataCell.h"
#import "FloatingTextField.h"
#import "User.h"

@implementation InsuranceDevileryDataCell {}

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self configUI];
    }
    return self;
}

#pragma mark -

- (void)layoutSubviews {
    [super layoutSubviews];

    self.firstname.x = 16;
    self.firstname.y = 10;
    self.lastname.x = 16;
    self.lastname.y = self.firstname.bottom + 5;
    self.middlename.x = 16;
    self.middlename.y = self.lastname.bottom + 5;
    self.email.x = 16;
    self.email.y = self.middlename.bottom + 5;
    self.phone.x = 16;
    self.phone.y = self.email.bottom + 5;
    self.street.x = 16;
    self.street.y = self.phone.bottom + 5;
    self.house.x = 16;
    self.house.y = self.street.bottom + 5;
    self.flat.x = 16;
    self.flat.y = self.house.bottom + 5;
}

#pragma mark - config ui

- (void)configUI {
    self.firstname = [[FloatingTextField alloc] initWithFrame:CGRectMake(0, 0, self.width - 20, 40)];
    self.lastname = [[FloatingTextField alloc] initWithFrame:CGRectMake(0, 0, self.width - 20, 40)];
    self.middlename = [[FloatingTextField alloc] initWithFrame:CGRectMake(0, 0, self.width - 20, 40)];
    self.email = [[FloatingTextField alloc] initWithFrame:CGRectMake(0, 0, self.width - 20, 40)];
    self.phone = [[FloatingTextField alloc] initWithFrame:CGRectMake(0, 0, self.width - 20, 40)];
    self.street = [[FloatingTextField alloc] initWithFrame:CGRectMake(0, 0, self.width - 20, 40)];
    self.house = [[FloatingTextField alloc] initWithFrame:CGRectMake(0, 0, self.width - 20, 40)];
    self.flat = [[FloatingTextField alloc] initWithFrame:CGRectMake(0, 0, self.width - 20, 40)];

    [self addSubview:self.firstname];
    [self addSubview:self.lastname];
    [self addSubview:self.middlename];
    [self addSubview:self.email];
    [self addSubview:self.phone];
    [self addSubview:self.street];
    [self addSubview:self.house];
    [self addSubview:self.flat];

    self.firstname.keyboardType = UIKeyboardTypeDefault;
    self.lastname.keyboardType = UIKeyboardTypeDefault;
    self.middlename.keyboardType = UIKeyboardTypeDefault;
    self.email.keyboardType = UIKeyboardTypeEmailAddress;
    self.phone.keyboardType = UIKeyboardTypePhonePad;
    self.street.keyboardType = UIKeyboardTypeDefault;
    self.house.keyboardType = UIKeyboardTypeNumberPad;
    self.flat.keyboardType = UIKeyboardTypeNumberPad;

    self.firstname.placeholder = @"Имя";
    self.lastname.placeholder = @"Фамилия";
    self.middlename.placeholder = @"Отчество";
    self.email.placeholder = @"Email";
    self.phone.placeholder = @"Телефон";
    self.street.placeholder = @"Улица";
    self.house.placeholder = @"Дом";
    self.flat.placeholder = @"Квартира";

    if ([User sharedInstance].isLoggedIn) {
        self.firstname.value = [User sharedInstance].firstname;
        self.lastname.value = [User sharedInstance].lastname;
        self.middlename.value = [User sharedInstance].middlename;
        self.email.value = [User sharedInstance].email;
        self.phone.value = [User sharedInstance].phoneNumber;
    } else {
        self.firstname.value = @"";
        self.lastname.value = @"";
        self.middlename.value = @"";
        self.email.value = @"";
        self.phone.value = @"";
    }
}

- (BOOL)isValid {
    return !((self.firstname.value.length == 0)
 ||(self.lastname.value.length == 0)
 ||(self.middlename.value.length == 0)
 ||(self.email.value.length == 0)
 ||(self.phone.value.length == 0)
 ||(self.street.value.length == 0)
 ||(self.house.value.length == 0)
 ||(self.flat.value.length == 0));
}

@end