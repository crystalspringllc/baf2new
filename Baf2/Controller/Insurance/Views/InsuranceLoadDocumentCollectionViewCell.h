//
//  InsuranceLoadDocumentCollectionViewCell.h
//  Baf2
//
//  Created by Askar Mustafin on 11/25/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DeleteBlock)();

@interface InsuranceLoadDocumentCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *addImageView;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (copy, nonatomic) DeleteBlock deleteBlock;

@end
