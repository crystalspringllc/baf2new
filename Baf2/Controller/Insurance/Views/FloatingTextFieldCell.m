//
// Created by Askar Mustafin on 11/22/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "FloatingTextFieldCell.h"
#import "FloatingTextField.h"

@implementation FloatingTextFieldCell {}

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self configUI];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.iin.x = 17;
    self.iin.y = 5;
}

- (void)configUI {
    self.iin = [[FloatingTextField alloc] init];
    self.iin.width = self.width - 20;;
    self.iin.height = 34;
    [self addSubview:self.iin];
}

@end