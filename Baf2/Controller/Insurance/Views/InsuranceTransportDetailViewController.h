//
// Created by Askar Mustafin on 11/23/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InsuranceSelectCatalogViewController.h"
#import "InsuranceTransport.h"

@interface InsuranceTransportDetailViewController : UITableViewController<InsuranceSelectCatalogDelegate>

@property (nonatomic, strong) InsuranceTransport *transport;

@property (nonatomic, assign) BOOL isAbleDelete;

@property (nonatomic, copy) void (^onTransportDelete)(InsuranceTransport *driver);

@end
