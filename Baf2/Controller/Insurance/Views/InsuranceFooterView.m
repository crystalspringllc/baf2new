//
// Created by Askar Mustafin on 11/23/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceFooterView.h"
#import "MainButton.h"

@interface InsuranceFooterView ()
@property(nonatomic, strong) MainButton *execute;
@property(nonatomic, strong) UILabel *amountLabel;
@property (nonatomic, strong) NSNumber *amount;
@end

@implementation InsuranceFooterView {}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configUI];
    }
    return self;
}

#pragma mark - config actions

- (void)onRuleSwitch:(UISwitch *)switchSender {
    if (switchSender.isOn) {
        if (self.amount) {
            self.execute.mainButtonStyle = MainButtonStyleOrange;
        } else {
            self.execute.mainButtonStyle = MainButtonStyleDisable;
        }
    } else {
        self.execute.mainButtonStyle = MainButtonStyleDisable;
    }
}

- (void)clickExecute {
    if (self.onExecuteClick) {
        self.onExecuteClick();
    }
}

#pragma mark - config ui

- (void)configUI {
    self.backgroundColor = [UIColor whiteColor];

    MainButton *calculate = [[MainButton alloc] initWithFrame:CGRectMake(0,10,250,44)];
    calculate.x = (CGFloat) ((self.width - calculate.width) * 0.5);
    calculate.mainButtonStyle = MainButtonStyleOrange;
    calculate.title = @"Расчитать";
    calculate.tag = 9;
    [calculate addTarget:self action:@selector(clickCalculateButton) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:calculate];

    UILabel *title = [[UILabel alloc] init];
    title.text = @"Стоимость вашего полиса:";
    [title sizeToFit];
    title.x = (CGFloat) ((self.width - title.width) * 0.5);
    title.y = calculate.bottom + 10;
    title.font = [UIFont systemFontOfSize:14];
    [self addSubview:title];

    self.amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, title.bottom + 5, self.width - 40, 20)];
    self.amountLabel.font = [UIFont boldSystemFontOfSize:18];
    [self addSubview:self.amountLabel];

    UISwitch *ruleSwitch = [[UISwitch alloc] init];
    [ruleSwitch addTarget:self action:@selector(onRuleSwitch:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:ruleSwitch];

    UILabel *ruleTitle = [[UILabel alloc] init];
    ruleTitle.text = @"Я даю согласие на предоставление персональных данных";
    ruleTitle.font = [UIFont systemFontOfSize:12];
    ruleTitle.width = self.width - ruleSwitch.width - 34 - 5;
    ruleTitle.numberOfLines = 0;
    [ruleTitle sizeToFit];
    ruleTitle.y = self.amountLabel.bottom + 10;
    [self addSubview:ruleTitle];

    ruleSwitch.x = self.width - ruleSwitch.width - 17;
    ruleTitle.x = self.width - ruleTitle.width - 10 - ruleSwitch.width - 10;
    ruleSwitch.y = self.amountLabel.bottom + 15;

    self.execute = [[MainButton alloc] initWithFrame:CGRectMake(0, ruleSwitch.bottom + 20, 250, 44)];
    self.execute.mainButtonStyle = MainButtonStyleDisable;
    self.execute.title = @"Оформить полис";
    self.execute.x = (CGFloat) ((self.width - self.execute.width) * 0.5);
    [self.execute addTarget:self action:@selector(clickExecute) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.execute];
}

- (void)setAmount:(NSNumber *)amount {
    _amount = amount;
    self.amountLabel.text = [NSString stringWithFormat:@"%@ KZT", [self.amount decimalFormatString]];
    [self.amountLabel sizeToFit];
    self.amountLabel.x = (CGFloat) (( self.width - self.amountLabel.width ) * 0.5);
}

- (void)clickCalculateButton {
    if (self.onCalculateClick) {
        self.onCalculateClick();
    }
}

#pragma mark -

- (void)dealloc {
    NSLog(@"Insurance dealloc");
}

@end
