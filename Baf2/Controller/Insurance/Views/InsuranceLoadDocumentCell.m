//
// Created by Askar Mustafin on 11/24/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceLoadDocumentCell.h"
#import "UIActionSheet+Blocks.h"
#import "InsuranceLoadDocumentCollectionViewCell.h"
#import "InsuranceApi.h"

#define CellIdentifier @"InsuranceLoadDocumentCollectionViewCell"


@interface InsuranceLoadDocumentCell()

@property (nonatomic, strong) NSMutableArray *imageUrls;

@property (nonatomic, strong) NSMutableArray *addedImages;

@end

@implementation InsuranceLoadDocumentCell {}

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initVars];
        [self configUI];

    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initVars];
    [self configUI];
}

#pragma mark -

- (void)initVars {
    self.addedImages = [[NSMutableArray alloc] init];
    self.imageUrls = [[NSMutableArray alloc] init];
}

#pragma mark -

- (void)layoutSubviews {
    [super layoutSubviews];
    self.collectionView.frame = self.bounds;
}

#pragma mark - collection view

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.addedImages.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) wself = self;

    InsuranceLoadDocumentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    if (indexPath.item < self.addedImages.count) {
        cell.addImageView.hidden = YES;
        cell.imageView.image = self.addedImages[(NSUInteger) indexPath.item];
        cell.deleteButton.hidden = NO;

        cell.deleteBlock = ^{
            [wself.addedImages removeObjectAtIndex:(NSUInteger) indexPath.item];
            [wself.imageUrls removeObjectAtIndex:(NSUInteger) indexPath.item];
            [collectionView reloadData];

            if (wself.onImageUploaded) {
                wself.onImageUploaded(wself.imageUrls);
            }
        };
    } else {
        cell.addImageView.hidden = NO;
        cell.imageView.image = nil;
        cell.deleteButton.hidden = YES;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item == self.addedImages.count) {
        [self openImagePicker];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize cellSize = CGSizeMake(60, 80);
    return cellSize;
}

- (void)openImagePicker {
    __weak typeof(self) wself = self;
    [UIActionSheet showInView:self withTitle:nil cancelButtonTitle:NSLocalizedString(@"cancel", nil) destructiveButtonTitle:nil otherButtonTitles:@[NSLocalizedString(@"choose_from_album", nil), NSLocalizedString(@"make_a_photo", nil)] tapBlock:^(UIActionSheet *_Nonnull actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == actionSheet.firstOtherButtonIndex) {
            [wself openPhotoAlbum];
        } else if (buttonIndex == actionSheet.firstOtherButtonIndex + 1) {
            [wself showCamera];
        }
    }];
}

#pragma mark - Private methods

- (void)showCamera {
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;

        [self.vcOwner presentViewController:controller animated:YES completion:NULL];
    } else {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"device_not_support_camera", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
}

- (void)openPhotoAlbum {
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self.vcOwner presentViewController:controller animated:YES completion:NULL];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    __weak typeof(self) wSelf = self;
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [wSelf cropPickedImage:image];
    }];
}

- (void)cropPickedImage:(UIImage *)image {
    if (image) {
        TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:image];
        cropViewController.delegate = self;
        [self.vcOwner presentViewController:cropViewController animated:YES completion:nil];
    }
}

#pragma mark - TOCropViewController

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [cropViewController dismissViewControllerAnimated:YES completion:nil];

    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading_photo", nil) animated:true];
    [InsuranceApi scandocsStore:image success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        [self.addedImages addObject:image];
        [self.imageUrls addObject:response];
        [self.collectionView reloadData];

        if (self.onImageUploaded) {
            self.onImageUploaded(self.imageUrls);
        }

    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark -

- (void)configUI {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"InsuranceLoadDocumentCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:CellIdentifier];
    [self.collectionView setPagingEnabled:YES];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self addSubview:self.collectionView];
}

@end