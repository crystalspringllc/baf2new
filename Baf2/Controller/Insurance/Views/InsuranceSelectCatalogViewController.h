//
// Created by Askar Mustafin on 11/22/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InsuranceCatalogEntity.h"
#import "InsuranceApi.h"

@protocol InsuranceSelectCatalogDelegate
- (void)didSelectCatalogEntity:(InsuranceCatalogEntity *)catalogEntity catalogType:(CatalogType)catalogType;
@end

@interface InsuranceSelectCatalogViewController : UITableViewController

- (id)initWithCatalogs:(InsuranceCatalogs *)catalogs type:(CatalogType)type;
- (id)initWithType:(CatalogType)catalogType;

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) id <InsuranceSelectCatalogDelegate> delegate;

@end