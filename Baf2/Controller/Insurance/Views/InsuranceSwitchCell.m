//
// Created by Askar Mustafin on 11/21/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceSwitchCell.h"


@implementation InsuranceSwitchCell {

}

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self configUI];
    }
    return self;
}

#pragma mark - config actions

- (void)changeSwitch:(UISwitch *)switchSender {
    if (self.onSwitchHasChanged) {
        self.onSwitchHasChanged(switchSender.isOn);
    }
}

#pragma mark - config ui

- (void)configUI {
    UISwitch *switcher = [[UISwitch alloc] init];
    [switcher addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    self.accessoryView = switcher;
}

@end