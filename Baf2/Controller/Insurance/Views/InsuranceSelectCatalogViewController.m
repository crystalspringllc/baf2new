//
// Created by Askar Mustafin on 11/22/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceSelectCatalogViewController.h"
#import "UITableViewController+Extension.h"


@interface InsuranceSelectCatalogViewController()
@property (nonatomic, strong) InsuranceCatalogs *catalogs;
@property (nonatomic, assign) CatalogType catalogType;
@end

@implementation InsuranceSelectCatalogViewController {

}

- (id)initWithCatalogs:(InsuranceCatalogs *)catalogs type:(CatalogType)type {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        self.catalogs = catalogs;
        self.catalogType = type;
    }
    return self;
}

- (id)initWithType:(CatalogType)catalogType {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        self.catalogType = catalogType;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

    if (!self.catalogs) {
        [MBProgressHUD showAstanaHUDWithTitle:@"Загрузка" animated:YES];
        [InsuranceApi catalogsByCategoryType:self.catalogType success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            self.catalogs = [InsuranceCatalogs instanceFromDictionary:response[@"catalogsMap"]];
            [self.tableView reloadData];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
        }];
    } else {
        [self.tableView reloadData];
    }
}

#pragma mark - table

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.catalogType == CatalogTypeDrivingExperience) {
        return self.catalogs.age25DrivingExperience.count;
    } else if (self.catalogType == CatalogTypeRegistrationTerritory) {
        return self.catalogs.registrationTerritory.count;
    } else if (self.catalogType == CatalogTypeTransportType) {
        return self.catalogs.transportType.count;
    } else if (self.catalogType == CatalogTypeTransportUsageTime) {
        return self.catalogs.transportUsageTime.count;
    } else if (self.catalogType == CatalogTypeInsuranceType) {
        return self.catalogs.insuranceType.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"IINCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }

    InsuranceCatalogEntity *entity;

    if (self.catalogType == CatalogTypeDrivingExperience) {
        entity = self.catalogs.age25DrivingExperience[(NSUInteger) indexPath.row];
    } else if (self.catalogType == CatalogTypeRegistrationTerritory) {
        entity = self.catalogs.registrationTerritory[(NSUInteger) indexPath.row];
    } else if (self.catalogType == CatalogTypeTransportType) {
        entity = self.catalogs.transportType[(NSUInteger) indexPath.row];
    } else if (self.catalogType == CatalogTypeTransportUsageTime) {
        entity = self.catalogs.transportUsageTime[(NSUInteger) indexPath.row];
    } else if (self.catalogType == CatalogTypeInsuranceType) {
        entity = self.catalogs.insuranceType[(NSUInteger) indexPath.row];
    }

    cell.textLabel.text = entity.title;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    InsuranceCatalogEntity *entity;

    if (self.catalogType == CatalogTypeDrivingExperience) {
        entity = self.catalogs.age25DrivingExperience[(NSUInteger) indexPath.row];
    } else if (self.catalogType == CatalogTypeRegistrationTerritory) {
        entity = self.catalogs.registrationTerritory[(NSUInteger) indexPath.row];
    } else if (self.catalogType == CatalogTypeTransportType) {
        entity = self.catalogs.transportType[(NSUInteger) indexPath.row];
    } else if (self.catalogType == CatalogTypeTransportUsageTime) {
        entity = self.catalogs.transportUsageTime[(NSUInteger) indexPath.row];
    } else if (self.catalogType == CatalogTypeInsuranceType) {
        entity = self.catalogs.insuranceType[(NSUInteger) indexPath.row];
    }

    if (self.delegate) {
        [self.delegate didSelectCatalogEntity:entity catalogType:self.catalogType];
    }

    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - config ui

- (void)configUI {
    [self setNavigationTitle:@"Cтрахование"];
    [self setBAFTableViewBackground];
    [self setNavigationBackButton];
}

@end