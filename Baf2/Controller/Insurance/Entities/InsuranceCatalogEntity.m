//
// Created by Askar Mustafin on 11/21/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceCatalogEntity.h"

@implementation InsuranceCatalogs{}

+ (InsuranceCatalogs *)instanceFromDictionary:(NSDictionary *)aDictionary {
    InsuranceCatalogs *instance = [[InsuranceCatalogs alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    NSArray *terData = objectData[@"registration_territory"][@"catalogList"];
    NSArray *typData = objectData[@"transport_type"][@"catalogList"];
    NSArray *expData = objectData[@"age25_driving_experience"][@"catalogList"];
    NSArray *usaData = objectData[@"transport_usage_time"][@"catalogList"];
    NSArray *insData = objectData[@"insurance_type"][@"catalogList"];

    self.registrationTerritory = [self parseCatalogFromDataArray:terData];
    self.transportType = [self parseCatalogFromDataArray:typData];
    self.age25DrivingExperience = [self parseCatalogFromDataArray:expData];
    self.transportUsageTime = [self parseCatalogFromDataArray:usaData];
    self.insuranceType = [self parseCatalogFromDataArray:insData];

    objectData = nil;
}

- (NSArray *)parseCatalogFromDataArray:(NSArray *)dataArray {
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (NSDictionary *data in dataArray) {
        [tempArray addObject:[InsuranceCatalogEntity instanceFromDictionary:data]];
    }
    return [tempArray mutableCopy];
}

@end

@implementation InsuranceCatalogEntity {}

+ (InsuranceCatalogEntity *)instanceFromDictionary:(NSDictionary *)aDictionary {
    InsuranceCatalogEntity *instance = [[InsuranceCatalogEntity alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    objectData = aDictionary;

    self.catalogId = [self numberValueForKey:@"id"];
    self.name = [self stringValueForKey:@"name"];
    self.title = [self stringValueForKey:@"title"];

    objectData = nil;
}

@end