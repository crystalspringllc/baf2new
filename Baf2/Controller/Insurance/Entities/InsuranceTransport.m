//
// Created by Askar Mustafin on 11/23/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceTransport.h"

@implementation InsuranceTransport {}

- (NSDictionary *)jsonPrep {
    if (self.area && self.type && self.lifeTime) {

        NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
        d[@"areaId"] = self.area.catalogId;
        d[@"typeId"] = self.type.catalogId;
        d[@"lifetimeId"] = self.lifeTime.catalogId;

        if (IS_API_URL_TEST) {
            d[@"vehicleIdentityDocs"] = @[@"http://192.168.4.75:8080/general/scandocs_a5ba7b2a-f4d7-4c40-82d3-d9ba35c04c65.jpg"];
        }

        if (self.vehicleIdentityDocs) {
            d[@"vehicleIdentityDocs"] = self.vehicleIdentityDocs;
        }
        return d;
    } else {
        return nil;
    }
}

@end