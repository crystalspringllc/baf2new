//
// Created by Askar Mustafin on 11/21/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "BonusMalus.h"
#import "InsuranceCatalogEntity.h"


@implementation BonusMalus {}

+ (BonusMalus *)instanceFromDictionary:(NSDictionary *)aDictionary {
    BonusMalus *instance = [[BonusMalus alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    objectData = aDictionary;

    self.iin = [self stringValueForKey:@"iin"];
    self.lname = [self stringValueForKey:@"lname"];
    self.fname = [self stringValueForKey:@"fname"];
    self.mname = [self stringValueForKey:@"mname"];
    self.bonusMalus = [self stringValueForKey:@"bonusMalus"];
    self.resident = [self boolValueForKey:@"resident"];
    self.age = [self stringValueForKey:@"age"];

    objectData = nil;
}

@end