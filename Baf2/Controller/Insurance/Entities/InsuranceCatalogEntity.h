//
// Created by Askar Mustafin on 11/21/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface InsuranceCatalogs : ModelObject

@property (nonatomic, strong) NSArray *registrationTerritory;
@property (nonatomic, strong) NSArray *transportType;
@property (nonatomic, strong) NSArray *age25DrivingExperience;
@property (nonatomic, strong) NSArray *transportUsageTime;
@property (nonatomic, strong) NSArray *insuranceType;


+ (InsuranceCatalogs *)instanceFromDictionary:(NSDictionary *)aDictionary;

@end


@interface InsuranceCatalogEntity : ModelObject

@property (nonatomic, strong) NSNumber *catalogId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *title;

+ (InsuranceCatalogEntity *)instanceFromDictionary:(NSDictionary *)aDictionary;

@end