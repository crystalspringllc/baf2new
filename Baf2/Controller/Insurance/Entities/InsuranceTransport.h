//
// Created by Askar Mustafin on 11/23/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"
#import "InsuranceCatalogEntity.h"


@interface InsuranceTransport : ModelObject

@property (nonatomic, strong) InsuranceCatalogEntity *area;
@property (nonatomic, strong) InsuranceCatalogEntity *lifeTime;
@property (nonatomic, strong) InsuranceCatalogEntity *type;

@property (nonatomic, strong) NSArray *vehicleIdentityDocs;

@property (nonatomic, strong) NSDictionary *jsonPrep;

@end