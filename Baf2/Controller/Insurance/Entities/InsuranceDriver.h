//
// Created by Askar Mustafin on 11/22/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"
#import "InsuranceCatalogEntity.h"

@class BonusMalus;


@interface InsuranceDriver : ModelObject

@property (nonatomic, strong) InsuranceCatalogEntity *ageExperience;
@property (nonatomic, strong) BonusMalus *bonusMalus;
@property (nonatomic, strong) NSString *iin;

@property (nonatomic, strong) NSString *fname;
@property (nonatomic, strong) NSString *lname;
@property (nonatomic, strong) NSString *mname;
@property (nonatomic, assign) BOOL resident;

@property (nonatomic, strong) NSArray *identityScanDocs;
@property (nonatomic, strong) NSArray *drivingLicenseScanDocs;


@property (nonatomic, strong) NSDictionary *jsonPrep;

@end