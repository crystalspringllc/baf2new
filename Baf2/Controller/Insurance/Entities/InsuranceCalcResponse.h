#import <Foundation/Foundation.h>
#import "ModelObject.h"

@interface InsuranceCalcResponse : ModelObject {

    NSString *bonusMalus;
    NSNumber *insuranceAmount;
    NSString *name;
    NSNumber *providerId;
    NSString *providerLogo;
    NSString *providerPromotions;

}

@property (nonatomic, copy) NSString *bonusMalus;
@property (nonatomic, copy) NSNumber *insuranceAmount;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *providerId;
@property (nonatomic, copy) NSString *providerLogo;
@property (nonatomic, copy) NSString *providerPromotions;

+ (InsuranceCalcResponse *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
