//
// Created by Askar Mustafin on 11/22/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "InsuranceDriver.h"
#import "BonusMalus.h"


@implementation InsuranceDriver {}

- (NSDictionary *)jsonPrep {
    if (self.ageExperience && self.iin && self.bonusMalus) {

        NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
        d[@"ageExperienceId"] = self.ageExperience.catalogId;
        d[@"iin"] = self.iin;
        d[@"bonusMalus"] = self.bonusMalus.bonusMalus;
        d[@"lname"] = self.bonusMalus.lname;
        d[@"fname"] = self.bonusMalus.fname;
        d[@"mname"] = self.bonusMalus.mname;
        d[@"resident"] = self.bonusMalus.resident ? @"true" : @"false";

        if (IS_API_URL_TEST) {
            d[@"identityScanDocs"] = @[@"http://192.168.4.75:8080/general/scandocs_a5ba7b2a-f4d7-4c40-82d3-d9ba35c04c65.jpg"];;
            d[@"drivingLicenseScanDocs"] = @[@"http://192.168.4.75:8080/general/scandocs_a5ba7b2a-f4d7-4c40-82d3-d9ba35c04c65.jpg"];;
        }


        if (self.identityScanDocs) {
            d[@"identityScanDocs"] = self.identityScanDocs;
        }
        if (self.drivingLicenseScanDocs) {
            d[@"drivingLicenseScanDocs"] = self.drivingLicenseScanDocs;
        }
        return d;

    } else {
        return nil;
    }
}

@end
