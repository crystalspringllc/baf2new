#import "InsuranceCalcResponse.h"

@implementation InsuranceCalcResponse

@synthesize bonusMalus;
@synthesize insuranceAmount;
@synthesize name;
@synthesize providerId;
@synthesize providerLogo;
@synthesize providerPromotions;

+ (InsuranceCalcResponse *)instanceFromDictionary:(NSDictionary *)aDictionary {

    InsuranceCalcResponse *instance = [[InsuranceCalcResponse alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    objectData = aDictionary;

    self.bonusMalus = [self stringValueForKey:@"bonusMalus"];
    self.insuranceAmount = [self numberValueForKey:@"insuranceAmount"];
    self.name = [self stringValueForKey:@"name"];
    self.providerId = [self numberValueForKey:@"providerId"];
    self.providerLogo = [self stringValueForKey:@"providerLogo"];
    self.providerPromotions = [self stringValueForKey:@"providerPromotions"];

    objectData = nil;
}


@end
