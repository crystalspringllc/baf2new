//
// Created by Askar Mustafin on 11/21/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObject.h"


@interface BonusMalus : ModelObject

@property (nonatomic, strong) NSString *iin;
@property (nonatomic, strong) NSString *lname;
@property (nonatomic, strong) NSString *fname;
@property (nonatomic, strong) NSString *mname;
@property (nonatomic, strong) NSString *bonusMalus;
@property (nonatomic, assign) BOOL resident;
@property (nonatomic, strong) NSString *age;

+ (BonusMalus *)instanceFromDictionary:(NSDictionary *)aDictionary;

@end