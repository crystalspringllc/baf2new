//
//  AddPhysicalBeneficiaryViewController.h
//  BAF
//
//  Created by Almas Adilbek on 8/20/15.
//
//



@interface AddPhysicalBeneficiaryViewController : UIViewController

+ (instancetype)createVC;
- (void)loadBankInfo;

@end
