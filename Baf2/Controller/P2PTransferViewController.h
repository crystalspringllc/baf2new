//
//  P2PTransferViewController.h
//  BAF
//
//  Created by Askar Mustafin on 1/18/16.
//  Copyright © 2016 Банк Астаны. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface P2PTransferViewController : UITableViewController

+ (id)createVC;

@end
