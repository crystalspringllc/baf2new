//
//  NewsViewController.m
//  Baf2
//
//  Created by nmaksut on 14.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsApi.h"
#import "NewsTableCell.h"
#import "NewsDetailViewController.h"
#import "UIView+AAPureLayout.h"
#import "NewsAddPostViewController.h"
#import "NotificationCenterHelper.h"
#import "ViewController.h"
#import "RegistrationViewController.h"
#import "UIScrollView+EmptyDataSet.h"
#import "AstanaRefreshControl.h"

@interface NewsViewController () <UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (weak, nonatomic) IBOutlet UIView *segmentedControlContainerView;
@property (strong, nonatomic) UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation NewsViewController {
    BOOL showBankNews;
    NSMutableArray *bankNews, *generalNews;
    NSInteger lastBankNewsAmount, lastGeneralNewsAmount;
    NSString *bankLast, *generalLast;
    AstanaRefreshControl *refreshControl;
    BOOL showAddPost;
    BOOL loading;
    BOOL hasNotMoreBankNews, hasNotMoreGeneralNews;
}

#pragma mark - init


+ (id)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"News" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)initVars {
    bankNews    = [NSMutableArray new];
    generalNews = [NSMutableArray new];
    showBankNews = YES;
}

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self]; // sometimes dealloc not called, therefore remove previous observers if exist
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoLogin) name:kNotificationGotoLogin object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoRegister) name:kNotificationGotoRegistration object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedIn) name:kNotificationUserLoggedIn object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedIn) name:kNotificationUserRegistered object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reload) name:kNotificationUserAddedPost object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reload) name:kNotificationNewsDeleted object:nil];
}

#pragma mark - lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initVars];
    [self configUI];
    [self loadBankNews:false loadMore:false];
    [self registerNotifications];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    showAddPost = YES;
}

#pragma mark - actions

- (void)addPostTapped {
    if ([User sharedInstance].loggedIn) {
        NewsAddPostViewController *vc = [NewsAddPostViewController createVC];
        [self.navigationController pushViewController:vc animated:true];
    } else {
        [UIHelper showActionSheetAuthorizationRequiredWithTarget:self];
    }
}

#pragma mark - load

- (void)loadBankNews:(BOOL)refresh loadMore:(BOOL)loadMore {
    if (loading) {
        return;
    }
    
    loading = true;
    
    if (!refresh && !loadMore) {
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading_news", nil) animated:true];
    }
    
    [NewsApi getBankNewsWithLastItem:bankLast success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        
        if (refresh || (!refresh && !loadMore)) {
            [bankNews removeAllObjects];
            generalLast = nil;
        }
        
        NSArray *news = response[@"news"];
        hasNotMoreBankNews = !news || news.count == 0;
        lastBankNewsAmount = news.count;
        
        NSLog(@"Response from bankNews %@", response);
        
        NSString *last = response[@"last"];
        if (last.length > 0) {
            bankLast = last;
        }
        [bankNews addObjectsFromArray:news];
        
        [self.tableView reloadData];
        
        if (refresh) {
            [refreshControl finishingLoading];
        }
        
        loading = false;
        
    } failure:^(NSString *code, NSString *message) {
        
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        
        [self.tableView reloadData];
        [self.tableView layoutIfNeeded];
        if (refresh) {
            [refreshControl finishingLoading];
        }
        
        loading = false;
    }];
}

- (void)loadGeneralNews:(BOOL)refresh loadMore:(BOOL)loadMore {
    if (loading) {
        return;
    }
    
    loading = true;

    if (!refresh && !loadMore) {
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading_news", nil) animated:true];
    }
    
    [NewsApi getGeneralNewsWithLastItem:generalLast success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        
        if (refresh || (!refresh && !loadMore)) {
            [generalNews removeAllObjects];
            bankLast = nil;
        }
        
        NSArray *news = response[@"news"];
        hasNotMoreGeneralNews = !news || news.count == 0;
        lastGeneralNewsAmount = news.count;
        NSString *last = response[@"last"];
        if (last.length > 0) {
            generalLast = last;
        }
        [generalNews addObjectsFromArray: news];
        
        [self.tableView reloadData];
        [self.tableView layoutIfNeeded];
        
        if (refresh) {
            [refreshControl finishingLoading];
        }
        loading = false;
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        
        [self.tableView reloadData];
        [self.tableView layoutIfNeeded];
        if (refresh) {
            [refreshControl finishingLoading];
        }
        
        loading = false;
    }];
}

#pragma mark - actions

- (void)refresh {
    if (showBankNews) {
        lastBankNewsAmount = 0;
        bankLast = nil;
        [self loadBankNews:true loadMore:false];
    } else {
        lastGeneralNewsAmount = 0;
        generalLast = nil;
        [self loadGeneralNews:true loadMore:false];
    }
}

- (void)reload {
    if (showBankNews) {
        lastBankNewsAmount = 0;
        bankLast = nil;
        [self loadBankNews:false loadMore:false];
    } else {
        lastGeneralNewsAmount = 0;
        generalLast = nil;
        [self loadGeneralNews:false loadMore:false];
    }
}

#pragma mark - segmented control

- (void)segmentedControlValueChanged:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        showBankNews = YES;
        [self loadBankNews:false loadMore:false];
        [self removeAddPostHeader];
    } else {
        showBankNews = NO;
        [self loadGeneralNews:false loadMore:false];
        [self showAddPostHeader];
    }
    [self.tableView reloadData];
}

#pragma mark - uitableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = showBankNews ? bankNews.count : generalNews.count;
    if (count > 0) {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1) {
        return 1;
    }
    
    return showBankNews ? bankNews.count : generalNews.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        UIActivityIndicatorView *spinner = [cell.contentView viewWithTag:7];
        if (!spinner) {
            UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            spinner.tag = 7;
            spinner.translatesAutoresizingMaskIntoConstraints = false;
            [cell.contentView addSubview:spinner];
            [spinner autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:8];
            [spinner autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:8];
            [spinner autoAlignAxisToSuperviewAxis:ALAxisVertical];
        }
        
        if ((hasNotMoreBankNews && self.segmentedControl.selectedSegmentIndex == 0) ||
            (hasNotMoreGeneralNews && self.segmentedControl.selectedSegmentIndex == 1)) {
            [spinner stopAnimating];
            cell.textLabel.text = NSLocalizedString(@"no_more_news", nil);
        } else {
            [spinner startAnimating];
            cell.textLabel.text = nil;
        }
        
        if (!loading && showBankNews)  {
            if (lastBankNewsAmount > 0) [self loadBankNews:false loadMore:true];
        } else if (!loading) {
            if (lastGeneralNewsAmount > 0) [self loadGeneralNews:false loadMore:true];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        static NSString *cellIdentifier = @"LoadMoreCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor appGrayColor];
        
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        
        return cell;
    }
    
    NewsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    News *news;
    if (showBankNews) {
        news = bankNews[indexPath.row];
    } else {
        news = generalNews[indexPath.row];
    }
    [cell setNews:news];
    
    
    cell.tapLikeButton = ^(NewsTableCell *tapInCell) {
        if ([User sharedInstance].loggedIn) {
            if (!news.like) {
                tapInCell.likesButton.selected = YES;
                [NewsApi likeObjectID:news.newsId type:news.type success:^(id response) {
        
                    [news updateAfterLike:response];
                    [tapInCell.likesButton setTitle:[NSString stringWithFormat:@"%@ ", news.likes.stringValue] forState:UIControlStateNormal];
                    
                } failure:^(NSString *code, NSString *message) {
                    tapInCell.likesButton.selected = news.like;
                }];
            } else {
                tapInCell.likesButton.selected = NO;
                [NewsApi dislikeObjectID:news.newsId type:news.type success:^(id response) {
                    
                    [news updateAfterLike:response];
                    tapInCell.likesButton.selected = news.like;
                    [tapInCell.likesButton setTitle:[NSString stringWithFormat:@"%@ ", news.likes.stringValue] forState:UIControlStateNormal];
                    
                } failure:^(NSString *code, NSString *message) {
                    tapInCell.likesButton.selected = news.like;}];
            }
        }
    };
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        return;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    News *news = showBankNews ? bankNews[indexPath.row] : generalNews[indexPath.row];
    NewsDetailViewController *vc = [NewsDetailViewController createVC];
    [vc setNews:news];
    [self.navigationController pushViewController:vc animated:YES];
    showAddPost = NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [refreshControl scrollViewDidScroll:scrollView];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    [refreshControl scrollViewWillEndDragging];
}

#pragma mark - methods

- (void)showAddPostHeader {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 50)];
    view.backgroundColor = [UIColor clearColor];
    UIButton *add = [UIButton buttonWithType:UIButtonTypeSystem];
    [add setTitle:NSLocalizedString(@"add_new_post", nil) forState:UIControlStateNormal];
    [add setTitleColor:[UIColor fromRGB:0x005780] forState:UIControlStateNormal];
    
    [add addTarget:self action:@selector(addPostTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:add];
    
    [add aa_centerVertical];
    [add aa_centerHorizontal];
    
    self.tableView.tableHeaderView = view;
}

- (void)removeAddPostHeader {
    self.tableView.tableHeaderView = nil;
}


- (void)gotoLogin {
    UINavigationController *navigationController = [UIHelper defaultNavigationController];
    ViewController *viewController = [ViewController createVC];
    viewController.showAsModal = YES;
    [navigationController setViewControllers:@[viewController]];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)gotoRegister {
    UINavigationController *navigationController = [UIHelper defaultNavigationController];
    RegistrationViewController *viewController = [[RegistrationViewController alloc] init];
    viewController.fromNews = YES;
    [navigationController setViewControllers:@[viewController]];
    [self presentViewController:navigationController animated:YES completion:nil];
    
}

- (void)userLoggedIn {
//    if (showAddPost) [self addPostTapped];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"no_data", nil)];
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}

#pragma mark - config ui

- (void)configUI {
    [self setBafBackground];
    [self showMenuButton];

    if ([User sharedInstance].loggedIn) {
        self.segmentedControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"ga_news", nil), NSLocalizedString(@"forum", nil)]];
        self.segmentedControlContainerView.backgroundColor = [UIColor whiteColor];
        self.segmentedControl.tintColor = [UIColor fromRGB:0x444444];
        [self.segmentedControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.segmentedControl setSelectedSegmentIndex:0];
        self.navigationItem.titleView = self.segmentedControl;
    } else {
        self.segmentedControl = nil;
        [self setNavigationTitle:NSLocalizedString(@"ga_news", nil)];
    }
    
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.estimatedRowHeight = 313;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    refreshControl = [[AstanaRefreshControl alloc] init];
    refreshControl.target = self;
    refreshControl.action = @selector(refresh);
    [self.tableView addSubview: refreshControl];
    [self.tableView sendSubviewToBack:refreshControl];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    NSLog(@"dealloc news");
}

@end
