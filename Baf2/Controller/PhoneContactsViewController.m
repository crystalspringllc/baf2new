//
//  PhoneContactsViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 14.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "PhoneContactsViewController.h"
#import "PeopleContact.h"
#import <AddressBook/AddressBook.h>

@interface PhoneContactsViewController () <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *peopleContacts, *filteredPeopleContacts;
@property (nonatomic, strong) NSArray *contactsFirstLetters;
@property (nonatomic, strong) NSMutableDictionary *contacts;

@end

@implementation PhoneContactsViewController

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.peopleContacts = [NSMutableArray new];
        self.filteredPeopleContacts = [NSMutableArray new];
        self.contactsFirstLetters = [NSArray new];
        self.contacts = [NSMutableDictionary new];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
    [self authorize];
}

#pragma mark - Methods

- (void)authorize {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"form_list", nil) animated:true];
    
    ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
    
    if (status == kABAuthorizationStatusDenied || status == kABAuthorizationStatusRestricted) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"this_section_require_access_to_contacts", nil) delegate:nil cancelButtonTitle:@"ОК" otherButtonTitles:nil] show];
        return;
    }
    
    CFErrorRef error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    
    if (!addressBook) {
        NSLog(@"ABAddressBookCreateWithOptions error: %@", CFBridgingRelease(error));
        return;
    }
    
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
        if (error) {
            NSLog(@"ABAddressBookRequestAccessWithCompletion error: %@", CFBridgingRelease(error));
        }
        
        if (granted) {
            [self listPeopleInAddressBook:addressBook];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAstanaHUDAnimated:true];
                [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"this_section_require_access_to_contacts", nil) delegate:nil cancelButtonTitle:@"ОК" otherButtonTitles:nil] show];
            });
        }
        
        CFRelease(addressBook);
    });
}

- (void)listPeopleInAddressBook:(ABAddressBookRef)addressBook {
    NSArray *allPeople = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
    NSInteger numberOfPeople = [allPeople count];
    
    for (NSInteger i = 0; i < numberOfPeople; i++) {
        PeopleContact *contact = [PeopleContact new];
        
        ABRecordRef person = (__bridge ABRecordRef)allPeople[i];
        
        NSString *firstName = CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        contact.firstName = firstName;
        NSString *lastName  = CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
        contact.lastName = lastName;
        
        ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
        
        NSMutableArray *phoneNumberList = [NSMutableArray new];
        CFIndex numberOfPhoneNumbers = ABMultiValueGetCount(phoneNumbers);
        for (CFIndex i = 0; i < numberOfPhoneNumbers; i++) {
            NSString *phoneNumber = CFBridgingRelease(ABMultiValueCopyValueAtIndex(phoneNumbers, i));
            [phoneNumberList addObject:phoneNumber];
        }
        contact.phoneNumbers = phoneNumberList;
        
        CFRelease(phoneNumbers);

        [self.peopleContacts addObject:contact];
    }
    
    self.filteredPeopleContacts = [NSMutableArray arrayWithArray:self.peopleContacts];
    
    [self groupAndSort];
}

#pragma mark - Helpers

- (void)searchForText:(NSString *)text {
    if (text && text.length > 0) {
        NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"fullName contains[CD] %@", text];
        self.filteredPeopleContacts = [self.peopleContacts filteredArrayUsingPredicate:filterPredicate].mutableCopy;
    } else {
        self.filteredPeopleContacts = [NSMutableArray arrayWithArray:self.peopleContacts];
    }
    
    [self groupAndSort];
}

- (void)groupAndSort {
    self.contactsFirstLetters = [NSArray new];
    self.contacts = [NSMutableDictionary new];
    
    // get all letters for sections
    NSMutableSet *contactsFirstLetters = [NSMutableSet new];
    for (PeopleContact *contact in self.filteredPeopleContacts) {
        NSString *fullName = contact.fullName;
        if (fullName && fullName.length > 1) {
            NSString *fullNameFirstLetter = [[fullName substringToIndex:1] uppercaseString];
            
            [contactsFirstLetters addObject:fullNameFirstLetter];
            
            // group by first letter in full name
            if (self.contacts[fullNameFirstLetter]) {
                NSMutableArray *contacts = [self.contacts[fullNameFirstLetter] mutableCopy];
                [contacts addObject:contact];
                self.contacts[fullNameFirstLetter] = contacts;
            } else {
                self.contacts[fullNameFirstLetter] = [NSMutableArray arrayWithObject:contact];
            }
        }
    }
    
    // sort all contacts alphabetically by full name
    self.contactsFirstLetters = [contactsFirstLetters.allObjects sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    for (int i = 0; i < self.contacts.allKeys.count; i++) {
        NSString *key = self.contacts.allKeys[i];
        NSMutableArray *contacts = [self.contacts[key] mutableCopy];
        [contacts sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            PeopleContact *pc1 = (PeopleContact *)obj1;
            PeopleContact *pc2 = (PeopleContact *)obj2;
            return [pc1.fullName caseInsensitiveCompare:pc2.fullName] == NSOrderedDescending;
        }];
        self.contacts[key] = contacts;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideAstanaHUDAnimated:true];
        [self.tableView reloadData];
    });
}

- (NSInteger)numberOfCellsForLetter:(NSString *)letter {
    NSInteger numberOfCellForLetter = 0;
    
    NSMutableArray *contacts = self.contacts[letter];
    if (contacts) {
        for (PeopleContact *contact in contacts) {
            NSInteger numberOfCellsForPeopleContact = contact.phoneNumbers.count;
            numberOfCellForLetter += numberOfCellsForPeopleContact;
        }
    }
    
    return numberOfCellForLetter;
}

- (NSString *)contactNumberForIndexPath:(NSIndexPath *)indexPath {
    NSString *letter = self.contactsFirstLetters[indexPath.section];
    NSMutableArray *contacts = self.contacts[letter];
    
    NSInteger numberOfCellForLetter = 0;
    if (contacts) {
        for (PeopleContact *contact in contacts) {
            NSInteger numberOfCellsForPeopleContact = contact.phoneNumbers.count;
            
            if (numberOfCellForLetter <= indexPath.row && indexPath.row < numberOfCellForLetter + numberOfCellsForPeopleContact) {
                for (int i = 0; i < numberOfCellsForPeopleContact; i++) {
                    if (indexPath.row == numberOfCellForLetter + i) {
                        return contact.phoneNumbers[i];
                    }
                }
            }
            
            numberOfCellForLetter += numberOfCellsForPeopleContact;
        }
    }
    
    return @"";
}

- (NSString *)contactFullNameForIndexPath:(NSIndexPath *)indexPath {
    NSString *letter = self.contactsFirstLetters[indexPath.section];
    NSMutableArray *contacts = self.contacts[letter];
    
    NSInteger numberOfCellForLetter = 0;
    if (contacts) {
        for (PeopleContact *contact in contacts) {
            NSInteger numberOfCellsForPeopleContact = contact.phoneNumbers.count;
            
            if (numberOfCellForLetter <= indexPath.row && indexPath.row < numberOfCellForLetter + numberOfCellsForPeopleContact) {
                return contact.fullName;
            }
            
            numberOfCellForLetter += numberOfCellsForPeopleContact;
        }
    }
    
    return @"";
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:true animated:true];
    
    return true;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:false animated:true];
    
    return true;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self searchForText:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self searchForText:nil];
    
    [searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchForText:searchBar.text];
    
    [searchBar resignFirstResponder];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.contactsFirstLetters.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *letter = self.contactsFirstLetters[section];

    return [self numberOfCellsForLetter:letter];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    cell.textLabel.text = [self contactFullNameForIndexPath:indexPath];
    cell.detailTextLabel.text = [self contactNumberForIndexPath:indexPath];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.contactsFirstLetters[section];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.contactsFirstLetters;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    if (self.didSelectContact) {
        self.didSelectContact([self contactFullNameForIndexPath:indexPath], [self contactNumberForIndexPath:indexPath]);
    }
    
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - Config UI

- (void)configUI {
    [self setNavigationTitle:NSLocalizedString(@"my_contacts", nil)];
    [self setNavigationBackButton];
    
    [self setBafBackground];
    
    self.searchBar = [UISearchBar newAutoLayoutView];
    self.searchBar.placeholder = NSLocalizedString(@"searching_contracts", nil);
    self.searchBar.delegate = self;
    self.searchBar.tintColor = [UIColor appBlueColor];
    [self.view addSubview:self.searchBar];
    [self.searchBar autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero excludingEdge:ALEdgeBottom];
    
    self.tableView = [UITableView newAutoLayoutView];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView = nil;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.sectionIndexColor = [UIColor appBlueColor];
    self.tableView.sectionIndexBackgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    [self.view addSubview:self.tableView];
    [self.tableView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero excludingEdge:ALEdgeTop];
    [self.tableView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.searchBar];
}

@end
