//
// Created by Askar Mustafin on 6/21/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "ConfirmPasswordViewController.h"
#import <STPopup/STPopup.h>
#import "JVFloatLabeledTextField.h"
#import "MainButton.h"
#import "ProfileApi.h"


@interface ConfirmPasswordViewController() {}
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *currentPasswordTextField;
@property (weak, nonatomic) IBOutlet MainButton *confirmButton;

@end

@implementation ConfirmPasswordViewController {

}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.confirmButton.mainButtonStyle = MainButtonStyleOrange;

    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 250);
    
    self.currentPasswordTextField.secureTextEntry = YES;
}

- (IBAction)clickConfirmButton:(id)sender {
    if (self.currentPasswordTextField.text.length == 0) {
        return;
    }

    if (self.onCorfirmTapped) {
        self.onCorfirmTapped(self.currentPasswordTextField.text);
        [self.popupController popViewControllerAnimated:YES];
    }
}

@end
