//
// Created by Askar Mustafin on 4/11/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Card.h"
#import "UIScrollView+EmptyDataSet.h"

@class ChoosePeriodControlView;
@class AccountDetailHeaderView;


@interface AccountDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

+ (instancetype)createVC;
@property (nonatomic, strong) Account *account;
@property (nonatomic, copy) void (^onUpdateAccounts)();

@end