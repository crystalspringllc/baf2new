//
//  PaymentsSearchViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 23.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Contract;
@class PaymentProvider;
@class ProviderCategory;
@class PaymentsViewController;

@protocol PaymentsSearchViewControllerDelegate;

@interface PaymentsSearchViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UISearchBarDelegate> {
    UITableView *list;
}

@property (nonatomic, weak) id<PaymentsSearchViewControllerDelegate> delegate;

-(void)search:(NSString *)text;
-(void)searchOnlyProviderCategories:(NSString *)text;
- (UITableView *)getList;

@property (nonatomic, weak) PaymentsViewController *paymentsViewController;

@end

@class PaymentsSearchViewController;

@protocol PaymentsSearchViewControllerDelegate <NSObject>

@optional
-(void)searchResultsView:(PaymentsSearchViewController *)paymentsSearchViewController contractTapped:(Contract *)contract;
-(void)searchResultsView:(PaymentsSearchViewController *)paymentsSearchViewController providerTapped:(PaymentProvider *)provider;
-(void)searchResultsView:(PaymentsSearchViewController *)paymentsSearchViewController providerCategoryTapped:(ProviderCategory *)category;
-(void)searchResultsViewHideKeyboard;
-(void)searchResultsViewDidHide;

@end