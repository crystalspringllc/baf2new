//
//  EditBankTransferBeneficiaryAccountsViewController.h
//  BAF
//
//  Created by Almas Adilbek on 7/23/15.
//
//



@interface EditBankTransferBeneficiaryAccountsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) NSArray *beneficiaryAccounts;

@end