//
//  BankDetailsTableViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 13.07.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "BankDetailsTableViewController.h"
#import "BankDetailTableViewCell.h"
#import "UITableViewController+Extension.h"

#define DETAIL_CODES @[ @"USD", @"EURO", @"GBP", @"RUB", @"CNY"]

@interface BankDetailsTableViewController ()

@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *KZT1detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *KZT2detailLabel;

@property (nonatomic, strong) NSDictionary *details;

@end

@implementation BankDetailsTableViewController

- (id)init {
    self = [super init];
    if(self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BankDetails" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initVars];
    [self configUI];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.addressLabel.preferredMaxLayoutWidth = self.addressLabel.frame.size.width;
    self.KZT1detailLabel.preferredMaxLayoutWidth = self.KZT1detailLabel.frame.size.width;
    self.KZT2detailLabel.preferredMaxLayoutWidth = self.KZT2detailLabel.frame.size.width;
}

- (void)initVars {
    self.details = @{
                     @"USD": @{@"title": NSLocalizedString(@"requisites_4", nil),
                               @"beneficiaryBank": @"BANK of ASTANA JSC",
                               @"correspondentBank": @"SWIFT/BIC code: ASFBKZKA \n\nCorrespondent account: KZ366010011000301362 with HALYK SAVINGS BANK OF KAZAKSTAN JSC, Almaty, Kazakhstan, \nSWIFT: HSBKKZKX \n\nCorrespondent account: KZ729260001000816002 with KAZKOMMERTSBANK JSC, Almaty, Kazakhstan \nSWIFT: KZKOKZKX"},
                     @"EURO": @{@"title": NSLocalizedString(@"requisites_5", nil),
                                @"beneficiaryBank": @"BANK of ASTANA JSC",
                                @"correspondentBank": @"SWIFT/BIC code: ASFBKZKA \n\nCorrespondent account: 30111978190000598502 EUR with PROMSVYAZBANK PJSC, MOSCOW, RUSSIA\nSWIFT: PRMSRUMM"},
                     @"GBP": @{@"title": NSLocalizedString(@"requisites_6", nil),
                               @"beneficiaryBank": @"BANK of ASTANA JSC",
                               @"correspondentBank": @"SWIFT/BIC code: ASFBKZKA \n\nCorrespondent account: KZ079260001000816008 GPB with KAZKOMMERTSBANK JSC, Almaty, Kazakhstan\nSWIFT: KZKOKZKX"},
                     @"RUB": @{@"title": NSLocalizedString(@"requisites_7", nil),
                               @"beneficiaryBank": @"BANK of ASTANA JSC",
                               @"correspondentBank": @"SWIFT/BIC code: ASFBKZKA \n\nНаименование корр. банка: АО «Альфа-Банк», г.Москва \n\nКорр. счет АО «Банк Астаны» в АО «Альфа-Банк»: 30111810200000000273 \n\nРеквизиты АО «Альфа-Банк», г.Москва: \nКорр. счет 30101810200000000593 в ГУ Банка России по ЦФО, БИК 044525593 \n\nSWIFT: ALFARUMM, ИНН 7728168971"
                               },
                     @"CNY": @{@"title": NSLocalizedString(@"requisites_8", nil),
                               @"beneficiaryBank": @"BANK of ASTANA JSC",
                               @"correspondentBank": @"SWIFT/BIC code: ASFBKZKA \n\nCorrespondent account: KZ029260001000816001 with KAZKOMMERTSBANK JSC, Almaty, Kazakhstan \nSWIFT: KZKOKZKX \n\nCorrespondent account: KZ889139845002010СNY with BANK OF CHINA KAZAKHSTAN, Almaty, Kazakhstan \nSWIFT: BKCHKZKA"},
                     };
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"BankDetailTableViewCell";
    
    BankDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    if (!cell) {
        cell = [[BankDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *detailCode = DETAIL_CODES[indexPath.section];
    
    NSDictionary *detailInfo = self.details[detailCode];
    cell.beneficiaryBankTextView.backgroundColor = [UIColor clearColor];
    cell.beneficiaryBankTextView.text = detailInfo[@"beneficiaryBank"];
    cell.correspondentBankTextView.backgroundColor = [UIColor clearColor];
    cell.correspondentBankTextView.text = detailInfo[@"correspondentBank"];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    
    NSString *detailCode = DETAIL_CODES[section];

    NSDictionary *detailInfo = self.details[detailCode];
    NSString *detailTitle = detailInfo[@"title"];
    
    CGFloat height = [detailTitle boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 12, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]} context:nil].size.height;
    
    UILabel *detailTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 8, [ASize screenWidth] - 2 * 12, height)];
    detailTitleLabel.font = [UIFont boldSystemFontOfSize:14];
    detailTitleLabel.numberOfLines = 0;
    detailTitleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    detailTitleLabel.textColor = [UIColor appDarkBlueColor];
    detailTitleLabel.text = detailTitle;
    [headerView addSubview:detailTitleLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSString *detailCode = DETAIL_CODES[section];
    
    NSDictionary *detailInfo = self.details[detailCode];
    NSString *detailTitle = detailInfo[@"title"];
    
    CGFloat height = [detailTitle boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 12, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]} context:nil].size.height;
    
    return height + 2 * 8;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

#pragma mark - Config UI

- (void)configUI {
    [self setNavigationTitle:NSLocalizedString(@"ga_bank_detail", nil)];
    [self showMenuButton];
    [self setBAFTableViewBackground];

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
    self.tableView.tableFooterView = [UIView new];
    
    // set the tableHeaderView so that the required height can be determined
    self.tableView.tableHeaderView = self.headerView;
    [self.headerView setNeedsLayout];
    [self.headerView layoutIfNeeded];
    CGFloat height = [self.headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    // update the header's frame and set it again
    CGRect headerFrame = self.headerView.frame;
    headerFrame.size.height = height;
    self.headerView.frame = headerFrame;
    self.tableView.tableHeaderView = self.headerView;
}

@end
