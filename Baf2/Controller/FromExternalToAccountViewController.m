//
// Created by Askar Mustafin on 7/20/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "FromExternalToAccountViewController.h"
#import "OperationAccounts.h"
#import "AccountsActionSheetViewController.h"
#import "STPopupController.h"
#import "STPopup.h"
#import "STPopupController+Extensions.h"
#import "Accounts.h"
#import "Account.h"
#import "TransfersFormCell.h"
#import "NewTransfersApi.h"
#import "JVFloatLabeledTextField.h"
#import "NSString+Ext.h"
#import "FromExternalToAccountButtonCheckResponse.h"
#import "MainButton.h"
#import "MBProgressHUD.h"
#import "MBProgressHUD+AstanaView.h"
#import "P2PBrowserViewController.h"
#import "P2PInitResponse.h"
#import "TransfersFormDisclaimerCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIHelper.h"


@interface FromExternalToAccountViewController()<UITextFieldDelegate>
@property (nonatomic, strong) NSDictionary *selectedTargetMap;
@property (nonatomic, strong) Account *selectedTargetAccount;
@property (nonatomic, strong) FromExternalToAccountButtonCheckResponse *checkResponse;
@property (nonatomic, strong) FromExternalToAccountButtonCheckResponse *buttonResponse;
@property (weak, nonatomic) IBOutlet TransfersFormDisclaimerCell *convertationDisclamer;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *sumTextField;
@property (weak, nonatomic) IBOutlet MainButton *transferButton;
@property (weak, nonatomic) IBOutlet TransfersFormDisclaimerCell *comissionCell;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *currencyTextField;
@property (weak, nonatomic) IBOutlet TransfersFormDisclaimerCell *enrolDisclamer;
@end

@implementation FromExternalToAccountViewController {}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Transfers" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([FromExternalToAccountViewController class])];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self configUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.operationAccounts.targetAccounts.deposits.count == 0
            && self.operationAccounts.targetAccounts.currents.count == 0) {
        [UIHelper showAlertWithMessage:@"Нет счетов для перевода"];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - config requests

- (void)checkButton {
    [MBProgressHUD showAstanaHUDWithTitle:@"Проверка перевода" animated:YES];
    [NewTransfersApi checkTransferButtonDestinationId:self.selectedTargetAccount.accountId
                                      destinationType:self.selectedTargetAccount.type
                                    destinationAmount:[self.sumTextField.text numberDecimalFormat]
                                  destinationCurrency:self.selectedTargetAccount.currency
                                               succes:^(id response) {
                                                   [MBProgressHUD hideAstanaHUDAnimated:YES];
                                                   if (response) {
                                                       self.checkResponse =
                                                               [FromExternalToAccountButtonCheckResponse instanceFromDictionary:response];

                                                       [self showCurrencyRate:self.checkResponse.exchange.composeCurrencyRateMessage
                                                                andUpdateTime:self.checkResponse.exchange.composeUpdateTime];

                                                       if (self.checkResponse.isValid) {
                                                           self.transferButton.mainButtonStyle = MainButtonStyleOrange;
                                                       }

                                                       if (self.checkResponse.comission && self.checkResponse.comissionCurrency) {
                                                           self.comissionCell.mainTextLabel.text =
                                                                   [NSString stringWithFormat:@"Комиссия: %@ %@",
                                                                                   self.checkResponse.comission,
                                                                                   self.checkResponse.comissionCurrency];
                                                       }

                                                       if (self.sumTextField.text.length > 0 && self.selectedTargetAccount.currency) {
                                                           self.enrolDisclamer.leftDescriptionTextLabel.text =
                                                                   [NSString stringWithFormat:@"%@ %@",
                                                                                   [self.checkResponse.amount decimalFormatString],
                                                                   self.checkResponse.currency];
                                                                    self.sumTextField.text = [self.checkResponse.destinationAmount decimalFormatString];
                                                           self.currencyTextField.text = self.checkResponse.destinationCurrency;
                                                       }
                                                   }
                                               } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
                                                   if([code isEqualToString:@"UNAVAILABLE"]){
                                                       message = [message stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
                                                       [UIHelper showAlertWithMessageTitle:nil message:message];
                                                   }
            }];
}

#pragma mark - config actions

- (IBAction)clickTransferButton:(id)sender {
    if (self.checkResponse) {
        [MBProgressHUD showAstanaHUDWithTitle:@"Подготовка к переводу" animated:YES];
        [NewTransfersApi initPayboxButtonDestinationId:self.selectedTargetAccount.accountId
                                       destinationType:self.selectedTargetAccount.type
                                     destinationAmount:[self.sumTextField.text numberDecimalFormat]
                                   destinationCurrency:self.selectedTargetAccount.currency
                                                succes:^(id response) {
                                                    [MBProgressHUD hideAstanaHUDAnimated:YES];
                                                    if (response) {

                                                        self.buttonResponse = [FromExternalToAccountButtonCheckResponse instanceFromDictionary:response];
                                                        P2PInitResponse *p2pResponse = [[P2PInitResponse alloc] init];
                                                        p2pResponse.processLink = self.buttonResponse.processLink;
                                                        p2pResponse.servicelogId = self.buttonResponse.servicelogId;
                                                        p2pResponse.provider = self.buttonResponse.provider;
                                                        p2pResponse.uuid = self.buttonResponse.uuid;
                                                        p2pResponse.session = self.buttonResponse.session;
                                                        p2pResponse.type = self.buttonResponse.type;
                                                        p2pResponse.clientId = self.buttonResponse.clientId;
                                                        p2pResponse.backLink = self.buttonResponse.backLink;

                                                        P2PBrowserViewController *vc = [P2PBrowserViewController new];
                                                        vc.p2PInitResponse = p2pResponse;
                                                        vc.isNew = NO;
                                                        [self.navigationController pushViewController:vc animated:true];

                                                    }
                                                } failure:^(NSString *code, NSString *message) {
                    [MBProgressHUD hideAstanaHUDAnimated:YES];
                }];
    }
}

#pragma mark - delegates

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([textField isEqual:self.sumTextField]) {
        if (textField.text.length > 0) {
            [self checkButton];
        }
    }
}

- (void)actionSheet:(AccountsActionSheetViewController *)actionSheet didSelectedAccount:(Account *)account {
    if (account) {
        self.selectedTargetAccount = account;
        self.selectedTargetMap = [self prepareSelectedAccounts:account];
        [self.tableView reloadData];
        [self checkButton];
        self.currencyTextField.text = account.currency;
    }
    [actionSheet dismissViewControllerAnimated:YES completion:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
            if (self.convertationDisclamer.hidden) {
                return 0;
            } else {
                return UITableViewAutomaticDimension;
            }
        }
        case 2: {
            if (indexPath.row == 1) {
                if (self.checkResponse.comission && self.checkResponse.comissionCurrency) {
                    return UITableViewAutomaticDimension;
                } else {
                    return 0;
                }
            } else {
                return UITableViewAutomaticDimension;
            }
        }
        case 3: {

            NSArray *warningMessages = self.checkResponse.warningMessages;
            if (indexPath.row == 0 && (!warningMessages || warningMessages.count == 0)) {
                return 0;
            } else if (indexPath.row == 1 && (!warningMessages || warningMessages.count < 2)) {
                return 0;
            } else if (indexPath.row == 2 && (!warningMessages || warningMessages.count < 3)) {
                return 0;
            } else if (indexPath.row == 3 && (!warningMessages || warningMessages.count < 4)) {
                return 0;
            } else if (indexPath.row == 4 && (!warningMessages || warningMessages.count < 5)) {
                return 0;
            } else if (indexPath.row == 5 && (!warningMessages || warningMessages.count < 6)) {
                return 0;
            }

        }
        case 4: {
            if (self.sumTextField.text.length > 0) {
                return UITableViewAutomaticDimension;
            } else {
                return 0;
            }
        }
        default: {
            return UITableViewAutomaticDimension;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row == 0 && indexPath.section == 1) {
        TransfersFormCell *sourceCell = (TransfersFormCell *)cell;
        if (self.selectedTargetMap)
        {
            [sourceCell.iconImageView
                    sd_setImageWithURL:self.selectedTargetMap[@"logo"]
                      placeholderImage:[UIImage imageNamed:@"icon-icon-placeholder"]];
            sourceCell.mainTitleLabel.text = self.selectedTargetMap[@"alias"];
            sourceCell.leftDescriptionLabel.text = self.selectedTargetMap[@"number"];
            sourceCell.rightDescriptionLabel.text = self.selectedTargetMap[@"amount"];
        } else {
            [sourceCell.iconImageView setImage:[UIImage imageNamed:@"icon-icon-placeholder"]];
            sourceCell.mainTitleLabel.text = @"Выберите счет";
            sourceCell.leftDescriptionLabel.text = @"";
            sourceCell.rightDescriptionLabel.text = @"";
        }
    } else if (indexPath.section == 3) {
        TransfersFormDisclaimerCell *disclaimerCell = (TransfersFormDisclaimerCell *) cell;
        if (self.checkResponse.warningMessages.count > indexPath.row) {
            [disclaimerCell setHTML:self.checkResponse.warningMessages[(NSUInteger) indexPath.row]];
        }
        return disclaimerCell;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"indexPath row: %li, section: %li", indexPath.row, indexPath.section);
    if (indexPath.row == 0 && indexPath.section == 1) {
        if (self.operationAccounts) {
            [self openAccountsActionSheet:[self prepareAccountsForActionSheet:self.operationAccounts]];
        }
    }
}

#pragma mark - config ui

- (void)configUI {
    [self setNavigationTitle:@"С карты другого банка на счет/депозит"];
   // @"С карты другого банка на счет/депозит"
    [self setNavigationBackButton];
    [self setBafBackground];

    self.transferButton.mainButtonStyle = MainButtonStyleDisable;
    self.convertationDisclamer.hidden = YES;
    self.currencyTextField.enabled = NO;
    self.tableView.estimatedRowHeight = 100;
    self.sumTextField.delegate = self;
    self.sumTextField.keyboardType = UIKeyboardTypeDecimalPad;
}

- (void)openAccountsActionSheet:(NSArray *)accounts {
    AccountsActionSheetViewController *p = [[AccountsActionSheetViewController alloc] initWithAccounts:accounts];
    p.delegate = self;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:p];
    popupController.dismissOnBackgroundTap = true;
    popupController.style = STPopupStyleBottomSheet;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [popupController presentInViewController:self];
}

- (NSArray *)prepareAccountsForActionSheet:(OperationAccounts *)operationAccounts {
    if (operationAccounts.targetAccounts) {
        Accounts *tagetAccounts = operationAccounts.targetAccounts;
        NSMutableArray *filteredSourceAccounts = [[NSMutableArray alloc] init];
        if (tagetAccounts.currents.count) {
            NSMutableDictionary *currentsSection = [[NSMutableDictionary alloc] init];
            currentsSection[@"title"] = @"Счета";
            currentsSection[@"rows"] = tagetAccounts.currents;
            [filteredSourceAccounts addObject:currentsSection];
        }
        if (tagetAccounts.deposits.count) {
            NSMutableDictionary *depositSection = [[NSMutableDictionary alloc] init];
            depositSection[@"title"] = @"Депозиты";
            depositSection[@"rows"] = tagetAccounts.deposits;
            [filteredSourceAccounts addObject:depositSection];
        }
        return filteredSourceAccounts;
    }
    return nil;
}

- (NSDictionary *)prepareSelectedAccounts:(Account *)account {
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    dictionary[@"alias"] = account.alias;
    dictionary[@"number"] = account.iban;
    dictionary[@"amount"] = [account balanceAvailableWithCurrency];
    dictionary[@"logo"] = account.logo;
    return dictionary;
}

- (void)showCurrencyRate:(NSString *)message andUpdateTime:(NSString *)time {
    self.convertationDisclamer.leftDescriptionTextLabel.text = message;
    self.convertationDisclamer.rightDescriptionTextLabel.text = time;

    if (message && time) {
        self.convertationDisclamer.hidden = NO;
    } else {
        self.convertationDisclamer.hidden = YES;
    }
    [self.tableView reloadData];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"FROM EXTERNAL dealloc");
}

@end
