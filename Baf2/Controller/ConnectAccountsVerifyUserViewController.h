//
//  ConnectAccountsVerifyUserViewController.h
//  BAF
//
//  Created by Almas Adilbek on 8/25/14.
//
//



#import "AAFormViewController.h"

@protocol ConnectAccountsVerifyUserViewControllerDelegate;

@interface ConnectAccountsVerifyUserViewController : AAFormViewController

@property(nonatomic, copy) NSString *mobilePhone;
@property(nonatomic, assign) id <ConnectAccountsVerifyUserViewControllerDelegate> delegate;

@end

@protocol ConnectAccountsVerifyUserViewControllerDelegate<NSObject>
    -(void)connectAccountsVerifyUserViewControllerSmsVerified;
@end
