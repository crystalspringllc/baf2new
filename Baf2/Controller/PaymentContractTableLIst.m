//
//  PaymentContractTableLIst.m
//  Baf2
//
//  Created by developer on 05.01.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "PaymentContractTableLIst.h"
#import "Contract.h"
#import "ConratctPaymentTableViewCell.h"
#import "UITableViewController+Extension.h"
#import "AlsekoReceiptViewController.h"
#import "PaymentFormViewController.h"
#import "ContractsApi.h"

@interface PaymentContractTableLIst ()

@end

@implementation PaymentContractTableLIst
@dynamic tableView;

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Payments" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PaymentContractTableLIst class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contracts.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ConratctPaymentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PaymentContractListCell" forIndexPath:indexPath];
    
    [cell setContract:self.contracts[indexPath.row]];
    cell.backgroundColor = [UIColor clearColor];
    cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Удалить" icon:[UIImage imageNamed:@"delete.png"] backgroundColor:[UIColor redColor]]];
    cell.rightSwipeSettings.transition = MGSwipeTransitionDrag;
    cell.delegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Contract *conract = [self.contracts objectAtIndex:indexPath.row];
    [self payForContract:conract];
}



- (BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSMutableArray *deletedIndexPaths = [NSMutableArray array];
    
    Contract *contract = self.contracts[indexPath.row];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"delete_contract", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *_Nonnull action) {
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"deleting_contract", nil) animated:true];
        
        [ContractsApi deleteContract:contract.identifier success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
           
            [deletedIndexPaths addObject:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
            [self.contracts removeObject:self.contracts[indexPath.row]];
            
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:deletedIndexPaths withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView endUpdates];

        }  failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];
    }]];
    
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancellation", nil) style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:alertController animated:true completion:nil];

    //cell.touchOnDismissSwipe = YES;
   // if([alertController.preferredAction.title isEqualToString:NSLocalizedString(@"cancellation", nil)]) return NO;
    return YES;
    
}


- (void)payForContract:(Contract *)contract {
    UIViewController *v;
    
    if ([contract.provider.infoRequestType isEqualToNumber:@2]) {
        AlsekoReceiptViewController *alsekoVC = [AlsekoReceiptViewController createVC];
        alsekoVC.contract = contract;
        v = alsekoVC;
    } else {
        PaymentFormViewController *paymentVC = [PaymentFormViewController createVC];
        paymentVC.actionType = ActionTypeMakePayment;
        paymentVC.contract = contract;
        v = paymentVC;
    }
    
    [self.navigationController pushViewController:v animated:YES];
}

- (void)configUI {
    [self setBAFTableViewBackground];
    [self setNavigationTitle:@"Шаблоны платежей"];
}


@end
