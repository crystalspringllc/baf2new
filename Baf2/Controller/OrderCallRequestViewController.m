//
//  OrderCallRequestViewController.m
//  BAF
//
//  Created by Almas Adilbek on 10/31/14.
//
//

#import <ChameleonFramework/ChameleonMacros.h>
#import "OrderCallRequestViewController.h"
#import "FloatingTextField.h"
#import "HeaderTitleView.h"
#import "DatePickerInputTextField.h"
#import "CommonApi.h"
#import "BafMakeCallDirs.h"
#import "IdTitleObject.h"
#import "MainButton.h"
#import "NSString+Validators.h"
#import "ActionSheetListPopupViewController.h"
#import "STPopupController+Extensions.h"
#import "NSDate+Ext.h"

@interface OrderCallRequestViewController ()

@property (nonatomic, strong) NSMutableArray *tasks;

@property (nonatomic, strong) ActionSheetListPopupViewController *callTypeActionSheetVC, *cityActionSheetVC;

@property (nonatomic, strong) IdTitleObject *selectedCity;
@property (nonatomic, strong) IdTitleObject *selectedTopic;

@end

@implementation OrderCallRequestViewController {
    FloatingTextField *lastnameField;
    FloatingTextField *nameField;
    FloatingTextField *middlenameField;
    UIButton *cityDropdown;
    UIButton *subjectDropdown;

    FloatingTextField *emailField;
    FloatingTextField *telField;
    DatePickerInputTextField *datePicker;
    FloatingTextField *timeField;
    MainButton *submitButton;
    UISwitch *acceptSwitch;
    BafMakeCallDirs *makeCallDirs;
}

-(id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

-(void)initVars {
    self.tasks = [NSMutableArray new];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
    [self configAction];

    [MBProgressHUD showAstanaHUDWithoutMaskWithTitle:nil animated:true];
    NSURLSessionDataTask *task = [CommonApi getBafMakeCallDirs:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        [self onBafMakeDirs:response];
    } failure:^(NSInteger code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
//        [UIHelper showAlertTryAgain];
        [self popViewController];
    }];
    [self.tasks addObject:task];

    if([User sharedInstance].loggedIn)
    {
        User *user = [User sharedInstance];
        [lastnameField setValue:user.lastname];
        [nameField setValue:user.firstname];
        [emailField setValue:user.email];
        [telField setValue:user.phoneNumber];
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    UIImage *image = [UIImage imageNamed:@"expand_arrow"];
    
    subjectDropdown.imageEdgeInsets = UIEdgeInsetsMake(0., subjectDropdown.frame.size.width - (image.size.width), 0., 0.);
    subjectDropdown.titleEdgeInsets = UIEdgeInsetsMake(0., -image.size.width, 0., 0);
    
    cityDropdown.imageEdgeInsets = UIEdgeInsetsMake(0., cityDropdown.frame.size.width - (image.size.width), 0., 0.);
    cityDropdown.titleEdgeInsets = UIEdgeInsetsMake(0., -image.size.width, 0, 0);
}

- (void)goBack {
    [MBProgressHUD hideAstanaHUDAnimated:true];
    
    for (NSURLSessionDataTask *task in self.tasks) {
        [task cancel];
    }
    
    [super goBack];
}

- (void)configAction
{

}

- (void)onBafMakeDirs:(id)response
{
    makeCallDirs = (BafMakeCallDirs *)response;
    
    __weak OrderCallRequestViewController *weakSelf = self;
    __weak BafMakeCallDirs *weakMakeCallDirs = makeCallDirs;
    
    self.cityActionSheetVC = [[ActionSheetListPopupViewController alloc] init];
    self.cityActionSheetVC.numberOfRowsInSection = ^NSInteger(NSInteger section) {
        return weakMakeCallDirs.cities.count;
    };
    
    for (int i = 0; i < makeCallDirs.cities.count; i++) {
        IdTitleObject *city = makeCallDirs.cities[i];
        
        if (city == self.selectedCity) {
            weakSelf.cityActionSheetVC.selectedIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
            break;
        }
    }
    
    self.cityActionSheetVC.rowForIndexPath = ^ActionSheetListRowObject*(NSIndexPath *indexPath) {
        IdTitleObject *city = weakMakeCallDirs.cities[indexPath.row];
        
        ActionSheetListRowObject *rowObject = [ActionSheetListRowObject new];
        rowObject.title = city.title;
        rowObject.keepObject = city;
        
        return rowObject;
    };
    
    __weak UIButton *weakCityDropdown = cityDropdown;
    self.cityActionSheetVC.onSelectObject = ^(id selectedObject, NSIndexPath *indexPath) {
        weakSelf.selectedCity  = selectedObject;
        [weakCityDropdown setTitle:weakSelf.selectedCity.title forState:UIControlStateNormal];
    };
    
    self.cityActionSheetVC.title = NSLocalizedString(@"choose_city", nil);
    
    self.callTypeActionSheetVC = [[ActionSheetListPopupViewController alloc] init];
    self.callTypeActionSheetVC.numberOfRowsInSection = ^NSInteger(NSInteger section) {
        return weakMakeCallDirs.topics.count;
    };
    
    if (!self.callTypeActionSheetVC.selectedIndexPath) {
        for (int i = 0; i < makeCallDirs.topics.count; i++) {
            IdTitleObject *topic = makeCallDirs.topics[i];
            switch(self.requestType) {
                case BafRequestTypeDeposit:
                    if ([topic.title.lowercaseString isEqualToString:[NSLocalizedString(@"_deposits", nil) lowercaseString]]) {
                        self.selectedTopic = topic;
                        self.callTypeActionSheetVC.selectedIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
                        [subjectDropdown setTitle:topic.title forState:UIControlStateNormal];
                    }
                    break;
                case BafRequestTypeLoan:
                    if ([topic.title.lowercaseString isEqualToString:[NSLocalizedString(@"_loans", nil) lowercaseString]]) {
                        self.selectedTopic = topic;
                        self.callTypeActionSheetVC.selectedIndexPath = [NSIndexPath indexPathForRow:i inSection:0];;
                        [subjectDropdown setTitle:topic.title forState:UIControlStateNormal];
                    }
                    break;
                case BafRequestTypeRequestCard:
                    if ([topic.title.lowercaseString isEqualToString:[NSLocalizedString(@"bank_cards", nil) lowercaseString]]) {
                        self.selectedTopic = topic;
                        self.callTypeActionSheetVC.selectedIndexPath = [NSIndexPath indexPathForRow:i inSection:0];;
                        [subjectDropdown setTitle:topic.title forState:UIControlStateNormal];
                    }
                    break;
                case BafRequestTypeOpenAccount:
                    if ([topic.title.lowercaseString isEqualToString:[NSLocalizedString(@"opening_accounts", nil) lowercaseString]]) {
                        self.selectedTopic = topic;
                        self.callTypeActionSheetVC.selectedIndexPath = [NSIndexPath indexPathForRow:i inSection:0];;
                        [subjectDropdown setTitle:topic.title forState:UIControlStateNormal];
                    }
                    break;
                default: break;
            }
        }
    }
    
    for (int i = 0; i < makeCallDirs.topics.count; i++) {
        IdTitleObject *topic = makeCallDirs.topics[i];
        
        if (topic == self.selectedTopic) {
            weakSelf.callTypeActionSheetVC.selectedIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
            break;
        }
    }
    
    __weak UIButton *weakSubjectDropdown = subjectDropdown;
    self.callTypeActionSheetVC.rowForIndexPath = ^ActionSheetListRowObject*(NSIndexPath *indexPath) {
        IdTitleObject *topic = weakMakeCallDirs.topics[indexPath.row];
        
        ActionSheetListRowObject *rowObject = [ActionSheetListRowObject new];
        rowObject.title = topic.title;
        rowObject.keepObject = topic;
    
        return rowObject;
    };
    
    self.callTypeActionSheetVC.onSelectObject = ^(id selectedObject, NSIndexPath *indexPath) {
        weakSelf.selectedTopic  = selectedObject;
        [weakSubjectDropdown setTitle:weakSelf.selectedTopic.title forState:UIControlStateNormal];
    };
    
    self.callTypeActionSheetVC.title = NSLocalizedString(@"choose_topic", nil);
}

- (void)acceptSwitchChanged:(UISwitch *)sender {
    [submitButton setEnabled:sender.isOn];
}


#pragma mark -
#pragma mark Actions

- (void)showCitiesActionSheet {
    [self.cityActionSheetVC.tableView reloadData];
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:self.cityActionSheetVC];
    popupController.dismissOnBackgroundTap = true;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    popupController.style = STPopupStyleBottomSheet;
    [popupController presentInViewController:self];
}

- (void)showCallTypesActionSheet {
    [self.callTypeActionSheetVC.tableView reloadData];
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:self.callTypeActionSheetVC];
    popupController.dismissOnBackgroundTap = true;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    popupController.style = STPopupStyleBottomSheet;
    [popupController presentInViewController:self];
}

- (void)submitTapped
{
    [self.view endEditing:YES];

    if(![emailField.text validateEmail]) {
        return;
    }

    if(![telField.text validatePhoneNumber]) {
        return;
    }

    if(!self.selectedCity) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"select_your_city", nil)];
        return;
    }

    if(!self.selectedTopic) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"select_theme_of_message", nil)];
        return;
    }

    if (![nameField.text validate] || ![lastnameField.text validate]) {
        return;
    }

    // Build parameters
    NSMutableDictionary *o = [NSMutableDictionary dictionary];

    // Request type
    NSString *order = @"call";
    if(_requestType == BafRequestTypeDeposit) order = @"deposit";
    else if(_requestType == BafRequestTypeLoan) order = @"loan";
    o[@"order"] = order;

    o[@"lname"] = [lastnameField value];
    o[@"fname"] = [nameField value];
    o[@"mname"] = [middlenameField value];
    o[@"email"] = [emailField value];
    o[@"phone"] = [telField value];

    IdTitleObject *city = self.selectedCity;
    o[@"city"] = city.objectId;

    IdTitleObject *topic = self.selectedTopic;
    o[@"topic"] = topic.objectId;

    o[@"date"] = [datePicker value];
    o[@"time"] = [timeField value];

    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"sending", nil) animated:true];
    [CommonApi getDepositOrLoanRequest:o success:^(id response)
    {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"request_sent", nil) message:NSLocalizedString(@"thank_you_for_request", nil)];
        [self popViewController];
    } failure:^(NSInteger code, NSString *message)
    {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark -
#pragma mark TimeIntervalPickerView

- (void)timeIntervalPickerViewDidChange:(NSString *)timeIntervalString {
    NSArray *times = [timeIntervalString componentsSeparatedByString:@" - "];
    if(times.count == 2) [timeField setValue:[NSString stringWithFormat:@"С %@ до %@", times[0], times[1]]];
}


#pragma mark -

-(void)configUI
{
    self.view.backgroundColor = [UIColor whiteColor];
    self.contentScrollView.backgroundColor = [UIColor clearColor];
    
    [self setBafBackground];
    [self setNavigationTitle:NSLocalizedString(@"request_for_call", nil)];

    if(self.navigationController.viewControllers.count > 1) [self setNavigationBackButton];

    CGFloat fieldSpacing = 0;

    subjectDropdown = [[UIButton alloc] initWithFrame:CGRectMake(15, 0, [ASize screenWidth] - 2 * 15, 44)];
    subjectDropdown.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [subjectDropdown setTitle:NSLocalizedString(@"choose_topic", nil) forState:UIControlStateNormal];
    [subjectDropdown setTitleColor:[UIColor appBlueColor] forState:UIControlStateNormal];
    subjectDropdown.titleLabel.font = [UIFont systemFontOfSize:15];
    subjectDropdown.y = 10;
    [subjectDropdown addTarget:self action:@selector(showCallTypesActionSheet) forControlEvents:UIControlEventTouchUpInside];
    [subjectDropdown setImage:[UIImage imageNamed:@"expand_arrow"] forState:UIControlStateNormal];
    [contentScrollView addSubview:subjectDropdown];

    lastnameField = [[FloatingTextField alloc] initWithFrame:CGRectMake(15, 0, [ASize screenWidth] - 2 * 15, 44)];
    [lastnameField setPlaceholder:NSLocalizedString(@"last_name", nil)];
    lastnameField.y = subjectDropdown.bottom + fieldSpacing;
    [contentScrollView addSubview:lastnameField];

    nameField = [[FloatingTextField alloc] initWithFrame:CGRectMake(15, 0, [ASize screenWidth] - 2 * 15, 44)];
    [nameField setPlaceholder:NSLocalizedString(@"first_name", nil)];
    nameField.y = lastnameField.bottom + fieldSpacing;
    [contentScrollView addSubview:nameField];

    middlenameField = [[FloatingTextField alloc] initWithFrame:CGRectMake(15, 0, [ASize screenWidth] - 2 * 15, 44)];
    [middlenameField setPlaceholder:NSLocalizedString(@"middle_name", nil)];
    middlenameField.y = nameField.bottom + fieldSpacing;
    [contentScrollView addSubview:middlenameField];

    cityDropdown = [[UIButton alloc] initWithFrame:CGRectMake(15, 0, [ASize screenWidth] - 2 * 15, 44)];
    cityDropdown.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [cityDropdown setTitle:NSLocalizedString(@"choose_city", nil) forState:UIControlStateNormal];
    [cityDropdown setTitleColor:[UIColor appBlueColor] forState:UIControlStateNormal];
    cityDropdown.titleLabel.font = [UIFont systemFontOfSize:15];
    cityDropdown.y = middlenameField.bottom + 10;
    [cityDropdown addTarget:self action:@selector(showCitiesActionSheet) forControlEvents:UIControlEventTouchUpInside];
    [cityDropdown setImage:[UIImage imageNamed:@"expand_arrow"] forState:UIControlStateNormal];
    [contentScrollView addSubview:cityDropdown];

    // Contacts
    HeaderTitleView *headerTitleView1 = [[HeaderTitleView alloc] initWithTitle:NSLocalizedString(@"contact_information", nil)];
    headerTitleView1.y = cityDropdown.bottom + 15;
    [contentScrollView addSubview:headerTitleView1];
    
    UIView *headerBackgroundView = [headerTitleView1 viewWithTag:100];
    if (headerBackgroundView) {
        headerBackgroundView.backgroundColor = [[UIColor fromRGB:0xF4F4F4] colorWithAlphaComponent:0.5];
    }

    emailField = [[FloatingTextField alloc] initWithFrame:CGRectMake(15, 0, [ASize screenWidth] - 2 * 15, 44)];
//    [emailField setEmailKeyboard];
    [emailField setPlaceholder:NSLocalizedString(@"your_email", nil)];
    emailField.y = headerTitleView1.bottom + 10;
//    [emailField setIcon:@"icon_mail"];
    [contentScrollView addSubview:emailField];

    telField = [[FloatingTextField alloc] initWithFrame:CGRectMake(15, 0, [ASize screenWidth] - 2 * 15, 44)];
//    [telField setPhoneNumberKeyboard];
    [telField setPlaceholder:@"(7XX) XXX XX XX"];
    telField.y = emailField.bottom;
//    [telField setIcon:@"icon_mobile"];
//    [telField setPrefix:@"+7"];
    [telField setMaxCharacters:10];
    [contentScrollView addSubview:telField];

    datePicker = [[DatePickerInputTextField alloc] initWithTitle:NSLocalizedString(@"prefered_call_date", nil) frame:CGRectMake(15, 0, [ASize screenWidth] - 2 * 15, 44)];
    [datePicker setPlaceholder:NSLocalizedString(@"call_date", nil)];
    datePicker.datePickerView.datePicker.minimumDate = [NSDate date];
    datePicker.datePickerView.datePicker.maximumDate = [[NSDate date] dateByAddingDays:3];
    datePicker.y = telField.bottom + 4;
    [datePicker setValue:[[NSDate date] stringWithDateFormat:@"dd.MM.yyyy"]];
    [contentScrollView addSubview:datePicker];

    // Time picker
    TimeIntervalPickerView *timePickerView = [[TimeIntervalPickerView alloc] init];
    timePickerView.delegate = self;
    [timePickerView setTitle:NSLocalizedString(@"fromm", nil) left:YES];
    [timePickerView setTitle:NSLocalizedString(@"untill", nil) left:NO];

    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:ss"];
    [timePickerView setDatePickerDate:[df dateFromString:@"09:00"] left:YES];
    [timePickerView setDatePickerDate:[df dateFromString:@"18:00"] left:NO];

    timeField = [[FloatingTextField alloc] initWithFrame:CGRectMake(15, 0, [ASize screenWidth] - 2 * 15, 44)];
    [timeField setPlaceholder:NSLocalizedString(@"prefered_time", nil)];
    timeField.y = datePicker.bottom + 5;
    timeField.inputView = timePickerView;
    [timeField setValue:NSLocalizedString(@"from_nine_till_six", nil)];
    [contentScrollView addSubview:timeField];
    
    // Accept
    UILabel *acceptLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kContentWidth - 80, 1)];
    acceptLabel.backgroundColor = [UIColor clearColor];
    acceptLabel.font = [UIFont helveticaNeue:[ASize iphone:11 ipad:14]];
    acceptLabel.textColor = [UIColor darkGrayColor];
    acceptLabel.lineBreakMode = NSLineBreakByWordWrapping;
    acceptLabel.numberOfLines = 0;
    acceptLabel.text = NSLocalizedString(@"agree_to_call_agreement", nil);
    [acceptLabel sizeToFit];
    acceptLabel.x = kViewSidePadding + 80;
    acceptLabel.y = timeField.bottom + 20;
    [contentScrollView addSubview:acceptLabel];

    acceptSwitch = [[UISwitch alloc] init];
    [acceptSwitch addTarget:self action:@selector(acceptSwitchChanged:) forControlEvents:UIControlEventValueChanged];
    acceptSwitch.x = kViewSidePadding + [ASize iOS7:10 or:0];
    acceptSwitch.centerY = acceptLabel.centerY;
    acceptSwitch.on = NO;
    acceptSwitch.onTintColor = [UIColor flatSkyBlueColorDark];
    [contentScrollView addSubview:acceptSwitch];

    // Buttons
    submitButton = [[MainButton alloc] initWithFrame:CGRectMake(0, acceptLabel.bottom + 35, 240, 44)];
    [submitButton addTarget:self action:@selector(submitTapped) forControlEvents:UIControlEventTouchUpInside];
    submitButton.mainButtonStyle = MainButtonStyleOrange;
    submitButton.frame = CGRectMake(0, acceptLabel.bottom + 35, 240, 44);
    submitButton.title = NSLocalizedString(@"send_request", nil);
    [submitButton setEnabled:acceptSwitch.isOn];
    submitButton.centerX = contentScrollView.centerX;
    [contentScrollView addSubview:submitButton];

    [self setContentScrollViewContentHeight:submitButton.bottom + 20];
    [self addKeyboardControlFields:@[lastnameField, nameField, middlenameField, emailField, telField, datePicker, timeField]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end