//
//  RegistrationStep3ViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 26.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationStep3ViewController : UITableViewController

@property (nonatomic, copy) void (^didConfirm)();
@property (nonatomic, copy) void (^didTapConfirmLater)();

@property (nonatomic) BOOL isModal;

@end
