//
//  CardsDetailedStatisticsViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 4/14/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "UIView+ConcisePureLayout.h"
#import "CardsDetailedStatisticsViewController.h"

@interface CardsDetailedStatisticsViewController ()

@end

@implementation CardsDetailedStatisticsViewController

- (id)init {
    self = [super init];
    if(self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Cards" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([CardsDetailedStatisticsViewController class])];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];


    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor lightGrayColor];
    label.text = NSLocalizedString(@"section_uder_development", nil);
    [self.view addSubview:label];

    [label aa_centerInSuperview];
}

#pragma mark -

- (void)configUI {
    [self setNavigationBackButton];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
