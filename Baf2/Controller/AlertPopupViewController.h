//
// Created by Askar Mustafin on 4/8/16.
// Copyright (c) 2016 Банк Астаны. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertPopupViewController : UIViewController

@property (nonatomic, strong) NSString *titleString;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *textString;
@property (nonatomic, strong) NSString *buttonTitle;
@property (nonatomic, copy) void (^OnButtonClick)();

@end