//
// Created by Askar Mustafin on 7/20/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class OperationAccounts;

@interface FromExternalToAccountViewController : UITableViewController

+ (instancetype)createVC;
@property (nonatomic, copy) OperationAccounts *operationAccounts;

@end