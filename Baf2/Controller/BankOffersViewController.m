//
//  BankOffersViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 4/21/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

/*
 * Экран "Предложения  Банка"
 */

#import "BankOffersViewController.h"
#import "RequestCardTypesViewController.h"
#import "OrderCallRequestViewController.h"
#import "ExternalCardRegistrationViewController.h"
#import "BankOfferTableViewCell.h"
#import "LinkButton.h"
#import "NotificationCenterHelper.h"
#import "ChameleonMacros.h"
#import "User.h"
#import "InsuranceViewController.h"
#import "OnlineDepositCalculatorViewController.h"
#import "OnlineDepositMainViewController.h"
#import "OnlineDepositStartViewController.h"
#import <SDCycleScrollView.h>


@interface BankOffersViewController ()<UITableViewDataSource, UITableViewDelegate, SDCycleScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSArray *dummyBannerData;
@property (nonatomic, strong) NSArray *colors;

@end

@implementation BankOffersViewController

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BankOffers" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([BankOffersViewController class])];
}

- (void)initVars {

    if ([User sharedInstance].isLoggedIn) {
        self.data = @[
                      @{@"title" : @"Онлайн депозит", @"subtitle" : @"", @"image" : @"icn_online_dep"}, //todo: find f*cking image
                @{@"title" : NSLocalizedString(@"order_card_title", nil), @"subtitle" : @"", @"image": @"new_card"},
                @{@"title" : NSLocalizedString(@"connect_card_title", nil), @"subtitle" : NSLocalizedString(@"connect_card_value", nil), @"image": @"add_card"},
                @{@"title" : NSLocalizedString(@"order_call", nil), @"subtitle" : @"", @"image": @"order_call"},
                @{@"title" : @"Страхование", @"subtitle" : @"", @"image": @"insurance"}
        ];
    } else {
        self.data = @[

                      @{@"title" : NSLocalizedString(@"order_card_title", nil), @"subtitle" : @"", @"image": @"new_card"},
                      @{@"title" : NSLocalizedString(@"order_call", nil), @"subtitle" : @"", @"image": @"order_call"},
                      @{@"title" : @"Страхование", @"subtitle" : @"", @"image": @"insurance"}
                      ];
    }

    self.dummyBannerData = @[

            @"iphone-banner1",
            @"iphone-banner2",
            @"iphone-banner3",
            @"iphone-banner4",
            @"iphone-banner5"

    ];

    self.colors = @[[UIColor flatGreenColor], [UIColor flatBlueColorDark], [UIColor flatGrayColor], [UIColor flatOrangeColor], [UIColor flatPurpleColor]];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initVars];
    [self configUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - tableview delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"BankOfferTableViewCell";
    BankOfferTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) {
        cell = [[BankOfferTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *data = self.data[(NSUInteger) indexPath.row];
    
    cell.detailColorView.backgroundColor = self.colors[(NSUInteger) indexPath.row];
    if (data[@"image"]) {
        cell.detailImageView.image = [UIImage imageNamed:data[@"image"]];
    }
    [cell setTitle:data[@"title"] description:data[@"subtitle"]];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    if (![User sharedInstance].isLoggedIn) {
        
        if (indexPath.row == 0) {
            
            //RequestCardViewController *v = [[RequestCardViewController alloc] init];
            RequestCardTypesViewController *v = [[RequestCardTypesViewController alloc] initWithStyle:UITableViewStyleGrouped];
            [self.navigationController pushViewController:v animated:YES];
            
        } else if (indexPath.row == 1) {
            
            //открыть счет
            [self gotoCallcenterContactFormWithRequestType:BafRequestTypeOpenAccount animated:YES];
            
        } else if (indexPath.row == 2) {
            
            InsuranceViewController *v = [[InsuranceViewController alloc] init];
            [self.navigationController pushViewController:v animated:YES];

        }
        
    } else {

        if (indexPath.row == 0) {

            //Открыть депозит
            OnlineDepositStartViewController *vc = [OnlineDepositStartViewController createVC];
            vc.isNotForMenu = YES;
            [self.navigationController pushViewController:vc animated:YES];

        } else if (indexPath.row == 1) {

            //Заказать пластиковую карточку
            RequestCardTypesViewController *v = [[RequestCardTypesViewController alloc] initWithStyle:UITableViewStyleGrouped];
            [self.navigationController pushViewController:v animated:YES];

        } else if (indexPath.row == 2) {

            //зарегистрировать карту другого банка
            ExternalCardRegistrationViewController *v = [[ExternalCardRegistrationViewController alloc] init];
            [self.navigationController pushViewController:v animated:YES];

        } else if (indexPath.row == 3) {

            //открыть счет (через звонок в кол центр)
            [self gotoCallcenterContactFormWithRequestType:BafRequestTypeOpenAccount animated:YES];

        } else if (indexPath.row == 4) {
            
            //Страхование
            InsuranceViewController *v = [[InsuranceViewController alloc] init];
            [self.navigationController pushViewController:v animated:YES];
            
        } else if (indexPath.row == 5) {
            
            //получить кредит (через звонок в кол центр)
            [self gotoCallcenterContactFormWithRequestType:BafRequestTypeDeposit animated:YES];

        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, [ASize screenWidth], 150) shouldInfiniteLoop:true imageNamesGroup:self.dummyBannerData];
    cycleScrollView.autoScrollTimeInterval = 3;
    cycleScrollView.delegate = self;
    cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
    cycleScrollView.scrollDirection = UICollectionViewScrollDirectionHorizontal;

    return cycleScrollView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 120;
}


- (void)gotoCallcenterContactFormWithRequestType:(BafRequestType)requestType animated:(BOOL)animated
{
    OrderCallRequestViewController *v = [[OrderCallRequestViewController alloc] init];
    v.requestType = requestType;
    [self.navigationController pushViewController:v animated:animated];
}

#pragma mark - config ui

- (void)configUI {
    [self setBafBackground];
    
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 80;
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (self.isOpenedFromMenu) {
        [self showMenuButton];
        
        if (self.becomeClient) {
            [self setNavigationTitle:NSLocalizedString(@"become_client", nil)];
        } else {
            [self setNavigationTitle:NSLocalizedString(@"bank_offers", nil)];
        }
    }
    
    if (self.becomeClient) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 1)];
        headerView.backgroundColor = [UIColor whiteColor];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 16, [ASize screenWidth] - 20, 1)];
        label.text = NSLocalizedString(@"become_client", nil);
        label.font = [UIFont mediumHelveticaNeue:25];
        label.textColor = [UIColor fromRGB:0x087f87];
        [label sizeToFit];
        [headerView addSubview:label];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(20, label.bottom + 8, [ASize screenWidth] - 20, 1)];
        label2.text = NSLocalizedString(@"of_bank_astana", nil);
        label2.font = [UIFont helveticaNeueThin:25];
        label2.textColor = [UIColor fromRGB:0x087f87];
        [label2 sizeToFit];
        [headerView addSubview:label2];

        LinkButton *offersButton = [[LinkButton alloc] initWithLabel:NSLocalizedString(@"bank_sentence", nil)];
        [offersButton setTitleFont:[UIFont systemFontOfSize:14] textColor:[UIColor fromRGB:0x146888]];
        [offersButton onClick:^(id _self) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMenuOpenBankOffers object:nil];
        }];
        offersButton.x = 20;
        offersButton.y = label2.bottom + 10;

        headerView.height = offersButton.bottom + 10;
        [headerView addSubview:offersButton];
        
        self.tableView.tableHeaderView = headerView;
    }

    [self.tableView setContentInsetTop:10];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
