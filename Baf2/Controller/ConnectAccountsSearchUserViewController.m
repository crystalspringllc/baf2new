//
//  ConnectAccountsSearchUserViewController.m
//  BAF
//
//  Created by Almas Adilbek on 8/25/14.
//
//

#import "ConnectAccountsSearchUserViewController.h"
#import "AAField.h"
#import "AAForm.h"
#import "AccountsApi.h"
#import "AAField+Validation.h"
#import "UIView+Coordinate.h"
#import "MainButton.h"

@interface ConnectAccountsSearchUserViewController ()
@end

@implementation ConnectAccountsSearchUserViewController {
    AAField *iinField;
    AAField *telField;
    AAForm *form;
    MainButton *searchButton;
}

-(id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

-(void)initVars {}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
    [self configAction];

    [telField setValue:[User sharedInstance].phoneNumber];
    
    if (self.iin) {
        [iinField setValue:self.iin];
    }

    if (telField.value.length > 0 && iinField.value.length > 0) {
        [self searchTapped];
    }
}

- (void)configAction
{
    [iinField onValueChanged:^(NSString *value) {
//        [wSelf validateEmail];
    }];

    [telField onValueChanged:^(NSString *value) {
//        [wSelf validateTel];
    }];
}

#pragma mark -
#pragma mark Actions

- (void)searchTapped
{
    [self.view endEditing:YES];

    if(![form isRequiredFieldsFilled]) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"fill_all_required_fields", nil) message:@""];
        return;
    }

    if(![iinField validateIin]) return;
    if(![telField validatePhoneNumber]) return;

    NSString *telValue = [telField validPhoneNumber];

    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"search_users", nil) animated:true];

    [AccountsApi checkMyAccountsWithIin:[iinField value] tel:telValue success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];

        BOOL foundData = [response boolValue];
        if (foundData) {
            ConnectAccountsVerifyUserViewController *v = [[ConnectAccountsVerifyUserViewController alloc] init];
            v.delegate = self;
            v.mobilePhone = telValue;
            [self.navigationController pushViewController:v animated:YES];
        } else {
            [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"accounts_not_found", nil) message:NSLocalizedString(@"check_iin_correctness", nil)];
        }
    } failure:^(NSString *code, NSString *message) {

        [MBProgressHUD hideAstanaHUDAnimated:true];
        if ([code isEqualToString:@"926"]) {
            // CLIENT_NOT_FOUND

        }
    }];
}

#pragma mark -
#pragma mark ConnectAccountVerifyUserViewController

-(void)connectAccountsVerifyUserViewControllerSmsVerified
{
    [self.delegate connectAccountsSearchUserViewControllerAccountConnected];
}

#pragma mark -

- (void)configUI {
    self.contentScrollView.backgroundColor = [UIColor clearColor];
    
    [self setBafBackground];
    [self setNavigationTitle:NSLocalizedString(@"my_card_and_accounts", nil)];
    [self setNavigationBackButton];
    self.view.backgroundColor = [UIColor whiteColor];

    form = [[AAForm alloc] initWithScrollView:self.contentScrollView];

    // EMAIL
    iinField = [[AAField alloc] init];
    iinField.required = YES;
    [iinField setTitle:NSLocalizedString(@"your_iin", nil)];
    [iinField setPlaceholder:NSLocalizedString(@"enter_iin", nil)];
    [iinField setMaxCharacters:12];
    iinField.keyboardType = UIKeyboardTypeNumberPad;
    [form pushView:iinField];

    UILabel *telMessageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidthSidePadding:15], 1)];
    telMessageLabel.backgroundColor = [UIColor clearColor];
    telMessageLabel.font = [UIFont systemFontOfSize:14];
    telMessageLabel.textColor = [UIColor blackColor];
    telMessageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    telMessageLabel.numberOfLines = 0;
    telMessageLabel.text = NSLocalizedString(@"enter_phone_numebr", nil);
    [telMessageLabel sizeToFit];
    [form pushView:telMessageLabel marginLeft:17 marginTop:20];

    // TEL
    telField = [[AAField alloc] init];
    telField.required = YES;
    [telField setIcon:[UIImage imageNamed:@"icon-tel.png"]];
    [telField setTitle:NSLocalizedString(@"number_mobile_phone", nil)];
    [telField setPlaceholder:[NSString stringWithFormat:@"%@ 701XXXXXXX", NSLocalizedString(@"example", nil)]];
    [telField setPrefix:@"+7"];
    [telField setMaxCharacters:10]; // Tel length
    telField.keyboardType = UIKeyboardTypePhonePad;
    [form pushView:telField];

    // Register button
    searchButton = [[MainButton alloc] initWithFrame:CGRectMake(0, telField.bottom + 20, 150, 44)];
    searchButton.mainButtonStyle = MainButtonStyleOrange;
    searchButton.frame = CGRectMake(0, telField.bottom + 20, 150, 44);
    [searchButton centerH];
    [searchButton setTitle:NSLocalizedString(@"search", nil)];
    [searchButton addTarget:self action:@selector(searchTapped) forControlEvents:UIControlEventTouchUpInside];
    [form pushView:searchButton marginTop:25 centered:YES];

    [self setFormScrollViewHeight:[ASize screenHeightWithoutStatusBarAndNavigationBar]];

    // Keyboard bsKeyboardControls
    [form initKeyboardControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc {

}

@end
