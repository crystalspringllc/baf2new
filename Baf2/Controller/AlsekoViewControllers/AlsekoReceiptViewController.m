 //
// Created by Mustafin Askar on 10/09/15.
// Crystal Spring LLC ALL RIGHTS RESERVED
//

//  Главный контроллер для Алсеко
//  Загружается из storyboard'а
//  И сразу содержит в себе AMScrollView, а также такие элементы шапки, как
//  InvoiceNameLabel, InvoiceIcon, InfoButton, InvoiceID
//  Остальные элементы отрисовываются сразу после зашрузки квитанции
//

#import <HMSegmentedControl/HMSegmentedControl.h>
#import <ChameleonFramework/ChameleonMacros.h>
#import "AlsekoReceiptViewController.h"
#import "AlsekoServiceView.h"
#import "Invoice.h"
#import "InvoiceHistory.h"
#import "Service.h"
#import "AlsekoFormedAndExpireDatesView.h"
#import "AlsekoTotalPaySumView.h"
#import "Contract.h"
#import "AAActionSheetView.h"
#import "AlsekoInfoView.h"
#import "AAFieldActionSheet.h"
#import "NSString+Ext.h"
#import "NSDate+Ext.h"
#import "InvoiceApi.h"
#import "PaymentFormViewController.h"
#import "ContractsApi.h"
#import "ContractModelView.h"
#import "NotificationCenterHelper.h"
#import "OperationHistory.h"
#import "LastOperationDetailsViewController.h"

@interface AlsekoReceiptViewController () {}

@property (nonatomic, strong) NSMutableArray *invoiceHistories;
@property (nonatomic, strong) InvoiceHistory *invoiceHistory;
@property (nonatomic, strong) Invoice *invoice;
@property (nonatomic, strong) NSMutableArray *serviceViews;
@property (nonatomic, strong) AlsekoTotalPaySumView *alsekoTotalPaySumView;
@property (nonatomic, strong) UILabel *totalSumBottomLabel;
@property (nonatomic, strong) HMSegmentedControl *segmentedControl;
@property (nonatomic, strong) NSLayoutConstraint *segmentedControlConstraint;
@property (nonatomic, strong) NSNumber *totalSum;
@property (nonatomic, strong) NSString *currentInvoiceState;
@end

@implementation AlsekoReceiptViewController {
    NSString *formedDate;
}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Alseko" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self configSegmentedControl];
    [self loadInvoices];
}

#pragma mark - config actions

- (void)rejectInvoice:(BOOL)isRejectInvoice {

    __weak AlsekoReceiptViewController *wSelf = self;
    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];

    if (!isRejectInvoice) {
        [InvoiceApi setInvoiceRejectedByInvoiceContract:self.invoice.account
                                             contractId:self.contract.identifier
                                             providerId:self.contract.provider.paymentProviderId
                                             formedDate:formedDate
                                            rejectState:RejectStateAlreadyPaid
                                                success:^(id response) {
                                                    [MBProgressHUD hideAstanaHUDAnimated:true];
                                                    [wSelf loadInvoiceForHistories];
                                                } failure:^(NSString *code, NSString *message) {
                    [MBProgressHUD hideAstanaHUDAnimated:true];
                }];
    } else {
        [InvoiceApi cancelInvoiceRejectByInvoiceContract:self.invoice.account
                                              contractId:self.contract.identifier
                                              providerId:self.contract.provider.paymentProviderId
                                              formedDate:formedDate
                                                 success:^(id response) {
                                                     [MBProgressHUD hideAstanaHUDAnimated:true];
                                                     [wSelf loadInvoiceForHistories];

                                                 } failure:^(NSString *code, NSString *message) {
                    [MBProgressHUD hideAstanaHUDAnimated:true];
                }];
    }
}
 
- (void)deleteContract:(UIBarButtonItem *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"delete_contract", nil) preferredStyle:UIAlertControllerStyleAlert];
    alertController.popoverPresentationController.barButtonItem = sender;
    
    __weak AlsekoReceiptViewController *weakSelf = self;
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *_Nonnull action) {
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"deleting_contract", nil) animated:true];

        [ContractsApi deleteContract:weakSelf.contract.identifier success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:true];

            [weakSelf goBack];

            [[ContractModelView sharedInstance] deleteLocalContractWithIdentifier:weakSelf.contract.identifier];

            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentTemplateDeleted object:weakSelf.contract.identifier];
        }                    failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:alertController animated:true completion:nil];
}

/**
*
*   Config History ActionSheet
*
*/

- (void)configHistoryActionSheet {

    __weak AlsekoReceiptViewController *wSelf = self;

    AAFieldActionSheet *actionSheet = [[AAFieldActionSheet alloc] initWithTitle:NSLocalizedString(@"history_payment", nil)];
    NSMutableArray *histories = [[NSMutableArray alloc] init];

    if (self.invoiceHistories.count > 0) {
        for (InvoiceHistory *invoiceHistory1 in self.invoiceHistories) {
            NSDictionary *historyOption = @{@"id" : invoiceHistory1.invoiceId, @"title" : [NSString stringWithFormat:@"%@ %@  (%@)", NSLocalizedString(@"receipt_for", nil),[invoiceHistory1 getInvoiceForMonthString], [invoiceHistory1 getStateMessage]]};
            [histories addObject:historyOption];
        }
        actionSheet.options = histories;
        [actionSheet onValueChange:^(NSDictionary *option, NSInteger index) {

            wSelf.invoiceHistory = wSelf.invoiceHistories[(NSUInteger) index];

            [wSelf loadInvoiceForHistories];

        }];
        [actionSheet show];
    }
}

- (void)loadInvoiceForHistories {
    __weak AlsekoReceiptViewController *wSelf = self;

    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
    [wSelf loadInvoiceById:wSelf.invoiceHistory.invoiceId completionBlock:^(id response2) {
        NSDictionary *responseDictionary = response2;

        //Р
        if (responseDictionary[@"invoiceObj"]) {

            wSelf.segmentedControlConstraint.constant = 44;

            NSString *updatedDateString = responseDictionary[@"updated"];
            updatedDateString = [updatedDateString stringByReplacingOccurrencesOfString:@" AM" withString:@""];
            updatedDateString = [updatedDateString stringByReplacingOccurrencesOfString:@" PM" withString:@""];
            NSDate *updatedDate = [updatedDateString dateWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *updateDateStandardFormatString = [updatedDate stringWithDateFormat:@"dd.MM.yyyy"];

            wSelf.segmentedControl.sectionTitles = @[NSLocalizedString(@"original_bill", nil), [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"saved", nil), updateDateStandardFormatString]];

        } else {
            wSelf.segmentedControlConstraint.constant = 0;
        }

        [wSelf.scrollView removeAllPushsedViews];
        //Отрисовать вьюшки для прилетевших сервисов
        [wSelf buildServiceViews];
        //Посчитать и отобразить общую сумму в header и bottom view
        [wSelf calcAndDisplayTotalSumWithService];

        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

/**
*
*   Go to pay
*
*/

- (void)payForInvoice {
     __weak AlsekoReceiptViewController *wSelf = self;

    [self saveInvoiceWithHud:YES completion:^(BOOL isSuccess) {
        if (isSuccess) {

            if (wSelf.segmentedControl.selectedSegmentIndex == 0) {
                [wSelf.segmentedControl setSelectedSegmentIndex:1 animated:YES];
                [wSelf segmentedControlChangedValue:wSelf.segmentedControl];
            }

            [MBProgressHUD showAstanaHUDWithTitle:@"Подготовка к оплате" animated:YES];
            [InvoiceApi getInvoiceById:wSelf.invoiceHistory.invoiceId success:^(id response) {

                NSDictionary *invoiceObj = response[@"invoiceObj"];
                NSDictionary *services = invoiceObj[@"services"];

                if (services.count > 0) {

                    [wSelf calcAndDisplayTotalSumWithService];

                    PaymentFormViewController *v = [PaymentFormViewController createVC];
                    //v.isAlsekoPayment = YES;
                    v.actionType = ActionTypeMakePayment;
                    v.paymentProvider = wSelf.contract.provider;
                    v.contract = wSelf.contract;
                    v.inputAmount = wSelf.totalSum;
                    v.invoiceData = [response[@"invoiceObj"] JSONRepresentation];
                    v.invoiceId = wSelf.invoiceHistory.invoiceId;
                    [wSelf.navigationController pushViewController:v animated:YES];
                } else {
                    [UIHelper showAlertWithMessage:@"Нет квитанции для оплаты"];
                }

                [MBProgressHUD hideAstanaHUDAnimated:YES];

            } failure:^(NSString *code, NSString *message) {

                [MBProgressHUD hideAstanaHUDAnimated:YES];

            }];
        }
    }];
}

/**
*  Compose parameters and save invoice
*/

- (void)saveInvoice {
     __weak AlsekoReceiptViewController *wSelf = self;

    [self saveInvoiceWithHud:YES completion:^(BOOL isSuccess) {
        if (isSuccess) {

            [wSelf.segmentedControl setSelectedSegmentIndex:1 animated:YES];
            [wSelf segmentedControlChangedValue:wSelf.segmentedControl];
        }
    }];
}

/**
*
* Save invoice
*
*/

- (void)saveInvoiceWithHud:(BOOL)withHud completion:(void (^)(BOOL isSuccess))completion {
    __weak AlsekoReceiptViewController *wSelf = self;

    if (withHud) [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"saving_receipt", nil) animated:true];

     NSMutableArray *services = [[NSMutableArray alloc] init];
     for (AlsekoServiceView *serviceView in self.serviceViews) {
         Service *service = serviceView.getServiceDataToBeSaved;
         [services addObject:service];
     }

     NSDictionary *invoiceJSON = [self.invoice repreentJSONWithServices:services];

     [InvoiceApi saveInvoiceWithInvoiceContract:self.contract.contract
                                       invoice:[invoiceJSON JSONRepresentation]
                                    providerId:self.contract.provider.paymentProviderId
                                    contractId:self.contract.identifier
                                       success:^(id response)
     {

         if (withHud) {
             [MBProgressHUD hideAstanaHUDAnimated:YES];
             [Toast showToast:NSLocalizedString(@"receipt_saved", nil)];
         }

         [wSelf loadInvoiceById:response[@"invoiceId"] completionBlock:^(id response2) {
             NSDictionary *responseDictionary = response2;


             //Р
             if (responseDictionary[@"invoiceObj"]) {
                 wSelf.segmentedControlConstraint.constant = 44;

                 NSString *updatedDateString = responseDictionary[@"updated"];
                 NSDate *updatedDate = [updatedDateString dateWithDateFormat:@"yyyy-MM-dd hh:mm:ss"];
                 NSString *updateDateStandardFormatString = [updatedDate stringWithDateFormat:@"dd.MM.yyyy"];

                 NSString *secondSection = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"saved", nil),updateDateStandardFormatString];
                 wSelf.segmentedControl.sectionTitles = @[NSLocalizedString(@"original_bill", nil), secondSection];

             } else {

                 wSelf.segmentedControlConstraint.constant = 0;

             }

             [wSelf.scrollView removeAllPushsedViews];
             //Отрисовать вьюшки для прилетевших сервисов
             [wSelf buildServiceViews];
             //Посчитать и отобразить общую сумму в header и bottom view
             [wSelf calcAndDisplayTotalSumWithService];

             [MBProgressHUD hideAstanaHUDAnimated:true];

             if (completion) {
                 completion(YES);
             }

         }];

     } failure:^(NSString *code, NSString *message)
     {
         if ([code isEqualToString:@"AUTH_SUCCESS"]) {

             [Toast showToast:NSLocalizedString(@"could_not_keep_receipt", nil)];

             [InvoiceApi getServiceLogIdByInvoiceId:self.invoiceHistory.invoiceId
                                            success:^(NSNumber *serviceLogId) {

                                                [MBProgressHUD hideAstanaHUDAnimated:true];


                                                [self showOperationHistoryDetail:serviceLogId];



                                            } failure:^(NSString *code2, NSString *message2) {
                         [MBProgressHUD hideAstanaHUDAnimated:true];
                     }];
         } else {
             if (completion) {
                 completion(NO);
             }

             if (withHud) {
                 [Toast showToast:NSLocalizedString(@"could_not_keep_receipt", nil)];
                 [MBProgressHUD hideAstanaHUDAnimated:true];
             }
         }
     }];
}

- (void)showOperationHistoryDetail:(NSNumber *)serviceLogId {
    OperationHistory *o = [[OperationHistory alloc] init];
    o.operationId = serviceLogId;
    LastOperationDetailsViewController *v = [[LastOperationDetailsViewController alloc] init];
    v.operation = o;
    v.newVersion = NO;
    [self.navigationController pushViewController:v animated:YES];
}

/**
*
*   Display action sheet with Alseko contract information
*
*/

- (IBAction)infoAction:(id)sender {
    AAActionSheetView *infoSheet = [[AAActionSheetView alloc] initWithTitle:@" "];
    infoSheet.backgroundView.backgroundColor = [UIColor whiteColor];
    AlsekoInfoView *alsekoInfoView = [[AlsekoInfoView alloc] initWithInvoice:self.invoice
                                                                    contract:self.contract];
    [infoSheet setContentView:alsekoInfoView];
    [infoSheet show];
}

/**
*
*   Method loads firstly invoice history
*
*/

- (void)loadInvoices {
    __weak AlsekoReceiptViewController *wSelf = self;

    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];

    //Вродекак будет прилетать только 1 элемент в массиве,
    //поэтому сразу берем invoiceId 1ого элемента и дергаем getInvoiceById
    //Затем отрисовываем вьюшки для прилетевших сервисов
    [InvoiceApi getInvoiceIdRecentByContract:self.contract.contract
                                  providerId:self.contract.provider.paymentProviderId
                                     success:^(id response) {
                                         
        [InvoiceApi getInvoicesHistory:self.contract.contract
                            providerId:self.contract.provider.paymentProviderId
                               success:^(id response2) {

            //[MBProgressHUD hideAstanaHUDAnimated:true];

            id invoices = [response2 objectForKey:@"invoices"];
            if (![invoices isKindOfClass:[NSArray class]]) {
                [UIAlertView showWithTitle:nil message:NSLocalizedString(@"could_not_load_receipt", nil) cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                [MBProgressHUD hideAstanaHUDAnimated:true];
                return;
            }

            //Храним историю
            for (NSDictionary *invoiceDict in invoices) {
                InvoiceHistory *invoiceHistoryInstance = [InvoiceHistory instanceFromDictionary:invoiceDict];
                if (!self.invoiceHistories) {
                    self.invoiceHistories = [[NSMutableArray alloc] init];
                }
                [self.invoiceHistories addObject:invoiceHistoryInstance];
            }
            wSelf.invoiceHistory = self.invoiceHistories[0];

            //Подтягиваем invoice по invoiceId полученный с историей
            [self loadInvoiceById:wSelf.invoiceHistory.invoiceId completionBlock:^(id response3) {

                NSDictionary *responseDictionary = response3;

                //Р
                if (responseDictionary[@"invoiceObj"]) {
                    wSelf.segmentedControlConstraint.constant = 44;

                    NSString *updatedDateString = responseDictionary[@"updated"];
                    updatedDateString = [updatedDateString stringByReplacingOccurrencesOfString:@" AM" withString:@""];
                    updatedDateString = [updatedDateString stringByReplacingOccurrencesOfString:@" PM" withString:@""];
                    NSDate *updatedDate = [updatedDateString dateWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSString *updateDateStandardFormatString = [updatedDate stringWithDateFormat:@"dd.MM.yyyy"];

                    wSelf.segmentedControl.sectionTitles = @[NSLocalizedString(@"original_bill", nil), [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"saved", nil),updateDateStandardFormatString]];

                } else {
                    wSelf.segmentedControlConstraint.constant = 0;
                }

                //[wSelf.scrollView removeAllPushsedViews];
                //Отрисовать вьюшки для прилетевших сервисов
                [wSelf buildServiceViews];
                //Посчитать и отобразить общую сумму в header и bottom view
                [wSelf calcAndDisplayTotalSumWithService];

                [MBProgressHUD hideAstanaHUDAnimated:true];
            }];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];
    } failure:^(NSString *code, NSString *message) {
            [UIHelper showAlertWithMessage:NSLocalizedString(@"could_not_load_receipt", nil)];
            [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

/**
*   Load invoice by id callback response
*
*/

- (void)loadInvoiceById:(NSNumber *)invoiceId completionBlock:(void (^)(id response2))completion {
    __weak AlsekoReceiptViewController *wSelf = self;

    [InvoiceApi getInvoiceById:invoiceId success:^(id response2) {
        //Храним полученный обьект в проперти invoice
        
        if (response2[@"invoiceObj"]) {
            id invoiceObj = response2[@"invoiceObj"];
            formedDate = invoiceObj[@"formedDate"];
        } else {
            id invoiceObj = response2[@"originalObj"];
            formedDate = invoiceObj[@"formedDate"];
        }

        wSelf.currentInvoiceState = response2[@"invoiceState"];

        NSString *jsonKey;
        if (self.segmentedControl.selectedSegmentIndex == 0 ) {
            jsonKey = @"originalObj";
        } else {
            jsonKey = @"invoiceObj";
        }
        wSelf.invoice = [Invoice instanceFromDictionary:response2[jsonKey]];

            completion(response2);

        } failure:^(NSString *code, NSString *message) {
        }];
}

/**
*  Draw total sum in alsekoTotalPaySumView and totalSumBottomLabel
*/

- (void)calcAndDisplayTotalSumWithService {
    double totalSum = 0;
    for (AlsekoServiceView *alsekoServiceView in self.serviceViews) {
        if (alsekoServiceView.isIncludeBill) {
            totalSum = totalSum + [alsekoServiceView.totalSum doubleValue];
        } else {
            
        }
    }
    self.alsekoTotalPaySumView.paySum = totalSum;
    self.totalSumBottomLabel.text = [NSString stringWithFormat:@"%@ %@ KZT", NSLocalizedString(@"bill_total_sum", nil), [@(totalSum) decimalFormatString]];

    self.totalSum = @(totalSum);
}

#pragma mark - HMSegmentedControl value changed

/**
*  Segment delegate method
*
*/

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControlChangedValue {
    __weak AlsekoReceiptViewController *wSelf = self;

    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
    [self loadInvoiceById:self.invoiceHistory.invoiceId completionBlock:^(id response2) {
        NSDictionary *responseDictionary = response2;

        if (segmentedControlChangedValue.selectedSegmentIndex == 0) {
            wSelf.invoice = [Invoice instanceFromDictionary:responseDictionary[@"originalObj"]];
        } else {
            wSelf.invoice = [Invoice instanceFromDictionary:responseDictionary[@"invoiceObj"]];
        }


        //remove all pushed views
        [wSelf.scrollView removeAllPushsedViews];


        [wSelf buildServiceViews];
        [wSelf calcAndDisplayTotalSumWithService];

        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark - config ui

- (void)configUI {
    [self setNavigationBackButton];
    self.view.backgroundColor = [UIColor fromRGB:0xF1F1F1];
    [self setNavigationTitle:self.contract.alias];
    [self configHeader];

    // Trash button.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteContract:)];
}

- (void)configHeader {
    //Если providerId = 6, то это invoiceNameLabel = Алсеко
    self.invoiceNameLabel.text = self.contract.provider.name;

    [self.invoiceIcon sd_setImageWithURL:[[NSURL alloc] initWithString:self.contract.provider.logo]
                        placeholderImage:[UIImage imageNamed:@"icon-icon-placeholder"]];
}

- (void)configSegmentedControl {
    self.segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[NSLocalizedString(@"original_bill", nil), NSLocalizedString(@"saved", nil)]];
    self.segmentedControl.titleTextAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:13],
            NSForegroundColorAttributeName : [UIColor blackColor]};
    self.segmentedControl.selectionIndicatorColor = [UIColor flatBlueColor];
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;

    self.segmentedControlConstraint = [self.segmentedControl autoSetDimension:ALDimensionHeight toSize:0];
    [self.segmentedControl autoSetDimension:ALDimensionWidth toSize:[ASize screenWidth]];
    [self.segmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.scrollView pushView:self.segmentedControl marginTop:1 centered:YES];
}

/**
*  Метод для построения вьюшек для Алсеко, на основе загруженных данных квитанции
*  Вьюшки добавляются на AMScrollView через метод pushView
*/

- (void)buildServiceViews {
    __weak AlsekoReceiptViewController *wSelf = self;
    [self.serviceViews removeAllObjects];

    self.invoiceId.text = self.contract.contract;

    //Formed and Expired dates view
    AlsekoFormedAndExpireDatesView *alsekoFormedAndExpireDatesView = [[AlsekoFormedAndExpireDatesView alloc] initWithInvoice:self.invoice invoiceState:self.currentInvoiceState];
    if (self.invoiceHistories.count > 1) {
        alsekoFormedAndExpireDatesView.onOpenHistoryButtonTap = ^{

            [wSelf configHistoryActionSheet];

        };
    }
    [self.scrollView pushView:alsekoFormedAndExpireDatesView marginTop:1 centered:YES];


    //Total pay sum view
    self.alsekoTotalPaySumView = [[AlsekoTotalPaySumView alloc] initWithTotalPaySum:[self.invoice calcTotalSum]];
    [self.alsekoTotalPaySumView setInvoiceHistoryState:self.currentInvoiceState];
    self.alsekoTotalPaySumView.onGoToPayButtonClick = ^{
        [wSelf payForInvoice];
    };
    self.alsekoTotalPaySumView.onSaveButtonClick = ^{
        [wSelf saveInvoice];
    };
    self.alsekoTotalPaySumView.onRejectButtonClick = ^(BOOL isRejectInvoice) {
        [wSelf rejectInvoice:isRejectInvoice];
    };
    self.alsekoTotalPaySumView.paySum = [[self.initialAmount numberDecimalFormat] doubleValue];
    [self.scrollView pushView:self.alsekoTotalPaySumView marginTop:1 centered:YES];


    //Service views
    for (Service *service in self.invoice.services) {

        AlsekoServiceView *alsekoServiceView = [[AlsekoServiceView alloc] initWithService:service andNumberOfLivings:[self.invoice getNumberOfLivingsParam]];
        [self.scrollView pushView:alsekoServiceView marginTop:1 centered:YES];
        alsekoServiceView.onIncludeInBillValueChanged = ^(BOOL switchState, AlsekoServiceView *alsekoServiceView1) {
            [wSelf calcAndDisplayTotalSumWithService];
        };

        alsekoServiceView.onTotalSumValueChanged = ^{
            [wSelf calcAndDisplayTotalSumWithService];
        };
        
        
        if (!self.serviceViews) {
            self.serviceViews = [[NSMutableArray alloc] init];
        }
        [self.serviceViews addObject:alsekoServiceView];
    }

    //bottom total pay sum view
    self.totalSumBottomLabel = [[UILabel alloc] init];
    self.totalSumBottomLabel.textColor = [UIColor grayColor];
    self.totalSumBottomLabel.font = [UIFont boldSystemFontOfSize:13];
    [self.scrollView pushView:self.totalSumBottomLabel marginTop:30 centered:YES];

    [wSelf.view layoutIfNeeded];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {}

@end
