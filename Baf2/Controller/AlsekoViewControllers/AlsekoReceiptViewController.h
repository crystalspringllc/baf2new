//
// Created by Mustafin Askar on 10/09/15.
//

#import <Foundation/Foundation.h>
#import "AMScrollView.h"

@class Contract;
@class AlsekoTotalPaySumView;
@class HMSegmentedControl;


@interface AlsekoReceiptViewController : UIViewController

+ (instancetype)createVC;

@property (nonatomic, strong) Contract *contract;
@property (nonatomic, copy) NSString *initialAmount;


/**
*  Элементы загруженные из storyboad'a
*
*/
@property (nonatomic, weak) IBOutlet AMScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *invoiceIcon;
@property (weak, nonatomic) IBOutlet UILabel *invoiceNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceId;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;

@end
