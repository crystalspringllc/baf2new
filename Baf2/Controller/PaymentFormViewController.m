//
//  PaymentFormViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 4/27/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "PaymentFormViewController.h"

#import "CardsApi.h"
#import "ContractsApi.h"
#import "AccountsApi.h"
#import "PaymentsApi.h"
#import "Contract.h"

#import "STPopupController.h"
#import "STPopupController+Extensions.h"
#import "MainButton.h"
#import "ActionSheetListPopupViewController.h"
#import "SMSConfirmationPopupViewController.h"
#import "ExternalCardRegistrationViewController.h"
#import "PaymentPaycheckViewController.h"
#import "Paycheck.h"
#import "PhoneContactsViewController.h"
#import "OperationAccounts.h"
#import "Accounts.h"
#import "ExtCard.h"
#import "ContractModelView.h"
#import "TextFieldTableViewCell.h"
#import "NotificationCenterHelper.h"
#import "UITableViewController+Extension.h"
#import "SubContract.h"
#import "NSString+Ext.h"
#import "ENTCheckAccountResult.h"
#import <UITextField+Blocks.h>
#import <VMaskTextField.h>

#define kProcessingBankBAF @"ow_baf"


@interface PaymentFormViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView                  *contractIconImageView;
@property (weak, nonatomic) IBOutlet UILabel                      *contractTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel                      *contractSubtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel                      *contractAliasLabel;
@property (weak, nonatomic) IBOutlet UILabel                      *contractFormatLabel;
@property (weak, nonatomic) IBOutlet UILabel                      *disclaimerLabel;
@property (weak, nonatomic) IBOutlet UIImageView                  *cardIconImageView;
@property (weak, nonatomic) IBOutlet UILabel                      *cardTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel                      *cardNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel                      *cardBalanceLabel;
@property (weak, nonatomic) IBOutlet UIImageView                  *cardDisclosureImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView      *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UILabel                      *comisionLabel;
@property (weak, nonatomic) IBOutlet UILabel                      *totalSumLabel;
@property (weak, nonatomic) IBOutlet UIButton                     *selectContactButton;
@property (nonatomic, strong) IBOutlet UILabel                    *phoneTitleLabel;


@property (nonatomic, strong) IBOutlet VMaskTextField             *phoneTextField;
@property (nonatomic, strong) IBOutlet TextFieldTableViewCell     *phoneCell;
@property (weak, nonatomic) IBOutlet TextFieldTableViewCell       *templateNameCell;
@property (nonatomic, strong) IBOutlet UITextField                *amountTextField;
@property (nonatomic, strong) IBOutlet UITextField                *templateNameTextField;
@property (weak, nonatomic) IBOutlet VMaskTextField *entAttemptsTextField;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView    *templateNameSpinner;
@property (nonatomic, strong) IBOutlet MainButton                 *payButton;
@property (nonatomic, strong) IBOutlet MainButton                 *saveTemplateButton;
@property (weak, nonatomic) IBOutlet UILabel                      *subcontractLabel;
@property (weak, nonatomic) IBOutlet UIImageView                  *subcontractDisclosure;


@property (nonatomic, strong) id                                  selectedCard;
@property (nonatomic, strong) NSArray                             *bankCards;
@property (nonatomic, strong) NSArray                             *registeredCards;
@property (nonatomic, copy) NSString                              *acquiring;
@property (nonatomic, strong) NSString                            *info2;
@property (nonatomic, strong) OperationAccounts                   *operationAccounts;
@property (nonatomic, strong) NSString                            *disclaimer;

@property (nonatomic, strong) NSNumber                            *comission;

//For security subcontracts
@property (nonatomic, strong) NSArray                             *subContractList;
@property (nonatomic, strong) SubContract                         *selectedSubcontract;

@end

@implementation PaymentFormViewController

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Payments" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PaymentFormViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configUI];
    [self passDataToViews];
    [self loadClientAllCardsWithBankCards];
    [self configActions];

    [self checkAccount];

    if (self.actionType == ActionTypeMakePayment) {
        int infoRequestType = [self.contract.provider.infoRequestType intValue];
        if (infoRequestType == InfoRequestTypeGetSubContract ||
                infoRequestType == InfoRequestTypeSelectContract) {
            self.paymentProvider = self.contract.provider;
            [self loadSubContractsWithContractNumber:self.contract.contract];
        }
    }

    if (self.paymentProvider.getInfoRequestTypeEnum == InfoRequestTypeEnt ||
            self.contract.provider.getInfoRequestTypeEnum == InfoRequestTypeEnt) {
        self.amountTextField.enabled = NO;
        [self checkContractForEnt];
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.contractFormatLabel.preferredMaxLayoutWidth = self.contractFormatLabel.frame.size.width;
}

#pragma mark - loading request

/**
 * Reqest payments/check_account method if category = Охранные услуги id 6
 * Assing respons's subcontract list to global proerty self.subContractList
 * Then shows first element in subcontract list in cell
 * This method call in "textFieldDidEndEditing"
 *
 * @param contractNumber - string from textfield
 */
- (void)loadSubContractsWithContractNumber:(NSString *)contractNumber {
    if (([self.contract.provider.infoRequestType intValue] == 5 || [self.contract.provider.infoRequestType intValue] == 6) ||
            ([self.paymentProvider.infoRequestType intValue] == 5 || [self.paymentProvider.infoRequestType intValue] == 6))
    {

        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"checking_contract", nil) animated:YES];
        [PaymentsApi checkAccount:self.paymentProvider.paymentProviderId
                         contract:contractNumber
                          success:^(id response) {
                              [MBProgressHUD hideAstanaHUDAnimated:YES];

                              if ([response[@"state"] isEqualToString:@"ACCEPTED"] &&
                                      response[@"subContracts"] &&
                                      response[@"subContracts"][@"subContractList"])
                              {
                                  NSArray *subContracts = response[@"subContracts"][@"subContractList"];
                                  NSMutableArray *parsedSC = [[NSMutableArray alloc] init];
                                  for (NSDictionary *sc in subContracts) {
                                      SubContract *scInstance = [SubContract instanceFromDictionary:sc];
                                      if (scInstance.canPay) {
                                          [parsedSC addObject:scInstance];
                                      }
                                  }
                                  self.subContractList = parsedSC;
                                  [self showSubContract:self.subContractList.firstObject];
                                  [self.tableView reloadData];
                              }
                          }
                          failure:^(NSString *code, NSString *message) {
                              [MBProgressHUD hideAstanaHUDAnimated:YES];
                              [self.navigationController popViewControllerAnimated:YES];
                          }];
    }
}

- (void)loadClientAllCardsWithBankCards {
    // Get Bank cards and other bank cards.
    self.activityIndicatorView.hidden = NO;
    [self.activityIndicatorView startAnimating];
    
    __weak PaymentFormViewController *weakSelf = self;
    [AccountsApi getOperationAccountsWithSuccess:^(OperationAccounts *operationAccounts) {
        weakSelf.operationAccounts = operationAccounts;
        
        self.activityIndicatorView.hidden = YES;
        [self.activityIndicatorView stopAnimating];
        
        // Get bank cards.
        self.bankCards = operationAccounts.sourceAccounts.cards;
        
        // Get other bank cards.
        NSMutableArray *extCards = [NSMutableArray new];
        for (ExtCard *extCard in operationAccounts.sourceAccounts.extCards) {
            if (extCard.epayApproved) {
                [extCards addObject:extCard];
            }
        }
        self.registeredCards = extCards;

        if (self.bankCards.count == 0 && self.registeredCards.count == 0) {
            self.cardTitleLabel.text = NSLocalizedString(@"no_cards", nil);
        } else {
            self.cardTitleLabel.text = NSLocalizedString(@"choose_card", nil);
        }
    } failure:^(NSString *code, NSString *message) {
        self.activityIndicatorView.hidden = YES;
        [self.activityIndicatorView stopAnimating];
        
        self.cardTitleLabel.text = NSLocalizedString(@"not_able_to_load_cards", nil);
        
        [self showErrorMessageAndGoBack];
    }];
}

- (void)checkAccount {
    if ([self.contract.provider.infoRequestType integerValue] == ONAY_INFO_REQUEST_TYPE
            || [self.contract.provider.paymentProviderId integerValue] == KAZAKHTELECOM_ID) {

        __weak PaymentFormViewController *weakSelf = self;
        NSNumber *providerId = self.contract ? self.contract.provider.paymentProviderId : self.paymentProvider.paymentProviderId;
        NSString *contract = self.contract ? self.contract.contract : self.phoneTextField.text;

        [PaymentsApi checkAccount:providerId
                         contract:contract
                          success:^(id response) {

            if ([response[@"state"] isEqualToString:@"REJECTED"]) {
                NSString *message = response[@"reason"];
                [UIHelper showAlertWithMessage:message];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }

            if (response[@"info"]) {
                weakSelf.disclaimer = response[@"info"];
                weakSelf.info2 = response[@"info2"];
                
                if (response[@"amount"]) {
                    weakSelf.amountTextField.text = [response[@"amount"] stringValue];
                }
                
                [weakSelf.tableView reloadData];
            }
        } failure:^(NSString *code, NSString *message) {
        }];
    }
}

- (void)savePaymentTemplate:(UIButton *)sender {
    if (![User isEmailAndPhoneConfirmedM]) { return; }
    
    if (self.phoneTextField.text.length == 0 || self.templateNameTextField.text.length == 0) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"all_filelds_are_required", nil)];
        return;
    }

    NSNumber *providerId = self.contract ? self.contract.provider.paymentProviderId : self.paymentProvider.paymentProviderId;
    
    if (self.actionType == ActionTypeNewPayment) {

        NSString *textFieldText = self.phoneTextField.text;
        if (self.providerCategoryId == 1) {
            textFieldText = [textFieldText preparePhone];
        }

        for (Contract *oldContract in [ContractModelView sharedInstance].contracts) {
            if ([oldContract.contract isEqualToString:textFieldText] &&
                    [oldContract.provider.paymentProviderId isEqualToNumber:providerId]) {
                [Toast showToast:NSLocalizedString(@"that_contract_already_saved", nil)];
                return;
            }
        }

        if (self.providerCategoryId == 1) {

            [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"checking", nil) animated:true];
            [PaymentsApi checkAccount:providerId contract:textFieldText success:^(id response) {
                if ([response[@"state"] isEqualToString:@"ACCEPTED"]) {

                    [self addContract:textFieldText providerId:providerId];

                } else {
                    [Toast showToast:response[@"reason"]];
                    [MBProgressHUD hideAstanaHUDAnimated:true];
                }
            } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:true];

            }];

        } else {
            [self addContract:textFieldText providerId:providerId];
        }
    }
}

- (void)addContract:(NSString *)contract providerId:(NSNumber *)providerId {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"saving_template", nil) animated:true];

    [ContractsApi addContract:providerId
                     contract:contract
                contractAlias:self.templateNameTextField.text success:^(id response1) {
                [MBProgressHUD hideAstanaHUDAnimated:true];

                [Toast showToast:NSLocalizedString(@"payment_template_successfully_added", nil)];
                [self.navigationController popToRootViewControllerAnimated:true];

                [[ContractModelView sharedInstance] setNeedsReload:true];

                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentTemplatesUpdated object:nil];
            } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:true];
            }];
}

- (void)deletePaymentTemplate:(UIBarButtonItem *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"delete_contract", nil) preferredStyle:UIAlertControllerStyleAlert];
    alertController.popoverPresentationController.barButtonItem = sender;
    
    __weak PaymentFormViewController *weakSelf = self;
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *_Nonnull action) {
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"deleting_contract", nil) animated:true];

        [ContractsApi deleteContract:weakSelf.contract.identifier success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:true];

            [weakSelf goBack];

            [[ContractModelView sharedInstance] deleteLocalContractWithIdentifier:weakSelf.contract.identifier];

            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentTemplateDeleted object:weakSelf.contract.identifier];
        }                    failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancellation", nil) style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:alertController animated:true completion:nil];
}

- (void)updatePaymentTemplateAlias:(NSString *)newAlias {
    if (self.actionType == ActionTypeNewPayment) {
        return;
    }
    
    self.templateNameTextField.enabled = false;
    [self.payButton setMainButtonStyle:MainButtonStyleDisable];
    [self.saveTemplateButton setMainButtonStyle:MainButtonStyleDisable];
    
    self.templateNameSpinner.hidden = false;
    [self.templateNameSpinner startAnimating];
    
    __weak PaymentFormViewController *weakSelf = self;

    [ContractsApi updateContractAlias:self.contract.identifier alias:newAlias success:^(id response) {
        [Toast showToast:@"Название шаблона успешно обновлено"];
        [weakSelf.templateNameSpinner stopAnimating];
        weakSelf.templateNameSpinner.hidden = true;
        
        weakSelf.templateNameTextField.enabled = true;
        
        if (weakSelf.comisionLabel.text && weakSelf.comisionLabel.text.length > 0) {
            [weakSelf.payButton setMainButtonStyle:MainButtonStyleOrange];
            [weakSelf.saveTemplateButton setMainButtonStyle:MainButtonStyleOrange];
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSString *code, NSString *message) {
        [weakSelf.templateNameSpinner stopAnimating];
        weakSelf.templateNameSpinner.hidden = true;
        
        weakSelf.templateNameTextField.enabled = true;
        
        if (weakSelf.comisionLabel.text && weakSelf.comisionLabel.text.length > 0) {
            [weakSelf.payButton setMainButtonStyle:MainButtonStyleOrange];
            [weakSelf.saveTemplateButton setMainButtonStyle:MainButtonStyleOrange];
        }
        [weakSelf.tableView reloadData];
    }];
}

#pragma mark - config actions

/**
 *
 * Show template textfield on templateSwitchOn
 *
 */
- (void)onSaveTemplateSwitchOn {
    [self.tableView reloadData];
}


/**
 *
 *  call getCommission method on paymetSumTextField value changed
 *
 */
- (void)configActions {
    __weak PaymentFormViewController *weakSelf = self;
    self.amountTextField.didEndEditingBlock = ^(UITextField *textField) {
        if (weakSelf.selectedCard != nil && textField.text.length > 0) {
            [weakSelf countCommission];
        }
    };
}

/**
 * Count commission
 */
- (void)countCommission {

    NSNumber *amount = [self.amountTextField.text numberDecimalFormat];
    NSString *providerCode = self.contract ? self.contract.provider.providerCode : self.paymentProvider.providerCode;
    NSString *cardType;
    NSString *bank;
    NSNumber *accountId;
    NSString *accountType;

    if ([self.selectedCard isKindOfClass:[Card class]]) {
        cardType = ((Card *)self.selectedCard).cardType;
        self.acquiring = kProcessingBankBAF;
        bank = @"";
        Card *card = self.selectedCard;
        accountId = card.cardId;
        accountType = card.type;

    } else if ([self.selectedCard isKindOfClass:[NSString class]]) {

        if ([self.selectedCard isEqualToString:kWithoutCardRegister]) {
            cardType = @"any";
            self.acquiring = kProcessingBankKkb;
            bank = @"any";
            accountType = @"any";
            accountId = @-1;
        }

    } else {
        cardType = ((ExtCard *)self.selectedCard).cardType;
        self.acquiring = ((ExtCard *)self.selectedCard).processing;
        bank = ((ExtCard *)self.selectedCard).bank;
        Account *account = self.selectedCard;
        accountId = account.accountId;
        accountType = account.type;

    }

    NSString *textFieldText = [self.phoneTextField.text preparePhone];

    id identifier = self.contract ? self.contract.identifier : textFieldText;

    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"calculating_commission", nil) animated:YES];
    [PaymentsApi checkAndCalcCommissionAccountId:accountId
                                     accountType:accountType
                                    providerCode:providerCode
                                            bank:bank
                                            card:cardType
                                   paymentAmount:amount
                                        contract:identifier
                                       acquiring:self.acquiring
                                         success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        self.comission = (NSNumber *)response[@"commission"];
        [self setTotalAmountWithComission:self.comission];
        [self.payButton setMainButtonStyle:MainButtonStyleOrange];
        [self.saveTemplateButton setMainButtonStyle:MainButtonStyleOrange];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}


/**
 *
 * Show Cards to select
 *
 */
- (void)showCardListPopup {
    __weak PaymentFormViewController *wSelf = self;

    ActionSheetListPopupViewController *cardsListView = [[ActionSheetListPopupViewController alloc] init];
    cardsListView.checkmarkDisabledSections = [NSMutableIndexSet indexSetWithIndex:2];
    
    cardsListView.numberOfSections = ^{
        return (NSInteger)3;
    };
    
    cardsListView.titleForSection = ^NSString*(NSInteger section) {
        if (section == 0) {
            return NSLocalizedString(@"my_cards", nil);
        } else if (section == 1) {
            return NSLocalizedString(@"other_cards", nil);
        }
        return NSLocalizedString(@"actions", nil);
    };
    
    cardsListView.numberOfRowsInSection = ^NSInteger(NSInteger section) {
        if (section == 0) {
            return wSelf.operationAccounts.sourceAccounts.cards.count;
        } else if (section == 1) {
            return wSelf.registeredCards.count;
        } else {
            if (wSelf.actionType == ActionTypeInsurancePayment) {
                return 2;
            } else {
                return 1;
            }
        }
    };
    
    cardsListView.rowForIndexPath = ^ActionSheetListRowObject*(NSIndexPath *indexPath) {

        //BAF cards
        if (indexPath.section == 0) {
            Card *card = wSelf.operationAccounts.sourceAccounts.cards[(NSUInteger) indexPath.row];
            
            ActionSheetListRowObject *rowObject = [[ActionSheetListRowObject alloc] init];
            rowObject.title = [card.alias stringByAppendingFormat:@" (%@)", [card balanceWithCurrency]];
            rowObject.imageUrl = card.logo;
            rowObject.keepObject = card;
            
            return rowObject;
        }
        //External cards
        else if (indexPath.section == 1) {

            ExtCard *card = wSelf.registeredCards[(NSUInteger) indexPath.row];
            if (card.epayApproved) {
                ActionSheetListRowObject *rowObject = [[ActionSheetListRowObject alloc] init];
                rowObject.title = card.alias;
                rowObject.imageUrl = card.logo;
                rowObject.keepObject = card;
                return rowObject;
            }
            return nil;
        }
        //Other options
        else {
            if (wSelf.actionType == ActionTypeInsurancePayment) {
                if (indexPath.row == 0) {
                    ActionSheetListRowObject *rowObject = [[ActionSheetListRowObject alloc] init];
                    rowObject.title = @"Без регистрации карты";
                    rowObject.imageUrl = nil;
                    rowObject.keepObject = kWithoutCardRegister;
                    return rowObject;
                } else {
                    ActionSheetListRowObject *rowObject = [[ActionSheetListRowObject alloc] init];
                    rowObject.title = NSLocalizedString(@"add_new_card", nil);
                    rowObject.imageName = @"new_bank_card";
                    return rowObject;
                }
            } else {
                ActionSheetListRowObject *rowObject = [[ActionSheetListRowObject alloc] init];
                rowObject.title = NSLocalizedString(@"add_new_card", nil);
                rowObject.imageName = @"new_bank_card";
                return rowObject;
            }
        }
    };
    cardsListView.title = NSLocalizedString(@"select_card", nil);
    
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:cardsListView];
    popupController.dismissOnBackgroundTap = true;
    popupController.style = STPopupStyleBottomSheet;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [popupController presentInViewController:self];


    cardsListView.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
        if (selectedIndexPath.section == 2) {
            // show add card vc
            if (wSelf.actionType == ActionTypeInsurancePayment && selectedIndexPath.row != 1) {
                if (wSelf.amountTextField.text.length > 0) {
                    [wSelf showSelectedCardInRow];
                    wSelf.selectedCard = selectedObject;
                    [wSelf countCommission];
                }
                return;
            }
            ExternalCardRegistrationViewController *v = [[ExternalCardRegistrationViewController alloc] init];
            [wSelf.navigationController pushViewController:v animated:YES];
            return;
        }

        wSelf.selectedCard = selectedObject;
        [wSelf showSelectedCardInRow];

        if (wSelf.amountTextField.text.length > 0 && wSelf.selectedCard != nil) {
            [wSelf countCommission];
        }
    };
}

/**
 *
 * Show Selected Card Data in Row
 *
 */
- (void)showSelectedCardInRow {

    if ([self.selectedCard isKindOfClass:[Card class]]) {

        Card *selectedBankCard = self.selectedCard;
        [self.cardIconImageView sd_setImageWithURL:[NSURL URLWithString:selectedBankCard.logo]];
        self.cardTitleLabel.text = selectedBankCard.alias;
        self.cardNumberLabel.text = selectedBankCard.number;
        self.cardBalanceLabel.text = [NSString stringWithFormat:@"%@ %@", [selectedBankCard.balance decimalFormatString], selectedBankCard.currency];

    } else if ([self.selectedCard isKindOfClass:[ExtCard class]]) {

        ExtCard *selectedBankCard = self.selectedCard;
        [self.cardIconImageView sd_setImageWithURL:[NSURL URLWithString:selectedBankCard.logo]];
        self.cardTitleLabel.text = selectedBankCard.cardType;
        self.cardNumberLabel.text = selectedBankCard.alias;
        self.cardBalanceLabel.text = @"";

    } else if (self.actionType == ActionTypeInsurancePayment) {

        self.cardTitleLabel.text = @"Без регистрации карты";
        self.cardNumberLabel.text = @"";
        self.cardBalanceLabel.text = @"";

    }

    [self.tableView reloadData];
}

/**
 * Show commission value in label
 * And show total sum value
 */
- (void)setTotalAmountWithComission:(NSNumber *)comission {
    NSNumber *amount = [self.amountTextField.text numberDecimalFormat];
    NSNumber *totalAmount = @([amount floatValue] + [comission floatValue]);
    
    self.comisionLabel.text = [NSString stringWithFormat:@"%@ KZT", [comission decimalFormatString]];
    self.totalSumLabel.text = [NSString stringWithFormat:@"%@ KZT", [totalAmount decimalFormatString]];
    [self.tableView reloadData];
}

/**
 *
 * Call makePayment method
 * If acquiring isEqual kProcessingBank then open PaymentPaycheckVC
 * Otherwise open EpaybrowserVC
 *
 */
- (void)clickPayButton {
    if (![User isEmailAndPhoneConfirmedM]) { return; }

    Account *account;
    if ([self.selectedCard isKindOfClass:[Account class]]) {
        account = (Account *)self.selectedCard;
    }

    NSNumber *providerId = self.contract ? self.contract.provider.paymentProviderId : self.paymentProvider.paymentProviderId;
    NSString *phone = [self.phoneTextField.text preparePhone];
    __block NSNumber *identifier = self.contract ? self.contract.identifier : nil;

    NSNumber *totalAmount = @([[self.amountTextField.text numberDecimalFormat] floatValue] + [self.comission floatValue]);

    if ([self.selectedCard isKindOfClass:[Card class]]) {
        self.acquiring = kProcessingBankBAF;
    } else if ([self.selectedCard isKindOfClass:[NSString class]]) {
        if ([self.selectedCard isEqualToString:kWithoutCardRegister]) {
            self.acquiring = kProcessingBankKkb;
        }
    } else {
        self.acquiring = ((ExtCard *)self.selectedCard).processing;
    }

    if (self.actionType == ActionTypeNewPayment) {

        for (Contract *oldContract in [ContractModelView sharedInstance].contracts) {
            if ([oldContract.contract isEqualToString:phone] &&
                    [oldContract.provider.paymentProviderId isEqualToNumber:providerId]) {
                [Toast showToast:NSLocalizedString(@"that_contract_already_saved", nil)];
                return;
            }
        }

        if (self.paymentProvider.getInfoRequestTypeEnum == InfoRequestTypeEnt ||
                self.contract.provider.getInfoRequestTypeEnum == InfoRequestTypeEnt) {
            phone = self.phoneTextField.text;
        }

        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"register_payment", nil) animated:true];
        [ContractsApi addContract:providerId contract:phone contractAlias:phone success:^(id response) {
            //request simple payment
            [self registerPaymentWithContractId:response account:account providerId:providerId totalAmount:totalAmount];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];
    } else if (self.actionType == ActionTypeInsurancePayment) {

        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"register_payment", nil) animated:true];
        NSNumber *amount = [self.amountTextField.text numberDecimalFormat];
        NSNumber *comission = self.comission;
        [PaymentsApi paymentForInsuranceWithAmount:amount
                              amountWithCommission:totalAmount
                                            cardId:account ? account.accountId : nil
                                         acquiring:self.acquiring
                                        commission:comission
                                       bonusAmount:@0
                                         payOnline:YES
                                        providerId:providerId
                                        bonusMalus:self.bonusMalus
                                      deliveryDate:self.deliveryDate
                                         firstName:self.fName
                                          lastName:self.lName
                                        middleName:self.mName
                                             email:self.email
                                             phone:self.phone
                                            cityId:self.cityId
                                          cityName:self.cityName
                                       ogpoRequest:self.ogpoRequest
                                            street:self.street
                                              home:self.home
                                              flat:self.flat
                                           success:^(id response) {
                                               [MBProgressHUD hideAstanaHUDAnimated:true];
                                               NSNumber *isSmsAuth = response[@"isSmsAuthNeeded"];
                                               NSNumber *serviceLogId = response[@"serviceLogId"];
                                               NSString *uuid = response[@"uuid"];

                                               if ([isSmsAuth boolValue]) {
                                                   [self confirmSMSWithServiceLogId:serviceLogId uuid:uuid];
                                               } else {
                                                   [self continueWithSerivceLogId:serviceLogId uuid:uuid];
                                               }

                                           } failure:^(NSString *code, NSString *message) {
                    [MBProgressHUD hideAstanaHUDAnimated:true];
                }];

    } else {
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"register_payment", nil) animated:true];
        //request simple payment
        [self registerPaymentWithContractId:identifier account:account providerId:providerId totalAmount:totalAmount];
    }
}

- (void)registerPaymentWithContractId:(id)contractId
                              account:(Account *)account
                           providerId:(NSNumber *)providerId
                          totalAmount:(NSNumber *)totalAmount
{

    NSNumber *amount = [self.amountTextField.text numberDecimalFormat];
    NSNumber *comission = self.comission ? self.comission : @0;

    [PaymentsApi paymentRegisterProviderId:providerId
                                        contractId:contractId
                                            amount:amount
                                        commission:comission
                              amountWithCommission:totalAmount
                                       bonusAmount:@(0)
                                         acquiring:self.acquiring
                                            cardId:account.accountId
                                  additionalParams:self.info2 ? self.info2 : @""
                                       invoiceData:self.invoiceData
                                         invoiceId:self.invoiceId
                                       subContract:self.selectedSubcontract ? [self.selectedSubcontract getJsonStringNotation] : nil
                                       entAttempts:self.entAttemptsTextField.text ? : nil
                                           success:^(id response1)
                                           {
                                               [MBProgressHUD hideAstanaHUDAnimated:YES];

                                               NSNumber *isSmsAuth = response1[@"isSmsAuthNeeded"];
                                               NSNumber *serviceLogId = response1[@"serviceLogId"];
                                               NSString *uuid = response1[@"uuid"];

                                               if ([isSmsAuth boolValue]) {
                                                   [self confirmSMSWithServiceLogId:serviceLogId uuid:uuid];
                                               } else {
                                                   [self continueWithSerivceLogId:serviceLogId uuid:uuid];
                                               }

                                           }
                                           failure:^(NSString *code, NSString *message)
                                           {
                                               [MBProgressHUD hideAstanaHUDAnimated:true];
                                           }];
}

/**
 *
 * Show SMSConfirmationVC if response sms_auth_need
 *
 */
- (void)confirmSMSWithServiceLogId:(NSNumber *)serviceLogId uuid:(NSString *)uuid {
    __weak PaymentFormViewController *wSelf = self;

    SMSConfirmationPopupViewController *v = [[SMSConfirmationPopupViewController alloc] init];
    v.didDismissWithSuccessBlock = ^{
        [wSelf continueWithSerivceLogId:serviceLogId uuid:uuid];
    };
    v.verifySmsCode = ^(SMSConfirmationPopupViewController *smsConfirmationPopup, NSString *smsCode) {
        [PaymentsApi checkConfirmSMSPaymentOrderId:serviceLogId code:smsCode success:^(id response) {
            [smsConfirmationPopup smsTextFieldStopLoading];
            [smsConfirmationPopup showSuccessIcon];
            [smsConfirmationPopup dismiss];
        } failure:^(NSString *code, NSString *message) {
            [smsConfirmationPopup smsTextFieldStopLoading];
        }];
    };
    v.sendSmsCodeAgain = ^(SMSConfirmationPopupViewController *smsConfirmationPopup) {
        [CardsApi sendSMSAgainWithSuccess:^(id response) {
            [smsConfirmationPopup smsTextFieldStopLoading];
            [UIAlertView showWithTitle:nil message:NSLocalizedString(@"new_code_send", nil) cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        } failure:^(NSInteger code, NSString *message) {
            [smsConfirmationPopup smsTextFieldStopLoading];
        }];
    };
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:v];
    popupController.style = STPopupStyleFormSheet;
    popupController.navigationBar.barTintColor = [UIColor fromRGB:0x09716d];
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [popupController presentInViewController:self];
}

- (void)continueWithSerivceLogId:(NSNumber *)serviceLogId uuid:(NSString *)uuid {
    // If paying with bank card
    if ([self.acquiring isEqualToString:kProcessingBankBAF]) {
        
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"payment", nil) animated:true];
        
        [PaymentsApi processOrderId:serviceLogId success:^(id response) {
            [PaymentsApi getPaymentInfoByServiceLogUUID:uuid success:^(id response1) {
                [MBProgressHUD hideAstanaHUDAnimated:true];

                PaymentPaycheckViewController *v = [[PaymentPaycheckViewController alloc] init];
                v.paycheck = [Paycheck instanceFromDictionary:response1];
                [self.navigationController pushViewController:v animated:YES];

            }                                   failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:true];
            }];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];

    } else if ([self.selectedCard isKindOfClass:[ExtCard class]]) {
        ExtCard *card = self.selectedCard;

        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"payment", nil) animated:true];
        [PaymentsApi getEpayProcessingLinkParamsCardId:card.accountId paymentId:serviceLogId cardType:card.cardType bank:card.bank processing:self.acquiring uuid:uuid success:^(id responseObject) {
            [MBProgressHUD hideAstanaHUDAnimated:true];

            NSMutableDictionary *params = [NSMutableDictionary new];
            [params addEntriesFromDictionary:responseObject];
            params[@"acquiring"] = self.acquiring;

            EpayBrowserViewController *v = [[EpayBrowserViewController alloc] init];
            v.delegate = self;
            v.uuid = uuid;
            v.data = params;
            [self.navigationController pushViewController:v animated:YES];

        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];
    }

    //if paying without register
    else if (self.actionType == ActionTypeInsurancePayment && [self.acquiring isEqualToString:kProcessingBankKkb]) {

        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"payment", nil) animated:true];
        [PaymentsApi getEpayProcessingLinkParamsCardId:nil
                                             paymentId:serviceLogId
                                              cardType:@"any"
                                                  bank:@"any"
                                            processing:self.acquiring
                                                  uuid:uuid
                                               success:^(id responseObject) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
            NSMutableDictionary *params = [NSMutableDictionary new];
            [params addEntriesFromDictionary:responseObject];
            params[@"acquiring"] = self.acquiring;

            EpayBrowserViewController *v = [[EpayBrowserViewController alloc] init];
            v.delegate = self;
            v.uuid = uuid;
            v.data = params;
            [self.navigationController pushViewController:v animated:YES];

        } failure:^(NSString *code, NSString *message) {
                    [MBProgressHUD hideAstanaHUDAnimated:true];

        }];
    }

}

- (IBAction)selectContact:(id)sender {
    PhoneContactsViewController *vc = [PhoneContactsViewController new];
    
    __weak PaymentFormViewController *weakSelf = self;
    vc.didSelectContact = ^(NSString *contactFullName, NSString *contactNumber) {
        weakSelf.templateNameTextField.text = contactFullName;
        weakSelf.phoneTextField.text = contactNumber;
    };
    
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - tableview delegate/datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (self.actionType == ActionTypeNewPayment || self.actionType == ActionTypeInsurancePayment) {
            return 68;
        }
        
        return 86;

    } else if (indexPath.section == 1) {
        //phone and contract number cell
        if (indexPath.row == 0) {
            if (self.actionType != ActionTypeNewPayment) {
                return 0;
            } else {
                return UITableViewAutomaticDimension;
            }
        }
        //ENT attempts field
        else if (indexPath.row == 1) {

            if (self.paymentProvider.getInfoRequestTypeEnum == InfoRequestTypeEnt ||
                    self.contract.provider.getInfoRequestTypeEnum == InfoRequestTypeEnt) {

                return UITableViewAutomaticDimension;
            } else {
                return 0;
            }

        }
        //Template name
        else if (indexPath.row == 2) {
            if (self.actionType == ActionTypeInsurancePayment) {
                return 0;
            } else {
                return 50;
            }
        }
        //Choose subcontract (for SECURITY_SERVICES_CATEGORY_ID)
        else if (indexPath.row == 3) {
            if (self.subContractList.count > 0) {
                return 50;
            } else {
                return 0;
            }
        }
        //Enter sum for pay
        else if (indexPath.row == 4) {
            if ([self isTemplateOnlyForm]) {
                return 0;
            }
            return 50;
        }
        // disclamer
        else if (indexPath.row == 5) {
            if (!self.disclaimer) {
                return 0;
            }
            return UITableViewAutomaticDimension;
        }
        return 50;
    } else if (indexPath.section == 2) {
        if ([self isTemplateOnlyForm]) {
            return 0;
        }
        return UITableViewAutomaticDimension;
    }

    else if (indexPath.section == 3) {
        if (!self.comisionLabel.text ||
                self.comisionLabel.text.length == 0 ||
                !self.totalSumLabel.text ||
                self.totalSumLabel.text.length == 0) {
            return 0;
        }
        return 50;
    }

    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setClipsToBounds:YES];

    if (indexPath.section == 1 && indexPath.row == 4) {
        if (self.disclaimer) {
            self.disclaimerLabel.text = self.disclaimer;
            cell.backgroundColor = [[UIColor appDisclaimerYellowColor] colorWithAlphaComponent:0.5];
        }
    } else if (indexPath.section == 2 && indexPath.row == 0 && self.hasCards) {
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( indexPath.section == 2 && indexPath.row == 0 && self.hasCards) {
        [self showCardListPopup];
    } else if (indexPath.section == 1 && indexPath.row == 2 && self.subContractList.count > 1) {
        [self openSubContractList];
    }
}

- (NSString*) tableView:(UITableView *) tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 2) {
        if (([self.paymentProvider.infoRequestType intValue] != 2 ) &&
                (self.actionType != ActionTypeNewPayment && self.providerCategoryId != ContractCategoryTypeCommunal))
        {
            return NSLocalizedString(@"choose_pay_card", nil);
        } else {
            return nil;
        }
    } else {
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 3) {
        if (!self.comisionLabel.text || self.comisionLabel.text.length == 0 || !self.totalSumLabel.text || self.totalSumLabel.text.length == 0) {
            return 0.001;
        }
    }
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 3) {
        if (!self.comisionLabel.text || self.comisionLabel.text.length == 0 || !self.totalSumLabel.text || self.totalSumLabel.text.length == 0) {
            return 0.001;
        }
    }
    
    return UITableViewAutomaticDimension;
}

#pragma mark - Helpers

- (BOOL)hasCards {
    return self.operationAccounts.sourceAccounts.cards.count > 0
            || self.registeredCards.count > 0;
}

- (BOOL)isTemplateOnlyForm {
    return self.actionType == ActionTypeNewPayment &&
        (self.paymentProvider.getInfoRequestTypeEnum == InfoRequestTypeGetInvoice ||
                self.paymentProvider.getInfoRequestTypeEnum == InfoRequestTypeGetBalance ||
                self.paymentProvider.getInfoRequestTypeEnum == InfoRequestTypeGetAdditionalInfo ||
                self.paymentProvider.getInfoRequestTypeEnum == InfoRequestTypeGetSubContract ||
                self.paymentProvider.getInfoRequestTypeEnum == InfoRequestTypeSelectContract);
}


#pragma mark - config ui

/**
 *
 * Configure User Interface
 *
 */

- (void)configUI {
    [self setNavigationBackButton];
    
    if (self.actionType == ActionTypeMakePayment) {
        [self setNavigationTitle:self.contract.alias];
    } else {
        [self setNavigationTitle:NSLocalizedString(@"Добавление услуги", nil)];
    }
    
    if ((self.actionType == ActionTypeMakePayment) && !self.invoiceData) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deletePaymentTemplate:)];
    }

    if ([self.paymentProvider.infoRequestType isEqualToNumber:@1]) {
        self.phoneTextField.keyboardType = UIKeyboardTypeDefault;
    } else {
        self.phoneTextField.keyboardType = UIKeyboardTypeDefault;
    }
   // if(self.isAlsekoPayment) self.amountTextField.enabled = NO;
    self.amountTextField.keyboardType = UIKeyboardTypeDecimalPad;
    self.payButton.mainButtonStyle = MainButtonStyleDisable;
    self.payButton.title = NSLocalizedString(@"pay", nil);
    [self.payButton addTarget:self action:@selector(clickPayButton) forControlEvents:UIControlEventTouchUpInside];
    if ([self isTemplateOnlyForm]) {
        self.payButton.hidden = YES;
    }

    self.saveTemplateButton.hidden = self.actionType == ActionTypeMakePayment || self.actionType == ActionTypeInsurancePayment;
    self.saveTemplateButton.mainButtonStyle = MainButtonStyleDisable;
    self.saveTemplateButton.title = NSLocalizedString(@"save_template", nil);
    [self.saveTemplateButton addTarget:self action:@selector(savePaymentTemplate:) forControlEvents:UIControlEventTouchUpInside];

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 50;
    [self setBAFTableViewBackground];

    
    if (self.actionType != ActionTypeNewPayment) {
        self.phoneTextField.enabled = false;
        self.phoneCell.hidden = true;
    }
    
    self.cardIconImageView.image = [UIImage imageNamed:@"icon-placeholder"];
    self.cardTitleLabel.text = NSLocalizedString(@"cards_loading", nil);
    self.cardBalanceLabel.text = @"-";
    self.cardNumberLabel.text = @"-";
    
    __weak PaymentFormViewController *weakSelf = self;
    self.templateNameTextField.didEndEditingBlock = ^(UITextField *textField) {
        if (textField.text.length > 0) {
            [weakSelf updatePaymentTemplateAlias:textField.text];
        }
    };
    self.templateNameSpinner.hidden = true;
    
    if (self.providerCategoryId == CELLULAR_CATEGORY_ID) {
        self.selectContactButton.hidden = false;
    }

    if (self.actionType == ActionTypeInsurancePayment) {
        self.templateNameCell.hidden = YES;
    }
}


/**
 *
 * Fill Views with contract data
 *
 */
- (void)passDataToViews {

    if (self.actionType == ActionTypeMakePayment) {

        [self.contractIconImageView sd_setImageWithURL:[[NSURL alloc] initWithString:self.contract.provider.logo] placeholderImage:nil];
        self.contractTitleLabel.text = self.contract.provider.name;
        self.contractSubtitleLabel.text = self.contract.provider.categoryProviderName;
        self.contractAliasLabel.text = self.contract.contract;
        self.phoneTextField.text = self.contract.contract;
        
        self.templateNameTextField.text = self.contract.alias;
        
        if (self.contract.provider.contractName && self.contract.provider.contractName.length > 0) {
            self.phoneTitleLabel.text = self.contract.provider.contractName;
        }
        self.contractFormatLabel.text = @"";
        self.amountTextField.text = [self.inputAmount decimalFormatString];

        self.phoneTextField.mask = @"+7 (###) ### ## ##";
        self.phoneTextField.delegate = self;
        self.templateNameTextField.placeholder = NSLocalizedString(@"template_alias", nil);

    } else {

        [self.contractIconImageView sd_setImageWithURL:[[NSURL alloc] initWithString:self.paymentProvider.logo] placeholderImage:nil];
        self.contractTitleLabel.text = self.paymentProvider.name;
        self.contractSubtitleLabel.text = self.paymentProvider.categoryProviderName;
        self.contractAliasLabel.text = @"";
        
        if (self.paymentProvider.contractName && self.paymentProvider.contractName.length > 0) {
            self.phoneTitleLabel.text = self.paymentProvider.contractName;
        }
        self.contractFormatLabel.text = @"";
        if (self.inputAmount) self.amountTextField.text = [self.inputAmount decimalFormatString];

        self.phoneTextField.mask = @"+7 (###) ### ## ##";
        self.phoneTextField.delegate = self;
        self.templateNameTextField.placeholder = NSLocalizedString(@"template_alias", nil);
        self.templateNameTextField.delegate = self;
    }

}

/**
 * Assign subcontract in global parameten selectedSubContract
 * Show name and amount in cell
 *
 * @param subContract
 */
- (void)showSubContract:(SubContract *)subContract {
    self.selectedSubcontract = subContract;


    self.subcontractLabel.textColor = [UIColor blackColor];

    if ([self.paymentProvider getInfoRequestTypeEnum] == InfoRequestTypeSelectContract) {
        self.amountTextField.text = self.selectedSubcontract.sum;
        self.subcontractLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@", self.selectedSubcontract.name, self.selectedSubcontract.num, NSLocalizedString(@"fromm", nil), self.selectedSubcontract.date];
        self.subcontractLabel.font = [UIFont systemFontOfSize:12];
    } else if ([self.paymentProvider getInfoRequestTypeEnum] == InfoRequestTypeGetSubContract) {
        self.amountTextField.text = [self.selectedSubcontract getAmountString];
        self.subcontractLabel.text = self.selectedSubcontract.name;
    }

    self.subcontractDisclosure.hidden = self.subContractList.count == 1;
}

/**
 * Called when pushing subcontracts cell
 * Shows actionSheetListPopup with available subcontracts within
 */
- (void)openSubContractList {
    __weak PaymentFormViewController *wSelf = self;
    ActionSheetListPopupViewController *a = [[ActionSheetListPopupViewController alloc] init];
    a.checkmarkDisabledSections = [NSMutableIndexSet indexSetWithIndex:0];
    a.title = NSLocalizedString(@"select_account_to_pay", nil);
    a.numberOfSections = ^NSInteger {
        return 1;
    };
    a.numberOfRowsInSection = ^NSInteger(NSInteger section) {
        return wSelf.subContractList.count;
    };
    a.rowForIndexPath = ^ActionSheetListRowObject*(NSIndexPath *indexPath) {
        SubContract *subContract = wSelf.subContractList[(NSUInteger) indexPath.row];
        ActionSheetListRowObject *rowObject = [[ActionSheetListRowObject alloc] init];
        rowObject.title = [subContract.name stringByAppendingFormat:@" (%@ KZT)", subContract.amount];
        rowObject.keepObject = subContract;
        return rowObject;
    };
    a.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
        [wSelf showSubContract:selectedObject];
    };
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:a];
    popupController.dismissOnBackgroundTap = true;
    popupController.style = STPopupStyleBottomSheet;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [popupController presentInViewController:self];
}

#pragma mark - check contract for ent

- (void)checkContractForEnt {
    if (self.phoneTextField.text.length > 0 && self.entAttemptsTextField.text.length > 0) {

        NSString *contractName = [NSString stringWithFormat:@"%@;%@",self.phoneTextField.text,self.entAttemptsTextField.text];

        [PaymentsApi checkAccount:self.paymentProvider ? self.paymentProvider.paymentProviderId : self.contract.provider.paymentProviderId
                         contract:contractName
                          success:^(id response) {

                              ENTCheckAccountResult *entCheckAccountResult = [ENTCheckAccountResult instanceFromDictionary:response];
                              if (entCheckAccountResult.isAccepted) {
                                  self.amountTextField.text = entCheckAccountResult.amountString;
                              }

                              //[self.payButton setMainButtonStyle:MainButtonStyleOrange];

                          } failure:^(NSString *code, NSString *message) {
                }];
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (self.templateNameTextField.text.length > 0 && self.phoneTextField.text.length > 0) {
        [self.saveTemplateButton setMainButtonStyle:MainButtonStyleOrange];
    }

    //Check if contract has subContracts
    [self checkAccount];

    //if yes load to subContractsList array
    [self loadSubContractsWithContractNumber:textField.text];



    if (self.paymentProvider.getInfoRequestTypeEnum == InfoRequestTypeEnt ||
            self.contract.provider.getInfoRequestTypeEnum == InfoRequestTypeEnt) {
        [self checkContractForEnt];
    }

}

- (BOOL)textField:(UITextField *)textField
        shouldChangeCharactersInRange:(NSRange)range
        replacementString:(NSString *)string
{
    if (((self.contract && self.contract.provider.contractMask && self.contract.provider.contractMask.length > 0) ||
            (self.paymentProvider.contractMask && self.paymentProvider.contractMask.length > 0)) &&
            textField == self.phoneTextField)
    {
        return [self.phoneTextField shouldChangeCharactersInRange:range replacementString:string];
    }
 
    return true;
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"PaymentFormViewControler dealloc");
}

@end
