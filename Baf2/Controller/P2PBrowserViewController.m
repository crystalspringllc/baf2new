//
//  P2PBrowserViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 25.07.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "P2PBrowserViewController.h"
#import "PaymentsApi.h"
#import "Paycheck.h"
#import "PaymentPaycheckViewController.h"
#import "P2PInitResponse.h"
#import "NSURL+Extension.h"
#import "NewTransfersApi.h"
#import "AviaMilesViewController.h"
#import "AviaMilesResultViewController.h"
#import <WebKit/WebKit.h>
#import "NewTransfersApi.h"

@interface P2PBrowserViewController ()
//@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) WKWebView *wkWebView;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;
@end

@interface P2PBrowserViewController () <WKNavigationDelegate>
@end

@implementation P2PBrowserViewController
{
    NSMutableURLRequest *request;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

//    if ([self.webView respondsToSelector:@selector(setKeyboardDisplayRequiresUserAction:)]) {
//        self.webView.keyboardDisplayRequiresUserAction = NO;
//    }
}

#pragma mark - UIWebViewDelegate

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    if (navigationAction.navigationType == WKNavigationTypeLinkActivated){
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    if(self.isNew == NO){
    if (navigationAction.request.URL) {
        NSString *url = navigationAction.request.URL.absoluteString;
        if (self.p2PInitResponse.backLink && [url containsString:self.p2PInitResponse.backLink]) {
            
            NSDictionary *backParameters = [navigationAction.request.URL parseKeyValues];
            NSString *uuid = [[[[self.p2PInitResponse.backLink componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"?"] firstObject];
            if (uuid.length == 0) {
                uuid = self.p2PInitResponse.uuid;
            }
            
            [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"receipt_loading", nil) animated:YES];
            [PaymentsApi getPaymentInfoMakePaymentUUID:uuid payboxParams:backParameters success:^(id response) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
                
                Paycheck *paycheck = [Paycheck instanceFromDictionary:response];
                
                [NewTransfersApi getTransfer:paycheck.orderId success:^(id response) {
                    [MBProgressHUD hideAstanaHUDAnimated:YES];
                    
                    AviaMilesResultViewController *v = [[AviaMilesResultViewController alloc] init];
                    v.output = nil;
                    v.dataForTable = [self prepareDataForTableView:response];
                    [v setNavigationTitle:@"Успешный перевод"];
                    [self.navigationController pushViewController:v animated:YES];
                    
                } failure:^(NSString *code, NSString *message) {
                    [MBProgressHUD hideAstanaHUDAnimated:YES];
                }];
                
            } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
            }];
        }
    }
    decisionHandler(WKNavigationActionPolicyAllow);
    }else{
        if (navigationAction.request.URL) {
            NSString *url = navigationAction.request.URL.absoluteString;
            if (self.backLink && [url containsString:self.backLink]) {
                
                NSDictionary *backParameters = [navigationAction.request.URL parseKeyValues];
                NSString *uuid = [[[[self.backLink componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"?"] firstObject];
               /* if (uuid.length == 0) {
                    uuid = self.uuid;
                }*/
                
                [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"receipt_loading", nil) animated:YES];
                //[PaymentsApi getPaymentInfoMakePaymentUUID:uuid payboxParams:backParameters success:^(id response) {
                  //  [MBProgressHUD hideAstanaHUDAnimated:YES];
                    
                   // Paycheck *paycheck = [Paycheck instanceFromDictionary:response];
                    
                    [NewTransfersApi getTransfer:self.servicelogId success:^(id response) {
                        [MBProgressHUD hideAstanaHUDAnimated:YES];
                        
                        AviaMilesResultViewController *v = [[AviaMilesResultViewController alloc] init];
                        v.output = nil;
                        v.dataForTable = [self prepareDataForTableView:response];
                        [v setNavigationTitle:@"Успешный перевод"];
                        [self.navigationController pushViewController:v animated:YES];
                        
                    } failure:^(NSString *code, NSString *message) {
                        [MBProgressHUD hideAstanaHUDAnimated:YES];
                    }];
                    
               // } failure:^(NSString *code, NSString *message) {
                 //   [MBProgressHUD hideAstanaHUDAnimated:YES];
                //}];
                
             /*   [NewTransfersApi getTransferResultFromWebWithServiceLogID:self.servicelogId success:^(id response) {
                    [MBProgressHUD hideAstanaHUDAnimated:YES];
                    NSLog(@"Succes response %@", response);
                } failure:^(NSString *code, NSString *message) {
                    [MBProgressHUD hideAstanaHUDAnimated:YES];
                }];*/
            }
        }
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [self.spinner startAnimating];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
     [self.spinner stopAnimating];
    //[self.spinner stopAnimating];
   // NSString *string = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('html')[0].innerHTML"];
  //  BOOL isEmpty = string==nil || [string length]==0;
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    [self.spinner stopAnimating];
}



- (NSData *)httpBodyForParameters:(NSDictionary *)parameters {
    NSMutableArray *parameterArray = [NSMutableArray array];
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
        NSString *param = [NSString stringWithFormat:@"%@=%@", [self percentEscapeString:key], [self percentEscapeString:obj]];
        [parameterArray addObject:param];
    }];
    
    NSString *string = [parameterArray componentsJoinedByString:@"&"];
    
    return [string dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)percentEscapeString:(NSString *)string {
    NSCharacterSet *allowed = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~"];
    return [string stringByAddingPercentEncodingWithAllowedCharacters:allowed];
}

#pragma mark - Config UI

- (void)configUI {
    [self setNavigationBackButton];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.spinner];
    self.wkWebView = [WKWebView newAutoLayoutView];
    self.wkWebView.navigationDelegate = self;
    [self.view addSubview:self.wkWebView];
    [self.wkWebView autoPinEdgesToSuperviewEdges];
    
   // if(self.isNew == NO && self.isSecurityType == NO){
    request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.p2PInitResponse.processLink]];
/*}else
    if(self.isSecurityType == YES){
        if(!self.securityParams) return;
        request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.securityLink]];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[self httpBodyForParameters:self.securityParams]];
}else{
     request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.processLink]];
    }*/
    [self.wkWebView loadRequest:request];
}

#pragma mark - helpers

- (NSArray *)prepareDataForTableView:(NSDictionary *)resultData {

    NSMutableArray *infoRowTitles = [[NSMutableArray alloc] init];
    NSMutableArray *infoRowSubtitles = [[NSMutableArray alloc] init];

    //Дата операции
    if (resultData[@"date"]) {
        [infoRowTitles addObject:@"Дата операции:"];
        [infoRowSubtitles addObject:resultData[@"date"]];
    }

    //Карта списания
    if (resultData[@"requestorAccount"][@"key"]) {
        [infoRowTitles addObject:NSLocalizedString(@"card_from_enroll", nil)];
        NSString *requestorCard = [NSString stringWithFormat:@"%@", resultData[@"requestorAccount"][@"key"]];
        [infoRowSubtitles addObject:requestorCard];
    }

    //Карта зачисления
    if (resultData[@"destinationAccount"][@"key"]) {
        [infoRowTitles addObject:NSLocalizedString(@"card_to_enroll", nil)];
        NSString *destinationCard = [NSString stringWithFormat:@"%@", resultData[@"destinationAccount"][@"key"]];
        [infoRowSubtitles addObject:destinationCard];
    }

    //Назначение платежа
    if (resultData[@"knpName"]) {
        [infoRowTitles addObject:@"Назначение платежа:"];
        [infoRowSubtitles addObject:resultData[@"knpName"]];
    }

    //Сумма операции
    if (resultData[@"destinationAmount"] && resultData[@"destinationCurrency"]) {
        [infoRowTitles addObject:@"Сумма операции:"];
        NSNumber *destinationAmount = resultData[@"destinationAmount"];
        NSString *destinationCurrency = resultData[@"destinationCurrency"];
        NSString *dAmount = [NSString stringWithFormat:@"%@ %@", [destinationAmount decimalFormatString], destinationCurrency];
        [infoRowSubtitles addObject:dAmount];
    }

    //Комиссия
    if (resultData[@"comission"]) {
        [infoRowTitles addObject:@"Комиссия:"];
        NSNumber *comission = resultData[@"comission"];
        [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@", [comission decimalFormatString]]];
    }

    //Id транзакции
    if (resultData[@"reference"]) {
        [infoRowTitles addObject:@"ID транзакции:"];
        [infoRowSubtitles addObject:resultData[@"reference"]];
    }

    //Id операции
    if (resultData[@"servicelogId"]) {
        [infoRowTitles addObject:NSLocalizedString(@"operation_id", nil)];
        NSNumber *servicelogId = resultData[@"servicelogId"];
        [infoRowSubtitles addObject:[servicelogId stringValue]];
    }

    return @[infoRowTitles, infoRowSubtitles];
}

@end
