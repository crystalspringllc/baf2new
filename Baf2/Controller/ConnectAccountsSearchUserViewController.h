//
//  ConnectAccountsSearchUserViewController.h
//  BAF
//
//  Created by Almas Adilbek on 8/25/14.
//
//

#import "AAFormViewController.h"
#import "ConnectAccountsVerifyUserViewController.h"

@protocol ConnectAccountsSearchUserViewControllerDelegate;

@interface ConnectAccountsSearchUserViewController : AAFormViewController <ConnectAccountsVerifyUserViewControllerDelegate>

@property(nonatomic, weak) id <ConnectAccountsSearchUserViewControllerDelegate> delegate;
@property (nonatomic, copy) NSString *iin;

@end

@protocol ConnectAccountsSearchUserViewControllerDelegate<NSObject>
-(void)connectAccountsSearchUserViewControllerAccountConnected;
@end