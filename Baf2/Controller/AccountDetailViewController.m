//
// Created by Askar Mustafin on 4/11/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "AccountDetailViewController.h"
#import "AccountDetailHeaderView.h"
#import "StatementCell.h"
#import "STPopupController+Extensions.h"
#import "DateIntervalPickerPopupViewController.h"
#import "AccountAliasEditViewController.h"
#import "AccountAdditionalInfoViewController.h"
#import "TransfersFormViewController.h"
#import "AccountManageLimitsViewController.h"
#import "AccountsApi.h"
#import "AccountStatementsResponse.h"
#import "NotificationCenterHelper.h"
#import "NSDateFormatter+Ext.h"
#import "Deposit.h"
#import "Loan.h"
#import "AccountHelper.h"
#import "InfoApi.h"
#import "Baf2-Prefix.pch"
#import "AviataStatementCell.h"
#import "AviaModuleDependencies.h"
#import "SmsBankingDependencies.h"
#import "SmsBankingInteractor.h"
#import "CardAccount.h"
#import "Current.h"
#import "SMSConfirmationPopupViewController.h"
#import "CardsApi.h"
#import "MyTransfersDependencies.h"
#import "TransferTemplate.h"
#import "CashBackConfigDependencies.h"

typedef enum SelectedStatement:NSInteger {
    SelectedStatementNormal,
    SelectedStatementBlocked,
    SelectedStatementAviata
} SelectedStatement;

@interface AccountDetailViewController () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) AccountDetailHeaderView *cardDetailHeaderView;
@property (nonatomic, strong) AccountStatementsResponse *accountStatementsResponse;
@property (nonatomic) SelectedStatement selectedStatement;
@property (nonatomic, strong) NSDate *fromDate;
@property (nonatomic, strong) NSDate *toDate;
@property (nonatomic, strong) OperationAccounts *operationAccounts;
@property (nonatomic, strong) UIActivityIndicatorView *statementsActiviryIndicator;
@property (nonatomic, strong) NSArray *templatesArray;

@end

@implementation AccountDetailViewController {}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Cards" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AccountDetailViewController class])];
}

/**
 * написовать начальные вьющки и загрузить выписку
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

    //Загрузка счета
    [self loadAccount];

    //Загрузка выписки
    self.fromDate = [[NSDate date] dateBySubtractingDays:3];
    self.toDate = [NSDate date];
    [self loadStatementsFromDate:self.fromDate toDate:self.toDate];
}

#pragma mark - loadings

/**
 * Load account
 */

- (void)loadAccount {
    [AccountsApi getAccount:self.account.accountId accountType:self.account.type success:^(id account) {
        self.account = account;
        self.cardDetailHeaderView.account = self.account;
        [self.tableView reloadData];

    } failure:^(NSString *code, NSString *message) {
    }];
}

-(void)loadAccountWithCompletion:(void (^)(Account* account))completion{
    [MBProgressHUD showAstanaHUDWithTitle:@"" animated:true];
    [AccountsApi getAccount:self.account.accountId accountType:self.account.type success:^(id account) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        completion(account);
        self.account = account;
        self.cardDetailHeaderView.account = self.account;
        [self.tableView reloadData];
        
    } failure:^(NSString *code, NSString *message) {
        
    }];
}

/**
 * Load statements
 */
- (void)loadStatementsFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate  {
    if ([self.account accountType] == AccountTypeLoan) {
        return;
    }


    if (!self.statementsActiviryIndicator) {
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.width, 100)];
        self.statementsActiviryIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.statementsActiviryIndicator.centerX = footerView.centerX;
        self.statementsActiviryIndicator.centerY = footerView.centerY;
        [footerView addSubview:self.statementsActiviryIndicator];
        self.tableView.tableFooterView = footerView;
        [self.statementsActiviryIndicator startAnimating];
    }

    self.accountStatementsResponse = nil;
    [self.tableView reloadData];

    [AccountsApi getAccountStatementsWithAccountId:self.account.accountId accountType:self.account.type dateFrom:fromDate dateTo:toDate success:^(id response) {
        self.accountStatementsResponse = [AccountStatementsResponse instanceFromDictionary:response];

        if ([self.account accountType] == AccountTypeAccount) {
            [AccountsApi getCashbackAccountId:self.account.accountId accountType:self.account.type success:^(id response) {

                self.accountStatementsResponse.cashBack = response[@"amount"];
                self.accountStatementsResponse.cashBackCurrency = response[@"currency"];
                [self handleStatements];
                
                [self.tableView reloadData];
                [self.statementsActiviryIndicator stopAnimating];
                self.statementsActiviryIndicator = nil;
                self.tableView.tableFooterView = nil;

            } failure:^(NSString *code, NSString *message) {
            }];
        } else {
            //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reloadAccount)];
            [self handleStatements];
            
            [self.tableView reloadData];
            [self.statementsActiviryIndicator stopAnimating];
            self.statementsActiviryIndicator = nil;
            self.tableView.tableFooterView = nil;
        }
    } failure:^(NSString *code, NSString *message) {
        
        [self.statementsActiviryIndicator stopAnimating];
        self.statementsActiviryIndicator = nil;
        self.tableView.tableFooterView = nil;
        
    }];
}

/**
 * Обновить выписку
 */
- (void)reloadAccount {
    if (self.onUpdateAccounts) {
        self.onUpdateAccounts();
    }
    [self loadAccount];
}

/**
 * обновить хэдэр и показать выписку в таблице
 */
- (void)handleStatements {
    [self.cardDetailHeaderView setAccountStatementsResponse:self.accountStatementsResponse];
    [self.cardDetailHeaderView setChoosePeriodText:self.fromDate toDate:self.toDate];
    [self.cardDetailHeaderView setNeedsLayout];
    [self.cardDetailHeaderView layoutIfNeeded];
    CGFloat height = [self.cardDetailHeaderView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    self.cardDetailHeaderView.height = height;
    self.cardDetailHeaderView.width = [ASize screenWidth];
    self.tableView.tableHeaderView = self.cardDetailHeaderView;
    [self.tableView reloadData];
}

#pragma mark - config actions

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Описывает все действия которые были проведены в хэдер вью
 */
- (void)configCardDetailHeaderActions {
    __weak AccountDetailViewController *wSelf = self;
    
    self.cardDetailHeaderView.onClickTransferMultiCurrencyButton = ^{
        TransfersFormViewController *v = [TransfersFormViewController createVC];
        v.title = NSLocalizedString(@"transfer_between_currencies", nil);
        v.currencyConvertAccount = wSelf.account;
        v.transfersType = MY_CONVERT;
        [wSelf.navigationController pushViewController:v animated:YES];
    };

    self.cardDetailHeaderView.onClickChoosePeriod = ^{
        [wSelf chooseStatementPeriod];
    };

#pragma mark - onSegmentValueChanged
    
    self.cardDetailHeaderView.onSegmentValueChanged = ^(NSInteger selectedIndex) {
        wSelf.selectedStatement = (SelectedStatement) selectedIndex;
        [wSelf.tableView reloadData];
    };

    self.cardDetailHeaderView.onBlockCardTapped = ^() {
        [wSelf showCardAlertOptionsBlock];
    };
    
    self.cardDetailHeaderView.onAccountAliasEditTapped = ^() {
        AccountAliasEditViewController *vc = [[UIStoryboard storyboardWithName:@"Cards" bundle:nil] instantiateViewControllerWithIdentifier:@"AccountAliasEditViewController"];
        vc.initialAlias = wSelf.account.alias;
        vc.didTapDoneButton = ^(NSString *alias) {
            [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];

            [AccountsApi setAccount:wSelf.account.accountId accountType:wSelf.account.type alias:alias success:^(id response) {
                [MBProgressHUD hideAstanaHUDAnimated:true];
                wSelf.account.alias = alias;
                wSelf.cardDetailHeaderView.account = wSelf.account;
                [wSelf.cardDetailHeaderView setChoosePeriodText:wSelf.fromDate toDate:wSelf.toDate];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAccountAliasChanged object:nil];
            } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:true];
            }];
        };
        
        STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:vc];
        popupController.dismissOnBackgroundTap = true;
        popupController.style = STPopupStyleBottomSheet;
        popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
        [popupController presentInViewController:wSelf];
    };
    
    self.cardDetailHeaderView.onAdditionalInfoTapped = ^() {
        AccountAdditionalInfoViewController *vc = [[UIStoryboard storyboardWithName:@"Cards" bundle:nil] instantiateViewControllerWithIdentifier:@"AccountAdditionalInfoViewController"];
        vc.title = NSLocalizedString(@"additional_info", nil);
        
        switch (wSelf.account.accountType) {
            case AccountTypeAccount:
            case AccountTypeCurrent:
            {
                Account *account = wSelf.account;
                
                NSString *iban = [NSString stringWithFormat:@"%@ %@", account.iban, account.currency];
                
                if (account.opened) {
                    NSDate *openedDate = [[NSDateFormatter baf2DateFormatter] dateFromString:account.opened];
                    NSDateFormatter *df = [NSDateFormatter new];
                    [df setDateFormat:@"dd.MM.yyyy"];
                    NSString *dateString = [df stringFromDate:openedDate];
                    if (!dateString) dateString = @"";
                    NSString *openedDateText = [NSString stringWithFormat:@"%@ %@", dateString, account.filial];
                    
                    vc.infoDataList = @[
                                        @{@"title": @"IBAN",
                                          @"subtitle": iban},
                                        @{@"title": NSLocalizedString(@"opening_date", nil),
                                          @"subtitle": openedDateText}
                                        ];
                }
            }
                break;
            case AccountTypeCard:
            {
                Card *card = (Card *)wSelf.account;
                
                NSString *iban = [NSString stringWithFormat:@"%@ %@", card.iban, card.currency];
                
                NSDate *openedDate = [[NSDateFormatter baf2DateFormatter] dateFromString:card.opened];
                NSDateFormatter *df = [NSDateFormatter new];
                [df setDateFormat:@"dd.MM.yyyy"];
                NSString *dateString = [df stringFromDate:openedDate];
                if (!dateString) dateString = @"";
                
                NSString *openedDateText = [NSString stringWithFormat:@"%@ %@", dateString, card.filial];
                
                NSDate *expiryDate = [[NSDateFormatter baf2DateFormatter] dateFromString:card.expiryDate ];
                NSString *expiryDateText = [df stringFromDate:expiryDate];
                if (!expiryDateText) expiryDateText = @"";

                NSString *cardHolder = [NSString stringWithFormat:@"%@ %@", card.lastName, card.firstName];
                NSString *cardProduct = card.product;
                
                if (card.isCredit) {
                    //NSDate *nextPaymentDate = [[NSDateFormatter baf2DateFormatter] dateFromString:card.nextPaymentDate];
                    //NSString *nextPaymentDateString = [df stringFromDate:nextPaymentDate];
                    //NSString *nextPaymentDateText = [NSString stringWithFormat:@"Оплатить до %@", nextPaymentDateString];
                    //NSString *nextPaymentText = [NSString stringWithFormat:@"%@ %@\n(%@)", card.nextPaymentAmount, card.currency, nextPaymentDateText];
                    //NSString *interestAmount = [NSString stringWithFormat:@"%@ %@", [card.interestAmount decimalFormatString], card.currency];
                    //NSString *debtAmount = [NSString stringWithFormat:@"%@ %@", [card.debtAmount decimalFormatString], card.currency];
                    //NSString *totalDebtAmount = [NSString stringWithFormat:@"%@ %@", [card.totalDebtAmount decimalFormatString], card.currency];
                    
                    vc.infoDataList = @[
                                        @{@"title": @"IBAN",
                                          @"subtitle": iban},
                                        @{@"title": NSLocalizedString(@"card_expiration_date", nil),
                                          @"subtitle": expiryDateText},
                                        /*@{@"title": @"Следующий платеж",
                                          @"subtitle": nextPaymentText},
                                        @{@"title": @"Сумма начисленного вознаграждения",
                                          @"subtitle": interestAmount},
                                        @{@"title": @"Сумма просроченной задолженности",
                                          @"subtitle": debtAmount},
                                        @{@"title": @"Общая сумма задолженности",
                                          @"subtitle": totalDebtAmount},*/
                                        @{@"title": NSLocalizedString(@"opening_date", nil),
                                          @"subtitle": openedDateText},
                                        @{@"title": NSLocalizedString(@"cardowner", nil),
                                                @"subtitle": cardHolder}
                                        ];
                } else {

                    vc.infoDataList = @[
                                        @{@"title": @"IBAN",
                                          @"subtitle": iban},
                                        @{@"title": NSLocalizedString(@"card_expiration_date", nil),
                                          @"subtitle": expiryDateText},
                                        @{@"title": NSLocalizedString(@"opening_date", nil),
                                          @"subtitle": openedDateText},
                                        @{@"title": NSLocalizedString(@"cardowner", nil),
                                          @"subtitle": cardHolder}
                                        /*@{@"title": NSLocalizedString(@"banking_product", nil),
                                          @"subtitle": cardProduct},*/
                                        ];

                    [AccountsApi getCardLimits:card.cardId success:^(NSArray *limits) {
                        NSMutableArray *array = [[NSMutableArray alloc] init];
                        if (limits && limits.count > 0) {
                            for (CardLimit *cardLimit in limits) {
                                NSDictionary *item = @{@"title" : cardLimit.name, @"subtitle" : [cardLimit amoungAndCurrency]};
                                [array addObject:item];
                            }
                        }
                        [vc addItems:array];
                    } failure:^(NSString *code, NSString *message) {

                    }];
                }
            } break;
            case AccountTypeDeposit:
            {
                Deposit *deposit = wSelf.account;
                
                NSString *iban = [NSString stringWithFormat:@"%@ %@", deposit.iban, deposit.currency];
                
                NSDate *openedDate = [[NSDateFormatter baf2DateFormatter] dateFromString:deposit.opened];
                NSDateFormatter *df = [NSDateFormatter new];
                [df setDateFormat:@"dd.MM.yyyy"];
                NSString *dateString = [df stringFromDate:openedDate];
                if (!dateString) dateString = @"";
                NSString *openedDateText = [NSString stringWithFormat:@"%@ %@", dateString, deposit.filial];
                
                vc.infoDataList = @[
                                    @{@"title": @"IBAN",
                                      @"subtitle": iban},
                                    @{@"title": NSLocalizedString(@"remuneration_rate", nil),
                                      @"subtitle": deposit.fee},
                                    @{@"title": NSLocalizedString(@"deposit_term_in_months", nil),
                                      @"subtitle": deposit.period},
                                    @{@"title": NSLocalizedString(@"minimum_balance", nil),
                                      @"subtitle": deposit.minimal},
                                    @{@"title": NSLocalizedString(@"opening_date", nil),
                                      @"subtitle": openedDateText},
                                    ];
            }
                break;
            case AccountTypeLoan:
            {
                Loan *loan = wSelf.account;
                                
                NSDate *openedDate = [[NSDateFormatter baf2DateFormatter] dateFromString:loan.opened];
                NSDateFormatter *df = [NSDateFormatter new];
                [df setDateFormat:@"dd.MM.yyyy"];
                NSString *dateString = [df stringFromDate:openedDate];
                if (!dateString) dateString = @"";
                NSString *openedDateText = [NSString stringWithFormat:@"%@ %@", dateString, loan.filial];
                
                if (loan.planDate) {

                    NSDate *planDate = [[NSDateFormatter baf2DateFormatter] dateFromString:loan.planDate];
                    NSString *planDateString = [df stringFromDate:planDate];
                    if (!planDateString) planDateString = @"";
                    NSString *planDateText = [NSString stringWithFormat:@"%@ %@ %@ %@", NSLocalizedString(@"will_be_charged", nil), planDateString, NSLocalizedString(@"from_this_account", nil),loan.linkedAccountAlias];

                    NSString *planText = [NSString stringWithFormat:@"%@ %@\n%@", loan.planAmount, loan.currency, planDateText];
                    
                    NSString *periodText = [NSString stringWithFormat:@"%@", loan.periodMonth];
                    
                    vc.infoDataList = @[
                                        @{@"title": NSLocalizedString(@"credit_info_next_payment", nil),
                                          @"subtitle": planText},
                                        @{@"title": NSLocalizedString(@"remuneration_rate", nil),
                                          @"subtitle": loan.rate},
                                        @{@"title": NSLocalizedString(@"loan_term_in_months", nil),
                                          @"subtitle": periodText},
                                        @{@"title": NSLocalizedString(@"opening_date", nil),
                                          @"subtitle": openedDateText},
                                        ];
                } else {
                    NSString *periodText = [NSString stringWithFormat:@"%@", loan.periodMonth];
                    
                    vc.infoDataList = @[
                                        @{@"title": NSLocalizedString(@"remuneration_rate", nil),
                                          @"subtitle": loan.rate},
                                        @{@"title": NSLocalizedString(@"loan_term_in_months", nil),
                                          @"subtitle": periodText},
                                        @{@"title": NSLocalizedString(@"opening_date", nil),
                                          @"subtitle": openedDateText},
                                        ];
                }
            }
                break;
                
            default:
                break;
        }
        
        STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:vc];
        popupController.dismissOnBackgroundTap = true;
        popupController.style = STPopupStyleBottomSheet;
        popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
        [popupController presentInViewController:wSelf];
    };
    
    self.cardDetailHeaderView.onChargeDeposit = ^(Deposit *deposit) {
        /* TransfersFormViewController *vc = [TransfersFormViewController createVC];
        vc.title = [NewTransfersApi transferTypeToDescriptionString:MY_TRANSFERS];
        vc.transfersType = MY_TRANSFERS;
        vc.depositToRefill = deposit;
        [wSelf.navigationController pushViewController:vc animated:true];*/
        
       /* [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
        [NewTransfersApi getTemplatesWithSuccess:^(NSArray *transferTemplate) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            wSelf.templatesArray = transferTemplate;
            for(TransferTemplate *tt in wSelf.templatesArray)
            {
                NSLog(@"Template type: %@", tt.type);
            }
        } failure:^(NSString *code, NSString *message) {
             [MBProgressHUD hideAstanaHUDAnimated:YES];
        }];*/
        
        MyTransfersDependencies *myTransfersDependencies = [[MyTransfersDependencies alloc] init];
        myTransfersDependencies.targetChargeAccount = wSelf.account;
        [myTransfersDependencies installRootViewController];
        
    };
    
    self.cardDetailHeaderView.onFavouriteTapped = ^() {
        BOOL isFavourite = !wSelf.account.isFavorite;
        
        [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
        [AccountsApi setAccount:[AccountHelper getIdOfAccount:wSelf.account] accountType:wSelf.account.type isFavorite:isFavourite success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
            wSelf.account.isFavorite = isFavourite;
            wSelf.cardDetailHeaderView.account = wSelf.account;
            [wSelf.cardDetailHeaderView setChoosePeriodText:wSelf.fromDate toDate:wSelf.toDate];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAccountFavouriteChanged object:nil];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];
    };
    
    self.cardDetailHeaderView.onManageLimitsTapped = ^{
        AccountManageLimitsViewController *vc = [AccountManageLimitsViewController new];
        vc.card = (Card *)wSelf.account;
        
        STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:vc];
        popupController.dismissOnBackgroundTap = true;
        popupController.style = STPopupStyleBottomSheet;
        popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
        [popupController presentInViewController:wSelf];
    };

    self.cardDetailHeaderView.onSmsBankingTapped = ^{
        SmsBankingDependencies *d = [[SmsBankingDependencies alloc] init];
        d.parentVC = wSelf;
        d.smsBankingInteractor.cardId = wSelf.account.accountId;
        [d installRootViewController];
    };

    self.cardDetailHeaderView.onShareRequisitesTapped = ^(Account *account) {

        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"preparing_requisites", nil) animated:YES];
        [InfoApi clientInfo:[User sharedInstance].iin success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];

            NSString *fio = [NSString stringWithFormat:@"%@ %@ %@ %@\n", NSLocalizedString(@"fio_ben", nil),response[@"lastName"], response[@"firstName"], response[@"middleName"]];
            NSString *iban = [NSString stringWithFormat:@"%@ %@\n", NSLocalizedString(@"iban_ben", nil), account.iban];
            NSString *iin = [NSString stringWithFormat:@"%@ %@\n", NSLocalizedString(@"iin_ben", nil),[User sharedInstance].iin];
            NSString *other = NSLocalizedString(@"bank_info_ben", nil);
            NSString *wholeMsg = [NSString stringWithFormat:@"%@%@%@%@", fio, iban, iin, other];

            UIActivityViewController *activityViewController =
                    [[UIActivityViewController alloc] initWithActivityItems:@[wholeMsg]
                                                      applicationActivities:nil];
            [wSelf.navigationController presentViewController:activityViewController
                                               animated:YES
                                             completion:^{}];

        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
        }];

    };

    self.cardDetailHeaderView.onChargeCard = ^(Card *card) {

        [MBProgressHUD showAstanaHUDWithTitle:@"" animated:YES];
        [AccountsApi getOperationAccountsWithSuccess:^(OperationAccounts *operationAccounts) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            wSelf.operationAccounts = operationAccounts;



//            TransfersFormViewController *vc = [[TransfersFormViewController alloc] init];
//            vc.title = NSLocalizedString(@"add_card", nil);
//            vc.transfersType = MY_TRANSFERS;
//            vc.cardToRefill = card;
//            vc.operationAccounts = wSelf.operationAccounts;
//            [wSelf.navigationController pushViewController:vc animated:YES];



            MyTransfersDependencies *myTransfersDependencies = [[MyTransfersDependencies alloc] init];
            myTransfersDependencies.targetChargeAccount = card;
            [myTransfersDependencies installRootViewController];


        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
        }];
    };
    
    self.cardDetailHeaderView.onConfigCashBackTapped = ^{
        [wSelf loadAccountWithCompletion:^(Account *account) {
            CashBackConfigDependencies *d = [[CashBackConfigDependencies alloc] init];
            d.parentVC = wSelf;
            d.account = account;
            [d installRootViewController];
        }];
    };
}

/**
 * Показывает окошко для установки периода для фильтра выписки
 */
- (void)chooseStatementPeriod {
    __weak AccountDetailViewController *wSelf = self;

    DateIntervalPickerPopupViewController *v = [[DateIntervalPickerPopupViewController alloc] init];
    [v setFromDate:_fromDate andToDate:_toDate];
    v.datePickerTitle = NSLocalizedString(@"choose_statement_period", nil);

    v.completionBlock = ^(NSDate *fromDate, NSDate *toDate) {
        wSelf.fromDate = fromDate;
        wSelf.toDate = toDate;
        [wSelf loadStatementsFromDate:fromDate toDate:toDate];
        [wSelf.cardDetailHeaderView setChoosePeriodText:fromDate toDate:toDate];
    };

    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:v];
    popupController.navigationBarHidden = YES;
    popupController.style = STPopupStyleBottomSheet;
    popupController.dismissOnBackgroundTap = true;
    [popupController presentInViewController:self];
}

/**
 * Показывает алерт для выбора типа блокировки / разблокировки карты
 */
- (void)showCardAlertOptionsBlock {

    __weak AccountDetailViewController *wSelf = self;

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"card_block", nil) message:NSLocalizedString(@"choose_action", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    alertController.popoverPresentationController.sourceView = wSelf.view;

    if (!wSelf.account.isBlocked) {

        [alertController addAction:[UIAlertAction actionWithTitle:wSelf.account.isBlockedIBFL ? @"Разблокировать карту" : NSLocalizedString(@"block_card_temporarily_blocked", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *_Nonnull action) {

            if (!wSelf.account.isBlockedIBFL) {


                UIAlertController *alertController2 = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"prevention", nil)
                                                                                          message:NSLocalizedString(@"block_card_temporarily_blocked_status", nil) preferredStyle:UIAlertControllerStyleAlert];
                alertController2.popoverPresentationController.sourceView = wSelf.view;

                [alertController2 addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"confirm", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *_Nonnull action) {

                    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"card_block", nil) animated:true];
                    [AccountsApi blockAccount:wSelf.account.accountId status:@"TEMPORARILY_BLOCKED_IBFL" success:^(id response) {
                        [MBProgressHUD hideAstanaHUDAnimated:true];
                        [Toast showToast:[NSString stringWithFormat:@"%@\nСтатус карты в Интернет-Банкинге обновится в течении 10 минут.", NSLocalizedString(@"card_successfully_blocked", nil)]];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAccountBlocked object:nil];
                        [wSelf.navigationController popToRootViewControllerAnimated:YES];
                    } failure:^(NSString *code, NSString *message) {
                        [MBProgressHUD hideAstanaHUDAnimated:true];
                        [Toast showToast:NSLocalizedString(@"error_occured_while_card_blocking", nil)];
                    }];

                }]];

                [alertController2 addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:nil]];

                [wSelf presentViewController:alertController2 animated:true completion:nil];

            } else {

                [MBProgressHUD showAstanaHUDWithTitle:@"Отправка запроса на разблокировку" animated:true];
                [AccountsApi unblockAccount:wSelf.account.accountId success:^(id response) {
                    [wSelf showConfirSMSPopup];
                    [MBProgressHUD hideAstanaHUDAnimated:YES];
                } failure:^(NSString *code, NSString *message) {
                    [Toast showToast:@"Произошла ошибка при разблокировки карты"];
                    [MBProgressHUD hideAstanaHUDAnimated:YES];
                }];

            }

        }]];
    }

    if (!wSelf.account.isBlockByLock) {

        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"block_card_stolen", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *_Nonnull action) {
            UIAlertController *alertController2 = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"prevention", nil) message:NSLocalizedString(@"block_card_stolen_status", nil) preferredStyle:UIAlertControllerStyleAlert];
            alertController2.popoverPresentationController.sourceView = wSelf.view;

            [alertController2 addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"confirm", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *_Nonnull action) {
                [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"card_block", nil) animated:true];
                [AccountsApi blockAccount:wSelf.account.accountId status:@"LOST" success:^(id response) {
                    [MBProgressHUD hideAstanaHUDAnimated:true];
                    wSelf.account.isWarning = true;
                    wSelf.account.status = @"PickUp S 43";
                    [Toast showToast:NSLocalizedString(@"card_successfully_blocked", nil)];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAccountBlocked object:nil];
                } failure:^(NSString *code, NSString *message) {
                    [MBProgressHUD hideAstanaHUDAnimated:true];
                    [Toast showToast:NSLocalizedString(@"error_occured_while_card_blocking", nil)];
                }];
            }]];

            [alertController2 addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
            [wSelf presentViewController:alertController2 animated:true completion:nil];
        }]];

        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"block_card_lost", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *_Nonnull action) {
            UIAlertController *alertController2 = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"prevention", nil) message:NSLocalizedString(@"block_card_lost_status", nil) preferredStyle:UIAlertControllerStyleAlert];
            alertController2.popoverPresentationController.sourceView = wSelf.view;
            [alertController2 addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"confirm", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *_Nonnull action) {
                [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"card_block", nil) animated:true];
                [AccountsApi blockAccount:wSelf.account.accountId status:@"STOLEN" success:^(id response) {
                    [MBProgressHUD hideAstanaHUDAnimated:true];
                    wSelf.account.isWarning = true;
                    wSelf.account.status = @"PickUp S 43";
                    [Toast showToast:NSLocalizedString(@"card_successfully_blocked", nil)];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAccountBlocked object:nil];
                } failure:^(NSString *code, NSString *message) {
                    [MBProgressHUD hideAstanaHUDAnimated:true];
                    [Toast showToast:NSLocalizedString(@"error_occured_while_card_blocking", nil)];
                }];
            }]];
            [alertController2 addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
            [wSelf presentViewController:alertController2 animated:true completion:nil];
        }]];

        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
        [wSelf presentViewController:alertController animated:true completion:nil];
    }
}

/**
 * Показывает окошко для ввода смс кода для подтверждения разблокировки карты
 */
- (void)showConfirSMSPopup {
    __weak AccountDetailViewController *wSelf = self;
    __block SMSConfirmationPopupViewController *v = [[SMSConfirmationPopupViewController alloc] init];
    v.didDismissWithSuccessBlock = ^{
        [wSelf.tableView reloadData];
        [Toast showToast:@"Карта успешно разблокирована.\nСтатус карты в Интернет-Банкинге обновится в течении 10 минут."];
    };
    v.verifySmsCode = ^(SMSConfirmationPopupViewController *smsConfirmationPopup, NSString *smsCode) {
        [MBProgressHUD showAstanaHUDWithTitle:@"Верификация смс кода" animated:YES];
        [AccountsApi unblockProcessAccount:wSelf.account.accountId code:smsCode success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            wSelf.account.status = @"Card OK";
            wSelf.account.isWarning = NO;
            [v dismiss];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
        }];
    };
    v.sendSmsCodeAgain = ^(SMSConfirmationPopupViewController *smsConfirmationPopup) {
        [MBProgressHUD showAstanaHUDWithTitle:@"Повторная отправка смс кода" animated:YES];
        [AccountsApi resendSmsWithCardId:wSelf.account.accountId success:^(id response) {
            [Toast showToast:@"На ваш номер повторно отправлен смс код"];
            [MBProgressHUD hideAstanaHUDAnimated:YES];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
        }];
    };
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:v];
    popupController.style = STPopupStyleFormSheet;
    popupController.navigationBar.barTintColor = [UIColor fromRGB:0x09716d];
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [popupController presentInViewController:self];
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark - Empty data set

/**
 * определить месседж о пустых данных на скролвью
 * @param scrollView
 * @return
 */
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = NSLocalizedString(@"no_statements_for_period", nil);
    if (self.selectedStatement == SelectedStatementBlocked) {
        text = NSLocalizedString(@"no_block_statements", nil);
    }
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:13],
            NSForegroundColorAttributeName: [UIColor fromRGB:0xCBCBCB]};
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

/**
 * определись фрейм для месседжа о пустых данных
 * @param scrollView
 * @return
 */
- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return self.tableView.tableHeaderView.frame.size.height/2.0f;
}

/**
 * позволять делать скрол скролвью если нет данных
 * @param scrollView
 * @return
 */
- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}

#pragma mark - table delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (self.selectedStatement == SelectedStatementAviata) {
        return self.accountStatementsResponse.aviaStatements.count;
    }

    if (self.selectedStatement == SelectedStatementBlocked && self.account && ![self.account isKindOfClass:[Loan class]]) {
        if (self.accountStatementsResponse.blockStatements.count == 0) {
            return 1;
        }
        
        return self.accountStatementsResponse.blockStatements.count;
    }
    
    if (self.accountStatementsResponse.statements.count == 0 && self.account && ![self.account isKindOfClass:[Loan class]]) {
        return 1;
    }
    
    return self.accountStatementsResponse.statements.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;

    if (self.selectedStatement == SelectedStatementAviata) {
        Statement *selectedStatement = self.accountStatementsResponse.aviaStatements[(NSUInteger) indexPath.row];

        AviataStatementCell *cell = [tableView dequeueReusableCellWithIdentifier:AviaStatementCellIdentifier];
        cell.aviaStatement = selectedStatement;

        __weak AccountDetailViewController *wSelf = self;

        cell.onCompensateMileButtonClick = ^{
            AviaModuleDependencies *aviaModuleDependency = [[AviaModuleDependencies alloc] init];
            aviaModuleDependency.aviaMilesInteractor.card = (Card *) wSelf.account;
            aviaModuleDependency.aviaMilesInteractor.stament = selectedStatement;
            [aviaModuleDependency installRootViewController];
        };
        return cell;
    }
    
    if (self.selectedStatement == SelectedStatementBlocked) {

        if (self.accountStatementsResponse.blockStatements.count == 0 && self.account && ![self.account isKindOfClass:[Loan class]]) {
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            
            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"EmptyCell"];
            
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EmptyCell"];
            }
            
            cell.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.textLabel.font = [UIFont systemFontOfSize:13];
            cell.textLabel.textColor = [UIColor fromRGB:0xCBCBCB];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            
            NSString *text = NSLocalizedString(@"no_statements_for_period", nil);
            if (self.selectedStatement == SelectedStatementBlocked) {
                text = NSLocalizedString(@"no_block_statements", nil);
            }
            
            cell.textLabel.text = text;
            
            return cell;
        }
        
        StatementCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"StatementCell"];
        cell.statement = self.accountStatementsResponse.blockStatements[(NSUInteger) indexPath.row];
        return cell;
    }
    
    if (self.accountStatementsResponse.statements.count == 0 &&
            self.account && ![self.account isKindOfClass:[Loan class]]) {
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"EmptyCell"];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EmptyCell"];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont systemFontOfSize:13];
        cell.textLabel.textColor = [UIColor fromRGB:0xCBCBCB];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        NSString *text = NSLocalizedString(@"no_statements_for_period", nil);
        if (self.selectedStatement == SelectedStatementBlocked) {
            text = NSLocalizedString(@"no_block_statements", nil);
        }
        
        cell.textLabel.text = text;
        
        return cell;
    }
    
    StatementCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"StatementCell"];
    cell.statement = self.accountStatementsResponse.statements[(NSUInteger) indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

#pragma mark - config ui

- (void)configUI {

    //back button
    [self setNavigationBackButton];

    if ([self.account isKindOfClass:[Card class]]) {
        [self setNavigationTitle:NSLocalizedString(@"card_detail", nil)];
    } else if ([self.account isKindOfClass:[CardAccount class]]) {
        [self setNavigationTitle:@"Детали карт счета"];
    } else if ([self.account isKindOfClass:[Current class]]) {
        [self setNavigationTitle:@"Детали текущего счета"];
    } else if ([self.account isKindOfClass:[Deposit class]]) {
        [self setNavigationTitle:@"Детали депозита"];
    } else if ([self.account isKindOfClass:[Loan class]]) {
        [self setNavigationTitle:@"Детали кредита"];
    }


    [self setBafBackground];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reloadAccount)];

    self.tableView.backgroundColor = [UIColor clearColor];

    //table header view
    self.cardDetailHeaderView = [[AccountDetailHeaderView alloc] init];
    self.cardDetailHeaderView.parentVC = self;
    self.cardDetailHeaderView.width = [ASize screenWidth];
    self.cardDetailHeaderView.account = self.account;
    [self configCardDetailHeaderActions];
    [self.cardDetailHeaderView setChoosePeriodText:_fromDate toDate:_toDate];

    //tableheaderview with autolayout solution
    [self.cardDetailHeaderView setNeedsLayout];
    [self.cardDetailHeaderView layoutIfNeeded];
    CGFloat height = [self.cardDetailHeaderView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    self.cardDetailHeaderView.height = height;
    self.cardDetailHeaderView.width = [ASize screenWidth];
    self.tableView.tableHeaderView = self.cardDetailHeaderView;

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 400;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0,0,[ASize screenWidth], 80)];
    self.tableView.bounces = true;
    self.tableView.alwaysBounceVertical = YES;

    self.tableView.clipsToBounds = NO;
    self.tableView.layer.masksToBounds = NO;
    self.tableView.layer.shadowColor = [UIColor fromRGB:0x444444].CGColor;
    self.tableView.layer.shadowOpacity = 0.3;
    self.tableView.layer.shadowOffset = CGSizeMake(0,0);
    self.tableView.layer.shadowRadius = 5;
    self.tableView.layer.cornerRadius = 10;

    //register aviastatementcell
    UINib *nib = [UINib nibWithNibName:@"AviataStatementCell" bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:AviaStatementCellIdentifier];

    // observers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadAccount) name:kNotificationAccountLimitChanged object:nil];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"AccountDetailViewController DEALLOC");
}

@end
