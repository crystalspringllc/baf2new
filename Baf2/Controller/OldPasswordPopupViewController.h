//
//  OldPasswordPopupViewController.h
//  BAF
//
//  Created by Askar Mustafin on 2/24/16.
//  Copyright © 2016 Банк Астаны. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <STPopup/STPopup.h>

#define kPopupWidth ([ASize screenWidth] - 20)
@class InputTextField;

@interface OldPasswordPopupViewController : UIViewController

- (id)initWithSecretQuestion:(NSString *)secretQuestion;

@property (nonatomic, copy) void (^completeWithSuccess)(BOOL success, NSString *oldPassword, NSString *secretQuestion);

@property (nonatomic, strong) InputTextField *passwordTextField;
@property (nonatomic, strong) InputTextField *secretQuestionTextField;

@end
