//
//  RegistrationStep1ViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 26.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationStep1ViewController : UITableViewController

@property (nonatomic, copy) void (^didValidate)();

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *middleName;

@end
