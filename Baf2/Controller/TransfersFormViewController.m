//
//  TransfersFormViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 31.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "TransfersFormViewController.h"

#import "AddJuridicalBeneficiaryViewController.h"
#import "AddPhysicalBeneficiaryViewController.h"

#import "BankTransferConfirmViewController.h"
#import "CardMultiCurrencyConverterConfirmationViewController.h"
#import "EditBankTransferBeneficiaryAccountsViewController.h"
#import "KnpCatalogViewController.h"
#import "SearchBeneficiaryViewController.h"
#import "ActionSheetListPopupViewController.h"
#import "Card.h"
#import "MainButton.h"
#import "STPopupController+Extensions.h"
#import "TransfersFormCell.h"
#import "TransfersFormDisclaimerCell.h"
#import <JVFloatLabeledTextField.h>
#import <UITextField+Blocks.h>
#import <ChameleonFramework/ChameleonMacros.h>
#import <UIActionSheet+Blocks/UIActionSheet+Blocks.h>
// Models
#import "TransferCurrencyRates.h"
#import "AccountsApi.h"
#import "OperationAccounts.h"
#import "Accounts.h"
#import "Deposit.h"
#import "Current.h"
#import "Knp.h"
#import "TransferTemplate.h"
// Other
#import "NotificationCenterHelper.h"
#import "NSDateFormatter+Ext.h"
#import "AccountHelper.h"
#import "UITableViewController+Extension.h"
#import "FloatingTextField.h"
#import "NSString+Ext.h"
#import "KnpCell.h"
#import "CatalogApi.h"
#import "IQKeyboardManager.h"

@interface TransfersFormViewController ()
        <UIPickerViewDataSource, UIPickerViewDelegate,
        UITextFieldDelegate, SearchBeneficiaryViewControllerDelegate>

@property (nonatomic, strong) UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet TransfersFormCell *transferFromCell;
@property (weak, nonatomic) IBOutlet TransfersFormCell *transferToCell;
@property (weak, nonatomic) IBOutlet TransfersFormDisclaimerCell *currencyDisclaimerCell;
@property (weak, nonatomic) IBOutlet TransfersFormDisclaimerCell *currencyCalculatorDisclaimerCell;
@property (nonatomic, strong) ActionSheetListPopupViewController *fromActionSheet;
@property (nonatomic, strong) ActionSheetListPopupViewController *toActionSheet;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *amountTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *currencyTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *templateNameTextField;
@property (nonatomic, strong) UISwitch *saveAsTemplateSwitch;
@property (weak, nonatomic) IBOutlet MainButton *submitButton;
@property (nonatomic, strong) Knp *selectedKnp;
@property (nonatomic, strong) NSArray *knpList;
@property (nonatomic, strong) NSArray *allKnpList;
@property (nonatomic, strong) NSString *selectedKnpCode;
@property (nonatomic, strong) NSString *selectedKnpName;
@property (nonatomic, strong) NSString *selectedClientDesc;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *templateAliasSpinner;
@property (nonatomic, strong) UIActivityIndicatorView *rightNavBarSpinner;
@property (nonatomic, strong) id fromAccount;
@property (nonatomic, strong) NSString *fromCurrency;
@property (nonatomic, strong) id toAccount;
@property (nonatomic, strong) NSString *toCurrency;
@property (nonatomic, strong) NSNumber *comission;
@property (nonatomic, strong) NSString *comissionCurrency;
@property (nonatomic, strong) NSArray *warningMessages;
@property (nonatomic, strong) NSString *errorMessage;
@property (nonatomic) BOOL isValid;
@property (nonatomic, strong) TransferCurrencyRates *currencyRates;
@property (nonatomic, strong) NSNumber *servicelogId;
@property (nonatomic, strong) NSString *safetyLevel;
@property (nonatomic, strong) NSNumber *requestorAmount;
@property (nonatomic, strong) NSNumber *destinationAmount;
@property (nonatomic, strong) NSString *selectedCurrency;
@property (nonatomic) NSInteger selectedCurrencyIndex;
@end

@implementation TransfersFormViewController {
    BOOL loading;
    BOOL needReload;
}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Transfers" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([TransfersFormViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self configUI];

    switch (self.transfersType) {
        case INTERBANK_TRANSFERS: {
            [self getOperationAccounts:false];
            [self loadKnpList];
        }
            break;
        case MY_CONVERT: {
            [self onOperationAccounts:nil];;
        }
            break;

        default:
            break;
    }

    if (self.transferTemplate) {
        [self checkTransfer];
    }
}

#pragma mark - Actions

- (void)goBack {
    [super goBack];

    if (needReload) {
        [[NSNotificationCenter defaultCenter] 
                postNotificationName:kNotificationTransfersOperationAccountsUpdated 
                              object:nil];
    }
}

- (void)saveAsTempate:(UISwitch *)sw {
    self.templateNameTextField.enabled = sw.isOn;
    [self.tableView reloadData];
}

- (void)searchBeneficiaryTapped {
    __weak TransfersFormViewController *wSelf = self;
    if (self.transfersType == INTERBANK_TRANSFERS) {
        [UIActionSheet showInView:self.view
                        withTitle:kTextStringAddingReceiverAccout
                cancelButtonTitle:kTextStringcCncel
           destructiveButtonTitle:nil
                otherButtonTitles:@[kTextStringFizFace, kTextStringJurFace]
                         tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
                             if (actionSheet.cancelButtonIndex != buttonIndex) {
                                 if (buttonIndex == 0) {
                                     AddPhysicalBeneficiaryViewController *v = [AddPhysicalBeneficiaryViewController createVC];
                                     [wSelf.navigationController pushViewController:v animated:YES];
                                 } else {
                                     AddJuridicalBeneficiaryViewController *v = [AddJuridicalBeneficiaryViewController createVC];
                                     [wSelf.navigationController pushViewController:v animated:YES];
                                 }
                             }
                         }];

        return;
    }

    UINavigationController *nc = [UIHelper defaultNavigationController];
    nc.navigationBar.barStyle = UIBarStyleBlack;
    SearchBeneficiaryViewController *v = [[SearchBeneficiaryViewController alloc] init];
    v.delegate = self;
    [nc setViewControllers:@[v]];
    [self presentViewController:nc animated:YES completion:nil];
}

- (void)removeBeneficiaryTapped {
    EditBankTransferBeneficiaryAccountsViewController *vc = [EditBankTransferBeneficiaryAccountsViewController new];

    NSPredicate *filter = [NSPredicate predicateWithFormat:@"isInterbank == NO"];

        NSMutableArray *benefs = [[NSMutableArray alloc] init];

        NSMutableDictionary *toData = [NSMutableDictionary new];
        NSArray *toOptions = [self toOptions];
        for (NSDictionary *option in toOptions) {
            id account = option[@"id"];

            if ([account isKindOfClass:[Card class]]) {
                if (toData[kTextStringCards]) {
                    NSMutableArray *options = [toData[kTextStringCards] mutableCopy];
                    [options addObject:option];
                    toData[kTextStringCards] = options;
                } else {
                    toData[kTextStringCards] = [NSMutableArray arrayWithObject:option];
                }
            } else if ([account isKindOfClass:[Current class]]) {
                if (toData[kTextStringAcounts]) {
                    NSMutableArray *options = [toData[kTextStringAcounts] mutableCopy];
                    [options addObject:option];
                    toData[kTextStringAcounts] = options;
                } else {
                    toData[kTextStringAcounts] = [NSMutableArray arrayWithObject:option];
                }
            } else if ([account isKindOfClass:[Deposit class]]) {
                if (toData[kTextStringDeposits]) {
                    NSMutableArray *options = [toData[kTextStringDeposits] mutableCopy];
                    [options addObject:option];
                    toData[kTextStringDeposits] = options;
                } else {
                    toData[kTextStringDeposits] = [NSMutableArray arrayWithObject:option];
                }
            } else {
                if (toData[@""]) {
                    NSMutableArray *options = [toData[@""] mutableCopy];
                    [options addObject:option];
                    toData[@""] = options;
                } else {
                    toData[@""] = [NSMutableArray arrayWithObject:option];
                }
            }
        }

        for (NSDictionary *d in toData[@""]) {
            [benefs addObject:d[@"id"]];
        }

        vc.beneficiaryAccounts = benefs;



    [self.navigationController pushViewController:vc animated:true];
}

- (void)deleteTransferTemplate:(UIBarButtonItem *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:kTextStringDeletaTemplate preferredStyle:UIAlertControllerStyleAlert];
    alertController.popoverPresentationController.barButtonItem = sender;

    __weak TransfersFormViewController *weakSelf = self;
    [alertController addAction:[UIAlertAction actionWithTitle:kTextStringRemove style:UIAlertActionStyleDestructive handler:^(UIAlertAction *_Nonnull action) {
        [MBProgressHUD showAstanaHUDWithTitle:kTextStringDeletingTemplate animated:true];

        [NewTransfersApi setTemplatesDelete:true templateId:self.transferTemplate.templateId success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:true];

            [Toast showToast:kTextStringTemplateSuccesfullyRemoved];

            [weakSelf goBack];

            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransfersTemplatesUpdated object:nil];
        }                           failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:kTextStringcCncel style:UIAlertActionStyleCancel handler:nil]];

    [self presentViewController:alertController animated:true completion:nil];
}

#pragma mark - API

- (void)getOperationAccounts:(BOOL)reload {
    __weak TransfersFormViewController *weakSelf = self;

    if (!loading && ((self.depositToRefill && !self.operationAccounts) || reload)) {

        if (loading) {
            return;
        }

        loading = true;

        [MBProgressHUD showAstanaHUDWithTitle:kTextStringLoadingOperationalAccounts animated:true];
        [AccountsApi getOperationAccountsWithSuccess:^(OperationAccounts *operationAccounts) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
            [weakSelf onOperationAccounts:operationAccounts];

            loading = false;
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];

            loading = false;
        }];

        return;
    }

    if (self.transferTemplate) {
        [self onOperationAccounts:nil];

        return;
    }

    [self onOperationAccounts:self.operationAccounts];
}

- (void)reloadOperationsAccount:(NSNotification *)notification {
    needReload = true;

    if (!loading) {
        [self getOperationAccounts:true];
    }
}

- (void)checkTransfer {


    // make request if only all fields are filled
    switch (self.transfersType) {
        case INTERBANK_TRANSFERS: {
            if (!self.amountTextField.text || self.amountTextField.text.length == 0) {
                return;
            }
        }
            break;
        case MY_CONVERT: {
            if (!self.amountTextField.text || self.amountTextField.text.length == 0 || !self.fromCurrency || !self.toCurrency) {
                return;
            }
        }
            break;
        default:
            break;
    }

    if (!self.toAccount || !self.fromAccount) {
        return;
    }

    if (self.saveAsTemplateSwitch.isOn && self.templateNameTextField.text.length == 0) {
        return;
    }

    [self.submitButton setMainButtonStyle:MainButtonStyleDisable];

    BOOL direction = true;

    NSString *fromCurrency = self.transfersType == MY_CONVERT ? self.fromCurrency : ((Account *) self.fromAccount).currency;
    NSString *toCurrency = self.transfersType == MY_CONVERT ? self.toCurrency : ((Account *) self.toAccount).currency;

    if (self.selectedCurrency && [self.selectedCurrency isEqualToString:toCurrency] && ![fromCurrency isEqualToString:toCurrency]) {
        direction = false;
    }

    NSNumber *amountFloat = [[self.amountTextField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] numberDecimalFormat];

    [MBProgressHUD showAstanaHUDWithTitle:kTextStringChecking animated:YES];
    [NewTransfersApi checkTransfer:self.transfersType
                       requestorId:[AccountHelper getIdOfAccount:self.fromAccount]
                   requestorAmount:amountFloat
                     destinationId:[AccountHelper getIdOfAccount:self.toAccount]
                     requestorType:((Account *) self.fromAccount).type
                 requestorCurrency:fromCurrency
                   destinationType:((Account *) self.toAccount).type
               destinationCurrency:toCurrency
                           knpCode:self.selectedKnpCode ? : @""
                           knpName:self.selectedKnpName ? : @""
                         direction:direction
                              save:self.saveAsTemplateSwitch.isOn
                             alias:self.templateNameTextField.text
                           success:^(id response) {
                               [MBProgressHUD hideAstanaHUDAnimated:YES];
                               [self handleCheckTransferResponse:response];
                           } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
            }];
}

- (void)handleCheckTransferResponse:(id)response {

    self.warningMessages = response[@"warningMessages"];
    self.currencyRates = [TransferCurrencyRates instanceFromDictionary:response[@"exchange"]];
    self.requestorAmount = response[@"requestorAmount"];
    self.destinationAmount = response[@"destinationAmount"];
    self.comission = response[@"comission"];
    self.comissionCurrency = response[@"comissionCurrency"];
    self.safetyLevel = response[@"safetyLevel"];
    self.isValid = [response[@"isValid"] boolValue];

    //disable button if check return isValid false
    if (self.isValid) {
        [self.submitButton setMainButtonStyle:MainButtonStyleOrange];
    } else {
        [self.submitButton setMainButtonStyle:MainButtonStyleDisable];
    }

    //show errorMessage
    self.errorMessage = response[@"errorMessage"];
    if (self.errorMessage) {
        if ([self.errorMessage isEqualToString:@"Operations from the deposit to a third party is prohibited"]) {
            self.errorMessage = kTextStringProviderTemporaryUnavailable;
        }

        [Toast showToast:self.errorMessage];
    }



    NSMutableArray *knps = [[NSMutableArray alloc] init];
    if (response[@"knpList"][@"list"]) {
        for (NSDictionary *knp in response[@"knpList"][@"list"]) {
            [knps addObject:knp];
        }
        self.knpList = knps;
    }


    [self.tableView reloadData];
}

- (void)initTransfer {

    // make request if only all fields are filled
    switch (self.transfersType) {
        case INTERBANK_TRANSFERS: {
            if (!self.amountTextField.text ||
                    self.amountTextField.text.length == 0 ||
                    !self.selectedCurrency) {
                [UIHelper showAlertWithMessageTitle:kTextStringErrorInForm message:kTextStringFillAllFields];
                return;
            }
            if ((!self.selectedKnpCode || self.selectedKnpCode.length == 0) || (!self.selectedKnpName || self.selectedKnpName.length == 0)) {
                [UIHelper showAlertWithMessageTitle:kTextStringErrorInForm message:kTextStringIncorrectKnpCode];
                return;
            }
        }
            break;
        case MY_CONVERT: {
            if (!self.amountTextField.text || self.amountTextField.text.length == 0 || !self.fromCurrency || !self.toCurrency) {
                [UIHelper showAlertWithMessageTitle:kTextStringErrorInForm message:kTextStringFillAllFields];
                return;
            }
        }
            break;

        default:
            break;
    }

    if (!self.toAccount || !self.fromAccount) {
        [UIHelper showAlertWithMessage:kTextStringChooseAccountsFortransfer];
        return;
    }

    if (self.saveAsTemplateSwitch.isOn && self.templateNameTextField.text.length == 0) {
        [UIHelper showAlertWithMessage:kTextStringEnterTemplateName];
        return;
    }

    [self.submitButton setMainButtonStyle:MainButtonStyleDisable];

    BOOL direction = true;

    NSString *fromCurrency = self.transfersType == MY_CONVERT ? self.fromCurrency : ((Account *) self.fromAccount).currency;
    NSString *toCurrency = self.transfersType == MY_CONVERT ? self.toCurrency : ((Account *) self.toAccount).currency;

    if (self.selectedCurrency && [self.selectedCurrency isEqualToString:toCurrency] && ![fromCurrency isEqualToString:toCurrency]) {
        direction = false;
    }

    [MBProgressHUD showAstanaHUDWithTitle:kTextStringChekingTransfer animated:true];
    
    float amountFloat = [[self.amountTextField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];

    __weak TransfersFormViewController *weakSelf = self;
    
    
    [NewTransfersApi initTransfer:self.transfersType
                      requestorId:[AccountHelper getIdOfAccount:self.fromAccount]
                  requestorAmount:@(amountFloat)
                    destinationId:[AccountHelper getIdOfAccount:self.toAccount]
                    requestorType:((Account *) self.fromAccount).type
                requestorCurrency:fromCurrency
                  destinationType:((Account *) self.toAccount).type
              destinationCurrency:toCurrency
                          knpCode:self.selectedKnpCode
                          knpName:self.selectedKnpName
                        direction:direction
                             save:self.saveAsTemplateSwitch.isOn
                            alias:self.templateNameTextField.text
                       clientDesc:self.selectedClientDesc
                          success:^(id response) {
                              [MBProgressHUD hideAstanaHUDAnimated:true];

                              weakSelf.warningMessages = response[@"warningMessages"];
                              weakSelf.currencyRates = [TransferCurrencyRates instanceFromDictionary:response[@"exchange"]];
                              weakSelf.destinationAmount = response[@"destinationAmount"];

                              weakSelf.errorMessage = response[@"errorMessage"];
                              if (weakSelf.errorMessage) {
                                  if ([weakSelf.errorMessage isEqualToString:@"Operations from the deposit to a third party is prohibited"]) {
                                      weakSelf.errorMessage = kTextStringProviderTemporaryUnavailable;
                                  }

                                  [Toast showToast:weakSelf.errorMessage];
                              }

                              weakSelf.isValid = [response[@"isValid"] boolValue];
                              weakSelf.servicelogId = response[@"servicelogId"];
                              weakSelf.comission = response[@"comission"];
                              weakSelf.comissionCurrency = response[@"comissionCurrency"];
                              weakSelf.safetyLevel = response[@"safetyLevel"];

                              if (weakSelf.isValid) {
                                  [self.submitButton setMainButtonStyle:MainButtonStyleOrange];
                              }


                              [weakSelf.tableView reloadData];


                              [self onTransferInit:weakSelf.saveAsTemplateSwitch.isOn];
                          } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:true];

                [self.submitButton setMainButtonStyle:MainButtonStyleOrange];
            }
    ];
}

- (void)getTransfer:(NSNumber *)servicelogId saveAsTemplate:(BOOL)saveAsTemplate {
    [MBProgressHUD showAstanaHUDWithTitle:kTextStringPreparingToTransfer animated:true];

    __weak TransfersFormViewController *weakSelf = self;
    [NewTransfersApi getTransfer:servicelogId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];

        if (self.transfersType == MY_CONVERT) {
            CardMultiCurrencyConverterConfirmationViewController *v = [[CardMultiCurrencyConverterConfirmationViewController alloc] init];
            v.servicelogId = weakSelf.servicelogId;
            v.cardId = weakSelf.currencyConvertAccount.accountId;
            v.currencyRates = weakSelf.currencyRates;
            v.fromCurrency = weakSelf.fromCurrency;
            v.toCurrency = weakSelf.toCurrency;
            v.amount = self.requestorAmount;//[weakSelf.amountTextField.text numberDecimalFormat];
            v.cardCurrencyAccount = (Card *) weakSelf.currencyConvertAccount;
            v.reqType = weakSelf.currencyConvertAccount.type;
            v.convertedAmountWithCurrency = [NSString stringWithFormat:@"%@ %@", [weakSelf.destinationAmount decimalFormatString], weakSelf.toCurrency];
            v.comission = weakSelf.comission;
            v.comissionCurrency = weakSelf.comissionCurrency;
            v.saveAsTemplate = saveAsTemplate;
            [weakSelf.navigationController pushViewController:v animated:YES];
        } else {
            BankTransferConfirmViewController *v = [[BankTransferConfirmViewController alloc] init];
            v.isOldVersion = NO;
            v.servicelogId = weakSelf.servicelogId;
            v.safetyLevel = weakSelf.safetyLevel;
            v.commission = weakSelf.comission;
            v.commissionCurrency = weakSelf.comissionCurrency;
            v.fromAccount = weakSelf.fromAccount;
            v.toAccount = weakSelf.toAccount;
            v.currency = weakSelf.selectedCurrency;
            v.amount = [weakSelf.amountTextField.text numberDecimalFormat];
            v.transferType = [NewTransfersApi transferTypeToString:weakSelf.transfersType];
            v.saveAsTemplate = saveAsTemplate;
            [weakSelf.navigationController pushViewController:v animated:YES];
        }
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

- (void)saveTransferAlias {
    __weak TransfersFormViewController *weakSelf = self;

    self.templateAliasSpinner.hidden = false;
    [self.templateAliasSpinner startAnimating];
    [NewTransfersApi setTemplatesAlias:self.templateNameTextField.text templateId:self.transferTemplate.templateId success:^(id response) {
        NSLog(@"Successfully set new alias for template");
        [weakSelf.templateAliasSpinner stopAnimating];
        weakSelf.templateAliasSpinner.hidden = true;
    }                          failure:^(NSString *code, NSString *message) {
        NSLog(@"Failed to set new alias for template");
        [weakSelf.templateAliasSpinner stopAnimating];
        weakSelf.templateAliasSpinner.hidden = true;
    }];
}

- (void)onOperationAccounts:(OperationAccounts *)operationAccounts {
    self.operationAccounts = operationAccounts;

    NSArray *fromOptions = [self fromOptions];

    if ((fromOptions && fromOptions.count > 0) || self.transferTemplate) {
        switch (self.transfersType) {
            case INTERBANK_TRANSFERS: {
                if (!self.transferTemplate) {
                    // !!!: Uncomment if bank ask to change again
                    // Commented lines of code select first cell on load
//                    id fromAccount = fromOptions[0][@"id"];
//                    self.fromAccount = fromAccount;
                    if (self.cardToRefill) {
                        self.toAccount = self.cardToRefill;
                        break;
                    }
                    self.fromAccount = nil;

                } else {
                    self.fromAccount = self.transferTemplate.requestorAccount;
                }
            }
                break;
            case MY_CONVERT: {
                if (!self.transferTemplate) {
                    // !!!: Uncomment if bank ask to change again
                    // Commented lines of code select first cell on load
//                    self.fromCurrency = fromOptions[0][@"id"];
//                    self.fromAccount = self.currencyConvertAccount;
                    self.fromAccount = nil;
                } else {
                    self.fromCurrency = self.transferTemplate.requestorCurrency;
                    self.fromAccount = self.transferTemplate.requestorAccount;
                }
            }
                break;

            default:
                break;
        }
    }

    [self.tableView reloadData];
}

- (void)onTransferInit:(BOOL)saveAsTemplate {
    if (self.isValid && self.servicelogId) {
        [self getTransfer:self.servicelogId saveAsTemplate:saveAsTemplate];
    }
}

#pragma mark - Getters/Setters

- (void)setFromAccount:(Account *)fromAccount {
    _fromAccount = fromAccount;

    if (!fromAccount) {
        self.transferFromCell.iconImageView.image = nil;
        self.transferFromCell.mainTitleLabel.text = kTextStringChooseAccount;
        self.transferFromCell.leftDescriptionLabel.text = @"-";
        self.transferFromCell.rightDescriptionLabel.text = @"-";

        self.toAccount = nil;

        self.transferToCell.iconImageView.image = [UIImage new];
        self.transferToCell.mainTitleLabel.text = kTextStringChooseAccount;
        self.transferToCell.leftDescriptionLabel.text = @"-";
        self.transferToCell.rightDescriptionLabel.text = @"-";


        self.amountTextField.enabled = false;
        self.currencyTextField.enabled = false;


        self.submitButton.mainButtonStyle = MainButtonStyleDisable;

        return;
    }

    switch (self.transfersType) {
        case INTERBANK_TRANSFERS: {
            //Заполняет текста в сел
            if ([fromAccount isKindOfClass:[Card class]]) {
                [self.transferFromCell.iconImageView sd_setImageWithURL:[NSURL URLWithString:((Card *) fromAccount).logo]];
                self.transferFromCell.mainTitleLabel.text = ((Card *) fromAccount).alias;
                self.transferFromCell.leftDescriptionLabel.text = ((Card *) fromAccount).number;
                self.transferFromCell.rightDescriptionLabel.text = ((Card *) fromAccount).balanceWithCurrency;
            } else if ([fromAccount isKindOfClass:[Current class]]) {
                [self.transferFromCell.iconImageView sd_setImageWithURL:[NSURL URLWithString:kBafLogoUrlSmall]];
                self.transferFromCell.mainTitleLabel.text = ((Current *) fromAccount).alias;
                self.transferFromCell.leftDescriptionLabel.text = ((Current *) fromAccount).iban;
                self.transferFromCell.rightDescriptionLabel.text = ((Current *) fromAccount).balanceWithCurrency;
            } else if ([fromAccount isKindOfClass:[Deposit class]]) {
                [self.transferFromCell.iconImageView sd_setImageWithURL:[NSURL URLWithString:kBafLogoUrlSmall]];
                self.transferFromCell.mainTitleLabel.text = ((Deposit *) fromAccount).alias;
                self.transferFromCell.leftDescriptionLabel.text = ((Deposit *) fromAccount).iban;
                self.transferFromCell.rightDescriptionLabel.text = ((Deposit *) fromAccount).balanceWithCurrency;
            } else {
                if (self.transferTemplate.requestorAccount.logo) {
                    [self.transferFromCell.iconImageView sd_setImageWithURL:[NSURL URLWithString:self.transferTemplate.requestorAccount.logo]];
                } else {
                    [self.transferFromCell.iconImageView sd_setImageWithURL:[NSURL URLWithString:kBafLogoUrlSmall]];
                }
                self.transferFromCell.mainTitleLabel.text = ((Account *) fromAccount).alias;
                self.transferFromCell.leftDescriptionLabel.text = ((Account *) fromAccount).iban;
                self.transferFromCell.rightDescriptionLabel.text = ((Account *) fromAccount).getBalanceWithCurrency;
            }

            if (!self.currencyTextField.text || self.currencyTextField.text.length == 0 || (self.selectedCurrencyIndex == 0 && self.selectedCurrency != ((Account *) self.fromAccount).currency)) {
                self.currencyTextField.text = ((Account *) fromAccount).currency;

                self.selectedCurrency = ((Account *) self.fromAccount).currency;
                self.selectedCurrencyIndex = 0;
            }
        }
            break;
        case MY_CONVERT: {
            id fa = fromAccount;
            if ([fa respondsToSelector:@selector(logo)]) {
                [self.transferFromCell.iconImageView sd_setImageWithURL:[NSURL URLWithString:[fa logo]]];
            } else {
                self.transferFromCell.iconImageView.image = [UIImage imageNamed:@"icon-baf-logo"];
            }

            self.transferFromCell.mainTitleLabel.text = ((Account *) fromAccount).alias;
            self.transferFromCell.leftDescriptionLabel.text = ((Account *) fromAccount).iban;

            NSString *balanceWithCurrency = ((Account *) fromAccount).getBalanceWithCurrency;
            if ([self.fromCurrency isEqualToString:@"KZT"]) {
                balanceWithCurrency = [NSString stringWithFormat:@"%@ %@", [fromAccount.balanceKzt decimalFormatString], @"KZT"];
            } else if ([self.fromCurrency isEqualToString:@"USD"]) {
                balanceWithCurrency = [NSString stringWithFormat:@"%@ %@", [fromAccount.balanceUsd decimalFormatString], @"USD"];
            } else if ([self.fromCurrency isEqualToString:@"EUR"]) {
                balanceWithCurrency = [NSString stringWithFormat:@"%@ %@", [fromAccount.balanceEur decimalFormatString], @"EUR"];
            } else if ([self.fromCurrency isEqualToString:@"RUB"]) {
                balanceWithCurrency = [NSString stringWithFormat:@"%@ %@", [fromAccount.balanceRub decimalFormatString], @"RUB"];
            } else if ([self.fromCurrency isEqualToString:@"GBP"]) {
                balanceWithCurrency = [NSString stringWithFormat:@"%@ %@", [fromAccount.balanceGbp decimalFormatString], @"GBP"];
            }

            self.transferFromCell.rightDescriptionLabel.text = balanceWithCurrency;

            if (!self.currencyTextField.text || self.currencyTextField.text.length == 0 || (self.selectedCurrencyIndex == 0 && self.selectedCurrency != self.fromCurrency)) {
                self.currencyTextField.text = self.fromCurrency;

                self.selectedCurrency = self.fromCurrency;
                self.selectedCurrencyIndex = 0;
            }
        }
            break;

        default:
            break;
    }

    NSArray *toOptions = [self toOptions];
    if ((toOptions && toOptions.count > 0) || self.transferTemplate) {

        self.amountTextField.enabled = YES;
        self.currencyTextField.enabled = YES;

        switch (self.transfersType) {
            case INTERBANK_TRANSFERS: {
                if (!self.transferTemplate) {
                    if (!self.cardToRefill) {
                        self.toAccount = nil;
                        self.transferToCell.iconImageView.image = [UIImage new];
                        self.transferToCell.mainTitleLabel.text = kTextStringChooseAccount;
                        self.transferToCell.leftDescriptionLabel.text = @"-";
                        self.transferToCell.rightDescriptionLabel.text = @"-";
                        self.amountTextField.enabled = false;
                        self.currencyTextField.enabled = false;
                        self.submitButton.mainButtonStyle = MainButtonStyleDisable;
                    }
                } else {
                    self.toAccount = self.transferTemplate.destinationAccount;
                    self.amountTextField.enabled = true;
                    self.currencyTextField.enabled = true;
                }
            }
                break;
            case MY_CONVERT: {
                if (!self.transferTemplate) {

                    if (self.fromAccount &&
                            self.toAccount &&
                            [((Account *) self.fromAccount).accountId integerValue] ==
                                    [((Account *) self.toAccount).accountId integerValue]) {

                        self.toCurrency = toOptions[0][@"id"];
                        self.toAccount = self.currencyConvertAccount;

                        self.amountTextField.enabled = true;
                        self.currencyTextField.enabled = true;
                    }
                } else {
                    self.toCurrency = self.transferTemplate.destinationCurrency;
                    self.toAccount = self.transferTemplate.destinationAccount;

                    self.amountTextField.enabled = true;
                    self.currencyTextField.enabled = true;
                }
            }
                break;

            default:
                break;
        }
    } else {
        self.toAccount = nil;

        self.transferToCell.iconImageView.image = [UIImage new];
        self.transferToCell.mainTitleLabel.text = kTextStringChooseAccount;
        self.transferToCell.leftDescriptionLabel.text = @"-";
        self.transferToCell.rightDescriptionLabel.text = @"-";

        self.amountTextField.enabled = false;
        self.currencyTextField.enabled = false;

        self.submitButton.mainButtonStyle = MainButtonStyleDisable;
    }
}

- (void)setToAccount:(Account *)toAccount {
    _toAccount = toAccount;

    if (!toAccount) {
        return;
    }

    switch (self.transfersType) {
        case INTERBANK_TRANSFERS: {
            if ([toAccount isKindOfClass:[Card class]]) {
                [self.transferToCell.iconImageView sd_setImageWithURL:[NSURL URLWithString:((Card *) toAccount).logo]];
                self.transferToCell.mainTitleLabel.text = ((Card *) toAccount).alias;
                self.transferToCell.leftDescriptionLabel.text = ((Card *) toAccount).number;
                    self.transferToCell.rightDescriptionLabel.text = ((Card *) toAccount).currency;
            } else if ([toAccount isKindOfClass:[Current class]]) {
                [self.transferToCell.iconImageView sd_setImageWithURL:[NSURL URLWithString:kBafLogoUrlSmall]];
                self.transferToCell.mainTitleLabel.text = ((Current *) toAccount).alias;
                self.transferToCell.leftDescriptionLabel.text = ((Current *) toAccount).iban;
                    self.transferToCell.rightDescriptionLabel.text = ((Current *) toAccount).currency;
            } else if ([toAccount isKindOfClass:[Deposit class]]) {
                [self.transferToCell.iconImageView sd_setImageWithURL:[NSURL URLWithString:kBafLogoUrlSmall]];
                self.transferToCell.mainTitleLabel.text = ((Deposit *) toAccount).alias;
                self.transferToCell.leftDescriptionLabel.text = ((Deposit *) toAccount).iban;
                    self.transferToCell.rightDescriptionLabel.text = ((Deposit *) toAccount).currency;

            } else if ([toAccount isKindOfClass:[Beneficiary class]]) {
                [self.transferToCell.iconImageView sd_setImageWithURL:[NSURL URLWithString:toAccount.logo]];
                self.transferToCell.mainTitleLabel.text = ((Beneficiary *) toAccount).alias;
                self.transferToCell.leftDescriptionLabel.text = ((Beneficiary *) toAccount).iban;

                    self.transferToCell.rightDescriptionLabel.text = ((Beneficiary *) toAccount).currency;

            } else {
                if (self.transferTemplate.destinationAccount.logo) {
                    [self.transferToCell.iconImageView sd_setImageWithURL:[NSURL URLWithString:self.transferTemplate.destinationAccount.logo]];
                } else {
                    [self.transferToCell.iconImageView sd_setImageWithURL:[NSURL URLWithString:kBafLogoUrlSmall]];
                }
                self.transferToCell.mainTitleLabel.text = ((Account *) toAccount).alias;
                self.transferToCell.leftDescriptionLabel.text = ((Account *) toAccount).iban;

                    self.transferToCell.rightDescriptionLabel.text = ((Account *) toAccount).currency;

            }

            if (!self.currencyTextField.text || self.currencyTextField.text.length == 0 || (self.selectedCurrencyIndex == 1 && self.selectedCurrency != ((Account *) self.toAccount).currency)) {
                self.currencyTextField.text = ((Account *) self.toAccount).currency;

                self.selectedCurrency = ((Account *) self.toAccount).currency;
                self.selectedCurrencyIndex = 1;
            }
        }
            break;
        case MY_CONVERT: {
            id ta = toAccount;
            if ([ta respondsToSelector:@selector(logo)]) {
                [self.transferToCell.iconImageView sd_setImageWithURL:[NSURL URLWithString:[ta logo]]];
            } else {
                self.transferToCell.iconImageView.image = [UIImage imageNamed:@"icon-baf-logo"];
            }

            self.transferToCell.mainTitleLabel.text = ((Account *) toAccount).alias;
            self.transferToCell.leftDescriptionLabel.text = ((Account *) toAccount).iban;

            NSString *balanceWithCurrency = ((Account *) toAccount).getBalanceWithCurrency;
            if ([self.toCurrency isEqualToString:@"KZT"]) {
                balanceWithCurrency = [NSString stringWithFormat:@"%@ %@", [toAccount.balanceKzt decimalFormatString], @"KZT"];
            } else if ([self.toCurrency isEqualToString:@"USD"]) {
                balanceWithCurrency = [NSString stringWithFormat:@"%@ %@", [toAccount.balanceUsd decimalFormatString], @"USD"];
            } else if ([self.toCurrency isEqualToString:@"EUR"]) {
                balanceWithCurrency = [NSString stringWithFormat:@"%@ %@", [toAccount.balanceEur decimalFormatString], @"EUR"];
            } else if ([self.toCurrency isEqualToString:@"RUB"]) {
                balanceWithCurrency = [NSString stringWithFormat:@"%@ %@", [toAccount.balanceRub decimalFormatString], @"RUB"];
            } else if ([self.toCurrency isEqualToString:@"GBP"]) {
                balanceWithCurrency = [NSString stringWithFormat:@"%@ %@", [toAccount.balanceGbp decimalFormatString], @"GBP"];
            }

            self.transferToCell.rightDescriptionLabel.text = balanceWithCurrency;

            if (!self.currencyTextField.text || self.currencyTextField.text.length == 0 || (self.selectedCurrencyIndex == 1 && self.selectedCurrency != self.toCurrency)) {
                self.currencyTextField.text = self.toCurrency;

                self.selectedCurrency = self.toCurrency;
                self.selectedCurrencyIndex = 1;
            }
        }
            break;

        default:
            break;
    }
}

#pragma mark - Methods

- (NSMutableArray *)fromOptions {
    NSMutableArray *fromOptions = [NSMutableArray new];

    switch (self.transfersType) {
        case INTERBANK_TRANSFERS: {

            if ((self.operationAccounts.sourceAccounts.cards && self.operationAccounts.sourceAccounts.cards.count > 0) ||
                    (self.operationAccounts.sourceAccounts.currents && self.operationAccounts.sourceAccounts.currents.count > 0) ||
                    (self.operationAccounts.sourceAccounts.deposits && self.operationAccounts.sourceAccounts.deposits.count > 0)) {

                // Проверка Для карт
                for (Card *card in self.operationAccounts.sourceAccounts.cards) {

                    if (self.transfersType == INTERBANK_TRANSFERS) {

                        if ([card.currency isEqualToString:@"KZT"] && !card.isCredit) {
                            [fromOptions addObject:@{
                                    @"id" : card,
                                    @"type" : card.type,
                                    @"title" : [NSString stringWithFormat:@"%@ (%@)", card.number, [card balanceWithCurrency]],
                                    @"icon" : [NSURL URLWithString:card.logo]
                            }];
                        }

                    } else {
                                [fromOptions addObject:@{
                                        @"id" : card,
                                        @"type" : card.type,
                                        @"title" : [NSString stringWithFormat:@"%@ (%@)", card.number, [card balanceWithCurrency]],
                                        @"icon" : [NSURL URLWithString:card.logo]
                                }];

                    }
                }

                if (self.cardToRefill && fromOptions.count > 0) {
                    NSMutableArray *newFromOptionsArray = [[NSMutableArray alloc] init];
                    for (NSDictionary *dict in fromOptions) {
                        Card *card = dict[@"id"];
                        if (![card.number isEqualToString:self.cardToRefill.number]) {
                            [newFromOptionsArray addObject:dict];
                        }
                    }
                    fromOptions = newFromOptionsArray;
                }


                // Проверка Для текущих счетов
                for (Current *currentAccount in self.operationAccounts.sourceAccounts.currents) {
                    if (self.transfersType == INTERBANK_TRANSFERS) {
                        if ([currentAccount.currency isEqualToString:@"KZT"]) {
                            [fromOptions addObject:@{
                                    @"id": currentAccount,
                                    @"type": currentAccount.type,
                                    @"title": [NSString stringWithFormat:@"%@ (%@)", currentAccount.alias, [currentAccount balanceWithCurrency]],
                                    @"icon": kBafLogoUrlSmall
                            }];
                        }
                    } else {
                        [fromOptions addObject:@{
                                @"id": currentAccount,
                                @"type": currentAccount.type,
                                @"title": [NSString stringWithFormat:@"%@ (%@)", currentAccount.alias, [currentAccount balanceWithCurrency]],
                                @"icon": kBafLogoUrlSmall
                        }];
                    }
                }
            } else {
                if (self.transferTemplate) {
                    return fromOptions;
                }

                // Set "No content" message
                self.transferFromCell.mainTitleLabel.text = kTextStringNoAccountsForTransfer;

                NSString *message1 = kTextStringYouHaveNoAccountForTransfer;
                NSString *message2 = @"";
                if (![User sharedInstance].isClient) {
                    message2 = kTextStringGoToCardAndAccountsForAdd;
                }
                [UIHelper showAlertWithFioTreatmentAndMessage:[NSString stringWithFormat:@"%@%@", message1, message2]];
            }
        }
            break;

        case MY_CONVERT: {
            Account *card = (Account *) self.currencyConvertAccount;

            // KZT
            if (card.balanceKzt && [card.balanceKzt floatValue] > 0) {
                id cardImage;
                id fa = self.currencyConvertAccount;
                if ([fa respondsToSelector:@selector(logo)]) {
                    cardImage = [NSURL URLWithString:[fa logo]];
                } else {
                    cardImage = [UIImage imageNamed:@"icon-baf-logo"];
                }

                [fromOptions addObject:@{@"id" : @"KZT", @"type" : card.type, @"title" : [NSString stringWithFormat:@"%@ KZT", [card.balanceKzt decimalFormatString]], @"icon" : cardImage}];
            }

            // USD
            if (card.balanceUsd && [card.balanceUsd floatValue] > 0) {
                id cardImage;
                id fa = self.currencyConvertAccount;
                if ([fa respondsToSelector:@selector(logo)]) {
                    cardImage = [NSURL URLWithString:[fa logo]];
                } else {
                    cardImage = [UIImage imageNamed:@"icon-baf-logo"];
                }

                [fromOptions addObject:@{@"id" : @"USD", @"type" : card.type, @"title" : [NSString stringWithFormat:@"%@ USD", [card.balanceUsd decimalFormatString]], @"icon" : cardImage}];
            }

            // EUR
            if (card.balanceEur && [card.balanceEur floatValue] > 0) {
                id cardImage;
                id fa = self.currencyConvertAccount;
                if ([fa respondsToSelector:@selector(logo)]) {
                    cardImage = [NSURL URLWithString:[fa logo]];
                } else {
                    cardImage = [UIImage imageNamed:@"icon-baf-logo"];
                }

                [fromOptions addObject:@{@"id" : @"EUR", @"type" : card.type, @"title" : [NSString stringWithFormat:@"%@ EUR", [card.balanceEur decimalFormatString]], @"icon" : cardImage}];
            }

            // RUB
            if (card.balanceRub && [card.balanceRub floatValue] > 0) {
                id cardImage;
                id fa = self.currencyConvertAccount;
                if ([fa respondsToSelector:@selector(logo)]) {
                    cardImage = [NSURL URLWithString:[fa logo]];
                } else {
                    cardImage = [UIImage imageNamed:@"icon-baf-logo"];
                }

                [fromOptions addObject:@{@"id" : @"RUB", @"type" : card.type, @"title" : [NSString stringWithFormat:@"%@ RUB", [card.balanceRub decimalFormatString]], @"icon" : cardImage}];
            }

            // GBP
            if (card.balanceGbp && [card.balanceGbp floatValue] > 0) {
                id cardImage;
                id fa = self.currencyConvertAccount;
                if ([fa respondsToSelector:@selector(logo)]) {
                    cardImage = [NSURL URLWithString:[fa logo]];
                } else {
                    cardImage = [UIImage imageNamed:@"icon-baf-logo"];
                }

                [fromOptions addObject:@{@"id" : @"GBP", @"type" : card.type, @"title" : [NSString stringWithFormat:@"%@ GBP", [card.balanceGbp decimalFormatString]], @"icon" : cardImage}];
            }
        }
             break;
        case MY_TRANSFERS:
        {
            
        }
          break;

        default:
            break;
    }

    return fromOptions;
}

- (NSMutableArray *)toOptions {
    NSMutableArray *toOptions = [NSMutableArray new];

    if (self.cardToRefill) {
        [toOptions addObject:@{
                @"id" : self.cardToRefill,
                @"type" : self.cardToRefill.type,
                @"title" : [NSString stringWithFormat:@"%@ (%@)", self.cardToRefill.alias, [self.cardToRefill balanceWithCurrency]],
                @"icon" : self.cardToRefill.logo
        }];
        return toOptions;
    }

    if (self.depositToRefill) {
        [toOptions addObject:@{@"id" : self.depositToRefill, @"type" : self.depositToRefill.type, @"title" : [NSString stringWithFormat:@"%@ (%@)", self.depositToRefill.alias, [self.depositToRefill balanceWithCurrency]], @"icon" : kBafLogoUrlSmall}];
        return toOptions;
    }

    switch (self.transfersType) {
        case INTERBANK_TRANSFERS: {
            if (self.operationAccounts.targetAccounts.beneficiars && self.operationAccounts.targetAccounts.beneficiars.count > 0) {
                // Beneficiars
                for (Beneficiary *benficiary in self.operationAccounts.targetAccounts.beneficiars) {
                    [toOptions addObject:@{@"id" : benficiary, @"type" : benficiary.type, @"title" : [NSString stringWithFormat:@"%@ (%@)", benficiary.alias, [benficiary currency]], @"icon" : [NSURL URLWithString:benficiary.logo]}];
                }
            } else {
                if (self.transferTemplate) {
                    return toOptions;
                }

                // Set "No content" message
                self.transferToCell.mainTitleLabel.text = kTextStringNoAccountsForTransfer;

                [UIHelper showAlertWithMessage:kTextStringYouHaveNoAccountsForTransfer];
            }
        }
            break;
        case MY_CONVERT: {
            Account *card = (Account *) self.currencyConvertAccount;

            // KZT
            if (card.balanceKzt) {
                id cardImage;
                id fa = self.currencyConvertAccount;
                if ([fa respondsToSelector:@selector(logo)]) {
                    cardImage = [NSURL URLWithString:[fa logo]];
                } else {
                    cardImage = [UIImage imageNamed:@"icon-baf-logo"];
                }

                [toOptions addObject:@{@"id" : @"KZT", @"type" : card.type, @"title" : [NSString stringWithFormat:@"%@ KZT", [card.balanceKzt decimalFormatString]], @"icon" : cardImage}];
            }

            // USD
            if (card.balanceUsd) {
                id cardImage;
                id fa = self.currencyConvertAccount;
                if ([fa respondsToSelector:@selector(logo)]) {
                    cardImage = [NSURL URLWithString:[fa logo]];
                } else {
                    cardImage = [UIImage imageNamed:@"icon-baf-logo"];
                }

                [toOptions addObject:@{@"id" : @"USD", @"type" : card.type, @"title" : [NSString stringWithFormat:@"%@ USD", [card.balanceUsd decimalFormatString]], @"icon" : cardImage}];
            }

            // EUR
            if (card.balanceEur) {
                id cardImage;
                id fa = self.currencyConvertAccount;
                if ([fa respondsToSelector:@selector(logo)]) {
                    cardImage = [NSURL URLWithString:[fa logo]];
                } else {
                    cardImage = [UIImage imageNamed:@"icon-baf-logo"];
                }

                [toOptions addObject:@{@"id" : @"EUR", @"type" : card.type, @"title" : [NSString stringWithFormat:@"%@ EUR", [card.balanceEur decimalFormatString]], @"icon" : cardImage}];
            }

            // RUB
            if (card.balanceRub) {
                id cardImage;
                id fa = self.currencyConvertAccount;
                if ([fa respondsToSelector:@selector(logo)]) {
                    cardImage = [NSURL URLWithString:[fa logo]];
                } else {
                    cardImage = [UIImage imageNamed:@"icon-baf-logo"];
                }

                [toOptions addObject:@{@"id" : @"RUB", @"type" : card.type, @"title" : [NSString stringWithFormat:@"%@ RUB", [card.balanceRub decimalFormatString]], @"icon" : cardImage}];
            }

            // GBP
            if (card.balanceGbp) {
                id cardImage;
                id fa = self.currencyConvertAccount;
                if ([fa respondsToSelector:@selector(logo)]) {
                    cardImage = [NSURL URLWithString:[fa logo]];
                } else {
                    cardImage = [UIImage imageNamed:@"icon-baf-logo"];
                }

                [toOptions addObject:@{@"id" : @"GBP", @"type" : card.type, @"title" : [NSString stringWithFormat:@"%@ GBP", [card.balanceGbp decimalFormatString]], @"icon" : cardImage}];
            }
        }
            break;

        default:
            break;
    }

    [self fitlerToOptions:toOptions];

    return toOptions;
}

- (void)fitlerToOptions:(NSMutableArray *)toOptions {
    switch (self.transfersType) {
        case INTERBANK_TRANSFERS: {
            // delete beneficiars whos is not interbank
            NSPredicate *filter = [NSPredicate predicateWithFormat:@"id.isInterbank == YES"];
            [toOptions filterUsingPredicate:filter];
        }
            break;
        case MY_CONVERT: {
            // delete option with same currency
            if (self.fromCurrency) {
                NSPredicate *filter = [NSPredicate predicateWithFormat:@"id != %@", self.fromCurrency];
                [toOptions filterUsingPredicate:filter];
            }
        }
            break;

        default:
            break;
    }
}

- (void)showRightSpinner {
    if (!self.rightNavBarSpinner) {
        self.rightNavBarSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightNavBarSpinner];
    [self.rightNavBarSpinner startAnimating];
}

- (void)hideRightSpinner {
    [self.rightNavBarSpinner stopAnimating];
    if (self.transferTemplate) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteTransferTemplate:)];
    }
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // hide cells for some condition
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            if (self.transfersType == MY_CONVERT) {
                if (!self.currencyRates || !self.currencyRates.rate)
                {
                    return 0;
                }
            } else {
                if (!self.currencyRates || !self.currencyRates.rate ||
                        [self.currencyRates.fromCurrency isEqualToString:self.currencyRates.toCurrency])
                {
                    return 0;
                }
            }
        }
    } else if (indexPath.section == 2) {
        if (indexPath.row == 1) {
            if (self.transfersType == INTERBANK_TRANSFERS) {
                return 50;
            }
            return 0;
        } else if (indexPath.row == 2) {
            if (self.transfersType == INTERBANK_TRANSFERS) {
                return 50;
            }
            return 0;
        } else if (indexPath.row == 4) {
            if (!self.saveAsTemplateSwitch.isOn && !self.transferTemplate) {
                return 0;
            }
            return 50;
        }
    } else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            return 50;
        } else if (indexPath.row == 1) {
            if (!self.destinationAmount || !self.comission || !self.comissionCurrency) {
                return 0;
            }
            return 65;
        }
    } else if (indexPath.section == 4) {
        if (self.transfersType != INTERBANK_TRANSFERS) {
            return 0;
        }
    } else if (indexPath.section == 5) {
        if (indexPath.row == 0 && (!self.warningMessages || self.warningMessages.count == 0)) {
            return 0;
        } else if (indexPath.row == 1 && (!self.warningMessages || self.warningMessages.count < 2)) {
            return 0;
        } else if (indexPath.row == 2 && (!self.warningMessages || self.warningMessages.count < 3)) {
            return 0;
        }else if (indexPath.row == 3 && (!self.warningMessages || self.warningMessages.count < 4)) {
            return 0;
        }else if (indexPath.row == 4 && (!self.warningMessages || self.warningMessages.count < 5)) {
            return 0;
        }else if (indexPath.row == 5 && (!self.warningMessages || self.warningMessages.count < 6)) {
            return 0;
        }
    }

    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    cell.textLabel.backgroundColor = [UIColor clearColor];

    // customize cell
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }

        if (self.currencyRates && self.currencyRates.rate) {
            NSString *currencyRate = @"";

            BOOL showFrom = self.currencyRates.fromCurrency && ![self.currencyRates.fromCurrency.uppercaseString isEqualToString:@"KZT"];
            BOOL showTo = self.currencyRates.toCurrency && ![self.currencyRates.toCurrency.uppercaseString isEqualToString:@"KZT"];
            if (showFrom) {
                currencyRate = [currencyRate stringByAppendingFormat:@"1 %@ = %@ KZT%@", self.currencyRates.fromCurrency, [self.currencyRates.fromSell decimalFormatString], (showTo ? @"\n" : @"")];

            }
            if (showTo) {
                currencyRate = [currencyRate stringByAppendingFormat:@"1 %@ = %@ KZT%@", self.currencyRates.toCurrency, [self.currencyRates.toBuy decimalFormatString], (showFrom ? @"\n" : @"")];
            }

            if (showFrom && showTo) {
                if ([self.currencyRates.fromSell doubleValue] >= [self.currencyRates.toBuy doubleValue]) {
                    currencyRate = [currencyRate stringByAppendingFormat:@"1 %@ = %@ %@", self.currencyRates.fromCurrency, [self.currencyRates.rate decimalFormatString], self.currencyRates.toCurrency];
                } else {
                    currencyRate = [currencyRate stringByAppendingFormat:@"1 %@ = %@ %@", self.currencyRates.toCurrency, [self.currencyRates.rate decimalFormatString], self.currencyRates.fromCurrency];
                }
            }

            NSString *updatedText = @"";
            if (self.currencyRates.updated) {
                NSDate *updated = [[NSDateFormatter baf2DateFormatter] dateFromString:self.currencyRates.updated];
                NSDateFormatter *df = [NSDateFormatter new];
                [df setDateFormat:@"dd.MM.yyyy"];
                updatedText = [df stringFromDate:updated];
            }

            self.currencyDisclaimerCell.leftDescriptionTextLabel.text = currencyRate;
            self.currencyDisclaimerCell.rightDescriptionTextLabel.text = [NSString stringWithFormat:@"(%@ %@)", kTextStringRateFor, updatedText];
            cell.backgroundColor = [[UIColor appDisclaimerYellowColor] colorWithAlphaComponent:0.5];
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            if (self.transferTemplate) {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
        }
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            if (self.transferTemplate) {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
        } else if (indexPath.row == 1) {

        } else if (indexPath.row == 2) {

        } else if (indexPath.row == 3) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryView = self.saveAsTemplateSwitch;
        } else if (indexPath.row == 4) {

        }
    } else if (indexPath.section == 3) {
        if (indexPath.row == 0) {

        } else if (indexPath.row == 1) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            if (self.destinationAmount && self.comission && self.comissionCurrency) {

                NSString *fromCurrency = self.transfersType == MY_CONVERT ? self.fromCurrency : ((Account *) self.fromAccount).currency;
                NSString *toCurrency = self.transfersType == MY_CONVERT ? self.toCurrency : ((Account *) self.toAccount).currency;

                NSMutableAttributedString *currencyText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@ %@", kTextStringWillEnroll, [self.destinationAmount decimalFormatString], toCurrency]];
                [currencyText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:12] range:NSMakeRange(0, 16)];
                [currencyText addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(16, currencyText.length - 16)];
                if (self.transfersType == INTERBANK_TRANSFERS || ([self.fromAccount isKindOfClass:[Card class]] && ((Card *) self.fromAccount).isCredit) || ([self.toAccount isKindOfClass:[Card class]] && ((Card *) self.toAccount).isCredit)) {
                    NSMutableAttributedString *commissionText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@ %@ %@", kTextStringComission, self.comission, self.comissionCurrency]];
                    [commissionText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:12] range:NSMakeRange(0, 9)];
                    [commissionText addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(9, commissionText.length - 9)];
                    [currencyText appendAttributedString:commissionText];
                }

                NSMutableAttributedString *requestorAmountText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@ %@\n", kTextStringWillWrittenOff, [self.requestorAmount decimalFormatString], fromCurrency]];
                [requestorAmountText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:12] range:NSMakeRange(0, 14)];
                [requestorAmountText addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(14, requestorAmountText.length - 14)];
                [requestorAmountText appendAttributedString:currencyText];

                self.currencyCalculatorDisclaimerCell.mainTextLabel.attributedText = requestorAmountText;
                cell.backgroundColor = [[UIColor appDisclaimerYellowColor] colorWithAlphaComponent:0.5];
            }
        }
    } else if (indexPath.section == 4) {
        if(self.allKnpList.count > 0){
        __weak TransfersFormViewController *wSelf = self;
        KnpCell *knpCell = (KnpCell *) cell;
        knpCell.knps = self.knpList;
        knpCell.allKnpList = self.allKnpList;
        knpCell.parentVC = self;
        knpCell.sourceAccount = self.fromAccount;
        knpCell.targetAccount = self.toAccount;
        knpCell.onKnpSelect = ^(NSString *knpCode, NSString *knpName, NSString *clientDesc) {
            if (knpCode && knpName) {
                wSelf.selectedKnpCode = knpCode;
                wSelf.selectedKnpName = knpName;
                wSelf.selectedClientDesc = clientDesc;
                [wSelf checkTransfer];
            }
        };
        }
    } else if (indexPath.section == 5) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        TransfersFormDisclaimerCell *disclaimerCell = (TransfersFormDisclaimerCell *) cell;

        if (self.warningMessages && self.warningMessages.count > indexPath.row) {
            [disclaimerCell setHTML:self.warningMessages[(NSUInteger) indexPath.row]];
        }
        cell.backgroundColor = [[UIColor appDisclaimerYellowColor] colorWithAlphaComponent:0.5];
    }

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak TransfersFormViewController *wSelf = self;

    [tableView deselectRowAtIndexPath:indexPath animated:false];

    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            if (self.transferTemplate) {
                return;
            }

            self.fromActionSheet = [[ActionSheetListPopupViewController alloc] init];
            self.fromActionSheet.emptyDataText = kTextStringNoAcocuntForWrittenOff;

            NSMutableDictionary *fromData = [NSMutableDictionary new];
            NSArray *fromOptions = [self fromOptions];
            for (NSDictionary *option in fromOptions) {
                id account = option[@"id"];

                if ([account isKindOfClass:[Card class]]) {
                    if (fromData[kTextStringCards]) {
                        NSMutableArray *options = [fromData[kTextStringCards] mutableCopy];
                        [options addObject:option];
                        fromData[kTextStringCards] = options;
                    } else {
                        fromData[kTextStringCards] = [NSMutableArray arrayWithObject:option];
                    }
                } else if ([account isKindOfClass:[Current class]]) {
                    if (fromData[kTextStringAcounts]) {
                        NSMutableArray *options = [fromData[kTextStringAcounts] mutableCopy];
                        [options addObject:option];
                        fromData[kTextStringAcounts] = options;
                    } else {
                        fromData[kTextStringAcounts] = [NSMutableArray arrayWithObject:option];
                    }
                } else if ([account isKindOfClass:[Deposit class]]) {
                    if (fromData[kTextStringDeposits]) {
                        NSMutableArray *options = [fromData[kTextStringDeposits] mutableCopy];
                        [options addObject:option];
                        fromData[kTextStringDeposits] = options;
                    } else {
                        fromData[kTextStringDeposits] = [NSMutableArray arrayWithObject:option];
                    }
                } else {
                    if (fromData[@""]) {
                        NSMutableArray *options = [fromData[@""] mutableCopy];
                        [options addObject:option];
                        fromData[@""] = options;
                    } else {
                        fromData[@""] = [NSMutableArray arrayWithObject:option];
                    }
                }
            }

            self.fromActionSheet.numberOfSections = ^NSInteger {
                return fromData.allKeys.count;
            };
            self.fromActionSheet.titleForSection = ^NSString *(NSInteger section) {
                if ([fromData.allKeys containsObject:kTextStringCards] && section == 0) {
                    return kTextStringCards;
                } else if (
                        ([fromData.allKeys containsObject:kTextStringCards] && [fromData.allKeys containsObject:kTextStringAcounts] && section == 1) ||
                                ([fromData.allKeys containsObject:kTextStringAcounts] && ![fromData.allKeys containsObject:kTextStringCards] && section == 0)) {
                    return kTextStringAcounts;
                } else if (
                        ([fromData.allKeys containsObject:kTextStringCards] && [fromData.allKeys containsObject:kTextStringAcounts] && [fromData.allKeys containsObject:kTextStringDeposits] && section == 2) ||
                                (![fromData.allKeys containsObject:kTextStringCards] && [fromData.allKeys containsObject:kTextStringAcounts] && [fromData.allKeys containsObject:kTextStringDeposits] && section == 1) ||
                                ([fromData.allKeys containsObject:kTextStringCards] && ![fromData.allKeys containsObject:kTextStringAcounts] && [fromData.allKeys containsObject:kTextStringDeposits] && section == 1) ||
                                (![fromData.allKeys containsObject:kTextStringCards] && ![fromData.allKeys containsObject:kTextStringAcounts] && [fromData.allKeys containsObject:kTextStringDeposits] && section == 0)) {
                    return kTextStringDeposits;
                } else {
                    return nil;
                }
            };
            self.fromActionSheet.numberOfRowsInSection = ^NSInteger(NSInteger section) {
                NSString *key;
                if ([fromData.allKeys containsObject:kTextStringCards] && section == 0) {
                    key = kTextStringCards;
                } else if (
                        ([fromData.allKeys containsObject:kTextStringCards] && [fromData.allKeys containsObject:kTextStringAcounts] && section == 1) ||
                                ([fromData.allKeys containsObject:kTextStringAcounts] && ![fromData.allKeys containsObject:kTextStringCards] && section == 0)) {
                    key = kTextStringAcounts;
                } else if (
                        ([fromData.allKeys containsObject:kTextStringCards] && [fromData.allKeys containsObject:kTextStringAcounts] && [fromData.allKeys containsObject:kTextStringDeposits] && section == 2) ||
                                (![fromData.allKeys containsObject:kTextStringCards] && [fromData.allKeys containsObject:kTextStringAcounts] && [fromData.allKeys containsObject:kTextStringDeposits] && section == 1) ||
                                ([fromData.allKeys containsObject:kTextStringCards] && ![fromData.allKeys containsObject:kTextStringAcounts] && [fromData.allKeys containsObject:kTextStringDeposits] && section == 1) ||
                                (![fromData.allKeys containsObject:kTextStringCards] && ![fromData.allKeys containsObject:kTextStringAcounts] && [fromData.allKeys containsObject:kTextStringDeposits] && section == 0)) {
                    key = kTextStringDeposits;
                } else {
                    key = @"";
                }
                NSMutableArray *options = fromData[key];

                return options.count;
            };
            self.fromActionSheet.rowForIndexPath = ^ActionSheetListRowObject *(NSIndexPath *indexPath) {
                NSString *key;
                if ([fromData.allKeys containsObject:kTextStringCards] && indexPath.section == 0) {
                    key = kTextStringCards;
                } else if (
                        ([fromData.allKeys containsObject:kTextStringCards] && [fromData.allKeys containsObject:kTextStringAcounts] && indexPath.section == 1) ||
                                ([fromData.allKeys containsObject:kTextStringAcounts] && ![fromData.allKeys containsObject:kTextStringCards] && indexPath.section == 0)) {
                    key = kTextStringAcounts;
                } else if (
                        ([fromData.allKeys containsObject:kTextStringCards] && [fromData.allKeys containsObject:kTextStringAcounts] && [fromData.allKeys containsObject:kTextStringDeposits] && indexPath.section == 2) ||
                                (![fromData.allKeys containsObject:kTextStringCards] && [fromData.allKeys containsObject:kTextStringAcounts] && [fromData.allKeys containsObject:kTextStringDeposits] && indexPath.section == 1) ||
                                ([fromData.allKeys containsObject:kTextStringCards] && ![fromData.allKeys containsObject:kTextStringAcounts] && [fromData.allKeys containsObject:kTextStringDeposits] && indexPath.section == 1) ||
                                (![fromData.allKeys containsObject:kTextStringCards] && ![fromData.allKeys containsObject:kTextStringAcounts] && [fromData.allKeys containsObject:kTextStringDeposits] && indexPath.section == 0)) {
                    key = kTextStringDeposits;
                } else {
                    key = @"";
                }
                NSMutableArray *options = fromData[key];
                NSDictionary *option = options[indexPath.row];

                if (wSelf.transfersType == MY_CONVERT) {
                    NSString *currency = option[@"id"];
                    if ([currency isEqualToString:wSelf.fromCurrency]) {
                        wSelf.fromActionSheet.selectedIndexPath = indexPath;
                    }
                } else {
                    NSNumber *accountId = ((Account *) option[@"id"]).accountId;
                    if ([accountId integerValue] == [((Account *) wSelf.fromAccount).accountId integerValue]) {
                        wSelf.fromActionSheet.selectedIndexPath = indexPath;
                    }
                }

                ActionSheetListRowObject *rowObject = [[ActionSheetListRowObject alloc] init];

                Account *account;
                if ([option[@"id"] isKindOfClass:[Account class]]) {
                    account = option[@"id"];
                }

                NSString *aliasAndBalance = [NSString stringWithFormat:@"%@ (%@)", account.alias, account.getBalanceWithCurrency];
                NSString *listRowTitle = account.alias.length > 0 ? aliasAndBalance : option[@"title"];
                rowObject.title = listRowTitle;
                if ([option[@"icon"] isKindOfClass:[NSURL class]]) {
                    rowObject.imageUrl = ((NSURL *) option[@"icon"]).absoluteString;
                } else if ([option[@"icon"] containsString:@"http"]) {
                    rowObject.imageUrl = option[@"icon"];
                } else {
                    rowObject.imageName = option[@"icon"];
                }
                rowObject.keepObject = option;

                return rowObject;
            };
            self.fromActionSheet.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
                NSDictionary *option = selectedObject;

                if (wSelf.transfersType == MY_CONVERT) {
                    wSelf.fromCurrency = option[@"id"];
                    wSelf.fromAccount = wSelf.currencyConvertAccount;
                } else {
                    wSelf.fromAccount = ((Account *) option[@"id"]);
                }

                [wSelf checkTransfer];
            };
            self.fromActionSheet.title = kTextStringWriteOffFrom;
            self.fromActionSheet.selectedIndexPath = nil;
            [self.fromActionSheet.tableView reloadData];

            STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:self.fromActionSheet];
            popupController.dismissOnBackgroundTap = true;
            popupController.style = STPopupStyleBottomSheet;
            popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
            [popupController presentInViewController:self];
        }
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            if (self.transferTemplate || !self.fromAccount) {
                return;
            }

            __weak TransfersFormViewController *wSelf = self;

            self.toActionSheet = [[ActionSheetListPopupViewController alloc] init];
            self.toActionSheet.emptyDataText = kTextStringYouHaveNoAccountsForTransfer;

            NSMutableDictionary *toData = [NSMutableDictionary new];
            NSArray *toOptions = [self toOptions];
            for (NSDictionary *option in toOptions) {
                id account = option[@"id"];

                if ([account isKindOfClass:[Card class]]) {
                    if (toData[kTextStringCards]) {
                        NSMutableArray *options = [toData[kTextStringCards] mutableCopy];
                        [options addObject:option];
                        toData[kTextStringCards] = options;
                    } else {
                        toData[kTextStringCards] = [NSMutableArray arrayWithObject:option];
                    }
                } else if ([account isKindOfClass:[Current class]]) {
                    if (toData[kTextStringAcounts]) {
                        NSMutableArray *options = [toData[kTextStringAcounts] mutableCopy];
                        [options addObject:option];
                        toData[kTextStringAcounts] = options;
                    } else {
                        toData[kTextStringAcounts] = [NSMutableArray arrayWithObject:option];
                    }
                } else if ([account isKindOfClass:[Deposit class]]) {
                    if (toData[kTextStringDeposits]) {
                        NSMutableArray *options = [toData[kTextStringDeposits] mutableCopy];
                        [options addObject:option];
                        toData[kTextStringDeposits] = options;
                    } else {
                        toData[kTextStringDeposits] = [NSMutableArray arrayWithObject:option];
                    }
                } else {
                    if (toData[@""]) {
                        NSMutableArray *options = [toData[@""] mutableCopy];
                        [options addObject:option];
                        toData[@""] = options;
                    } else {
                        toData[@""] = [NSMutableArray arrayWithObject:option];
                    }
                }
            }

            self.toActionSheet.numberOfSections = ^NSInteger {
                if (wSelf.transfersType == INTERBANK_TRANSFERS) {
                    return 2;
                }

                return toData.allKeys.count;
            };
            self.toActionSheet.titleForSection = ^NSString *(NSInteger section) {
                if ([toData.allKeys containsObject:kTextStringCards] && section == 0) {
                    return kTextStringCards;
                } else if (
                        ([toData.allKeys containsObject:kTextStringCards] && [toData.allKeys containsObject:kTextStringAcounts] && section == 1) ||
                                ([toData.allKeys containsObject:kTextStringAcounts] && ![toData.allKeys containsObject:kTextStringCards] && section == 0)) {
                    return kTextStringAcounts;
                } else if (
                        ([toData.allKeys containsObject:kTextStringCards] && [toData.allKeys containsObject:kTextStringAcounts] && [toData.allKeys containsObject:kTextStringDeposits] && section == 2) ||
                                (![toData.allKeys containsObject:kTextStringCards] && [toData.allKeys containsObject:kTextStringAcounts] && [toData.allKeys containsObject:kTextStringDeposits] && section == 1) ||
                                ([toData.allKeys containsObject:kTextStringCards] && ![toData.allKeys containsObject:kTextStringAcounts] && [toData.allKeys containsObject:kTextStringDeposits] && section == 1) ||
                                (![toData.allKeys containsObject:kTextStringCards] && ![toData.allKeys containsObject:kTextStringAcounts] && [toData.allKeys containsObject:kTextStringDeposits] && section == 0)) {
                    return kTextStringDeposits;
                } else {
                    return nil;
                }
            };
            self.toActionSheet.numberOfRowsInSection = ^NSInteger(NSInteger section) {
                if (wSelf.transfersType == INTERBANK_TRANSFERS && section == 1) {
                    return 0;
                }

                NSString *key;
                if ([toData.allKeys containsObject:kTextStringCards] && section == 0) {
                    key = kTextStringCards;
                } else if (
                        ([toData.allKeys containsObject:kTextStringCards] && [toData.allKeys containsObject:kTextStringAcounts] && section == 1) ||
                                ([toData.allKeys containsObject:kTextStringAcounts] && ![toData.allKeys containsObject:kTextStringCards] && section == 0)) {
                    key = kTextStringAcounts;
                } else if (
                        ([toData.allKeys containsObject:kTextStringCards] && [toData.allKeys containsObject:kTextStringAcounts] && [toData.allKeys containsObject:kTextStringDeposits] && section == 2) ||
                                (![toData.allKeys containsObject:kTextStringCards] && [toData.allKeys containsObject:kTextStringAcounts] && [toData.allKeys containsObject:kTextStringDeposits] && section == 1) ||
                                ([toData.allKeys containsObject:kTextStringCards] && ![toData.allKeys containsObject:kTextStringAcounts] && [toData.allKeys containsObject:kTextStringDeposits] && section == 1) ||
                                (![toData.allKeys containsObject:kTextStringCards] && ![toData.allKeys containsObject:kTextStringAcounts] && [toData.allKeys containsObject:kTextStringDeposits] && section == 0)) {
                    key = kTextStringDeposits;
                } else {
                    key = @"";
                }
                NSMutableArray *options = toData[key];

                return options.count;
            };
            self.toActionSheet.rowForIndexPath = ^ActionSheetListRowObject *(NSIndexPath *indexPath) {

                if (wSelf.transfersType == INTERBANK_TRANSFERS && indexPath.section == 1) {
                    ActionSheetListRowObject *rowObject = [[ActionSheetListRowObject alloc] init];
                    rowObject.title = kTextStringEditList;

                    return rowObject;
                }

                NSString *key;
                if ([toData.allKeys containsObject:kTextStringCards] && indexPath.section == 0) {
                    key = kTextStringCards;
                } else if (
                        ([toData.allKeys containsObject:kTextStringCards] && [toData.allKeys containsObject:kTextStringAcounts] && indexPath.section == 1) ||
                                ([toData.allKeys containsObject:kTextStringAcounts] && ![toData.allKeys containsObject:kTextStringCards] && indexPath.section == 0)) {
                    key = kTextStringAcounts;
                } else if (
                        ([toData.allKeys containsObject:kTextStringCards] && [toData.allKeys containsObject:kTextStringAcounts] && [toData.allKeys containsObject:kTextStringDeposits] && indexPath.section == 2) ||
                                (![toData.allKeys containsObject:kTextStringCards] && [toData.allKeys containsObject:kTextStringAcounts] && [toData.allKeys containsObject:kTextStringDeposits] && indexPath.section == 1) ||
                                ([toData.allKeys containsObject:kTextStringCards] && ![toData.allKeys containsObject:kTextStringAcounts] && [toData.allKeys containsObject:kTextStringDeposits] && indexPath.section == 1) ||
                                (![toData.allKeys containsObject:kTextStringCards] && ![toData.allKeys containsObject:kTextStringAcounts] && [toData.allKeys containsObject:kTextStringDeposits] && indexPath.section == 0)) {
                    key = kTextStringDeposits;
                } else {
                    key = @"";
                }
                NSMutableArray *options = toData[key];
                NSDictionary *option = options[(NSUInteger) indexPath.row];

                if (wSelf.transfersType == MY_CONVERT) {
                    NSString *currency = option[@"id"];
                    if ([currency isEqualToString:wSelf.toCurrency]) {
                        wSelf.toActionSheet.selectedIndexPath = indexPath;
                    }
                } else {
                    NSNumber *accountId = ((Account *) option[@"id"]).accountId;
                    if ([accountId integerValue] == [((Account *) wSelf.toAccount).accountId integerValue]) {
                        wSelf.toActionSheet.selectedIndexPath = indexPath;
                    }
                }

                ActionSheetListRowObject *rowObject = [[ActionSheetListRowObject alloc] init];

                Account *account;
                if ([option[@"id"] isKindOfClass:[Account class]]) {
                    account = option[@"id"];
                }

                NSString *aliasAndBalance = [NSString stringWithFormat:@"%@ (%@)", account.alias, account.getBalanceWithCurrency];
                NSString *listRowTitle = account.alias.length > 0 ? aliasAndBalance : option[@"title"];
                rowObject.title = listRowTitle;

                if ([option[@"icon"] isKindOfClass:[NSURL class]]) {
                    rowObject.imageUrl = ((NSURL *) option[@"icon"]).absoluteString;
                } else if ([option[@"icon"] containsString:@"http"]) {
                    rowObject.imageUrl = option[@"icon"];
                } else {
                    rowObject.imageName = option[@"icon"];
                }
                rowObject.keepObject = option;

                return rowObject;
            };
            self.toActionSheet.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
                if (wSelf.transfersType == INTERBANK_TRANSFERS && selectedIndexPath.section == 1) {
                    EditBankTransferBeneficiaryAccountsViewController *vc = [EditBankTransferBeneficiaryAccountsViewController new];
                    NSPredicate *filter = [NSPredicate predicateWithFormat:@"isInterbank == NO"];
                    NSMutableArray *benefs = [[NSMutableArray alloc] init];

                    for (NSDictionary *d in toData[@""]) {
                        [benefs addObject:d[@"id"]];
                    }

                    vc.beneficiaryAccounts = benefs;

                    [wSelf.navigationController pushViewController:vc animated:true];
                    return;
                }

                NSDictionary *option = selectedObject;

                if (wSelf.transfersType == MY_CONVERT) {
                    wSelf.toCurrency = option[@"id"];
                    wSelf.toAccount = wSelf.currencyConvertAccount;
                } else {
                    wSelf.toAccount = ((Account *) option[@"id"]);
                }

                wSelf.amountTextField.enabled = true;
                wSelf.currencyTextField.enabled = true;

                [wSelf checkTransfer];
            };
            self.toActionSheet.title = kTextStringEnrollOn;
            self.toActionSheet.selectedIndexPath = nil;
            [self.toActionSheet.tableView reloadData];

            STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:self.toActionSheet];
            popupController.dismissOnBackgroundTap = true;
            popupController.style = STPopupStyleBottomSheet;
            popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
            [popupController presentInViewController:self];
        } else if (indexPath.row == 1) {
            [self searchBeneficiaryTapped];
        } else if (indexPath.row == 2) {
            [self removeBeneficiaryTapped];
        }
    }
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSMutableSet *currencies = [NSMutableSet new];

    if (self.transfersType == MY_CONVERT) {
        if (self.fromCurrency) {
            [currencies addObject:self.fromCurrency];
        }
        if (self.toCurrency) {
            [currencies addObject:self.toCurrency];
        }
    } else {
        if (self.fromAccount) {
            [currencies addObject:((Account *) self.fromAccount).currency];
        }
        if (self.toAccount) {
            [currencies addObject:((Account *) self.toAccount).currency];
        }
    }

    return currencies.count;
}

#pragma mark - UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 44;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSMutableSet *currencies = [NSMutableSet new];

    if (self.transfersType == MY_CONVERT) {
        if (self.fromCurrency) {
            [currencies addObject:self.fromCurrency];
        }
        if (self.toCurrency) {
            [currencies addObject:self.toCurrency];
        }
    } else {
        if (self.fromAccount) {
            [currencies addObject:((Account *) self.fromAccount).currency];
        }
        if (self.toAccount) {
            [currencies addObject:((Account *) self.toAccount).currency];
        }
    }

    return currencies.allObjects[(NSUInteger) row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSMutableSet *currencies = [NSMutableSet new];

    if (self.transfersType == MY_CONVERT) {
        if (self.fromCurrency) {
            [currencies addObject:self.fromCurrency];
        }
        if (self.toCurrency) {
            [currencies addObject:self.toCurrency];
        }
    } else {
        if (self.fromAccount) {
            [currencies addObject:((Account *) self.fromAccount).currency];
        }
        if (self.toAccount) {
            [currencies addObject:((Account *) self.toAccount).currency];
        }
    }

    self.currencyTextField.text = currencies.allObjects[(NSUInteger) row];

    self.selectedCurrency = currencies.allObjects[(NSUInteger) row];
    self.selectedCurrencyIndex = row;


    [self checkTransfer];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {

    if (textField == self.amountTextField) {
        if (textField.text.length == 0) {
            self.submitButton.mainButtonStyle = MainButtonStyleDisable;
        }
    }
    [self checkTransfer];
}

#pragma mark - SearchBeneficiaryViewControllerDelegate

- (void)searchBeneficiaryViewControllerRecipientAddedWithId:(NSNumber *)beneficialAccountId {
    NSNotification *notification = [[NSNotification alloc] initWithName:kNotificationTransferRetailBeneficiarAdded object:self userInfo:nil];
    [self reloadOperationsAccount:notification];
}

#pragma mark - Config UI

- (void)configUI {
    [self setNavigationTitle:self.title];
    [self setNavigationBackButton];
    [self setBAFTableViewBackground];

    if (self.transferTemplate) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteTransferTemplate:)];
    }

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;

    // switch
    self.saveAsTemplateSwitch = [UISwitch new];
    self.saveAsTemplateSwitch.onTintColor = [UIColor flatSkyBlueColorDark];
    [self.saveAsTemplateSwitch addTarget:self action:@selector(saveAsTempate:) forControlEvents:UIControlEventValueChanged];

    // cells
    self.transferFromCell.iconImageView.image = nil;
    self.transferFromCell.mainTitleLabel.text = kTextStringChooseAccount;
    self.transferFromCell.leftDescriptionLabel.text = @"-";
    self.transferFromCell.rightDescriptionLabel.text = @"-";

    self.transferToCell.iconImageView.image = nil;
    self.transferToCell.mainTitleLabel.text = @"-";
    self.transferToCell.leftDescriptionLabel.text = @"-";
    self.transferToCell.rightDescriptionLabel.text = @"-";

    // text fields
    self.templateNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.templateNameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.templateAliasSpinner.hidden = true;
    if (self.transferTemplate) {
        __weak TransfersFormViewController *weakSelf = self;
        self.templateNameTextField.didEndEditingBlock = ^(UITextField *textField) {
            [weakSelf saveTransferAlias];
        };
    }

    self.amountTextField.enabled = false;
    self.amountTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.amountTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.amountTextField.keyboardType = UIKeyboardTypeDecimalPad;
    self.amountTextField.delegate = self;

    self.currencyTextField.enabled = false;
    self.currencyTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.currencyTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.pickerView = [UIPickerView new];
    self.pickerView.backgroundColor = [UIColor whiteColor];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    self.currencyTextField.inputView = self.pickerView;
    self.currencyTextField.delegate = self;
    if (self.transfersType == INTERBANK_TRANSFERS) {
        self.currencyTextField.enabled = false;
    }

    if (self.transferTemplate) {
        self.templateNameTextField.text = self.transferTemplate.alias;

        self.amountTextField.text = self.transferTemplate.direction ? [self.transferTemplate.requestorAmount stringValue] : [self.transferTemplate.destinationAmount stringValue];

        NSString *fromCurrency = self.transfersType == MY_CONVERT ? self.fromCurrency : ((Account *) self.fromAccount).currency;
        self.currencyTextField.text = fromCurrency;
        self.currencyTextField.enabled = false;
    }

    self.submitButton.mainButtonStyle = MainButtonStyleDisable;
    [self.submitButton setTitle:kTextStringTransfer];
    [self.submitButton addTarget:self action:@selector(initTransfer) forControlEvents:UIControlEventTouchUpInside];


    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadOperationsAccount:) name:kNotificationTransferRetailBeneficiarAdded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadOperationsAccount:) name:kNotificationTransferCorporateBeneficiarAdded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadOperationsAccount:) name:kNotificationUserUpdatedBeneficiaryAccount object:nil];
}

#pragma mark - Knp helper code

- (void)loadKnpList {
    //1 load knplist
    //2 after user enter knp number check if it ix exist in knplist
    //3 if yes message them that success and enable enter the naznachenie
    //4 if no message them that fail
    //5 after user entered naznachenie enable transfer button

    __weak TransfersFormViewController *wSelf = self;

    [CatalogApi getKnpListWithSuccess:^(NSArray *knpList) {

        NSMutableArray *groupedKnpList = [NSMutableArray new];
        [groupedKnpList addObjectsFromArray:knpList];

        NSSortDescriptor *sortKnpByCode = [[NSSortDescriptor alloc] initWithKey:@"code" ascending:true];
        [groupedKnpList sortUsingDescriptors:@[sortKnpByCode]];

        wSelf.allKnpList = [NSMutableArray arrayWithArray:groupedKnpList];

    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark -

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"Transfers dealloc");
}

@end
