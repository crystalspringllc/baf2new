//
//  OrderCallRequestViewController.h
//  BAF
//
//  Created by Almas Adilbek on 10/31/14.
//
//

typedef enum BafRequestType : NSUInteger {
    BafRequestTypeCall,
    BafRequestTypeDeposit,
    BafRequestTypeLoan,
    BafRequestTypeOpenAccount,
    BafRequestTypeRequestCard,
} BafRequestType;

#import "ScrollViewController.h"
#import "TimeIntervalPickerView.h"

@interface OrderCallRequestViewController : ScrollViewController <TimeIntervalPickerViewDelegate>

@property(nonatomic, assign) BafRequestType requestType;

- (void)submitTapped;

@end
