//
// Created by Askar Mustafin on 6/20/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NewProfileViewController : UITableViewController

+ (instancetype)createVC;

@end