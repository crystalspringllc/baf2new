//
// Created by Askar Mustafin on 4/8/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BAFTabPagerViewController.h"


@interface MainViewController : BAFTabPagerViewController <BAFTabPagerDataSource, BAFTabPagerDelegate>
@property (nonatomic) NSInteger graces;
@property (nonatomic) BOOL isPasswordExpired;
@end
