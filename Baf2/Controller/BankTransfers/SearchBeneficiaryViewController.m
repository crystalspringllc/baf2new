//
//  SearchBeneficiaryViewController.m
//  BAF
//
//  Created by Almas Adilbek on 11/27/14.
//
//

#import "SearchBeneficiaryViewController.h"
#import "TitleWithView.h"
#import "AccountsApi.h"
#import "AAForm.h"
#import "BankTransfersHelper.h"
#import "Version.h"
#import "MainButton.h"
#import "FloatingTextField.h"
#import "NSString+Validators.h"

@interface SearchBeneficiaryViewController ()
@end

@implementation SearchBeneficiaryViewController {
    FloatingTextField *iinField;
    FloatingTextField *digitsField;
    UILabel *placeholderLabel;
    UIView *digitsView;
    TitleWithView *digitsRow;
    UISegmentedControl *accountTypeControl;
}

-(id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

-(void)initVars {}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];

    // Select card
    accountTypeControl.selectedSegmentIndex = 0;
    [self setLastDigitsWithAccountType:ACCOUNT_TYPE_CARD];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //[[UIHelper appDelegate] hideTabbar:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    [[UIHelper appDelegate] showTabbar:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [iinField focus];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark -
#pragma mark Actions

- (void)searchTapped
{
    [self hideKeyboard];

    if(![iinField.value validateIinWithMessage:NSLocalizedString(@"enter_iin", nil)]) return;
    if(![digitsField.value validate]) {
        if(accountTypeControl.selectedSegmentIndex == 0) {
            [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_last_four_iin_digist", nil)];
        } else {
            [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_last_six_digist", nil)];
        }
        return;
    }

    NSString *accountType = [self accountTypeWithSelectedSegmentIndex:accountTypeControl.selectedSegmentIndex];
    AccountType at = accountTypeControl.selectedSegmentIndex == 0 ? AccountTypeCard : AccountTypeAccount;
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"search", nil) animated:true];
    [AccountsApi findBeneficiaryWithIIN:iinField.value accountType:at lastDigits:digitsField.value success:^(Beneficiary *beneficiary) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        if (beneficiary) {
            BeneficiarySearchResultViewController *v = [[BeneficiarySearchResultViewController alloc] init];
            v.delegate = self;
            v.beneficiary = beneficiary;
            v.accountType = accountType;
            v.lastDigits = [digitsField value];
            [self.navigationController pushViewController:v animated:YES];
        } else {
            [UIHelper showAlertWithMessage:NSLocalizedString(@"reeciver_account_no_found", nil)];
        }
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        if(message.length == 0) [UIHelper showAlertWithMessage:NSLocalizedString(@"reeciver_account_no_found", nil)];
    }];
}

- (void)accountTypeSelected:(UISegmentedControl *)sender
{
    NSString *optionId = [self accountTypeWithSelectedSegmentIndex:sender.selectedSegmentIndex];
    if(optionId) [self setLastDigitsWithAccountType:optionId];
}

#pragma mark -
#pragma mark BeneficiarySearchResultViewController

- (void)searchBeneficiaryViewControllerRecipientAddedWithId:(NSNumber *)beneficialAccountId {
    [self.delegate searchBeneficiaryViewControllerRecipientAddedWithId:beneficialAccountId];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -

- (NSString *)accountTypeWithSelectedSegmentIndex:(NSInteger)index {
    if(index == 0) {
        return ACCOUNT_TYPE_CARD;
    } else {
        return ACCOUNT_TYPE_CURRENT_INDIVIDUALS;
    }
}

- (void)setLastDigitsWithAccountType:(NSString *)type
{
    if([type isEqual:ACCOUNT_TYPE_CARD])
    {
        [digitsRow setTitle:NSLocalizedString(@"enter_last_four_digist_of_card", nil)];

        placeholderLabel.text = @"XXXX       XXXX       XXXX";
        [placeholderLabel sizeToFit];
        [digitsField setPlaceholder:@"____" floatingTitle:NSLocalizedString(@"four_digist", nil)];
        digitsField.maxCharacters = 4;

    } else {
        [digitsRow setTitle:NSLocalizedString(@"beneficiar_last_six_digits", nil)];

        placeholderLabel.text = @"     KZXXXXXXXXXXXX";
        [placeholderLabel sizeToFit];
        [digitsField setPlaceholder:@"______" floatingTitle:NSLocalizedString(@"six_digist", nil)];
        digitsField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
        digitsField.maxCharacters = 6;
    }

    [digitsField clear];
    placeholderLabel.centerY = digitsView.middleY;
    placeholderLabel.y += 8;
    digitsField.x = placeholderLabel.right + 17;
}

#pragma mark -

- (void)configUI {
    [self setBafBackground];
    [self setNavigationCloseButtonAtLeft:NO];
    [self setNavigationTitle:NSLocalizedString(@"add_beneficiar_dlg_title", nil)];

    self.view.backgroundColor = [UIColor whiteColor];
    self.contentScrollView.backgroundColor = [UIColor clearColor];
    
    contentScrollView.height = [ASize screenHeightWithoutStatusBarAndNavigationBar];
    AAForm *form = [[AAForm alloc] initWithScrollView:contentScrollView];

    CGFloat padding = [Version isIpad] ? 200 : 15;
    CGFloat w = [ASize screenWidth] - 2 * padding;

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, w, 1)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont helveticaNeue:16];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 0;
    titleLabel.text = NSLocalizedString(@"search_user_corporate_face", nil);
    [titleLabel sizeToFit];
    titleLabel.centerX = contentScrollView.middleX;
    [form pushView:titleLabel marginTop:15 centered:YES];

    iinField = [[FloatingTextField alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth] - 2 * padding, 44)];
    [iinField setKeyboardType:UIKeyboardTypeNumberPad];
    iinField.maxCharacters = 12;
    [iinField setPlaceholder:NSLocalizedString(@"beneficiar_iin", nil)];
    [form pushView:iinField marginTop:20 centered:YES];

    // Switch
    TitleWithView *accountTypeRow = [[TitleWithView alloc] initWithWidth:w title:NSLocalizedString(@"receiver_account_type", nil)];

    accountTypeControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"card", nil), NSLocalizedString(@"current_account_deposit", nil)]];
    accountTypeControl.width = w;
    accountTypeControl.height = 35;
    [accountTypeControl setWidth:(CGFloat) (accountTypeControl.width * 0.35) forSegmentAtIndex:0];
    accountTypeControl.tintColor = [UIColor fromRGB:0xff8000];
    [accountTypeControl addTarget:self action:@selector(accountTypeSelected:) forControlEvents:UIControlEventValueChanged];
    [accountTypeRow insertView:accountTypeControl];
    [form pushView:accountTypeRow marginTop:20 centered:YES];

    // Last digits
    digitsRow = [[TitleWithView alloc] initWithWidth:w title:@"-"];

    digitsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, w, 60)];
    digitsView.backgroundColor = [UIColor fromRGB:0xEDEDED];
    digitsView.layer.cornerRadius = 6;
    digitsView.layer.masksToBounds = YES;

    placeholderLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    placeholderLabel.backgroundColor = [UIColor clearColor];
    placeholderLabel.font = [UIFont helveticaNeue:16];
    placeholderLabel.textColor = [UIColor darkGrayColor];
    placeholderLabel.x = 15;
    [digitsView addSubview:placeholderLabel];

    digitsField = [[FloatingTextField alloc] initWithFrame:CGRectMake(0, 8, 100, 44)];
    digitsField.keepBaseline = true;
    [digitsField setKeyboardType:UIKeyboardTypeNumberPad];
//    digitsField.y = -8.0f;
    digitsField.clearButtonMode = UITextFieldViewModeNever;
    [digitsView addSubview:digitsField];
    [digitsRow insertView:digitsView];
    [form pushView:digitsRow marginTop:25 centered:YES];

    // Button
    MainButton *searchButton = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, 160, 44)];
    searchButton.mainButtonStyle = MainButtonStyleOrange;
    searchButton.frame = CGRectMake(0, 0, 160, 44);
    searchButton.title = NSLocalizedString(@"search", nil);
    searchButton.centerX = contentScrollView.middleX;
    searchButton.y = digitsRow.bottom + 30;
    [searchButton addTarget:self action:@selector(searchTapped) forControlEvents:UIControlEventTouchUpInside];
    [form pushView:searchButton marginTop:20 centered:YES];

    [self addKeyboardControlFields:@[iinField, digitsField]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"SearchBeneficiatyVC dealloc");
}

@end