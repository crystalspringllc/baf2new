//
//  BeneficiarySearchResultViewController.m
//  BAF
//
//  Created by Almas Adilbek on 11/28/14.
//
//

#import "BeneficiarySearchResultViewController.h"
#import "TitleWithView.h"
#import "FloatingTextField.h"
#import "NSString+Validators.h"
#import "UIHelper.h"
#import "AccountsApi.h"
#import "Toast.h"
#import "BankTransfersHelper.h"
#import "AASimpleTableViewCell.h"
#import "AATableSection.h"
#import "TabbarView.h"
#import "Beneficiary.h"
#import "MainButton.h"
#import "MBProgressHUD+AstanaView.h"

#define kSectionUserInfo @"sectionUserInfo"
#define kSectionAlias @"sectionAlias"

@interface BeneficiarySearchResultViewController ()
@end

@implementation BeneficiarySearchResultViewController {
    FloatingTextField *aliasField;
    UITableView *list;

    NSMutableArray *sections;
    NSMutableArray *rowTitles;
    NSMutableArray *rowValues;

    NSString *alias;
}

-(id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

-(void)initVars {}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];

    rowTitles = [NSMutableArray array];
    rowValues = [NSMutableArray array];
    
    if([_accountType isEqual:ACCOUNT_TYPE_CARD])
    {
        [rowTitles addObject:NSLocalizedString(@"find_card", nil)];
        [rowValues addObject:self.beneficiary.alias];

        [rowTitles addObject:NSLocalizedString(@"ownerr", nil)];
        [rowValues addObject:[NSString stringWithFormat:@"%@ %@", self.beneficiary.lastName, self.beneficiary.firstName]];

        alias = self.beneficiary.alias;
    }
    else if([_accountType isEqual:ACCOUNT_TYPE_CURRENT_INDIVIDUALS])
    {
        [rowTitles addObject:NSLocalizedString(@"find_deposit", nil)];
        [rowValues addObject:[NSString stringWithFormat:@"%@ %@", self.beneficiary.alias, self.beneficiary.iban]];

        [rowTitles addObject:NSLocalizedString(@"ownerr", nil)];
        [rowValues addObject:[NSString stringWithFormat:@"%@ %@", self.beneficiary.lastName, self.beneficiary.firstName]];

        alias = self.beneficiary.alias;
    }

    sections = [[NSMutableArray alloc] init];

    AATableSection *section1 = [[AATableSection alloc] initWithSectionId:kSectionUserInfo];
    section1.rows = rowTitles;
    section1.rows2 = rowValues;
    section1.headerTitle = NSLocalizedString(@"find_account", nil);
    [sections addObject:section1];

    AATableSection *section2 = [[AATableSection alloc] initWithSectionId:kSectionAlias];
    section2.rows = [@[@"row"] mutableCopy];
    section2.headerTitle = NSLocalizedString(@"choose_comfort_name", nil);
    [sections addObject:section2];
}

#pragma mark -
#pragma mark Actions

- (void)addTapped
{
    [self hideKeyboard];

    if(![aliasField.value validate]) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"choose_arbitrary_receiver_name", nil)];
        return;
    }

    NSString *accountNumber = @" ";
    NSString *iban = nil;

    if([_accountType isEqual:ACCOUNT_TYPE_CARD])
    {
        accountNumber = self.beneficiary.alias;
        iban = self.beneficiary.iban;

    }
    else if([_accountType isEqual:ACCOUNT_TYPE_CURRENT_INDIVIDUALS])
    {
        iban = self.beneficiary.iban;
    }
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"adding", nil) animated:true];
    [AccountsApi disableBeneficiary:false beneficiaryId:self.beneficiary.accountId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        [self.delegate searchBeneficiaryViewControllerRecipientAddedWithId:response];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark -
#pragma mark UITableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return sections.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    AATableSection *_section = sections[section];
    return _section.rows.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AATableSection *_section = sections[indexPath.section];
    if([_section.sectionId isEqual:kSectionUserInfo]) {
        NSString *title = rowTitles[indexPath.row];
        NSString *value = rowValues[indexPath.row];
        return [AASimpleTableViewCell cellHeightWithTitle:title subtitle:value];
    }
    return 62;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    AATableSection *_section = sections[section];
    if([_section.sectionId isEqual:kSectionAlias]) {
        return 80;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    AATableSection *_section = sections[section];
    if ([_section.sectionId isEqual:kSectionAlias]) {
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [self tableView:tableView heightForFooterInSection:section])];
        // Button
        MainButton *addButton = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
        [addButton addTarget:self action:@selector(addTapped) forControlEvents:UIControlEventTouchUpInside];
        addButton.mainButtonStyle = MainButtonStyleOrange;
        addButton.frame = CGRectMake(0, 0, 200, 44);
        addButton.centerX = (CGFloat) ([ASize screenWidth] * 0.5);
        addButton.y = footerView.height - addButton.height - 20;
        [addButton setTitle:NSLocalizedString(@"add", nil)];
        [footerView addSubview:addButton];

        return footerView;
    }
    return nil;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    AATableSection *_section = sections[section];
    return _section.headerTitle;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;

    AATableSection *section = sections[indexPath.section];
    if([section.sectionId isEqual:kSectionUserInfo])
    {
        CellIdentifier = @"userInfoCell";

        AASimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[AASimpleTableViewCell alloc] initWithReuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }

        NSString *title = rowTitles[indexPath.row];
        NSString *value = rowValues[indexPath.row];

        [cell setTitle:title];
        [cell setSubtitle:value];
        
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        cell.textLabel.backgroundColor = [UIColor clearColor];

        return cell;
    }
    else
    {
        CellIdentifier = @"fieldCell";

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            aliasField = [[FloatingTextField alloc] initWithFrame:CGRectMake(0, 9, [ASize screenWidth] - 2 * 15, 44)];
            aliasField.centerX = (CGFloat) ([ASize screenWidth] * 0.5);
            [aliasField setPlaceholder:NSLocalizedString(@"name_receiver", nil)];
            [aliasField setValue:alias];
            [cell.contentView addSubview:aliasField];
        }
        
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        cell.textLabel.backgroundColor = [UIColor clearColor];

        return cell;
    }

    return nil;
}

#pragma mark -

- (void)configUI {
    [self setBafBackground];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setNavigationTitle:NSLocalizedString(@"search_resultt", nil)];
    [self setNavigationBackButton];
    
    list = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeightWithoutStatusBarAndNavigationBar]) style:UITableViewStyleGrouped];
    list.backgroundColor = [UIColor clearColor];
    list.backgroundView = nil;
    list.delegate = self;
    list.dataSource = self;
    [self.view addSubview:list];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {}

@end