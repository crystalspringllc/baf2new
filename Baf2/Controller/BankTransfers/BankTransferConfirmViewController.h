//
//  BankTransferConfirmViewController.h
//  BAF
//
//  Created by Almas Adilbek on 11/26/14.
//
//



#import "ScrollViewController.h"
#import "Knp.h"
#import "NewCheckAndInitTransferResponse.h"

@class Account;

@interface BankTransferConfirmViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSString *operationAmount;
@property (nonatomic, strong) NSString *operationCurrency;
@property (nonatomic, strong) NSNumber *servicelogId;
@property (nonatomic, copy) NSString *safetyLevel;
@property (nonatomic, strong) NSString *levelStatus;
@property(nonatomic, strong) Account *fromAccount;
@property(nonatomic, strong) Account *toAccount;
@property(nonatomic, strong) NSString *knpDescription;
@property(nonatomic, copy) NSNumber *amount;
@property(nonatomic, copy) NSNumber *commission;
@property(nonatomic, copy) NSString *commissionCurrency;
@property(nonatomic, copy) NSString *transferType;
@property(nonatomic, copy) NSString *operationDate;
@property(nonatomic, copy) NSString *requsetorCurrency;
@property(nonatomic, copy) NSString *currency;
@property(nonatomic, copy) NSString *convertedAmount;
@property (nonatomic, assign) BOOL isPayboxType;
@property (nonatomic, assign) BOOL isOldVersion;

@property (nonatomic, copy) NSNumber *destinationAmount;
@property (nonatomic, strong) NSString *destinationCurrency;
@property (nonatomic, strong) NSString *rate;
@property (nonatomic, strong) NSString *statusText;

@property (nonatomic) BOOL saveAsTemplate;

@end
