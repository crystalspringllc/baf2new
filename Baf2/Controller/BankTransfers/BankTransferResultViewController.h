//
//  BankTransferResultViewController.h
//  BAF
//
//  Created by Almas Adilbek on 2/19/15.
//
//


@class BankCard;

typedef NS_ENUM(NSUInteger, BankTransferType) {
    BankTransferTypeInternal,
    BankTransferTypeThirdParty,
    BankTransferTypeAmongBanks
};

@interface BankTransferResultViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, assign) BankTransferType bankTransferType;

@property (nonatomic, strong) NSNumber *servicelogId;

@property(nonatomic, strong) id operationId;
@property(nonatomic, strong) id fromAccount;
@property(nonatomic, strong) id toAccount;
@property(nonatomic, copy) NSNumber *amount;
@property(nonatomic, copy) NSString *currency;
@property(nonatomic, copy) NSString *convertedAmount;
@property(nonatomic, strong) NSNumber *comissionAmount;
@property(nonatomic, strong) NSString *comissionCurrency;
@property(nonatomic, copy) NSString *beneficiaryBankName;
@property(nonatomic, copy) NSString *destinationTransferDescription;

@property (nonatomic) BOOL saveAsTemplate;

@end
