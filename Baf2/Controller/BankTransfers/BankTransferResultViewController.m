//
//  BankTransferResultViewController.m
//  BAF
//
//  Created by Almas Adilbek on 2/19/15.
//
//

#import "BankTransferResultViewController.h"
#import "AccountHelper.h"
#import "AASimpleTableViewCell.h"
#import "MainButton.h"
#import "NotificationCenterHelper.h"
#import "UIViewController+NavigationController.h"

@interface BankTransferResultViewController ()
@end

@implementation BankTransferResultViewController {
    UITableView *list;
    UIView *footerButtonView;
    NSMutableArray *infoRowTitles;
    NSMutableArray *infoRowSubtitles;
}

-(id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

- (void)initVars {}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];

    if(!_comissionAmount) _comissionAmount = @(0.0f);

    // Rows
    infoRowTitles = [[NSMutableArray alloc] init];
    infoRowSubtitles = [[NSMutableArray alloc] init];

    [infoRowTitles addObject:NSLocalizedString(@"operation_id", nil)];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@", self.servicelogId]];

    [infoRowTitles addObject:NSLocalizedString(@"account_of_outcome", nil)];
    [infoRowSubtitles addObject:[AccountHelper accountIdentifierAlias:_fromAccount]];

    [infoRowTitles addObject:NSLocalizedString(@"account_enrollment", nil)];
    [infoRowSubtitles addObject:[AccountHelper accountIdentifierAlias:_toAccount]];

    [infoRowTitles addObject:NSLocalizedString(@"transfer_summ", nil)];
    NSString *amount = [NSString stringWithFormat:@"%@ %@", [self.amount decimalFormatString], _currency];
   // if(_convertedAmount) amount = [amount stringByAppendingFormat:@" (%@)", _convertedAmount];
    [infoRowSubtitles addObject:amount];

    [infoRowTitles addObject:NSLocalizedString(@"comission", nil)];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@ %@", [_comissionAmount decimalFormatString], _currency]];

    if(_bankTransferType == BankTransferTypeAmongBanks) {
        [infoRowTitles addObject:NSLocalizedString(@"destination_bank", nil)];
        [infoRowSubtitles addObject:self.beneficiaryBankName];

        [infoRowTitles addObject:NSLocalizedString(@"client_description", nil)];
        [infoRowSubtitles addObject:self.destinationTransferDescription];
    }
}

#pragma mark -
#pragma mark Actions

- (void)goHomeTapped {
    [self.navigationController popToRootViewControllerAnimated:NO];
//    [[UIHelper appDelegate] popToRootInTab:TabbarViewTabMain];
//    [[UIHelper appDelegate] openTab:TabbarViewTabMain];
    
    if (self.saveAsTemplate) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransfersTemplatesUpdated object:nil];
    }
}

- (void)goHistoryTapped {
    [self popToRoot];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferDoneGotoHistory object:nil];
}

#pragma mark -
#pragma mark UITableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return infoRowTitles.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 70;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [self tableView:tableView heightForHeaderInSection:0])];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (CGFloat) (headerView.width * 0.8), 1)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:16];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 0;
    titleLabel.text = NSLocalizedString(@"transfer_sent_to_treatment", nil);
    [titleLabel sizeToFit];
    titleLabel.centerX = headerView.middleX;
    titleLabel.centerY = headerView.middleY;
    [headerView addSubview:titleLabel];
    
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 122;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(!footerButtonView)
    {
        footerButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [self tableView:tableView heightForFooterInSection:section])];
        
        // Goto main button
        MainButton *gohomeButton = [[MainButton alloc] initWithFrame:CGRectMake(0, 12, 220, 45)];
        gohomeButton.mainButtonStyle = MainButtonStyleOrange;
        gohomeButton.frame = CGRectMake(0, 20, 220, 40);
        gohomeButton.centerX = footerButtonView.middleX;
        gohomeButton.title = NSLocalizedString(@"go_to_main_activity", nil);
        [gohomeButton addTarget:self action:@selector(goHomeTapped) forControlEvents:UIControlEventTouchUpInside];
        [footerButtonView addSubview:gohomeButton];
        
        // Goto main button
        MainButton *goHistoryButton = [[MainButton alloc] initWithFrame:CGRectMake(0, gohomeButton.bottom + 8, 220, 45)];
        goHistoryButton.mainButtonStyle = MainButtonStyleOrange;
        goHistoryButton.frame = CGRectMake(0, gohomeButton.bottom + 8, 220, 45);
        goHistoryButton.centerX = footerButtonView.middleX;
        goHistoryButton.title = NSLocalizedString(@"ga_operation_history", nil);
        [goHistoryButton addTarget:self action:@selector(goHistoryTapped) forControlEvents:UIControlEventTouchUpInside];
        [footerButtonView addSubview:goHistoryButton];
    }
    return footerButtonView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *title = infoRowTitles[indexPath.row];
    NSString *subtitle = infoRowSubtitles[indexPath.row];
    return [AASimpleTableViewCell cellHeightWithTitle:title subtitle:subtitle];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    AASimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AASimpleTableViewCell alloc] initWithReuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    NSString *title = infoRowTitles[indexPath.row];
    NSString *subtitle = infoRowSubtitles[indexPath.row];

    [cell setTitle:title];
    [cell setSubtitle:subtitle];
    
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

    return cell;
}

#pragma mark -

-(void)configUI
{
    [self setBafBackground];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationItem setHidesBackButton:YES];
    [self showMenuButton];

    if(_bankTransferType == BankTransferTypeAmongBanks) {
        [self setNavigationTitle:NSLocalizedString(@"transfer_interbank", nil)];
    } else {
        [self setNavigationTitle:NSLocalizedString(@"bank_transfers_lowercase", nil)];
    }

    list = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeightWithoutStatusBarAndNavigationBar]) style:UITableViewStyleGrouped];
    list.backgroundColor = [UIColor clearColor];
    list.backgroundView = nil;
    list.delegate = self;
    list.dataSource = self;
    list.rowHeight = 52;
    [self.view addSubview:list];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc {

}

@end
