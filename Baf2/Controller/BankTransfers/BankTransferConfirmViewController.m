//
//  BankTransferConfirmViewController.m
//  BAF
//
//  Created by Almas Adilbek on 11/26/14.
//
//

#import "BankTransferConfirmViewController.h"
#import "Card.h"
#import "FinancialProduct.h"
#import "BeneficiaryAccount.h"
#import "InputTextField.h"
#import "NSDate+Helper.h"
#import "MyCardsAndAccountsViewController.h"
#import "BankTransferResultViewController.h"
#import "AccountHelper.h"
#import "AASimpleTableViewCell.h"
#import "BankTransferSmsConfirmationViewController.h"
#import "NewTransfersApi.h"
#import "MainButton.h"
#import "NSString+Ext.h"
#import "NotificationCenterHelper.h"
#import "SMSConfirmCell.h"
#import "TransferAmountCellTableViewCell.h"
#import "NSLayoutConstraint+ConcisePureLayout.h"
#import "P2PBrowserViewController.h"
#import "Security3DSViewController.h"
#import "LastOperationDetailsViewController.h"

@interface BankTransferConfirmViewController ()
@end

@implementation BankTransferConfirmViewController {
    InputTextField *codeField;
    UITableView *list;
    UIView *footerButtonView;
    NSMutableArray *infoRowTitles;
    NSMutableArray *infoRowSubtitles;
}

-(id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

-(void)initVars {}



- (void)viewDidLoad
{
    
   /* [super viewDidLoad];
    
    // Use Oper date or today date
    NSString *todayDateString = [[NSDate date] stringWithFormat:@"dd.MM.yyyy"];
    if(!_operationDate) _operationDate = todayDateString;
    
    [self configUI];
    
    // Rows
    infoRowTitles = [NSMutableArray array];
    infoRowSubtitles = [NSMutableArray array];
    
    //    if ([self.transferType isEqualToString:@"MY_TRANSFERS"]) {
    //
    //        [infoRowTitles addObject:NSLocalizedString(@"client_description_code", nil)];
    //        [infoRowSubtitles addObject:NSLocalizedString(@"transfer_from_card_to_my_card", nil)];
    //
    //    } else
    //    if ([self.transferType isEqualToString:@"TRANSFERS_TO_OTHER"]) {
    //
    //        [infoRowTitles addObject:NSLocalizedString(@"client_description_code", nil)];
    //        [infoRowSubtitles addObject:NSLocalizedString(@"transfer_among_thirdparties", nil)];
    //
    //    } else
    //    if ([self.transferType isEqualToString:@"INTERBANK_TRANSFERS"]) {
    //
    //        [infoRowTitles addObject:NSLocalizedString(@"client_description_code", nil)];
    //        [infoRowSubtitles addObject:NSLocalizedString(@"transfer_interbank", nil)];
    //
    //    } else
    //    if ([self.transferType isEqualToString:@"P2P_TRANSFERS"]) {
    //
    //        [infoRowTitles addObject:NSLocalizedString(@"client_description_code", nil)];
    //        [infoRowSubtitles addObject:NSLocalizedString(@"ga_transfers_p2p", nil)];
    //
    //    }
    
    [infoRowTitles addObject:NSLocalizedString(@"write_off_from", nil)];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@\n%@", [AccountHelper accountIdentifierAlias:_fromAccount], self.fromAccount.number ? : @""]];
    
    [infoRowTitles addObject:NSLocalizedString(@"transfer_to", nil)];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@\n%@", [AccountHelper accountIdentifierAlias:_toAccount], self.toAccount.number ? : @""]];
    
    [infoRowTitles addObject:@"Списать"];
    NSString *amount = [NSString stringWithFormat:@"%@ %@", [self.amount decimalFormatString], _currency];
    if(_convertedAmount) amount = [amount stringByAppendingFormat:@" (%@)", _convertedAmount];
    [infoRowSubtitles addObject:amount];
    
    if ([self.transferType isEqualToString:@"MY_TRANSFERS"] ||
        [self.transferType isEqualToString:@"TRANSFERS_TO_OTHER"]) {
        [infoRowTitles addObject:@"Зачислить"];
        [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@ %@", [self.destinationAmount decimalFormatString], self.destinationCurrency]];
        
        if (self.rate.length > 0) {
            [infoRowTitles addObject:@"Курсы валют"];
            [infoRowSubtitles addObject:self.rate];
        }
    }
    
    if (![self.transferType isEqualToString:@"MY_TRANSFERS"] &&
        ![self.transferType isEqualToString:@"TRANSFERS_TO_OTHER"] ) {
        [infoRowTitles addObject:NSLocalizedString(@"comission", nil)];
        [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@ %@", _commission, _commissionCurrency]];
    }
    
    // Oper date
    [infoRowTitles addObject:NSLocalizedString(@"value_date", nil)];
    NSString *operationDateString = _operationDate;
    NSDate *operationDate = [NSDate dateFromString:_operationDate withFormat:@"dd.MM.yyyy"];
    if(operationDate) {
        if([operationDate isToday]) operationDateString = [NSString stringWithFormat:@"%@ (%@)", NSLocalizedString(@"today", nil),_operationDate];
        else if([operationDate isTomorrow]) operationDateString = [NSString stringWithFormat:@"%@ (%@)", NSLocalizedString(@"tomorow", nil),_operationDate];
    }
    [infoRowSubtitles addObject:operationDateString];
    
}

#pragma mark -
#pragma mark Actions

- (void)nextTapped {
    
    // Show sms confirmation field if transfer external
    if([[self.safetyLevel lowercaseString] containsString:@"sms"])
    {
        BankTransferSmsConfirmationViewController *v = [[BankTransferSmsConfirmationViewController alloc] init];
        v.servicelogId = self.servicelogId;
        v.fromAccount = _fromAccount;
        v.toAccount = _toAccount;
        v.amount = _amount;
        v.currency = _currency;
        v.convertedAmount = self.convertedAmount;
        v.commissionAmount = self.commission;
        v.commissionCurrency = self.commissionCurrency;
        v.saveAsTemplate = self.saveAsTemplate;
        [self.navigationController pushViewController:v animated:YES];
    }
    else
    {
        __weak BankTransferConfirmViewController *weakSelf = self;
        
        [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
        
        [NewTransfersApi runTransfer:self.servicelogId success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
            
            // Send notification that should update accounts
            [[NSNotificationCenter defaultCenter] postNotificationName:kMyCardsAndAccountsViewControllerUpdateAccountsNotification object:nil];
            
            // Goto result view controller
            BankTransferResultViewController *v = [[BankTransferResultViewController alloc] init];
            v.servicelogId = weakSelf.servicelogId;
            v.operationId = response;
            v.fromAccount = weakSelf.fromAccount;
            v.toAccount = weakSelf.toAccount;
            v.amount = weakSelf.amount;
            v.currency = weakSelf.currency;
            v.convertedAmount = weakSelf.convertedAmount;
            v.comissionAmount = _commission;
            v.comissionCurrency = weakSelf.commissionCurrency;
            v.saveAsTemplate = weakSelf.saveAsTemplate;
            [weakSelf.navigationController pushViewController:v animated:YES];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];
    }
}

#pragma mark -
#pragma mark UITableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return infoRowTitles.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *title = infoRowTitles[indexPath.row];
    NSString *subtitle = infoRowSubtitles[indexPath.row];
    return [AASimpleTableViewCell cellHeightWithTitle:title subtitle:subtitle];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title1 = @"";
    NSString *title2 = @"";
    
    if ([_fromAccount isKindOfClass:[FinancialProduct class]]) {
        if([(FinancialProduct *)_fromAccount isDeposit]) {
            title1 = NSLocalizedString(@"transfer_from_deposin_on", nil);
        } else {
            title1 = NSLocalizedString(@"transfer_from_current_account_pn", nil);
        }
    }
    
    if ([_toAccount isKindOfClass:[FinancialProduct class]]) {
        if([(FinancialProduct *)_toAccount isDeposit]) {
            title2 = NSLocalizedString(@"deposit_account", nil);
        } else {
            title2 = NSLocalizedString(@"own_current_account", nil);
        }
    }
    else if ([_toAccount isKindOfClass:[BeneficiaryAccount class]])
    {
        // Check if deposit or card account
        BeneficiaryAccount *beneficiaryAccount = (BeneficiaryAccount *) _toAccount;
        if ([beneficiaryAccount isCurrentAccount]) {
            title2 = NSLocalizedString(@"current_account_third_parties", nil);
        } else if([beneficiaryAccount isDeposit]) {
            title2 = NSLocalizedString(@"deposit_third_parties", nil);
        } else {
            title2 = NSLocalizedString(@"card_third_parties", nil);
        }
    }
    
    return [NSString stringWithFormat:@"%@ %@", title1, title2];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 70;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(!footerButtonView) {
        footerButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [self tableView:tableView heightForFooterInSection:section])];
        // Button
        MainButton *submitButton = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, 170, 40)];
        submitButton.title = NSLocalizedString(@"accepting", nil);
        submitButton.mainButtonStyle = MainButtonStyleOrange;
        submitButton.frame = CGRectMake(0, 0, 170, 40);
        submitButton.y = footerButtonView.height - submitButton.height - 15;
        submitButton.centerX = footerButtonView.middleX;
        [submitButton addTarget:self action:@selector(nextTapped) forControlEvents:UIControlEventTouchUpInside];
        [footerButtonView addSubview:submitButton];
    }
    return footerButtonView;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    AASimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AASimpleTableViewCell alloc] initWithReuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.font = [UIFont boldSystemFontOfSize:15];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //            cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
    }
    
    NSString *title = infoRowTitles[(NSUInteger) indexPath.row];
    NSString *subtitle = infoRowSubtitles[(NSUInteger) indexPath.row];
    
    [cell setTitle:title];
    [cell setSubtitle:subtitle];
    
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    
    // THe operation date cell
    if(indexPath.row == 4) {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd.MM.yyyy"];
        NSDate *operDate = [df dateFromString:_operationDate];
        if(![operDate isToday]) {
            [cell setSubtitleTextColor:[UIColor deleteColor]];
        }
    }
    
    return cell;
}

- (void)configUI {
    [self setBafBackground];
    //self.view.backgroundColor = [UIColor whiteColor];
    
    [self setNavigationTitle:NSLocalizedString(@"accepting_of_data", nil)];
    [self setNavigationBackButton];
    
    list = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeightWithoutStatusBarAndNavigationBar]) style:UITableViewStyleGrouped];
    list.backgroundColor = [UIColor clearColor];
    list.backgroundView = nil;
    list.delegate = self;
    list.dataSource = self;
    [self.view addSubview:list];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    if (self.saveAsTemplate) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransfersTemplatesUpdated object:nil];
    }
}*/
    
    [super viewDidLoad];

    // Use Oper date or today date
    NSString *todayDateString = [[NSDate date] stringWithFormat:@"dd.MM.yyyy"];
    if(!_operationDate) _operationDate = todayDateString;

    [self configUI];

    // Rows
    infoRowTitles = [NSMutableArray array];
    infoRowSubtitles = [NSMutableArray array];

//    if ([self.transferType isEqualToString:@"MY_TRANSFERS"]) {
//
//        [infoRowTitles addObject:NSLocalizedString(@"client_description_code", nil)];
//        [infoRowSubtitles addObject:NSLocalizedString(@"transfer_from_card_to_my_card", nil)];
//
//    } else
//    if ([self.transferType isEqualToString:@"TRANSFERS_TO_OTHER"]) {
//
//        [infoRowTitles addObject:NSLocalizedString(@"client_description_code", nil)];
//        [infoRowSubtitles addObject:NSLocalizedString(@"transfer_among_thirdparties", nil)];
//
//    } else
//    if ([self.transferType isEqualToString:@"INTERBANK_TRANSFERS"]) {
//
//        [infoRowTitles addObject:NSLocalizedString(@"client_description_code", nil)];
//        [infoRowSubtitles addObject:NSLocalizedString(@"transfer_interbank", nil)];
//
//    } else
//    if ([self.transferType isEqualToString:@"P2P_TRANSFERS"]) {
//
//        [infoRowTitles addObject:NSLocalizedString(@"client_description_code", nil)];
//        [infoRowSubtitles addObject:NSLocalizedString(@"ga_transfers_p2p", nil)];
//
//    }
    
  /*  [infoRowTitles addObject:@"ID операции"];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@", self.servicelogId]];
    
    [infoRowTitles addObject:@"Время операции"];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@", self.operationDate]];
    
    if(!self.isOldVersion) [infoRowTitles addObject:@"Cтатус"];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@", self.statusText]];
    
    [infoRowTitles addObject:NSLocalizedString(@"write_off_from", nil)];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@ %@", [AccountHelper accountIdentifierAlias:_fromAccount], self.fromAccount.number ? : @""]];
    
    [infoRowTitles addObject:@"Списать"];
    NSString *amount = [NSString stringWithFormat:@"%@ %@", self.amount, self.requsetorCurrency];
    //if(_convertedAmount.length > 4) amount = [amount stringByAppendingFormat:@" (%@)", _convertedAmount];
    [infoRowSubtitles addObject:amount];

    [infoRowTitles addObject:NSLocalizedString(@"transfer_to", nil)];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@ %@", [AccountHelper accountIdentifierAlias:_toAccount], self.toAccount.number ? : @""]];


      if(!self.isOldVersion) [infoRowTitles addObject:@"Зачислить"];
        [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@ %@", [self.destinationAmount decimalFormatString], self.destinationCurrency]];

        if (self.rate.length > 0) {
            [infoRowTitles addObject:@"Курсы валют"];
            [infoRowSubtitles addObject:self.rate];
    }
    
    [infoRowTitles addObject:@"Назначение платежа"];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@", self.knpDescription]];

    NSString *operationDateString = _operationDate;
    NSDate *operationDate = [NSDate dateFromString:_operationDate withFormat:@"dd.MM.yyyy"];
    if(operationDate) {
        if([operationDate isToday]) operationDateString = [NSString stringWithFormat:@"%@ (%@)", NSLocalizedString(@"today", nil),_operationDate];
        else if([operationDate isTomorrow]) operationDateString = [NSString stringWithFormat:@"%@ (%@)", NSLocalizedString(@"tomorow", nil),_operationDate];
    }*/
    
   /* [infoRowTitles addObject:NSLocalizedString(@"write_off_from", nil)];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@ %@", [AccountHelper accountIdentifierAlias:_fromAccount], self.fromAccount.number ? : @""]];
    
    [infoRowTitles addObject:@"Списать"];
    NSString *amount = [NSString stringWithFormat:@"%@ %@", self.amount, self.requsetorCurrency];
    //if(_convertedAmount.length > 4) amount = [amount stringByAppendingFormat:@" (%@)", _convertedAmount];
    [infoRowSubtitles addObject:amount];
    
    [infoRowTitles addObject:NSLocalizedString(@"transfer_to", nil)];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@ %@", [AccountHelper accountIdentifierAlias:_toAccount], self.toAccount.number ? : @""]];
    
    if(!self.isOldVersion) [infoRowTitles addObject:@"Зачислить"];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@ %@", [self.destinationAmount decimalFormatString], self.destinationCurrency]];
    
    [infoRowTitles addObject:@"Назначение платежа"];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@", self.knpDescription]];
    
    [infoRowTitles addObject:@"Комиссия за операцию:"];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@ %@", self.commission, self.commissionCurrency]];
    
    [infoRowTitles addObject:@"Итого, перевод на сумму"];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@ %@", self.amount, self.requsetorCurrency]];
    
    [infoRowTitles addObject:@"ID операции"];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@", self.servicelogId]];
    
    [infoRowTitles addObject:@"Время операции"];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@", self.operationDate]];
    
    if(!self.isOldVersion) [infoRowTitles addObject:@"Cтатус"];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@", self.statusText]];
   
    if (self.rate.length > 0) {
        [infoRowTitles addObject:@"Курсы валют"];
        [infoRowSubtitles addObject:self.rate];
    }
    */
    
    [infoRowTitles addObject:NSLocalizedString(@"write_off_from", nil)];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@\n%@", [AccountHelper accountIdentifierAlias:_fromAccount], self.fromAccount.number ? : @""]];
    
    [infoRowTitles addObject:NSLocalizedString(@"transfer_to", nil)];
    [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@\n%@", [AccountHelper accountIdentifierAlias:_toAccount], self.toAccount.number ? : @""]];
    
    [infoRowTitles addObject:@"Списать"];
    NSString *amount = [NSString stringWithFormat:@"%@ %@", [self.amount decimalFormatString], _currency];
    if(_convertedAmount) amount = [amount stringByAppendingFormat:@" (%@)", _convertedAmount];
    [infoRowSubtitles addObject:amount];
    
    if ([self.transferType isEqualToString:@"MY_TRANSFERS"] ||
        [self.transferType isEqualToString:@"TRANSFERS_TO_OTHER"]) {
        [infoRowTitles addObject:@"Зачислить"];
        [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@ %@", [self.destinationAmount decimalFormatString], self.destinationCurrency]];
        
        if (self.rate.length > 0) {
            [infoRowTitles addObject:@"Курсы валют"];
            [infoRowSubtitles addObject:self.rate];
        }
    }
    
    if (![self.transferType isEqualToString:@"MY_TRANSFERS"] &&
        ![self.transferType isEqualToString:@"TRANSFERS_TO_OTHER"] ) {
        [infoRowTitles addObject:NSLocalizedString(@"comission", nil)];
        [infoRowSubtitles addObject:[NSString stringWithFormat:@"%@ %@", _commission, _commissionCurrency]];
    }
    
    // Oper date
    [infoRowTitles addObject:NSLocalizedString(@"value_date", nil)];
    NSString *operationDateString = _operationDate;
    NSDate *operationDate = [NSDate dateFromString:_operationDate withFormat:@"dd.MM.yyyy"];
    if(operationDate) {
        if([operationDate isToday]) operationDateString = [NSString stringWithFormat:@"%@ (%@)", NSLocalizedString(@"today", nil),_operationDate];
        else if([operationDate isTomorrow]) operationDateString = [NSString stringWithFormat:@"%@ (%@)", NSLocalizedString(@"tomorow", nil),_operationDate];
    }
    [infoRowSubtitles addObject:operationDateString];
    
}

#pragma mark -
#pragma mark Actions

- (void)nextTapped {
    
    // Show sms confirmation field if transfer external
    if([[self.safetyLevel lowercaseString] containsString:@"sms"])
    {
        BankTransferSmsConfirmationViewController *v = [[BankTransferSmsConfirmationViewController alloc] init];
        v.servicelogId = self.servicelogId;
        v.fromAccount = _fromAccount;
        v.toAccount = _toAccount;
        v.amount = _amount;
        v.currency = _currency;
        v.convertedAmount = self.convertedAmount;
        v.commissionAmount = self.commission;
        v.commissionCurrency = self.commissionCurrency;
        v.saveAsTemplate = self.saveAsTemplate;
        [self.navigationController pushViewController:v animated:YES];
    }
    else
    {
        __weak BankTransferConfirmViewController *weakSelf = self;
        
        [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
        
        [NewTransfersApi runTransfer:self.servicelogId success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
            
            // Send notification that should update accounts
            [[NSNotificationCenter defaultCenter] postNotificationName:kMyCardsAndAccountsViewControllerUpdateAccountsNotification object:nil];
            
            // Goto result view controller
            BankTransferResultViewController *v = [[BankTransferResultViewController alloc] init];
            v.servicelogId = weakSelf.servicelogId;
            v.operationId = response;
            v.fromAccount = weakSelf.fromAccount;
            v.toAccount = weakSelf.toAccount;
            v.amount = weakSelf.amount;
            v.currency = weakSelf.currency;
            v.convertedAmount = weakSelf.convertedAmount;
            v.comissionAmount = _commission;
            v.comissionCurrency = weakSelf.commissionCurrency;
            v.saveAsTemplate = weakSelf.saveAsTemplate;
            [weakSelf.navigationController pushViewController:v animated:YES];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];
    }
}

#pragma mark -
#pragma mark UITableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return infoRowTitles.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *title = infoRowTitles[indexPath.row];
    NSString *subtitle = infoRowSubtitles[indexPath.row];
    return [AASimpleTableViewCell cellHeightWithTitle:title subtitle:subtitle];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title1 = @"";
    NSString *title2 = @"";
    
    if ([_fromAccount isKindOfClass:[FinancialProduct class]]) {
        if([(FinancialProduct *)_fromAccount isDeposit]) {
            title1 = NSLocalizedString(@"transfer_from_deposin_on", nil);
        } else {
            title1 = NSLocalizedString(@"transfer_from_current_account_pn", nil);
        }
    }
    
    if ([_toAccount isKindOfClass:[FinancialProduct class]]) {
        if([(FinancialProduct *)_toAccount isDeposit]) {
            title2 = NSLocalizedString(@"deposit_account", nil);
        } else {
            title2 = NSLocalizedString(@"own_current_account", nil);
        }
    }
    else if ([_toAccount isKindOfClass:[BeneficiaryAccount class]])
    {
        // Check if deposit or card account
        BeneficiaryAccount *beneficiaryAccount = (BeneficiaryAccount *) _toAccount;
        if ([beneficiaryAccount isCurrentAccount]) {
            title2 = NSLocalizedString(@"current_account_third_parties", nil);
        } else if([beneficiaryAccount isDeposit]) {
            title2 = NSLocalizedString(@"deposit_third_parties", nil);
        } else {
            title2 = NSLocalizedString(@"card_third_parties", nil);
        }
    }
    
    return [NSString stringWithFormat:@"%@ %@", title1, title2];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 70;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(!footerButtonView) {
        footerButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [self tableView:tableView heightForFooterInSection:section])];
        // Button
        MainButton *submitButton = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, 170, 40)];
        submitButton.title = NSLocalizedString(@"accepting", nil);
        submitButton.mainButtonStyle = MainButtonStyleOrange;
        submitButton.frame = CGRectMake(0, 0, 170, 40);
        submitButton.y = footerButtonView.height - submitButton.height - 15;
        submitButton.centerX = footerButtonView.middleX;
        [submitButton addTarget:self action:@selector(nextTapped) forControlEvents:UIControlEventTouchUpInside];
        [footerButtonView addSubview:submitButton];
    }
    return footerButtonView;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    AASimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AASimpleTableViewCell alloc] initWithReuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.font = [UIFont boldSystemFontOfSize:15];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //            cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
    }
    
    NSString *title = infoRowTitles[(NSUInteger) indexPath.row];
    NSString *subtitle = infoRowSubtitles[(NSUInteger) indexPath.row];
    
    [cell setTitle:title];
    [cell setSubtitle:subtitle];
    
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    
    // THe operation date cell
    if(indexPath.row == 4) {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd.MM.yyyy"];
        NSDate *operDate = [df dateFromString:_operationDate];
        if(![operDate isToday]) {
            [cell setSubtitleTextColor:[UIColor deleteColor]];
        }
    }
    
    return cell;
}

- (void)configUI {
    [self setBafBackground];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setNavigationTitle:NSLocalizedString(@"accepting_of_data", nil)];
    [self setNavigationBackButton];
    
    list = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeightWithoutStatusBarAndNavigationBar]) style:UITableViewStyleGrouped];
    list.backgroundColor = [UIColor clearColor];
    list.backgroundView = nil;
    list.delegate = self;
    list.dataSource = self;
    [self.view addSubview:list];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    if (self.saveAsTemplate) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransfersTemplatesUpdated object:nil];
    }
}
@end

