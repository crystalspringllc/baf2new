//
//  SearchBeneficiaryViewController.h
//  BAF
//
//  Created by Almas Adilbek on 11/27/14.
//
//



#import "ScrollViewController.h"
#import "BeneficiarySearchResultViewController.h"

@protocol SearchBeneficiaryViewControllerDelegate<NSObject>
- (void)searchBeneficiaryViewControllerRecipientAddedWithId:(NSNumber *)beneficialAccountId;
@end

@interface SearchBeneficiaryViewController : ScrollViewController <BeneficiarySearchResultViewControllerDelegate>

@property(nonatomic, weak) id <SearchBeneficiaryViewControllerDelegate> delegate;

@end
