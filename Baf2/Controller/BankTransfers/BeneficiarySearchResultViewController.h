//
//  BeneficiarySearchResultViewController.h
//  BAF
//
//  Created by Almas Adilbek on 11/28/14.
//
//



#import "ScrollViewController.h"

@class Beneficiary;

@protocol BeneficiarySearchResultViewControllerDelegate<NSObject>
- (void)searchBeneficiaryViewControllerRecipientAddedWithId:(NSNumber *)beneficialAccountId;
@end

@interface BeneficiarySearchResultViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, weak) id <BeneficiarySearchResultViewControllerDelegate> delegate;
@property(nonatomic, strong) Beneficiary *beneficiary;
@property(nonatomic, copy) NSString *lastDigits;
@property(nonatomic, copy) NSString *accountType;

@end
