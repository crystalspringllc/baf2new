//
// Created by Askar Mustafin on 4/10/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "CardsViewController.h"
#import "IconedLabel.h"
#import "CardCell.h"
#import "AccountDetailViewController.h"
#import "MyCardsAndAccountsViewController.h"
#import "AstanaRefreshControl.h"
#import "MDButton.h"
#import "AccountsApi.h"
#import "AuthApi.h"
#import "ChameleonMacros.h"
#import "ScrollDirectionsHelper.h"
#import "ExternalCardRegistrationViewController.h"
#import "NotificationCenterHelper.h"
#import "RequestCardMainViewController.h"
#import "OrderCallRequestViewController.h"
#import "PasswordExpireViewController.h"
#import "PasswordExpireDisclaimerView.h"
#import "RequestCardTypesViewController.h"


@interface CardsViewController ()<UITableViewDelegate, UITableViewDataSource, ConnectAccountsSearchUserViewControllerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate> {}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet MDButton *allAccountsButton;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *allAccountsButtonBottomConstraint;
@property (nonatomic, strong) AstanaRefreshControl *astanaRefreshControl;
@property (nonatomic, strong) ScrollDirectionsHelper *scrollDirectionHelper;
@property (nonatomic, strong) AccountsResponse *accountsResponse;
@property (nonatomic, strong) NSArray *cards;

@property (nonatomic, strong) PasswordExpireDisclaimerView *passwordExpireDisclaimerView;

@end

@implementation CardsViewController {
    BOOL didLoadAccounts;
    BOOL shouldNotShowPasswordExpireDisclaimer;
}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Cards" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([CardsViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self loadAccounts];

    if (![User sharedInstance].isEmailConfirmed || ![User sharedInstance].isPhoneConfirmed)
    {
        [UIHelper showEmailPhoneConfirmationWindow:YES target:^{
            [AuthApi getClientBySession:[User sharedInstance].sessionID success:^(id response2) {
                [[User sharedInstance] setUserProfileData:response2];
            } failure:^(NSString *code, NSString *message) {}];
        }];
    }
}

#pragma mark - loadings

- (void)loadAccountsWithUpdate {
    [AccountsApi getAccountsWithUpdateForce:false withSuccess:^(id response) {
        [self handleAccountsWithResponse:response];
        [self.astanaRefreshControl finishingLoading];
    } failure:^(NSString *code, NSString *message) {
        [self.astanaRefreshControl finishingLoading];
    }];
}

- (void)loadAccounts {
    [MBProgressHUD showAstanaHUDWithTitle:@"Загрузка счетов" animated:YES];
    [AccountsApi getAccountsSuccess:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [self handleAccountsWithResponse:response];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

- (void)handleAccountsWithResponse:(id)response {
    didLoadAccounts = true;
    [User sharedInstance].accountsResponse = [AccountsResponse instanceFromDictionary:response];
    self.accountsResponse = [User sharedInstance].accountsResponse;
    if (self.accountsResponse.inUpdating) {
        [self ping];
    }
    NSPredicate *favouritePredictate = [NSPredicate predicateWithFormat:@"isFavorite == YES"];
    self.cards = [[self.accountsResponse getAllCards] filteredArrayUsingPredicate:favouritePredictate];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    [self.tableView reloadData];
}

- (void)ping {
    __weak CardsViewController *wSelf = self;

    [AccountsApi isUpdating:^(id response) {

        if (response && [response isKindOfClass:[NSNumber class]]) {
            NSNumber *isUpdating = (NSNumber *)response;

            if ([isUpdating boolValue]) {
                //true isUpdating ewe raz
                [wSelf performBlock:^{
                    [wSelf ping];
                } afterDelay:3];
            } else {
                //false getClient
                [wSelf loadAccounts];
            }

        }

    } failure:^(NSString *code, NSString *message) {
        [ApiClient cancelAllOperations];
    }];
}

#pragma mark - config actions

- (void)clickAllAccountsButton:(id)sender {
    MyCardsAndAccountsViewController *v = [[MyCardsAndAccountsViewController alloc] init];
    v.isOpenedFromMenu = NO;
    v.accountsResponse = self.accountsResponse;
    [self.navigationController pushViewController:v animated:YES];
}

- (void)searchForAccountsByIIN {
    if (![User isEmailAndPhoneConfirmedM]) { return; }

    ConnectAccountsSearchUserViewController *v = [[ConnectAccountsSearchUserViewController alloc] init];
    v.delegate = self;
    [self.navigationController pushViewController:v animated:YES];
}

- (void)registerCard {
    if (![User isEmailAndPhoneConfirmedM]) { return; }

    ExternalCardRegistrationViewController *v = [[ExternalCardRegistrationViewController alloc] init];
    [self.navigationController pushViewController:v animated:YES];
}

- (void)orderCard {
    if (![User isEmailAndPhoneConfirmedM]) { return; }

    RequestCardTypesViewController *v = [[RequestCardTypesViewController alloc] initWithStyle:UITableViewStyleGrouped];
    [self.navigationController pushViewController:v animated:YES];
}

#pragma mark - Methods

- (void)postponePasswordChange {
    __weak CardsViewController *weakSelf = self;
    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
    [AuthApi postponePasswordChangeWithSuccess:^(id response) {
        shouldNotShowPasswordExpireDisclaimer = true;

        [MBProgressHUD hideAstanaHUDAnimated:true];
        [weakSelf clearHeader];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        NSLog(@"Failed to postpone password change with error code %@, message %@", code, message);
    }];
}

- (void)changePassword {
    PasswordExpireViewController *vc = [[PasswordExpireViewController alloc] init];
    vc.graces = [User sharedInstance].graces;
    vc.expireDays = [User sharedInstance].expireDays;
    __weak CardsViewController *weakSelf = self;
    vc.didUpdatePassword = ^{
        shouldNotShowPasswordExpireDisclaimer = true;
        [weakSelf updateHeader];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - ConnectAccountsSearchUserViewControllerDelegate

- (void)connectAccountsSearchUserViewControllerAccountConnected {
    [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"accounts_successfully_plugedin", nil) message:@""];

    [[User sharedInstance] setIsClient:YES];

    [self loadAccounts];

    // Post account conencted notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationClientAccountsConnected object:nil];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSAttributedString *title = [[NSAttributedString alloc]
                                 initWithString:NSLocalizedString(@"fav_cards_empty_disclaimer", nil)
                                     attributes:@{
                                             NSFontAttributeName: [UIFont systemFontOfSize:15],
                                             NSForegroundColorAttributeName: [UIColor darkGrayColor]
                                     }];

    return title;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSAttributedString *description = [[NSAttributedString alloc]
                                 initWithString:NSLocalizedString(@"for_add_card_to_favourites", nil)
                                     attributes:@{
                                             NSFontAttributeName: [UIFont systemFontOfSize:13],
                                             NSForegroundColorAttributeName: [UIColor grayColor]
                                     }];
    return description;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView {
    return true;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return true;
}

#pragma mark - tableview delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.cards.count == 0 && didLoadAccounts) {
        return nil;
    }

    IconedLabel *iconedLabel = [[IconedLabel alloc] init];
    iconedLabel.iconImageView.image = [UIImage imageNamed:@"icon-baf-logo"];
    NSString *update = [self.accountsResponse getUpdatedNiceFormat];
    if (update) {
        iconedLabel.textLabel.text = [NSString stringWithFormat:@"%@ (%@ %@)", NSLocalizedString(@"_cards", nil), NSLocalizedString(@"datas_on", nil), update];
    } else {
        iconedLabel.textLabel.text = NSLocalizedString(@"_cards", nil);
    }

    iconedLabel.textLabel.textColor = [UIColor flatBlackColorDark];
    iconedLabel.backgroundColor = [UIColor clearColor];
    iconedLabel.textLabel.font = [UIFont systemFontOfSize:10];
    iconedLabel.leftIndent = 15.0;

    if (!self.accountsResponse.inUpdating) {
        [iconedLabel.loaderView stopAnimating];
    }

    return iconedLabel;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.cards.count == 0 && didLoadAccounts) {
        return UITableViewAutomaticDimension;
    }

    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (didLoadAccounts && (!self.accountsResponse || self.accountsResponse.isEmpty) && section == 2) {
        return NSLocalizedString(@"order_card_proposal", nil);
    }

    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (didLoadAccounts && (!self.accountsResponse || self.accountsResponse.isEmpty)) {
        return 2;
    }

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (didLoadAccounts && (!self.accountsResponse || self.accountsResponse.isEmpty)) {
        return 1;
    }

    return self.cards.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (didLoadAccounts && (!self.accountsResponse || self.accountsResponse.isEmpty)) {
        if (indexPath.section == 0) {
            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"EmptyHeaderCell"];
            return cell;

        } else if (indexPath.section == 1) {
            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DisclaimerTableViewCell"];

            UIButton *searchAccounsByIINButton = [cell.contentView viewWithTag:101];
            UIButton *registerExternalCardButton = [cell.contentView viewWithTag:102];
            UIButton *orderCardButton = [cell.contentView viewWithTag:103];

            [searchAccounsByIINButton addTarget:self action:@selector(searchForAccountsByIIN) forControlEvents:UIControlEventTouchUpInside];
            [registerExternalCardButton addTarget:self action:@selector(registerCard) forControlEvents:UIControlEventTouchUpInside];
            [orderCardButton addTarget:self action:@selector(orderCard) forControlEvents:UIControlEventTouchUpInside];

            return cell;
        }
    }


    CardCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CardCell"];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    Card *card = self.cards[(NSUInteger) indexPath.row];
    cell.card = card;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CardsViewController *wSelf = self;
    if (self.cards.count != 0) {
        Card *card = self.cards[(NSUInteger) indexPath.row];
        AccountDetailViewController *accountDetailVC = [AccountDetailViewController createVC];
        accountDetailVC.account = card;
        accountDetailVC.onUpdateAccounts = ^{
            [wSelf loadAccounts];
        };
        [self.navigationController pushViewController:accountDetailVC animated:YES];

        [tableView deselectRowAtIndexPath:indexPath animated:true];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.astanaRefreshControl scrollViewDidScroll:scrollView];

    [self.scrollDirectionHelper scrollAutolayoutView:self.allAccountsButton scrollView:scrollView];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    [self.astanaRefreshControl scrollViewWillEndDragging];
}

- (void)refreshTableView:(AstanaRefreshControl *)refreshControl {
    [self loadAccountsWithUpdate];
}

- (void)updateTableView:(NSNotification *)notification {
    NSPredicate *favouritePredictate = [NSPredicate predicateWithFormat:@"isFavorite == YES"];
    self.accountsResponse = [User sharedInstance].accountsResponse;
    self.cards = [[self.accountsResponse getAllCards] filteredArrayUsingPredicate:favouritePredictate];
    [self.tableView reloadData];
}

#pragma mark - config ui

- (void)configUI {
    [self setBafBackground];
    self.automaticallyAdjustsScrollViewInsets = NO;

    [self hideBackButtonTitle];

    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 200;
    self.tableView.tableFooterView = [UIView new];

    [self updateHeader];

    //refresh control
    self.astanaRefreshControl = [AstanaRefreshControl new];
    self.astanaRefreshControl.target = self;
    self.astanaRefreshControl.action = @selector(refreshTableView:);
    [self.tableView addSubview:self.astanaRefreshControl];
    [self.tableView sendSubviewToBack:self.astanaRefreshControl];
    //self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 80, 0);

    self.allAccountsButton.backgroundColor = [UIColor mainButtonColor];
    [self.allAccountsButton setImage:[UIImage imageNamed:@"icon_six_dots"] forState:UIControlStateNormal];
    self.allAccountsButton.type = 2;
    [self.allAccountsButton addTarget:self action:@selector(clickAllAccountsButton:) forControlEvents:UIControlEventTouchUpInside];

    [self.allAccountsButtonBottomConstraint autoRemove];

    self.scrollDirectionHelper = [ScrollDirectionsHelper new];
    [self.scrollDirectionHelper setBottomConstraint:self.allAccountsButton offset:20];

    [[NSNotificationCenter defaultCenter] removeObserver:self]; // sometimes dealloc not called, therefore remove previous observers if exist
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTableView:) name:kNotificationAccountAliasChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTableView:) name:kNotificationAccountBlocked object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTableView:) name:kNotificationCardInfoChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTableView:) name:kNotificationAccountFavouriteChanged object:nil];
}

- (void)updateHeader {
    if ([User sharedInstance].isPasswordExpired && !shouldNotShowPasswordExpireDisclaimer) {
        [self addDisclaimerHeaderView];
    } else {
        [self clearHeader];
    }
    [self.tableView reloadData];
}

- (void)addDisclaimerHeaderView {
    self.passwordExpireDisclaimerView = [[PasswordExpireDisclaimerView alloc] initWithGraces:[User sharedInstance].graces passwordChangePeriod:[User sharedInstance].expireDays];

    __weak CardsViewController *weakSelf = self;
    self.passwordExpireDisclaimerView.didTapNotNow = ^{
        shouldNotShowPasswordExpireDisclaimer = true;
        [weakSelf clearHeader];
    };
    self.passwordExpireDisclaimerView.didTapPostpone = ^{
        [weakSelf postponePasswordChange];
    };
    self.passwordExpireDisclaimerView.didTapChangePassword = ^{
        [weakSelf changePassword];
    };

    // set the tableHeaderView so that the required height can be determined
    self.tableView.tableHeaderView = self.passwordExpireDisclaimerView;
    [self.passwordExpireDisclaimerView setNeedsLayout];
    [self.passwordExpireDisclaimerView layoutIfNeeded];
    CGFloat height = [self.passwordExpireDisclaimerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;

    // update the header's frame and set it again
    CGRect headerFrame = self.passwordExpireDisclaimerView.frame;
    headerFrame.size.height = height;
    self.passwordExpireDisclaimerView.frame = headerFrame;
    self.tableView.tableHeaderView = self.passwordExpireDisclaimerView;
}

- (void)clearHeader {
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, CGFLOAT_MIN)];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"CardsViewController dealloced");
}

@end
