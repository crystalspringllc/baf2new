//
//  ContactsMapViewController.h
//  BAF
//
//  Created by Almas Adilbek on 5/18/15.
//
//



#import "ObjectsMapView.h"

@class OCMapViewSampleHelpAnnotation;

@interface ContactsMapViewController : UIViewController <ObjectsMapViewDelegate, CLLocationManagerDelegate>
@property (nonatomic, strong) OCMapViewSampleHelpAnnotation *annotationToShow;
@end
