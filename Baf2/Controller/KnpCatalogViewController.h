//
//  KnpCatalogViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 03.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Knp;

@interface KnpCatalogViewController : UITableViewController

@property (nonatomic, strong) NSArray *preparedKnpList;

@property (nonatomic) BOOL isLegal;

@property (nonatomic, copy) void (^didSelectKnpItem)(Knp *knp);

@end
