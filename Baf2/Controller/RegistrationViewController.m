//
//  RegistrationViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 11.04.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "RegistrationViewController.h"
#import "MainViewController.h"
#import "MenuViewController.h"
#import "RegistrationStep1ViewController.h"
#import "RegistrationStep2ViewController.h"
#import "RegistrationStep3ViewController.h"
#import "NotificationCenterHelper.h"

#define STEPS_COUNT 3

@interface RegistrationViewController ()

@property (nonatomic, strong) UIView *containerView;

@property (nonatomic, strong) UIView *stepNavigationView;
@property (nonatomic, strong) UILabel *pageDetailLabel;
@property (nonatomic, strong) UIButton *backButton;

@property (nonatomic, strong) RegistrationStep1ViewController *step1VC;
@property (nonatomic, strong) RegistrationStep2ViewController *step2VC;
@property (nonatomic, strong) RegistrationStep3ViewController *step3VC;

@property (nonatomic, strong) NSLayoutConstraint *toViewTopConstraint;
@property (nonatomic, strong) NSLayoutConstraint *toViewBottomConstraint;
@property (nonatomic, strong) NSLayoutConstraint *toViewLeftConstraint;
@property (nonatomic, strong) NSLayoutConstraint *toViewRightConstraint;

@property (nonatomic, strong) NSLayoutConstraint *fromViewTopConstraint;
@property (nonatomic, strong) NSLayoutConstraint *fromViewBottomConstraint;
@property (nonatomic, strong) NSLayoutConstraint *fromViewLeftConstraint;
@property (nonatomic, strong) NSLayoutConstraint *fromViewRightConstraint;

@property (nonatomic) NSInteger currentPage;

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initVars];
    [self configUI];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)initVars {
}

#pragma mark - Methods

- (void)buildRegistrationForm {
    // Navigation view
    self.stepNavigationView = [UIView newAutoLayoutView];
    self.stepNavigationView.backgroundColor = [UIColor fromRGB:0xffffbb];
    [self.view addSubview:self.stepNavigationView];
    [self.stepNavigationView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero excludingEdge:ALEdgeBottom];
    
    // Page detail line
    UIView *pageDetailLine = [UIView newAutoLayoutView];
    pageDetailLine.backgroundColor = [UIColor fromRGB:0xfa8144];
    [self.stepNavigationView addSubview:pageDetailLine];
    [pageDetailLine autoSetDimension:ALDimensionHeight toSize:5];
    [pageDetailLine autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [pageDetailLine autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [pageDetailLine autoPinEdgeToSuperviewEdge:ALEdgeRight];
    
    // Page detail label
    self.pageDetailLabel = [UILabel newAutoLayoutView];
    self.pageDetailLabel.font = [UIFont systemFontOfSize:14];
    self.pageDetailLabel.textColor = [UIColor fromRGB:0x4c4c4c];
    self.pageDetailLabel.numberOfLines = 0;
    self.pageDetailLabel.preferredMaxLayoutWidth = [ASize screenWidth] - 2 * 10;
    self.pageDetailLabel.text = NSLocalizedString(@"fill_all_fields_reminder", nil);
    [self.stepNavigationView addSubview:self.pageDetailLabel];
    [self.pageDetailLabel autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(20, 44, 10, 10)];
    
    self.backButton = [UIButton newAutoLayoutView];
    [self.backButton setImage:[UIImage imageNamed:@"icon-arrow-back"] forState:UIControlStateNormal];
    self.backButton.tintColor = [UIColor fromRGB:0xfa8144];
    [self.backButton addTarget:self action:@selector(goStepBack) forControlEvents:UIControlEventTouchUpInside];
    [self.stepNavigationView addSubview:self.backButton];
    [self.backButton autoSetDimension:ALDimensionWidth toSize:44];
    [self.backButton autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [self.backButton autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [self.backButton autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    self.backButton.hidden = true;
    
    // Container view
    self.containerView = [UIView newAutoLayoutView];
    [self.view addSubview:self.containerView];
    [self.containerView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.stepNavigationView];
    [self.containerView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero excludingEdge:ALEdgeTop];
    
    // Step 1
    self.step1VC = [[UIStoryboard storyboardWithName:@"Registration" bundle:nil] instantiateViewControllerWithIdentifier:@"RegistrationStep1ViewController"];
    self.step1VC.phone = self.phone;
    [self addChildViewController:self.step1VC];
    [self.containerView addSubview:self.step1VC.view];
    self.fromViewTopConstraint = [self.step1VC.view autoPinEdgeToSuperviewEdge:ALEdgeTop];
    self.fromViewLeftConstraint = [self.step1VC.view autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    self.fromViewBottomConstraint = [self.step1VC.view autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    self.fromViewRightConstraint = [self.step1VC.view autoPinEdgeToSuperviewEdge:ALEdgeRight];
    [self.step1VC didMoveToParentViewController:self];
    
    __weak RegistrationViewController *weakSelf = self;
    self.step1VC.didValidate = ^{
        [weakSelf goToStep2];
    };
    
    // Step 2
    self.step2VC = [[UIStoryboard storyboardWithName:@"Registration" bundle:nil] instantiateViewControllerWithIdentifier:@"RegistrationStep2ViewController"];
    self.step2VC.didRegister = ^{
        [weakSelf goToStep3];
    };
    
    // Step 3
    self.step3VC = [[UIStoryboard storyboardWithName:@"Registration" bundle:nil] instantiateViewControllerWithIdentifier:@"RegistrationStep3ViewController"];
    self.step3VC.didConfirm = ^{
        [weakSelf login];
    };
    self.step3VC.didTapConfirmLater = ^{
        [weakSelf login];
    };
}

- (void)login {
    [self enterTheBank];
}

- (void)enterTheBank {

    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserRegistered object:nil];
    if (self.fromNews) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }

    MainViewController *main = [[MainViewController alloc] init];
    UINavigationController *nc = [UIHelper defaultNavigationController];
    nc.viewControllers = @[main];
    LGSideMenuController *sideMenuController = kSideMenuController;

    sideMenuController.rootViewController = nc;
    [sideMenuController hideRightViewAnimated:YES completionHandler:nil];
}

- (void)cleanCache {
    // remove data from cache
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[RequestsCache sharedCache] removeAllObjects];
    });
}

- (void)goToStep2 {
    self.backButton.hidden = false;
    
    self.step2VC.phone = self.step1VC.phone;
    self.step2VC.email = self.step1VC.email;
    self.step2VC.firstName = self.step1VC.firstName;
    self.step2VC.lastName = self.step1VC.lastName;
    self.step2VC.middleName = self.step1VC.middleName;
    
    self.currentPage++;
    [self updatePageTitle];
    [self cycleFromViewController:self.step1VC toViewController:self.step2VC forward:true];
}

- (void)goToStep3 {
    self.backButton.hidden = true;
    
    self.currentPage++;
    [self updatePageTitle];
    [self cycleFromViewController:self.step2VC toViewController:self.step3VC forward:true];
}

- (void)goStepBack {
    switch (self.currentPage) {
        case 2:
        {
            self.currentPage--;
            [self updatePageTitle];
            
            [self cycleFromViewController:self.step3VC toViewController:self.step2VC forward:false];
        }
            break;
        case 1:
        {
            self.backButton.hidden = true;
            
            self.currentPage--;
            [self updatePageTitle];
            
            [self cycleFromViewController:self.step2VC toViewController:self.step1VC forward:false];
        }
        default:
            break;
    }
}

- (void)cycleFromViewController:(UIViewController *)oldVC
               toViewController:(UIViewController *)newVC
                        forward:(BOOL)forward {
    NSInteger direction = forward ? -1 : 1;
    
    [oldVC willMoveToParentViewController:nil];
    [self addChildViewController:newVC];
    
    UIView *toView = newVC.view;
    UIView *fromView = oldVC.view;
    
    [self.containerView addSubview:toView];
    
    if (self.toViewTopConstraint) {
        [self.toViewTopConstraint autoRemove];
    }
    if (self.toViewBottomConstraint) {
        [self.toViewBottomConstraint autoRemove];
    }
    if (self.toViewLeftConstraint) {
        [self.toViewLeftConstraint autoRemove];
    }
    if (self.toViewRightConstraint) {
        [self.toViewRightConstraint autoRemove];
    }
    self.toViewTopConstraint = [toView autoPinEdgeToSuperviewEdge:ALEdgeTop];
    self.toViewBottomConstraint = [toView autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    self.toViewLeftConstraint = [toView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:-direction * [ASize screenWidth]];
    self.toViewRightConstraint = [toView autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:direction * [ASize screenWidth]];

    if (self.fromViewTopConstraint) {
        [self.fromViewTopConstraint autoRemove];
    }
    if (self.fromViewBottomConstraint) {
        [self.fromViewBottomConstraint autoRemove];
    }
    if (self.fromViewLeftConstraint) {
        [self.fromViewLeftConstraint autoRemove];
    }
    if (self.fromViewRightConstraint) {
        [self.fromViewRightConstraint autoRemove];
    }
    self.fromViewTopConstraint = [fromView autoPinEdgeToSuperviewEdge:ALEdgeTop];
    self.fromViewBottomConstraint = [fromView autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    self.fromViewLeftConstraint = [fromView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    self.fromViewRightConstraint = [fromView autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
    
    [self.containerView layoutIfNeeded];
    
    [self transitionFromViewController:oldVC toViewController:newVC
                              duration:0.25 options:0
                            animations:^{
                                if (self.fromViewLeftConstraint) {
                                    [self.fromViewLeftConstraint autoRemove];
                                }
                                if (self.fromViewRightConstraint) {
                                    [self.fromViewRightConstraint autoRemove];
                                }
                                [fromView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:direction * [ASize screenWidth]];
                                [fromView autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:-direction * [ASize screenWidth]];
                                
                                self.fromViewTopConstraint = self.toViewTopConstraint;
                                self.fromViewBottomConstraint = self.toViewBottomConstraint;
                                if (self.toViewLeftConstraint) {
                                    [self.toViewLeftConstraint autoRemove];
                                }
                                if (self.toViewRightConstraint) {
                                    [self.toViewRightConstraint autoRemove];
                                }
                                self.fromViewLeftConstraint = [toView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
                                self.fromViewRightConstraint = [toView autoPinEdgeToSuperviewEdge:ALEdgeRight];
                                
                                [self.containerView layoutIfNeeded];
                            }
                            completion:^(BOOL finished) {
                                [newVC didMoveToParentViewController:self];
                                [fromView removeFromSuperview];
                                [oldVC removeFromParentViewController];
                            }];
}

- (void)updatePageTitle {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"Шаг %ld из %ld", (long)self.currentPage + 1, (long)STEPS_COUNT] style:UIBarButtonItemStylePlain target:nil action:nil];
}

#pragma mark - Config UI

- (void)configUI {
    self.title = NSLocalizedString(@"ga_registration", nil);
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNavigationBackButton];
    [self setBafBackground];

    [self setNavigationBackButton];
    [self buildRegistrationForm];
    [self updatePageTitle];
    if (self.fromNews) [self setNavigationCloseButtonAtLeft:YES];
}

@end
