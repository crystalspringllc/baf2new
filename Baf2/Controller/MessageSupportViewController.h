//
//  MessageSupportViewController.h
//  Baf2
//
//  Created by nmaksut on 19.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <JSQMessagesViewController/JSQMessagesViewController.h>

@interface MessageSupportViewController : JSQMessagesViewController
@property(nonatomic, strong) UIView *disclamer;
@end
