//
// Created by Askar Mustafin on 4/10/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import "PaymentsQuickViewController.h"
#import "PaymentFormViewController.h"
#import "Contract.h"
#import "PaymentQuickCell.h"
#import "ProviderCategory.h"
#import "PaymentsViewController.h"
#import "ContractsApi.h"
#import "ProvidersApi.h"
#import "PaymentProvidersByCategory.h"
#import "ScrollDirectionsHelper.h"
#import "NotificationCenterHelper.h"
#import "AlsekoReceiptViewController.h"
#import "ContractModelView.h"
#import "AstanaRefreshControl.h"
#import "MDButton.h"
#import <UIScrollView+EmptyDataSet.h>
#import <IQKeyboardManager.h>

@interface PaymentsQuickViewController () <UISearchBarDelegate,
        DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,
        UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *allPaymentsButtonBottomConstraint;
@property (nonatomic, strong) MDButton *allPaymentsButton;
@property (nonatomic, strong) AstanaRefreshControl *astanaRefreshControl;
@property (nonatomic, strong) ScrollDirectionsHelper *scrollDirectionHelper;

@property (nonatomic, strong) NSMutableArray *contracts;
@property (nonatomic, strong) NSArray *paymentProvidersWithCategories;


@property (nonatomic, strong) NSMutableArray *providerCategories;
@property (nonatomic, strong) NSDictionary *contractsByCategories;
@property (nonatomic, strong) NSMutableDictionary *textFieldValues;
@property (nonatomic) CGFloat currentKeyboardHeight;

@end

@implementation PaymentsQuickViewController

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Payments" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PaymentsQuickViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textFieldValues = [NSMutableDictionary new];
        
    [self configUI];
    [self loadPayments:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    if ([ContractModelView sharedInstance].needsReload) {
        [self loadPayments:false];
        [[ContractModelView sharedInstance] setNeedsReload:false];
    } else if ([ContractModelView sharedInstance].didLoadPaymentTemplates) {
        self.contracts = [ContractModelView sharedInstance].contracts.mutableCopy;
        [self reloadData];
    }
    
    [IQKeyboardManager sharedManager].previousNextDisplayMode = true;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [IQKeyboardManager sharedManager].previousNextDisplayMode = false;
}

#pragma mark - API

- (void)loadPayments:(BOOL)refresh {
    __weak PaymentsQuickViewController *weakSelf = self;

    if (!refresh) {
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading_payment_template_and_providers", nil) animated:YES];
    }
    
    [[ContractModelView sharedInstance] loadContracts:^(BOOL success, NSError *error) {
        NSUserDefaults *sud = [NSUserDefaults standardUserDefaults];
        [ProvidersApi getProvidersWithCategoriesByLocation:[sud objectForKey:kUserSelectedCountry]
                                                      city:[sud objectForKey:kUserSelectedCity]
                                                   success:^(NSArray *paymentProvidersByCategories) {

                                                       self.paymentProvidersWithCategories = paymentProvidersByCategories;

                                                       if (refresh) {
                                                           [self.astanaRefreshControl finishingLoading];
                                                       } else {
                                                           [MBProgressHUD hideAstanaHUDAnimated:YES];
                                                       }
                                                       
                                                       [weakSelf reloadData];
                                                       
                                                   } failure:^(NSString *code, NSString *message) {
                    [MBProgressHUD hideAstanaHUDAnimated:YES];
                }];
    }];

}

- (void)reloadPayments {
    [self loadPayments:true];
}

- (void)onPaymentTemplatesUpdate {
    [self loadPayments:false];
}

- (void)deleteContract:(NSNotification *)notification {
    self.contracts = [ContractModelView sharedInstance].contracts.mutableCopy;

    [self reloadData];
}

- (void)reloadData {
    self.contracts = [ContractModelView sharedInstance].contracts.mutableCopy;

    if (!self.contracts || self.contracts.count == 0) {
        self.tableView.tableHeaderView = nil;
    } else {
        self.tableView.tableHeaderView = self.headerView;
    }
    [self groupData:nil];
}

#pragma mark - Actions

- (IBAction)clickAllPaymentsButton:(id)sender {
    PaymentsViewController *v = [[PaymentsViewController alloc] init];
    v.contracts = self.contracts.mutableCopy;
    [self.navigationController pushViewController:v animated:YES];
}

- (void)payForContract:(Contract *)contract indexPath:(NSIndexPath *)indexPath collectionViewIndexPath:(NSIndexPath *)collectionViewIndexPath inputAmount:(NSNumber *)inputAmount {
    NSString *key = [NSString stringWithFormat:@"%ld%ld%ld%ld", (long)indexPath.section, (long)indexPath.row, (long)collectionViewIndexPath.section, (long)collectionViewIndexPath.item];

    if (contract.provider.infoRequestType.integerValue == 2) {

        AlsekoReceiptViewController *vc = [AlsekoReceiptViewController createVC];
        vc.contract = contract;
        vc.initialAmount = self.textFieldValues[key];;
        [self.navigationController pushViewController:vc animated:YES];

    } else {

        PaymentFormViewController *vc = [PaymentFormViewController createVC];
        vc.actionType = ActionTypeMakePayment;
        vc.contract = contract;
        vc.inputAmount = inputAmount;
        [self.navigationController pushViewController:vc animated:YES];

    }
}

#pragma mark - tableview delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.contracts.count == 0 && [ContractModelView sharedInstance].didLoadPaymentTemplates) {
        return 1;
    }
    return self.providerCategories.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.contracts.count == 0 && [ContractModelView sharedInstance].didLoadPaymentTemplates) {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"EmptyHeaderCell"];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        return cell;
    }
    
    PaymentQuickCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"PaymentQuickCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSArray *contracts = [self contractsForCategoryInSection:indexPath.section];
    [cell setContracts:contracts];
    
    NSMutableDictionary *textFieldValues = [NSMutableDictionary new];
    for (int i = 0; i < self.textFieldValues.allKeys.count; i++) {
        NSString *key = self.textFieldValues.allKeys[i];
        if ([key hasPrefix:[NSString stringWithFormat:@"%ld%ld", (long)indexPath.section, (long)indexPath.row]]) {
            textFieldValues[key] = self.textFieldValues[key];
        }
    }
    cell.textFieldValues = textFieldValues;

    __weak PaymentsQuickViewController *weakSelf = self;
    
    cell.initialAmountChanged = ^(UITextField *textField, NSIndexPath *collectionViewIndexPath) {
        NSString *key = [NSString stringWithFormat:@"%ld%ld%ld%ld", (long)indexPath.section, (long)indexPath.row, (long)collectionViewIndexPath.section, (long)collectionViewIndexPath.item];
        weakSelf.textFieldValues[key] = textField.text;
        [weakSelf.tableView reloadData];
    };
    cell.payButtonTapped = ^(Contract *contract, NSIndexPath *collectionViewIndexPath, NSNumber *inputAmount) {
        [weakSelf payForContract:contract
                       indexPath:indexPath
         collectionViewIndexPath:collectionViewIndexPath
                     inputAmount:inputAmount];
    };
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];

   
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.providerCategories.count == 0 && [ContractModelView sharedInstance].didLoadPaymentTemplates) {
        return nil;
    }
    
    UIView *headerViewContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
    headerViewContainer.backgroundColor = [UIColor clearColor];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(12, 0, [UIScreen mainScreen].bounds.size.width - 12, 30)];
    headerView.backgroundColor = [UIColor clearColor];
    
    NSString *categoryName = self.providerCategories[(NSUInteger) section];
    
    UILabel *headerTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 8, headerView.frame.size.width - 13, 16)];
    headerTitleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    headerTitleLabel.textColor = [UIColor fromRGB:0x4c4c4c];
    headerTitleLabel.text = categoryName;
    [headerView addSubview:headerTitleLabel];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 29, headerView.frame.size.width, 1)];
    separatorView.backgroundColor = [[UIColor fromRGB:0x989898] colorWithAlphaComponent:0.5];
    [headerView addSubview:separatorView];
    
    [headerViewContainer addSubview:headerView];
    
    return headerViewContainer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.providerCategories.count == 0 && [ContractModelView sharedInstance].didLoadPaymentTemplates) {
        return UITableViewAutomaticDimension;
    }

    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (self.providerCategories.count == 0 && [ContractModelView sharedInstance].didLoadPaymentTemplates) {
        return UITableViewAutomaticDimension;
    }
    
    return 0.001;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.tableView) {
        [self.astanaRefreshControl scrollViewDidScroll:scrollView];
        
        [self.scrollDirectionHelper scrollAutolayoutView:self.allPaymentsButton scrollView:scrollView];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.tableView) {
        [self.astanaRefreshControl scrollViewWillEndDragging];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self groupData:searchText];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self groupData:searchBar.text];
    [searchBar resignFirstResponder];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"no_template_for_your_request", nil) attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14], NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return -(self.currentKeyboardHeight)/2.0;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return true;
}

#pragma mark - Keyboard

- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    
    [self updateConstraintsAccordingKeyboardInfo:info];
}

- (void)keyboardWillHide:(NSNotification*)notification {
    self.currentKeyboardHeight = 0;
    
    [self.tableView reloadEmptyDataSet];
}

- (void)updateConstraintsAccordingKeyboardInfo:(NSDictionary *)info {
    NSValue *keyboardFrameBegin = [info valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [keyboardFrameBegin CGRectValue];
    self.currentKeyboardHeight = keyboardFrame.size.height;
    
    [self.tableView reloadEmptyDataSet];
}

#pragma mark - Config UI

- (void)configUI {
    [self setBafBackground];

    [self hideBackButtonTitle];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    [self.allPaymentsButton setImage:[UIImage imageNamed:@"icon_all_payments"] forState:UIControlStateNormal];
    
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 80, 0);
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 147;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.astanaRefreshControl = [AstanaRefreshControl new];
    self.astanaRefreshControl.target = self;
    self.astanaRefreshControl.action = @selector(reloadPayments);
    [self.tableView insertSubview:self.astanaRefreshControl atIndex:0];
    
    [self.allPaymentsButtonBottomConstraint autoRemove];
    
    self.allPaymentsButton = [[MDButton alloc] initWithFrame:CGRectMake(0, 0, 55, 55) type:2 rippleColor:[UIColor colorWithWhite:0.5 alpha:1]];
    self.allPaymentsButton.backgroundColor = [UIColor mainButtonColor];
    [self.allPaymentsButton setImage:[UIImage imageNamed:@"icon_six_dots"] forState:UIControlStateNormal];
    self.allPaymentsButton.type = 2;
    [self.allPaymentsButton addTarget:self action:@selector(clickAllPaymentsButton:) forControlEvents:UIControlEventTouchUpInside];
    self.allPaymentsButton.translatesAutoresizingMaskIntoConstraints = false;
    [self.view addSubview:self.allPaymentsButton];
    [self.allPaymentsButton autoSetDimensionsToSize:CGSizeMake(55, 55)];
    [self.allPaymentsButton autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:20];
    
    self.scrollDirectionHelper = [ScrollDirectionsHelper new];
    [self.scrollDirectionHelper setBottomConstraint:self.allPaymentsButton offset:20];
    
    self.searchBar.tintColor = [UIColor appBlueColor];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self]; // sometimes dealloc not called, therefore remove previous observers if exist

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPaymentTemplatesUpdate) name:kNotificationPaymentTemplatesUpdated object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteContract:) name:kNotificationPaymentTemplateDeleted object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:kNotificationPaymentTemplatesNeedsReload object:nil];
}

#pragma mark - Helpers

- (void)groupData:(NSString *)filterText {

    self.providerCategories = [NSMutableArray new];
    NSMutableDictionary *data = [NSMutableDictionary new];

    NSArray *filteredContracts = self.contracts;
    if (filterText && filterText.length > 0) {
        NSPredicate *filterContractsPredictate = [NSPredicate predicateWithFormat:@"alias contains[cd] %@ OR contract contains[cd] %@", filterText, filterText];
        filteredContracts = [self.contracts filteredArrayUsingPredicate:filterContractsPredictate];
    }
    
    for (PaymentProvidersByCategory *ppbc in self.paymentProvidersWithCategories) {

        NSString *categoryName = ppbc.category.categoryName;
        
        NSMutableArray *contracts = [[NSMutableArray alloc] init];

            for (PaymentProvider *paymentProvider in ppbc.paymentProviderListWS) {

                for (Contract *contract in filteredContracts) {

                    if ([paymentProvider.paymentProviderId isEqualToNumber:contract.provider.paymentProviderId]) {

                        if (![self.providerCategories containsObject:categoryName]) {
                            [self.providerCategories addObject:categoryName];
                        }
                        [contracts addObject:contract];

                    }
                }

            }

        if (contracts.count > 0) {
            data[categoryName] = contracts;
        }
    }

    self.contractsByCategories = [NSMutableDictionary dictionaryWithDictionary:data];
    
    [self.tableView reloadData];
}

- (NSArray *)contractsForCategoryInSection:(NSInteger)section {
    NSString *categoryName = self.providerCategories[(NSUInteger) section];
    return self.contractsByCategories[categoryName];
}

#pragma mark -

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
