//
//  P2PTransferViewController.m
//  BAF
//
//  Created by Askar Mustafin on 1/18/16.
//  Copyright © 2016 Банк Астаны. All rights reserved.
//

#import "P2PTransferViewController.h"
#import "P2PBrowserViewController.h"
#import "NewTransfersApi.h"
#import "MainButton.h"
#import "UITableViewController+Extension.h"
#import "P2PInitResponse.h"

@interface P2PTransferViewController ()
@property (weak, nonatomic) IBOutlet MainButton *nextButton;
@end

@implementation P2PTransferViewController

+ (id)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Transfers" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([P2PTransferViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - Actions

- (IBAction)clickNextButton:(id)sender {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"please_wait", nil) animated:true];

    [NewTransfersApi initPayboxWithSuccess:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        P2PInitResponse *p2PInitResponse = [P2PInitResponse instanceFromDictionary:response];
        
        if (p2PInitResponse) {
            P2PBrowserViewController *vc = [P2PBrowserViewController new];
            vc.p2PInitResponse = p2PInitResponse;
            vc.isNew = NO;
            [self.navigationController pushViewController:vc animated:true];
        }

    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [[UIColor appDisclaimerYellowColor] colorWithAlphaComponent:0.5];
    
    return cell;
}

#pragma mark - Config UI

- (void)configUI {
    [self setBAFTableViewBackground];
    [self.nextButton setMainButtonStyle:MainButtonStyleOrange];
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"from_card_to_card", nil)];

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 200;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.nextButton.title = NSLocalizedString(@"next", nil);
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
