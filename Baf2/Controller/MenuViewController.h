//
//  MenuViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 4/23/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PaymentsViewController;

@interface MenuViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

- (void)selectLoginVC;

@end
