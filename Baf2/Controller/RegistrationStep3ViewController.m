//
//  RegistrationStep3ViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 26.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "RegistrationStep3ViewController.h"
#import "MainButton.h"
#import "AccountActivationActionSheetView.h"
#import "TitleWithView.h"
#import "MultilineLabel.h"
#import "AuthApi.h"
#import "UITableViewController+Extension.h"

@interface RegistrationStep3ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *smsCodeTextField;
@property (weak, nonatomic) IBOutlet MainButton *submitSMSCodeButton;

@property (weak, nonatomic) IBOutlet UITextField *emailCodeTextField;
@property (weak, nonatomic) IBOutlet MainButton *submitEmailCodeButton;

@property (weak, nonatomic) IBOutlet MainButton *resendSMSCodeButton;
@property (weak, nonatomic) IBOutlet MainButton *resendEmailCodeButton;

@end

@implementation RegistrationStep3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - Actions

- (void)confirmLater:(MainButton *)sender {
    if (self.didTapConfirmLater) {
        self.didTapConfirmLater();
    }
}

- (void)confirmSMSCode:(MainButton *)sender {
    [self.smsCodeTextField resignFirstResponder];
    [self.emailCodeTextField resignFirstResponder];
    
    NSString *code = [self.smsCodeTextField text];
    if(code.length == 0) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"fill_field", nil) message:NSLocalizedString(@"enter_code_for_verify_phone_number", nil)];
        return;
    }
    
    __weak RegistrationStep3ViewController *wSelf = self;
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"acceptance", nil) animated:true];
    
    [AuthApi checkSMSCode:code success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        // Phone number confirmed
        [[User sharedInstance] setIsPhoneConfirmed:YES];
        [Toast showToast:NSLocalizedString(@"phone_number_not_verified", nil)];
        
        if ([[User sharedInstance] isEmailConfirmed]) {
            
            if (wSelf.didConfirm) {
                wSelf.didConfirm();
            }
            
            if (self.isModal) {
                [self dismissViewControllerAnimated:true completion:nil];
            }
            
        } else {
            
            
        }
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
//        AccountActivationActionSheetView *asv = [[AccountActivationActionSheetView alloc] initWithTitle:@"Вы ввели неверный код подтверждения, возможно истек срок действия."];
//        asv.tag = 123;
//        
//        TitleWithView *tv = [[TitleWithView alloc] initWithFrame:CGRectMake(0, 0, [asv contentViewWidth], 1) title:@"Указанный Вами номер телефона:"];
//        [tv setLabelWithText:[[User sharedInstance] phoneNumber]];
//        [asv insertPrepareContentSubview:tv];
//        
//        MainButton *tryButtonTel = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, [asv contentViewWidth], 40)];// onTap:^(UIView *_self) {
//        [tryButtonTel addTarget:wSelf action:@selector(resendTelTapped) forControlEvents:UIControlEventTouchUpInside];
//        tryButtonTel.mainButtonStyle = MainButtonStyleOrange;
//        tryButtonTel.frame = CGRectMake(0, 0, [asv contentViewWidth], 40);
//        tryButtonTel.title = @"Отправить код повторно";
//        [asv insertPrepareContentSubview:tryButtonTel];
//        
//        [asv setContentView];
//        [asv show];
    }];
}

- (void)confirmEmailCode:(MainButton *)sender {
    [self.smsCodeTextField resignFirstResponder];
    [self.emailCodeTextField resignFirstResponder];
    
    __weak RegistrationStep3ViewController *wSelf = self;

    NSString *emailCode = [self.emailCodeTextField text];
    if (emailCode.length == 0) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"fill_field", nil) message:NSLocalizedString(@"enter_code_for_verify_email", nil)];
        return;
    }
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"acceptance", nil) animated:true];
    
    [AuthApi checkEmailCode:emailCode success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        // Email confirmed
        [[User sharedInstance] setIsEmailConfirmed:YES];
        [Toast showToast:NSLocalizedString(@"email_verified", nil)];
        
        if ([[User sharedInstance] isPhoneConfirmed]) {
            
            if (wSelf.didConfirm) {
                wSelf.didConfirm();
            }
            
            if (self.isModal) {
                [self dismissViewControllerAnimated:true completion:nil];
            }
            
        }
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

- (void)resendEmailTapped {
    [self.smsCodeTextField resignFirstResponder];
    [self.emailCodeTextField resignFirstResponder];
    
    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
    
    __weak AccountActivationActionSheetView *weakActionSheetView = [self actionSheetView];
    
    [AuthApi sendEmailCodeWithSuccess:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        // Confirmation email sent
        [weakActionSheetView hide];
        
        //        [GAN sendEventWithCategory:weakTitle action:@"Отправлена почта для активации email"];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        //        [GAN sendEventWithCategory:weakTitle action:[NSString stringWithFormat:@"Ошибка sendEmailActivationEmail: %@", message]];
    }];
}

- (void)resendTelTapped {
    [self.smsCodeTextField resignFirstResponder];
    [self.emailCodeTextField resignFirstResponder];
    
    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
    
    __weak AccountActivationActionSheetView *weakActionSheetView = [self actionSheetView];
    
    [AuthApi sendSMSCodeWithSuccess:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        // SMS sent
        [weakActionSheetView hide];
        
        //        [GAN sendEventWithCategory:weakTitle action:@"Отправлен SMS для активации телефона"];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        //        [GAN sendEventWithCategory:weakTitle action:[NSString stringWithFormat:@"Ошибка sendTelActivationSMS: %@", message]];
    }];
}

- (void)didNotGetCodeButtonTapped {
    AccountActivationActionSheetView *asv = [[AccountActivationActionSheetView alloc] initWithTitle:NSLocalizedString(@"not_get_code_on_email_or_phone", nil)];
    asv.tag = 123;
    
    if (![[User sharedInstance] isEmailConfirmed]) {
        MultilineLabel *t1 = [[MultilineLabel alloc] initWithTitle:NSLocalizedString(@"check_maybe_mail_in_spam", nil) width:[asv contentViewWidth]];
        [asv insertPrepareContentSubview:t1];
        
        TitleWithView *tv = [[TitleWithView alloc] initWithFrame:CGRectMake(0, 0, [asv contentViewWidth], 1) title:NSLocalizedString(@"your_email_address", nil)];
        [tv setLabelWithText:[[User sharedInstance] email]];
        [asv insertPrepareContentSubview:tv];
        
        MainButton *tryButtonEmail = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, [asv contentViewWidth], 40)];
        tryButtonEmail.mainButtonStyle = MainButtonStyleOrange;
        tryButtonEmail.frame = CGRectMake(0, 0, [asv contentViewWidth], 40);
        tryButtonEmail.title = NSLocalizedString(@"send_code_again", nil);
        [tryButtonEmail addTarget:self action:@selector(resendEmailTapped) forControlEvents:UIControlEventTouchUpInside];
        [asv insertPrepareContentSubview:tryButtonEmail];
    }
    
    if (![[User sharedInstance] isPhoneConfirmed]) {
        [asv insertSeparator];
        
        TitleWithView *tv2 = [[TitleWithView alloc] initWithFrame:CGRectMake(0, 0, [asv contentViewWidth], 1) title:NSLocalizedString(@"mentioned_phone_number", nil)];
        [tv2 setLabelWithText:[[User sharedInstance] phoneNumber]];
        [asv insertPrepareContentSubview:tv2];
        
        MainButton *tryButtonTel = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, [asv contentViewWidth], 40)];
        tryButtonTel.mainButtonStyle = MainButtonStyleOrange;
        tryButtonTel.frame = CGRectMake(0, 0, [asv contentViewWidth], 40);
        tryButtonTel.title = NSLocalizedString(@"send_code_again", nil);
        [tryButtonTel addTarget:self action:@selector(resendTelTapped) forControlEvents:UIControlEventTouchUpInside];
        [asv insertPrepareContentSubview:tryButtonTel];
    }
    
    [asv setContentView];
    [asv show];
}

#pragma mark - UITableViewDataSource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        if ([User sharedInstance].isPhoneConfirmed) {
            return @"";
        }
        return NSLocalizedString(@"code_from_sms", nil);
    } else if (section == 1) {
        if ([User sharedInstance].isEmailConfirmed) {
            return @"";
        }
        return NSLocalizedString(@"code_from_email", nil);
    }
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section == 0) {
        if ([User sharedInstance].isPhoneConfirmed) {
            return @"";
        }
        if (self.isModal) {
            return @"Запросите код повторно";
        }
        return [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"code_send_on_number", nil), [User sharedInstance].phoneNumber];
    } else if (section == 1) {
        if ([User sharedInstance].isEmailConfirmed) {
            return @"";
        }
        if (self.isModal) {
            return @"Запросите код повторно";
        }
        return [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"code_send_on_email", nil), [User sharedInstance].email];
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if ([User sharedInstance].isPhoneConfirmed) {
            return 0;
        } else {
            return UITableViewAutomaticDimension;
        }
    } else if (indexPath.section == 1) {
        if ([User sharedInstance].isEmailConfirmed) {
            return 0;
        } else {
            return UITableViewAutomaticDimension;
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    cell.clipsToBounds = YES;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

#pragma mark - Helpers

- (AccountActivationActionSheetView *)actionSheetView {
    AccountActivationActionSheetView *v = (AccountActivationActionSheetView *)[[[[UIApplication sharedApplication] delegate] window] viewWithTag:123];
    return v;
}

#pragma mark - Config UI

- (void)configUI {
    [self setNavigationBackButton];
    [self setBAFTableViewBackground];

    if (self.isModal) {
        [self setNavigationTitle:NSLocalizedString(@"accept_account", nil)];
        [self setNavigationCloseButtonAtLeft:false];
    }

    self.smsCodeTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.emailCodeTextField.keyboardType = UIKeyboardTypeNumberPad;
    
    self.submitSMSCodeButton.title = NSLocalizedString(@"confirm", nil);
    [self.submitSMSCodeButton addTarget:self action:@selector(confirmSMSCode:) forControlEvents:UIControlEventTouchUpInside];
    
    self.submitEmailCodeButton.title = NSLocalizedString(@"confirm", nil);
    [self.submitEmailCodeButton addTarget:self action:@selector(confirmEmailCode:) forControlEvents:UIControlEventTouchUpInside];
    
    self.resendSMSCodeButton.title = NSLocalizedString(@"ask_sms_code_again", nil);
    [self.resendSMSCodeButton addTarget:self action:@selector(resendTelTapped) forControlEvents:UIControlEventTouchUpInside];
    
    self.resendEmailCodeButton.title = NSLocalizedString(@"ask_email_code_again", nil);
    [self.resendEmailCodeButton addTarget:self action:@selector(resendEmailTapped) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.smsCodeTextField.placeholder = NSLocalizedString(@"enter_code_from_sms", nil);
    self.emailCodeTextField.placeholder = NSLocalizedString(@"enter_code_from_email", nil);


    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 60;


    if ([User sharedInstance].isPhoneConfirmed) {
        self.resendSMSCodeButton.hidden = YES;
    }
    if ([User sharedInstance].isEmailConfirmed) {
        self.resendEmailCodeButton.hidden = YES;
    }
}

@end
