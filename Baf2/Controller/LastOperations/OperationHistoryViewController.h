//
//  OperationHistoryViewController.h
//  Myth
//
//  Created by Almas Adilbek on 12/25/12.
//
//

#import <UIKit/UIKit.h>
#import "UIScrollView+EmptyDataSet.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface OperationHistoryViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource>

@property (nonatomic, strong) NSArray *operationList;

@end
