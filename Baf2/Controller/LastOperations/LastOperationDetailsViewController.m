//
//  LastOperationDetailsViewController.m
//  BAF
//
//  Created by Almas Adilbek on 1/20/15.
//
//

#import "LastOperationDetailsViewController.h"
#import "OperationsApi.h"
#import "AATableSection.h"
#import "OperationDetailsHeaderCell.h"
#import "AASimpleTableViewCell.h"
#import "UIViewController+NavigationController.h"
#import "NewTransfersApi.h"
#import "NewCheckAndInitTransferResponse.h"
#import "TransferAmountCellTableViewCell.h"
#import "MainButton.h"
#import "UIView+AAPureLayout.h"
#import "NotificationCenterHelper.h"
#import "UIColor+Extensions.h"

#define kSectionDetails @"details"
#define kSectionProviderInfo @"providerInfo"
#define kSectionTransferInfo @"transferInfo"
#define kSectionLogsButton @"LogsButton"

@interface LastOperationDetailsViewController ()
@property (nonatomic, strong) OperationHistoryDetails *operationDetails;
@end

@implementation LastOperationDetailsViewController {
    UITableView *list;
    NSMutableArray *sections;
    NewCheckAndInitTransferResponse *transferResponse;
    MainButton *toTransferButton;
    MainButton *toPaymentsButton;
    UIView *footerButtonView;
    BOOL *transferError;
}

-(id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

-(void)initVars {
    sections = [[NSMutableArray alloc] init];
    transferError = NO;
}

- (void)goBack {
    if (self.operation) {
        [super goBack];
    } else {
        [self popToRoot:NO];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];

    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
    if(self.newVersion) {
    [NewTransfersApi getTransferResultFromWebWithServiceLogID:self.operationId
                                                      success:^(id response) {
                                                          [MBProgressHUD hideAstanaHUDAnimated:true];
                                                          transferResponse = [NewCheckAndInitTransferResponse instanceFromDictionary:response];
                                                          [self onResponse];
                                                      } failure:^(NSString *code, NSString *message) {
                                                          [MBProgressHUD hideAstanaHUDAnimated:true];
                                                      }];
    }else{
    [OperationsApi getInfoForDetailedOrdering: self.operation ? self.operation.operationId : self.operationId  success:^(OperationHistoryDetails *details) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        self.operationDetails = details;

        if ([self.operationDetails.processingCode isEqualToString:@"DEPOSIT_OPEN"]) {

        }
        [self onResponse];


    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
    }
}


- (void)goTransfersTapped
{
    [self popToRoot];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenNewTransfers object:nil];
}

- (void)goPaymentsTapped
{
    [self popToRoot];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenPayments object:nil];
}

- (void)onDepositResponse {
    NSMutableArray *detailRows = [NSMutableArray array];
    AATableSection *section1 = [[AATableSection alloc] initWithSectionId:kSectionDetails];
    section1.headerTitle = [NSString stringWithFormat:NSLocalizedString(@"detail_operation", nil)];

    AATableSection *section0 = [[AATableSection alloc] initWithSectionId:kSectionProviderInfo];
    section0.rows = [@[@"row"] mutableCopy];
    [sections addObject:section0];
    
    if (self.operation.operationId || self.operationId) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"operation_id_", nil) value:[NSString stringWithFormat:@"%@", self.operation ? self.operation.operationId : self.operationId]]];
    }

    [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"date_payment", nil) value:[self.operationDetails dateNice]]];


    if (self.operationDetails.reference.length > 0) {
        [detailRows addObject:[self rowWithTitle:@"ID операции с SIC" value:self.operationDetails.reference]];
    }


    if (self.operation.operationId != nil) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"operation_id_", nil) value:self.operation.operationId]];
    }


    section1.rows = detailRows;
    [sections addObject:section1];

    [list reloadData];
    list.hidden = NO;
}

- (void)onResponse
{
    NSMutableArray *detailRows = [NSMutableArray array];
    AATableSection *section1 = [[AATableSection alloc] initWithSectionId:kSectionDetails];
    section1.headerTitle = [NSString stringWithFormat:NSLocalizedString(@"detail_operation", nil)];

    AATableSection *section0 = [[AATableSection alloc] initWithSectionId:kSectionProviderInfo];
    section0.rows = [@[@"row"] mutableCopy];
    [sections addObject:section0];
    
    if(self.newVersion)
    {
    if (transferResponse.transferInfo.serviceId || self.operationId) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"operation_id_", nil) value:[NSString stringWithFormat:@"%@", transferResponse ? transferResponse.transferInfo.serviceId : self.operationId]]];
    }
    
    if (transferResponse.transferInfo.reference.length > 0) {
        [detailRows addObject:[self rowWithTitle:@"Референс" value:transferResponse.transferInfo.reference]];
    }
    
    [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"date_payment", nil) value:transferResponse.transferInfo.date]];
    
    [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"Статус:", nil) value:transferResponse.transferInfo.statusText]];
   
    if([transferResponse.transferInfo.status isEqualToString:@"ERROR"])
    {
        transferError = YES;
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"Причина:", nil) value:[transferResponse.messages objectForKey:@"error"]]];
    }else if([transferResponse.transferInfo.status isEqualToString:@"AML_TEST_QUEUE"])
    {
        transferError = YES;
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"Причина:", nil) value:@"Отказано. Просьба обратиться в ближайшее отделение банка или колл-центр"]];
    }
    
    // from account card number
    
    if (transferResponse.requestor.account.key.length > 0) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"account_of_outcomee", nil) value:transferResponse.requestor.account.key]];
    } else if (transferResponse.requestor.account.number && transferResponse.requestor.account.number.length > 0) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"account_of_outcomee", nil) value:transferResponse.requestor.account.number]];
    }
    
    if (transferResponse.amountInfo.finalAmount) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"Списать", nil) value:[NSString stringWithFormat:@"%@ %@", [transferResponse.amountInfo.finalAmount decimalFormatString], transferResponse ? transferResponse.amountInfo.currency : @"KZT"]]];
    }
    
    // to account card number
      if (transferResponse.destination.account.key.length > 0) {
     [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"account_of_incomee", nil) value:transferResponse.destination.account.key]];
     } else if (transferResponse.destination.account.number.length > 0) {
     [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"account_of_incomee", nil) value:transferResponse.destination.account.number]];
     }
    
    if (transferResponse.amountInfo.finalAmount) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"Зачислить", nil) value:[NSString stringWithFormat:@"%@ %@", [transferResponse.amountInfo.finalAmount decimalFormatString], transferResponse ? transferResponse.amountInfo.currency : @"KZT"]]];
    }
    
    if ([transferResponse.knpObject objectForKey:@"name"]) {
        [detailRows addObject:[self rowWithTitle:@"Назначение платежа" value:[transferResponse.knpObject objectForKey:@"name"]]];
    }
    
    
    }else{
//    [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"card", nil) value:self.operationDetails.cardNumber]];

    // from account card number
        
         [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"date_payment", nil) value:[self.operationDetails dateNice]]];
    
        
     if (self.operationDetails.transfer.requestorAccount.key.length > 0) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"account_of_outcomee", nil) value:self.operationDetails.transfer.requestorAccount.key]];
    } else if (self.operationDetails.transfer.requestorAccount.number && self.operationDetails.transfer.requestorAccount.number.length > 0) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"account_of_outcomee", nil) value:self.operationDetails.transfer.requestorAccount.number]];
    } else if (self.operationDetails.cardNumber.length > 0) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"card", nil) value:self.operationDetails.cardNumber]];
    }

    // to account card number
    if (self.operationDetails.transfer.destinationAccount.key.length > 0) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"account_of_incomee", nil) value:self.operationDetails.transfer.destinationAccount.key]];
    } else if (self.operationDetails.transfer.destinationAccount.number.length > 0) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"account_of_incomee", nil) value:self.operationDetails.transfer.destinationAccount.number]];
    }


    if (self.operationDetails.transfer.knpName.length > 0) {
        [detailRows addObject:[self rowWithTitle:@"Назначение платежа" value:self.operationDetails.transfer.knpName]];
    }

    if (self.operationDetails.amount) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"operation_summm", nil) value:[NSString stringWithFormat:@"%@ %@", [self.operationDetails.amount decimalFormatString], self.operation ? self.operation.currency : @"KZT"]]];
    }

    if (self.operationDetails.commission) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"commission", nil) value:[NSString stringWithFormat:@"%@ %@", [self.operationDetails.commission decimalFormatString], self.operation ? self.operation.commissionCurrency : @"KZT"]]];
    }

    if (self.operationDetails.reference.length > 0) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"transaction_idd", nil) value:self.operationDetails.reference]];
    }

    if (self.operationDetails.transfer.reference.length > 0) {
        [detailRows addObject:[self rowWithTitle:@"ID операции с SIC" value:self.operationDetails.transfer.reference]];
    }

    if (self.operation.operationId || self.operationId) {
        [detailRows addObject:[self rowWithTitle:NSLocalizedString(@"operation_id_", nil) value:[NSString stringWithFormat:@"%@", self.operation ? self.operation.operationId : self.operationId]]];
    }
    }
    section1.rows = detailRows;
    [sections addObject:section1];

    [list reloadData];
    list.hidden = NO;
}

#pragma mark -
#pragma mark UITableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return sections.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    AATableSection *sectionObject = sections[(NSUInteger) section];
   // if([sectionObject.sectionId isEqualToString:kSectionDetails] && self.newVersion == YES)  return [sectionObject.rows count] + 1;
    return [sectionObject.rows count];
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    AATableSection *sectionObject = sections[(NSUInteger) section];
    return sectionObject.headerTitle?sectionObject.headerTitle:@"";
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    AATableSection *sectionObject = sections[(NSUInteger) indexPath.section];
    if([sectionObject.sectionId isEqual:kSectionProviderInfo] || [sectionObject.sectionId isEqual:kSectionTransferInfo])
    {
        if([sectionObject.sectionId isEqual:kSectionTransferInfo]) {
            return 44;
        }
        return 60;
    } else if([sectionObject.sectionId isEqual:kSectionLogsButton]) {
        return 47;
    }

    NSDictionary *row = sectionObject.rows[(NSUInteger) indexPath.row];
    return [AASimpleTableViewCell cellHeightWithTitle:row[@"title"] subtitle:row[@"value"]];
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if(section == 2 && [OperationHistory statusFromString:self.operation.status] == StatusTypeError) {
        if([view isKindOfClass:[UITableViewHeaderFooterView class]]){

            UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
            tableViewHeaderFooterView.textLabel.textColor = [UIColor deleteColor];
        }
    }
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AATableSection *section = sections[(NSUInteger) indexPath.section];
    
     if([section.sectionId isEqual:kSectionProviderInfo] || [section.sectionId isEqual:kSectionTransferInfo])
    {
        static NSString *CellIdentifier = @"OperationDetailsHeaderCell";
        OperationDetailsHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[OperationDetailsHeaderCell alloc] initWithReuseIdentifier:CellIdentifier];
        }

        cell.textLabel.font = [UIFont systemFontOfSize:15];

        if([section.sectionId isEqual:kSectionProviderInfo])
        {
            cell.textLabel.text = self.operationDetails.contract.alias;
            cell.detailTextLabel.text = self.operationDetails.providerName;
            [cell.imageView sd_setImageWithURL:[NSURL URLWithString:self.operationDetails.providerLogo] placeholderImage:[UIImage imageNamed:@"icon-icon-placeholder.png"] completed:nil];
        }
        else if([section.sectionId isEqual:kSectionTransferInfo])
        {
            [cell setTitle:NSLocalizedString(@"bank_transfers_lowercase", nil) subtitle:self.operationDetails.providerName];
        }

        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        cell.contentView.backgroundColor = [UIColor clearColor];

        return cell;
    }
    else if([section.sectionId isEqual:kSectionLogsButton])
    {
        static NSString *CellIdentifier = @"cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.textLabel.text = NSLocalizedString(@"more_info", nil);
        cell.imageView.image = [UIImage imageNamed:@"icon-content-list.png"];
        
        return cell;
    }
    else
    {
        if(indexPath.row == section.rows.count - 1 && self.newVersion)
        {
            static NSString *CellIdentifier = @"amountCell";
            TransferAmountCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[TransferAmountCellTableViewCell alloc] initWithReuseIdentifier:CellIdentifier];
                cell.textLabel.font = [UIFont systemFontOfSize:14];
                cell.textLabel.textColor = [UIColor grayColor];
                cell.detailTextLabel.font = [UIFont boldSystemFontOfSize:15];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            NSString *commissionSubtitle = [NSString stringWithFormat:@"+%@",[transferResponse.amountInfo.commission.commissionAmount decimalFormatString]];
            NSNumber *finalSummTransfer = [NSNumber numberWithFloat:([transferResponse.amountInfo.finalAmount floatValue] + [transferResponse.amountInfo.commission.commissionAmount floatValue])];
            [cell setTitleForTransferSumm:@"Сумма перевода" comissionSumm:@"Коммиссия" finalSumm:@"Итого"];
            [cell setSubtitleForTransferSumm:[transferResponse.amountInfo.finalAmount decimalFormatString] commissionSubtitle:commissionSubtitle finalSubtitle:[finalSummTransfer decimalFormatString]];
            
            cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
            
            return cell;
        }else{
        
        static NSString *CellIdentifier = @"OperationDetailsRowCell";
        AASimpleTableViewCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[AASimpleTableViewCell alloc] initWithReuseIdentifier:CellIdentifier];
        }
        NSDictionary *row = section.rows[(NSUInteger) indexPath.row];
        
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        [cell setTitle:row[@"title"]];
        [cell setSubtitle:row[@"value"]];
            if(transferError && (indexPath.row == 3 || indexPath.row == 4)){
              [cell setSubtitleTextColor:[UIColor deleteColor]];
            } else if(!transferError && indexPath.row == 3)
            {
              [cell setSubtitleTextColor:[UIColor appGradientTopGreenColor]];
            }
            return cell;
        }
    }

    return nil;
}

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    return (action == @selector(copy:));
}

- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(copy:)) {
        AATableSection *section = sections[indexPath.section];
        NSDictionary *row = section.rows[indexPath.row];
        NSString *value = row[@"value"];
        if(value) {
            [[UIPasteboard generalPasteboard] setString:value];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
   if(section == [sections count] - 1) return 135;
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(!footerButtonView)
    {
        footerButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [self tableView:tableView heightForFooterInSection:section])];
        
        toTransferButton = [MainButton newAutoLayoutView];
        [toTransferButton setTitle:@"Повторить перевод"];
        toTransferButton.mainButtonStyle = MainButtonStyleOrange;
        [toTransferButton addTarget:self action:@selector(goTransfersTapped) forControlEvents:UIControlEventTouchUpInside];
        [footerButtonView addSubview:toTransferButton];
        [toTransferButton aa_setWidth:200];
        [toTransferButton aa_setHeight:40];
        [toTransferButton aa_centerHorizontal];
        [toTransferButton aa_superviewTop:25];
        
        toPaymentsButton = [MainButton newAutoLayoutView];
        [toPaymentsButton setTitle:@"К платежам"];
        toPaymentsButton.mainButtonStyle = MainButtonStyleOrange;
        [toPaymentsButton addTarget:self action:@selector(goPaymentsTapped) forControlEvents:UIControlEventTouchUpInside];
        [footerButtonView addSubview:toPaymentsButton];
        [toPaymentsButton aa_setWidth:200];
        [toPaymentsButton aa_setHeight:40];
        [toPaymentsButton aa_centerHorizontal];
        [toPaymentsButton aa_pinUnderView:toTransferButton offset:15];
        
    }
    return footerButtonView;
}*/


#pragma mark -

- (NSDictionary *)rowWithTitle:(NSString *)title value:(NSString *)value {
    if(!title) title = @"-";
    if(!value) value = @"-";
    return @{@"title":title, @"value":value};
}

-(void)configUI
{
    [self setBafBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"operation_detail", nil)];

    self.contentScrollView.backgroundColor = [UIColor clearColor];
    
    list = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeightWithoutStatusBarAndNavigationBar]) style:UITableViewStyleGrouped];
    list.backgroundView = nil;
    list.backgroundColor = [UIColor clearColor];
    list.delegate = self;
    list.dataSource = self;
    list.hidden = YES;
    [self.view addSubview:list];
    
    footerButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 135)];
    
    toTransferButton = [MainButton newAutoLayoutView];
    [toTransferButton setTitle:@"Повторить перевод"];
    toTransferButton.mainButtonStyle = MainButtonStyleOrange;
    [toTransferButton addTarget:self action:@selector(goTransfersTapped) forControlEvents:UIControlEventTouchUpInside];
    [footerButtonView addSubview:toTransferButton];
    [toTransferButton aa_setWidth:200];
    [toTransferButton aa_setHeight:40];
    [toTransferButton aa_centerHorizontal];
    [toTransferButton aa_superviewTop:25];
    
    toPaymentsButton = [MainButton newAutoLayoutView];
    [toPaymentsButton setTitle:@"К платежам"];
    toPaymentsButton.mainButtonStyle = MainButtonStyleOrange;
    [toPaymentsButton addTarget:self action:@selector(goPaymentsTapped) forControlEvents:UIControlEventTouchUpInside];
    [footerButtonView addSubview:toPaymentsButton];
    [toPaymentsButton aa_setWidth:200];
    [toPaymentsButton aa_setHeight:40];
    [toPaymentsButton aa_centerHorizontal];
    [toPaymentsButton aa_pinUnderView:toTransferButton offset:15];
                                                                
    list.tableFooterView = footerButtonView;
    
    /*toPaymentsButton = [MainButton newAutoLayoutView];
    [toPaymentsButton setTitle:@"К платежам"];
    toPaymentsButton.layer.cornerRadius = 0.2;
    [self.view addSubview:toPaymentsButton];
    [toPaymentsButton aa_setWidth:120];
    [toPaymentsButton aa_setHeight:60];
    [toPaymentsButton aa_centerHorizontal];
    [toPaymentsButton aa_pinUnderView:list offset:25];
    
    toTransferButton = [MainButton newAutoLayoutView];
    [toTransferButton setTitle:@"Повторить перевод"];
    toTransferButton.layer.cornerRadius = 0.2;
    [self.view addSubview:toTransferButton];
    [toTransferButton aa_setWidth:120];
    [toTransferButton aa_setHeight:60];
    [toTransferButton aa_centerHorizontal];
    [toPaymentsButton aa_pinUnderView:toPaymentsButton offset:15];
    [toPaymentsButton aa_superviewBottom:25];*/
    
}

#pragma mark - dealloc

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"LastOperaionDetailsVC dealloc");
}

@end
