//
//  LastOperationsFilterViewController.h
//  BAF
//
//  Created by Shyngys Kassymov on 11.08.15.
//
//

#import <UIKit/UIKit.h>
#import "OperationHistoryFilter.h"

@interface LastOperationsFilterViewController : UIViewController

@property (nonatomic, strong) OperationHistoryFilter *filter;
@property (nonatomic, copy) void (^didChangeFilter)();

@end
