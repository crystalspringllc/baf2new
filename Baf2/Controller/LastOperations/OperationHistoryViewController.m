//
//  OperationHistoryViewController.m
//  Myth
//
//  Created by Almas Adilbek on 12/25/12.
//
//

#import "OperationHistoryViewController.h"
#import "OperationsApi.h"
#import "LastOperationCell.h"
#import "LastOperationDetailsViewController.h"
#import "LastOperationsFilterViewController.h"
#import "NotificationCenterHelper.h"
#import "AstanaRefreshControl.h"
#import "NewPaymentsDetailViewController.h"

@interface OperationHistoryViewController ()
@end

@implementation OperationHistoryViewController {
    UIActivityIndicatorView *loader;
    UITableView *list;
    AstanaRefreshControl *refreshControl;
    OperationHistoryFilter *filter;
}

- (id)init
{
    self = [super init];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initVars];
    [self configUI];
    
    list.hidden = YES;
    [self loadLastOperations:false];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self]; // sometimes dealloc not called, therefore remove previous observers if exist

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(filterChanged) name:kNotificationLastOperationsFilterChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadLastOperations:) name:kNotificationLastOperationsUpdate object:nil];
    
    [GAN sendView:NSLocalizedString(@"last_operations", nil)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)initVars {
    filter = [OperationHistoryFilter defaultFilter];
}

- (void)loadLastOperations:(BOOL)refreshing {
    if (!refreshing) {
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading", nil) animated:true];
        list.hidden = YES;
    }

    [OperationsApi getLastOperationsWithFilter:filter success:^(NSArray *operationStatusList, NSArray *operationList, NSString *startDate, NSString *endDate) {
        [MBProgressHUD hideAstanaHUDAnimated:true];

        self.operationList = operationList;

        [self onSuccessResponse];

        [list layoutIfNeeded];
        if (refreshing) {
            [refreshControl finishingLoading];
        }
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        [list layoutIfNeeded];
        if (refreshing) {
            [refreshControl finishingLoading];
        }
    }];
}

- (void)reloadLastOperations {
    [self loadLastOperations:true];
}

- (void)onSuccessResponse {
    [list reloadData];
    list.hidden = NO;
}

#pragma mark -
#pragma mark Actions

- (void)filterTapped {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LastOperationsFilter" bundle:nil];
    LastOperationsFilterViewController *v = [storyboard instantiateViewControllerWithIdentifier:@"lastOperationsFilter"];
    v.filter = filter;
    __weak OperationHistoryViewController *weakSelf = self;
    v.didChangeFilter = ^() {
        [weakSelf filterChanged];
    };
    [self.navigationController pushViewController:v animated:YES];
}

#pragma mark - Nofifications

- (void)filterChanged {
    [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
    
    [OperationsApi getLastOperationsWithFilter:filter success:^(NSArray *operationStatusList, NSArray *operationList, NSString *startDate, NSString *endDate) {
        [MBProgressHUD hideAstanaHUDAnimated:true];

        self.operationList = operationList;

        [self onSuccessResponse];

    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark -
#pragma mark UITableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.operationList.count == 0 ? 0 : 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.operationList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"LastOperationCell";
    LastOperationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[LastOperationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"LastOperationCell"];
    }
    [cell setOperationHistory:self.operationList[(NSUInteger) indexPath.row]];

    if ([cell respondsToSelector:@selector(preservesSuperviewLayoutMargins)]){
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
    }
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }

    cell.backgroundColor = [UIColor whiteColor];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [list deselectRowAtIndexPath:indexPath animated:YES];
    OperationHistory *operation = self.operationList[(NSUInteger) indexPath.row];
    /*if([operation isPaymenType])
    {
      [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading", nil) animated:true];
    [OperationsApi getPaymentsVersion:operation.operationId success:^(NSNumber *version) {
         [MBProgressHUD hideAstanaHUDAnimated:true];
       
        if(version == [NSNumber numberWithInt:1])
        {
            NewPaymentsDetailViewController *v = [[NewPaymentsDetailViewController alloc] init];
            v.operation = operation;
            [self.navigationController pushViewController:v animated:YES];
        }else{
            LastOperationDetailsViewController *v = [[LastOperationDetailsViewController alloc] init];
            v.operation = operation;
            [self.navigationController pushViewController:v animated:YES];
        }

    } failure:^(NSString *code, NSString *message) {
         [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
    }else{*/
        LastOperationDetailsViewController *v = [[LastOperationDetailsViewController alloc] init];
        v.operation = operation;
        v.operationId = operation.operationId;
        if([operation.type isEqualToString:@"TRANSFER"]) v.newVersion = YES;
        [self.navigationController pushViewController:v animated:YES];
    //}
   
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = NSLocalizedString(@"no_found_operation_on_that_period", nil);

    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
            NSForegroundColorAttributeName: [UIColor darkGrayColor]};

    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [refreshControl scrollViewDidScroll:scrollView];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    [refreshControl scrollViewWillEndDragging];
}

#pragma mark -

-(void)configUI
{
    [self setBafBackground];
    
    [self setBafBackground];
    [self setNavigationTitle:NSLocalizedString(@"ga_operation_history", nil)];
    [self showMenuButton];
    
    [self buildTableView];
    
    if ([list respondsToSelector:@selector(cellLayoutMarginsFollowReadableWidth)]) {
        list.cellLayoutMarginsFollowReadableWidth = false;
    }
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(filterTapped)];
}

- (void)buildTableView
{
    list = [[UITableView alloc] initWithFrame:CGRectMake(8, 0, [ASize screenWidth] - 2 * 8, [ASize screenHeightWithoutStatusBarAndNavigationBar]) style:UITableViewStylePlain];
    list.backgroundColor = [UIColor clearColor];
    list.delegate = self;
    list.dataSource = self;
    list.rowHeight = UITableViewAutomaticDimension;
    list.estimatedRowHeight = 106;
    list.tableFooterView = [UIView new];
    list.bounces = true;
    list.alwaysBounceVertical = true;
    list.layer.shadowColor = [UIColor fromRGB:0x444444].CGColor;
    list.layer.shadowOpacity = 0.3;
    list.layer.shadowOffset = CGSizeMake(0,0);
    list.layer.shadowRadius = 5;
    list.layer.masksToBounds = false;
    list.emptyDataSetSource = self;
    [self.view addSubview:list];

    refreshControl = [[AstanaRefreshControl alloc] init];
    refreshControl.target = self;
    refreshControl.action = @selector(reloadLastOperations);
    [refreshControl addTarget:self action:@selector(loadLastOperations:) forControlEvents:UIControlEventValueChanged];
    [list addSubview:refreshControl];
    [list sendSubviewToBack:refreshControl];
    
    [list registerNib:[UINib nibWithNibName:@"LastOperationCell" bundle:nil] forCellReuseIdentifier:@"LastOperationCell"];

}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"LastOperationHistoryVC dealloc");
}

@end
