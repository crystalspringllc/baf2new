//
//  LastOperationDetailsViewController.h
//  BAF
//
//  Created by Almas Adilbek on 1/20/15.
//
//

#import "ScrollViewController.h"
#import "OperationHistory.h"

@class LastOperation;

@interface LastOperationDetailsViewController : ScrollViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) OperationHistory *operation;
@property (nonatomic, strong) NSNumber *operationId;
@property (nonatomic) BOOL newVersion;

@end
