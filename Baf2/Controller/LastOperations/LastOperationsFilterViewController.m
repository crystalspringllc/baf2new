//
//  LastOperationsFilterViewController.m
//  BAF
//
//  Created by Shyngys Kassymov on 11.08.15.
//
//

#import "LastOperationsFilterViewController.h"
#import "ProfileApiOLD.h"
#import "DatePickerSuperTextFieldView.h"
#import "DropdownSuperTextFieldView.h"
#import <M13Checkbox.h>
#import "Contract.h"
#import "ContractModelView.h"
#import "NSDate+Ext.h"
#import "MainButton.h"

@interface LastOperationsFilterViewController () <DatePickerSuperTextFieldViewDelegate>

@end

@implementation LastOperationsFilterViewController {
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet DatePickerSuperTextFieldView *fromDateTextFieldView;
    __weak IBOutlet DatePickerSuperTextFieldView *toDateTextFieldView;
    __weak IBOutlet DropdownSuperTextFieldView *periodTextFieldView;
    __weak IBOutlet DropdownSuperTextFieldView *providerDropdown;
    __weak IBOutlet DropdownSuperTextFieldView *contractDropdown;
    __weak IBOutlet SuperTextFieldView *fromAmountOfMoneyTextField;
    __weak IBOutlet SuperTextFieldView *toAmountOfMoneyTextField;
    __weak IBOutlet M13Checkbox *failedStatusCheckbox;
    __weak IBOutlet M13Checkbox *succeededStatusCheckbox;
    __weak IBOutlet M13Checkbox *inProcessStatusCheckbox;
    __weak IBOutlet MainButton *submitButton;
    
    NSArray *myContracts;
    NSMutableArray *resultProviders;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
    [self populateContractsAndProvidersDropDownMenus];
}

#pragma mark - Actions

- (void)updateDatesIfNeeded {
    NSDate *date;
    if (periodTextFieldView.selectedIndex == 0) {
        date = [[NSDate date] getDateYears:0 monthsAgo:0 andDaysAgo:7];
    } else if (periodTextFieldView.selectedIndex == 1) {
        date = [[NSDate date] getDateYears:0 monthsAgo:1 andDaysAgo:0];
    } else if (periodTextFieldView.selectedIndex == 2) {
        date = [[NSDate date] getDateYears:1 monthsAgo:0 andDaysAgo:0];
    } else {
        date = [NSDate date];
    }

    fromDateTextFieldView.date = date;
    [fromDateTextFieldView setValue:[fromDateTextFieldView.dateFormatter stringFromDate:date]];
    
    toDateTextFieldView.date = [NSDate date];
    [toDateTextFieldView setValue:[toDateTextFieldView.dateFormatter stringFromDate:[NSDate date]]];
}

- (void)updateDropDownMenusIfNeeded:(DropdownSuperTextFieldView *)sender atIndex:(int)index {
    if (index != 0) {
        [sender setIconWithURL:sender.options[index][AAFieldActionSheetOptionIcon]];

        if (sender == providerDropdown) {
            [contractDropdown removeLeftIcon];
            [contractDropdown setSelectedIndex:0];
        } else {
            [providerDropdown removeLeftIcon];
            [providerDropdown setSelectedIndex:0];
        }
    } else {
        if (sender != providerDropdown) {
            [contractDropdown removeLeftIcon];
        } else {
            [providerDropdown removeLeftIcon];
        }
    }
}

- (IBAction)doneTapped {
    self.filter.startDate = fromDateTextFieldView.value;
    self.filter.endDate = toDateTextFieldView.value;
    if (fromAmountOfMoneyTextField.value && ![fromAmountOfMoneyTextField.value isEqualToString:@""]) {
        self.filter.minAmount = fromAmountOfMoneyTextField.value;
    } else {
        self.filter.minAmount = nil;
    }
    if (toAmountOfMoneyTextField.value && ![toAmountOfMoneyTextField.value isEqualToString:@""]) {
        self.filter.maxAmount = toAmountOfMoneyTextField.value;
    } else {
        self.filter.maxAmount = nil;
    }
    if (providerDropdown.optionId && [providerDropdown.optionId integerValue] != 0) {
        self.filter.providerId = providerDropdown.optionId;
    } else {
        self.filter.providerId = nil;
    }
    if (contractDropdown.optionId && [contractDropdown.optionId integerValue] != 0) {
        self.filter.contractId = contractDropdown.optionId;
    } else {
        self.filter.contractId = nil;
    }
    NSMutableArray *statuses = [NSMutableArray new];
    if (failedStatusCheckbox.checkState == M13CheckboxStateChecked) {
        [statuses addObject:@"ERROR"];
    }
    if (succeededStatusCheckbox.checkState == M13CheckboxStateChecked) {
        [statuses addObject:@"COMPLETE"];
    }
    if (inProcessStatusCheckbox.checkState == M13CheckboxStateChecked) {
        [statuses addObject:@"IN_PROGRESS"];
    }
    self.filter.selectedOperationStatuses = statuses;
    
    if (self.didChangeFilter) {
        self.didChangeFilter();
    }

    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - DatePickerSuperTextFieldViewDelegate

- (void)datePickerDateChanged:(UIDatePicker *)datePicker {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger comps = (NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear);
    
    NSDateComponents *date1Components = [calendar components:comps
                                                    fromDate: fromDateTextFieldView.date];
    NSDateComponents *date2Components = [calendar components:comps
                                                    fromDate: toDateTextFieldView.date];
    NSDateComponents *date3Components = [calendar components:comps
                                                    fromDate: [[NSDate date] dateBySubtractingWeeks:1]];
    NSDateComponents *date4Components = [calendar components:comps
                                                    fromDate: [[NSDate date] dateBySubtractingMonths:1]];
    NSDateComponents *date5Components = [calendar components:comps
                                                    fromDate: [[NSDate date] dateBySubtractingYears:1]];
    NSDateComponents *date6Components = [calendar components:comps
                                                    fromDate: [NSDate date]];
    
    NSDate *dateFrom = [calendar dateFromComponents:date1Components];
    NSDate *dateTo = [calendar dateFromComponents:date2Components];
    NSDate *date7 = [calendar dateFromComponents:date3Components];
    NSDate *date30 = [calendar dateFromComponents:date4Components];
    NSDate *dateYearAgo = [calendar dateFromComponents:date5Components];
    NSDate *dateNow = [calendar dateFromComponents:date6Components];
    
    if (dateFrom == date7 && dateTo == dateNow) {
        [periodTextFieldView setSelectedIndex:0];
        [periodTextFieldView setValue:NSLocalizedString(@"last_week", nil)];
    } else if (dateFrom == date30 && dateTo == dateNow) {
        [periodTextFieldView setSelectedIndex:1];
        [periodTextFieldView setValue:NSLocalizedString(@"last_month", nil)];
    } else if (dateFrom == dateYearAgo && dateTo == dateNow) {
        [periodTextFieldView setSelectedIndex:2];
        [periodTextFieldView setValue:NSLocalizedString(@"last_year", nil)];
    } else {
        [periodTextFieldView setSelectedIndex:3];
        [periodTextFieldView setValue:NSLocalizedString(@"other", nil)];
    }
    
    fromDateTextFieldView.datePicker.maximumDate = toDateTextFieldView.date;
    toDateTextFieldView.datePicker.minimumDate = fromDateTextFieldView.date;
    toDateTextFieldView.datePicker.maximumDate = [NSDate date];
}

- (void)datePickerShowed:(UIDatePicker *)datePicker {
    fromDateTextFieldView.datePicker.maximumDate = toDateTextFieldView.date;
    toDateTextFieldView.datePicker.minimumDate = fromDateTextFieldView.date;
    toDateTextFieldView.datePicker.maximumDate = [NSDate date];
}

#pragma mark -

- (void)populateContractsAndProvidersDropDownMenus {
    dispatch_group_t group_queue = dispatch_group_create();

    dispatch_group_enter(group_queue);
//    if (!ContractsApi.contracts) {
//        [MBProgressHUD showAstanaHUDWithTitle:@"Загрузка контрактов" animated:true];
//        [ContractsApi getContractsWithSuccess:^(NSArray *contracts) {
//            myContracts = contracts;
//            dispatch_group_leave(group_queue);
//        } failure:^(NSString *code, NSString *message) {
//            dispatch_group_leave(group_queue);
//        }];
//    } else {
//        dispatch_group_leave(group_queue);
//    }
    dispatch_group_leave(group_queue);
    
    dispatch_group_notify(group_queue, dispatch_get_main_queue(), ^{
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        // get contracts
        if(!myContracts)
        {
            myContracts = [[NSArray alloc] initWithArray:[ContractModelView sharedInstance].contracts];
        }
        
        // get providers
        resultProviders = [NSMutableArray new];
        if(myContracts) {
            for(Contract *contract in myContracts) {
                BOOL isAlreadyInArray = NO;
                for (PaymentProvider *paymentprovider in resultProviders) {
                    if ([paymentprovider.paymentProviderId integerValue] == [contract.provider.paymentProviderId integerValue]) {
                        isAlreadyInArray = YES;
                        break;
                    }
                }
                if (!isAlreadyInArray) {
                    [resultProviders addObject:contract.provider];
                }
            }
        }
        
        // populate drop down menus
        [providerDropdown addOptionWithId:@"" title:NSLocalizedString(@"any", nil) icon:nil];
        for (PaymentProvider *paymentProvider in resultProviders) {
            [providerDropdown addOptionWithId:paymentProvider.paymentProviderId title:paymentProvider.name icon:paymentProvider.logo];
        }
        [providerDropdown setSelectedIndex:0];
        
        [contractDropdown addOptionWithId:@"" title:NSLocalizedString(@"any", nil) icon:nil];
        for (Contract *contract in myContracts) {
            [contractDropdown addOptionWithId:contract.identifier title:contract.alias icon:contract.provider.logo];
        }
        [contractDropdown setSelectedIndex:0];
        
        [self setInitialStateIfNeeded];
    });
}

- (void)setInitialStateIfNeeded {
    if (self.filter) {
        fromDateTextFieldView.value = self.filter.startDate;
        fromDateTextFieldView.date = [fromDateTextFieldView.dateFormatter dateFromString:self.filter.startDate];
        
        toDateTextFieldView.value = self.filter.endDate;
        toDateTextFieldView.date = [toDateTextFieldView.dateFormatter dateFromString:self.filter.endDate];
        
        [self datePickerDateChanged:nil];
        
        if (self.filter.minAmount && ![self.filter.minAmount isEqualToString:@""]) {
            fromAmountOfMoneyTextField.value = self.filter.minAmount;
        }
        if (self.filter.maxAmount && ![self.filter.maxAmount isEqualToString:@""]) {
            toAmountOfMoneyTextField.value = self.filter.maxAmount;
        }
        if (self.filter.providerId && [self.filter.providerId integerValue] != 0) {
            for (int i = 0; i < resultProviders.count; i++) {
                PaymentProvider *paymentProvider = resultProviders[i];
                if ([paymentProvider.paymentProviderId integerValue] == [self.filter.providerId integerValue]) {
                    [providerDropdown setSelectedIndex:i + 1];
                    [self updateDropDownMenusIfNeeded:providerDropdown atIndex:i + 1];
                    break;
                }
            }
        } else {
            [providerDropdown setSelectedIndex:0];
        }
        if (self.filter.contractId && [self.filter.contractId integerValue] != 0) {
            for (int i = 0; i < myContracts.count; i++) {
                Contract *contract = myContracts[i];
                if ([contract.identifier integerValue]  == [self.filter.contractId integerValue]) {
                    [contractDropdown setSelectedIndex:i + 1];
                    [self updateDropDownMenusIfNeeded:contractDropdown atIndex:i + 1];
                    break;
                }
            }
        } else {
            [contractDropdown setSelectedIndex:0];
        }
        if (self.filter.selectedOperationStatuses) {
            if ([self.filter.selectedOperationStatuses containsObject:@"ERROR"]) {
                [failedStatusCheckbox setCheckState:M13CheckboxStateChecked];
            }
            if ([self.filter.selectedOperationStatuses containsObject:@"COMPLETE"]) {
                [succeededStatusCheckbox setCheckState:M13CheckboxStateChecked];
            }
            if ([self.filter.selectedOperationStatuses containsObject:@"IN_PROGRESS"]) {
                [inProcessStatusCheckbox setCheckState:M13CheckboxStateChecked];
            }
        }
    }
}

#pragma mark - UI

- (void)configUI {
    [self setBafBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"operation_filter", nil)];
    
    scrollView.backgroundColor = [UIColor clearColor];
    
    // time period
    [periodTextFieldView addOptionWithId:@"1" title:NSLocalizedString(@"last_week", nil)];
    [periodTextFieldView addOptionWithId:@"2" title:NSLocalizedString(@"last_month", nil)];
    [periodTextFieldView addOptionWithId:@"3" title:NSLocalizedString(@"last_year", nil)];
    [periodTextFieldView addOptionWithId:@"4" title:NSLocalizedString(@"other", nil)];
    [periodTextFieldView setPlaceholder:NSLocalizedString(@"for_period", nil)];
    [periodTextFieldView setValue:NSLocalizedString(@"last_week", nil)];
    [periodTextFieldView setSelectedIndex:0];
    [periodTextFieldView setOnOptionChangedBlock:^(NSDictionary *option, NSInteger index) {
        [self updateDatesIfNeeded];
    }];

    [fromDateTextFieldView setPrefix:@"с" font:[UIFont helveticaNeue:14] textColor:[UIColor grayColor]];
    [fromDateTextFieldView setFont:[UIFont helveticaNeue:14]];
    fromDateTextFieldView.delegate = self;
    fromDateTextFieldView.isToolbarHidden = YES;
    [fromDateTextFieldView onValueChanged:^(NSString *value) {
        NSLog(@"value - %@", value);
    }];
    
    [toDateTextFieldView setPrefix:@"по" font:[UIFont helveticaNeue:14] textColor:[UIColor grayColor]];
    [toDateTextFieldView setFont:[UIFont helveticaNeue:14]];
    toDateTextFieldView.delegate = self;
    toDateTextFieldView.isToolbarHidden = YES;
    [toDateTextFieldView onValueChanged:^(NSString *value) {
        NSLog(@"value - %@", value);
    }];
    
    [self updateDatesIfNeeded];
    
    // providers drop down
    [providerDropdown setPlaceholder:NSLocalizedString(@"choose_provider", nil)];
    __weak LastOperationsFilterViewController *weakSelf = self;
    __weak DropdownSuperTextFieldView *weakProviderDropdown= providerDropdown;
    [providerDropdown setOnOptionChangedBlock:^(NSDictionary *option, NSInteger index) {
        [weakSelf updateDropDownMenusIfNeeded:weakProviderDropdown atIndex:(int)index];
    }];
    
    // contracts drop down
    [contractDropdown setPlaceholder:NSLocalizedString(@"choose_contract", nil)];
    __weak DropdownSuperTextFieldView *weakContractDropdown = contractDropdown;
    [contractDropdown setOnOptionChangedBlock:^(NSDictionary *option, NSInteger index) {
        [weakSelf updateDropDownMenusIfNeeded:weakContractDropdown atIndex:(int)index];
    }];
    
    // money filter
    [fromAmountOfMoneyTextField setPrefix:@"от" font:[UIFont helveticaNeue:14] textColor:[UIColor grayColor]];
    [fromAmountOfMoneyTextField setFont:[UIFont helveticaNeue:14]];
    fromAmountOfMoneyTextField.numeric = YES;
    
    [toAmountOfMoneyTextField setPrefix:@"до" font:[UIFont helveticaNeue:14] textColor:[UIColor grayColor]];
    [toAmountOfMoneyTextField setFont:[UIFont helveticaNeue:14]];
    toAmountOfMoneyTextField.numeric = YES;
    
    // status filter
    failedStatusCheckbox.titleLabel.text = NSLocalizedString(@"payment_history_filter_error", nil);
    failedStatusCheckbox.titleLabel.font = [UIFont helveticaNeue:14];
    [failedStatusCheckbox setCheckAlignment:M13CheckboxAlignmentLeft];
    failedStatusCheckbox.checkColor = [UIColor darkGreen];
    failedStatusCheckbox.strokeColor = [UIColor darkGreen];
    
    succeededStatusCheckbox.titleLabel.text = NSLocalizedString(@"payment_history_filter_complete", nil);
    succeededStatusCheckbox.titleLabel.font = [UIFont helveticaNeue:14];
    [succeededStatusCheckbox setCheckAlignment:M13CheckboxAlignmentLeft];
    succeededStatusCheckbox.checkColor = [UIColor darkGreen];
    succeededStatusCheckbox.strokeColor = [UIColor darkGreen];
    
    inProcessStatusCheckbox.titleLabel.text = NSLocalizedString(@"inprogress", nil);
    inProcessStatusCheckbox.titleLabel.font = [UIFont helveticaNeue:14];
    [inProcessStatusCheckbox setCheckAlignment:M13CheckboxAlignmentLeft];
    inProcessStatusCheckbox.checkColor = [UIColor darkGreen];
    inProcessStatusCheckbox.strokeColor = [UIColor darkGreen];
    
    submitButton.mainButtonStyle = MainButtonStyleOrange;
    submitButton.title = NSLocalizedString(@"apply", nil);
}

@end
