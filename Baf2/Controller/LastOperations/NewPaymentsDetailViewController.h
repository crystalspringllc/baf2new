//
//  NewPaymentsDetailViewController.h
//  Baf2
//
//  Created by developer on 13.12.17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OperationHistory.h"

@interface NewPaymentsDetailViewController : UIViewController

@property (nonatomic, weak) OperationHistory *operation;
@property (nonatomic, strong) NSNumber *operationId;

@end
