//
//  ForgotPasswordViewController.m
//  Baf2
//
//  Created by nmaksut on 06.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "FloatingTextField.h"
#import "ProfileApiOLD.h"
#import "NSString+Validators.h"
#import "AuthApi.h"
#import "UITableViewController+Extension.h"
#import "MainButton.h"

@interface ForgotPasswordViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet FloatingTextField *usernameField;
@property (weak, nonatomic) IBOutlet MainButton *send_button;
@end

@implementation ForgotPasswordViewController

#pragma mark -
#pragma mark - Init

- (instancetype)init {
    self = [super init];
    if (self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    }
    return self;
}

#pragma mark -
#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
}

#pragma mark -
#pragma mark - Actions

- (IBAction)sendTapped:(id)sender {
    if (!self.usernameField.isset) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"please_write_your_number", nil)];
        return;
    }
    
    if (!self.usernameField.value.validateEmail) {
        return;
    }
    
    NSString *email = [self.usernameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if(email.length > 0) {
        [self.usernameField resignFirstResponder];

        [MBProgressHUD showAstanaHUDWithTitle:nil animated:true];
        [AuthApi recoverEmail:email success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
            [Toast showToast:NSLocalizedString(@"letter_with_instruction_sent_on_your_mail", nil)];
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];

    } else {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"please_write_your_number", nil) message:@""];
    }

}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self sendTapped:nil];
    return YES;
}

#pragma mark - UITableViewDataSource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return NSLocalizedString(@"enter_email_or_phonenumber", nil);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    
    return cell;
}

#pragma mark -
#pragma mark - ConfigUI

- (void)configUI {
    [self setBAFTableViewBackground];
    [self setNavigationTitle:NSLocalizedString(@"forget_password", nil)];
    [self setNavigationBackButton];

    [self.usernameField setPlaceholder:NSLocalizedString(@"your_mail", nil)];
    self.usernameField.delegate = self;
    self.usernameField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.usernameField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.usernameField.keyboardType = UIKeyboardTypeEmailAddress;
    
    [self.send_button setTitle:NSLocalizedString(@"sendd", nil)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
