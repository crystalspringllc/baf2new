//
//  ContactsViewController.m
//  BAF
//
//  Created by Askar Mustafin on 2/12/16.
//  Copyright © 2016 Банк Астаны. All rights reserved.
//

#import "ContactsViewController.h"
#import "LinksRouter.h"
#import <UIActionSheet+Blocks.h>

#define kBafShortNumber @"2555"

@interface ContactsViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *leftCallButton;

@property (weak, nonatomic) IBOutlet UIButton *forMobileCallButton;
@property (weak, nonatomic) IBOutlet UIButton *forCityCallButton;
@property (weak, nonatomic) IBOutlet UIButton *forWhatsAppCallButton;

@property (weak, nonatomic) IBOutlet UIButton *leftEmailButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;

@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;
@property (weak, nonatomic) IBOutlet UIButton *vkButton;

@end

@implementation ContactsViewController

- (id)init {
    if (self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Contacts" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ContactsViewController class])];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // center scroll view
    CGFloat newContentInsetY = (self.scrollView.bounds.size.height - self.scrollView.contentSize.height)/2;
    if (newContentInsetY > 0 && self.scrollView.bounds.size.height > 0 && self.scrollView.contentSize.height > 0) {
        self.scrollView.contentInset = UIEdgeInsetsMake(newContentInsetY, 0, 0, 0);
    }
}

#pragma mark - actions

- (IBAction)callTapped:(id)sender {
    [UIActionSheet showInView:self.view withTitle:NSLocalizedString(@"enter_num", nil) cancelButtonTitle:NSLocalizedString(@"cancel", nil) destructiveButtonTitle:nil otherButtonTitles:@[NSLocalizedString(@"2555_from_mobile", nil), NSLocalizedString(@"from_city_phone", nil)] tapBlock:^(UIActionSheet *_Nonnull actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            NSURL *phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", kBafShortNumber]];
            [[UIApplication sharedApplication] openURL:phoneUrl];
        } else if (buttonIndex == 1) {
            NSURL *phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", @"+77272596060"]];
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
    }];
}

- (IBAction)forMobileCallTapped:(id)sender {
    [UIHelper callPhone:kBafShortNumber showSheetInView:self.view];
}

- (IBAction)forCityCallTapped:(id)sender {
    [UIHelper callPhone:@"+77272596060" showSheetInView:self.view];
}

- (IBAction)forWhatsAppCallTapped:(id)sender {
    [UIActionSheet showInView:self.view withTitle:NSLocalizedString(@"choose_actionn", nil) cancelButtonTitle:NSLocalizedString(@"cancel", nil) destructiveButtonTitle:nil otherButtonTitles:@[NSLocalizedString(@"copy_number", nil)] tapBlock:^(UIActionSheet *_Nonnull actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = @"+77020552555";
        }
    }];
}

- (IBAction)emailTapped:(id)sender {
    NSURL *emailURL = [NSURL URLWithString:@"mailto:24@bankastana.kz"];
    [[LinksRouter sharedInstance] routeUrl:emailURL];
}

- (IBAction)facebookTapped:(id)sender {
    NSURL *emailURL = [NSURL URLWithString:@"https://www.facebook.com/bankastana"];
    [[LinksRouter sharedInstance] routeUrl:emailURL];
}

- (IBAction)twitterTapped:(id)sender {
    NSURL *emailURL = [NSURL URLWithString:@"https://twitter.com/BankofAstana"];
    [[LinksRouter sharedInstance] routeUrl:emailURL];
}

- (IBAction)vkTapped:(id)sender {
    NSURL *emailURL = [NSURL URLWithString:@"https://vk.com/bankastana"];
    [[LinksRouter sharedInstance] routeUrl:emailURL];
}

#pragma mark - config ui

- (void)configUI {
    [self setBafBackground];
    [self showMenuButton];
    [self setNavigationTitle:NSLocalizedString(@"contacts", nil)];
    self.scrollView.backgroundColor = [UIColor clearColor];

    self.facebookButton.layer.cornerRadius = self.facebookButton.width/2;
    self.facebookButton.layer.shadowColor = [UIColor fromRGB:0x444444].CGColor;
    self.facebookButton.layer.shadowOpacity = 0.3;
    self.facebookButton.layer.shadowOffset = CGSizeMake(0, 3);
    self.facebookButton.imageEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15);

    self.twitterButton.layer.cornerRadius = self.facebookButton.width/2;
    self.twitterButton.layer.shadowColor = [UIColor fromRGB:0x444444].CGColor;
    self.twitterButton.layer.shadowOpacity = 0.3;
    self.twitterButton.layer.shadowOffset = CGSizeMake(0, 3);
    self.twitterButton.imageEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15);

    self.vkButton.layer.cornerRadius = self.facebookButton.width/2;
    self.vkButton.layer.shadowColor = [UIColor fromRGB:0x444444].CGColor;
    self.vkButton.layer.shadowOpacity = 0.3;
    self.vkButton.layer.shadowOffset = CGSizeMake(0, 3);
    self.vkButton.imageEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15);
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end