//
//  ChangeEmailConfirmViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 6/22/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangeEmailConfirmViewController : UIViewController

@property (nonatomic, strong) NSString *neuEmailStr;
@property (nonatomic, strong) NSNumber *serviceLogId;

+ (instancetype)createVC;

@end
