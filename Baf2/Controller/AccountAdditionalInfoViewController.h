//
//  AccountAdditionalInfoViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 20.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountAdditionalInfoViewController : UITableViewController

/**
 Format like: @code @[@{title: @"Title", subtitle: @"Subtitle"}, ...]
 */
@property (nonatomic, strong) NSArray *infoDataList;

- (void)addItems:(NSArray *)items;

@end
