//
//  AccountAliasEditViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 20.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "AccountAliasEditViewController.h"
#import "MainButton.h"
#import "FloatingTextField.h"
#import <STPopup.h>

@implementation AccountAliasEditViewController

- (instancetype)init {
    self = [super init];
    
    if (self ) {
        self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 68);
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 68);
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
    
    self.aliasTextField.placeholder = NSLocalizedString(@"enter_convenience_name", nil);
    self.aliasTextField.delegate = self;
    [self.doneButton setTitle:NSLocalizedString(@"done", nil)];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.aliasTextField becomeFirstResponder];
}

#pragma mark - Actions

- (void)makeDone:(MainButton *)sender {
    [self.aliasTextField resignFirstResponder];
    
    __weak AccountAliasEditViewController *weakSelf = self;
    [self.popupController dismissWithCompletion:^{
        if (weakSelf.didTapDoneButton) {
            weakSelf.didTapDoneButton(weakSelf.aliasTextField.text);
        }
    }];
}

#pragma mark - UITextFieldDelegat

#define KNPCODEMAXLENGTH 50

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    
    if ([textField isEqual:self.aliasTextField]) {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= KNPCODEMAXLENGTH || returnKey;
    }
    return YES;
}

#pragma mark - Config UI

- (void)configUI {
    self.aliasTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.aliasTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.aliasTextField.text = self.initialAlias;
    
    self.doneButton.title = NSLocalizedString(@"done", nil);
    [self.doneButton addTarget:self action:@selector(makeDone:) forControlEvents:UIControlEventTouchUpInside];
}

@end
