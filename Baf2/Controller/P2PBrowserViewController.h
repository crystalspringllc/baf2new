//
//  P2PBrowserViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 25.07.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class P2PInitResponse;

@interface P2PBrowserViewController : UIViewController

@property (nonatomic, strong) P2PInitResponse *p2PInitResponse;

@property (nonatomic, strong) NSString *processLink;
@property (nonatomic, strong) NSString *postLink;
@property (nonatomic, strong) NSString *backLink;
@property (nonatomic, strong) NSNumber *servicelogId;
@property (nonatomic, strong) NSNumber *terminalId;
@property (nonatomic, strong) NSString *securityLink;
@property (nonatomic, strong) NSDictionary *securityParams;

@property (nonatomic, assign) BOOL isSecurityType;
@property (nonatomic, assign) BOOL isNew;

@end
