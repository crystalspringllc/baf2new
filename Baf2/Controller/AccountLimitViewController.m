//
//  AccountLimitViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 12.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "AccountLimitViewController.h"
#import "AccountsApi.h"
#import "CardLimitFormView.h"
#import "NotificationCenterHelper.h"
#import "NSString+Ext.h"
#import <STPopup.h>

@interface AccountLimitViewController ()
@property (nonatomic, strong) CardLimitFormView *cardLimitFormView;
@end

@implementation AccountLimitViewController

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.title = NSLocalizedString(@"limit_control", nil);
        self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 316);
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
}

#pragma mark - API

- (void)setNewLimit:(NSString *)newAmount {
    if (newAmount.doubleValue < self.limit.minAmount.doubleValue || newAmount.doubleValue > self.limit.maxAmount.doubleValue) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"error", nil) message:[NSString stringWithFormat:@"%@ %@ - %@", NSLocalizedString(@"limit_sum_must_be_in_diapazon", nil), self.limit.minAmount, self.limit.maxAmount]];
        return;
    }

    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"set_new_limit", nil) animated:true];
    [AccountsApi setCardLimit:self.card.cardId
                    limitCode:self.limit.code
                  limitAmount:[newAmount numberDecimalFormat]
                      success:^(id response) {

        [MBProgressHUD hideAstanaHUDAnimated:true];

        [Toast showToast:NSLocalizedString(@"new_limit_successfully_set", nil)];

        [self.popupController dismiss];

        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAccountLimitChanged object:nil];
    }                                                                                                                failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark - Config UI

- (void)configUI {
    self.cardLimitFormView = [[[NSBundle mainBundle] loadNibNamed:@"CardLimitFormView" owner:self options:nil] firstObject];
    self.cardLimitFormView.translatesAutoresizingMaskIntoConstraints = false;
    [self.cardLimitFormView setCardLimit:self.limit];
    
    __weak AccountLimitViewController *weakSelf = self;
    self.cardLimitFormView.didTapSubmitButton = ^(NSString *newAmount) {
        [weakSelf setNewLimit:newAmount];
    };
    
    [self.view addSubview:self.cardLimitFormView];
    [self.cardLimitFormView autoPinEdgesToSuperviewEdges];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], self.cardLimitFormView.bounds.size.height);
}

@end
