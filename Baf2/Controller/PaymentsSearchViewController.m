//
//  PaymentsSearchViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 23.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "PaymentsSearchViewController.h"
#import "ContractsAPI.h"
#import "SideMenuContractTableCell.h"
#import "ProviderCategory.h"
#import "SearchResultIconTitleTableCell.h"
#import "PaymentsViewController.h"
#import "CategoryProvidersListViewController.h"
#import "PaymentFormViewController.h"
#import "ContractModelView.h"
#import "ProvidersApi.h"
#import "PaymentProvidersByCategory.h"

#define kMyContractsKey @"myContracts"
#define kProvidersKey @"providers"
#define kProviderCategories @"providerCategories"

@implementation PaymentsSearchViewController {
    NSMutableDictionary *resultsDic;
    
    NSMutableArray *myContracts;
    NSMutableArray *resultMyContracts;
    
    NSMutableArray *providers;
    NSMutableArray *resultProviders;
    
    NSMutableArray *providerCategories;
    NSMutableArray *resultProviderCategories;
    
    UILabel *noResultLabel;
}

@synthesize delegate;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
    
    myContracts = [ContractModelView sharedInstance].contracts.mutableCopy;
    resultMyContracts = [NSMutableArray arrayWithArray:myContracts];
    
    NSArray *paymentProvidersByCategories = ProvidersApi.paymentProvidersByCategories;
    NSMutableArray *p = [NSMutableArray new];
    NSMutableArray *c = [NSMutableArray new];
    for (PaymentProvidersByCategory *paymentProvidersByCategory in paymentProvidersByCategories) {
        if ([paymentProvidersByCategory.category.providerCategoryId integerValue] != -1) {
            [p addObjectsFromArray:paymentProvidersByCategory.paymentProviderListWS];
            [c addObject:paymentProvidersByCategory.category];
        }
    }
    
    providers = p;
    resultProviders = [NSMutableArray arrayWithArray:providers];
    
    providerCategories = c;
    resultProviderCategories = [NSMutableArray arrayWithArray:providerCategories];
    
    [self search:nil]; // do initial search
    
    if (self.paymentsViewController) {
        __weak PaymentsSearchViewController *weakSelf = self;
        self.paymentsViewController.didChangeSearchText = ^(NSString *searchText) {
            NSLog(@"Searching in payments: %@ ...", searchText);
            
            [weakSelf search:searchText];
        };
    }
}

#pragma mark - Methods

- (void)search:(NSString *)text {
    if (!text || text.length == 0) {
        resultMyContracts = [NSMutableArray arrayWithArray:myContracts];
        resultProviders = [NSMutableArray arrayWithArray:providers];
        resultProviderCategories = [NSMutableArray arrayWithArray:providerCategories];
    } else {
        text = [text lowercaseString];
        
        // Search contracts
        if(resultMyContracts) {
            resultMyContracts = nil;
        }
        if (myContracts) {
            NSPredicate *contractPredictate = [NSPredicate predicateWithFormat:@"alias CONTAINS[CD] %@ OR contract CONTAINS[CD] %@", text, text];
            resultMyContracts = [myContracts filteredArrayUsingPredicate:contractPredictate].mutableCopy;
        }
        
        // Search providers
        if(resultProviders) {
            resultProviders = nil;
        }
        if (providers) {
            NSPredicate *providersPredictate = [NSPredicate predicateWithFormat:@"name CONTAINS[CD] %@", text];
            resultProviders = [providers filteredArrayUsingPredicate:providersPredictate].mutableCopy;
        }
        
        if(resultProviderCategories) {
            resultProviderCategories = nil;
        }
        if(providerCategories) {
            NSPredicate *providersPredictate = [NSPredicate predicateWithFormat:@"categoryName CONTAINS[CD] %@", text];
            resultProviderCategories = [providerCategories filteredArrayUsingPredicate:providersPredictate].mutableCopy;
        }
    }
    
    [self setArray:resultMyContracts forKey:kMyContractsKey];
    [self setArray:resultProviders forKey:kProvidersKey];
    [self setArray:resultProviderCategories forKey:kProviderCategories];
    
    [list reloadData];
    
    if([[resultsDic allKeys] count] > 0) {
        if(noResultLabel) {
            [noResultLabel removeFromSuperview];
            noResultLabel = nil;
        }
    } else {
        if(!noResultLabel) {
            noResultLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, list.width, 100)];
            noResultLabel.backgroundColor = [UIColor clearColor];
            noResultLabel.font = [UIFont helveticaNeue:15];
            noResultLabel.textColor = [UIColor grayColor];
            noResultLabel.lineBreakMode = NSLineBreakByWordWrapping;
            noResultLabel.numberOfLines = 0;
            noResultLabel.textAlignment = NSTextAlignmentCenter;
            noResultLabel.text = NSLocalizedString(@"nothing_was_found", nil);
            [self.view addSubview:noResultLabel];
        }
    }
}

- (void)searchOnlyProviderCategories:(NSString *)text {
    if (!text || text.length == 0) {
        resultMyContracts = [NSMutableArray arrayWithArray:myContracts];
        resultProviders = [NSMutableArray arrayWithArray:providers];
        resultProviderCategories = [NSMutableArray arrayWithArray:providerCategories];
    } else {
        text = [text lowercaseString];
        
        // Search providers
        if(resultProviders) {
            resultProviders = nil;
        }
        if (providers) {
            NSPredicate *providersPredictate = [NSPredicate predicateWithFormat:@"name CONTAINS[CD] %@", text];
            resultProviders = [providers filteredArrayUsingPredicate:providersPredictate].mutableCopy;
        }
        
        if(resultProviderCategories) {
            resultProviderCategories = nil;
        }
        if(providerCategories) {
            NSPredicate *providersPredictate = [NSPredicate predicateWithFormat:@"categoryName CONTAINS[CD] %@", text];
            resultProviderCategories = [providerCategories filteredArrayUsingPredicate:providersPredictate].mutableCopy;
        }
    }
    
    [self setArray:resultProviders forKey:kProvidersKey];
    [self setArray:resultProviderCategories forKey:kProviderCategories];
    
    [list reloadData];
    
    if([[resultsDic allKeys] count] > 0) {
        if(noResultLabel) {
            [noResultLabel removeFromSuperview];
            noResultLabel = nil;
        }
    } else {
        if(!noResultLabel) {
            noResultLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, list.width, 100)];
            noResultLabel.backgroundColor = [UIColor clearColor];
            noResultLabel.font = [UIFont helveticaNeue:15];
            noResultLabel.textColor = [UIColor grayColor];
            noResultLabel.lineBreakMode = NSLineBreakByWordWrapping;
            noResultLabel.numberOfLines = 0;
            noResultLabel.textAlignment = NSTextAlignmentCenter;
            noResultLabel.text = NSLocalizedString(@"nothing_was_found", nil);
            [self.view addSubview:noResultLabel];
        }
    }
}

#pragma mark - Helpers

-(void)setArray:(id)arr forKey:(NSString *)key {
    if(arr) resultsDic[key] = arr;
    else [resultsDic removeObjectForKey:key];
}

#pragma mark - UIScrollView

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.delegate respondsToSelector:@selector(searchResultsViewHideKeyboard)]) {
        [self.delegate searchResultsViewHideKeyboard];
    }
}

#pragma mark - UITableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[resultsDic allKeys] count];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *keys = [resultsDic allKeys];
    NSArray *results = resultsDic[keys[section]];
    if(results) return [results count];
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSArray *keys = [resultsDic allKeys];
    NSArray *results = resultsDic[keys[(NSUInteger) section]];
    if (results.count == 0) {
        return 0;
    }
    return 36;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *title = @"";
    
    NSArray *keys = [resultsDic allKeys];
    NSString *key = keys[section];
    
    if([key isEqual:kMyContractsKey]) title = NSLocalizedString(@"my_contracts", nil);
    else if([key isEqual:kProvidersKey]) title = NSLocalizedString(@"service_providers", nil);
    else if([key isEqual:kProviderCategories]) title = NSLocalizedString(@"provider_categories", nil);
    
    if(![title isEqual:@""])
    {
        UIView *headerView = [UIView new];
        headerView.backgroundColor = [[UIColor fromRGB:0xffffff] colorWithAlphaComponent:0.8];
        
        UILabel *headerTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 8, [ASize screenWidth] - 2 * 15, 20)];
        headerTitleLabel.font = [UIFont boldSystemFontOfSize:14];
        headerTitleLabel.textColor = [UIColor fromRGB:0x146888];
        headerTitleLabel.text = title;
        [headerView addSubview:headerTitleLabel];
        
        return headerView;
    }
    
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [resultsDic allKeys][indexPath.section];
    
    if([key isEqual:kMyContractsKey]) {
        return [SideMenuContractTableCell cellHeight];
    } else if([key isEqual:kProvidersKey]) {
        PaymentProvider *provider = [resultsDic[key] objectAtIndex:indexPath.row];
        return [SearchResultIconTitleTableCell height:provider.name];
    } else if([key isEqual:kProviderCategories]) {
        ProviderCategory *category = [resultsDic[key] objectAtIndex:indexPath.row];
        return [SearchResultIconTitleTableCell height:category.categoryName];
    }
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = nil;
    
    NSString *key = [resultsDic allKeys][indexPath.section];
    
    if([key isEqual:kMyContractsKey])
    {
        CellIdentifier = @"SideMenuContractTableCell";
        SideMenuContractTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[SideMenuContractTableCell alloc] initWithReuseIdentifier:CellIdentifier];
        }
        
        Contract *contract = (Contract *)[resultsDic[key] objectAtIndex:indexPath.row];
        [cell setData:contract];
        
        return cell;
    }
    else if([key isEqual:kProvidersKey])
    {
        CellIdentifier = @"providerTableCell";
        SearchResultIconTitleTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[SearchResultIconTitleTableCell alloc] initWithReuseIdentifier:CellIdentifier];
        }
        
        PaymentProvider *provider = [resultsDic[key] objectAtIndex:indexPath.row];
        [cell setIcon:provider.logo];
        [cell setTitle:provider.name];
        
        return cell;
    }
    else if([key isEqual:kProviderCategories])
    {
        CellIdentifier = @"providerCategoryTableCell";
        SearchResultIconTitleTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[SearchResultIconTitleTableCell alloc] initWithReuseIdentifier:CellIdentifier];
        }
        
        ProviderCategory *category = [resultsDic[key] objectAtIndex:indexPath.row];
        [cell setIcon:category.categoryLogo];
        [cell setTitle:category.categoryName];
        
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
//    if(![Connection isReachableM]) {
//        [tableView deselectRowAtIndexPath:indexPath animated:YES];
//        return;
//    }
    
    NSString *key = [resultsDic allKeys][indexPath.section];
    
    if([key isEqual:kMyContractsKey])
    {
        Contract *contract = (Contract *)[resultsDic[key] objectAtIndex:indexPath.row];
        if(contract) {
            if([contract.provider isProviderAvailableForPayment]) {
                PaymentFormViewController *vc = [PaymentFormViewController createVC];
                vc.actionType = ActionTypeMakePayment;
                vc.contract = contract;
                [self.paymentsViewController.navigationController pushViewController:vc animated:true];
            } else {
                [UIHelper showAlertContractNotValidForPayment];
            }
        }
    }
    else if([key isEqual:kProvidersKey])
    {
        PaymentProvider *provider = [resultsDic[key] objectAtIndex:indexPath.row];
        if(provider) {
            PaymentFormViewController *vc = [PaymentFormViewController createVC];
            vc.actionType = ActionTypeNewPayment;
            vc.paymentProvider = provider;
            [self.paymentsViewController.navigationController pushViewController:vc animated:true];
        }
    }
    else if([key isEqual:kProviderCategories])
    {
        PaymentProvidersByCategory *providerList = ProvidersApi.paymentProvidersByCategories[indexPath.row];
        ProviderCategory *category = [resultsDic[key] objectAtIndex:indexPath.row];
        if(category) {
            CategoryProvidersListViewController *v = [[CategoryProvidersListViewController alloc] init];
            //v.contractsViewController = contractsViewController;
            v.title = category.categoryName;
            v.paymentProviders = providerList.paymentProviderListWS;
            [self.paymentsViewController.navigationController pushViewController:v animated:true];
        }
    }
}

- (UITableView *)getList {
    return list;
}

#pragma mark - Config UI

- (void)configUI {
    self.view.backgroundColor = [UIColor whiteColor];
    
    list = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height) style:UITableViewStylePlain];
    list.translatesAutoresizingMaskIntoConstraints = false;
    list.delegate = self;
    list.dataSource = self;
    list.backgroundColor = [UIColor whiteColor];
    list.separatorColor = [UIColor clearColor];
    list.rowHeight = 50;
    [self.view addSubview:list];
    [list autoPinEdgesToSuperviewEdges];
    
    resultsDic = [[NSMutableDictionary alloc] init];
}

#pragma mark -

-(void)dealloc {
    list = nil;
}


@end
