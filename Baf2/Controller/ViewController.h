//
//  ViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 3/28/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PinAuthView.h"
#import "TitledSwitchView.h"

/**
 * @brief Authorization view controller
 */
@interface ViewController : UIViewController

+ (instancetype)createVC;

@property (nonatomic, assign) BOOL showAsModal;

@end
