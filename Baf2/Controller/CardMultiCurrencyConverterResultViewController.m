//
//  CardMultiCurrencyConverterResultViewController.m
//  BAF
//
//  Created by Almas Adilbek on 6/10/15.
//
//

#import "CardMultiCurrencyConverterResultViewController.h"
#import "TransferCurrencyRates.h"
#import "AATableSection.h"
#import "AASimpleTableViewCell.h"
#import "UIViewController+NavigationController.h"
#import "Card.h"
#import "MainButton.h"
#import "NotificationCenterHelper.h"

@interface CardMultiCurrencyConverterResultViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *list;
@end

@implementation CardMultiCurrencyConverterResultViewController {
    UIView *footerButtonView;
    NSMutableArray *sections;
}

- (id)init
{
    self = [super init];
    if(self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CardMultiCurrencyConverter" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([CardMultiCurrencyConverterResultViewController class])];
        [self initVars];
    }
    return self;
}

-(void)initVars {
    sections = [[NSMutableArray alloc] init];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];

    AATableSection *section0 = [[AATableSection alloc] initWithSectionId:@"section0"];
    section0.headerTitle = [NSString stringWithFormat:@"%@%@", NSLocalizedString(@"detail_operation_number", nil),self.operationId];
    [section0 addRow:@{@"title": NSLocalizedString(@"bank_transfers_lowercase", nil), @"value": NSLocalizedString(@"convert_multi_card", nil)}];
    [sections addObject:section0];

    AATableSection *section1 = [[AATableSection alloc] initWithSectionId:@"section1"];
    section1.headerTitle = NSLocalizedString(@"your_operation_is_in_treatment", nil);
    [section1 addRow:@{@"title" : NSLocalizedString(@"multi_card", nil), @"value" : self.cardCurrencyAccount.number}];
    [section1 addRow:@{@"title" : NSLocalizedString(@"from_curr_to_curr", nil), @"value" : [NSString stringWithFormat:@"%@ → %@", self.fromCurrency, self.toCurrency]}];
    [section1 addRow:@{@"title" : NSLocalizedString(@"amount", nil), @"value" : [NSString stringWithFormat:@"%@ %@", [_amount decimalFormatString], self.fromCurrency]}];
    [section1 addRow:@{@"title" : NSLocalizedString(@"operation_commission", nil), @"value" : [NSString stringWithFormat:@"%@ %@", [self.comission decimalFormatString], self.comissionCurrency]}];

    NSString *formattedRatesInfo = [self.currencyRates presentation];
    formattedRatesInfo = [formattedRatesInfo stringByReplacingOccurrencesOfString:@"</br>" withString:@"\n"];
    formattedRatesInfo = [formattedRatesInfo stringByReplacingOccurrencesOfString:@"<b>" withString:@""];
    formattedRatesInfo = [formattedRatesInfo stringByReplacingOccurrencesOfString:@"</b>" withString:@""];
    [section1 addRow:@{@"title" : NSLocalizedString(@"convert_rate", nil), @"value" : formattedRatesInfo}];

    [sections addObject:section1];

    [self.list reloadData];

    // Show the toast.
    [self performBlock:^{
        [Toast showToast:NSLocalizedString(@"convert_in_treatment", nil)];
    } afterDelay:0.75];
}

#pragma mark -
#pragma mark Actions

- (void)gotoOperations {
    [self popToRoot];

    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferDoneGotoHistory object:nil];
}

- (void)gotoRoot {
    [self popToRoot];

    if (self.saveAsTemplate) {
        NSLog(@"saveAsTemplate");
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransfersTemplatesUpdated object:nil];
    }
}

#pragma mark -
#pragma mark UITableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return sections.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    AATableSection *theSection = sections[(NSUInteger) section];
    return theSection.rows.count;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    AATableSection *theSection = sections[(NSUInteger) section];
    return theSection.headerTitle;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if(section == 1) {
        return 130;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(section == 1) {
        if(!footerButtonView) {
            footerButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [self tableView:tableView heightForFooterInSection:section])];

            // Button
            MainButton *mainButton = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, 190, 40)];
            mainButton.mainButtonStyle = MainButtonStyleOrange;
            mainButton.frame = CGRectMake(0, 0, 190, 40);
            [mainButton addTarget:self action:@selector(gotoRoot) forControlEvents:UIControlEventTouchUpInside];
            mainButton.centerX = footerButtonView.middleX;
            mainButton.y = 15;
            [mainButton setTitle:NSLocalizedString(@"go_to_main_activity", nil)];
            [footerButtonView addSubview:mainButton];


            MainButton *historyButton = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, 190, 40)];
            historyButton.mainButtonStyle = MainButtonStyleOrange;
            historyButton.frame = CGRectMake(0, 0, 190, 40);
            [historyButton addTarget:self action:@selector(gotoOperations) forControlEvents:UIControlEventTouchUpInside];
            historyButton.centerX = footerButtonView.middleX;
            historyButton.y = mainButton.bottom + 5;
            [historyButton setTitle:NSLocalizedString(@"operation_history", nil)];
            [footerButtonView addSubview:historyButton];


        }
        return footerButtonView;
    }

    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AATableSection *theSection = sections[(NSUInteger) indexPath.section];

    NSDictionary *row = theSection.rows[(NSUInteger) indexPath.row];
    NSString *title = row[@"title"];
    NSString *subtitle = row[@"value"];
    return [AASimpleTableViewCell cellHeightWithTitle:title subtitle:subtitle];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    AASimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AASimpleTableViewCell alloc] initWithReuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.font = [UIFont boldSystemFontOfSize:15];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    AATableSection *theSection = sections[(NSUInteger) indexPath.section];
    NSDictionary *row = theSection.rows[(NSUInteger) indexPath.row];

    [cell setTitle:row[@"title"]];
    [cell setSubtitle:row[@"value"]];
    
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

    return cell;
}

#pragma mark -

- (void)configUI {
    [self setBafBackground];

    [self setNavigationTitle:NSLocalizedString(@"operation_stay_in_treatment", nil)];
    [self hideBackButton];
    [self showMenuButton];
    
    self.list.backgroundColor = [UIColor clearColor];
    self.list.backgroundView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {

}

@end
