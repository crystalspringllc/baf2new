//
//  MessageSupportViewController.m
//  Baf2
//
//  Created by nmaksut on 19.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "MessageSupportViewController.h"
#import "NewsApi.h"
#import "JSQMessagesBubbleImage.h"
#import "JSQMessagesBubbleImageFactory.h"
#import "JSQMessagesAvatarImageFactory.h"
#import "BAFChatPhotoItem.h"
#import "NSString+HTML.h"
#import "TOCropViewController.h"
#import "MWPhotoBrowser.h"
#import <ChameleonFramework/ChameleonMacros.h>
#import "NSString+Ext.h"



@interface MessageSupportViewController () <UIActionSheetDelegate, TOCropViewControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, MWPhotoBrowserDelegate>
@property (nonatomic, assign) BOOL fromMenu;
@property (nonatomic, strong) NSMutableArray *chatImages;
@end

#define scrollOffsetZero -self.disclamer.viewSize.height - 10

@implementation MessageSupportViewController {
    NSMutableArray *messages;
    NSMutableArray *messageCells;
    NSInteger appearCount;
    NSString *lastDate;
    UIImage *userAvatarImage;
    NSIndexPath *selectedIndexPath;
}

- (void)initVars {
    messages = [NSMutableArray new];
    messageCells = [NSMutableArray new];
    appearCount = 0;
}

#pragma mark -
#pragma mark - Lifestyle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initVars];
    [self configUI];

    self.fromMenu = YES;

    if (self.fromMenu) {
        [messages removeAllObjects];
        [messageCells removeAllObjects];
        lastDate = nil;
        [self.collectionView reloadData];
    }
    appearCount ++;
    [self loadMessages];
}

- (void)loadMessages {
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"getting_message", nil) animated:YES];
    [NewsApi getPrivateNewsWithLast:lastDate success:^(id response) {
        NSArray *arr = [response objectForKey:@"news"];
        NSString *last = response[@"last"];
        if (last.length > 0) {
            lastDate = last;
        }
        
        [self collectMessages:arr];
        [self finishReceivingMessage];

        if (self.fromMenu) {
            self.fromMenu = NO;
            [self scrollToBottomAnimated:YES];
        }

        [MBProgressHUD hideAstanaHUDAnimated:YES];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

- (void)reloadMessages {
    [messages removeAllObjects];
    [messageCells removeAllObjects];
    lastDate = nil;
    [self.chatImages removeAllObjects];
    [self loadMessages];
}

- (void)showCamera {
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:controller animated:YES completion:NULL];
    } else {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"device_not_support_camera", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
}

- (void)openPhotoAlbum {
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    
    [self presentViewController:controller animated:YES completion:NULL];
}

- (void)didPressAccessoryButton:(id )sender {
    [self.inputToolbar.contentView.textView resignFirstResponder];
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"send_photo", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"cancellation", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"choose_from_album", nil), NSLocalizedString(@"make_a_photo", nil), nil];
    
    [sheet showFromToolbar:self.inputToolbar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        [self.inputToolbar.contentView.textView becomeFirstResponder];
        return;
    }
    
    switch (buttonIndex) {
        case 0:
            [self openPhotoAlbum];
            break;
        case 1:
            [self showCamera];
            break;
        default:
            break;
    }
    
    // [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    [self finishSendingMessageAnimated:YES];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    __weak MessageSupportViewController *wSelf = self;
    
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [wSelf cropPickedImage:image];
    }];
}

- (void)cropPickedImage:(UIImage *)image {
    if (image) {
        TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:image];
        cropViewController.delegate = self;
        [self presentViewController:cropViewController animated:YES completion:nil];
    }
}

#pragma mark - TOCropViewController

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading_photo", nil) animated:true];
    
    [cropViewController dismissViewControllerAnimated:YES completion:nil];
    
    NSMutableArray *imageUrls = [NSMutableArray new];
    NSMutableArray *addedImages = [NSMutableArray new];
    
    [NewsApi storeImage:image success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        [imageUrls addObject:response];
        [addedImages addObject:image];
        
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"sending", nil) animated:YES];
        [NewsApi addPostWithSubject:@"" message:@"" share:@"support" imageUrls:imageUrls success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:NO];
            
            [self finishSendingMessageAnimated:YES];
            
            [messages removeAllObjects];
            [messageCells removeAllObjects];
            lastDate = nil;
            [self.collectionView reloadData];
            [self loadMessages];
            self.fromMenu = YES;

        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
        }];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
    }];
}



#pragma mark - UICollectionView DataSource

- (NSString *)senderId {
    return @([User sharedInstance].userID).stringValue;
}

- (NSString *)senderDisplayName {
    return NSLocalizedString(@"you", nil);
}

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [messageCells objectAtIndex:indexPath.item];
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return messageCells.count;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    News *message = [messageCells objectAtIndex:indexPath.item];
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"sending_message", nil) animated:true];
    [NewsApi addPostWithSubject:@"" message:text share:@"support" imageUrls:nil success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        [self finishSendingMessageAnimated:YES];

        [messages removeAllObjects];
        [messageCells removeAllObjects];
        lastDate = nil;
        [self.collectionView reloadData];
        [self loadMessages];
        self.fromMenu = YES;
        if(!self.disclamer.hidden && messages.count >= 1) self.disclamer.hidden = YES;
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    News *message = [messageCells objectAtIndex:indexPath.item];

    if (![message.senderId isEqualToString:self.senderId]) {
        return [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor flatWhiteColor]];

    } else {
        return [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor flatMintColor]];

    }
    
    return nil;
}

#pragma mark - JSQ Delegate

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Delete message at index path - %@", indexPath);
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    News *message = [messageCells objectAtIndex:indexPath.item];
    if ([message.senderId isEqualToString:self.senderId]) {
        if (userAvatarImage) {
            return (id)[JSQMessagesAvatarImageFactory avatarImageWithImage:userAvatarImage diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
        }
        
        return (id)[JSQMessagesAvatarImageFactory avatarImageWithUserInitials:NSLocalizedString(@"you", nil) backgroundColor:[UIColor grayColor] textColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:14] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    JSQMessage *message = [messageCells objectAtIndex:indexPath.item];
    NSDate *messageDate = message.date;
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"dd.MM.yyyy HH:mm"];
    NSString *messageDateString = [df stringFromDate:messageDate];
    if ([messageDate isLaterThanOrEqualTo:[[NSDate date] dateBySubtractingDays:1]]) {
        messageDateString = [messageDate timeAgoSinceNow];
    }
    NSDictionary *attributes = @{
            NSFontAttributeName: [UIFont systemFontOfSize:12],
            NSForegroundColorAttributeName: [UIColor flatGrayColor]
    };

    NSAttributedString *as = [[NSAttributedString alloc] initWithString:messageDateString attributes:attributes];

    return as ? : @"";
}


- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    JSQMessagesCollectionViewCell *cell = [super collectionView:collectionView cellForItemAtIndexPath:indexPath];

    int userId = (int)[User sharedInstance].userID;
    JSQMessage *msg = messageCells[(NSUInteger) indexPath.item];

    if ([msg.senderId intValue] == userId) {
        cell.textView.textColor = [UIColor whiteColor];
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor],
                NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    } else {
        cell.textView.textColor = [UIColor blackColor];
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : [UIColor blackColor],
                NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }

    return cell;
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath {
    JSQMessage *message = messageCells[indexPath.row];

    if (self.chatImages.count > 0 && message.media) {
        MWPhotoBrowser *vc = [[MWPhotoBrowser alloc] initWithPhotos:self.chatImages];
        vc.delegate = self;
        vc.enableGrid = YES;
        vc.alwaysShowControls = YES;
        vc.enableSwipeToDismiss = YES;
        vc.displayNavArrows = YES;
        vc.startOnGrid = YES;
        vc.displayActionButton = YES;
        [vc setCurrentPhotoIndex:0];
        [vc setNavigationBackButton];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.chatImages.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    return [[MWPhoto alloc] initWithURL:[NSURL URLWithString: self.chatImages[index]]];;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender {
    [self loadMessages];
}

- (void)collectMessages:(NSArray *)arr {
    
    NSMutableArray *newArrCells = [NSMutableArray new];

    for (NSInteger i = arr.count-1; i>=0; i--) {

        News *news = arr[i];
        
        JSQMessage *msg = [[JSQMessage alloc] initWithSenderId:news.src.newsSrcId.stringValue
                                             senderDisplayName:news.src.nick
                                                          date:news.formattedDate
                                                          text:[self getPlainHtml:news.info.message]];
        if (news.images && news.images.count > 0) {
            BAFChatPhotoItem *photoItem = [[BAFChatPhotoItem alloc] initWithURL:[news.images firstObject]];
            msg = [[JSQMessage alloc] initWithSenderId:news.src.newsSrcId.stringValue senderDisplayName:news.src.nick date:news.formattedDate media:photoItem];;
        }
        
        [newArrCells addObject:msg];
        
        for (Comment *comment in news.comments) {
            
            JSQMessage *msg = [[JSQMessage alloc] initWithSenderId:comment.src.commentSrcId.stringValue
                                                 senderDisplayName:comment.src.nick
                                                              date:comment.formattedDate
                                                              text:[self urllinkFromHTML:comment.info.message]];

            if (comment.images && comment.images.count > 0) {
                BAFChatPhotoItem *photoItem = [[BAFChatPhotoItem alloc] initWithURL:[comment.images firstObject]];

                msg = [[JSQMessage alloc] initWithSenderId:comment.src.commentSrcId.stringValue
                                         senderDisplayName:comment.src.nick
                                                      date:comment.formattedDate
                                                     media:photoItem];
            }

            [newArrCells addObject:msg];
        }
    }
    
    NSArray *oldArr = [messages mutableCopy];
    [messages removeAllObjects];
    [messages addObjectsFromArray: [[arr reverseObjectEnumerator] allObjects]];
    [messages addObjectsFromArray:oldArr];

    NSArray *oldArrCells = [messageCells mutableCopy];
    [messageCells removeAllObjects];
    [messageCells addObjectsFromArray:newArrCells];
    [messageCells addObjectsFromArray:oldArrCells];


    if (!self.chatImages) {
        self.chatImages = [[NSMutableArray alloc] init];
    }
    [self collectImagesInNews:arr];

}

- (void)collectImagesInNews:(NSArray *)newsArray {
    for (News *newsItem in newsArray) {

        if (newsItem.images.count > 0) {
            for (NSString *imageUrl in newsItem.images) {
                [self.chatImages addObject:imageUrl];
            }
        }

        for (Comment *comment in newsItem.comments) {

            if (comment.images.count > 0) {
                for (NSString *imageUrl in comment.images) {
                    [self.chatImages addObject:imageUrl];
                }
            }
        }
    }
}

- (NSString *)getPlainHtml:(NSString *)html {
    return [html stringByConvertingHTMLToPlainText];
}

#pragma mark - scroll decelerating

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (self.disclamer && scrollView.contentOffset.y == scrollOffsetZero) {
        [UIView animateWithDuration:0.2 animations:^{
            self.disclamer.alpha = 1;
        } completion:nil];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if (self.disclamer) {
        [UIView animateWithDuration:0.2 animations:^{
            self.disclamer.alpha = 0;
        } completion:nil];
    }
}

#pragma mark - config ui

- (void)configUI {

    [self setBafBackground];
    [self showMenuButton];
    [self setNavigationTitle:NSLocalizedString(@"chat_with_bank", nil)];
    [self hideBackButtonTitle];
    
    self.collectionView.backgroundView = nil;
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reloadMessages)];
    
    self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    [self setShowLoadEarlierMessagesHeader: YES];
    self.automaticallyScrollsToMostRecentMessage = YES;
    
    self.inputToolbar.contentView.textView.placeHolder = NSLocalizedString(@"Новое сообщение", nil);
    [self.inputToolbar.contentView.rightBarButtonItem setTitle:NSLocalizedString(@"sendd", nil) forState:UIControlStateNormal];
    
    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[User sharedInstance].avatarImageUrlString] options:0 progress:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        if (image) {
            userAvatarImage = image;
            [self.collectionView reloadData];
        }
    }];


    self.disclamer = [[UIView alloc] initWithFrame:CGRectMake(0,0,[ASize screenWidth],100)];
    self.disclamer.backgroundColor = [[UIColor flatYellowColorDark] colorWithAlphaComponent:0.8];
    UILabel *label = [[UILabel alloc] init];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.text = NSLocalizedString(@"chat_warning", nil);
    label.width = self.disclamer.width - 20 - 40;
    label.x = 10;
    label.y = 5;
    label.font = [UIFont systemFontOfSize:13];
    label.textColor = [UIColor flatNavyBlueColorDark];
    [label sizeToFit];
    UIButton *closedisclamer = [UIButton buttonWithType:UIButtonTypeCustom];
    closedisclamer.x = self.disclamer.right - 40;
    closedisclamer.y = 0;
    closedisclamer.width = 40;
    closedisclamer.height = 40;
    [closedisclamer setImage:[UIImage imageNamed:@"btn_close_normal"] forState:UIControlStateNormal];
    [closedisclamer addTarget:self action:@selector(clickCloseDisclamer) forControlEvents:UIControlEventTouchUpInside];
    [self.disclamer addSubview:label];
    [self.disclamer addSubview:closedisclamer];
    self.disclamer.height = label.height + 10;
    [self.view addSubview:self.disclamer];

    [self.collectionView setContentInsetTop:self.disclamer.height + 10];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (void)clickCloseDisclamer {
    if (self.disclamer) {
        [self.disclamer removeFromSuperview];
    }
}

#pragma mark - helper

- (NSString *)urllinkFromHTML:(NSString *)htmlString {

    NSAttributedString *attr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUTF8StringEncoding]
                                                                options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                        NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)}
                                                     documentAttributes:nil
                                                                  error:nil];
    return attr.string ? : @"";

//    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
//    NSString *ahref = @"<a href=\"";
//    NSString *target = @"\" target";
//
//    if ([htmlString containsString:target]) {
//        NSArray *parts = [htmlString componentsSeparatedByString:target];
//        if (parts.count > 0) {
//            NSString *part1 = parts[0];
////            NSRange range = NSMakeRange(@"<a href=\"".length, part1.length - ahref.length);
////            NSString *substringed = [htmlString substringWithRange:range];
//            return part1;
//        } else {
//            return htmlString;
//        }
//    } else {
//        return htmlString;
//    }
}

@end
