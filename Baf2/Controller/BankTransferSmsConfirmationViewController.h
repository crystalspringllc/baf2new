//
//  BankTransferSmsConfirmationViewController.h
//  BAF
//
//  Created by Almas Adilbek on 3/13/15.
//
//

#import "NewCheckAndInitTransferResponse.h"

@interface BankTransferSmsConfirmationViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NewCheckAndInitTransferResponse *checkTarnsferResponse;
@property (nonatomic, strong) NSNumber *servicelogId;
@property(nonatomic, strong) id fromAccount;
@property(nonatomic, strong) id toAccount;
@property(nonatomic, copy) NSNumber *amount;
@property(nonatomic, copy) NSString *currency;
@property(nonatomic, copy) NSString *convertedAmount;
@property(nonatomic, copy) NSNumber *commissionAmount;
@property(nonatomic, copy) NSString *commissionCurrency;
@property(nonatomic) BOOL isPayBox;
@property (nonatomic) BOOL saveAsTemplate;

@end
