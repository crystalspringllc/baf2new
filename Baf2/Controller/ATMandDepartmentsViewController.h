//
//  ATMandDepartmentsViewController.h
//  BAF
//
//  Created by Almas Adilbek on 5/15/15.
//
//



#import "ObjectsMapView.h"
#import "UIScrollView+EmptyDataSet.h"

@interface ATMandDepartmentsViewController : UIViewController <MKMapViewDelegate, ObjectsMapViewDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@end
