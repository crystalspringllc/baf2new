//
//  PaymentPaycheckViewController.m
//  BAF
//
//  Created by Almas Adilbek on 5/25/15.
//
//

#import "PaymentPaycheckViewController.h"
#import "Paycheck.h"
#import "Contract.h"
#import "NotificationCenterHelper.h"
#import "UIViewController+NavigationController.h"
#import "MainButton.h"
#import "PaymentReceiptView.h"

@interface PaymentPaycheckViewController ()
@end

@implementation PaymentPaycheckViewController

-(id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

-(void)initVars {}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self showPaycheckWithPaycheck:self.paycheck];
    [self addObservers];
}

#pragma mark -
#pragma mark Actions

-(void)gotoMyPayments {
    [self popToRoot:NO];
}

- (void)gotoHome {
    [self popToRoot:NO];
}

- (void)gotoLastOperations {
    [self popToRoot];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferDoneGotoHistory object:nil];
}

#pragma mark -
#pragma mark Notifications

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self]; // sometimes dealloc not called, therefore remove previous observers if exist

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationAnotherTabOpenned:) name:kTabbarViewAnotherTabSelectedNotification object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTabbarViewAnotherTabSelectedNotification object:nil];
}

- (void)notificationAnotherTabOpenned:(id)notification {
    [self popToRoot];
}

#pragma mark -

- (void)showPaycheckWithPaycheck:(Paycheck *)paycheck {
    CGFloat viewWidth = 280;
    UIView *conview = [UIHelper successMessageWithIconWithContentWidth:viewWidth message:NSLocalizedString(@"authorization_service_tells_abount_successfull_card_withdraw", nil)];

    __weak PaymentPaycheckViewController * weakSelf = self;

    switch (_epayBrowserPaymentType)
    {
        case EpayBrowserPaymentTypeDefault:
        {
            [self drawPaymentCheck:paycheck viewWidth:viewWidth conview:conview weakSelf:weakSelf isReference:YES];

        };
            break;
        case EpayBrowserPaymentTypeRequestCard:
        {
            // Message
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 270, 1)];
            messageLabel.backgroundColor = [UIColor clearColor];
            messageLabel.font = [UIFont ptSansCaption:14];
            messageLabel.textColor = [UIColor blackColor];
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
            messageLabel.numberOfLines = 0;
            messageLabel.text = [NSString stringWithFormat:@"Уважаемый клиент!\nВаша заявка №%@ находиться в обработке.\nСтоимость услуги: %.02f KZT.\nУслуга «Выпуск карты» успешно оплачена.\nОжидайте, с вами свяжется сотрудник Банка.", paycheck.orderId, [paycheck.amount floatValue]];
            [messageLabel sizeToFit];
            messageLabel.y = conview.bottom + 5;
            [conview addSubview:messageLabel];

            // Buttons --
            // Goto main button
            MainButton *button1 = [[MainButton alloc] initWithFrame:CGRectMake(0, messageLabel.bottom + 25, conview.width, 40)];
            [button1 addTarget:self action:@selector(gotoHome) forControlEvents:UIControlEventTouchUpInside];
            button1.mainButtonStyle = MainButtonStyleOrange;
            button1.frame = CGRectMake(0, messageLabel.bottom + 25, conview.width, 40);
            button1.centerX = conview.middleX;
            [button1 setTitle:NSLocalizedString(@"go_to_main_activity", nil)];
            [conview addSubview:button1];

            // Last operations button
            MainButton *button2 = [[MainButton alloc] initWithFrame:CGRectMake(0, button1.bottom + 10, button1.width, button1.height)];
            [button2 addTarget:self action:@selector(gotoLastOperations) forControlEvents:UIControlEventTouchUpInside];
            button2.mainButtonStyle = MainButtonStyleOrange;
            button2.frame = CGRectMake(0, button1.bottom + 10, button1.width, button1.height);
            button2.centerX = conview.middleX;
            [button2 setTitle:NSLocalizedString(@"ga_operation_history", nil)];
            [conview addSubview:button2];

            conview.height = button2.bottom;
        };
            break;
        case EpayBrowserPaymentTypeP2PTransfer: {
            [self drawPaymentCheck:paycheck viewWidth:viewWidth conview:conview weakSelf:weakSelf isReference:NO];
        };
        break;
    }

    self.scrollView.contentSize = CGSizeMake(self.view.width, conview.height + 50);
}

- (void)drawPaymentCheck:(Paycheck *)paycheck
               viewWidth:(CGFloat)viewWidth
                 conview:(UIView *)conview
                weakSelf:(PaymentPaycheckViewController *)weakSelf
             isReference:(BOOL)isReference
{

    UIView *paymentReceiptContainerView = [UIView newAutoLayoutView];
    paymentReceiptContainerView.backgroundColor = [UIColor clearColor];
    
    // payment receipt
    PaymentReceiptView *paymentReceiptView = [[NSBundle mainBundle] loadNibNamed:@"PaymentReceiptView" owner:self options:nil].firstObject;
    paymentReceiptView.translatesAutoresizingMaskIntoConstraints = false;
    paymentReceiptView.titleLabel.text = NSLocalizedString(@"authorization_service_tells_abount_successfull_card_withdraw", nil);
    paymentReceiptView.amountValueLabel.text = [NSString stringWithFormat:@"%@ тг.", paycheck.amount];
    if (paycheck.contract.provider.name) {
        [paymentReceiptView addReceiptRowWithTitle:NSLocalizedString(@"provider", nil) value:paycheck.contract.provider.name];
    }
    if (paycheck.contract.alias) {
        [paymentReceiptView addReceiptRowWithTitle:NSLocalizedString(@"contract", nil) value:paycheck.contract.alias];
    }
    [paymentReceiptView addReceiptRowWithTitle:NSLocalizedString(@"order_numberr", nil) value:paycheck.orderId.stringValue];
    [paymentReceiptView addReceiptRowWithTitle:NSLocalizedString(@"date_payment", nil) value:[paycheck normalDate]];
    if (isReference) {
        [paymentReceiptView addReceiptRowWithTitle:NSLocalizedString(@"reference", nil) value:paycheck.reference.stringValue];
    }
    [paymentReceiptView addReceiptRowWithTitle:NSLocalizedString(@"card", nil) value:paycheck.cardNumber];
    [paymentReceiptView autoSetDimension:ALDimensionWidth toSize:300];
    [paymentReceiptContainerView addSubview:paymentReceiptView];
    [paymentReceiptView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:20];
    [paymentReceiptView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [paymentReceiptView autoPinEdgeToSuperviewEdge:ALEdgeRight];
    [paymentReceiptView setNeedsLayout];
    [paymentReceiptView layoutIfNeeded];
    
    // Pay other services button.
    MainButton *button1 = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, 240, 45)];
    button1.translatesAutoresizingMaskIntoConstraints = false;
    [button1 addTarget:self action:@selector(gotoMyPayments) forControlEvents:UIControlEventTouchUpInside];
    button1.mainButtonStyle = MainButtonStyleOrange;
    [button1 setTitle:NSLocalizedString(@"pay_services", nil)];
    [paymentReceiptContainerView addSubview:button1];
    [button1 autoSetDimensionsToSize:CGSizeMake(240, 45)];
    CGFloat button1Offset = [ASize screenHeight] < 500 ? 15 : 25;
    [button1 autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:paymentReceiptView withOffset:button1Offset];
    [button1 autoAlignAxisToSuperviewAxis:ALAxisVertical];
    
    // Goto operations history.
    MainButton *button2 = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, 240, 45)];
    button2.translatesAutoresizingMaskIntoConstraints = false;
    [button2 addTarget:self action:@selector(gotoLastOperations) forControlEvents:UIControlEventTouchUpInside];
    button2.mainButtonStyle = MainButtonStyleOrange;
    [button2 setTitle:NSLocalizedString(@"ga_operation_history", nil)];
    [paymentReceiptContainerView addSubview:button2];
    [button2 autoSetDimensionsToSize:CGSizeMake(240, 45)];
    [button2 autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:button1 withOffset:10];
    [button2 autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20];
    [button2 autoAlignAxisToSuperviewAxis:ALAxisVertical];
    
    [self.view addSubview:paymentReceiptContainerView];
    [paymentReceiptContainerView autoCenterInSuperview];
    [paymentReceiptContainerView setNeedsLayout];
    [paymentReceiptContainerView layoutIfNeeded];
}

#pragma mark -

-(void)configUI {
    [self setBafBackground];
    [self hideBackButton];
    [self showMenuButton];

    NSString *title = NSLocalizedString(@"successfull_pay", nil);
    if(self.epayBrowserPaymentType == EpayBrowserPaymentTypeRequestCard) {
        title = NSLocalizedString(@"application_successfully_accepted", nil);
    }
    [self setNavigationTitle:title];

    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeightWithoutStatusBarAndNavigationBar])];
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.alwaysBounceVertical = YES;
    [self.view addSubview:self.scrollView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc {
    [self removeObservers];
}

@end