//
//  DateIntervalPickerPopupViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 4/13/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "DateIntervalPickerPopupViewController.h"
#import "NSDate+Ext.h"
#import "NSString+Ext.h"
#import <STPopup/STPopup.h>

@interface DateIntervalPickerPopupViewController ()
@property (weak, nonatomic) IBOutlet UITextField *fromDateTextField;
@property (weak, nonatomic) IBOutlet UITextField *toDateTextField;
@property (weak, nonatomic) IBOutlet UILabel *datePickerLabel;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UITextField *choosenTextfield;
@end

@implementation DateIntervalPickerPopupViewController

- (id)init {
    self = [super init];
    if (self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DateIntervalPicker" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([DateIntervalPickerPopupViewController class])];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 144);
    self.landscapeContentSizeInPopup = CGSizeMake([ASize screenWidth], 144);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

    self.datePickerLabel.text = self.datePickerTitle;
}

#pragma makr - setter

- (void)setFromDate:(NSDate *)fromDate andToDate:(NSDate *)toDate {
    _fromDate = fromDate;
    _toDate = toDate;
}

#pragma mark - config action

- (IBAction)textFieldEditingBegin:(UITextField *)textField {
    self.choosenTextfield = textField;

    if ([textField isEqual:self.fromDateTextField]) {
        self.datePicker.date = self.fromDate;
    } else if ([textField isEqual:self.toDateTextField]) {
        self.datePicker.date = self.toDate;
    }

}

- (void)updateTextFieldDate:(UIDatePicker *)datePicker {
    NSDate *date = datePicker.date;
    NSString *dateString = [date stringWithDateFormat:@"dd.MM.yyyy"];
    self.choosenTextfield.text = dateString;

    self.fromDate = [self.fromDateTextField.text dateWithDateFormat:@"dd.MM.yyyy"];
    self.toDate = [self.toDateTextField.text dateWithDateFormat:@"dd.MM.yyyy"];
}

- (IBAction)clickDoneButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.completionBlock) {
            self.completionBlock(self.fromDate, self.toDate);
        }
    }];
}

#pragma mark - config ui

- (void)configUI {
    self.datePicker = [[UIDatePicker alloc] init];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    [self.datePicker addTarget:self action:@selector(updateTextFieldDate:) forControlEvents:UIControlEventValueChanged];

    self.fromDateTextField.inputView = self.datePicker;
    self.toDateTextField.inputView = self.datePicker;


    self.fromDateTextField.text = self.fromDate.ddMMyyyy;
    self.toDateTextField.text = self.toDate.ddMMyyyy;
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
