//
//  SMSConfirmationPopupViewController.h
//  BAF
//
//  Created by Askar Mustafin on 1/26/16.
//  Copyright © 2016 Банк Астаны. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperTextFieldView.h"


@interface SMSConfirmationPopupViewController : UIViewController

@property (weak, nonatomic) IBOutlet SuperTextFieldView *smsTextField;
@property (weak, nonatomic) IBOutlet UIButton *smsAgainButton;
@property (nonatomic, copy) void (^didDismissWithSuccessBlock)();

@property (nonatomic, copy) void (^verifySmsCode)(SMSConfirmationPopupViewController *_self, NSString *smsCode);
@property (nonatomic, copy) void (^sendSmsCodeAgain)(SMSConfirmationPopupViewController *_self);

- (void)showSuccessIcon;
- (void)dismiss;
- (void)smsTextFieldStopLoading;
@end
