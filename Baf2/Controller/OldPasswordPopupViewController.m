//
//  OldPasswordPopupViewController.m
//  BAF
//
//  Created by Askar Mustafin on 2/24/16.
//  Copyright © 2016 Банк Астаны. All rights reserved.
//

#import "OldPasswordPopupViewController.h"
#import "InputTextField.h"
#import "MainButton.h"
#import "NSString+Validators.h"

@interface OldPasswordPopupViewController ()
@property (nonatomic, strong) NSString *secretQuestion;
@end

@implementation OldPasswordPopupViewController

- (id)initWithSecretQuestion:(NSString *)secretQuestion {
    if (self) {
        self.contentSizeInPopup = CGSizeMake(kPopupWidth, 240);
        self.secretQuestion = secretQuestion;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUIWithSecretQuestion:self.secretQuestion];
}

#pragma mark - config ui

- (void)configUIWithSecretQuestion:(NSString *)secretQuestion {
    NSString *titleString;
    if (secretQuestion) {
        titleString = NSLocalizedString(@"for_making_changes_enter_passwrd_and_answer_secter_question", nil);
    } else {
        titleString = NSLocalizedString(@"for_making_changes_enter", nil);
    }

    UILabel *title = [[UILabel alloc] init];
    title.width = [ASize screenWidth] - 40;
    title.numberOfLines = 0;
    title.lineBreakMode = NSLineBreakByWordWrapping;
    title.text = titleString;
    title.font = [UIFont systemFontOfSize:13];
    [title sizeToFit];
    title.x = (CGFloat) ((kPopupWidth - title.width) * 0.5);
    title.y = 10;
    [self.view addSubview:title];

    self.passwordTextField = [[InputTextField alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth] - 40, 44)];
    [self.passwordTextField setPlaceholder:NSLocalizedString(@"enter_password", nil)];
    self.passwordTextField.y = title.bottom + 10;
    self.passwordTextField.x = (CGFloat) ((kPopupWidth - self.passwordTextField.width) * 0.5);
    [self.passwordTextField setPasswordField];
    [self.view addSubview:self.passwordTextField];


    if (secretQuestion) {
        UILabel *question = [[UILabel alloc] init];
        question.width = [ASize screenWidth] - 40;
        question.numberOfLines = 0;
        question.lineBreakMode = NSLineBreakByWordWrapping;
        question.text = self.secretQuestion;
        question.font = [UIFont systemFontOfSize:11];
        [question sizeToFit];
        question.x = (CGFloat) ((kPopupWidth - title.width) * 0.5);
        question.y = self.passwordTextField.bottom + 10;
        [self.view addSubview:question];

        self.secretQuestionTextField = [[InputTextField alloc]
                initWithFrame:CGRectMake(0, 0, [ASize screenWidth] - 40, 44)];
        [self.secretQuestionTextField setPlaceholder:NSLocalizedString(@"enter_answer", nil)];
        self.secretQuestionTextField.y = question.bottom + 2;
        self.secretQuestionTextField.x = (CGFloat) ((kPopupWidth - self.secretQuestionTextField.width) * 0.5);
        [self.view addSubview:self.secretQuestionTextField];
    }

    MainButton *cancelButton = [[MainButton alloc] initWithFrame:CGRectMake(0,0,[ASize screenWidth] - 40, 44)];
    [cancelButton addTarget:self action:@selector(dissmissNotConfirm) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.mainButtonStyle = MainButtonStyleOrange;
    cancelButton.frame = CGRectMake(0,0,[ASize screenWidth] - 40, 44);
    [cancelButton setTitle:NSLocalizedString(@"cancel", nil)];
    cancelButton.y = secretQuestion ? self.secretQuestionTextField.bottom + 20 : self.passwordTextField.bottom + 20;
    cancelButton.x = (CGFloat) ((kPopupWidth - cancelButton.width) * 0.5);
    [self.view addSubview:cancelButton];

    MainButton *confirmButton = [[MainButton alloc] initWithFrame:CGRectMake(0,0,[ASize screenWidth] - 40, 44)];
    [confirmButton addTarget:self action:@selector(dismissConfirm) forControlEvents:UIControlEventTouchUpInside];
    confirmButton.mainButtonStyle = MainButtonStyleOrange;
    confirmButton.frame = CGRectMake(0,0,[ASize screenWidth] - 40, 44);
    [confirmButton setTitle:NSLocalizedString(@"confirm", nil)];
    confirmButton.y = cancelButton.bottom + 10;
    confirmButton.x = (CGFloat) ((kPopupWidth - confirmButton.width) * 0.5);
    [self.view addSubview:confirmButton];

    self.contentSizeInPopup = CGSizeMake(kPopupWidth, confirmButton.bottom + 20);
}

- (void)dissmissNotConfirm {
    __weak OldPasswordPopupViewController *wSelf = self;

    [self.popupController dismissWithCompletion:^{
        if (wSelf.completeWithSuccess) {
            wSelf.completeWithSuccess(false, wSelf.passwordTextField.value, wSelf.secretQuestionTextField.value ? : nil);
        }
    }];
}

- (void)dismissConfirm {
    __weak OldPasswordPopupViewController *wSelf = self;
    
    if (![wSelf.passwordTextField.value validate]) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"confirm", nil)
                                    message:NSLocalizedString(@"for_making_changes_enter", nil)];
        return;
    }
    
    [self.popupController dismissWithCompletion:^{
        if (wSelf.completeWithSuccess) {
            wSelf.completeWithSuccess(true, wSelf.passwordTextField.value, wSelf.secretQuestionTextField.value ? : nil);
        }
    }];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end