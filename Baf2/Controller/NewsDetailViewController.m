//
//  NewsDetailViewController.m
//  Baf2
//
//  Created by nmaksut on 17.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NewsDetailViewController.h"
#import "NewsContentCell.h"
#import "NewsCommentCell.h"
#import "NewsImagesCell.h"
#import "NewsImageCollectionCell.h"
#import "MWPhotoBrowser.h"
#import "NewsAddCommentViewController.h"
#import "NotificationCenterHelper.h"
#import "NewsApi.h"
#import "RegistrationViewController.h"
#import "STPopupController+Extensions.h"
#import "UITableViewController+Extension.h"
#import "AstanaRefreshControl.h"

@interface NewsDetailViewController () <MWPhotoBrowserDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@end

@implementation NewsDetailViewController {
    UIPageControl *pageControl;
    AstanaRefreshControl *refreshControl;
}

#pragma mark -
#pragma mark - Init


+ (id)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"News" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self]; // sometimes dealloc not called, therefore remove previous observers if exist

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoLogin) name:kNotificationGotoLogin object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoRegister) name:kNotificationGotoRegistration object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedIn) name:kNotificationUserLoggedIn object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedIn) name:kNotificationUserRegistered object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadNews) name:kNotificationUserCommentAdded object:nil];
}

#pragma mark -
#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
    [self registerNotifications];
}


#pragma mark -
#pragma mark - Actions 

- (void)likeTappedButton:(UIButton *)button {
    if ([User sharedInstance].loggedIn) {
        if (!self.news.like) {
            [MBProgressHUD showAstanaHUDWithTitle:@"" animated:YES];
            [NewsApi likeObjectID:self.news.newsId type:self.news.type success:^(id response) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
                [self.news updateAfterLike:response];
                button.selected = self.news.like;
                [button setTitle:[NSString stringWithFormat:@"%@ ", self.news.likes.stringValue] forState:UIControlStateNormal];
            } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
            }];
        } else {
            [MBProgressHUD showAstanaHUDWithTitle:@"" animated:YES];
            [NewsApi dislikeObjectID:self.news.newsId type:self.news.type success:^(id response) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
                [self.news updateAfterLike:response];
                button.selected = self.news.like;
                [button setTitle:[NSString stringWithFormat:@"%@ ", self.news.likes.stringValue] forState:UIControlStateNormal];
            } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
            }];
        }
    }
}

- (void)shareTapped {
    NSString *textObject = [NSString stringWithFormat:@"%@\n%@", self.news.info.message, NSLocalizedString(@"bankastanatag", nil)];
    NSArray *activityItems = [NSArray arrayWithObjects:textObject, nil];
    
    UIActivityViewController *avc = [[UIActivityViewController alloc]
                                     initWithActivityItems:activityItems
                                     applicationActivities:nil];
    
    avc.excludedActivityTypes = @[UIActivityTypePostToWeibo, UIActivityTypeAssignToContact];
    avc.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray * returnedItems, NSError * activityError) {
        
    };
    
    [self presentViewController:avc animated:YES completion:nil];
}

- (void)likeComment:(Comment *)comment button:(UIButton *)button {
    if ([User sharedInstance].loggedIn) {
        if (!comment.like) {
            button.selected = YES;
            [NewsApi likeObjectID:comment.commentId type:comment.type success:^(id response) {
                [comment updateAfterLike:response];
                [button setTitle:[NSString stringWithFormat:@"%@ ", comment.likes] forState:UIControlStateNormal];
            } failure:^(NSString *code, NSString *message) {
                button.selected = NO;
            }];
        } else {
            button.selected = NO;
            [NewsApi dislikeObjectID:comment.commentId type:comment.type success:^(id response) {
                [comment updateAfterLike:response];
                [button setTitle:[NSString stringWithFormat:@"%@ ", comment.likes] forState:UIControlStateNormal];
            } failure:^(NSString *code, NSString *message) {
                button.selected = YES;
            }];
        }
    }
}

- (void)deleteComment:(Comment *)comment indexPath:(NSIndexPath *)indexPath {
    __weak NewsDetailViewController *weakSelf = self;

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"remove_comment", nil) preferredStyle:UIAlertControllerStyleAlert];
    alertController.popoverPresentationController.sourceView = self.tableView;
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *_Nonnull action) {
        if ([User sharedInstance].loggedIn) {


            [MBProgressHUD showAstanaHUDWithTitle:@"" animated:YES];
            [NewsApi deleteObjectID:comment.commentId type:comment.type success:^(id response) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];

                NSMutableArray *comments = [weakSelf.news.comments mutableCopy];
                [comments removeObjectAtIndex:indexPath.row];
                weakSelf.news.comments = comments;

                [weakSelf.tableView beginUpdates];
                [weakSelf.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [weakSelf.tableView endUpdates];
            }failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
            }];
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:true completion:nil];
}

- (void)refreshNews {
    [MBProgressHUD showAstanaHUDWithTitle:nil animated:YES];
    [NewsApi getNewsById:self.news.newsId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        
        self.news = response;
        
        [self.tableView reloadData];
        [self.tableView layoutIfNeeded];
        
        [refreshControl finishingLoading];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [refreshControl finishingLoading];
    }];
}

- (void)reloadNews {
    [NewsApi getNewsById:self.news.newsId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        
        self.news = response;
        
        [self.tableView reloadData];
        [self.tableView layoutIfNeeded];
        
        [refreshControl finishingLoading];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [refreshControl finishingLoading];
    }];
}

- (void)deleteNews {
    __weak NewsDetailViewController *weakSelf = self;

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"remove_news", nil) preferredStyle:UIAlertControllerStyleAlert];
    alertController.popoverPresentationController.sourceView = self.tableView;
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *_Nonnull action) {
        if ([User sharedInstance].loggedIn) {

            [MBProgressHUD showAstanaHUDWithTitle:@"" animated:YES];
            [NewsApi deleteObjectID:weakSelf.news.newsId type:weakSelf.news.type success:^(id response) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];

                [weakSelf.navigationController popViewControllerAnimated:true];

                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewsDeleted object:nil];
            } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
            }];
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:true completion:nil];
}

#pragma mark -
#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) return 3;
    else {
        return self.news.comments.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 && indexPath.section == 0) {
        if (self.news.images.count == 0) {
            return 0;
        } else {
            return UITableViewAutomaticDimension;
        }
    }else {
        return UITableViewAutomaticDimension;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    __weak typeof(self) wself = self;
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            NewsImagesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Images" forIndexPath:indexPath];
            cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
            cell.collectionView.delegate = self;
            cell.collectionView.dataSource = self;
            cell.pageControl.hidesForSinglePage = YES;
            cell.pageControl.numberOfPages = self.news.images.count;
            pageControl = cell.pageControl;
            
            [cell setNews:self.news];
            return cell;
            
        } else if (indexPath.row == 1) {
            NewsContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Content" forIndexPath:indexPath];
            cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
            [cell setNews:self.news];
            return cell;
        } else if (indexPath.row == 2) {
            NewsAuthorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Author" forIndexPath:indexPath];
            cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
            [cell setNews:self.news];
            __weak NewsAuthorCell *wcell = cell;
            cell.likeBlock = ^{
                [wself likeTappedButton:wcell.likesButton];
            };
            cell.shareBlock = ^{
                [wself shareTapped];
            };
            
            return cell;
        }
    } else {
        NewsCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Comment" forIndexPath:indexPath];
        __weak NewsCommentCell *wcell = cell;
        Comment *c = self.news.comments[indexPath.row];
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        [cell setComment:c];
        cell.likeBlock = ^{
            [wself likeComment:c button:wcell.likesButton];
        };
        cell.didTapTrashButton = ^() {
            [wself deleteComment:c indexPath:indexPath];
        };
        
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark - UIScrollView

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isMemberOfClass:[UICollectionView class]]) {
        CGFloat pageWidth = scrollView.frame.size.width;
        float fractionalPage = scrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        pageControl.currentPage = page;
    }
    
    if (scrollView == self.tableView) {
        [refreshControl scrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (scrollView == self.tableView) {
        [refreshControl scrollViewWillEndDragging];
    }
}

#pragma mark -
#pragma mark - CollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.news.images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NewsImageCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:self.news.images[indexPath.item]]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize cellSize = CGSizeMake([ASize screenWidth], 160);
    return cellSize;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MWPhotoBrowser *vc = [[MWPhotoBrowser alloc] initWithPhotos:self.news.images];
    vc.delegate = self;
    [vc setCurrentPhotoIndex:indexPath.item];
    [vc setNavigationBackButton];
    [self.navigationController pushViewController:vc animated:YES];
}



#pragma mark -
#pragma mark - MWPhotoBrowser

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.news.images.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    MWPhoto *photo = [[MWPhoto alloc] initWithURL:[NSURL URLWithString: self.news.images[index]]];
    return photo;
}


- (void)addComment {
    if ([User sharedInstance].loggedIn) {
        NewsAddCommentViewController *vc = [NewsAddCommentViewController createVC];
        vc.news = self.news;

        STPopupController *commentPopupController = [[STPopupController alloc] initWithRootViewController:vc];
        commentPopupController.dismissOnBackgroundTap = true;
        commentPopupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
        [commentPopupController presentInViewController:self];
    } else {
        [UIHelper showActionSheetAuthorizationRequiredWithTarget:self];
    }

}

- (void)gotoLogin {
    UINavigationController *navigationController = [UIHelper defaultNavigationController];
    ViewController *viewController = [ViewController createVC];
    viewController.showAsModal = YES;
    [navigationController setViewControllers:@[viewController]];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)gotoRegister {
    UINavigationController *navigationController = [UIHelper defaultNavigationController];
    RegistrationViewController *viewController = [[RegistrationViewController alloc] init];
    viewController.fromNews = YES;
    [navigationController setViewControllers:@[viewController]];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)userLoggedIn {
    //[self addComment];
}

#pragma mark -
#pragma mark - ConfigUI

- (void)configUI {
    [self setBAFTableViewBackground];
    [self hideBackButtonTitle];
    [self setNavigationBackButton];
    [self setNavigationTitle:self.news.src.nick];
    
    UIBarButtonItem *addComment = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"add_comment"] style:UIBarButtonItemStylePlain target:self action:@selector(addComment)];
    UIBarButtonItem *deleteNewsItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteNews)];
    
    if ([User sharedInstance].userID == [self.news.src.newsSrcId integerValue]) {
        self.navigationItem.rightBarButtonItems = @[addComment, deleteNewsItem];
    } else {
        if ([User sharedInstance].isLoggedIn) {
            self.navigationItem.rightBarButtonItem = addComment;
        }
    }

    refreshControl = [[AstanaRefreshControl alloc] init];
    refreshControl.target = self;
    refreshControl.action = @selector(refreshNews);
    [self.tableView addSubview:refreshControl];
    [self.tableView sendSubviewToBack:refreshControl];
    
    self.tableView.estimatedRowHeight = 200;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"newsdetail dealloc");
}

@end
