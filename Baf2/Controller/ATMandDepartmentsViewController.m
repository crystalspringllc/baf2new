//
//  ATMandDepartmentsViewController.m
//  BAF
//
//  Created by Almas Adilbek on 5/15/15.
//
//

#import "ATMandDepartmentsViewController.h"
#import "UIView+AAPureLayout.h"
#import "ContactsMapViewController.h"
#import "ContactsSQLiteManager.h"
#import "Atm.h"
#import "AtmsDataSource.h"
#import "AtmTableViewCell.h"
#import "BankBranch.h"
#import "BankBranchesDataSource.h"
#import "BankBranchTableViewCell.h"
#import "BankBranchTableViewCell+ConfigureForBankBranch.h"
#import "OCMapViewSampleHelpAnnotation.h"
#import "LocationHelper.h"
#import "AtmTableViewCell+ConfigureForAtm.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

#define kBafShortNumber @"2555"
static NSString * const AtmsCellIdentifier = @"AtmCell";
static NSString * const BankBranchesCellIdentifier = @"BankBranchCell";

@interface ATMandDepartmentsViewController () <UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic ,strong) AtmsDataSource *atmsDataSource;
@property (nonatomic, strong) BankBranchesDataSource *bankBrancesDataSource;
@end

@implementation ATMandDepartmentsViewController {
    ObjectsMapView *objectsMapView;
}

-(id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

-(void)initVars {}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self updateTableViewDataSourceIfNeeded];
    NSLog(@"updateTableViewDataSourceIfNeeded viewWillAppear");
    if (objectsMapView) {
        [objectsMapView updateTitles];
    }
}

#pragma mark -
#pragma mark ObjectsMapVIew

- (void)objectsMapViewOpenFullscreenMap
{
    UINavigationController *nc = [UIHelper defaultNavigationController];
    ContactsMapViewController *v = [[ContactsMapViewController alloc] init];
    [nc setViewControllers:@[v]];
    v.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:nc animated:YES completion:nil];
}

- (void)didChangeMapFilter {
    [self.tableView reloadData];
    [self updateTableViewDataSourceIfNeeded];
    NSLog(@"updateTableViewDataSourceIfNeeded didChangeMapFilter");
}

#pragma mark -

- (void)configUI {
    [self setBafBackground];
    [self showMenuButton];
    [self setNavigationTitle:NSLocalizedString(@"atm", nil)];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    __weak ATMandDepartmentsViewController * weakSelf = self;

    [self setupTableView];

    UIView *containerView = [UIView new];
    containerView.frame = CGRectMake(0, 0, [ASize screenWidth], 180);
    self.tableView.tableHeaderView = containerView;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView = nil;

    // Map
    objectsMapView = [[ObjectsMapView alloc] initWithHeight:180];
    objectsMapView.delegate = self;
    objectsMapView.parentVC = self;
    [containerView addSubview:objectsMapView];
    //[objectsMapView aa_pinUnderView:requestCallButton offset:13];
    [objectsMapView aa_superviewBottom:0];
    [objectsMapView autoPinEdgeToSuperviewEdge:ALEdgeLeft];

    __weak ObjectsMapView *weakObjectsMapView = objectsMapView;
    
    // start location manager
    [[LocationHelper sharedInstance] requestLocation:^(CLLocation *currentLocation) {
        if (weakObjectsMapView) {
            [weakObjectsMapView updateTitles];
            [weakSelf updateTableViewDataSourceIfNeeded];
            NSLog(@"updateTableViewDataSourceIfNeeded configUI requestLocation");
        }
    } failed:^(INTULocationStatus status) {
        NSLog(@"Error to get current location with status - %ld", (long)status);
    }];
}

- (void)setupTableView {
    self.tableView = [UITableView newAutoLayoutView];
    self.tableView.delegate = self;
    self.tableView.alwaysBounceVertical = YES;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.tableView];
    [self.tableView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    [self updateTableViewDataSourceIfNeeded];

    self.tableView.emptyDataSetDelegate = self;
    self.tableView.emptyDataSetSource = self;
}

- (void)updateTableViewDataSourceIfNeeded {
    if (self.tableView) {
        if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms) {
            AtmTableViewCellConfigureBlock configureAtmCell = ^(AtmTableViewCell *cell, Atm *atm) {
                [cell configureForAtm:atm];
            };
            NSArray *atms = [[ContactsSQLiteManager sharedContactsSQLiteManager] loadAtms:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity.cityId];
            atms = [Atm getSortedAtmsFrom:atms];
            self.atmsDataSource = [[AtmsDataSource alloc] initWithItems:atms cellIdentifier:AtmsCellIdentifier configureCellBlock:configureAtmCell];
            self.tableView.dataSource = self.atmsDataSource;
        } else {
            BankBranchTableViewCellConfigureBlock configureBankBranchCell = ^(BankBranchTableViewCell *cell, BankBranch *bankBranch) {
                [cell configureForBankBranch:bankBranch];
            };
            NSArray *bankBrances = [[ContactsSQLiteManager sharedContactsSQLiteManager] loadBranches:[ContactsSQLiteManager sharedContactsSQLiteManager].selectedCity.cityId];
            bankBrances = [BankBranch getSortedBankBranchesFrom:bankBrances];
            self.bankBrancesDataSource = [[BankBranchesDataSource alloc] initWithItems:bankBrances cellIdentifier:BankBranchesCellIdentifier configureCellBlock:configureBankBranchCell];
            self.tableView.dataSource = self.bankBrancesDataSource;
        }
    }
}

#pragma mark - empty data set

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text;
    if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms) {
        text = NSLocalizedString(@"not_atms_in_region", nil);
    } else {
        text = NSLocalizedString(@"no_branch_in_region", nil);
    }

    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
            NSForegroundColorAttributeName: [UIColor darkGrayColor]};

    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UINavigationController *nc = [UIHelper defaultNavigationController];
    ContactsMapViewController *v = [[ContactsMapViewController alloc] init];
    
    OCMapViewSampleHelpAnnotation *annotationToShow;
    if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms) {
        Atm *selectedAtm = [self.atmsDataSource itemAtIndexPath:indexPath];
        
        annotationToShow = [[OCMapViewSampleHelpAnnotation alloc] initWithCoordinate:selectedAtm.location];
        annotationToShow.title = selectedAtm.bankName;
        annotationToShow.subtitle = selectedAtm.address;
    } else {
        BankBranch *selectedBankBranch = [self.bankBrancesDataSource itemAtIndexPath:indexPath];
        
        annotationToShow = [[OCMapViewSampleHelpAnnotation alloc] initWithCoordinate:selectedBankBranch.location];
        annotationToShow.title = selectedBankBranch.title;
        annotationToShow.subtitle = selectedBankBranch.address;
    }
    
    v.annotationToShow = annotationToShow;
    [nc setViewControllers:@[v]];
    v.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:nc animated:YES completion:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([ContactsSQLiteManager sharedContactsSQLiteManager].selectedObjectsType == MapObjectsTypeAtms) {
        CGFloat cellHeight = 0;
        
        Atm *selectedAtm = [self.atmsDataSource itemAtIndexPath:indexPath];
        
        // add atm bank name height
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping; //set the line break mode
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont helveticaNeue:14], NSFontAttributeName, paragraphStyle, NSParagraphStyleAttributeName, nil];
        CGRect rect = [selectedAtm.bankName boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                          attributes:attributes
                                                             context:nil];
        cellHeight += rect.size.height;
        
        // add atm address height
        rect = [selectedAtm.address boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                     attributes:attributes
                                                        context:nil];
        cellHeight += rect.size.height;
        
        // add atm distance height
        rect = [[NSString stringWithFormat:@"%d м", selectedAtm.distanceToUser] boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributes
                                      context:nil];
        cellHeight += rect.size.height;
        
        // add cell margins
        cellHeight += 3 * 7 + 5;
        
        return cellHeight;
    }
    
    CGFloat cellHeight = 0;
    
    BankBranch *selectedBankBranch = [self.bankBrancesDataSource itemAtIndexPath:indexPath];
    
    // add branch title height
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping; //set the line break mode
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont helveticaNeue:14], NSFontAttributeName, paragraphStyle, NSParagraphStyleAttributeName, nil];
    CGRect rect = [selectedBankBranch.title boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont helveticaNeue:14], NSFontAttributeName, nil]
                                              context:nil];
    cellHeight += rect.size.height;
    
    // add branch address height
    rect = [selectedBankBranch.address boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];
    cellHeight += rect.size.height;
    
    // add branch phone height
    rect = [selectedBankBranch.phone boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];
    cellHeight += rect.size.height;
    
    // add branch work time height
    rect = [selectedBankBranch.workTime boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];
    cellHeight += rect.size.height;
    
    // add branch email address height
    rect = [selectedBankBranch.email boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];
    cellHeight += rect.size.height;
    
    // add branch distance height
    rect = [[NSString stringWithFormat:@"%d м", selectedBankBranch.distanceToUser] boundingRectWithSize:CGSizeMake([ASize screenWidth] - 2 * 15, CGFLOAT_MAX)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];
    cellHeight += rect.size.height;
    
    // add cell margins
    cellHeight += 3 * 7 + 4 * 5;
    
    return cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *headerTitleLabel = [UILabel new];
    headerTitleLabel.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    headerTitleLabel.frame = CGRectMake(0, 0, [ASize screenWidth], 30);
    headerTitleLabel.textAlignment = NSTextAlignmentCenter;
    headerTitleLabel.font = [UIFont helveticaNeue:12];
    headerTitleLabel.text = NSLocalizedString(@"near_objects", nil);
    
    UIView *bottomSeparatorLine = [[UIView alloc] initWithFrame:CGRectMake(0, headerTitleLabel.bottom - 1, [ASize screenWidth], 1)];
    bottomSeparatorLine.backgroundColor = [UIColor bbb];
    [headerTitleLabel addSubview:bottomSeparatorLine];
    
    return headerTitleLabel;
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
