//
//  PaymentFormViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 4/27/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EpayBrowserViewController.h"

typedef enum ActionType:NSInteger {
    ActionTypeNewPayment,
    ActionTypeMakePayment,
    ActionTypeInsurancePayment
} ActionType;

@class Contract, PaymentProvider;
@class InputTextField;
@class MainButton;

@interface PaymentFormViewController : UITableViewController <EpayBrowserViewControllerDelegate>

@property (nonatomic, assign) ActionType actionType;

@property (nonatomic, weak) Contract *contract;
@property (nonatomic, weak) PaymentProvider *paymentProvider;

@property (nonatomic, strong) NSNumber *inputAmount;

@property (nonatomic) NSInteger providerCategoryId;

/**
 * For Alseko payments
 */
@property (nonatomic, strong) NSString *invoiceData;
@property (nonatomic, strong) NSNumber *invoiceId;


/**
 * For Insurance
 */
@property (nonatomic, strong) NSNumber *bonusMalus;
@property (nonatomic, strong) NSString *deliveryDate;
@property (nonatomic, strong) NSString *fName;
@property (nonatomic, strong) NSString *lName;
@property (nonatomic, strong) NSString *mName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSNumber *cityId;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSDictionary *ogpoRequest;
@property (nonatomic, strong) NSString *street;
@property (nonatomic, strong) NSString *home;
@property (nonatomic, strong) NSString *flat;


/**
 * initializing vc
 */
+ (instancetype)createVC;

@end
