//
//  ScrollViewController.h
//  AstanaKzNationalControl
//
//  Created by Almas Adilbek on 5/15/13.
//  Copyright (c) 2013 Astana.kz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+Extension.h"

@interface ScrollViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate>{
    UIScrollView *contentScrollView;
}

@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, assign) BOOL useAutolayout;

-(void)setContentScrollViewContentHeight:(float)height;
-(void)clearContent;

-(float)contentScrollHeight;

// ---

-(void)pushViewToBottom:(UIView *)insertView;
-(void)pushViewToBottom:(UIView *)insertView paddingBottom:(int)paddingBottom;

-(void)pushButtonToBottom:(UIView *)button;
-(void)pushButtonToBottom:(UIView *)button paddingBottom:(int)paddingBottom;

- (void)addKeyboardControlFields:(NSArray *)fields;

@end
