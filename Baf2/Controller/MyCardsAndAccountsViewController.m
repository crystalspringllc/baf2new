//
//  MyCardsAndAccountsViewController.m
//  BAF
//
//  Created by Almas Adilbek on 8/12/14.
//
//

#import "MyCardsAndAccountsViewController.h"
#import "AccountsApi.h"
#import "ViewsListView.h"
#import "AstanaRefreshControl.h"
#import "Current.h"
#import "Loan.h"
#import "AccountDetailViewController.h"
#import "ExternalCardRegistrationViewController.h"
#import "UIImage+Icon.h"
#import "ExtCard.h"
#import "ExternalCardDetailViewController.h"
#import "UIView+Ext.h"
#import "NotificationCenterHelper.h"
#import "IconedLabel.h"
#import <Chameleon.h>

#define kWarningColor [UIColor fromRGB:0xC0392B]

#define kAccordionHeaderMarginTop 0

@interface MyCardsAndAccountsViewController () <ConnectAccountsMessageBoxViewDelegate, ConnectAccountsSearchUserViewControllerDelegate>
@property (nonatomic, strong) AstanaRefreshControl *astanaRefreshControl;
@property (nonatomic, strong) UIActivityIndicatorView *loader;
@property (nonatomic, strong) ConnectAccountsMessageBoxView *accountsMessageBoxView;
@property (nonatomic, strong) AAccordionView *accordionView;
@end

@implementation MyCardsAndAccountsViewController {}

- (id)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];

    [[NSNotificationCenter defaultCenter] removeObserver:self]; // sometimes dealloc not called, therefore remove previous observers if exist
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(composeAccordionView) name:kNotificationCardInfoChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(composeAccordionView) name:kNotificationAccountAliasChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(composeAccordionView) name:kNotificationAccountBlocked object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:kNotificationUserUpdatedProfile object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    BOOL pushed = [self isMovingToParentViewController];

    if (pushed) {
        if (self.accountsResponse || ([User sharedInstance].accountsResponse)) {
            if (!self.accountsResponse) {
                self.accountsResponse = [User sharedInstance].accountsResponse;
            }
            
            [AccountsApi getAccountsWithUpdateForce:NO withSuccess:^(id response) {
                [User sharedInstance].accountsResponse = [AccountsResponse instanceFromDictionary:response];
                self.accountsResponse = [User sharedInstance].accountsResponse;
                [self composeAccordionView];
            } failure:^(NSString *code, NSString *message) {
            }];
            
            
            [self composeAccordionView];
            
        } else {
            [self loadAccounts];
        }
    }
}

#pragma mark - loadings

- (void)loadAccounts {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"load_accounts", nil) animated:YES];
    [AccountsApi getAccountsWithUpdateForce:NO withSuccess:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];

        [User sharedInstance].accountsResponse = [AccountsResponse instanceFromDictionary:response];
        self.accountsResponse = [User sharedInstance].accountsResponse;
        [self composeAccordionView];

    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

#pragma mark - config actions

- (void)refresh {
    [self loadAccounts];
}

- (void)addCardButtonTapped {
    if (![User isEmailAndPhoneConfirmedM]) { return; }

    ExternalCardRegistrationViewController *v = [[ExternalCardRegistrationViewController alloc] init];
    [self.navigationController pushViewController:v animated:YES];
}

#pragma mark - accordion delegate methods

- (void)aacordionView:(AAccordionView *)aAccordionView header:(AAccordionHeaderView *)aAccordionHeaderView didOpen:(BOOL)didOpen
{
    __weak MyCardsAndAccountsViewController *wSelf = self;
    CGSize size = wSelf.contentScrollView.contentSize;
    size.height = aAccordionView.height + 10 + self.accordionView.y;

    if(aAccordionHeaderView.y < wSelf.contentScrollView.contentOffset.y)
    {
        [wSelf.contentScrollView setContentOffset:CGPointMake(0, aAccordionHeaderView.y - kAccordionHeaderMarginTop) animated:YES];
        [self performBlock:^{
            wSelf.contentScrollView.contentSize = size;
        } afterDelay:0.4];
    }
    else if(size.height < wSelf.contentScrollView.height)
    {
        [wSelf.contentScrollView setContentOffset:CGPointZero animated:YES];
        [self performBlock:^{
            wSelf.contentScrollView.contentSize = size;
        } afterDelay:0.4];
    }
    else if(size.height < (wSelf.contentScrollView.contentOffset.y + wSelf.contentScrollView.height))
    {
        [wSelf.contentScrollView setContentOffset:CGPointMake(0, size.height - wSelf.contentScrollView.height) animated:YES];
        [self performBlock:^{
            wSelf.contentScrollView.contentSize = size;
        } afterDelay:0.4];
    }
    else
    {
        wSelf.contentScrollView.contentSize = size;
    }
}

- (UIView *)noProductsMessageView:(NSString *)message
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth] - 16, 50)];
    view.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

    CGFloat padding = 10;

    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(padding, 0, view.width - 2 * padding, 1)];
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.font = [UIFont systemFontOfSize:14];
    messageLabel.textColor = [UIColor darkGrayColor];
    messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    messageLabel.numberOfLines = 0;
    messageLabel.text = message;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    [messageLabel sizeToFit];

    view.height = 2 * padding + messageLabel.height;

    messageLabel.x = (CGFloat) ((view.width - messageLabel.width) * 0.5);
    messageLabel.y = (CGFloat) ((view.height - messageLabel.height) * 0.5);

    [view addSubview:messageLabel];

    return view;
}

#pragma mark - delegates methods

- (void)productRowViewTapped:(ProductRowView *)rowView {
    [self onProductRowViewTapped:rowView];
}

- (void)depositsListViewProductRowViewDidTapped:(ProductRowView *)rowView {
    [self onProductRowViewTapped:rowView];
}

- (void)registeredCardsListViewProductRowViewDidTapped:(ProductRowView *)rowView {
    [self onProductRowViewTapped:rowView];
}

- (void)bankCardsListViewCardTapped:(ProductRowView *)rowView {
    [self onProductRowViewTapped:rowView];
}

- (void)onProductRowViewTapped:(ProductRowView *)rowView {
    __weak MyCardsAndAccountsViewController *wSelf = self;

    if ([rowView.data isKindOfClass:[ExtCard class]]) {

        ExternalCardDetailViewController *v = [[ExternalCardDetailViewController alloc] init];
        v.externalCard = rowView.data;
        v.onDeleteCardSuccess = ^{
            [wSelf refresh];
        };
        [self.navigationController pushViewController:v animated:YES];

        return;
    }

    AccountDetailViewController *v = [AccountDetailViewController createVC];
    v.account = rowView.data;
    [self.navigationController pushViewController:v animated:YES];

}

- (void)connectAccountsMessageBoxViewConnectTapped {
    if (![User isEmailAndPhoneConfirmedM]) { return; }

    ConnectAccountsSearchUserViewController *v = [[ConnectAccountsSearchUserViewController alloc] init];
    v.delegate = self;
    [self.navigationController pushViewController:v animated:YES];
}

- (void)connectAccountsSearchUserViewControllerAccountConnected {
    [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"accounts_successfully_plugedin", nil) message:@""];
    
    [[User sharedInstance] setIsClient:YES];
    
    if (self.accountsMessageBoxView) {
        [self.accountsMessageBoxView removeFromSuperview];
        self.accountsMessageBoxView = nil;
    }
    self.accordionView.y = 0;
    
    [self loadAccounts];
    
    // Post account conencted notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationClientAccountsConnected object:nil];
}

#pragma mark - config ui

/**
 * UI configurations
 */
- (void)configUI {
    self.contentScrollView.backgroundColor = [UIColor clearColor];
    [self setBafBackground];
    
    if (self.isOpenedFromMenu) {
        [self showMenuButton];
        [self showRefreshAtRight:NO];
    } else {
        [self showRefreshAtRight:YES];
    }
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"accounts_and_cards", nil)];


    if (![User sharedInstance].isClient) {
        self.accountsMessageBoxView = [[ConnectAccountsMessageBoxView alloc] init];
        self.accountsMessageBoxView.delegate = self;
        self.accountsMessageBoxView.y = 5;
        [contentScrollView addSubview:self.accountsMessageBoxView];
    }
}

/**
 * Initialize accourdion property
 */
- (void)composeAccordionView {
    if ([User sharedInstance].isClient) {
        [contentScrollView removeSubviews];

        IconedLabel *iconedLabel = [[IconedLabel alloc] init];
        iconedLabel.iconImageView.image = [UIImage imageNamed:@"icon-baf-logo"];
        NSString *update = [self.accountsResponse getUpdatedNiceFormat];
        if (update) {
            iconedLabel.textLabel.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"datas_on", nil), update];
        } else {
            iconedLabel.textLabel.text = @"";
        }
        iconedLabel.textLabel.textColor = [UIColor flatBlackColorDark];
        iconedLabel.backgroundColor = [UIColor clearColor];
        iconedLabel.textLabel.font = [UIFont systemFontOfSize:10];
        iconedLabel.leftIndent = 15.0;
        [iconedLabel.loaderView stopAnimating];
        [contentScrollView addSubview:iconedLabel];

        self.accordionView = [[AAccordionView alloc] init];
        self.accordionView.backgroundColor = [UIColor clearColor];
        self.accordionView.delegate = self;
        [contentScrollView addSubview:self.accordionView];

        [self createCardAccount:NO];
        [self createCardsAndExtCardsAccordion:NO];
        [self createCurrentsAccordion:NO];
        [self createDepositAccordion:NO];
        [self createLoanAccordion:NO];
    }
}

/**
 *  Add accordion header with content for LOANS
 */
- (void)createLoanAccordion:(BOOL)isHeaderOpen {
    UIView *contentView;

    // Loans
    NSMutableArray * rowViews = [NSMutableArray array];
    for (Loan *loan in self.accountsResponse.loans) {
        ProductRowView *rowView = [[ProductRowView alloc] initWithTitle:loan.alias subtitle:[NSString stringWithFormat:@"%@ %@", @"Остаток долга:", [loan debtAmountWithCurrency]] icon:[UIImage imageNamed:@"icon_credit_color"]];
        rowView.delegate = self;
        rowView.data = loan;
        rowView.subtitleFont = [UIFont boldSystemFontOfSize:14];
        [rowView showDisclosure];
        if(loan.isFavorite) [rowView setFavorited:YES];
        [rowViews addObject:rowView];

        if (loan.isWarning) {
            [rowView addTagWithText:@"BLOCK" color:kWarningColor];
        }
    }
    if(self.accountsResponse.loans.count == 0) {
        contentView = [self noProductsMessageView:NSLocalizedString(@"empty_credit_list", nil)];
    } else {
        ViewsListView *listView = [ViewsListView new];
        contentView = [listView viewsListWithViews:rowViews];
        contentView.backgroundColor = [UIColor clearColor];
    }
    [self.accordionView addHeaderWithTitle:NSLocalizedString(@"_loans", nil)
                                      icon:nil
                               contentView:contentView
                               detailColor:[UIColor flatBrownColor]
                                 marginTop:kAccordionHeaderMarginTop
                                    isOpen:isHeaderOpen];

}

/**
 *  Add accordion header with content for DEPOSITS
 */
- (void)createDepositAccordion:(BOOL)isHeaderOpen {
    UIView *depositContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 1)];
    depositContentView.backgroundColor = [UIColor clearColor];
    CGFloat contentSubviewY = 0;

    DepositsListView *listView = [[DepositsListView alloc] initWithDepositGroup:self.accountsResponse.depositGroups];
    listView.delegate = self;
    listView.y = contentSubviewY;
    [depositContentView addSubview:listView];
    contentSubviewY = listView.bottom;
    depositContentView.height = contentSubviewY;

    if (self.accountsResponse.depositGroups.count == 0) {
        depositContentView = [self noProductsMessageView:NSLocalizedString(@"empty_deposit_list", nil)];
    }
    [self.accordionView addHeaderWithTitle:NSLocalizedString(@"_deposits", nil)
                                      icon:nil
                               contentView:depositContentView
                               detailColor:[UIColor flatOrangeColor]
                                 marginTop:kAccordionHeaderMarginTop
                                    isOpen:isHeaderOpen];
}

/**
 *  Add accordion header with content for CURRENTS
 */
- (void)createCurrentsAccordion:(BOOL)isHeaderOpen {
    UIView *contentView;

    NSMutableArray *rowViews = [NSMutableArray array];
    for (Current *current in self.accountsResponse.currents) {
        ProductRowView *rowView = [[ProductRowView alloc] initWithTitle:current.alias subtitle:[current getBalanceWithCurrency] icon:[UIImage currencyIconImage:current.currency]];
        rowView.subtitleFont = [UIFont boldSystemFontOfSize:14];
        rowView.delegate = self;
        rowView.data = current;
        [rowView showDisclosure];
        if (current.isFavorite) [rowView setFavorited:YES];
        [rowViews addObject:rowView];

        if (current.isWarning) {
            [rowView addTagWithText:@"BLOCK" color:kWarningColor];
        }
    }

    if (rowViews.count == 0) {
        contentView = [self noProductsMessageView:NSLocalizedString(@"empty_current_list", nil)];
    } else {
        ViewsListView *listView = [ViewsListView new];
        contentView = [listView viewsListWithViews:rowViews];
        contentView.backgroundColor = [UIColor clearColor];
    }

    [self.accordionView addHeaderWithTitle:NSLocalizedString(@"_currents", nil)
                                      icon:nil
                               contentView:contentView
                               detailColor:[UIColor flatSkyBlueColorDark]
                                 marginTop:kAccordionHeaderMarginTop
                                    isOpen:isHeaderOpen];

}


/**
 *  Add accordion header with content for CARDS and CARDACCOUNTS
 */
- (void)createCardAccount:(BOOL)isHeaderOpen {
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 1)];
    contentView.backgroundColor = [UIColor clearColor];

    // CardAccount and Cards
    UIView *otherBankCardsContentView;
    if (self.accountsResponse.cardAccounts.count == 0) {
        otherBankCardsContentView = [self noProductsMessageView:NSLocalizedString(@"you_have_no_cards", nil)];
        [contentView addSubview:otherBankCardsContentView];
        
        contentView.height = otherBankCardsContentView.bottom;
    } else {
        BankCardsListView *bankCardsListView = [[BankCardsListView alloc]
                initWithBankCardAccounts:self.accountsResponse.cardAccounts
                                   title:@"Карты Банк Астаны"];
        bankCardsListView.delegate = self;
        [contentView addSubview:bankCardsListView];
        
        contentView.height = bankCardsListView.bottom;
    }
    
    [self.accordionView addHeaderWithTitle:NSLocalizedString(@"_cards", nil)
                                      icon:nil
                               contentView:contentView
                               detailColor:[UIColor flatBlueColorDark]
                                 marginTop:kAccordionHeaderMarginTop
                                    isOpen:isHeaderOpen];
}

- (void)createCardsAndExtCardsAccordion:(BOOL)isHeaderOpen {
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 1)];
    contentView.backgroundColor = [UIColor clearColor];

    // External banks cards
    NSMutableArray * otherBankCardsSectionViews = [NSMutableArray array];
    
    RegisteredCardsListView *otherBankCardslistView = [[RegisteredCardsListView alloc] initWithCards:self.accountsResponse.extCards title:@"Карты других банков:"];
    otherBankCardslistView.delegate = self;
    [otherBankCardsSectionViews addObject:otherBankCardslistView];
    
    // Add card button
    UIView *addCardButtonContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 60)];
    UIButton *addCardButton = [[UIButton alloc] init];
    addCardButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    addCardButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"ga_register_card", nil) attributes:@{NSForegroundColorAttributeName: [UIColor fromRGB:0x146888], NSFontAttributeName: [UIFont systemFontOfSize:15], NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
    [addCardButton setAttributedTitle:title forState:UIControlStateNormal];
    [addCardButton addTarget:self action:@selector(addCardButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    addCardButton.width = 200;
    addCardButton.height = 35;
    addCardButton.centerX = addCardButtonContainer.centerX;
    addCardButton.y = addCardButtonContainer.bottom - addCardButton.height - 10;
    [addCardButtonContainer addSubview:addCardButton];
    [otherBankCardsSectionViews addObject:addCardButtonContainer];
    
    UIView *otherBankCardsContentView;
    if(otherBankCardsSectionViews.count == 0) {
        otherBankCardsContentView = [self noProductsMessageView:NSLocalizedString(@"you_have_no_payment_cards", nil)];
    } else {
        
        ViewsListView *listView = [ViewsListView new];
        otherBankCardsContentView = [listView viewsListWithViews:otherBankCardsSectionViews];
    }
    
    [contentView addSubview:otherBankCardsContentView];
    contentView.height = otherBankCardsContentView.bottom;
    
    [self.accordionView addHeaderWithTitle:NSLocalizedString(@"_extcards", nil)
                                      icon:nil
                               contentView:contentView
                               detailColor:[UIColor flatYellowColor]
                                 marginTop:kAccordionHeaderMarginTop
                                    isOpen:isHeaderOpen];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
