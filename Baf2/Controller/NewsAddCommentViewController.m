//
//  NewsAddCommentViewController.m
//  Baf2
//
//  Created by nmaksut on 19.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NewsAddCommentViewController.h"
#import "FloatingTextView.h"
#import "MainButton.h"
#import "NewsApi.h"
#import "NotificationCenterHelper.h"
#import <STPopup.h>

@interface NewsAddCommentViewController ()
@property (weak, nonatomic) IBOutlet FloatingTextView *textView;
@property (weak, nonatomic) IBOutlet MainButton *addButton;

@end

@implementation NewsAddCommentViewController

#pragma mark -
#pragma mark - Init

+ (id)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"News" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth] - 2 * 20, 231);
}

#pragma mark -
#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.textView becomeFirstResponder];
}

#pragma mark -
#pragma mark - Actions

- (IBAction)addTapped:(id)sender {
    if (self.textView.text.length == 0) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"fill_comments", nil)];
        return;
    }
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"adding_comments", nil) animated:YES];
    [NewsApi addCommentNewsId:self.news.newsId message:self.textView.text success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];

        [self.popupController dismiss];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserCommentAdded object:nil];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}


#pragma mark -
#pragma mark - ConfigUI

- (void)configUI
{
    [self setNavigationTitle:NSLocalizedString(@"comment", nil)];
    [self setNavigationBackButton];
    [self.addButton setMainButtonStyle:MainButtonStyleOrange];


    [(JVFloatLabeledTextView *)self.textView setPlaceholder:NSLocalizedString(@"comment", nil)];
    self.textView.text = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    NSLog(@"dealloc news add comment");
}

@end