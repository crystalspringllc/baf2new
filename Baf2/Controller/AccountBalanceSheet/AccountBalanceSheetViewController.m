//
// Created by Askar Mustafin on 5/24/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "AccountBalanceSheetViewController.h"
#import <STPopup/STPopup.h>

@interface AccountBalanceSheetViewController() {}
@property (nonatomic, strong) NSArray *balances;
@end

@implementation AccountBalanceSheetViewController {}

- (instancetype)initWithBalanceAndCurrencies:(NSArray *)balanceAndCurrencies
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"AccountBalanceSheet" bundle:[NSBundle mainBundle]];
    self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AccountBalanceSheetViewController class])];
    if (self) {
        self.balances = balanceAndCurrencies;
    }
    return  self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 308);
    self.landscapeContentSizeInPopup = CGSizeMake([ASize screenWidth], 144);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - tableview

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.balances.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"newFriendCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    NSDictionary* balanceAndCurrency = self.balances[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", balanceAndCurrency[@"balance"], balanceAndCurrency[@"currency"]];
    cell.textLabel.textAlignment = NSTextAlignmentRight;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(accountBalanceSheet:didSelectedBalance:)]) {
        [self.delegate accountBalanceSheet:self didSelectedBalance:self.balances[indexPath.row]];
    }
}

#pragma mark - config ui

- (void)configUI {
    self.tableView.tableFooterView = [UIView new];
}

- (void)setTitleString:(NSString *)titleString {
    _titleString = titleString;
    self.title = _titleString;
}

@end