//
// Created by Askar Mustafin on 5/24/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AccountBalanceSheetViewController;

@protocol AccountBalanceSheetDelegate

- (void)accountBalanceSheet:(AccountBalanceSheetViewController *)balanceSheet didSelectedBalance:(id)selectedBalance;

@end

@interface AccountBalanceSheetViewController : UIViewController

- (instancetype)initWithBalanceAndCurrencies:(NSArray *)balanceAndCurrencies;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, weak) NSObject <AccountBalanceSheetDelegate> *delegate;

@property (nonatomic, strong) NSString *titleString;

@end