//
// Created by Askar Mustafin on 5/24/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Account.h"

@class AccountsActionSheetViewController;


@protocol AccountsActionSheetDelegate

- (void)actionSheet:(AccountsActionSheetViewController *)actionSheet didSelectedAccount:(Account *)account;

@end

@interface AccountsActionSheetViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

- (instancetype)initWithAccounts:(NSArray *)accounts;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, weak) NSObject <AccountsActionSheetDelegate> *delegate;
@property (nonatomic, strong) NSString *titleString;

@end