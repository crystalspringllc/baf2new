//
// Created by Askar Mustafin on 5/24/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "AccountsActionSheetViewController.h"
#import "ActionSheetListTableViewCell.h"
#import "Card.h"
#import <STPopup/STPopup.h>

@interface AccountsActionSheetViewController() {}
@property (nonatomic, strong) NSArray *accounts;
@end

@implementation AccountsActionSheetViewController {}

- (instancetype)initWithAccounts:(NSArray *)accounts
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"AccountsActionSheet" bundle:[NSBundle mainBundle]];
    self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AccountsActionSheetViewController class])];
    if (self) {
        self.accounts = accounts;
    }
    return  self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 308);
    self.landscapeContentSizeInPopup = CGSizeMake([ASize screenWidth], 144);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - tableview delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.accounts.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *s = self.accounts[section];
    return [s[@"rows"]count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSDictionary *s = self.accounts[section];
    return s[@"title"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ActionSheetList";
    ActionSheetListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    NSDictionary *s = self.accounts[indexPath.section];
    Account *account = s[@"rows"][indexPath.row];

    [cell.logoImageView sd_setImageWithURL:account.logo placeholderImage:[UIImage imageNamed:@"icon-icon-placeholder.png"]];
    
    cell.titleLabel.text = account.alias;
    cell.subtitleLabel.text = account.balanceAvailableWithCurrency;
   // if(account.accountType != AccountTypeBeneficiary) cell.subtitleLabel.text = account.balanceAvailableWithCurrency;

    if (![account isKindOfClass:[Card class]]) {
        cell.subtitle2Label.text = account.iban;
    } else {
        cell.subtitle2Label.text = account.number;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(actionSheet: didSelectedAccount:)]) {

        NSDictionary *s = self.accounts[indexPath.section];
        Account *account = s[@"rows"][indexPath.row];

        [self.delegate actionSheet:self didSelectedAccount:account];
    }
}

#pragma mark - config ui

- (void)configUI {
    self.tableView.tableFooterView = [UIView new];
}

- (void)setTitleString:(NSString *)titleString {
    _titleString = titleString;
    self.title = _titleString;
}

@end
