//
//  AccountManageLimitsViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 12.08.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "AccountManageLimitsViewController.h"
#import "AccountLimitViewController.h"
#import "AccountsApi.h"
#import <STPopup.h>
#import <UIScrollView+EmptyDataSet.h>

#define CELL_HEIGHT 80

@interface AccountManageLimitsViewController () <DZNEmptyDataSetSource, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIActivityIndicatorView *tableViewSpinner;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;

@property (nonatomic, strong) NSArray *limits;
@property (nonatomic) BOOL errorToLoadLimits;

@end

@implementation AccountManageLimitsViewController

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.title = NSLocalizedString(@"limit_control", nil);
        self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 3 * CELL_HEIGHT);
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadLimits)];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
    [self loadLimits];
}


#pragma mark - API

- (void)loadLimits {
    if (!self.card) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"account_not_loaded_fully", nil)];
        return;
    }
    
    [self showSpinner];
    [AccountsApi getCardLimits:self.card.cardId success:^(NSArray *limits) {
        [self hideSpinner];
        
        self.limits = limits;
        
        self.tableView.hidden = false;
        
        self.errorToLoadLimits = false;
        self.tableView.emptyDataSetSource = self;
        [self.tableView reloadData];
    } failure:^(NSString *code, NSString *message) {
        [self hideSpinner];
        
        self.tableView.hidden = false;
        
        self.errorToLoadLimits = true;
        self.tableView.emptyDataSetSource = self;
        [self.tableView reloadData];
    }];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = NSLocalizedString(@"no_limits", nil);
    if (self.errorToLoadLimits) {
        text = NSLocalizedString(@"error_during_loading_limits", nil);
    }
    
    return [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14], NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
}


#pragma mark - Methods

- (void)showSpinner {
    if (self.tableView.hidden) {
        [self.tableViewSpinner startAnimating];
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.spinner];
    [self.spinner startAnimating];
}

- (void)hideSpinner {
    if (self.tableView.hidden) {
        [self.tableViewSpinner stopAnimating];
    }
    [self.spinner stopAnimating];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadLimits)];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.limits.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    CardLimit *limit = self.limits[indexPath.row];
    
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.preferredMaxLayoutWidth = cell.textLabel.frame.size.width;
    cell.textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.textLabel.textColor = [UIColor appGrayColor];
    cell.textLabel.text = limit.name;
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    CardLimit *limit = self.limits[indexPath.row];
    
    AccountLimitViewController *vc = [AccountLimitViewController new];
    vc.card = self.card;
    vc.limit = limit;
    [self.popupController pushViewController:vc animated:true];
}


#pragma mark - Config UI

- (void)configUI {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = false;
    self.tableView.delegate  = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = CELL_HEIGHT;
    self.tableView.estimatedRowHeight = CELL_HEIGHT;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.hidden = true;
    [self.view addSubview:self.tableView];
    [self.tableView autoPinEdgesToSuperviewEdges];
    
    self.tableViewSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.tableViewSpinner.translatesAutoresizingMaskIntoConstraints = false;
    [self.view addSubview:self.tableViewSpinner];
    [self.tableViewSpinner autoCenterInSuperview];
    
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
}

@end
