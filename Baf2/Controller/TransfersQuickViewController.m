//
//  TransfersQuickViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 5/4/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "TransfersQuickViewController.h"
#import "TransfersFormViewController.h"
#import "TransfersTemplatesViewController.h"
#import "ConnectAccountsSearchUserViewController.h"
#import "ExternalCardRegistrationViewController.h"
#import "TransferTemplateCollectionViewCell.h"
#import "DisclaimerTableViewCell.h"
#import "MainButton.h"
#import "P2PTransferViewController.h"
#import "AccountsApi.h"
#import "TransferTemplate.h"
#import "NotificationCenterHelper.h"
#import "AstanaRefreshControl.h"
#import "Accounts.h"
#import "OperationAccounts.h"
#import "TransferTypesButtonContainerCell.h"
#import "MyTransfersDependencies.h"
#import "FromExternalToAccountViewController.h"
#import "TransfersToOtherDependencies.h"

@interface TransfersQuickViewController ()<UITableViewDataSource,
        UITableViewDelegate,
        ConnectAccountsSearchUserViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UIButton *templatesListButton;
@property (nonatomic, strong) OperationAccounts *operationAccounts;
@property (nonatomic, strong) NSArray *transferTemplates;
@property (nonatomic, strong) AstanaRefreshControl *astanaRefreshControl;
@end

@implementation TransfersQuickViewController {
    BOOL loading;
}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Transfers" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([TransfersQuickViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self loadPaymentTemplates:true];
    //check tranfer button
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - Actions

- (void)showAllTemplates {
    TransfersTemplatesViewController *vc = [[UIStoryboard storyboardWithName:@"Transfers" bundle:nil] instantiateViewControllerWithIdentifier:@"TransfersTemplatesViewController"];
    vc.transferTemplates = self.transferTemplates;
    [self.navigationController pushViewController:vc animated:true];
}

- (void)searchForAccountsByIIN {
    if (![User isEmailAndPhoneConfirmedM]) { return; }

    ConnectAccountsSearchUserViewController *v = [[ConnectAccountsSearchUserViewController alloc] init];
    v.delegate = self;
    [self.navigationController pushViewController:v animated:YES];
}

- (void)registerCard {
    if (![User isEmailAndPhoneConfirmedM]) { return; }

    ExternalCardRegistrationViewController *v = [[ExternalCardRegistrationViewController alloc] init];
    [self.navigationController pushViewController:v animated:YES];
}

#pragma mark - API

- (void)loadPaymentTemplates:(BOOL)isNotRefreshing {
    if (loading) {
        return;
    }
    
    loading = true;
    
    dispatch_group_t group_async = dispatch_group_create();
    
    __weak TransfersQuickViewController *weakSelf = self;
    
    self.templatesListButton.hidden = true;
    
    if (isNotRefreshing) {
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading_operational_account_and_template", nil) animated:true];
    }
    
    dispatch_group_enter(group_async);
    [AccountsApi getOperationAccountsWithSuccess:^(OperationAccounts *operationAccounts) {
        weakSelf.operationAccounts = operationAccounts;
        dispatch_group_leave(group_async);
    } failure:^(NSString *code, NSString *message) {
        dispatch_group_leave(group_async);
    }];
    
    dispatch_group_enter(group_async);
    [NewTransfersApi getTemplatesWithSuccess:^(NSArray *transferTemplates) {
        weakSelf.transferTemplates = transferTemplates;
        dispatch_group_leave(group_async);
    } failure:^(NSString *code, NSString *message) {
        dispatch_group_leave(group_async);
    }];
    
    dispatch_group_notify(group_async, dispatch_get_main_queue(), ^{
        loading = false;
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        if (!isNotRefreshing) {
            [weakSelf.astanaRefreshControl finishingLoading];
        }
        
        weakSelf.collectionView.backgroundView = nil;
        
        if (weakSelf.transferTemplates.count == 0) {
            weakSelf.emptyView.hidden = false;
            weakSelf.headerTitleLabel.hidden = true;
            weakSelf.collectionView.hidden = true;
        } else {
            weakSelf.emptyView.hidden = true;
            weakSelf.headerTitleLabel.hidden = false;
            weakSelf.collectionView.hidden = false;
        }
        
        [weakSelf.collectionView reloadData];
        
        weakSelf.tableView.hidden = false;
        [weakSelf.tableView reloadData];
    });
}

- (void)reloadTransferTemplatesAndOperationAccounts {
    [self loadPaymentTemplates:false];
}

- (void)onTransfersUpdate:(NSNotification *)notification {
    [self loadPaymentTemplates:true];
}

#pragma mark - ConnectAccountsSearchUserViewControllerDelegate

- (void)connectAccountsSearchUserViewControllerAccountConnected {
    [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"accounts_successfully_plugedin", nil) message:@""];
    
    [[User sharedInstance] setIsClient:YES];
    
    [self loadPaymentTemplates:true];
    
    // Post account conencted notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationClientAccountsConnected object:nil];
}

#pragma mark - collectionview protocol methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.transferTemplates count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TransferTemplateCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TransferTemplateCollectionViewCell" forIndexPath:indexPath];
    
    TransferTemplate *tt = self.transferTemplates[(NSUInteger) indexPath.item];
    
    cell.requestorAccountLabel.text = tt.requestorAccount.alias;
    cell.destinationAccountLabel.text = tt.destinationAccount.alias;
    
    cell.titleLabel.text = tt.alias;
    cell.detailLabel.text = [NewTransfersApi transferTypeToDescriptionString:[NewTransfersApi transferTypeForString:tt.type]];

    cell.requestorAccountLabel.textColor = [UIColor fromRGB:0x146888];
    cell.destinationAccountLabel.textColor = [UIColor fromRGB:0x146888];
    
    NSNumber *amount = tt.direction ? tt.requestorAmount : tt.destinationAmount;
    NSString *currency = tt.direction ? tt.requestorCurrency : tt.destinationCurrency;
    cell.amountLabel.text = [NSString stringWithFormat:@"%@ %@", [amount decimalFormatString], currency];
    
    if (!tt.templateIsActive) {
        if (!tt.requestorAccount || !tt.requestorAccount.accountId) {
            cell.requestorAccountLabel.text = NSLocalizedString(@"account_is_not_available", nil);
            cell.requestorAccountLabel.textColor = [UIColor redColor];
        } else {
            cell.requestorAccountLabel.textColor = [UIColor fromRGB:0x146888];
        }
        if (!tt.destinationAccount || !tt.destinationAccount.accountId) {
            cell.destinationAccountLabel.text = NSLocalizedString(@"account_is_not_available", nil);
            cell.destinationAccountLabel.textColor = [UIColor redColor];
        } else {
            cell.destinationAccountLabel.textColor = [UIColor fromRGB:0x146888];
        }
    }
    
    __weak TransfersQuickViewController *weakSelf = self;
    __weak TransferTemplateCollectionViewCell *weakCell = cell;
    cell.didTapDeleteButton = ^() {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"remove_template", nil) preferredStyle:UIAlertControllerStyleAlert];
        alertController.popoverPresentationController.sourceView = weakCell.deleteButton;
        
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *_Nonnull action) {
            [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"deleting_template", nil) animated:true];

            [NewTransfersApi setTemplatesDelete:true templateId:tt.templateId success:^(id response) {
                [MBProgressHUD hideAstanaHUDAnimated:true];

                [Toast showToast:NSLocalizedString(@"template_successfully_removed", nil)];

                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransfersTemplatesUpdated object:nil];
            }                           failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:true];
            }];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancellation", nil) style:UIAlertActionStyleCancel handler:nil]];
        
        [weakSelf presentViewController:alertController animated:true completion:nil];
    };
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    TransferTemplate *tt = self.transferTemplates[(NSUInteger) indexPath.item];
    
    if (!tt.templateIsActive) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"template_not_available", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        alertController.popoverPresentationController.sourceView = [collectionView cellForItemAtIndexPath:indexPath];
        
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"remove_templatee", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *_Nonnull action) {
            [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"deleting_template", nil) animated:true];

            [NewTransfersApi setTemplatesDelete:true templateId:tt.templateId success:^(id response) {
                [MBProgressHUD hideAstanaHUDAnimated:true];

                [Toast showToast:NSLocalizedString(@"template_successfully_removed", nil)];

                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransfersTemplatesUpdated object:nil];
            }                           failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:true];
            }];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancellation", nil) style:UIAlertActionStyleCancel handler:nil]];
        
        [self presentViewController:alertController animated:true completion:nil];
        
        return;
    }

    TransfersFormViewController *vc = [TransfersFormViewController createVC];
    
    switch([NewTransfersApi transferTypeForString:tt.type]) {
        case MY_TRANSFERS: {
            MyTransfersDependencies *myTransfersDependencies = [[MyTransfersDependencies alloc] init];
            myTransfersDependencies.transferTemplate = tt;
            [myTransfersDependencies installRootViewController];

            break;
        }
        case TRANSFERS_TO_OTHER: {
            TransfersToOtherDependencies *transfersToOtherDependencies = [[TransfersToOtherDependencies alloc] init];
            transfersToOtherDependencies.transferTemplate = tt;
            [transfersToOtherDependencies installRootViewController];
            break;
        }
        case INTERBANK_TRANSFERS:
        case MY_CONVERT: {
            vc.transfersType = [NewTransfersApi transferTypeForString:tt.type];
            vc.transferTemplate = tt;
            vc.title = [NewTransfersApi transferTypeToDescriptionString:[NewTransfersApi transferTypeForString:tt.type]];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        default:break;
    }
}

#pragma mark - tableview protocol methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (![User sharedInstance].accountsResponse.hasAtLeastOneAccount) {
//        return UITableViewAutomaticDimension;
//    } else {
//        return 360;
//    }
    return 360;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak TransfersQuickViewController *wSelf = self;

        static NSString *CellIdentifier = @"TransferTypesButtonContainer";
        TransferTypesButtonContainerCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[TransferTypesButtonContainerCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
        }

        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        cell.onAmongAccountClick = ^{
                MyTransfersDependencies *myTransfersDependencies = [[MyTransfersDependencies alloc] init];
                [myTransfersDependencies installRootViewController];
        };
        cell.onAmongBanksTransferClick = ^{
                TransfersFormViewController *vc = [TransfersFormViewController createVC];
                vc.operationAccounts = [self.operationAccounts copy];
                vc.transfersType = INTERBANK_TRANSFERS;
                vc.title = [NewTransfersApi transferTypeToDescriptionString:INTERBANK_TRANSFERS];
                [wSelf.navigationController pushViewController:vc animated:true];

        };
        cell.onThirdPartiesClick = ^{
                TransfersToOtherDependencies *d = [[TransfersToOtherDependencies alloc] init];
                [d installRootViewController];
        };
        cell.onFromCardToCardClick = ^{
            P2PTransferViewController *v = [P2PTransferViewController createVC];
            [wSelf.navigationController pushViewController:v animated:YES];
        };
        cell.onFromExternalCardToAccountClick = ^{
                FromExternalToAccountViewController *v = [FromExternalToAccountViewController createVC];
                v.operationAccounts = [self.operationAccounts copy];
                [wSelf.navigationController pushViewController:v animated:YES];
        };
        return cell;

    //}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (![User sharedInstance].accountsResponse.hasAtLeastOneAccount && indexPath.row == 1) {
        return;
    }
    
    if (![User sharedInstance].accountsResponse.hasAtLeastOneAccount && indexPath.row == 0) {
        P2PTransferViewController *v = [P2PTransferViewController createVC];
        [self.navigationController pushViewController:v animated:YES];
        
        return;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.tableView) {
        [self.astanaRefreshControl scrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.tableView) {
        [self.astanaRefreshControl scrollViewWillEndDragging];
    }
}

#pragma mark - config ui

- (void)configUI {
    if (self != [self.navigationController.viewControllers objectAtIndex:0]) {
        [self setNavigationBackButton];
    }
    
    [self setBafBackground];
    [self showMenuButton];
    [self hideBackButtonTitle];
    [self setNavigationTitle:NSLocalizedString(@"ga_transfers", nil)];
    
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 350;
    
    self.astanaRefreshControl = [AstanaRefreshControl new];
    self.astanaRefreshControl.target = self;
    self.astanaRefreshControl.action = @selector(reloadTransferTemplatesAndOperationAccounts);
    [self.tableView insertSubview:self.astanaRefreshControl atIndex:0];
    
    self.tableView.hidden = true;
    self.emptyView.hidden = true;
    
    [self.templatesListButton addTarget:self action:@selector(showAllTemplates) forControlEvents:UIControlEventTouchUpInside];
    
    // Observers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTransfersUpdate:) name:kNotificationTransfersTemplatesUpdated object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTransfersUpdate:) name:kNotificationTransfersOperationAccountsUpdated object:nil];
}

#pragma mark -

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
