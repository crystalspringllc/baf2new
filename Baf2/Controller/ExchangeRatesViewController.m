//
// Created by Mustafin Askar on 03/11/2014.
//

#import "ExchangeRatesViewController.h"
#import "UIViewController+Extension.h"
#import "ExchangeRatesView.h"
#import "APIClientOLD.h"
#import "APIClientOLD+Methods.h"

@interface ExchangeRatesViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ExchangeRatesView *exchangeRatesView;
@end

@implementation ExchangeRatesViewController

- (id)init
{
    self = [super init];
    if(self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ExchangeRates" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ExchangeRatesViewController class])];

        [self initVars];
    }
    return self;
}


-(void)initVars {}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
}

- (void)goBack
{
    [super goBack];

    // Cancel load exchange rates if still loading.
    for (NSURLSessionDataTask *task in self.exchangeRatesView.tasks) {
        [task cancel];
    }
}

#pragma mark -

-(void)configUI
{
    [self setBafBackground];
    self.scrollView.backgroundColor = [UIColor clearColor];


    [self setNavigationTitle:NSLocalizedString(@"ga_exchange_rate", nil)];

    self.exchangeRatesView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

    if(self.navigationController.viewControllers.count > 1) {
        [self setNavigationBackButton];
    } else {
        [self showMenuButton];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {

}

@end