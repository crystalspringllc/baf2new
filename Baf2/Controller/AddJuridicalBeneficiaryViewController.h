//
//  AddJuridicalBeneficiaryViewController.h
//  BAF
//
//  Created by Almas Adilbek on 8/20/15.
//
//



@interface AddJuridicalBeneficiaryViewController : UIViewController <UITextViewDelegate>

+ (instancetype)createVC;
- (void)loadCompanyInfo;
- (void)loadBankInfo;

@end
