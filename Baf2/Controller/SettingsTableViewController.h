//
//  SettingsTableViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 28.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewController : UITableViewController
+ (id)createVC;
@end
