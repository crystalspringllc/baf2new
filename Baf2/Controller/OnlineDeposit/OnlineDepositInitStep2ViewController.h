//
// Created by Askar Mustafin on 10/24/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "OnlineDeposit.h"
#import "MainButton.h"
#import "FloatingTextField.h"

@class OnlineDepositOpenModel;

@interface OnlineDepositInitStep2ViewController : UITableViewController

@property (weak, nonatomic) IBOutlet FloatingTextField *filialTextField;

@property (weak, nonatomic) IBOutlet FloatingTextField *termTextField;

@property (weak, nonatomic) IBOutlet FloatingTextField *capitalizationTextField;

@property (nonatomic, strong) OnlineDeposit *onlineDeposit;

@property (weak, nonatomic) IBOutlet MainButton *nextButton;
@property (weak, nonatomic) IBOutlet UISwitch *ruleSwitch;

+ (instancetype)createVC;

@property (nonatomic, strong) OnlineDepositOpenModel *onlineDepositOpenModel;

@end
