//
//  OnlineDepositInfoViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 10/22/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OnlineDeposit;

@interface OnlineDepositInfoViewController : UITableViewController

@property (nonatomic, strong) OnlineDeposit *onlineDeposit;

+ (instancetype)createVC;


@end
