//
//  OnlineDepositInfoViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 10/22/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositInfoViewController.h"
#import "UITableViewController+Extension.h"
#import "OnlineDeposit.h"
#import "OnlineDepositProperty.h"
#import "OnlineDepositDescription.h"
#import "IconMultilineTitleCell.h"
#import "UILabel+Attributed.h"
#import "OnlineDepositProfitInfoCell.h"


@interface OnlineDepositInfoViewController ()
@end

@implementation OnlineDepositInfoViewController

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OnlineDeposit" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - table delegate & datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        NSArray *composedInfos = [self.onlineDeposit getComposedInfo];
        return composedInfos.count;
    } else if (section == 1) {
        return 3;
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        NSArray *composedInfos = [self.onlineDeposit getComposedInfo];
        NSDictionary *info = composedInfos[indexPath.row];
        return [IconMultilineTitleCell cellHeightWithTitle:[NSString stringWithFormat:@"%@ %@", info[@"title"], info[@"value"]] hasIcon:YES];
    } else {
        return 80;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"Условия вклада";
    } else if (section == 1) {
        return @"Выгодные условия по вкладам";
    }
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section == 1) {
        return @"*ГЭСВ";
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"cellDetails";
    static NSString *CellIdentifier2 = @"cellDetails2";
    static NSString *CellIdentifier3 = @"ProfitInfoCell";
    
    UITableViewCell *resultCell;
    
    if (indexPath.section == 0) {
        
        IconMultilineTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[IconMultilineTitleCell alloc] initWithReuseIdentifier:CellIdentifier];
            cell.textLabel.textColor = [UIColor fromRGB:0x444444];
        }
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        cell.imageView.image = [UIImage imageNamed:@"AAFieldCheckmark"];


        NSArray *composedInfos = [self.onlineDeposit getComposedInfo];
        NSDictionary *info = composedInfos[indexPath.row];

        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", info[@"title"], info[@"value"]];
        [cell.textLabel setFont:[UIFont boldSystemFontOfSize:14] textColor:[UIColor blackColor] forSubstring:info[@"title"]];
        resultCell = cell;

    
    } else if (indexPath.section == 1) {

        OnlineDepositProfitInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier3];
        }

        OnlineDepositProperty *property = self.onlineDeposit.properties[0];

        if (indexPath.row == 0) {
            cell.termLabel.text = @"Срок вклада";
            cell.kztLabel.text = @"KZT";
            cell.eurLabel.text = @"EUR";
            cell.usdLabel.text = @"USD";
            cell.rubLabel.text = @"RUB";

            cell.termLabel.font = [UIFont boldSystemFontOfSize:15];
            cell.kztLabel.font = [UIFont boldSystemFontOfSize:15];
            cell.eurLabel.font = [UIFont boldSystemFontOfSize:15];
            cell.usdLabel.font = [UIFont boldSystemFontOfSize:15];
            cell.rubLabel.font = [UIFont boldSystemFontOfSize:15];

        } else if (indexPath.row == 1) {

            NSDictionary *rates = property.annualRatesMap[property.periods[0]];

            cell.termLabel.text = [property.periods[0]stringByAppendingString:@" Месяцев"];
            cell.kztLabel.text = [rates[@"KZT"] stringByAppendingString:@" %"];
            cell.eurLabel.text = [rates[@"EUR"]stringByAppendingString:@" %"];
            cell.usdLabel.text = [rates[@"USD"]stringByAppendingString:@" %"];
            cell.rubLabel.text = [rates[@"RUB"]stringByAppendingString:@" %"];

        } else if (indexPath.row == 2) {

            NSDictionary *rates = property.annualRatesMap[property.periods[1]];

            cell.termLabel.text = [property.periods[1]stringByAppendingString:@" Месяцев"];
            cell.kztLabel.text = [rates[@"KZT"]stringByAppendingString:@" %"];
            cell.eurLabel.text = [rates[@"EUR"]stringByAppendingString:@" %"];
            cell.usdLabel.text = [rates[@"USD"]stringByAppendingString:@" %"];
            cell.rubLabel.text = [rates[@"RUB"]stringByAppendingString:@" %"];
        }

        resultCell = cell;
    }
        
    return resultCell;
}

#pragma mark - config ui

- (void)configUI {
    [self setBAFTableViewBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:@"Выгодные условия"];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
