//
// Created by Askar Mustafin on 10/26/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositResultViewController.h"
#import "UITableViewController+Extension.h"
#import "UILabel+Attributed.h"
#import "NSString+Ext.h"
#import "MainButton.h"
#import "UIViewController+NavigationController.h"
#import "NotificationCenterHelper.h"


@implementation OnlineDepositResultViewController {}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OnlineDeposit" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - config actions

- (void)clickProceedButton {
    [self popToRoot];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferDoneGotoHistory object:nil];
}

#pragma mark - table view

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 180;
    } else if (indexPath.section == 1) {
        NSArray *statements = [self.onlineDepositInitResponse composeStatement];
        NSDictionary *statement = statements[indexPath.row];
        NSString *text = [NSString stringWithFormat:@"%@ %@", statement[@"title"], statement[@"value"]];
        return [text heightWithFont:[UIFont systemFontOfSize:14] width:[ASize screenWidth] - 16] + 20;
    } else {
        return 0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 1 : {
            NSArray *statements = [self.onlineDepositInitResponse composeStatement];
            return statements.count;
            break;
        }
        case 0 : {
            return 1;
            break;
        }
        default: return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *StatementCellIdentifier = @"StatementCell";
    static NSString *InfoCellIdentifier = @"InfoCell";

    UITableViewCell *resultCell;

    if (indexPath.section == 0) {

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:InfoCellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:InfoCellIdentifier];
        }

        cell.textLabel.text = @"Ваше заявление принято в обработку, после открытия счетов Вам будет "
                "направлено смс уведомление. Операция может занять несколько минут. Статус заявки "
                "Вы сможете проверить в разделе «История операций». В случае, если заявление "
                "направлено в выходной или праздничный день, депозит будет открыт в следующий "
                "рабочий день Банка.";
        cell.textLabel.textColor = [UIColor bafBlueColor];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;

        resultCell = cell;

    } else if (indexPath.section == 1) {

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:StatementCellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:StatementCellIdentifier];
        }

        NSArray *statements = [self.onlineDepositInitResponse composeStatement];
        NSDictionary *statement = statements[indexPath.row];

        cell.textLabel.text = [NSString stringWithFormat:@"%@\n%@", statement[@"title"], statement[@"value"]];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [cell.textLabel setFont:[UIFont systemFontOfSize:13] textColor:[UIColor darkGrayColor] forSubstring:statement[@"title"]];
        [cell.textLabel setFont:[UIFont systemFontOfSize:14] textColor:[UIColor fromRGB:0x26718C] forSubstring:statement[@"value"]];

        resultCell = cell;
    }


    //etc.
    return resultCell;
}

#pragma mark - config ui

- (void)configUI {
    [self setBAFTableViewBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:self.onlineDepositInitResponse.bankAstanaOfficialName];

    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 160)];
    MainButton *proceedButton = [[MainButton alloc] initWithFrame:CGRectMake(( footerView.width - 200 ) * 0.5, (footerView.height - 50) * 0.5, 200, 50)];
    proceedButton.title = @"История операций";
    proceedButton.mainButtonStyle = MainButtonStyleOrange;
    [proceedButton addTarget:self action:@selector(clickProceedButton) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:proceedButton];
    self.tableView.tableFooterView = footerView;
}


@end