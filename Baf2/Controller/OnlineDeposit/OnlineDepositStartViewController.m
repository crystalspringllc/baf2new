//
// Created by Askar Mustafin on 11/2/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositStartViewController.h"
#import "OnlineDepositApi.h"
#import "OnlineDeposit.h"
#import "OnlineDepositProperty.h"
#import "MainButton.h"
#import "OnlineDepositMainViewController.h"
#import "UITableViewController+Extension.h"
#import "UILabel+Attributed.h"
#import "OnlineDepositInfoViewController.h"
#import "OnlineDepositCalculatorViewController.h"

@interface OnlineDepositStartViewController() {}
@property (weak, nonatomic) IBOutlet MainButton *depositCalcButton;
@property (weak, nonatomic) IBOutlet MainButton *conditionsButton;
@property (nonatomic, strong) OnlineDeposit *onlineDeposit;
@end

@implementation OnlineDepositStartViewController {}

+ (id)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OnlineDeposit" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self loadDepositInfo];
}

#pragma mark - load
- (IBAction)clickDepositConditionsButton:(id)sender {
    OnlineDepositInfoViewController *v = [OnlineDepositInfoViewController createVC];
    v.onlineDeposit = self.onlineDeposit;
    [self.navigationController pushViewController:v animated:YES];
}

- (IBAction)clickDepositCalcButton:(id)sender {
    OnlineDepositMainViewController *v = [[OnlineDepositMainViewController alloc] init];
    v.onlineDeposit = self.onlineDeposit;
    [self.navigationController pushViewController:v animated:YES];
}

- (void)loadDepositInfo {
    [MBProgressHUD showAstanaHUDWithTitle:@"Загрузка информации по депозиту" animated:YES];
    [OnlineDepositApi getDepositInfo:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        self.onlineDeposit = [OnlineDeposit instanceFromDictionary:response];
        [self.tableView reloadData];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

#pragma mark - tableview


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *composedInfoCards = [self.onlineDeposit composeInfoCards];
    return composedInfoCards.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier2 = @"cellDetails2";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2];
    }
    NSArray *composedInfoCards = [self.onlineDeposit composeInfoCards];
    NSDictionary *infoCard = composedInfoCards[indexPath.row];
    cell.imageView.frame = CGRectMake(cell.imageView.frame.origin.x, cell.imageView.frame.origin.y, 140, 40);
    cell.imageView.image = [UIImage imageNamed:infoCard[@"image"]];
    cell.imageView.contentMode = UIViewContentModeScaleToFill;
    cell.imageView.transform = CGAffineTransformMakeScale(1, 1);

    cell.textLabel.numberOfLines = 0;
    cell.textLabel.text = [NSString stringWithFormat:@"%@\n%@", infoCard[@"title"], infoCard[@"value"]];
    [cell.textLabel setFont:[UIFont boldSystemFontOfSize:14] textColor:[UIColor blackColor] forSubstring:infoCard[@"title"]];
    [cell.textLabel setFont:[UIFont systemFontOfSize:12] textColor:[UIColor grayColor] forSubstring:infoCard[@"value"]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *composedInfoCards = [self.onlineDeposit composeInfoCards];
    NSDictionary *infoCard = composedInfoCards[indexPath.row];

    switch (indexPath.row) {
        case 0 : {
            if (self.onlineDeposit.properties.count > 0) {
                if (!self.onlineDeposit.properties[0].benefitsArray) {
                    OnlineDepositCalculatorViewController *v = [OnlineDepositCalculatorViewController createVC];
                    v.onlineDeposit = self.onlineDeposit;
                    [self.navigationController pushViewController:v animated:YES];
                    break;
                }
            }
            OnlineDepositMainViewController *v = [[OnlineDepositMainViewController alloc] init];
            v.onlineDeposit = self.onlineDeposit;
            v.startSegmentIndex = 2;
            [self.navigationController pushViewController:v animated:YES];
            break;
        }
        case 1 : {
            OnlineDepositMainViewController *v = [[OnlineDepositMainViewController alloc] init];
            v.onlineDeposit = self.onlineDeposit;
            v.startSegmentIndex = 1;
            [self.navigationController pushViewController:v animated:YES];
            break;
        }
        case 2 : {
            OnlineDepositInfoViewController *v = [OnlineDepositInfoViewController createVC];
            v.onlineDeposit = self.onlineDeposit;
            [self.navigationController pushViewController:v animated:YES];
            break;
        }
        case 3 : {
            OnlineDepositInfoViewController *v = [OnlineDepositInfoViewController createVC];
            v.onlineDeposit = self.onlineDeposit;
            [self.navigationController pushViewController:v animated:YES];
            break;
        }
        case 4 : {
            OnlineDepositInfoViewController *v = [OnlineDepositInfoViewController createVC];
            v.onlineDeposit = self.onlineDeposit;
            [self.navigationController pushViewController:v animated:YES];
            break;
        }
        case 5 : {
            OnlineDepositInfoViewController *v = [OnlineDepositInfoViewController createVC];
            v.onlineDeposit = self.onlineDeposit;
            [self.navigationController pushViewController:v animated:YES];
            break;
        }
        default : break;
    }
}

#pragma mark - config ui

- (void)configUI {
    self.depositCalcButton.mainButtonStyle = MainButtonStyleOrange;
    self.conditionsButton.mainButtonStyle = MainButtonStyleOrange;
    if (self.isNotForMenu) {
        [self setNavigationBackButton];
    } else {
        [self showMenuButton];
    }
    [self setBAFTableViewBackground];
    [self setNavigationTitle:@"Онлайн депозит"];
}

-(void)dealloc
{
    NSLog(@"view is dealloc");
}

@end
