//
//  OnlineDepositCalculatorViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 10/10/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OnlineDeposit;

@interface OnlineDepositCalculatorViewController : UITableViewController<UITextFieldDelegate>

+ (instancetype)createVC;

@property (nonatomic, strong) OnlineDeposit *onlineDeposit;

@end
