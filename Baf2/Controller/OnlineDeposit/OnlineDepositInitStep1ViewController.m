//
//  OnlineDepositInitStep1ViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 10/23/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositInitStep1ViewController.h"
#import "UITableViewController+Extension.h"
#import "OnlineDeposit.h"
#import "OnlineDepositApi.h"
#import "OnlineDepositClientEkd.h"
#import "InsuranceLoadDocumentCell.h"
#import "OnlineDepositInitStep2ViewController.h"
#import "OnlineDepositOpenModel.h"
#import "ActionSheetListPopupViewController.h"
#import "CatalogApi.h"
#import "NSString+Ext.h"
#import "NSDate+Ext.h"
#import <STPopup/STPopupController.h>
#import "STPopupController+Extensions.h"
#import "NotificationCenterHelper.h"
#import "UIViewController+LGSideMenuController.h"
#import "DocumentModel.h"


@interface OnlineDepositInitStep1ViewController ()
@property (nonatomic, strong) OnlineDepositClientEkd *clientEkd;
@property (nonatomic, strong) NSArray *scanUrls;
@property (nonatomic, strong) NSArray *docTypes;
@end

@implementation OnlineDepositInitStep1ViewController
{
    NSMutableArray *typeDocArray;
    NSMutableArray *issuersDocArray;
    DocumentModel *document;
}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OnlineDeposit" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self checkClient];
    [self loadDocumentTypes];
    [self loadDocumentIssuers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - config loadings

- (void)loadDocumentTypes
{
    typeDocArray = [[NSMutableArray alloc] init];
    [CatalogApi getDocumentTypes:^(NSMutableArray *docArray) {
        typeDocArray = [NSMutableArray arrayWithArray:docArray];
    } failure:^(NSString *code, NSString *message) {
    }];
}

- (void)loadDocumentIssuers
{
    issuersDocArray = [[NSMutableArray alloc] init];
    [CatalogApi getDocumentIssuers:^(NSMutableArray *array) {
        issuersDocArray = [NSMutableArray arrayWithArray:array];
    } failure:^(NSString *code, NSString *message) {
    }];
}

- (void)checkClient {
    __weak OnlineDepositInitStep1ViewController *wSelf = self;

    [MBProgressHUD showAstanaHUDWithTitle:@"Загрузка" animated:YES];

    [OnlineDepositApi getClientEkdInfo:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];

        self.clientEkd = [OnlineDepositClientEkd instanceFromDictionary:response];

        if ([self.clientEkd.isresident isEqualToString:@"n"]) {
            [UIAlertView showWithTitle:@""
                               message:@"Уважаемый клиент! К сожалению, открытие счетов для нерезидентов РК через дистанционную систему Интернет банкинг недоступно. Вы можете открыть депозит в любом отделении Банка."
                     cancelButtonTitle:@"Наши адреса"
                     otherButtonTitles:nil
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                   [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenAtmPoints object:nil];
                                   [wSelf.navigationController popToRootViewControllerAnimated:YES];
                              }];
        }
        
        [self fillFields];

    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

#pragma mark - config actions

- (IBAction)switchDocsChangedSwitcher:(id)sender {
    UISwitch *docChangedSwitch = (UISwitch *)sender;
    [self enableAllFields:docChangedSwitch.isOn];
    [self.tableView reloadData];
}

- (IBAction)clickNextButton:(id)sender {

    NSDate *from = [self.dateofissueTextField getDateFromText];
    NSDate *to = [self.validitytermeTextField getDateFromText];

    if (self.doschangedSwitch.isOn) {
        if (self.scanUrls.count < 1) {
            return [UIHelper showAlertWithMessage:@"Пожалуйста вложите актуальный документ."];
        }
        if ([self.idcardnumberTextField.text isEmpty]) {
            return [UIHelper showAlertWithMessage:@"Пожалуйста введите номер документа."];
        }
        if ([self.issuingauthorityTextField.text isEmpty]) {
            return [UIHelper showAlertWithMessage:@"Пожалуйста выберите орган выдачи."];
        }
        if ([self.doctypeTextField.text isEmpty]) {
            return [UIHelper showAlertWithMessage:@"Пожалуйста выберите тип документа."];
        }
        if ([self.dateofissueTextField.value isEmpty]) {
            return [UIHelper showAlertWithMessage:@"Пожалуйста выберите дату выдачи."];
        }
        if ([self.validitytermeTextField.value isEmpty]) {
            return [UIHelper showAlertWithMessage:@"Пожалуйста выберите срок действия."];
        }
    }
    if (![from isEarlierThan:to] || [from isEqualToDate:to] || [to isEarlierThan:[NSDate date]]) {
        [UIHelper showAlertWithMessage:@"Срок действия документа истек, пожалуйста вложите актуальный документ и поменяйте срок."];
        return;
    }

    //!!!PAY ATENTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //PREPARING DATA FOR DEPOSIT OPEN INIT REQUEST
    //PASS THIS TO STEP1 FOR NEXT SETTINGS
    self.onlineDepositOpenModel.identityChanged = self.doschangedSwitch.isOn;
    self.onlineDepositOpenModel.identityScanUrl = self.scanUrls.count > 0 ? self.scanUrls[0] : @"";
    self.onlineDepositOpenModel.identityNum = self.idcardnumberTextField.text;
    self.onlineDepositOpenModel.identityIssueDate = [[self.dateofissueTextField getDateFromText] stringWithDateFormat:@"dd.MM.yyyy"];
    self.onlineDepositOpenModel.identityExpireDate = [[self.validitytermeTextField getDateFromText] stringWithDateFormat:@"dd.MM.yyyy"];
    self.onlineDepositOpenModel.clientBirthDate = [self.birthdayTextField.date stringWithDateFormat:@"dd.MM.yyyy"];
    
    /*if(!self.doschangedSwitch.isOn){
       self.onlineDepositOpenModel.identityType = self.clientEkd.document.docTypeCode;
       self.onlineDepositOpenModel.typeDocTitle = self.clientEkd.document.docType;
    }
    
    if(!self.doschangedSwitch.isOn){
       self.onlineDepositOpenModel.identityIssuer = self.clientEkd.document.docIssuedCode;
       self.onlineDepositOpenModel.issuedDocTitle = self.clientEkd.document.docIssuer;
    }*/
  
    ////////////////////////////////////////////

    OnlineDepositInitStep2ViewController *vc = [OnlineDepositInitStep2ViewController createVC];
    vc.onlineDepositOpenModel = self.onlineDepositOpenModel;
    vc.onlineDeposit = self.onlineDeposit;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - config ui

- (void)configUI {
    __weak OnlineDepositInitStep1ViewController *wSelf = self;

    [self setBAFTableViewBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:@"Шаг 1 из 3"];

    self.loadDocumentsCell.vcOwner = self;
    self.loadDocumentsCell.onImageUploaded = ^(NSArray *uploadedUrls) {
        NSLog(@"LOADED URLS: %@", uploadedUrls);
        wSelf.scanUrls = uploadedUrls;
    };

    //disable all fields
    [self enableAllFields:NO];
    
    self.nextButton.mainButtonStyle = MainButtonStyleOrange;
    self.nextButton.title = @"Далее";

    [self.birthdayTextField.line removeFromSuperview];
    [self.validitytermeTextField.line removeFromSuperview];
    [self.dateofissueTextField.line removeFromSuperview];

    self.doschangedSwitch.onTintColor = [UIColor bafBlueColor];


    self.lastnameTextField.textColor = [UIColor lightGrayColor];
    self.nameTextField.textColor = [UIColor lightGrayColor];
    self.middlenameTextField.textColor = [UIColor lightGrayColor];
    self.phonenumberTextField.textColor = [UIColor lightGrayColor];
    self.iinTextField.textColor = [UIColor lightGrayColor];
    self.birthdayTextField.textField.textColor = [UIColor lightGrayColor];
    self.idcardnumberTextField.textColor = [UIColor lightGrayColor];
    self.issuingauthorityTextField.textColor = [UIColor lightGrayColor];
    self.dateofissueTextField.textField.textColor = [UIColor lightGrayColor];
    self.validitytermeTextField.textField.textColor = [UIColor lightGrayColor];
    self.doctypeTextField.textColor = [UIColor lightGrayColor];
    
    [self.issuingauthorityTextField enabled:NO];
    [self.doctypeTextField enabled:NO];
}

- (void)enableAllFields:(BOOL)isEnable {
    if (isEnable) {
        self.idcardnumberTextField.textColor = [UIColor blackColor];
        self.issuingauthorityTextField.textColor = [UIColor blackColor];
        self.dateofissueTextField.textField.textColor = [UIColor blackColor];
        self.validitytermeTextField.textField.textColor = [UIColor blackColor];
        self.doctypeTextField.textColor = [UIColor blackColor];
    } else {
        self.idcardnumberTextField.textColor = [UIColor lightGrayColor];
        self.issuingauthorityTextField.textColor = [UIColor lightGrayColor];
        self.dateofissueTextField.textField.textColor = [UIColor lightGrayColor];
        self.validitytermeTextField.textField.textColor = [UIColor lightGrayColor];
        self.doctypeTextField.textColor = [UIColor lightGrayColor];
    }

    [self.lastnameTextField enabled:NO];
    [self.nameTextField enabled:NO];
    [self.middlenameTextField enabled:NO];
    [self.phonenumberTextField enabled:NO];
    [self.iinTextField enabled:NO];
    [self.birthdayTextField enabled:NO];

    [self.idcardnumberTextField enabled:isEnable];
   // [self.issuingauthorityTextField enabled:isEnable];
   // [self.doctypeTextField enabled:isEnable];
    [self.dateofissueTextField enabled:isEnable];
    [self.validitytermeTextField enabled:isEnable];
}

- (void)fillFields {
    //client
    self.nameTextField.text = self.clientEkd.firstname;
    self.lastnameTextField.text = self.clientEkd.lastname;
    self.middlenameTextField.text = self.clientEkd.middlename;
    self.birthdayTextField.value = self.clientEkd.birthdate;
    self.iinTextField.text = self.clientEkd.iin;
    self.phonenumberTextField.text = [User sharedInstance].phoneNumber;

    //doc
    self.idcardnumberTextField.text = self.clientEkd.document.docnumber;
    self.issuingauthorityTextField.text = self.clientEkd.document.docIssuer;
    self.doctypeTextField.text = self.clientEkd.document.docType;
    self.dateofissueTextField.value = self.clientEkd.document.issuedDate;
    self.validitytermeTextField.value = self.clientEkd.document.expireDate;
    
    self.onlineDepositOpenModel.identityType = self.clientEkd.document.docTypeCode;
    self.onlineDepositOpenModel.typeDocTitle = self.clientEkd.document.docType;
    self.onlineDepositOpenModel.identityIssuer = self.clientEkd.document.docIssuedCode;
    self.onlineDepositOpenModel.issuedDocTitle = self.clientEkd.document.docIssuer;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0 : {
            return 6;
        }
        case 1 : {
            return 6;
        }
        case 2 : {
            return 3;
        }
        default: return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0 : {
            return 44;
        }
        case 1 : {
            return 44;
        }
        case 2 : {
            
            if (indexPath.row == 0) {
                if (self.doschangedSwitch.isOn) {
                    return 44;
                } else {
                    return 0;
                }
            } else if (indexPath.row == 1) {
                if (self.doschangedSwitch.isOn) {
                    return 117;
                } else {
                    return 0;
                }
            } else if (indexPath.row == 2) {
                return 78;
            }
            
        }
        default: return 0;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2 && indexPath.row == 0) {
        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
    }    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   /* NSArray *docTypes = @[
            @"Паспорт гражданина РК",
            @"Удостоверение гражданина РК",
            @"Вид на жительство иностранного гражданина",
            @"Свидетельство о рождении",
            @"Удостоверения лица без гражданства РК",
            @"Удостоверение беженца",
            @"Паспорт гражданина иностранного государства",
            @"Копия регистрационного свидетельства о регистрации нерезидента в качестве налогоплательщика",
            @"Иной документ",
            @"Виза",
            @"Миграционная карта"
    ];

    NSArray *authorityIssuers = @[
            @"Министерство внутренних дел РК",
            @"Министерство юстиции РК",
            @"Министерство внутренних дел",
            @"Министерство юстиции",
            @"Министерство иностранных дел",
            @"ҚР ІІМ",
            @"ӘМ",
            @"ҚР ӘМ"
    ];*/

    if (indexPath.section == 1 && indexPath.row == 2 && self.doschangedSwitch.isOn) {
        [self showPopupWithList:typeDocArray forTextField:self.doctypeTextField];
    } else if (indexPath.section == 1 && indexPath.row == 1 && self.doschangedSwitch.isOn) {
        [self showPopupWithList:issuersDocArray forTextField:self.issuingauthorityTextField];
    }
}

- (void)showPopupWithList:(NSMutableArray *)list
                     forTextField:(UITextField *)textField
{
    ActionSheetListPopupViewController *cityActionSheet = [ActionSheetListPopupViewController new];
    cityActionSheet.rowForIndexPath = ^ActionSheetListRowObject *(NSIndexPath *indexPath) {
            ActionSheetListRowObject *object = [[ActionSheetListRowObject alloc] init];
            document = list[indexPath.row];
            object.title = document.name;
            return object;
        };
    cityActionSheet.numberOfRowsInSection = ^NSInteger(NSInteger section) {
            return list.count;
        };
    cityActionSheet.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
        DocumentModel *document = list[selectedIndexPath.row];
        textField.text = document.name;
        if(!document.definition == DocumentType){
            self.onlineDepositOpenModel.identityIssuer = document.code;
            self.onlineDepositOpenModel.issuedDocTitle = document.name;
        }else{
            self.onlineDepositOpenModel.identityType = document.code;
            self.onlineDepositOpenModel.typeDocTitle = document.name;
        }

        //textField.text = list[selectedIndexPath.row];
        };
    STPopupController *vc = [[STPopupController alloc] initWithRootViewController:cityActionSheet];
    vc.dismissOnBackgroundTap = true;
    vc.style = STPopupStyleBottomSheet;
    vc.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    [vc presentInViewController:self];
}

@end
