//
// Created by Askar Mustafin on 11/2/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "OnlineDeposit.h"

@interface OnlineDepositStartViewController : UITableViewController

+ (id)createVC;

@property (nonatomic, assign) BOOL isNotForMenu;


@end
