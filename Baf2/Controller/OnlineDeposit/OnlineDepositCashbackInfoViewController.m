//
// Created by Askar Mustafin on 10/24/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositCashbackInfoViewController.h"
#import "UIViewController+Extension.h"
#import "UITableViewController+Extension.h"
#import "UITableViewController+Extension.h"
#import "OnlineDepositProperty.h"
#import "OnlineDepositBenefits.h"
#import "depositeBonusInfoCell.h"
#import "OnlineDepositBenefitTable.h"
#import "BenefitsHeaderInfoCell.h"
#import "BenefitsFooterInfoCell.h"

@interface OnlineDepositCashbackInfoViewController()
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *headerInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftInfoLabel1;
@property (weak, nonatomic) IBOutlet UILabel *centerInfoLabel1;
@property (weak, nonatomic) IBOutlet UILabel *rightInfoLabel1;
@property (weak, nonatomic) IBOutlet UILabel *leftInfoLabel2;
@property (weak, nonatomic) IBOutlet UILabel *centerInfoLabel2;
@property (weak, nonatomic) IBOutlet UILabel *rightInfoLabel2;
@property (weak, nonatomic) IBOutlet UILabel *leftInfoLabel3;
@property (weak, nonatomic) IBOutlet UILabel *centerInfoLabel3;
@property (weak, nonatomic) IBOutlet UILabel *rightInfoLabel3;
@property (weak, nonatomic) IBOutlet UILabel *leftInfoLabel4;
@property (weak, nonatomic) IBOutlet UILabel *centerInfoLabel4;
@property (weak, nonatomic) IBOutlet UILabel *rightInfoLabel4;
@property (weak, nonatomic) IBOutlet UILabel *tableFooterLabel;
@property (weak, nonatomic) IBOutlet UILabel *footerLabel;
//@property (nonatomic, strong) OnlineDeposit *onlineDeposit;


@end
@implementation OnlineDepositCashbackInfoViewController
{
    //IBOutlet UITableView *tableView;
    OnlineDepositBenefits *benefit;
}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OnlineDeposit" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initVars];
    [self configUI];
    
}

/*-(void)setOnlineDeposit:(OnlineDeposit *)onlineDeposit
{
    if(self.onlineDepositCashBack != onlineDeposit){
        self.onlineDepositCashBack = onlineDeposit;
    }
}*/


- (void)initVars
{
    for(OnlineDepositBenefits *newBenefit in self.onlineDepositCashBack.properties[0].benefitsArray)
    {
        if([newBenefit.title isEqualToString:@"Cashback"])
        {
            benefit = newBenefit;
        }
    }
}

- (NSArray *)tableLeftLabelArray
{
    return @[_leftInfoLabel1, _leftInfoLabel2, _leftInfoLabel3, _leftInfoLabel4];
}

- (NSArray *)tableRightLabelArray
{
    return @[_rightInfoLabel1, _rightInfoLabel2, _rightInfoLabel3, _rightInfoLabel4];
}

- (NSArray *)tableCenterLabelArray
{
    return @[_centerInfoLabel1, _centerInfoLabel2, _centerInfoLabel3, _centerInfoLabel4];
}

#pragma mark - tableview delegate

//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (section == 0) {
//        return 1;
//    } else if (section == 1) {
//        return 10;
//    } else if (section == 2) {
//        return 1;
//    } else {
//        return 0;
//    }
//}

#pragma mark - actions

- (IBAction)openLinkClicked:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.bankastana.kz/ru/individual/deposits/astana"]];
}


#pragma mark - config ui

- (void)configUI {
    [self setNavigationTitle:@"Cashback"];
    [self setNavigationBackButton];
    
    
    NSAttributedString * headerHTML = [[NSAttributedString alloc] initWithData:[benefit.header dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    NSAttributedString * tableFooterHTML = [[NSAttributedString alloc] initWithData:[benefit.tableFooter dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    NSAttributedString * footerHTML = [[NSAttributedString alloc] initWithData:[benefit.footer dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];

    self.headerInfoLabel.attributedText = headerHTML;
    self.tableFooterLabel.attributedText = tableFooterHTML;
    self.footerLabel.attributedText = footerHTML;
    
    
    for(int i = 0; i< benefit.table.count; i++)
    {
        OnlineDepositBenefitTable *table = benefit.table[i];
        UILabel *leftLabel =  self.tableLeftLabelArray[i];
        UILabel *rigthLabel = self.tableRightLabelArray[i];
        UILabel *centerLabel = self.tableCenterLabelArray[i];
        leftLabel.text = table.fIELD1;
        rigthLabel.text = table.fIELD2;
        centerLabel.text = table.fIELD3;
    }

    self.tableView.backgroundColor = [UIColor whiteColor];

}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
