//
// Created by Askar Mustafin on 11/2/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositMainViewController.h"
#import "OnlineDepositCalculatorViewController.h"
#import "OnlineDepositCashbackInfoViewController.h"
#import "OnlineDepositCompenstationInfoViewController.h"
#import "OnlineDeposit.h"
#import "OnlineDepositInfoViewController.h"

@interface OnlineDepositMainViewController() {}
@property (nonatomic, strong) NSArray *controllers;
//@property (nonatomic, strong) OnlineDeposit *onlineDeposit;
@end

@implementation OnlineDepositMainViewController {

}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setNavigationBackButton];
    [self setNavigationTitle:@"Онлайн Депозит"];

    OnlineDepositCalculatorViewController *vc1 = [OnlineDepositCalculatorViewController createVC];
    vc1.onlineDeposit = self.onlineDeposit;
    OnlineDepositCompenstationInfoViewController *vc2 = [OnlineDepositCompenstationInfoViewController createVC];
    vc2.onlineDepositCompenstation = self.onlineDeposit;
    OnlineDepositCashbackInfoViewController *vc3 = [OnlineDepositCashbackInfoViewController createVC];
    vc3.onlineDepositCashBack = self.onlineDeposit;
    
    //[vc3 setOnlineDeposit:self.onlineDeposit];

    self.controllers = @[vc1, vc2, vc3];
    self.tabLabelNames = @[@"Калькулятор", @"Компенсация", @"Cashback"];

    [self setDataSource:self];
    self.delegate = self;
    [self reloadData];

    [self selectTabbarIndex:self.startSegmentIndex animation:NO];
    //[self selectTabbarIndex:0];


    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [infoButton addTarget:self action:@selector(clickInfoButton) forControlEvents:UIControlEventTouchUpInside];
    [self setNavigationButton:infoButton atLeft:NO];
}

/*-(void)setOnlineDeposit:(OnlineDeposit *)onlineDeposit
{
    self.onlineDeposit = onlineDeposit;
}*/

/**
 * @brief go to info view controller
 */
- (void)clickInfoButton {
    OnlineDepositInfoViewController *vc = [OnlineDepositInfoViewController createVC];
    vc.onlineDeposit = self.onlineDeposit;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - data source

- (NSInteger)numberOfViewControllers {
    return self.controllers.count;
}

- (UIViewController *)viewControllerForIndex:(NSInteger)index {
    return self.controllers[(NSUInteger) index];
}

- (void)dealloc
{
    NSLog(@"Online deposite main is dealloc");
}

@end
