//
// Created by Askar Mustafin on 10/24/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositCompenstationInfoViewController.h"
#import "UIViewController+Extension.h"
#import "UITableViewController+Extension.h"
#import "OnlineDepositProperty.h"
#import "OnlineDepositBenefits.h"
#import "depositeBonusInfoCell.h"
#import "OnlineDepositBenefitTable.h"
#import "BenefitsHeaderInfoCell.h"
#import "BenefitsFooterInfoCell.h"

@interface OnlineDepositCompenstationInfoViewController()
@property (weak, nonatomic) IBOutlet UILabel *rightLabel6;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel6;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel5;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel5;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel4;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel4;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel3;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel3;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel2;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel2;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel1;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel1;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *depositeInfoHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *depositeInfoFooter1;
@property (weak, nonatomic) IBOutlet UILabel *depositeInfoFooter2;
//@property (nonatomic, strong) OnlineDeposit *onlineDeposit;
@end

@implementation OnlineDepositCompenstationInfoViewController{
    OnlineDepositBenefits *benefit;
}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OnlineDeposit" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}


-(void)setOnlineDeposit:(OnlineDeposit *)onlineDeposit
{
    if(self.onlineDepositCompenstation != onlineDeposit){
    self.onlineDepositCompenstation = onlineDeposit;
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initVars];
    [self configUI];
}

- (NSArray *)tableLeftLabelArray
{
    return @[_leftLabel1, _leftLabel2, _leftLabel3, _leftLabel4, _leftLabel5, _leftLabel6];
}

- (NSArray *)tableRightLabelArray
{
    return @[_rightLabel1, _rightLabel2, _rightLabel3, _rightLabel4, _rightLabel5, _rightLabel6];
}

- (void)initVars
{
    for(OnlineDepositBenefits *newBenefit in self.onlineDepositCompenstation.properties[0].benefitsArray)
    {
        if([newBenefit.title isEqualToString:@"Компенсация"])
        {
            benefit = newBenefit;
        }
    }
}

/*- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return benefit.table.count;
            break;
        case 2:
            return 1;
            break;
        default:
            return 0;
            break;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *tableCellIdentifier = @"bonusInfoCell";
    static NSString *headerInfoCellIdnt = @"BenefitsHeaderInfoCell";
    static NSString *footerInfoCellIdnt = @"BenefitsFooterInfoCell";

  
    if (indexPath.section == 0)
    {
        BenefitsHeaderInfoCell *cell = [self.tableView dequeueReusableCellWithIdentifier:headerInfoCellIdnt forIndexPath:indexPath];
        cell.benefitHeaderLabel.text = benefit.header;
    }
    else if (indexPath.section == 1) {
        depositeBonusInfoCell *cell = [self.tableView dequeueReusableCellWithIdentifier:tableCellIdentifier forIndexPath:indexPath];
        OnlineDepositBenefitTable *table = benefit.table[indexPath.row];
        cell.leftInfoLabel.text = table.fIELD1;
        cell.rightInfoLabel.text = table.fIELD2;
        
        return cell;
    }
    else if (indexPath.section == 2) {
        BenefitsFooterInfoCell *cell = [self.tableView dequeueReusableCellWithIdentifier:footerInfoCellIdnt forIndexPath:indexPath];
        cell.benefitFooterLabel1.text = benefit.tableFooter;
        cell.benefitHeaderLabel2.text = benefit.footer;
        return cell;
    }
    return nil;
}*/



- (void)configUI {
    [self setNavigationTitle:@"Компенсация"];
    
    NSAttributedString * headerHTML = [[NSAttributedString alloc] initWithData:[benefit.header dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];

    NSAttributedString * tableFooterHTML = [[NSAttributedString alloc] initWithData:[benefit.tableFooter dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    NSAttributedString * footerHTML = [[NSAttributedString alloc] initWithData:[benefit.footer dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
        self.depositeInfoHeaderLabel.attributedText = headerHTML;
        self.depositeInfoFooter1.attributedText = tableFooterHTML;
        self.depositeInfoFooter2.attributedText = footerHTML;
    
    
    
    for(int i = 0; i< benefit.table.count; i++)
    {
        OnlineDepositBenefitTable *table = benefit.table[i];
        UILabel *leftLabel =  self.tableLeftLabelArray[i];
        UILabel *rigthLabel = self.tableRightLabelArray[i];
        leftLabel.text = table.fIELD1;
        rigthLabel.text = table.fIELD2;
    }

    self.tableView.height = UITableViewAutomaticDimension;
    
    [self setNavigationBackButton];
}


@end
