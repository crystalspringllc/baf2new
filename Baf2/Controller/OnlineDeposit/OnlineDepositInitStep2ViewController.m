//
// Created by Askar Mustafin on 10/24/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//


#import "OnlineDepositInitStep2ViewController.h"
#import "UITableViewController+Extension.h"
#import "UIViewController+Extension.h"
#import "OnlineDepositInitStep3ViewController.h"
#import "OnlineDepositApi.h"
#import "OnlineDepositOpenModel.h"
#import "MBProgressHUD+AstanaView.h"
#import "ActionSheetListPopupViewController.h"
#import <STPopup/STPopupController.h>
#import "STPopupController+Extensions.h"
#import "FilialBA.h"
#import "OnlineDepositProperty.h"


@interface OnlineDepositInitStep2ViewController() {}
@property (nonatomic, strong) ActionSheetListPopupViewController *sheetlist;
@property (nonatomic, strong) NSString *selectedFilialId;
@end

@implementation OnlineDepositInitStep2ViewController {
    NSMutableArray *filialsArray;
}

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OnlineDeposit" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   // [self configUI];
    [self loadRequisites];
  //  [self fillBankInfoFields];
}

- (void)loadRequisites {
    
    filialsArray = [NSMutableArray array];
      [MBProgressHUD showAstanaHUDWithTitle:@"Загрузка" animated:YES];
      [OnlineDepositApi getBankFilials:^(id response) {
          [MBProgressHUD hideAstanaHUDAnimated:YES];
          
         for(NSDictionary *responseItem in response)
         {
            
             FilialBA *filial = [FilialBA instanceFromDictionary:responseItem];
             [filialsArray addObject:filial];
             if([responseItem isEqualToDictionary:[response firstObject]]){
                _selectedFilialId = filial.filialIdString;
                self.onlineDepositOpenModel.filialBATitle = filial.filialTitle;
             }
         }
          
          self.onlineDepositOpenModel.bankAstanaOfficialName = @"АО «Банк Астаны»";
          self.onlineDepositOpenModel.bankAstanaBik = @"ASFBKZKA";
          self.onlineDepositOpenModel.bankAstanaBin = @"080540014021";
    
          [self configUI];
          [self fillBankInfoFields];

      } failure:^(NSString *code, NSString *message) {
          
          [MBProgressHUD hideAstanaHUDAnimated:YES];
          [self performBlock:^{
              [self goBack];
          } afterDelay:1];

      }];
    
    
//    [MBProgressHUD showAstanaHUDWithTitle:@"Загрузка" animated:YES];
//    [OnlineDepositApi getBankRequisites:^(id response) {
//        if (response[@"bankAstanaOfficialName"] &&
//                response[@"bankAstanaBik"] &&
//                response[@"bankAstanaBin"])
//        {
//            self.onlineDepositOpenModel.bankAstanaOfficialName = @"АО «Банк Астаны»";
//            self.onlineDepositOpenModel.bankAstanaBik = @"ASFBKZKA";
//            self.onlineDepositOpenModel.bankAstanaBin = @"080540014021";
//        }
//
//        [MBProgressHUD hideAstanaHUDAnimated:YES];
//    } failure:^(NSString *code, NSString *message) {
//        [MBProgressHUD hideAstanaHUDAnimated:YES];
//    }];

   
}

#pragma mark - config action

- (IBAction)changeAcceptRulesSwitch:(id)sender {
    UISwitch *changeAcceptSwitch = (UISwitch *)sender;
    if (changeAcceptSwitch.isOn) {
        self.nextButton.mainButtonStyle = MainButtonStyleOrange;
    } else {
        self.nextButton.mainButtonStyle = MainButtonStyleDisable;
    }
}

- (IBAction)clickNextButton:(id)sender {
    
    NSDictionary *params = @{
            @"depositCurrency" : self.onlineDepositOpenModel.depositCurrency ? : [NSNull null],
            @"depositAmount" : [self.onlineDepositOpenModel.depositAmount decimalFormatString] ? : [NSNull null],
            @"depositPeriod" : self.termTextField.text ? : [NSNull null],
            @"identityChanged" : self.onlineDepositOpenModel.identityChanged ? @"true" : @"false",
            @"identityScanUrl" : self.onlineDepositOpenModel.identityScanUrl ? : [NSNull null],
            @"identityNum" : self.onlineDepositOpenModel.identityNum ? : [NSNull null],
            @"identityIssueDate" : self.onlineDepositOpenModel.identityIssueDate ? : [NSNull null],
            @"identityExpireDate" : self.onlineDepositOpenModel.identityExpireDate ? : [NSNull null],
            @"identityType" : self.onlineDepositOpenModel.identityType ? : [NSNull null],
            @"identityIssuer" : self.onlineDepositOpenModel.identityIssuer ? : [NSNull null],
            @"depositInterestRate" : self.onlineDepositOpenModel.depositInterestRate ? : [NSNull null],
            @"depositEffectiveInterestRate" : self.onlineDepositOpenModel.depositEffectiveInterestRate ? : [NSNull null],
            @"depositCapitalization" : [self.capitalizationTextField.text isEqualToString:@"С капитализацией"] ? @"true" : @"false",
            @"depositBankBranch" : self.selectedFilialId ? : [NSNull null],
            @"bankAstanaOfficialName" : self.onlineDepositOpenModel.bankAstanaOfficialName ? : [NSNull null],
            @"bankAstanaBin" : self.onlineDepositOpenModel.bankAstanaBin ? : [NSNull null],
            @"bankAstanaBik" : self.onlineDepositOpenModel.bankAstanaBik ? : [NSNull null],
            @"clientBirthDate" : self.onlineDepositOpenModel.clientBirthDate ? : [NSNull null]
    };

    [MBProgressHUD showAstanaHUDWithTitle:@"Проверка документа" animated:YES];
    NSLog(@"Params to Deposite API: %@", params);
    [OnlineDepositApi openInit:params success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        if ([response isKindOfClass:[NSDictionary class]]) {
            OnlineDepositInitStep3ViewController *vc = [OnlineDepositInitStep3ViewController createVC];
            vc.onlineDepositOpenModel = self.onlineDepositOpenModel;
            vc.onlineDepositInitResponse = [OnlineDepositInitResponse instanceFromDictionary:response];
            [self.navigationController pushViewController:vc animated:YES];
        }
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

#pragma mark - tableview delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak OnlineDepositInitStep2ViewController *wSelf = self;

    self.sheetlist = nil;
    self.sheetlist = [[ActionSheetListPopupViewController alloc] init];

    if (indexPath.section == 0) {

        if (indexPath.row == 0) {
            self.sheetlist.rowForIndexPath = ^ActionSheetListRowObject *(NSIndexPath *indexPath) {
                ActionSheetListRowObject *object = [[ActionSheetListRowObject alloc] init];
               /* NSArray *caps = @[@"Алматинский филиал",
                        @"Астанинский филиал",
                        @"Атырауский филиал",
                        @"Актауский филиал",
                        @"Карагандинский филиал",
                        @"Шымкентский филиал",
                        @"Усть-Каменогорский филиал",
                        @"Павлодарский филиал"];*/
                FilialBA *filial = filialsArray[indexPath.row];
                NSString *cap = filial.filialTitle;
                object.keepObject = cap;
                object.title = cap;
                object.objectId = filial.filialIdString;
                return object;
            };
            self.sheetlist.numberOfRowsInSection = ^NSInteger(NSInteger section) {
                return filialsArray.count;
            };
            self.sheetlist.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
                FilialBA *filial = filialsArray[selectedIndexPath.row];
                wSelf.filialTextField.text = selectedObject;
                wSelf.selectedFilialId = filial.filialIdString;
                self.onlineDepositOpenModel.filialBATitle = filial.filialTitle;
            };

        } else if (indexPath.row == 1) {
        // OnlineDepositProperty *property =  [self.onlineDeposit.properties firstObject];
           // NSArray *caps = property.periods;
            self.sheetlist.rowForIndexPath = ^ActionSheetListRowObject *(NSIndexPath *indexPath) {
                ActionSheetListRowObject *object = [[ActionSheetListRowObject alloc] init];
               // OnlineDepositProperty *property =  [self.onlineDeposit.properties firstObject];
                
                NSArray *caps = @[@"12", @"24"];
                NSString *cap = caps[indexPath.row];
                object.keepObject = cap;
                object.title = cap;
                return object;
            };
            self.sheetlist.numberOfRowsInSection = ^NSInteger(NSInteger section) {
                return 2;
            };
            self.sheetlist.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
                wSelf.termTextField.text = selectedObject;
            };

        } else if (indexPath.row == 2) {

            self.sheetlist.rowForIndexPath = ^ActionSheetListRowObject *(NSIndexPath *indexPath) {
                ActionSheetListRowObject *object = [[ActionSheetListRowObject alloc] init];
                NSArray *caps = @[@"С капитализацией", @"Без капитализации"];
                NSString *cap = caps[indexPath.row];
                object.keepObject = cap;
                object.title = cap;
                return object;
            };
            self.sheetlist.numberOfRowsInSection = ^NSInteger(NSInteger section) {
                return 2;
            };
            self.sheetlist.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
                wSelf.capitalizationTextField.text = selectedObject;
            };
        }
        if (indexPath.row != 3 && indexPath.row != 4) {
            STPopupController *vc = [[STPopupController alloc] initWithRootViewController:self.sheetlist];
            vc.dismissOnBackgroundTap = true;
            vc.style = STPopupStyleBottomSheet;
            vc.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
            [vc presentInViewController:self];
        } else {
            if (indexPath.row == 3) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.bankastana.kz/ru/news/310-uvazhaemye-klienty"]];
            } else if (indexPath.row == 4) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.bankastana.kz/uploads/content/files/BA_tariffs.zip"]];
            }

        }
    }
}

#pragma mark - config ui

- (void)fillBankInfoFields {
    self.filialTextField.text = @"Алматинский";//self.onlineDepositOpenModel.depositBankBranch;
    self.termTextField.text = [self.onlineDepositOpenModel.depositPeriod stringValue];
    self.capitalizationTextField.text = self.onlineDepositOpenModel.depositCapitalization ? @"С капитализацией" : @"Без капитализации";
}

- (void)configUI {
    [self setBAFTableViewBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:@"Шаг 2 из 3"];

    self.nextButton.mainButtonStyle = MainButtonStyleDisable;
    self.nextButton.title = @"Далее";

    self.filialTextField.userInteractionEnabled = NO;
    self.filialTextField.placeholder = @"Филиал";
    self.termTextField.userInteractionEnabled = NO;
    self.termTextField.placeholder = @"Период";
    self.capitalizationTextField.userInteractionEnabled = NO;

    self.ruleSwitch.onTintColor = [UIColor bafBlueColor];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

@end
