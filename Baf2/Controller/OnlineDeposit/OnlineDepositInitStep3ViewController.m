//
//  OnlineDepositInitStep3ViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 10/25/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositInitStep3ViewController.h"
#import "UILabel+Attributed.h"
#import "OnlineDepositSMSTableViewCell.h"
#import "NSString+Ext.h"
#import "MainButton.h"
#import "OnlineDepositApi.h"
#import "OnlineDepositResultViewController.h"
#import "LastOperationDetails.h"
#import "UITableViewController+Extension.h"
#import "OnlineDepositOpenModel.h"

@interface OnlineDepositInitStep3ViewController ()
@property (nonatomic, strong) NSString *smsCode;
@property (nonatomic, strong) MainButton *proceedButton;
@property (nonatomic, assign) BOOL isAceptRules;
@property (nonatomic, assign) BOOL isSmsSentAgain;
@property (nonatomic, assign) BOOL isSmsCheck;

@end

@implementation OnlineDepositInitStep3ViewController

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OnlineDeposit" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - confgi actions

- (void)sendSmsCodeAgain {
    [MBProgressHUD showAstanaHUDWithTitle:@"Повторная отправка смс кода" animated:YES];
    [OnlineDepositApi sendSmsAgain:self.onlineDepositInitResponse success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [Toast showToast:@"На ваш номер выслан смс код"];
        self.isSmsSentAgain = YES;
        self.isSmsCheck = NO;
        [self.tableView reloadData];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

- (void)switchChanged:(UISwitch *)switcher {
    self.isAceptRules = switcher.isOn;
    [self.tableView reloadData];
}

- (void)checkSmsCode:(NSString *)sms {
    //if ok
    [MBProgressHUD showAstanaHUDWithTitle:@"Проверка смс кода" animated:YES];
    [OnlineDepositApi checkSms:sms serviceLogId:self.onlineDepositInitResponse.servicelogId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        self.proceedButton.mainButtonStyle = MainButtonStyleOrange;
        [Toast showToast:@"Смс код успешно подтвержден"];
        self.isSmsCheck = YES;
        [self.tableView reloadData];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        self.proceedButton.mainButtonStyle = MainButtonStyleDisable;
        self.isSmsSentAgain = NO;
        self.isSmsCheck = NO;
        [self.tableView reloadData];
        //@"Не верный смс код"
        [Toast showToast:message];
    }];

}

- (void)clickProceedButton {
    //validation
    if ([self.smsCode isEmpty]) {
        [UIHelper showAlertWithMessage:@"Введите смс код для подтверждеия"];
        return;
    }

    //run
    [MBProgressHUD showAstanaHUDWithTitle:@"Открытие дерозита" animated:YES];
    [OnlineDepositApi openRun:self.onlineDepositInitResponse.servicelogId
                      smsCode:self.smsCode success:^(id response) {

                [MBProgressHUD hideAstanaHUDAnimated:YES];

                if (response[@"servicelogId"]) {
                    OnlineDepositResultViewController *vc = [OnlineDepositResultViewController createVC];
                    vc.onlineDepositInitResponse = self.onlineDepositInitResponse;
                    [self.navigationController pushViewController:vc animated:YES];
                }

            } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
            }];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        NSArray *statements = [self.onlineDepositInitResponse composeStatement];
        NSDictionary *statement = statements[indexPath.row];
        NSString *text = [NSString stringWithFormat:@"%@ %@", statement[@"title"], statement[@"value"]];
        return [text heightWithFont:[UIFont systemFontOfSize:14] width:[ASize screenWidth] - 16] + 20;
    } else if (indexPath.section == 1) {
        return 60;
    } else if (indexPath.section == 2) {
        return 180;
    } else if (indexPath.section == 3) {
        if (self.isAceptRules) {
            if (!self.isSmsCheck) {
                return 170;
            } else {
                return 130;
            }
        }
        return 0;
    } else {
        return 0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return [self.onlineDepositInitResponse composeStatement].count;
    } else if (section == 1) {
        return 1;
    } else if (section == 2) {
        return 1;
    } else if (section == 3) {
        return 1;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak OnlineDepositInitStep3ViewController *wSelf = self;

    static NSString *infoCellIdent = @"CellIdentifier1";
    static NSString *smsCodeCellIdent = @"SmsCell";
    static NSString *smsSuccessCellIdent = @"SmsSuccessCell";

    UITableViewCell *resultCell;
    
    if (indexPath.section == 0 ) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:infoCellIdent];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:infoCellIdent];
        }
        
        NSArray *statements = [self.onlineDepositInitResponse composeStatement];
        NSDictionary *statement = statements[indexPath.row];
        
        if(indexPath.row == 2)
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%@\n%@", statement[@"title"], self.onlineDepositOpenModel.typeDocTitle];
        }else if (indexPath.row == 6)
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%@\n%@", statement[@"title"], self.onlineDepositOpenModel.issuedDocTitle];
           
        }else if(indexPath.row == 8)
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%@\n%@", statement[@"title"], self.onlineDepositOpenModel.filialBATitle];
        
        }else{
            cell.textLabel.text = [NSString stringWithFormat:@"%@\n%@", statement[@"title"], statement[@"value"]];
        }
        
        [cell.textLabel setFont:[UIFont systemFontOfSize:14] textColor:[UIColor fromRGB:0x26718C] forSubstring:self.onlineDepositOpenModel.typeDocTitle];
        
         [cell.textLabel setFont:[UIFont systemFontOfSize:14] textColor:[UIColor fromRGB:0x26718C] forSubstring:self.onlineDepositOpenModel.issuedDocTitle];
        
        [cell.textLabel setFont:[UIFont systemFontOfSize:14] textColor:[UIColor fromRGB:0x26718C] forSubstring:self.onlineDepositOpenModel.filialBATitle];
        
        [cell.textLabel setFont:[UIFont systemFontOfSize:13] textColor:[UIColor darkGrayColor] forSubstring:statement[@"title"]];
        [cell.textLabel setFont:[UIFont systemFontOfSize:14] textColor:[UIColor fromRGB:0x26718C] forSubstring:statement[@"value"]];
        
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        
        resultCell = cell;
        
    } else if (indexPath.section == 1) {
        //
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier5"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellIdentifier5"];
        }

        cell.textLabel.text = @"Комплексный договор банковского обслуживания (Договор присоединения) для физических лиц";
        cell.textLabel.textColor = [UIColor bafBlueColor];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;

        UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
        cell.accessoryView = switchView;
        if (!wSelf.isAceptRules) {
            [switchView setOn:NO animated:NO];
        } else {
            [switchView setOn:YES animated:NO];
        }
        [switchView addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];

        resultCell = cell;

    } else if (indexPath.section == 2) {

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier2"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellIdentifier2"];
        }
        cell.textLabel.text = @"Подписывая Заявление посредством динамической идентификации (ОТР-пароль), "
                "которое является неотъемлемой частью Комплексного договора банковского обслуживания, "
                "Клиент подтверждает, что он ознакомлен и соглашается со всеми условиями Договора "
                "и с соглашением дистанционной системы Интернет-банкинг для физических лиц размещенными "
                "на официальном веб-сайте Банка принимая их без каких-либо возражений и замечаний.";
        cell.textLabel.textColor = [UIColor bafBlueColor];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        resultCell = cell;

    } else if (indexPath.section == 3) {


        if (!self.isSmsCheck) {

            OnlineDepositSMSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:smsCodeCellIdent];
            if (cell == nil) {
                cell = [[OnlineDepositSMSTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:smsCodeCellIdent];
            }
            if (self.isSmsCheck) {
                cell.smsTextfield.enabled = NO;
            } else {
                cell.smsTextfield.enabled = YES;
            }
            cell.onSmsTextfieldEndEditing = ^(NSString *smsCode) {
                if (!wSelf.isSmsCheck) {
                    wSelf.smsCode = smsCode;
                    [wSelf checkSmsCode:smsCode];
                }
            };
            cell.onSendSmsCodeAgain = ^{
                [wSelf sendSmsCodeAgain];
            };
            if (self.isSmsSentAgain) {
                cell.smsTextfield.text = @"";
            }
            cell.phoneLabel.text = [NSString stringWithFormat:@"Код подтверждения отправлен вам на номер: %@", self.onlineDepositInitResponse.clientPhone];

            resultCell = cell;

        } else {

            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:smsSuccessCellIdent];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:smsSuccessCellIdent];
            }

            resultCell = cell;
        }

    }
    return resultCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.bankastana.kz/uploads/content/files/%D0%9A%D0%BE%D0%BC%D0%BF%D0%BB%D0%B5%D0%BA%D1%81%D0%BD%D1%8B%D0%B9%20%D0%B4%D0%BE%D0%B3%D0%BE%D0%B2%D0%BE%D1%80%20%D0%B4%D0%BB%D1%8F%20%D0%A4%D0%9B%2019072017.zip"]];
    }
}

#pragma mark - config ui

- (void)configUI {
    [self setBAFTableViewBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:@"Шаг 3 из 3"];

    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], 160)];
    self.proceedButton = [[MainButton alloc] initWithFrame:CGRectMake(( footerView.width - 100 ) * 0.5, (footerView.height - 50) * 0.5, 100, 50)];
    self.proceedButton.title = @"Подписать";
    self.proceedButton.mainButtonStyle = MainButtonStyleDisable;
    [self.proceedButton addTarget:self action:@selector(clickProceedButton) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:self.proceedButton];
    self.tableView.tableFooterView = footerView;
}

@end
