//
//  OnlineDepositInitStep3ViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 10/25/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnlineDepositInitResponse.h"
#import "OnlineDepositOpenModel.h"

@class MainButton;

@interface OnlineDepositInitStep3ViewController : UITableViewController

+ (instancetype)createVC;

@property (nonatomic, strong) OnlineDepositInitResponse *onlineDepositInitResponse;
@property (nonatomic, strong) OnlineDepositOpenModel *onlineDepositOpenModel;

@end
