//
//  OnlineDepositCalculatorViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 10/10/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositCalculatorViewController.h"
#import "OnlineDepositApi.h"
#import "OnlineDeposit.h"
#import "NSString+Ext.h"
#import "OnlineDepositProperty.h"
#import "UITableViewController+Extension.h"
#import "OnlineDepositInfoViewController.h"
#import "OnlineDepositInitStep1ViewController.h"
#import "OnlineDepositCompenstationInfoViewController.h"
#import "OnlineDepositCashbackInfoViewController.h"
#import "OnlineDepositOpenModel.h"
#import "NotificationCenterHelper.h"
#import "UIViewController+NavigationController.h"
#import "RequestCardTypesViewController.h"

@interface OnlineDepositCalculatorViewController ()
@property (weak, nonatomic) IBOutlet UILabel *accumulateSumLabel; //Я накоплю
@property (weak, nonatomic) IBOutlet UILabel *percentRateLabel; //Ставка
@property (weak, nonatomic) IBOutlet UILabel *persentAccrualLabel; //Начисление процентов
@property (weak, nonatomic) IBOutlet UILabel *monthlyRefillLabel; //Ежемесячно пополняю
@property (weak, nonatomic) IBOutlet UITextField *depositSumTextField;
@property (weak, nonatomic) IBOutlet UISlider *depositSumSlider;
@property (weak, nonatomic) IBOutlet UISegmentedControl *currencySegmentedControl;
@property (weak, nonatomic) IBOutlet UITextField *monthlyRefillTextField;
@property (weak, nonatomic) IBOutlet UISlider *monthlyRefillSlider;
@property (weak, nonatomic) IBOutlet UISegmentedControl *termSegmentedControl;
@property (weak, nonatomic) IBOutlet MainButton *onlineDepositButton;
@property (weak, nonatomic) IBOutlet UISwitch *isCapitalized;

@property (nonatomic, strong) OnlineDepositOpenModel *onlineDepositOpenModel;
@end

@implementation OnlineDepositCalculatorViewController

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OnlineDeposit" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

    self.onlineDepositOpenModel = [[OnlineDepositOpenModel alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - calculate deposit

- (double)calcIsTotal:(BOOL)isTotal {
    OnlineDepositProperty *depositProperty = self.onlineDeposit.properties[0];
    NSString *selectedTerm = [self.termSegmentedControl titleForSegmentAtIndex:self.termSegmentedControl.selectedSegmentIndex];
    NSString *selectedCurrency = [self.currencySegmentedControl titleForSegmentAtIndex:self.currencySegmentedControl.selectedSegmentIndex];

    double totalDepositAmount = [[self.depositSumTextField.text numberDecimalFormat] doubleValue];

    double depositMonthlyReplenishment = [[self.monthlyRefillTextField.text numberDecimalFormat] doubleValue];
    int depositPeriod = [selectedTerm intValue];
    double depositRate = [depositProperty.ratesMap[selectedTerm][selectedCurrency] doubleValue]; //depends on choosen currency and onlineDeposit object
    double depositAmount = [[self.depositSumTextField.text numberDecimalFormat] doubleValue];
    BOOL isCapitalized = self.isCapitalized.isOn;

    for (int i = 0; i < depositPeriod; i++) {
        double calculatedFrom = isCapitalized ? totalDepositAmount : depositAmount;
        totalDepositAmount += (calculatedFrom * depositRate / 100) / 12 + depositMonthlyReplenishment;
    }

    if (!isTotal) {
        totalDepositAmount = totalDepositAmount - depositAmount - depositPeriod * depositMonthlyReplenishment;
    }

    return totalDepositAmount;
}

#pragma mark -
#pragma mark - config action


/**
 * @brief initial deposit sum textfield
 * @param sender UITextField
 */
- (IBAction)changeValueDepositSum:(id)sender {
    self.depositSumSlider.value = [((UITextField *) sender).text floatValue];
    [self fillCalculatedInfoViews];
}

/**
 * @brief initial deposit sum slider
 * @param sender UISlider
 */
- (IBAction)changeValueDepositSumSlider:(id)sender {
    NSNumber *initialSumNumber = @(((UISlider *) sender).value);
    self.depositSumTextField.text = [initialSumNumber decimalFormatString];
    [self fillCalculatedInfoViews];
}

/**
 * @brief monthly deposit sum textfield
 * @param sender UITextField
 */
- (IBAction)changeValueMonthlyRefil:(id)sender {
    self.monthlyRefillSlider.value = [((UITextField *) sender).text floatValue];
    [self fillCalculatedInfoViews];
}

/**
 * @brief monthly deposit sum slider
 * @param sender UISlider
 */
- (IBAction)changeValueMonthlyRefillSlider:(id)sender {
    NSNumber *monthlySumNumber = @(((UISlider *) sender).value);
    self.monthlyRefillTextField.text = [monthlySumNumber decimalFormatString];
    [self fillCalculatedInfoViews];
}

/**
 * @brief Term 12 or 24 months
 * @param sender UISegmentedControll
 */
- (IBAction)changeValueTermSegment:(id)sender {
    [self fillCalculatedInfoViews];
}

/**
 * @brief Select currency segmented controll
 * @param sender UISegmentedControll
 */
- (IBAction)changeValueCurrencySegment:(id)sender {
    [self fillCalculatedInfoViews];
}
- (IBAction)changeValueCapitalization:(id)sender {
    [self fillCalculatedInfoViews];
}

/**
 * @brief click on open deposit button
 * @param sender
 */
- (IBAction)clickOnlineDepositButton:(id)sender {

    __weak OnlineDepositCalculatorViewController *wSelf = self;
    [self fillCalculatedInfoViews];

    [MBProgressHUD showAstanaHUDWithTitle:@"Проверка клиента" animated:YES];
    [OnlineDepositApi isBafClient:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];

        NSNumber *isClientNum = response[@"isClient"];
        BOOL isClient = [isClientNum boolValue];
        if (isClient) {
            OnlineDepositInitStep1ViewController *vc = [OnlineDepositInitStep1ViewController createVC];
            vc.onlineDeposit = self.onlineDeposit;
            vc.onlineDepositOpenModel = self.onlineDepositOpenModel;
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            [UIAlertView showWithTitle:@"Вы не являетесь клиентом банка"
                               message:@"Для того чтобы им стать откройте счет в нашем банке"
                     cancelButtonTitle:@"Отмена"
                     otherButtonTitles:@[@"Перейти в раздел заказа карты"]
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (alertView.cancelButtonIndex != buttonIndex) {
                                      RequestCardTypesViewController *v = [[RequestCardTypesViewController alloc] initWithStyle:UITableViewStyleGrouped];
                                      [wSelf.navigationController pushViewController:v animated:YES];
                                  }
                              }];
        }

    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

#pragma mark - textfield delegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSString *text = textField.text;
    textField.text = [[text numberDecimalFormat] decimalFormatString];
}

#pragma mark - config ui

- (void)configUI {
    [self setBAFTableViewBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:@"Онлайн депозит"];

    self.isCapitalized.onTintColor = [UIColor fromRGB:0x297FB2];

    self.depositSumTextField.delegate = self;
    self.monthlyRefillTextField.delegate = self;
}

- (void)fillCalculatedInfoViews {

    //1 'Я накоплю'
    NSString *selectedCurrency = [self.currencySegmentedControl titleForSegmentAtIndex:self.currencySegmentedControl.selectedSegmentIndex];
    self.accumulateSumLabel.text = [NSString stringWithFormat:@"%@ %@", [@([self calcIsTotal:YES]) decimalFormatString], selectedCurrency];

    //2 'Ставка'
    NSString *selectedTerm = [self.termSegmentedControl titleForSegmentAtIndex:self.termSegmentedControl.selectedSegmentIndex];
    OnlineDepositProperty *depositProperty = self.onlineDeposit.properties[0];
    NSString *percentRate = depositProperty.ratesMap[selectedTerm][selectedCurrency];
    self.percentRateLabel.text = [NSString stringWithFormat:@"%@%%", percentRate];

    //3 'Начисление процентов'
    self.persentAccrualLabel.text = [NSString stringWithFormat:@"%@ %@", [@([self calcIsTotal:NO]) decimalFormatString], selectedCurrency];


    //4 'Ежемесячно пополняю'
    self.monthlyRefillLabel.text = [NSString stringWithFormat:@"%@ %@", [[self.monthlyRefillTextField.text numberDecimalFormat] decimalFormatString] ? : @"0.00", selectedCurrency];


    //!!!PAY ATENTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //PREPARING DATA FOR DEPOSIT OPEN INIT REQUEST
    //PASS THIS TO STEP1 FOR NEXT SETTINGS
    self.onlineDepositOpenModel.depositCurrency = self.accumulateSumLabel.text;
    self.onlineDepositOpenModel.depositAmount = @0;
    self.onlineDepositOpenModel.depositPeriod = @([selectedTerm intValue]);
    self.onlineDepositOpenModel.depositInterestRate = [self.onlineDeposit getDepositInterestRate:selectedTerm];
    self.onlineDepositOpenModel.depositEffectiveInterestRate = [self.onlineDeposit getDepositEffectiveInterestRate:selectedTerm];

    ////////////////////////////////////////
    ///////////////////////////////////////
}

@end
