//
// Created by Askar Mustafin on 10/24/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "OnlineDeposit.h"

@interface OnlineDepositCashbackInfoViewController : UITableViewController

//-(void)setOnlineDeposit:(OnlineDeposit *)onlineDeposit;

@property (nonatomic, weak) OnlineDeposit *onlineDepositCashBack;

+ (instancetype)createVC;

@end
