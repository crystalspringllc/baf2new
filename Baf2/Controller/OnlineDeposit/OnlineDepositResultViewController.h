//
// Created by Askar Mustafin on 10/26/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OnlineDepositInitResponse.h"

@interface OnlineDepositResultViewController : UITableViewController

+ (instancetype)createVC;

@property (nonatomic, strong) OnlineDepositInitResponse *onlineDepositInitResponse;

@end