//
// Created by Askar Mustafin on 11/2/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BAFTabPagerViewController.h"

@class OnlineDeposit;


@interface OnlineDepositMainViewController : BAFTabPagerViewController
@property (nonatomic, strong) OnlineDeposit *onlineDeposit;
//-(void)setOnlineDeposit:(OnlineDeposit *)onlineDeposit;
@end
