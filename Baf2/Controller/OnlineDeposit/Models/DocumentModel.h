//
//  DocumentModel.h
//  Baf2
//
//  Created by developer on 03.01.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "ModelObject.h"

typedef NS_ENUM(NSInteger , DefinitonDocument) {
    DocumentIssued = 0,
    DocumentType
};

@interface DocumentModel : ModelObject

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSNumber *Id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) DefinitonDocument definition;

+ (DocumentModel *)instanceFromDictionary:(NSDictionary *)aDictionary;

@end
