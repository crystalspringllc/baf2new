//
//  DocumentModel.m
//  Baf2
//
//  Created by developer on 03.01.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import "DocumentModel.h"

@implementation DocumentModel

+ (DocumentModel *)instanceFromDictionary:(NSDictionary *)aDictionary {
    
    DocumentModel *instance = [[DocumentModel alloc]init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
    
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    objectData = aDictionary;
    
    self.code = [self stringValueForKey:@"code"];
    self.name = [self stringValueForKey:@"name"];
    self.Id = [self numberValueForKey:@"id"];
    
    objectData = nil;
}


@end
