//
//  OnlineDepositPopupViewController.h
//  Baf2
//
//  Created by Kazhenbayeva Gulnaz on 11/2/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnlineDepositPopupViewController : UIViewController

@property (nonatomic, copy) void (^onInterestingButtonClick)();

@end
