//
//  OnlineDepositPopupViewController.m
//  Baf2
//
//  Created by Kazhenbayeva Gulnaz on 11/2/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "OnlineDepositPopupViewController.h"
#import <STPopup/STPopup.h>
#import "Constants.h"

#define kOnlineDepositInfoText @"Открой Онлайн депозит в Мобильном Банке и следующие 3 месяца получай Бонусы: \n1. Дополнительный Cashback по вкладу до 10% \n2. Возмещение комиссии за перевод депозитов в Astana Bank из любых банков Казахстана до 10 000 тенге \n3. Бесплатная сейфовая ячейка сроком на 3 месяца Подробная информация и дистанционное открытие вклада в разделе «Онлайн депозит»."

@interface OnlineDepositPopupViewController ()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIButton *interestingButton;
@property (weak, nonatomic) IBOutlet UIButton *laterButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;


@end

@implementation OnlineDepositPopupViewController

-(id)init {
    self = [super init];
    if (self) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OnlineDepositPopup" bundle:nil];
        self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([OnlineDepositPopupViewController class])];
    }
    return self;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth] - 40, 450);
//    self.landscapeContentSizeInPopup = CGSizeMake([ASize screenWidth] - 40, 300);
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    CGSize size = _containerView.bounds.size;
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth] - 40, size.height);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
}

#pragma mark - configUI

-(void)configUI {
    
    _titleLabel.textColor = [UIColor bafBlueColor];
    _infoLabel.textColor = [UIColor bafBlueColor];
    _infoLabel.text = kOnlineDepositInfoText;
    
    [_interestingButton setTitleColor:[UIColor darkGreen] forState:UIControlStateNormal];
    [_laterButton setTitleColor:[UIColor darkGreen] forState:UIControlStateNormal];
    [_closeButton setTitleColor:[UIColor darkGreen] forState:UIControlStateNormal];
}

#pragma mark - actions

- (IBAction)interestingTapped:(id)sender {
    NSLog(@"interesting Tapped");
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kHideOnlineDepositeInfo];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (self.onInterestingButtonClick) {
        self.onInterestingButtonClick();
    }
    
    [self.popupController dismiss];
}

- (IBAction)laterTapped:(id)sender {
    NSLog(@"later Tapped");
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kHideOnlineDepositeInfo];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.popupController dismiss];
}

- (IBAction)closeTapped:(id)sender {
    NSLog(@"close Tapped");
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kHideOnlineDepositeInfo];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.popupController dismiss];
}

#pragma mark - 

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
@end
