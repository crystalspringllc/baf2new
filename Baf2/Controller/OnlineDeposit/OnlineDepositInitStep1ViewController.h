//
//  OnlineDepositInitStep1ViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 10/23/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FloatingTextField.h"
#import "MainButton.h"
#import "DatePickerInputTextField.h"
#import "OnlineDeposit.h"

@class InsuranceLoadDocumentCell;
@class OnlineDepositOpenModel;

@interface OnlineDepositInitStep1ViewController : UITableViewController

@property (nonatomic, weak) IBOutlet FloatingTextField *lastnameTextField;
@property (nonatomic, weak) IBOutlet FloatingTextField *nameTextField;
@property (nonatomic, weak) IBOutlet FloatingTextField *middlenameTextField;
@property (nonatomic, weak) IBOutlet FloatingTextField *phonenumberTextField;
@property (nonatomic, weak) IBOutlet FloatingTextField *iinTextField;
@property (nonatomic, weak) IBOutlet DatePickerInputTextField *birthdayTextField;

@property (nonatomic, weak) IBOutlet FloatingTextField *idcardnumberTextField;
@property (nonatomic, weak) IBOutlet FloatingTextField *issuingauthorityTextField;
@property (nonatomic, weak) IBOutlet FloatingTextField *doctypeTextField;
@property (nonatomic, weak) IBOutlet DatePickerInputTextField *dateofissueTextField;
@property (nonatomic, weak) IBOutlet DatePickerInputTextField *validitytermeTextField;

@property (weak, nonatomic) IBOutlet UISwitch *doschangedSwitch;

@property (weak, nonatomic) IBOutlet InsuranceLoadDocumentCell *loadDocumentsCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *loadDocumentTitleCell;
@property (weak, nonatomic) IBOutlet MainButton *nextButton;

@property (nonatomic, strong) OnlineDepositOpenModel *onlineDepositOpenModel;
@property (nonatomic, strong) OnlineDeposit *onlineDeposit;

+ (instancetype)createVC;

@end
