//
// Created by Askar Mustafin on 4/10/16.
// Copyright (c) 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PaymentsQuickViewController : UIViewController

+ (instancetype)createVC;

@end