//
//  AccountAliasEditViewController.h
//  Baf2
//
//  Created by Shyngys Kassymov on 20.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FloatingTextField, MainButton;

@interface AccountAliasEditViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet FloatingTextField *aliasTextField;
@property (weak, nonatomic) IBOutlet MainButton *doneButton;

@property (nonatomic, copy) void (^didTapDoneButton)(NSString *alias);

@property (nonatomic, weak) NSString *initialAlias;

@end
