//
//  CardMultiCurrencyConverterResultViewController.h
//  BAF
//
//  Created by Almas Adilbek on 6/10/15.
//
//



@class TransferCurrencyRates;
@class Card;

@interface CardMultiCurrencyConverterResultViewController : UIViewController

@property (nonatomic, weak) NSNumber *servicelogId;

@property(nonatomic, copy) NSString *operationId;
@property(nonatomic, weak) TransferCurrencyRates *currencyRates;
@property(nonatomic, copy) NSString *fromCurrency;
@property(nonatomic, copy) NSString *toCurrency;
@property(nonatomic, copy)   NSNumber *amount;
@property(nonatomic, weak) Card *cardCurrencyAccount;
@property (nonatomic, copy) NSNumber *comission;
@property (nonatomic, copy) NSString *comissionCurrency;

@property (nonatomic) BOOL saveAsTemplate;

- (void)gotoRoot;

- (void)gotoOperations;
@end
