//
//  NewsAddPostViewController.m
//  Baf2
//
//  Created by nmaksut on 18.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "NewsAddPostViewController.h"
#import "MainButton.h"
#import "NewsAddImageCollectionCell.h"
#import "UIActionSheet+Blocks.h"
#import "TOCropViewController.h"
#import "NewsApi.h"
#import "NotificationCenterHelper.h"
#import "JVFloatLabeledTextView.h"
#import "UITableViewController+Extension.h"
#import "FloatingTextView.h"

@interface NewsAddPostViewController () <UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, TOCropViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet FloatingTextView *textView;
@property (weak, nonatomic) IBOutlet MainButton *attachButton;
@property (weak, nonatomic) IBOutlet MainButton *shareButton;


@property (nonatomic, strong) NSMutableArray *addedImages;
@property (nonatomic, strong) NSMutableArray *imageUrls;
@end

@implementation NewsAddPostViewController

#pragma mark - Init

+ (id)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"News" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)initVars {
    self.addedImages = [NSMutableArray new];
    self.imageUrls   = [NSMutableArray new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initVars];
    [self configUI];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.textView becomeFirstResponder];
}

#pragma mark - config actions

- (IBAction)shareTapped:(id)sender {
    if (self.textView.text.length == 0) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_textt", nil)];
        return;
    }
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"adding_note", nil) animated:YES];
    [NewsApi addPostWithSubject:@"" message:self.textView.text share:@"public" imageUrls:self.imageUrls success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:NO];
        
        [Toast showToast:NSLocalizedString(@"note_added", nil)];
        
        [self.navigationController popViewControllerAnimated:YES];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserAddedPost object:nil];
        
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

- (void)openImagePicker {
    __weak typeof(self) wself = self;
    [UIActionSheet showInView:self.view withTitle:nil cancelButtonTitle:NSLocalizedString(@"cancel", nil) destructiveButtonTitle:nil otherButtonTitles:@[NSLocalizedString(@"choose_from_album", nil), NSLocalizedString(@"make_a_photo", nil)] tapBlock:^(UIActionSheet *_Nonnull actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == actionSheet.firstOtherButtonIndex) {
            [wself openPhotoAlbum];
        } else if (buttonIndex == actionSheet.firstOtherButtonIndex + 1) {
            [wself showCamera];
        }
    }];

}

- (void)showCamera {

    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;

        [self presentViewController:controller animated:YES completion:NULL];
    } else {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"device_not_support_camera", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
}

- (void)openPhotoAlbum {
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

    [self presentViewController:controller animated:YES completion:NULL];
}


#pragma mark - collectionview delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.addedImages.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) wSelf = self;
    
    NewsAddImageCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    if (indexPath.item < self.addedImages.count) {
        cell.addImageView.hidden = YES;
        cell.imageView.image = self.addedImages[(NSUInteger) indexPath.item];
        cell.deleteButton.hidden = NO;
        
        cell.deleteBlock = ^{
            [wSelf.addedImages removeObjectAtIndex:(NSUInteger) indexPath.item];
            [wSelf.imageUrls removeObjectAtIndex:(NSUInteger) indexPath.item];
            [wSelf.collectionView reloadData];
        };
    } else {
        cell.addImageView.hidden = NO;
        cell.imageView.image = nil;
        cell.deleteButton.hidden = YES;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item == self.addedImages.count) {
        [self openImagePicker];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(140, 180);
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    __weak NewsAddPostViewController *wSelf = self;
    
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        [wSelf cropPickedImage:image];
    }];
}

- (void)cropPickedImage:(UIImage *)image {
    if (image) {
        TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:image];
        cropViewController.delegate = self;
        [self presentViewController:cropViewController animated:YES completion:nil];
    }
}


#pragma mark - TOCropViewController

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    [cropViewController dismissViewControllerAnimated:YES completion:nil];
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading_photo", nil) animated:true];
    [NewsApi storeImage:image success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        [self.imageUrls addObject:response];
        [self.addedImages addObject:image];
        [self.collectionView reloadData];
        
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark - tableview datasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];

    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

    return cell;
}


#pragma mark - config ui

- (void)configUI {
    [self setBAFTableViewBackground];
    [self setNavigationTitle:NSLocalizedString(@"add_note", nil)];
    [self setNavigationBackButton];
    [self.shareButton setMainButtonStyle:MainButtonStyleOrange];
    [self.attachButton setMainButtonStyle:MainButtonStyleWhite];
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    [(JVFloatLabeledTextView *)self.textView setPlaceholder:NSLocalizedString(@"text", nil)];
    self.textView.text = @"";
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end