//
// Created by Askar Mustafin on 1/9/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestCardFormViewController.h"


@interface RequestCardFormCreditViewController : RequestCardFormViewController

+ (id)createVC;

@end