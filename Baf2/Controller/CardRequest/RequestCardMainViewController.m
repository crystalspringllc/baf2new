//
//  RequestCardMainViewController.m
//  BAF
//
//  Created by Mustafin Askar on 06.01.2017
//
//

#import "RequestCardMainViewController.h"
#import "CatalogCardCell.h"
#import "OCCategory.h"
#import "RequestCardDetailsViewController.h"

@implementation RequestCardMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return NSLocalizedString(@"payment_cards", nil);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.category.products.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 105;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellCards";
    CatalogCardCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CatalogCardCell alloc] initWithReuseIdentifier:CellIdentifier];
    }

    OCProduct *product = self.category.products[(NSUInteger) indexPath.row];

    [cell setCardImageUrl:product.logo title:product.name];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    OCProduct *product = self.category.products[(NSUInteger) indexPath.row];

    RequestCardDetailsViewController *v = [[RequestCardDetailsViewController alloc] init];
    v.product = product;
    [v setNavigationTitle:product.name];
    [self.navigationController pushViewController:v animated:YES];

}

#pragma mark - config ui

- (void)configUI {
    [self setBafBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"card_release", nil)];

    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeightWithoutStatusBarAndNavigationBar]) style:UITableViewStyleGrouped];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
}

#pragma mark - deallocs

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc {
    NSLog(@"RequestionCardMainViewController dealloced");
}

@end