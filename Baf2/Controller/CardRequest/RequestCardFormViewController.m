//
//  RequestCardFormViewController.m
//  BAF
//
//  Created by Almas Adilbek on 4/2/15.
//
//

#import "RequestCardFormViewController.h"
#import "OrderCardApi.h"
#import "OCInitResponse.h"
#import "RequestCardResultViewController.h"
#import "MainButton.h"
#import "FloatingTextField.h"
#import "ActionSheetListPopupViewController.h"
#import "UITableViewController+Extension.h"
#import "OCMaskedTextFieldView.h"

@interface RequestCardFormViewController()

@property (weak, nonatomic) IBOutlet UIImageView *cardTypeImageView;
@property (weak, nonatomic) IBOutlet UILabel *cardTypeLabel;
@property (nonatomic, strong) ActionSheetListPopupViewController *cardTypeActionSheet;

@property (weak, nonatomic) IBOutlet UITableViewCell *cityCell;
@property (weak, nonatomic) IBOutlet UILabel *chooseCityTitle;
@property (weak, nonatomic) IBOutlet UILabel *chooseCityValue;
@property (nonatomic, strong) ActionSheetListPopupViewController *cityActionSheet;
@property (weak, nonatomic) IBOutlet UILabel *iamResidentLabel;

@property (weak, nonatomic) IBOutlet UITableViewCell *currencyCell;
@property (weak, nonatomic) IBOutlet UILabel *currencyTitle;
@property (weak, nonatomic) IBOutlet UILabel *currencyValue;
@property (nonatomic, strong) ActionSheetListPopupViewController *currencyActionSheet;

@property (weak, nonatomic) IBOutlet FloatingTextField *phoneField;
@property (weak, nonatomic) IBOutlet FloatingTextField *iinField;
@property (weak, nonatomic) IBOutlet FloatingTextField *codeWordField;
@property (weak, nonatomic) IBOutlet FloatingTextField *creditLimitField;

@property (nonatomic, strong) UISwitch *residentSwitch;

@property (weak, nonatomic) IBOutlet MainButton *submitButton;

@end

@implementation RequestCardFormViewController

- (void)configUI {
    if ([User sharedInstance].phoneNumber && [User sharedInstance].iin) {
        self.iinField.value = [User sharedInstance].iin;


        if ([self.phoneField isKindOfClass:[OCMaskedTextFieldView class]]) {


            OCMaskedTextFieldView *maskedPhone = (OCMaskedTextFieldView *) self.phoneField;
            [maskedPhone setRawInput:[User sharedInstance].phoneNumber];

        }
    }
}

- (void)initIIN:(NSString *)iin
            phone:(NSString *)phone
         cityCode:(NSString *)cityCode
         codeWord:(NSString *)codeWord
      productCode:(NSString *)productCode
  virtualCurrency:(NSString *)virtualCurrency
          isMulti:(BOOL)isMulti
         isCredit:(BOOL)isCredit
        isVirtual:(BOOL)isVirtual
       isResident:(BOOL)isResident
      creditLimit:(NSNumber *)creditLimit
           income:(NSNumber *)income
          docType:(NSString *)docType
        docNumber:(NSString *)docNumber
     docIssueDate:(NSString *)docIssueDate
     categoryCode:(NSString *)categoryCode
         isAgreed:(BOOL)isAgreed
    childBirthDate:(NSString *)childBirthDate
         childIin:(NSString *)childIin
    childLastname:(NSString *)childLastname
   childFirstname:(NSString *)childFirstname
  childMiddlename:(NSString *)childMiddlename
  isChildResident:(BOOL)isChildResident
{
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading", nil) animated:YES];
    [OrderCardApi initWithIIN:iin
                        phone:[@"8" stringByAppendingString:phone]
                     cityCode:cityCode
                     codeWord:codeWord
                  productCode:productCode
              virtualCurrency:virtualCurrency
                      isMulti:isMulti
                     isCredit:isCredit
                    isVirtual:isVirtual
                   isResident:isResident
                  creditLimit:creditLimit
                       income:income
                      docType:docType
                    docNumber:docNumber
                 docIssueDate:docIssueDate
                 categoryCode:categoryCode
                     isAgreed:isAgreed
               childBirthDate:childBirthDate
                     childIin:childIin
                childLastname:childLastname
               childFirstname:childFirstname
              childMiddlename:childMiddlename
              isChildResident:isChildResident
                      success:^(id response) {
                          OCInitResponse *initResponse = [OCInitResponse instanceFromDictionary:response];
                          [self runOrderWithInitResponse:initResponse];
                      } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
            }];
}

- (void)initIIN:(NSString *)iin
          phone:(NSString *)phone
       cityCode:(NSString *)cityCode
       codeWord:(NSString *)codeWord
    productCode:(NSString *)productCode
virtualCurrency:(NSString *)virtualCurrency
        isMulti:(BOOL)isMulti
       isCredit:(BOOL)isCredit
      isVirtual:(BOOL)isVirtual
     isResident:(BOOL)isResident
    creditLimit:(NSNumber *)creditLimit
         income:(NSNumber *)income
        docType:(NSString *)docType
      docNumber:(NSString *)docNumber
   docIssueDate:(NSString *)docIssueDate
   categoryCode:(NSString *)categoryCode
       isAgreed:(BOOL)isAgreed
{

    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading", nil) animated:YES];
    [OrderCardApi initWithIIN:iin
                        phone:[@"8" stringByAppendingString:phone]
                     cityCode:cityCode
                     codeWord:codeWord
                  productCode:productCode
            virtualCurrency:virtualCurrency
                      isMulti:isMulti
                     isCredit:isCredit
                    isVirtual:isVirtual
                   isResident:isResident
                  creditLimit:creditLimit
                       income:income
                      docType:docType
                    docNumber:docNumber
                 docIssueDate:docIssueDate
                 categoryCode:categoryCode
                     isAgreed:isAgreed
            childBirthDate:nil
                  childIin:nil
             childLastname:nil
            childFirstname:nil
           childMiddlename:nil
           isChildResident:nil
                      success:^(id response) {
                          OCInitResponse *initResponse = [OCInitResponse instanceFromDictionary:response];
                          [self runOrderWithInitResponse:initResponse];
                      } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
            }];
}

- (void)runOrderWithInitResponse:(OCInitResponse *)initResponse {
    [OrderCardApi runWithServiceLogId:initResponse.servicelogId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        OCRunResponse *runResponse = [OCRunResponse instanceFromDictionary:response];
        RequestCardResultViewController *v = [[RequestCardResultViewController alloc] init];
        v.message = NSLocalizedString(@"your_order_successfully_sent", nil);
        v.resultStatus = ResultStatusTypeSuccess;
        v.serviceid = runResponse.servicelogId;
        [self.navigationController pushViewController:v animated:YES];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];
}

@end
