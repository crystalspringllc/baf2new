//
//  RequestCardDetailsViewController.h
//  BAF
//
//  Created by Almas Adilbek on 4/1/15.
//
//

#import "CardsApi.h"

@class OCProduct;

@interface RequestCardDetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) OCProduct *product;

@end