//
// Created by Askar Mustafin on 1/9/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestCardFormViewController.h"

@class City;
@class OCProduct;


@interface RequestCardFormDebitViewController : RequestCardFormViewController

+ (id)createVC;

@end
