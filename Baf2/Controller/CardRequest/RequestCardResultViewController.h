//
//  RequestCardResultViewController.h
//  BAF
//
//  Created by Almas Adilbek on 5/27/15.
//
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ResultStatusType) {
    ResultStatusTypeSuccess,
    ResultStatusTypeFailure
};

@interface RequestCardResultViewController : UIViewController

@property(nonatomic, strong) id serviceid;
@property(nonatomic, assign) ResultStatusType resultStatus;
@property(nonatomic, copy) NSString *message;

@end
