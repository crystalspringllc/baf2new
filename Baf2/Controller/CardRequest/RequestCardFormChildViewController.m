//
// Created by Askar Mustafin on 1/9/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <STPopup/STPopupController.h>
#import "RequestCardFormChildViewController.h"
#import "ActionSheetListPopupViewController.h"
#import "FloatingTextField.h"
#import "MainButton.h"
#import "STPopupController+Extensions.h"
#import "UITableViewController+Extension.h"
#import "NSDate+Helper.h"
#import "OCMaskedTextFieldView.h"
#import "OrderCardApi.h"


@interface RequestCardFormChildViewController()
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UILabel *cardTitle;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;


@property (weak, nonatomic) IBOutlet OCMaskedTextFieldView *phoneField;

@property (weak, nonatomic) IBOutlet FloatingTextField *iinField;
@property (weak, nonatomic) IBOutlet FloatingTextField *codeWordField;
@property (weak, nonatomic) IBOutlet MainButton *orderButton;
@property (nonatomic, strong) ActionSheetListPopupViewController *cityActionSheet;
@property (weak, nonatomic) IBOutlet UITextField *childBirthdayField;
@property (weak, nonatomic) IBOutlet FloatingTextField *childIINField;
@property (weak, nonatomic) IBOutlet FloatingTextField *childfamilyField;
@property (weak, nonatomic) IBOutlet FloatingTextField *childNameField;

@property (weak, nonatomic) IBOutlet FloatingTextField *childMiddlenameField;


@property (nonatomic, strong) UIDatePicker *childBirthdayPicker;

@property (nonatomic, assign) BOOL isResident;

@end

@implementation RequestCardFormChildViewController {}

+ (id)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"RequestCard" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([RequestCardFormChildViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - config action

- (IBAction)clickOrderButton:(id)sender {
    if(self.codeWordField.text.length > 0){
    [super initIIN:self.iinField.value
             phone:self.phoneField.getRawInputText
          cityCode:self.selectedCity.code
          codeWord:self.codeWordField.value
       productCode:self.product.code
   virtualCurrency:nil
           isMulti:YES
          isCredit:NO
         isVirtual:NO
        isResident:YES
       creditLimit:nil
            income:nil
           docType:nil
         docNumber:nil
      docIssueDate:nil
      categoryCode:self.product.categoryCode
          isAgreed:YES
            childBirthDate:self.childBirthdayField.text
                  childIin:self.childIINField.value
             childLastname:self.childfamilyField.value
            childFirstname:self.childNameField.value
           childMiddlename:self.childMiddlenameField.value
           isChildResident:self.isResident];
     } else {
     [UIHelper showAlertWithMessage:@"Заполните пожалуйста поле для кодового слово"];
     }
}

- (void)datePickerDateChanged:(UIDatePicker *)datePicker {
    self.childBirthdayField.text = [datePicker.date stringWithFormat:@"dd.MM.yyyy"];
}

#pragma mark - tableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak RequestCardFormChildViewController *wSelf = self;

    if (indexPath.row == 1) {
        if (!self.cityActionSheet) {
            self.cityActionSheet = [ActionSheetListPopupViewController new];
        }
        self.cityActionSheet.numberOfRowsInSection = ^NSInteger(NSInteger section) {
            return wSelf.product.cities.count;
        };
        self.cityActionSheet.rowForIndexPath = ^ActionSheetListRowObject *(NSIndexPath *indexPath1) {
            City *city = wSelf.product.cities[(NSUInteger) indexPath1.row];
            ActionSheetListRowObject *row = [ActionSheetListRowObject new];
            row.title = city.name;
            row.keepObject = city;
            return row;
        };

        NSInteger selectedIndex = -1;
        for (int i = 0; i < wSelf.product.cities.count; i++) {
            City *city = wSelf.product.cities[(NSUInteger) i];
            if (city.code == wSelf.selectedCity.code) {
                selectedIndex = i;
                break;
            }
        }
        if (selectedIndex >= 0) {
            self.cityActionSheet.selectedIndexPath = [NSIndexPath indexPathForItem:selectedIndex inSection:0];
        }

        self.cityActionSheet.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
            City *city = selectedObject;
            wSelf.selectedCity = city;
            //wSelf.cityLabel.text = wSelf.selectedCity.name;
            [wSelf.tableView reloadData];
        };
        self.cityActionSheet.title = NSLocalizedString(@"choose_city", nil);

        STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:self.cityActionSheet];
        popupController.dismissOnBackgroundTap = true;
        popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
        popupController.style = STPopupStyleBottomSheet;
        [popupController presentInViewController:self];
    }
}

#pragma mark - config ui

- (void)configUI {
    __weak RequestCardFormChildViewController *wSelf = self;

    [self setNavigationTitle:self.product.name];
    [self setNavigationBackButton];
    [self setBAFTableViewBackground];

    [self.logo sd_setImageWithURL:[[NSURL alloc] initWithString:self.product.logo]];
    self.cardTitle.text = self.product.name;
    self.cityLabel.text = self.selectedCity.name;

    self.childBirthdayPicker = [[UIDatePicker alloc] init];
    [self.childBirthdayPicker setDatePickerMode:UIDatePickerModeDate];
    [self.childBirthdayPicker addTarget:self action:@selector(datePickerDateChanged:) forControlEvents:UIControlEventValueChanged];
    self.childBirthdayPicker.backgroundColor = [UIColor whiteColor];
    self.childBirthdayField.inputView = self.childBirthdayPicker;


    [self.phoneField setMask:@"+7 (###) ### ## ##"];
    [self.phoneField showMask];
    self.phoneField.maskedTextField.textAlignment = NSTextAlignmentRight;


    [self.childIINField onValueChanged:^(NSString *value) {
        [wSelf getClientInfoByIIN:value];
    }];

    [super configUI];
}

- (void)getClientInfoByIIN:(NSString *)iin {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading", nil) animated:YES];
    [OrderCardApi getClientInfoByIIN:iin success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        if (response[@"firstName"]) {
            self.childNameField.value = response[@"firstName"];
        }
        if (response[@"lastName"]) {
            self.childfamilyField.value = response[@"lastName"];
        }
        if (response[@"middleName"]) {
            self.childMiddlenameField.value = response[@"middleName"];
        }
        if (response[@"isResident"]) {
            self.isResident = [response[@"isResident"] boolValue];
        }
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
    }];


}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {}

@end
