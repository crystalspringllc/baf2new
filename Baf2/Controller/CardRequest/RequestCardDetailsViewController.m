//
//  RequestCardDetailsViewController.m
//  BAF
//
//  Created by Almas Adilbek on 4/1/15.
//
//

#import "RequestCardDetailsViewController.h"
#import "AATableSection.h"
#import "CatalogCardCell.h"
#import "IconMultilineTitleCell.h"
#import "NSString+Ext.h"
#import "UILabel+Attributed.h"
#import "OCCategory.h"
#import "RequestCardFormViewController.h"
#import "MainButton.h"
#import "ActionSheetListPopupViewController.h"
#import "STPopupController+Extensions.h"
#import "RequestCardFormVirtualViewController.h"
#import "RequestCardFormDebitViewController.h"
#import "RequestCardFormCreditViewController.h"
#import "RequestCardFormChildViewController.h"
#import "RequestCardFormWGViewController.h"

#define kSectionCard @"sectionCard"
#define kSectionBenefits @"kSectionBenefits"
#define kSectionTariffs @"kSectionTariffs"

@interface RequestCardDetailsViewController ()
@end

@implementation RequestCardDetailsViewController {
    UITableView *list;
    NSMutableArray *sections;
    UIView *footerButtonView;
    ActionSheetListPopupViewController *cityPopupActionSheet;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];

    sections = [[NSMutableArray alloc] init];

    AATableSection *section1 = [[AATableSection alloc] initWithSectionId:kSectionCard];
    section1.headerTitle = NSLocalizedString(@"payment_card", nil);
    section1.rows = [@[@""] mutableCopy];
    [sections addObject:section1];


    AATableSection *section2 = [[AATableSection alloc] initWithSectionId:kSectionBenefits];
    section2.headerTitle = NSLocalizedString(@"benefits", nil);
    NSArray *benefits = self.product.properties.benefits;
    NSMutableArray * rows = [NSMutableArray array];
    for(OCBenefit *benefit in benefits) {
        NSString *s = benefit.title;
        if([benefit.text trim].length > 0) s = [s stringByAppendingFormat:@"\n%@", benefit.text];
        [rows addObject:s];
    }
    section2.rows = rows;
    [sections addObject:section2];


    AATableSection *section3 = [[AATableSection alloc] initWithSectionId:kSectionTariffs];
    section3.headerTitle = NSLocalizedString(@"tariffs", nil);
    NSArray *tariffs = self.product.properties.tariffs;
    NSMutableArray * rows2 = [NSMutableArray array];
    for(OCTariff *tariff in tariffs) {
        NSString *s = tariff.title;
        if([tariff.text trim].length > 0) s = [s stringByAppendingFormat:@"\n%@", tariff.text];
        [rows2 addObject:s];
    }
    section3.rows = rows2;
    [sections addObject:section3];
}

#pragma mark - Actions

- (void)requestTapped {
    [self showCitiesPopupActionSheet];
}

- (void)showCitiesPopupActionSheet {
    if (!cityPopupActionSheet) {
        __weak RequestCardDetailsViewController *weakSelf = self;
        
        if (!cityPopupActionSheet) {
            cityPopupActionSheet = [ActionSheetListPopupViewController new];
        }
        cityPopupActionSheet.numberOfRowsInSection = ^NSInteger(NSInteger section) {
            return weakSelf.product.cities.count;
        };
        cityPopupActionSheet.rowForIndexPath = ^ActionSheetListRowObject*(NSIndexPath *indexPath) {
            City *city = weakSelf.product.cities[(NSUInteger) indexPath.row];
            
            ActionSheetListRowObject *row = [ActionSheetListRowObject new];
            row.title = city.name;
            row.keepObject = city;
            
            return row;
        };
        
        cityPopupActionSheet.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
            City *city = selectedObject;
            [weakSelf citySelected:city];
        };
    }
    cityPopupActionSheet.title = NSLocalizedString(@"choose_city", nil);
    
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:cityPopupActionSheet];
    popupController.dismissOnBackgroundTap = true;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    popupController.style = STPopupStyleBottomSheet;
    
    [popupController presentInViewController:self];
}

- (void)citySelected:(City *)city {
    RequestCardFormViewController *v;

    if ([self.product.categoryCode isEqualToString:@"V"]) {
        v = [RequestCardFormVirtualViewController createVC];
    } else if ([self.product.categoryCode isEqualToString:@"D"]) {
        v = [RequestCardFormDebitViewController createVC];
    } else if ([self.product.categoryCode isEqualToString:@"R"]) {
        v = [RequestCardFormCreditViewController createVC];
    } else if ([self.product.categoryCode isEqualToString:@"DC"]) {
        if ([User sharedInstance].loggedIn) {
            v = [RequestCardFormChildViewController createVC];
        } else {
            UINavigationController *navigationController = [UIHelper defaultNavigationController];
            ViewController *viewController = [ViewController createVC];
            viewController.showAsModal = YES;
            [navigationController setViewControllers:@[viewController]];
            [self presentViewController:navigationController animated:YES completion:nil];
        }
    } else if ([self.product.categoryCode isEqualToString:@"WG"]) {
        v = [RequestCardFormWGViewController createVC];
    }

    v.product = self.product;
    v.selectedCity = city;
    [self.navigationController pushViewController:v animated:YES];
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return sections.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    AATableSection *s = sections[(NSUInteger) section];
    return s.headerTitle;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    AATableSection *s = sections[(NSUInteger) section];
    return s.rows.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    AATableSection *s = sections[(NSUInteger) section];
    if([s.sectionId isEqual:kSectionCard]) return 70;
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    AATableSection *s = sections[(NSUInteger) section];
    if ([s.sectionId isEqual:kSectionCard]) {
        if (!footerButtonView) {
            footerButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [self tableView:tableView heightForFooterInSection:section])];
            // Button
            MainButton *submitButton = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, (CGFloat) MIN([ASize screenWidth] * 0.85, 220), 40)];
            [submitButton addTarget:self action:@selector(requestTapped) forControlEvents:UIControlEventTouchUpInside];
            submitButton.mainButtonStyle = MainButtonStyleOrange;
            submitButton.frame = CGRectMake(0, 0, (CGFloat) MIN([ASize screenWidth] * 0.85, 220), 40);
            submitButton.title = NSLocalizedString(@"order_card", nil);
            submitButton.centerX = footerButtonView.middleX;
            submitButton.y = footerButtonView.height - submitButton.height - 15;
            [footerButtonView addSubview:submitButton];
        }
        return footerButtonView;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    AATableSection *s = sections[(NSUInteger) indexPath.section];
    if ([s.sectionId isEqual:kSectionCard]) {
        return 105;
    }
    return [IconMultilineTitleCell cellHeightWithTitle:s.rows[(NSUInteger) indexPath.row] hasIcon:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier;

    AATableSection *s = sections[(NSUInteger) indexPath.section];

    if ([s.sectionId isEqual:kSectionCard]) {
        CellIdentifier = @"CellCards";
        CatalogCardCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[CatalogCardCell alloc] initWithReuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryType = UITableViewCellAccessoryNone;
        }

        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        
        [cell setCardImageUrl:self.product.logo title:self.product.name];

        return cell;
    } else if ([s.sectionId isEqual:kSectionBenefits]) {
        CellIdentifier = @"cellDetails";
        IconMultilineTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[IconMultilineTitleCell alloc] initWithReuseIdentifier:CellIdentifier];
            cell.textLabel.textColor = [UIColor fromRGB:0x444444];
        }
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        cell.imageView.image = [UIImage imageNamed:@"AAFieldCheckmark"];
        cell.textLabel.text = s.rows[(NSUInteger) indexPath.row];

        NSArray *benefits = self.product.properties.benefits;
        
        OCBenefit *benefit = benefits[(NSUInteger) indexPath.row];
        [cell.textLabel setFont:[UIFont boldSystemFontOfSize:14] textColor:[UIColor blackColor] forSubstring:benefit.text];

        return cell;
    } else if ([s.sectionId isEqual:kSectionTariffs]) {
        CellIdentifier = @"cellDetails";
        IconMultilineTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[IconMultilineTitleCell alloc] initWithReuseIdentifier:CellIdentifier];
            cell.textLabel.textColor = [UIColor fromRGB:0x444444];
        }
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        cell.imageView.image = [UIImage imageNamed:@"AAFieldCheckmark"];
        cell.textLabel.text = s.rows[(NSUInteger) indexPath.row];
        
        NSArray *tariffs = self.product.properties.tariffs;
        
        OCTariff *tariff = tariffs[(NSUInteger) indexPath.row];
        [cell.textLabel setFont:[UIFont boldSystemFontOfSize:14] textColor:[UIColor blackColor] forSubstring:tariff.text];
        
        return cell;
    }

    return nil;
}

#pragma mark -

- (void)configUI {
    self.view.backgroundColor = [UIColor whiteColor];
    [self setBafBackground];
    [self setNavigationBackButton];

    list = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeightWithoutStatusBarAndNavigationBar]) style:UITableViewStyleGrouped];
    list.backgroundColor = [UIColor clearColor];
    list.backgroundView = nil;
    list.delegate = self;
    list.dataSource = self;
    list.allowsSelection = NO;
    [self.view addSubview:list];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
