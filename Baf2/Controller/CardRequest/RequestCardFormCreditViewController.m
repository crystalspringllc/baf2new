//
// Created by Askar Mustafin on 1/9/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import "RequestCardFormCreditViewController.h"
#import "FloatingTextField.h"
#import "MainButton.h"
#import "UITableViewController+Extension.h"
#import "ActionSheetListPopupViewController.h"
#import <STPopup/STPopupController.h>
#import "STPopupController+Extensions.h"
#import "NSDate+Helper.h"
#import "NSString+Ext.h"
#import "OCMaskedTextFieldView.h"
#import "IQUIView+IQKeyboardToolbar.h"

#define kIdCard @"Удостоверение гражданина РК"
#define kPassport @"Паспорт гражданина РК"
#define kResidentPermit @"Вид на жительство иностранного гражданина"

@interface RequestCardFormCreditViewController()

@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UILabel *cardTitle;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet OCMaskedTextFieldView *phoneField;
@property (weak, nonatomic) IBOutlet FloatingTextField *iinField;
@property (weak, nonatomic) IBOutlet FloatingTextField *codeWordField;
@property (weak, nonatomic) IBOutlet FloatingTextField *incomeField;
@property (weak, nonatomic) IBOutlet FloatingTextField *desiredSumField;
@property (weak, nonatomic) IBOutlet UISwitch *residentSwitch;
@property (weak, nonatomic) IBOutlet UILabel *documentTypeLabel;
@property (weak, nonatomic) IBOutlet FloatingTextField *documentNumber;
@property (weak, nonatomic) IBOutlet UITextField *givingDateField;
@property (weak, nonatomic) IBOutlet UISwitch *acceptWithRulesSwitch;
@property (weak, nonatomic) IBOutlet MainButton *orderButton;

@property (nonatomic, strong) ActionSheetListPopupViewController *cityActionSheet;
@property (nonatomic, strong) ActionSheetListPopupViewController *documentTypeActionSheet;
@property (nonatomic, strong) UIDatePicker *givingDatePicker;
@property (nonatomic, strong) NSString *selectedDocType;
@property (nonatomic, strong) NSArray *docTypes;

@end

@implementation RequestCardFormCreditViewController {}

+ (id)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"RequestCard" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([RequestCardFormCreditViewController class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    //self.selectedDocType = @"5218";
}

#pragma mark - config actions

- (IBAction)clickOrderButton:(id)sender {
    if(self.codeWordField.text.length > 0){
    [super initIIN:self.iinField.value
             phone:self.phoneField.getRawInputText
          cityCode:self.selectedCity.code
          codeWord:self.codeWordField.value
       productCode:self.product.code
   virtualCurrency:nil
           isMulti:NO
          isCredit:YES
         isVirtual:NO
        isResident:self.residentSwitch.isOn
       creditLimit:@([self.desiredSumField.value intValue])
            income:[self.incomeField.value numberDecimalFormat]
           docType:self.selectedDocType
         docNumber:self.documentNumber.value
      docIssueDate:self.givingDateField.text
      categoryCode:self.product.categoryCode
          isAgreed:self.acceptWithRulesSwitch.isOn];
    } else {
        [UIHelper showAlertWithMessage:@"Заполните пожалуйста поле для кодового слово"];
    }
}

- (void)datePickerDateChanged:(UIDatePicker *)datePicker {
    self.givingDateField.text = [datePicker.date stringWithFormat:@"dd.MM.yyyy"];
}

- (void)residentChanged:(UISwitch *)residentSwitch {
    if (residentSwitch.isOn) {
        self.docTypes = @[kIdCard, kPassport];
        self.documentTypeLabel.text = kIdCard;
        self.self.selectedDocType = @"5218";
    } else {
        self.docTypes = @[kResidentPermit];
        self.documentTypeLabel.text = kResidentPermit;
        self.self.selectedDocType =  @"5220";
    }

    if (self.documentTypeActionSheet) {
        self.documentTypeActionSheet = nil;
    }
}

#pragma mark - tableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0) {
        if (indexPath.row == 1) {

            [self initAndOpenCityChooserView];

        }
    } else if (indexPath.section == 4) {
        if (indexPath.row == 0) {

            [self initAndOpenDocumentTypeChooserView];

        }
    }
}

#pragma mark - config ui

- (void)initAndOpenCityChooserView {
    __weak RequestCardFormCreditViewController *wSelf = self;

    if (!self.cityActionSheet) {
        self.cityActionSheet = [ActionSheetListPopupViewController new];
    }
    self.cityActionSheet.numberOfRowsInSection = ^NSInteger(NSInteger section) {
        return wSelf.product.cities.count;
    };
    self.cityActionSheet.rowForIndexPath = ^ActionSheetListRowObject *(NSIndexPath *indexPath1) {
        City *city = wSelf.product.cities[(NSUInteger) indexPath1.row];
        ActionSheetListRowObject *row = [ActionSheetListRowObject new];
        row.title = city.name;
        row.keepObject = city;
        return row;
    };

    NSInteger selectedIndex = -1;
    for (int i = 0; i < wSelf.product.cities.count; i++) {
        City *city = wSelf.product.cities[(NSUInteger) i];
        if (city.code == wSelf.selectedCity.code) {
            selectedIndex = i;
            break;
        }
    }
    if (selectedIndex >= 0) {
        self.cityActionSheet.selectedIndexPath = [NSIndexPath indexPathForItem:selectedIndex inSection:0];
    }

    self.cityActionSheet.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
        City *city = selectedObject;
        wSelf.selectedCity = city;
        wSelf.cityLabel.text = wSelf.selectedCity.name;
        [wSelf.tableView reloadData];
    };
    self.cityActionSheet.title = NSLocalizedString(@"choose_city", nil);

    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:self.cityActionSheet];
    popupController.dismissOnBackgroundTap = true;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    popupController.style = STPopupStyleBottomSheet;
    [popupController presentInViewController:self];
}

- (void)initAndOpenDocumentTypeChooserView {
    __weak RequestCardFormCreditViewController *wSelf = self;

    if (!self.documentTypeActionSheet) {
        self.documentTypeActionSheet = [ActionSheetListPopupViewController new];
    }

    self.documentTypeActionSheet.numberOfRowsInSection = ^NSInteger(NSInteger section) {
        return wSelf.docTypes.count;
    };

    self.documentTypeActionSheet.rowForIndexPath = ^ActionSheetListRowObject*(NSIndexPath *indexPath1) {
        ActionSheetListRowObject *row = [ActionSheetListRowObject new];
        row.title = wSelf.docTypes[(NSUInteger) indexPath1.row];
        row.keepObject = wSelf.docTypes[(NSUInteger) indexPath1.row];
        return row;
    };

    NSInteger selectedIndex = -1;
    for (int i = 0; i < self.docTypes.count; i++) {
        if ([self.docTypes[(NSUInteger) i] isEqualToString:wSelf.selectedDocType]) {
            selectedIndex = i;
            break;
        }
    }
    if (selectedIndex >= 0) {
        self.documentTypeActionSheet.selectedIndexPath = [NSIndexPath indexPathForItem:selectedIndex inSection:0];
    }

    self.documentTypeActionSheet.onSelectObject = ^(id selectedObject, NSIndexPath *selectedIndexPath) {
        NSString *selObject = (NSString *)selectedObject;
        if ([selObject isEqualToString:kIdCard]) {
            wSelf.selectedDocType = @"5218";
        } else if ([selObject isEqualToString:kPassport]) {
            wSelf.selectedDocType = @"5219";
        } else if ([selObject isEqualToString:kResidentPermit]) {
            wSelf.selectedDocType = @"5220";
        }
        wSelf.documentTypeLabel.text = selectedObject;
        [wSelf.tableView reloadData];
    };
    self.documentTypeActionSheet.title = NSLocalizedString(@"choose_currency", nil);

    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:self.documentTypeActionSheet];
    popupController.dismissOnBackgroundTap = true;
    popupController.containerView.layer.cornerRadius = POPUP_CORNER_RADIUS;
    popupController.style = STPopupStyleBottomSheet;
    [popupController presentInViewController:self];
}

- (void)configUI {
    [self setNavigationTitle:self.product.name];
    [self setNavigationBackButton];
    [self setBAFTableViewBackground];

    [self.logo sd_setImageWithURL:[[NSURL alloc] initWithString:self.product.logo]];
    self.cardTitle.text = self.product.name;
    self.cityLabel.text = self.selectedCity.name;

    self.givingDatePicker = [[UIDatePicker alloc] init];
    [self.givingDatePicker setDatePickerMode:UIDatePickerModeDate];
    [self.givingDatePicker addTarget:self action:@selector(datePickerDateChanged:) forControlEvents:UIControlEventValueChanged];
    self.givingDatePicker.backgroundColor = [UIColor whiteColor];
    self.givingDateField.inputView = self.givingDatePicker;

    [self.residentSwitch addTarget:self action:@selector(residentChanged:) forControlEvents:UIControlEventValueChanged];

    self.docTypes = @[kIdCard, kPassport];
    self.selectedDocType = kIdCard;

    [self.phoneField setMask:@"+7 (###) ### ## ##"];
    [self.phoneField showMask];
    self.phoneField.maskedTextField.textAlignment = NSTextAlignmentRight;
    [super configUI];
}

#pragma mark - did receive

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"RequestCardFormCreditViewController dealloc");
}

@end
