//
//  RequestCardMainViewController.h
//  BAF
//
//  Created by Almas Adilbek on 3/31/15.
//
//

#import "CardsApi.h"

@class OCCategory;

@interface RequestCardMainViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) OCCategory *category;
@end
