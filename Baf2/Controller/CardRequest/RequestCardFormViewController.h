//
//  RequestCardFormViewController.h
//  BAF
//
//  Created by Almas Adilbek on 4/2/15.
//
//

#import <UIKit/UIKit.h>

#import "OCCategory.h"
#import "City.h"

@interface RequestCardFormViewController : UITableViewController

@property (nonatomic, strong) OCProduct *product;
@property (nonatomic, strong) City *selectedCity;

- (void)configUI;

- (void)initIIN:(NSString *)iin
          phone:(NSString *)phone
       cityCode:(NSString *)cityCode
       codeWord:(NSString *)codeWord
    productCode:(NSString *)productCode
        virtualCurrency:(NSString *)virtualCurrency
        isMulti:(BOOL)isMulti
       isCredit:(BOOL)isCredit
      isVirtual:(BOOL)isVirtual
     isResident:(BOOL)isResident
    creditLimit:(NSNumber *)creditLimit
         income:(NSNumber *)income
        docType:(NSString *)docType
      docNumber:(NSString *)docNumber
   docIssueDate:(NSString *)docIssueDate
   categoryCode:(NSString *)categoryCode
       isAgreed:(BOOL)isAgreed;

- (void)initIIN:(NSString *)iin
          phone:(NSString *)phone
       cityCode:(NSString *)cityCode
       codeWord:(NSString *)codeWord
    productCode:(NSString *)productCode
virtualCurrency:(NSString *)virtualCurrency
        isMulti:(BOOL)isMulti
       isCredit:(BOOL)isCredit
      isVirtual:(BOOL)isVirtual
     isResident:(BOOL)isResident
    creditLimit:(NSNumber *)creditLimit
         income:(NSNumber *)income
        docType:(NSString *)docType
      docNumber:(NSString *)docNumber
   docIssueDate:(NSString *)docIssueDate
   categoryCode:(NSString *)categoryCode
       isAgreed:(BOOL)isAgreed
 childBirthDate:(NSString *)childBirthDate
       childIin:(NSString *)childIin
  childLastname:(NSString *)childLastname
 childFirstname:(NSString *)childFirstname
childMiddlename:(NSString *)childMiddlename
isChildResident:(BOOL)isChildResident;

@end