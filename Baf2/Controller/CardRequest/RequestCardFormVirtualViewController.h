//
// Created by Askar Mustafin on 1/6/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OCCategory.h"
#import "City.h"
#import "RequestCardFormViewController.h"


@interface RequestCardFormVirtualViewController : RequestCardFormViewController

+ (id)createVC;

@end