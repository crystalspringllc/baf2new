//
//  RequestCardResultViewController.m
//  BAF
//
//  Created by Almas Adilbek on 5/27/15.
//
//

#import "RequestCardResultViewController.h"
#import "Version.h"
#import "MainButton.h"

@interface RequestCardResultViewController ()
@end

@implementation RequestCardResultViewController

- (id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

- (void)initVars {}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

#pragma mark - Actions

- (void)gotoHome {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Config UI

- (void)configUI {
    [self setBafBackground];
    self.view.backgroundColor = [UIColor whiteColor];

    [self hideBackButton];
    [self showMenuButton];
    
    NSString *navTitle;
    UIView *conview = nil;
    CGFloat viewWidth = 280;

    if(self.resultStatus == ResultStatusTypeSuccess) {
        conview = [UIHelper successMessageWithIconWithContentWidth:viewWidth message:NSLocalizedString(@"application_successfully_accepted", nil)];
        navTitle = NSLocalizedString(@"application_successfully_accepted", nil);
    } else {
        conview = [UIHelper failureMessageWithIconWithContentWidth:viewWidth message:self.message];
        navTitle = NSLocalizedString(@"application_rejected", nil);
    }
    conview.backgroundColor = [UIColor clearColor];

    // Set navigation title.
    [self setNavigationTitle:navTitle];

    conview.y = [Version iphone4:20 taller:35];
    conview.centerX = self.view.middleX;
    [self.view addSubview:conview];

    // Message
    CGFloat nexty = conview.bottom;
    if(self.resultStatus == ResultStatusTypeSuccess)
    {
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 270, 1)];
        messageLabel.backgroundColor = [UIColor clearColor];
        messageLabel.font = [UIFont ptSansCaption:14];
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        messageLabel.numberOfLines = 0;
        messageLabel.text = [NSString stringWithFormat:NSLocalizedString(@"dear_client_your_application", nil), self.serviceid];
        [messageLabel sizeToFit];
        messageLabel.y = nexty + 5;
        messageLabel.centerX = (CGFloat) (conview.width/2.0);
        [conview addSubview:messageLabel];

        nexty = messageLabel.bottom;
    }

    // Buttons --
    // Goto main button
    MainButton *button1 = [[MainButton alloc] initWithFrame:CGRectMake(0, nexty + 25, conview.width, 40)];
    [button1 addTarget:self action:@selector(gotoHome) forControlEvents:UIControlEventTouchUpInside];
    button1.mainButtonStyle = MainButtonStyleOrange;
    button1.frame = CGRectMake(0, nexty + 25, conview.width, 40);
    button1.title = NSLocalizedString(@"go_to_main_activity", nil);
    button1.centerX = conview.middleX;
    [conview addSubview:button1];

    // IF user logged in, show operations history button.
    conview.height = button1.bottom;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end