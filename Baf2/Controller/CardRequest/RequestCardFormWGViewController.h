//
// Created by Askar Mustafin on 2/23/17.
// Copyright (c) 2017 Crystal Spring. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestCardFormViewController.h"

@interface RequestCardFormWGViewController : RequestCardFormViewController
+ (id)createVC;
@end