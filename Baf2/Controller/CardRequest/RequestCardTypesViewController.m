//
//  RequestCardTypesViewController.m
//  BAF
//
//  Created by Mustafin Askar on 06.01.17
//
//

#import "RequestCardTypesViewController.h"
#import "UIViewController+NavigationController.h"
#import "RequestCardMainViewController.h"
#import "OrderCallRequestViewController.h"
#import "UITableViewController+Extension.h"
#import "OrderCardApi.h"
#import "OCCategory.h"

@interface RequestCardTypesViewController ()
@property (nonatomic, strong) NSArray *categories;
@end

@implementation RequestCardTypesViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self configUI];
    [self loadCategories];
}

#pragma mark - API

- (void)loadCategories {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"loading", nil) animated:YES];
    [OrderCardApi getProducts:^(id response) {
        self.categories = response;
        [self.tableView reloadData];
        [MBProgressHUD hideAstanaHUDAnimated:true];
    } failure:^(NSString *code, NSString *message) {
        //if failure go to order call vc
        if ([code intValue] == kCheckCardGoToOrderCallCode)
        {
            [CATransaction begin];
            [CATransaction setCompletionBlock:^{
                //completes when popback action ends
                OrderCallRequestViewController *vc = [[OrderCallRequestViewController alloc] init];
                vc.requestType = BafRequestTypeRequestCard;
                [self.navigationController pushViewController:vc animated:YES];
            }];
            [self.navigationController popViewControllerAnimated:NO];
            [CATransaction commit];
        } else {
            [self popToRoot:YES];
        }
        [MBProgressHUD hideAstanaHUDAnimated:true];
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.categories.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [NSLocalizedString(@"card_type", nil) uppercaseString];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier;
    
    CellIdentifier = @"CellCards";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.font = [UIFont helveticaNeue:16];
        cell.textLabel.preferredMaxLayoutWidth = cell.textLabel.frame.size.width;
        
        cell.detailTextLabel.numberOfLines = 0;
        cell.detailTextLabel.font = [UIFont helveticaNeue:14];
        cell.detailTextLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.preferredMaxLayoutWidth = cell.detailTextLabel.frame.size.width;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    }

    //RCTypeResponse *type = self.types[indexPath.row];
    OCCategory *category = self.categories[(NSUInteger) indexPath.row];

    cell.textLabel.text = category.name;
    cell.detailTextLabel.text = category.descr;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    RequestCardMainViewController *vc = [[RequestCardMainViewController alloc] init];
    vc.category = self.categories[(NSUInteger) indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Config UI

- (void)configUI {
    [self setBAFTableViewBackground];
    [self setNavigationBackButton];
    [self setNavigationTitle:NSLocalizedString(@"card_release", nil)];
    self.tableView.tableFooterView = [UIView new];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end