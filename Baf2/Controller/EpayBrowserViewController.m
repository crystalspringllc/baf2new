//
//  EpayBrowserViewController.m
//  Myth
//
//  Created by Almas Adilbek on 8/21/12.
//
//

#import "EpayBrowserViewController.h"
#import "User.h"
#import "BackgroundTaskManager.h"
#import "CardsApi.h"
#import "Paycheck.h"
#import "PaymentPaycheckViewController.h"
#import "MainButton.h"
#import "PaymentsApi.h"

#if DEBUG // !!!! Only for testing
@interface NSURLRequest (DummyInterface)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host;
+ (void)setAllowsAnyHTTPSCertificate:(BOOL)allow forHost:(NSString*)host;
@end
#endif

@interface EpayBrowserViewController ()

@end

@implementation EpayBrowserViewController {
    UIActivityIndicatorView *activityIndicator;
    int loadingCount;
    NSString *BACK_LINK;
    NSString *FAILURE_BACK_LINK;
}

@synthesize webview, data, infoView, fio, delegate = _delegate;

- (id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

-(void)initVars {
    loadingCount = 0;
    self.epayBrowserPaymentType = EpayBrowserPaymentTypeDefault;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
    
#if DEBUG
    [NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"3dsecure.kkb.kz"];
    [NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"test.paymentgate.ru"];
#endif
    
    BOOL isAlphaAcquiring = [data[@"acquiring"] isEqualToString:kProcessingBankAlpha];
    
    NSString *form;
    NSString *html = @"<html><head><title>KKB Epay</title></head><body style='background-color:#e5e4e1;'>";
    NSString *BASE_URL;
    
    startLoadingDate = [[NSDate alloc] init];
    
    if(!isAlphaAcquiring)
    {
        NSString *SINGNED_EPAY_PARAMS = data[@"base64Content"];
        NSString *PAYMENT_INFO = data[@"appendix"];
        NSString *EMAIL = [User sharedInstance].email;//data[@"EMAIL"];
        NSString *EPAY_URL = data[@"url"];
        NSString *LANGUAGE = data[@"language"];
        NSString *FAILURE_POST_LINK = data[@"failPostLink"];
        NSString *uuid = data[@"uuid"];
        BACK_LINK = [NSString stringWithFormat:@"https://www.myth.kz/EPayInteraction/paysuccess?UUID=%@", uuid];
        NSString *TEMPLATE = data[@"template"];
        NSString *POST_LINK = data[@"postlink"];
        FAILURE_BACK_LINK = data[@"failPostLink"];
        BASE_URL = EPAY_URL;
        
        form = [NSString stringWithFormat:@"<form id='epay' name='SendOrder' method='post' action='%@'>", EPAY_URL];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='Signed_Order_B64' value='%@'>", SINGNED_EPAY_PARAMS]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='email' size=50 maxlength=50  value='%@'>", EMAIL]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='Language' value='%@'>", LANGUAGE]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='BackLink' value='%@'>", BACK_LINK]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='PostLink' value='%@'>", POST_LINK]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='FailureBackLink' value='%@'>", FAILURE_BACK_LINK]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='FailurePostLink' value='%@'>", FAILURE_POST_LINK]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='ShowCurr' value='%@'>", @""]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='appendix' size=50 maxlength=50 value='%@'/>", PAYMENT_INFO]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='template' value='%@'>", TEMPLATE]];
    }
    else
    {
        BASE_URL = data[@"ALPHA_URL"];
        BACK_LINK = data[@"BACK_LINK"];
        FAILURE_BACK_LINK = data[@"FAILURE_BACK_LINK"];
        form = [NSString stringWithFormat:@"<form id='epay' name='SendOrder' method='get' action='%@'>", BASE_URL];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='BackLink' value='%@'>", BACK_LINK]];
        form = [form stringByAppendingString:[NSString stringWithFormat:@"<input type='hidden' name='FailureBackLink' value='%@'>", FAILURE_BACK_LINK]];
    }
    
    form = [form stringByAppendingString:[NSString stringWithFormat:@"</form>"]];
    html = [html stringByAppendingString:form];
    html = [html stringByAppendingString:@"<script type='text/javascript'> window.onload(document.getElementById('epay').submit()); </script>"];
    html = [html stringByAppendingString:@"</body></html>"];
    
    [webview loadHTMLString:html baseURL:[NSURL URLWithString:BASE_URL]];
    
    // Background tasking
    __weak EpayBrowserViewController *weakSelf = self;
    [[BackgroundTaskManager shared] startTaskWithExpiration:^{
        if(loadingCount > 3) {
            // After pay button pressed
            [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"cant_make_payment", nil) message:[NSString stringWithFormat:@"%@\n%@", NSLocalizedString(@"try_again_or_contact_helpdesk", nil),kSupportEmail]];
            [weakSelf close:YES];
        }
    }];
    
    if(_epayBrowserPaymentType == EpayBrowserPaymentTypeDefault) {
        [GAN sendView:[NSString stringWithFormat:@"%@ %@", !isAlphaAcquiring?@"Epay":@"Alpha", NSLocalizedString(@"browser", nil)]];
    } if (_epayBrowserPaymentType == EpayBrowserPaymentTypeRequestCard) {
        [GAN sendView:@"Epay browser Request Card"];
    }
}

- (void)goBack
{
    if([activityIndicator isAnimating]) {
        [UIAlertView showWithTitle:NSLocalizedString(@"close_payment", nil) message:NSLocalizedString(@"are_you_sure_want_to_close_payment", nil) cancelButtonTitle:NSLocalizedString(@"cancel", nil) otherButtonTitles:@[NSLocalizedString(@"close_info", nil)] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (alertView.cancelButtonIndex != buttonIndex) {
                [self close:YES];
            }
        }];
    } else {
        [self close:NO];
    }
}

-(void)close:(BOOL)terminated {
    if(!terminated) {
        NSString *html = [webview stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
        if ([self.delegate respondsToSelector:@selector(epayBrowserClosedWithHtmlContent:)]) {
            [self.delegate epayBrowserClosedWithHtmlContent:html];
        }
    }
    //    [self dismissViewControllerAnimated:YES completion:^{
    //        //
    //    }];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Actions

-(void)gotoMyPayments {
    if ([self.delegate respondsToSelector:@selector(epayBrowserGotoMyPaymentsTapped)]) {
        [self.delegate epayBrowserGotoMyPaymentsTapped];
        [self close:YES];
    }
}

#pragma mark -
#pragma mark UIWebView Delegates

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    ++loadingCount;
    if(webViewInitLoaded) {
        
    }
    [activityIndicator startAnimating];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    if(activityIndicator) {
        [activityIndicator stopAnimating];
    }
    if(!webViewInitLoaded) {
        webViewInitLoaded = YES;
        CGFloat loadingTime = (CGFloat) [[NSDate date] timeIntervalSinceDate:startLoadingDate];
        if(loadingTime < 3) {
            [self performSelector:@selector(showWebView) withObject:nil afterDelay:(3 - loadingTime)];
        } else {
            [self showWebView];
        }
    }
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    if(activityIndicator) [activityIndicator stopAnimating];
    [UIHelper showAlertTryAgain];
    //    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *urlString = [[request URL] absoluteString];
    
    if([urlString rangeOfString:BACK_LINK].location != NSNotFound)
    {
        // Success
        [webview removeFromSuperview];

        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"receipt_loading", nil) animated:YES];
        [PaymentsApi getPaymentInfoMakePaymentUUID:self.uuid success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];

            Paycheck *paycheck = [Paycheck instanceFromDictionary:response];

            if (response) {
                PaymentPaycheckViewController *v = [[PaymentPaycheckViewController alloc] init];
                v.paycheck = paycheck;
                v.epayBrowserPaymentType = _epayBrowserPaymentType;
                [self.navigationController pushViewController:v animated:YES];
            }
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            [self showUnexpectedMessage];
        }];

        return NO;
    }
    else if([urlString rangeOfString:FAILURE_BACK_LINK].location != NSNotFound || [urlString isEqual:data[@"FAILURE_POST_LINK"]])
    {
        [webview removeFromSuperview];
        
        CGFloat viewWidth = 280;
        UIView *conview = [UIHelper failureMessageWithIconWithContentWidth:viewWidth message:NSLocalizedString(@"paymet_with_bank", nil)];
        
        conview.y = 50;
        conview.x = (CGFloat) (([ASize screenWidth] - conview.width) * 0.5);
        
        MainButton *button1 = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, conview.width, 40)];
        [button1 addTarget:self action:@selector(gotoMyPayments) forControlEvents:UIControlEventTouchUpInside];
        button1.mainButtonStyle = MainButtonStyleOrange;
        button1.frame = CGRectMake(0, 0, conview.width, 40);
        button1.x = (CGFloat) ((conview.width - button1.width) * 0.5);
        button1.y = conview.bottom + 20;
        [button1 setTitle:NSLocalizedString(@"close_info", nil)];
        [conview addSubview:button1];
        
        conview.height = button1.bottom;
        [self.view addSubview:conview];
        
        return NO;
    }
    else if([urlString rangeOfString:@"epay_card_register_back"].location != NSNotFound)
    {
        NSLog(@"epay_card_register_back");
        [self close:YES];
        return NO;
    }
    
    return YES;
}

#pragma mark -

-(void)showWebView {
    if(infoView) {
        [infoView removeFromSuperview];
        infoView = nil;
    }
    webview.alpha = 0;
    webview.hidden = NO;
    [UIView animateWithDuration:0.25 animations:^{
        webview.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)showUnexpectedMessage
{
    CGFloat viewWidth = 280;
    UIView *conview = [UIHelper failureMessageWithIconWithContentWidth:viewWidth message:NSLocalizedString(@"cant_load_payment_receipt", nil)];
    conview.y = 50;
    conview.x = (CGFloat) (([ASize screenWidth] - conview.width) * 0.5);
    [self.view addSubview:conview];
}

#pragma mark -

-(void)configUI
{
    [self setNavigationTitle:NSLocalizedString(@"doing_payment", nil)];
    [self setNavigationBackButton];
    
    // ----
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.hidesWhenStopped = YES;
    UIView *loaderBarItemView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 28, 20)];
    [loaderBarItemView addSubview:activityIndicator];
    UIBarButtonItem *loaderBarItem = [[UIBarButtonItem alloc] initWithCustomView:loaderBarItemView];
    [self navigationItem].rightBarButtonItem = loaderBarItem;
    
    self.fio.text = [[User sharedInstance] fio];
    
    // Message
    CGRect vf = infoView.frame;
    UILabel *message = [[UILabel alloc] initWithFrame:CGRectMake(0, 80, vf.size.width, 1)];
    message.backgroundColor = [UIColor clearColor];
    message.font = [UIFont systemFontOfSize:14];
    message.textColor = [UIColor darkGrayColor];
    message.textAlignment = NSTextAlignmentCenter;
    message.numberOfLines = 0;
    message.lineBreakMode = NSLineBreakByWordWrapping;
    message.text = NSLocalizedString(@"now_you_will_transfers_in_epay", nil);
    [message sizeToFit];
    [infoView addSubview:message];
    
    vf.size.height = message.frame.origin.y + message.frame.size.height;
    vf.origin.x = (CGFloat) (([ASize screenWidth] - vf.size.width) * 0.5);
    vf.origin.y = (CGFloat) ((self.view.frame.size.height - vf.size.height) * 0.5 - 30);
    infoView.frame = vf;
    
    // ----
    
    webview.delegate = self;
    webview.hidden = YES;
    webview.scalesPageToFit = NO;
    webview.scrollView.showsVerticalScrollIndicator = NO;
    webview.scrollView.showsHorizontalScrollIndicator = NO;
}

-(void)dealloc {
    self.webview = nil;
    self.infoView = nil;
    self.fio = nil;
    self.data = nil;
    activityIndicator = nil;
    [BackgroundTaskManager kill];
}

@end
