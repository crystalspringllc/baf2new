//
//  PaymentPaycheckViewController.h
//  BAF
//
//  Created by Almas Adilbek on 5/25/15.
//
//



#import "EpayBrowserViewController.h"

@class Paycheck;

@interface PaymentPaycheckViewController : UIViewController

@property(nonatomic, assign) EpayBrowserPaymentType epayBrowserPaymentType;
@property(nonatomic, strong) Paycheck *paycheck;
@property(nonatomic, strong) UIScrollView *scrollView;

@end