//
//  AccountAdditionalInfoViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 20.06.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "AccountAdditionalInfoViewController.h"
#import <STPopup.h>

@implementation AccountAdditionalInfoViewController

- (instancetype)init {
    self = [super init];
    
    if (self ) {
        self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 200);
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 200);
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.infoDataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary *infoData = self.infoDataList[indexPath.row];
    
    NSAttributedString *titleText = [[NSAttributedString alloc] initWithString:infoData[@"title"] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    
    NSAttributedString *subtitleText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", infoData[@"subtitle"]] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor blackColor]}];
    
    NSMutableAttributedString *text  = [[NSMutableAttributedString alloc] initWithAttributedString:titleText];
    [text appendAttributedString:subtitleText];
    
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.attributedText = text;
    cell.detailTextLabel.text = @"";
    
    // add dummy view to set minimum height to 80
    UIView *dummyView;
    if ([cell.contentView viewWithTag:777]) {
        dummyView = [cell.contentView viewWithTag:777];
    } else {
        dummyView = [UIView newAutoLayoutView];
        [cell.contentView addSubview:dummyView];
    }
    dummyView.tag = 777;
    
    [dummyView.constraints autoRemoveConstraints];
    [dummyView autoSetDimension:ALDimensionHeight toSize:80 relation:NSLayoutRelationGreaterThanOrEqual];
    [dummyView autoPinEdgesToSuperviewEdges];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

#pragma mark - Config UI

- (void)configUI {
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], MIN(self.infoDataList.count * 80, [ASize screenHeight]/2.0));
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 80;
    self.tableView.tableFooterView = [UIView new];
}

#pragma mark - add items

- (void)addItems:(NSArray *)items {
    NSMutableArray *arr = [NSMutableArray arrayWithArray:self.infoDataList];
    for (NSDictionary *item in items) {
        [arr addObject:item];
    }
    self.infoDataList = arr;
    [self.tableView reloadData];
}

@end
