//
//  BankTransferSmsConfirmationViewController.m
//  BAF
//
//  Created by Almas Adilbek on 3/13/15.
//
//

#import "BankTransferSmsConfirmationViewController.h"
#import "InputTextField.h"
#import "MyCardsAndAccountsViewController.h"
#import "BankTransferResultViewController.h"
#import "NewTransfersApi.h"
#import "NSString+Validators.h"
#import "MainButton.h"
#import "BankTransferConfirmViewController.h"

@interface BankTransferSmsConfirmationViewController ()
@end

@implementation BankTransferSmsConfirmationViewController {
    UITableView *list;
    UIView *footerButtonView;
    InputTextField *codeField;
    BOOL viewDidAppear;
}

-(id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

-(void)initVars {}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(!viewDidAppear) {
        viewDidAppear = YES;

        NSString *sendMessageText = [NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"on_number", nil), [User sharedInstance].phoneNumber, NSLocalizedString(@"was_send_sms_code", nil)];
        [UIAlertView showWithTitle:[NSString stringWithFormat:@"%@,", [[User sharedInstance] fio]]
                           message:sendMessageText
                 cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                 otherButtonTitles:@[NSLocalizedString(@"enter_code", nil)] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    if (buttonIndex != alertView.cancelButtonIndex) {
                        [self performBlock:^{
                            [codeField focus];
                        }       afterDelay:0.2];
                    }
                }];
    }
}

#pragma mark -
#pragma mark Actions

- (void)submitTapped
{
    [self hideKeyboard];
    NSString *smsCode = nil;

    if(![codeField.value validate]) {
        [UIHelper showAlertWithMessage:NSLocalizedString(@"enter_code_accept_transfer", nil)];
        return;
    }

    smsCode = [codeField value];
    
    __weak BankTransferSmsConfirmationViewController *weakSelf = self;

    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"check_sms_code", nil) animated:true];
    [NewTransfersApi checkSMS:smsCode servicelogId:self.servicelogId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"transferr", nil) animated:true];
        [NewTransfersApi runTransfer:weakSelf.servicelogId success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
            
            // Send notification that should update accounts
            [[NSNotificationCenter defaultCenter] postNotificationName:kMyCardsAndAccountsViewControllerUpdateAccountsNotification object:nil];
            
            // Goto result view controller
            BankTransferResultViewController *v = [[BankTransferResultViewController alloc] init];
            v.servicelogId = weakSelf.servicelogId;
            v.operationId = response;
            v.fromAccount = weakSelf.fromAccount;
            v.toAccount = weakSelf.toAccount;
            v.amount = weakSelf.amount;
            v.currency = weakSelf.currency;
            v.convertedAmount = weakSelf.convertedAmount;
            v.comissionAmount = weakSelf.commissionAmount;
            v.comissionCurrency = weakSelf.commissionCurrency;
            v.saveAsTemplate = weakSelf.saveAsTemplate;
            [weakSelf.navigationController pushViewController:v animated:YES];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:true];
        }];

            /*BankTransferConfirmViewController *v = [[BankTransferConfirmViewController alloc] init];
            v.isOldVersion = YES;
            v.servicelogId = self.checkTarnsferResponse.transferInfo.serviceId;
            v.safetyLevel = self.checkTarnsferResponse.safetyLevel;
            v.statusText = self.checkTarnsferResponse.transferInfo.statusText;
            v.knpDescription = [self.checkTarnsferResponse.knpObject objectForKey:@"name"];
            v.commissionCurrency = self.checkTarnsferResponse.amountInfo.commission.commissionCurrency;
            v.commission = self.checkTarnsferResponse.amountInfo.commission.commissionAmount;
            v.convertedAmount = [self.checkTarnsferResponse.amountInfo.exchangeResponse.convertedAmount decimalFormatString];
            v.fromAccount = self.checkTarnsferResponse.requestor.account;
            v.toAccount = self.checkTarnsferResponse.destination.account;
            v.currency = self.checkTarnsferResponse.amountInfo.currency;
            v.requsetorCurrency = self.checkTarnsferResponse.requestor.currency;
            v.amount = self.checkTarnsferResponse.requestor.accountAmount;
            v.transferType = nil;
            v.operationDate = self.checkTarnsferResponse.transferInfo.date;
            v.saveAsTemplate = self.checkTarnsferResponse;
            v.destinationAmount = self.checkTarnsferResponse.destination.accountAmount;
            v.destinationCurrency = self.checkTarnsferResponse.destination.currency;
            v.rate = self.checkTarnsferResponse.amountInfo.exchangeResponse.presentation;
            v.operationAmount = [self.checkTarnsferResponse.amountInfo.finalAmount decimalFormatString];
            v.operationCurrency = self.checkTarnsferResponse.amountInfo.currency;*/
           // v.isPayboxType = self.checkTarnsferResponse.isPayBox;
        //} failure:^(NSString *code, NSString *message) {
         //   [MBProgressHUD hideAstanaHUDAnimated:true];
       // }];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

- (void)resendSmsTapped
{
    __weak BankTransferSmsConfirmationViewController *weakSelf = self;
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"sending_sms", nil) animated:true];
    [NewTransfersApi sendSMS:self.servicelogId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        [weakSelf resendSmsCodeViewControllerSmsSent];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

#pragma mark -
#pragma mark ResendSmsCodeViewController

- (void)resendSmsCodeViewControllerSmsSent {
    NSString *sendMessageText = [NSString stringWithFormat:@"%@ %@.", NSLocalizedString(@"repeat_code_sent_on_number", nil),[User sharedInstance].phoneNumber];
    [UIHelper showAlertWithMessage:sendMessageText];
}

#pragma mark -
#pragma mark UITableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 98;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"enter_confirmation_code", nil);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 70;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(!footerButtonView) {
        footerButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [self tableView:tableView heightForFooterInSection:section])];
        // Button
        MainButton *submitButton = [[MainButton alloc] initWithFrame:CGRectMake(0, 0, 170, 40)];
        submitButton.title = NSLocalizedString(@"accepting", nil);
        submitButton.mainButtonStyle = MainButtonStyleOrange;
        submitButton.frame = CGRectMake(0, 0, 170, 40);
        submitButton.centerX = footerButtonView.middleX;
        submitButton.y = footerButtonView.height - submitButton.height - 15;
        [submitButton addTarget:self action:@selector(submitTapped) forControlEvents:UIControlEventTouchUpInside];
        [footerButtonView addSubview:submitButton];
    }
    return footerButtonView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cellCodeField";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

        codeField = [[InputTextField alloc] initWithFrame:CGRectMake(15, 8, [ASize screenWidth] - 2 * 15, 44)];
        [codeField.textField setKeyboardType:UIKeyboardTypeNumberPad];
        [codeField setPlaceholder:NSLocalizedString(@"enter_codee", nil)];
        [codeField setIcon:@"icon-hash"];
        [cell.contentView addSubview:codeField];

        // Resend button
        UIButton *resendSmsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 240, 30)];
        [resendSmsButton addTarget:self action:@selector(resendSmsTapped) forControlEvents:UIControlEventTouchUpInside];
        NSAttributedString *buttonTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"send_code_again", nil)
                                                                          attributes:@{
                                                                                  NSForegroundColorAttributeName: [UIColor fromRGB:0x146888],
                                                                                  NSFontAttributeName: [UIFont systemFontOfSize:14],
                                                                                  NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                                                                          }];
        [resendSmsButton setAttributedTitle:buttonTitle forState:UIControlStateNormal];
        resendSmsButton.centerX = (CGFloat) ([ASize screenWidth] * 0.5);
        resendSmsButton.y = codeField.bottom + 8;
        [cell.contentView addSubview:resendSmsButton];
    }

    return cell;
}

#pragma mark -

- (void)configUI {
    [self setBafBackground];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNavigationTitle:NSLocalizedString(@"sms_aceptance", nil)];
    [self setNavigationBackButton];

    list = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [ASize screenWidth], [ASize screenHeightWithoutStatusBarAndNavigationBar]) style:UITableViewStyleGrouped];
    list.backgroundColor = [UIColor clearColor];
    list.backgroundView = nil;
    list.delegate = self;
    list.dataSource = self;
    [self.view addSubview:list];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc {
    
}

@end
