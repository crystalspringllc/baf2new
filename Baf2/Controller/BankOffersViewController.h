//
//  BankOffersViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 4/21/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankOffersViewController : UIViewController
@property (nonatomic, assign) BOOL isOpenedFromMenu, becomeClient;
+ (instancetype)createVC;
@end