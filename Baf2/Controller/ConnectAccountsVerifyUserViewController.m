//
//  ConnectAccountsVerifyUserViewController.m
//  BAF
//
//  Created by Almas Adilbek on 8/25/14.
//
//

#import "ConnectAccountsVerifyUserViewController.h"
#import "AAForm.h"
#import "AAField.h"
#import "AccountsApi.h"
#import "AAField+Validation.h"
#import "MainButton.h"

@interface ConnectAccountsVerifyUserViewController ()
@end

@implementation ConnectAccountsVerifyUserViewController {
    AAForm *form;
    AAField *codeField;
    MainButton *confirmButton;
    MainButton *retrymButton;
}

-(id)init {
    self = [super init];
    if(self) {
        [self initVars];
    }
    return self;
}

-(void)initVars {}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configUI];
    [self configAction];
}

- (void)configAction
{

}

#pragma mark -
#pragma mark Actions

- (void)sendTapped
{
    [self.view endEditing:YES];

    if(![codeField validateAmount]) return;

    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"checking_code", nil) animated:true];
    [AccountsApi checkConfirmationSmsAbisWithCode:[codeField value] tel:self.mobilePhone success:^(id response) {

        [MBProgressHUD hideAstanaHUDAnimated:true];
        [self.delegate connectAccountsVerifyUserViewControllerSmsVerified];

        NSMutableArray *views = [[self.navigationController viewControllers] mutableCopy];
        [views removeObjectAtIndex:views.count - 2];
        [self.navigationController setViewControllers:views];

        [self.navigationController popViewControllerAnimated:YES];

    }                                        failure:^(NSString *code, NSString *message) {

        [MBProgressHUD hideAstanaHUDAnimated:true];
        if ([code isEqualToString:@"927"]) {
            [self gotoRetryActivation];
        }

    }];
}

- (void)gotoRetryActivation {
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"sending_code", nil) animated:true];
    [AccountsApi sendConfirmationSmsAbisWithPhone:self.mobilePhone success:^(id response) {
        
        [MBProgressHUD hideAstanaHUDAnimated:true];
        [UIHelper showAlertWithMessage:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"repetition_code_acceptance", nil), self.mobilePhone]];
        
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
//    ConnectAccountsRetryActivationViewController *v = [[ConnectAccountsRetryActivationViewController alloc] init];
//    v.delegate = self;
//    v.mobilePhone = self.mobilePhone;
//    [self.navigationController pushViewController:v animated:YES];
}

#pragma mark -
#pragma mark ConnectAccountsRetryActivationViewController

-(void)connectAccountsRetryActivationViewControllerSmsVerified {
    [self.delegate connectAccountsVerifyUserViewControllerSmsVerified];
}

#pragma mark -

-(void)configUI
{
    [self setNavigationTitle:NSLocalizedString(@"my_card_and_accounts", nil)];
    [self setNavigationBackButton];
    self.view.backgroundColor = [UIColor whiteColor];

    form = [[AAForm alloc] initWithScrollView:self.contentScrollView];

    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (CGFloat) ([ASize screenWidth] * 0.9), 1)];
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.font = [UIFont boldSystemFontOfSize:15];
    messageLabel.textColor = [UIColor fromRGB:0x333333];
    messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"acceptance_code_was_sent_to_number", nil), self.mobilePhone];
    [messageLabel sizeToFit];
    [form pushView:messageLabel marginTop:20 centered:YES];

    // EMAIL
    codeField = [[AAField alloc] init];
    codeField.required = YES;
    [codeField setTitle:NSLocalizedString(@"enter_acceptance_code", nil)];
    [codeField setPlaceholder:NSLocalizedString(@"acceptance_code", nil)];
    [codeField setIcon:[UIImage imageNamed:@"icon-hash.png"]];
    codeField.keyboardType = UIKeyboardTypeNumberPad;
    [form pushView:codeField marginTop:25 centered:YES];

    // Register button
    confirmButton = [[MainButton alloc] initWithFrame:CGRectMake(0, codeField.bottom + 20, 220, 44)];
    confirmButton.mainButtonStyle = MainButtonStyleOrange;
    confirmButton.frame = CGRectMake(0, codeField.bottom + 20, 220, 44);
    [confirmButton addTarget:self action:@selector(sendTapped) forControlEvents:UIControlEventTouchUpInside];
    [confirmButton setTitle:NSLocalizedString(@"confirm", nil)];
    [form pushView:confirmButton marginTop:25 centered:YES];

    // Retry button
    retrymButton = [[MainButton alloc] initWithFrame:CGRectMake(0, codeField.bottom + 20, 220, 44)];
    retrymButton.mainButtonStyle = MainButtonStyleOrange;
    retrymButton.frame = CGRectMake(0, codeField.bottom + 20, 220, 44);
    [retrymButton addTarget:self action:@selector(gotoRetryActivation) forControlEvents:UIControlEventTouchUpInside];
    [retrymButton setTitle:NSLocalizedString(@"did_not_receive_confirmation_code", nil)];
    [form pushView:retrymButton marginTop:10 centered:YES];

    // Keyboard bsKeyboardControls
    [form initKeyboardControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc {

}

@end