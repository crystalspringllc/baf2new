//
//  PaymentContractTableLIst.h
//  Baf2
//
//  Created by developer on 05.01.18.
//  Copyright © 2018 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentContractTableLIst : UITableViewController

//@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *contracts;

+ (instancetype)createVC;

@end
