//
//  CardMultiCurrencyConverterConfirmationViewController.h
//  BAF
//
//  Created by Almas Adilbek on 6/10/15.
//
//



@class TransferCurrencyRates;
@class Card;

@interface CardMultiCurrencyConverterConfirmationViewController : UIViewController

@property (nonatomic, weak) NSNumber *servicelogId;

@property (nonatomic, weak) TransferCurrencyRates *currencyRates;
@property (nonatomic, copy) NSString *fromCurrency;
@property (nonatomic, copy) NSString *toCurrency;
@property (nonatomic, copy) NSNumber *amount;
@property (nonatomic, copy) NSString *convertedAmountWithCurrency;
@property (nonatomic, weak) Card *cardCurrencyAccount;
@property (nonatomic, copy) NSNumber *comission;
@property (nonatomic, copy) NSString *comissionCurrency;

@property(nonatomic, copy) NSString *reqType;
@property(nonatomic, strong) NSNumber *cardId;

@property (nonatomic) BOOL saveAsTemplate;
;

@end
