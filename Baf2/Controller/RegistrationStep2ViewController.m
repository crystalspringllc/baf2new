//
//  RegistrationStep2ViewController.m
//  Baf2
//
//  Created by Shyngys Kassymov on 26.05.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "RegistrationStep2ViewController.h"
#import "AAFieldActionSheet.h"
#import "MainButton.h"
#import "AuthApi.h"
#import "SecretQuestion.h"
#import "NSString+Validators.h"
#import <JVFloatLabeledTextField.h>
#import "MBProgressHUD+AstanaView.h"
#import "UITableViewController+Extension.h"

@interface RegistrationStep2ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *repeatPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *secretQuestionTextField;

@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
@property (weak, nonatomic) IBOutlet UILabel *repeatPasswordLabel;
@property (weak, nonatomic) IBOutlet UILabel *secretQuestionLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;

@property (weak, nonatomic) IBOutlet MainButton *registerButton;


@property (nonatomic, strong) NSArray *options;
@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic, strong) NSNumber *selectedQuestionID;

@end

@implementation RegistrationStep2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.selectedIndex = -1;
    
    [self configUI];
    [self getSecretQuestions];
}

#pragma mark - API

- (void)getSecretQuestions {
    __weak RegistrationStep2ViewController *weakSelf = self;
    
    [AuthApi getSecretQuestionsWithSuccess:^(NSArray *secretQuestions) {
        [weakSelf onSecretQuestionsSuccessResponse:secretQuestions];
    } failure:^(NSString *code, NSString *message) {
        NSLog(@"Failed to get secret question with code - %ld, message - %@", (long)code, message);
    }];
}

- (void)onSecretQuestionsSuccessResponse:(NSArray *)secretQuestions {
    NSMutableArray *options = [NSMutableArray new];
    for (SecretQuestion *q in secretQuestions) {
        NSNumber *qID = q.secretQuestionId;
        NSString *value = q.value;
        
        [options addObject:@{@"id": qID, @"title": value}];
    }
    
    self.options = options;
}

#pragma mark - Actions

- (void)tappedRegister:(MainButton *)sender {
    [self.view endEditing:true];
    
    if (![self.passwordTextField.text validate]) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"password_not_entered", nil) message:@""];
        return;
    }
    if (![self.repeatPasswordTextField.text validate] && ![self.passwordTextField.text isEqualToString:self.repeatPasswordTextField.text]) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"passwords_do_not_match", nil) message:@""];
        return;
    }
    if (!self.selectedQuestionID) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"not_selected_secret_question", nil) message:@""];
        return;
    }
    if (![self.secretQuestionTextField.text validate]) {
        [UIHelper showAlertWithMessageTitle:NSLocalizedString(@"answer_on_secret_question_not_entered", nil) message:@""];
        return;
    }
    
    [MBProgressHUD showAstanaHUDWithTitle:NSLocalizedString(@"ga_registration", nil) animated:true];

    __weak RegistrationStep2ViewController *weakSelf = self;

    [AuthApi registerUserWithFirstName:self.firstName
                              lastName:self.lastName
                            middleName:self.middleName
                                 phone:self.phone
                                 email:self.email
                                passwd:self.passwordTextField.text
                         passwdConfirm:self.repeatPasswordTextField.text
                            questionId:self.selectedQuestionID
                                answer:self.secretQuestionTextField.text
                          phoneCountry:@(1)
                                locale:@"ru"
                               success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
        
        NSMutableDictionary *params = [NSMutableDictionary new];
        params[@"session"] = response[@"reference"];
        params[@"id"] = response[@"id"];
        
        [weakSelf loginUserWithResponse:params];
        
        if (weakSelf.didRegister) {
            weakSelf.didRegister();
        }

    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:true];
    }];
}

- (void)chooseSecretQuestion {
    [self.view endEditing:true];

    AAFieldActionSheet *actionSheet = [[AAFieldActionSheet alloc] initWithTitle:NSLocalizedString(@"select_secter_question", nil)];
    
    __weak RegistrationStep2ViewController *weakSelf = self;
    [actionSheet onValueChange:^(NSDictionary *option, NSInteger index) {
        weakSelf.selectedIndex = index;
        weakSelf.selectedQuestionID = option[@"id"];
        
        weakSelf.secretQuestionLabel.text = option[@"title"];
        
        [weakSelf.tableView reloadData];
    }];
    actionSheet.visibleItems = 5;
    actionSheet.options = self.options.mutableCopy;
    actionSheet.selectedIndex = self.selectedIndex;
    [actionSheet show];
}

#pragma mark - Methods

- (void)loginUserWithResponse:(id)response {
    [[User sharedInstance] setUserProfileData:response];
}

#pragma mark - UITableViewDataSource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return NSLocalizedString(@"protection", nil);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (self.selectedQuestionID) {
            return 4;
        }
        
        return 3;
    }
    
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 2) {
            UIImageView *disclosureIndicator = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
            disclosureIndicator.contentMode = UIViewContentModeScaleAspectFit;
            disclosureIndicator.image = [UIImage imageNamed:@"next"];
            cell.accessoryView = disclosureIndicator;
        } else {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    } else  {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [self.passwordTextField becomeFirstResponder];
        } else if (indexPath.row == 1) {
            [self.repeatPasswordTextField becomeFirstResponder];
        } else if (indexPath.row == 2) {
            [self chooseSecretQuestion];
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row == 1) {
        }
    }
}

#pragma mark - Config UI

- (void)configUI {
    [self setNavigationBackButton];
    [self setBAFTableViewBackground];

    self.passwordTextField.secureTextEntry = true;
    self.passwordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.repeatPasswordTextField.secureTextEntry = true;
    self.repeatPasswordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.repeatPasswordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.registerButton.mainButtonStyle = MainButtonStyleOrange;
    self.registerButton.title = NSLocalizedString(@"sign_up", nil);
    [self.registerButton addTarget:self action:@selector(tappedRegister:) forControlEvents:UIControlEventTouchUpInside];
    
    self.answerLabel.text = NSLocalizedString(@"enter_answer", nil);
    self.repeatPasswordLabel.text = NSLocalizedString(@"repeat_password", nil);
    self.secretQuestionLabel.text = NSLocalizedString(@"select_secter_question", nil);
    self.passwordLabel.text = NSLocalizedString(@"password", nil);
    
    self.passwordTextField.placeholder = NSLocalizedString(@"atleast_8chars", nil);
    self.repeatPasswordTextField.placeholder = NSLocalizedString(@"atleast_8chars", nil);
}

@end