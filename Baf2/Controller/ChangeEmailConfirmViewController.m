//
//  ChangeEmailConfirmViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 6/22/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "ChangeEmailConfirmViewController.h"
#import "MainButton.h"
#import "ProfileApi.h"
#import "STPopup.h"
#import "FloatingTextField.h"
#import "NotificationCenterHelper.h"

@interface ChangeEmailConfirmViewController ()
@property (weak, nonatomic) IBOutlet UILabel *oldEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *neuEmailLabel;
@property (weak, nonatomic) IBOutlet FloatingTextField *smsCodeTextField;
@property (weak, nonatomic) IBOutlet MainButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIImageView *checkmark;
@end

@implementation ChangeEmailConfirmViewController

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ChangePhoneEmail" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 470);
}

#pragma mark - config actions

- (IBAction)clickConfirmButton:(id)sender {
    if (self.smsCodeTextField.text.length == 0) {
        [Toast showToast:@"Введите смс код"];
        return;
    }

    [MBProgressHUD showAstanaHUDWithTitle:@"Изменение данных" animated:YES];
    [ProfileApi runEmailWith:self.neuEmailStr
                     smsCode:self.smsCodeTextField.text
                serviceLogId:self.serviceLogId
                     success:^(id response) {
                         [MBProgressHUD hideAstanaHUDAnimated:YES];
                         [Toast showToast:@"Ваша заявка на смену email принята в обработку"];
                         [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserUpdatedProfile object:nil];
                         [self.popupController dismissWithCompletion:nil];
                     } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
            }];
}

- (void)checkSmsCode {
    [MBProgressHUD showAstanaHUDWithTitle:@"Проверка смс кода" animated:YES];
    [ProfileApi checkSmsCode:self.smsCodeTextField.text
                       phone:[User sharedInstance].phoneNumber
                serviceLogId:self.serviceLogId
                     success:^(id response) {
                         [MBProgressHUD hideAstanaHUDAnimated:YES];
                         [Toast showToast:@"Код успешно подтвержден"];
                         self.checkmark.hidden = NO;
                         self.confirmButton.mainButtonStyle = MainButtonStyleOrange;
                     } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
                [Toast showToast:message];
                self.checkmark.hidden = YES;
                self.confirmButton.mainButtonStyle = MainButtonStyleDisable;
                self.smsCodeTextField.value = @"";
            }];
}

- (IBAction)clickSendSmsCodeAgain:(id)sender {
    [MBProgressHUD showAstanaHUDWithTitle:@"Повторная отправка смс кода" animated:YES];
    [ProfileApi sendSmsAgain:[User sharedInstance].phoneNumber
                serviceLogId:self.serviceLogId
                     success:^(id response) {
                         [Toast showToast:@"СМС повторно отправлен"];
                        [MBProgressHUD hideAstanaHUDAnimated:YES];
                     } failure:^(NSString *code, NSString *message) {
                [MBProgressHUD hideAstanaHUDAnimated:YES];
                [Toast showToast:message];
            }];
}

#pragma mark - config ui

- (void)configUI {
    self.oldEmailLabel.text = [User sharedInstance].email;
    self.neuEmailLabel.text = self.neuEmailStr;
    self.smsCodeTextField.separatorHidden = NO;
    self.confirmButton.mainButtonStyle = MainButtonStyleDisable;

    self.smsCodeTextField.maxCharacters = 4;

    __weak ChangeEmailConfirmViewController *wSelf = self;
    [self.smsCodeTextField onValueChange:^(NSString *value) {
        if (value.length == 4) {
            [wSelf checkSmsCode];
            [wSelf.smsCodeTextField resignFirstResponder];
        }
    }];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
