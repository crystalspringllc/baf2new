//
//  EpayBrowserViewController.h
//  Myth
//
//  Created by Almas Adilbek on 8/21/12.
//
//

#import <UIKit/UIKit.h>

#define kProcessingCnp @"processing_cnp"
typedef NS_ENUM(NSInteger , EpayBrowserPaymentType) {
    EpayBrowserPaymentTypeDefault = 0,
    EpayBrowserPaymentTypeRequestCard = 1,
    EpayBrowserPaymentTypeP2PTransfer = 2
};

@protocol EpayBrowserViewControllerDelegate;

@interface EpayBrowserViewController : UIViewController<UIWebViewDelegate, UIAlertViewDelegate> {
    NSDictionary *data;
    UIWebView *webview;
    
    BOOL webViewInitLoaded;
    UIView *infoView;
    UILabel *fio;
    
    NSDate *startLoadingDate;
}

@property (nonatomic, weak) id<EpayBrowserViewControllerDelegate> delegate;
@property (nonatomic, strong) IBOutlet UIWebView *webview;
@property (nonatomic, strong) IBOutlet UIView *infoView;
@property (nonatomic, strong) IBOutlet UILabel *fio;
@property (nonatomic, strong) NSDictionary *data;
@property (nonatomic, assign) EpayBrowserPaymentType epayBrowserPaymentType;

@property (nonatomic, strong) NSString *uuid;

@end

@protocol EpayBrowserViewControllerDelegate<NSObject>
@optional
-(void)epayBrowserClosedWithHtmlContent:(NSString *)html;
-(void)epayBrowserGotoMyPaymentsTapped;
@end