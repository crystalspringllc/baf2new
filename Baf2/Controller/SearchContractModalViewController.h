//
// Created by Mustafin Askar on 08/05/15.
//

#import <Foundation/Foundation.h>
#import "SearchResultsView.h"

@class InputTextField;


@interface SearchContractModalViewController : UIViewController <SearchResultsViewDelegate>
@property(nonatomic, strong) InputTextField *searchField;
@end