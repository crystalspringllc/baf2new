//
//  SMSConfirmationPopupViewController.m
//  BAF
//
//  Created by Askar Mustafin on 1/26/16.
//  Copyright © 2016 Банк Астаны. All rights reserved.
//

#import "SMSConfirmationPopupViewController.h"
#import <STPopup/STPopup.h>

@interface SMSConfirmationPopupViewController ()
@property (nonatomic, strong) UIImageView *successFailureImageView;
@end

@implementation SMSConfirmationPopupViewController

- (id)init {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SMSConfirmViewController" bundle:nil];
    self = [storyboard instantiateViewControllerWithIdentifier:@"SMSConfirmationPopupViewController"];
    if (self) {
        [self initVars];
    }
    return self;
}

- (void)initVars {

    self.title = @"";
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth] - 2 * 15, 200);
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self configUI];
    [self configActions];
}

#pragma mark - config actions

- (void)configActions {
    __weak SMSConfirmationPopupViewController *wSelf = self;

    [self.smsTextField onValueChange:^(NSString *value) {
        if (value.length == 4) {
            [wSelf verifySms:value];
        } else if (value.length < 4) {
            [wSelf hideSuccessFailureIcons];
        }
    }];
}

- (void)verifySms:(NSString *)smsCode {
    [self.smsTextField startLoading];
    if (self.verifySmsCode) {
        self.verifySmsCode(self, smsCode);
    }
}

- (IBAction)clickSmsAgainButton:(id)sender {
    [self.smsTextField startLoading];
    if (self.sendSmsCodeAgain) {
        self.sendSmsCodeAgain(self);
    }
}

#pragma mark - config ui

- (void)dismiss {
    [self.popupController dismissWithCompletion:^{
        if (self.didDismissWithSuccessBlock) {
            self.didDismissWithSuccessBlock();
        }
    }];
}

- (void)smsTextFieldStopLoading {
    [self.smsTextField stopLoading];
}

- (void)showSuccessIcon {
    [self setIconIsSuccess:YES];
}

- (void)showFailureIcon {
    [self setIconIsSuccess:NO];
}

- (void)hideSuccessFailureIcons {
    self.successFailureImageView.alpha = 0;
    [self.successFailureImageView removeFromSuperview];
}

- (void)setIconIsSuccess:(BOOL)isSuccess {

    self.successFailureImageView = [[UIImageView alloc] init];
    self.successFailureImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.successFailureImageView.image = [UIImage imageNamed:isSuccess ? @"icon-success-outline": @"icon-failure-outline"];
    self.successFailureImageView.width = 40;
    self.successFailureImageView.height = 40;
    self.successFailureImageView.center = self.view.center;
    [self.view addSubview:self.successFailureImageView];


    self.successFailureImageView.alpha = 0;
    [UIView animateWithDuration:0.2 animations:^{
        self.successFailureImageView.alpha = 1;
    }];

}

- (void)configUI {
    self.title = NSLocalizedString(@"enter_confirmation_code", nil);
    
    self.smsTextField.maxCharacters = 4;
    self.smsTextField.textField.keyboardType = UIKeyboardTypeNumberPad;
    [self.smsTextField setPlaceholder:NSLocalizedString(@"enter_confirmation_code", nil)];
    [self.smsAgainButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    
    NSString *smsAgainButtonTitleString = NSLocalizedString(@"send_code_again", nil);
    
    NSMutableAttributedString *attrSelected = [[NSMutableAttributedString alloc] initWithString:smsAgainButtonTitleString];
    NSMutableAttributedString *attrSelected2 = [[NSMutableAttributedString alloc] initWithString:smsAgainButtonTitleString];

    [attrSelected addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(0, smsAgainButtonTitleString.length)];
    [attrSelected2 addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, smsAgainButtonTitleString.length)];
    
    [self.smsAgainButton setAttributedTitle:attrSelected forState:UIControlStateNormal];
    [self.smsAgainButton setAttributedTitle:attrSelected2 forState:UIControlStateHighlighted];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end