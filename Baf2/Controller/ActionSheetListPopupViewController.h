//
//  ActionSheetListPopupViewController.h
//  Baf2
//
//  Created by Askar Mustafin on 4/28/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kActionSheetListHeaderHeight 36
#define kActionSheetListCellHeight 60

#define kWithoutCardRegister @"without_card_register"

@interface ActionSheetListRowObject : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) id keepObject;
@property (nonatomic, strong) UIColor *tintColor;
@end

@interface ActionSheetListPopupViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, copy) NSInteger (^numberOfSections)(void); // if no, returns 1
@property (nonatomic, copy) NSString* (^titleForSection)(NSInteger section); // if no, returns nil and set 0 for height
@property (nonatomic, copy) NSInteger (^numberOfRowsInSection)(NSInteger section);
@property (nonatomic, copy) ActionSheetListRowObject* (^rowForIndexPath)(NSIndexPath *indexPath);

@property (nonatomic, copy) NSIndexPath *selectedIndexPath;
@property (nonatomic, copy) void (^onSelectObject)(id selectedObject, NSIndexPath *selectedIndexPath);

@property (nonatomic, copy) NSMutableIndexSet *checkmarkDisabledSections;

@property (nonatomic, copy) NSString *emptyDataText;

@end
