//
//  ChangePhoneConfirmViewController.m
//  Baf2
//
//  Created by Askar Mustafin on 6/22/17.
//  Copyright © 2017 Crystal Spring. All rights reserved.
//

#import "ChangePhoneConfirmViewController.h"
#import "STPopup.h"
#import "FloatingTextField.h"
#import "MainButton.h"
#import "ProfileApi.h"
#import "NotificationCenterHelper.h"

@interface ChangePhoneConfirmViewController ()
@property (weak, nonatomic) IBOutlet UILabel *oldPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *neuPhoneLabel;
@property (weak, nonatomic) IBOutlet FloatingTextField *emailCodeTextField;
@property (weak, nonatomic) IBOutlet FloatingTextField *smsCodeTextField;
@property (weak, nonatomic) IBOutlet MainButton *proceedButton;
@property (nonatomic, assign) BOOL isemailConfirmed;
@property (nonatomic, assign) BOOL issmsConfirmed;
@property (weak, nonatomic) IBOutlet UIButton *sensEmailCodeAgainButton;
@property (weak, nonatomic) IBOutlet UIButton *sendSmsCodeAgainButton;
@property (weak, nonatomic) IBOutlet UILabel *emailCodeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *firstCheckmark;
@property (weak, nonatomic) IBOutlet UIImageView *secondCheckmark;
@end

@implementation ChangePhoneConfirmViewController

+ (instancetype)createVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ChangePhoneEmail" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    self.contentSizeInPopup = CGSizeMake([ASize screenWidth], 500);
}

#pragma mark - config actions

/**
 * CONFIRM FROM EMAIL OR OLD PHONE
 * @param sender
 */
- (void)confirmEmailCodeButton {
    if (self.emailCodeTextField.text.length == 0) {
        [Toast showToast:@"Введите код"];
        return;
    }
    NSString *code = self.emailCodeTextField.text;
    [MBProgressHUD showAstanaHUDWithTitle:@"Подтверждение" animated:YES];
    if (self.isEmailInsteadOfPhone) {
        [ProfileApi checkEmailCode:code email:[User sharedInstance].email serviceLogId:self.serviceLogId success:^(id response) {
            [Toast showToast:@"Код успешно подтвержден"];
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            self.isemailConfirmed = YES;
            self.sensEmailCodeAgainButton.hidden = YES;
            if (self.issmsConfirmed && self.emailCodeTextField) {
                self.proceedButton.mainButtonStyle = MainButtonStyleOrange;
            }
            self.firstCheckmark.hidden = NO;
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            [Toast showToast:message];
            self.firstCheckmark.hidden = YES;
            self.emailCodeTextField.value = @"";
        }];
    } else {
        [ProfileApi checkSmsCode:code phone:self.oldPhoneLabel.text serviceLogId:self.serviceLogId success:^(id response) {
            [Toast showToast:@"Код успешно подтвержден"];
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            self.isemailConfirmed = YES;
            self.sensEmailCodeAgainButton.hidden = YES;
            if (self.issmsConfirmed && self.emailCodeTextField) {
                self.proceedButton.mainButtonStyle = MainButtonStyleOrange;
            }
            self.firstCheckmark.hidden = NO;
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            [Toast showToast:message];
            self.firstCheckmark.hidden = YES;
            self.emailCodeTextField.value = @"";
        }];
    }
}

/**
 * SEND CODE TO EMAIL OR OLD PHONE AGAIN
 * @param sender
 */
- (IBAction)clickSendEmailCodeAgain:(id)sender {
    [MBProgressHUD showAstanaHUDWithTitle:@"Повторная отправка кода" animated:YES];
    if (self.isEmailInsteadOfPhone) {
        [ProfileApi sendEmailCodeAgain:[User sharedInstance].email serviceLogId:self.serviceLogId success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            [Toast showToast:@"Код отправлен"];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            [Toast showToast:message];
        }];
    } else {
        [ProfileApi sendSmsAgain:self.oldPhoneLabel.text serviceLogId:self.serviceLogId success:^(id response) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            [Toast showToast:@"Код отправлен"];
        } failure:^(NSString *code, NSString *message) {
            [MBProgressHUD hideAstanaHUDAnimated:YES];
            [Toast showToast:message];
        }];
    }
}

/**
 * CONFIRM SMS CODE
 * @param sender
 */
- (void)сonfirmSmsCodeButton {
    if (self.smsCodeTextField.text.length == 0) {
        [Toast showToast:@"Введите смс код"];
        return;
    }
    NSString *smsCode = self.smsCodeTextField.text;
    NSString *phone = self.neuPhoneStr;
    [MBProgressHUD showAstanaHUDWithTitle:@"Подтверждение" animated:YES];
    [ProfileApi checkSmsCode:smsCode phone:phone serviceLogId:self.serviceLogId success:^(id response) {
        [Toast showToast:@"Смс код успешно подтвержден"];
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        self.issmsConfirmed = YES;
        self.sendSmsCodeAgainButton.hidden = YES;
        if (self.issmsConfirmed && self.emailCodeTextField) {
            self.proceedButton.mainButtonStyle = MainButtonStyleOrange;
        }
        self.secondCheckmark.hidden = NO;
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [Toast showToast:message];
        self.secondCheckmark.hidden = YES;
        self.smsCodeTextField.value = @"";
    }];
}

/**
 * SEND SMS CODE AGAIN
 * @param sender
 */
- (IBAction)clickSendSmsCodeAgain:(id)sender {
    [MBProgressHUD showAstanaHUDWithTitle:@"Повторная отправка кода" animated:YES];
    [ProfileApi sendSmsAgain:self.neuPhoneStr serviceLogId:self.serviceLogId success:^(id response) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [Toast showToast:@"Код отправлен"];
    } failure:^(NSString *code, NSString *message) {
        [MBProgressHUD hideAstanaHUDAnimated:YES];
        [Toast showToast:message];
    }];
}

/**
 * THEN PROCEED
 * @param sender
 */
- (IBAction)clickProceedButton:(id)sender {
    [MBProgressHUD showAstanaHUDWithTitle:@"Смена телефона" animated:YES];
    [ProfileApi runPhoneWith:self.neuPhoneStr
                  smsCodeNew:self.smsCodeTextField.text
                  smsCodeOld:self.isEmailInsteadOfPhone ? @"" : self.emailCodeTextField.text
                   emailCode:self.isEmailInsteadOfPhone ? self.emailCodeTextField.text : @""
                serviceLogId:self.serviceLogId
                     success:^(id response) {
                         [MBProgressHUD hideAstanaHUDAnimated:YES];
                         [Toast showToast:@"Телефон успешно сменен"];
                         [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserUpdatedProfile object:nil];
                         [self.popupController dismiss];
                     } failure:^(NSString *code, NSString *message) {
                        [MBProgressHUD hideAstanaHUDAnimated:YES];
                     }];
}

#pragma mark - config ui

- (void)configUI {
    self.oldPhoneLabel.text = [User sharedInstance].phoneNumber;
    self.proceedButton.mainButtonStyle = MainButtonStyleDisable;
    self.neuPhoneLabel.text = self.neuPhoneStr;

    if (self.isEmailInsteadOfPhone) {
        self.emailCodeLabel.text = @"Код выслан на почту";
        self.emailCodeTextField.placeholder = @"Код подтверждения из email";
    } else {
        self.emailCodeLabel.text = @"Код выслан на ваш старый номер";
        self.emailCodeTextField.placeholder = @"Код подтверждения из старого телефона";
    }

    self.emailCodeTextField.maxCharacters = 4;
    self.smsCodeTextField.maxCharacters = 4;

    self.emailCodeTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.smsCodeTextField.keyboardType = UIKeyboardTypeNumberPad;

    __weak ChangePhoneConfirmViewController *wSelf = self;
    [self.emailCodeTextField onValueChange:^(NSString *value) {
        if (value.length == 4) {
            [wSelf confirmEmailCodeButton];
            [wSelf.emailCodeTextField resignFirstResponder];
        }
    }];
    [self.smsCodeTextField onValueChange:^(NSString *value) {
        if (value.length == 4) {
            [wSelf сonfirmSmsCodeButton];
            [wSelf.smsCodeTextField resignFirstResponder];
        }
    }];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
