//
//  AuthTest.m
//  Baf2
//
//  Created by Askar Mustafin on 5/30/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AuthApi.h"
#import "TestHelper.h"

@interface AuthTest : XCTestCase

@end

@implementation AuthTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - 

- (void)testLogin {
    XCTestExpectation *expectation = [self expectationWithDescription:@"asynchronous request"];

    NSString *method = @"auth/auth_by_login";
    NSString *url = [[TestHelper baseApiURL] stringByAppendingString:method];

    NSString *username = @"7779998877";
    NSString *password = @"11111111";
    NSString *clientInfo = @"{   \"IMEIAndroid\": \"D3D35DC3-DF44-4314-B55D-A7DF305ABB5E\",   \"app_id\": \"\",   \"board\": \"\",   \"brand\": \"Apple\",   \"device\": \"iPhone Simulator\",   \"display\": \"Retina\",   \"display_size\": \"320:568\",   \"ip\": \"error-getting-ip\",   \"model\": \"iPhone\",   \"os\": \"iOS\",   \"product\": \"10.000000\",   \"push_token\": \"\",   \"simoperatorname\": \"\",   \"v_release\": \"3.0.2\" }";
    NSDictionary *params = @{
            @"login" : username,
            @"password" : password,
            @"clientInfo" : clientInfo
    };

    __block AFHTTPSessionManager *sharedManager = [AFHTTPSessionManager manager];
    sharedManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    sharedManager.responseSerializer.acceptableContentTypes = [sharedManager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];

    [sharedManager POST:url
             parameters:params
               progress:nil
                success:^(NSURLSessionDataTask *task, id responseObject)
                {

                    if ([responseObject[@"status"] isEqualToString:@"error"]) {
                        NSString *errorMessage = responseObject[@"message"];
                        const char *textC = [errorMessage UTF8String];
                        NSString *getText = [NSString stringWithCString:textC encoding:NSUTF8StringEncoding];
                        NSLog(@"Error message: %@", getText);

                    } else {
                        NSLog(@"Success response: %@", responseObject[@"data"]);

                    }

                    XCTAssertEqualObjects(responseObject[@"status"], @"success");
                    [expectation fulfill];
                }
                failure:^(NSURLSessionDataTask *task, NSError *error)
                {
                    //failed whe expression != nil
                    XCTAssertNil(error, @"dataTaskWithURL error %@", error);
                    [expectation fulfill];
                }
    ];

    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError *error) {
        NSLog(@"waitForExpectationsWithTimeout");
        [[sharedManager operationQueue] cancelAllOperations];
        sharedManager = nil;
    }];
}

@end
