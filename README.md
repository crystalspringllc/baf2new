iOS Мобильный Банк Астаны для физичестих лиц
============================================

Проект имеет клиент-серверную архитектуру.

![revolunet logo](https://image.ibb.co/nvHovm/baf_clientserver_scheme.png "baf client-server scheme")

Проект написан на языке [Objective-C](https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/ProgrammingWithObjectiveC/Introduction/Introduction.html). Для контроля зависимостей изпользует программу [CocoaPods](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiihPGU1PLWAhVlApoKHbjjDycQFggmMAA&url=https%3A%2F%2Fcocoapods.org%2F&usg=AOvVaw2Z4bn891KKykiSTO3oynD1).
Для контроля версий [Git](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=4&cad=rja&uact=8&ved=0ahUKEwiStPi51PLWAhUDJ5oKHeljCZAQFgg2MAM&url=https%3A%2F%2Fgit-scm.com%2F&usg=AOvVaw1lFNWgbWf8FsbaoU4AOPBr).


Запуск прокта:
--------------

1. Разархивуруйте проект
2. Пройдите в директорию проекта в Terminal и запустите команду 

```shell
pod install
```

для установки всех необходимых зависимостей.

3. Откройте фаил проекта 'Baf2.xcworkspace' через программу XCode 8.3.3

Рекумендуемая версия XCode - 8.3.3



Организация файлов и папок в проекте:
-------------------------------------


![architecture](https://image.ibb.co/kap41R/Baf2_xcodeproj_2017_10_15_18_54_53.png "files and folders")![architecture2](https://image.ibb.co/ddFcMR/Baf2_xcodeproj_2017_10_15_19_00_31.png "files and folders2")

- VIPER - некоторые модули программы были написаны с использованием архитектурного паттерна [VIPER](https://habrahabr.ru/post/273061/)
- ModelView - или [MVVM](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiNqMa12PLWAhWCYpoKHcbPCSUQFggmMAA&url=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2FModel-View-ViewModel&usg=AOvVaw1y8Y_bXlFApmauAuiVknKi)
- App - В этой папке хранятся Модели, Api (описаны все методы запросов на мобилное апи), Категории проекта, Файлы локализации, Утилиты, библиотеки подключенные вручную, и - NewApi (новая версия ApiClient).
- Views - вьюшки
- Supporting files - здесь находится Baf2-Prefix.pch фаил для глобального импорта классов, main.m фаил, Constants.h для констант
- Certificates - папка с ssl сертификатами для ssl pinning (сейчас отключен ввиду некоторых проблем с timeout)
- Controllers - контроллеры MVC
- IBFiles - файлы interface builder (.xib, .storyboard)
- зкуфрейворки Fabric и Crashlitics добавлены вручную


В течении разработки проекта в основном применялся архитектурный паттерн MVC, но также есть некоторые модули для которых использовался VIPER или MVVM.