//
//  WKInterfaceImage+Ext.h
//  BAF
//
//  Created by Almas Adilbek on 6/30/15.
//
//

#import <WatchKit/WatchKit.h>

@interface WKInterfaceImage (Ext)

- (WKInterfaceImage *)setImageWithUrl:(NSString *)urlString;

@end
