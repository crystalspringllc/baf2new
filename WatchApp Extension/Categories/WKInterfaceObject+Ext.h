//
//  WKInterfaceObject+Ext.h
//  BAF
//
//  Created by Almas Adilbek on 7/5/15.
//
//

#import <WatchKit/WatchKit.h>

@interface WKInterfaceObject (Ext)

- (void)show;
- (void)hide;

@end
