//
//  WKInterfaceObject+Ext.m
//  BAF
//
//  Created by Almas Adilbek on 7/5/15.
//
//

#import "WKInterfaceObject+Ext.h"

@implementation WKInterfaceObject (Ext)

- (void)show {
    [self setHidden:NO];
}

- (void)hide {
    [self setHidden:YES];
}


@end
