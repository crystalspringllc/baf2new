//
//  WKInterfaceImage+Ext.m
//  BAF
//
//  Created by Almas Adilbek on 6/30/15.
//
//

#import "WKInterfaceImage+Ext.h"

@implementation WKInterfaceImage (Ext)

- (WKInterfaceImage *)setImageWithUrl:(NSString *)urlString
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *url = [NSURL URLWithString:urlString];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *image = [UIImage imageWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setImage:image];
        });
    });
    return self;
}

@end
