//
//  MMWormhole+Ext.m
//  SajdeKZ
//
//  Created by Almas Adilbek on 6/28/15.
//  Copyright (c) 2015 GoodApp inc. All rights reserved.
//

#import "MMWormhole+Ext.h"
#import "Constants.h"

static MMWormhole *_appInstance = nil;
static MMWormhole *_watchInstance = nil;

@implementation MMWormhole (Ext)

+ (MMWormhole *)appInstance {
    if(!_appInstance) {
        _appInstance = [self wormInstance];
    }
    return _appInstance;
}

+ (MMWormhole *)watchInstance {
    if(!_watchInstance) {
        _watchInstance = [self wormInstance];
    }
    return _watchInstance;
}

+ (MMWormhole *)wormInstance {
    return [[MMWormhole alloc] initWithApplicationGroupIdentifier:kAppGroupContainer optionalDirectory:kWormholeDirectory];
}

+ (void)clean {
    _appInstance = nil;
    _watchInstance = nil;

    [_appInstance stopListeningForMessageWithIdentifier:kWormholeIdentifierFromWatch];
    [_watchInstance stopListeningForMessageWithIdentifier:kWormholeIdentifierFromApp];
}

- (void)passActionToApp:(NSString *)action object:(id)object {
    [self passAction:action object:object identifier:kWormholeIdentifierFromWatch];
}

- (void)passActionToWatch:(NSString *)action object:(id)object {
    [self passAction:action object:object identifier:kWormholeIdentifierFromApp];
}

- (void)passAction:(NSString *)action object:(id)object identifier:(NSString *)identifier {
    NSMutableDictionary *md = [NSMutableDictionary dictionary];
    md[@"action"] = action;
    if(object) md[@"object"] = object;
    [self passMessageObject:md identifier:identifier];
}


@end
