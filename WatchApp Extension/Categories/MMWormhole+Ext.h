//
//  MMWormhole+Ext.h
//  SajdeKZ
//
//  Created by Almas Adilbek on 6/28/15.
//  Copyright (c) 2015 GoodApp inc. All rights reserved.
//

#import <MMWormhole/MMWormhole.h>

#define kWormholeDirectory @"wormhole"
#define kWormholeIdentifierFromWatch @"fromWatch"
#define kWormholeIdentifierFromApp @"fromApp"

static NSString *WORMHOLE_ACTION_USER_AUTHORIZED_WITH_PIN = @"user_authorized_with_pin";
static NSString *WORMHOLE_ACTION_USER_LOGGED_OUT = @"user_logged_out";
static NSString* const WORMHOLE_ACTION_USER_AUTHORIZED_WITH_TOKEN = @"WORMHOLE_ACTION_USER_AUTHORIZED_WITH_TOKEN";

@interface MMWormhole (Ext)

+ (MMWormhole *)appInstance;
+ (MMWormhole *)watchInstance;
+ (MMWormhole *)wormInstance;
+ (void)clean;

- (void)passActionToApp:(NSString *)action object:(id)object;
- (void)passActionToWatch:(NSString *)action object:(id)object;

@end
