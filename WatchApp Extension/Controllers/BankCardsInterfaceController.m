//
//  BankCardsInterfaceController.m
//  BAF
//
//  Created by Almas Adilbek on 7/1/15.
//
//

#import "BankCardsInterfaceController.h"
#import "CardAccount.h"
#import "Card.h"
#import "TitleSubtitleRowController.h"
#import "WKInterfaceImage+Ext.h"
#import "NSString+Ext.h"

@interface BankCardsInterfaceController ()
@property (weak, nonatomic) IBOutlet WKInterfaceTable *table;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *messageGroup;

@end

@implementation BankCardsInterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    CardAccount *account = (CardAccount *)context;

    if(account.cards.count > 0)
    {
        [self.messageGroup setHidden:YES];
        
        [self.table setNumberOfRows:account.cards.count withRowType:@"listRow"];

        NSUInteger index = 0;
        for(Card *card in account.cards) {
            TitleSubtitleRowController *row = [self.table rowControllerAtIndex:index];
            [row.titleLabel setText:card.number];
            [row.subtitleLabel setText:[card balanceAvailableWithCurrency]];
            [row.iconImage setImageWithUrl:card.logo];

            if([card.alias isNotEmpty]) {
                [row.subtitleLabel2 setText:card.alias];
            } else {
                [row.subtitleLabel2 setHidden:YES];
            }
            ++index;
        }
    }
    else {
        [self.table setHidden:YES];
    }
}

- (void)willActivate {
    [super willActivate];
}

- (void)didDeactivate {
    [super didDeactivate];
}

@end



