//
//  PaymentAmountInterfaceController.m
//  BAF
//
//  Created by Almas Adilbek on 7/5/15.
//
//

#import "NSObject+PWObject.h"
#import "PaymentAmountInterfaceController.h"
#import "OptionTitleRowController.h"
#import "AmountHelper.h"
#import "DataCache.h"

@interface PaymentAmountInterfaceController ()
@property (weak, nonatomic) IBOutlet WKInterfaceTable *table;
@end

@implementation PaymentAmountInterfaceController {
    NSMutableArray *amounts;
}

- (void)awakeWithContext:(NSDictionary *)context {
    [super awakeWithContext:context];

    [self setTitle:@"Отмена"];

    NSNumber *selectedAmount = context[@"paymentAmount"];
    NSNumber *feeAmount = context[@"feeAmount"];

    // Predefined amounts.
    amounts = [[AmountHelper paymentAmounts] mutableCopy];

    // If fee amount is passed, put it at the top.
    if(feeAmount.integerValue > 0) {
        [amounts insertObject:feeAmount atIndex:0];
    }

    [self.table setNumberOfRows:amounts.count withRowType:@"amountRow"];

    for (int i = 0; i < amounts.count; i++) {
        OptionTitleRowController *row = [self.table rowControllerAtIndex:i];

        NSNumber *amount = amounts[i];
        [row.titleLabel setText:[AmountHelper formattedAmountWithKZT:amount]];

        if([amount integerValue] == [selectedAmount integerValue]) {
            [row checked];
        } else {
            [row unchecked];
        }
    }

    // Set selected amount
    [[DataCache instance] setPaymentAmount:selectedAmount];
}

- (void)willActivate {
    [super willActivate];
}

- (void)didDeactivate {
    [super didDeactivate];
}

#pragma mark -
#pragma mark Table

- (void)table:(WKInterfaceTable *)table didSelectRowAtIndex:(NSInteger)rowIndex {
    [self dismissController];
    [[DataCache instance] setPaymentAmount:amounts[rowIndex]];
}

@end



