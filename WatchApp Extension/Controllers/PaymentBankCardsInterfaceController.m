//
//  PaymentBankCardsInterfaceController.m
//  BAF
//
//  Created by Almas Adilbek on 7/5/15.
//
//

#import "PaymentBankCardsInterfaceController.h"
#import "Card.h"
#import "TitleSubtitleRowController.h"
#import "WKInterfaceImage+Ext.h"
#import "NSString+Ext.h"
#import "WKInterfaceObject+Ext.h"
#import "DataCache.h"

@interface PaymentBankCardsInterfaceController ()
@property (weak, nonatomic) IBOutlet WKInterfaceTable *table;

@end

@implementation PaymentBankCardsInterfaceController {
    NSArray *bankCards;
}

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    bankCards = [context objectForKey:@"bankCards"];
    NSNumber *selectedBankCardId = [context objectForKey:@"selectedBankCardId"];

    [self.table setNumberOfRows:bankCards.count withRowType:@"listRow"];

    NSUInteger index = 0;
    for(Card *card in bankCards) {
        TitleSubtitleRowController *row = [self.table rowControllerAtIndex:index];
        [row.titleLabel setText:card.number];
        [row.subtitleLabel setText:[card balanceAvailableWithCurrency]];
        [row.iconImage setImageWithUrl:card.logo];

        if([card.alias isNotEmpty]) {
            [row.subtitleLabel2 setText:card.alias];
        } else {
            [row.subtitleLabel2 hide];
        }

        if([card.accountId isEqual:selectedBankCardId]) {
            [row.disclosureImage show];
        } else {
            [row.disclosureImage hide];
        }

        ++index;
    }
}

- (void)willActivate {
    [super willActivate];
}

- (void)didDeactivate {
    [super didDeactivate];
}

#pragma mark -
#pragma mark Table

- (void)table:(WKInterfaceTable *)table didSelectRowAtIndex:(NSInteger)rowIndex
{
    [[DataCache instance] setSelectedBankCard:bankCards[rowIndex]];
    [self dismissController];
}

@end



