//
//  PaymentInterfaceController.m
//  BAF
//
//  Created by Almas Adilbek on 7/2/15.
//
//

#import "NSObject+PWObject.h"
#import "PaymentInterfaceController.h"
#import "Contract.h"
#import "WKInterfaceImage+Ext.h"
#import "DataCache.h"
#import "AccountsApi.h"
#import "Card.h"
#import "CardAccount.h"
#import "WKInterfaceObject+Ext.h"
#import "AmountHelper.h"
#import "ContractsAPI.h"
#import "Constants.h"
#import "OperationAccounts.h"
#import "Accounts.h"
#import "PaymentsApi.h"
#import "ExtCard.h"

@interface PaymentInterfaceController ()
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *container;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *payLoader;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *paySpinner;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *paymentSuccessGroup;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *errorMessageGroup;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *errorMessageLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *noAccountsGroup;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *paymentFieldsGroup;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *contractGroup;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *aliasLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *identifierLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *providerLogo;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *comissionLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *accountsLoader;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *accountsSpinner;
@property (weak, nonatomic) IBOutlet WKInterfaceButton *bankCardsButton;
@property (weak, nonatomic) IBOutlet WKInterfaceButton *payButton;

// Bank Card Row
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *bankCardAlias;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *bankCardNumber;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *bankCardBalance;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *bankCardIcon;

// Amount
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *amountLabel;

@property(nonatomic, strong) id selectedBankCard;
@property(nonatomic, copy) NSNumber *paymentAmount;
@property(nonatomic, copy) NSNumber *feeAmount;

@property(nonatomic, strong) NSDictionary *autoData;
@end

@implementation PaymentInterfaceController {
    Contract *contract;
    OperationAccounts *operationAccounts;
    
    NSMutableArray *bankCards;

    NSNumber *comissionLoadedBankCardId;
    NSNumber *comissionLoadedAmount;
    NSNumber *commissionAmount;

    BOOL feeLoading;
}

- (void)awakeWithContext:(id)context
{
    [super awakeWithContext:context];

    contract = (Contract *)context;
    
    [self.aliasLabel setText:contract.alias];
    [self.identifierLabel setText:contract.contract];
    [self.providerLogo setImageWithUrl:contract.provider.logo];
}

- (void)willActivate
{
    [super willActivate];
    
    // Hide groups.
    [self.noAccountsGroup hide];
    [self.paymentFieldsGroup hide];
    
    self.selectedBankCard = [DataCache instance].selectedBankCard;
    if(_selectedBankCard) {
        [self setBankCardRow:_selectedBankCard];
    }
    
    self.paymentAmount = [DataCache instance].paymentAmount;
    
    if(_paymentAmount) {
        [self.amountLabel setText:[AmountHelper formattedAmountWithKZT:_paymentAmount]];
        [self loadContractFeeAmount];
    } else {
        
        // Do get predefined amount for payment
        if(_autoData == nil && [contract.provider isInfoRequestTypeEquialTo:3]) {
            [self loadContractFeeAmount];
        } else {
            [self setDefaultAmountValue];
        }
    }
    
    // Check if accounts already cached.
    operationAccounts = [[DataCache instance] operationAccounts];
    if(operationAccounts) {
        [self accountsLoaded];
    } else {
        // Load accounts.
        [self showAccountsLoader];
        
        [AccountsApi getOperationAccountsWithSuccess:^(OperationAccounts *oa) {
            operationAccounts = oa;
            
            // Cache
            [[DataCache instance] setOperationAccounts:oa];
            
            [self hideAccountsLoader];
            [self accountsLoaded];
        } failure:^(NSString *code, NSString *message) {
            NSLog(@"error: %@", message);
            [self hideAccountsLoader];
            [self popController];
        }];
    }
}

- (void)didDeactivate {
    [super didDeactivate];
}

#pragma mark -
#pragma mark Data

- (void)accountsLoaded
{
    [self hideAccountsLoader];

    bankCards = [NSMutableArray array];
    for(Card *bankCard in operationAccounts.sourceAccounts.cards) {
        [bankCards addObject:bankCard];
    }

    // If user has payment accounts. (bank cards)
    if(bankCards.count > 0)
    {
        self.selectedBankCard = [bankCards firstObject];
        [[DataCache instance] setSelectedBankCard:_selectedBankCard];

        [self setBankCardRow:_selectedBankCard];

        // Show payment fields.
        [self.paymentFieldsGroup show];

        if(!feeLoading) {
            [self loadContractFeeAmount];
        }
    }
    else
    {
        [self.contractGroup hide];
        [self.noAccountsGroup show];
        
        // Activate handoff, to continue request card on iphone.
        [self activateHandoff];
    }
}

#pragma mark -
#pragma mark Comission


- (void)setComission:(id)amount {
    commissionAmount = amount;
    [self.comissionLabel setText:[NSString stringWithFormat:@"Комиссия: %@ тг.", amount]];
}

#pragma mark -
#pragma mark Amount

- (void)loadContractFeeAmount
{
    // Hide pay button on fee loading.
    [self.payButton hide];

    [self.amountLabel setText:@"Загрузка"];
    feeLoading = YES;
    
    NSString *providerCode = contract.provider.providerCode;
    
    NSString *cardType;
    if ([_selectedBankCard isMemberOfClass:[Card class]]) {
        cardType = ((Card *)_selectedBankCard).cardType;
    } else {
        cardType = ((ExtCard *)_selectedBankCard).cardType;
    }
    
    NSString *bank;
    if ([_selectedBankCard isMemberOfClass:[Card class]]) {
        bank = @"";
    } else {
        bank = ((ExtCard *)_selectedBankCard).bank;
    }
    
    NSString *acquiring;
    if ([_selectedBankCard isMemberOfClass:[Card class]]) {
        acquiring = kProcessingBankBAF;
    } else {
        acquiring = ((ExtCard *)_selectedBankCard).processing;
    }
    
    NSNumber *identifier = contract.identifier;
    
    [PaymentsApi checkAndCalcCommissionAccountId:((Card *)_selectedBankCard).cardId
                                     accountType:((Card *)_selectedBankCard).type
                                    providerCode:providerCode
                                            bank:bank
                                            card:cardType
                                   paymentAmount:_paymentAmount
                                        contract:identifier
                                       acquiring:acquiring
                                         success:^(id response) {
                                             NSNumber *commissionNumber = (NSNumber *)response[@"commission"];
                                             CGFloat commissionValue = [commissionNumber floatValue];
                                             if (commissionValue < 0) commissionValue = 0;
                                             
                                             feeLoading = NO;
                                             self.autoData = response;
                                             
                                             // Show pay button.
                                             [self.payButton show];
                                             
                                             [self setAmountValue:_paymentAmount];
                                             
                                             [self setComission:@(commissionValue)];
                                             
                                         } failure:^(NSString *code, NSString *message) {
                                             [self setComission:@"_"];
                                         }];
}

- (void)setAmountValue:(NSNumber *)amount {
    [self.amountLabel setText:[AmountHelper formattedAmountWithKZT:amount]];
}

- (void)setDefaultAmountValue {
    self.paymentAmount = [AmountHelper paymentAmounts][1];
    [self setAmountValue:_paymentAmount];
}

#pragma mark -
#pragma mark Actions

- (IBAction)tapPay
{
    if(_selectedBankCard)
    {
        [self.container hide];
        [self showPayLoader];

        // Check bank card balance.
        NSLog(@"%@", _paymentAmount);
        if(_paymentAmount.floatValue > ((Account *)_selectedBankCard).balance.floatValue) {
            [self performBlock:^{
                [self hidePayLoader];
                [self showErrorMessage:@"Не достаточно средств для оплаты"];
            } afterDelay:1];
            return;
        }
        
        // Additional Params
        NSString *additionalParams = @"";
        if(_autoData) {
            additionalParams = [NSString stringWithFormat:@"{\"GUID\":\"%@\"}", _autoData[@"GUID"]];
        }
        
        NSString *cardType;
        if ([_selectedBankCard isMemberOfClass:[Card class]]) {
            cardType = ((Card *)_selectedBankCard).cardType;
        } else {
            cardType = ((ExtCard *)_selectedBankCard).cardType;
        }
        
        NSString *bank;
        if ([_selectedBankCard isMemberOfClass:[Card class]]) {
            bank = @"";
        } else {
            bank = ((ExtCard *)_selectedBankCard).bank;
        }
        
        NSString *acquiring;
        if ([_selectedBankCard isMemberOfClass:[Card class]]) {
            acquiring = kProcessingBankBAF;
        } else {
            acquiring = ((ExtCard *)_selectedBankCard).processing;
        }
        
        Account *selectedAccountCard = (Account *)_selectedBankCard;
        
        CGFloat totalAmount = [_paymentAmount floatValue] + [commissionAmount floatValue];
        
        NSNumber *providerId = contract.provider.paymentProviderId;
        NSNumber *identifier = contract.identifier;
        
        [PaymentsApi paymentRegisterProviderId:providerId
                                    contractId:identifier
                                        amount:_paymentAmount
                                    commission:commissionAmount
                          amountWithCommission:@(totalAmount)
                                   bonusAmount:@(0)
                                     acquiring:acquiring
                                        cardId:selectedAccountCard.accountId
                              additionalParams:@""
                                       success:^(id response) {
                                           [self hidePayLoader];
                                           
                                           NSNumber *serviceLogId;
                                           if (response[@"serviceLogId"]) {
                                               serviceLogId = response[@"serviceLogId"];
                                           }
                                           
                                           NSString *uuid;
                                           if (response[@"uuid"]) {
                                               uuid = response[@"uuid"];
                                           }
                                           
                                           if ([response[@"isSmsAuthNeeded"] boolValue]) {

                                               [self pushControllerWithName:@"PaymentBankSmsConfirmationInterfaceController" context:@{@"serviceLogId" : serviceLogId}];
                                               
                                               
                                           } else {
                                               [self.paymentSuccessGroup show];
                                           }
                                       } failure:^(NSString *code, NSString *message) {
                                           [self hidePayLoader];
                                           [self showErrorMessage:message];
                                       }];
    }
}

- (IBAction)tapBankCard {
    if(bankCards) {
        [self presentControllerWithName:@"PaymentBankCardsInterfaceController" context:@{@"bankCards":bankCards, @"selectedBankCardId": ((Account *)_selectedBankCard).accountId}];
    }
}

- (IBAction)tapAmount {
    if(!feeLoading) {
        [self presentControllerWithName:@"PaymentAmountInterfaceController" context:@{@"paymentAmount":_paymentAmount, @"feeAmount":_feeAmount ?: @(0)}];
    }
}

- (IBAction)tapGotoMain {
    [self popController];
}

#pragma mark -
#pragma mark Helper

- (void)setBankCardRow:(Card *)card
{
    [self.bankCardAlias setText:card.alias];
    [self.bankCardNumber setText:card.number];
    [self.bankCardBalance setText:[card balanceAvailableWithCurrency]];
    [self.bankCardIcon setImageWithUrl:card.logo];
}

- (void)activateHandoff {
    [self updateUserActivity:ActivityOpenRequestCard userInfo:@{@"activity":@"requestCard"} webpageURL:nil];
}

#pragma mark -
#pragma mark Accounts loader

- (void)showAccountsLoader
{
    [self.accountsLoader show];
    [self.accountsSpinner setImageNamed:@"spinner"];
    [self.accountsSpinner startAnimatingWithImagesInRange:NSMakeRange(1, 42) duration:0.9 repeatCount:0];
}

- (void)hideAccountsLoader
{
    [self.accountsSpinner stopAnimating];
    [self.accountsLoader hide];
}

#pragma mark -
#pragma mark Pay loader

- (void)showPayLoader
{
    [self.payLoader show];
    [self.paySpinner setImageNamed:@"spinner"];
    [self.paySpinner startAnimatingWithImagesInRange:NSMakeRange(1, 42) duration:0.9 repeatCount:0];
}

- (void)hidePayLoader
{
    [self.paySpinner stopAnimating];
    [self.payLoader hide];
}

#pragma mark -
#pragma mark Error message

- (void)showErrorMessage:(NSString *)message
{
    [self.errorMessageGroup show];
    [self.errorMessageLabel setText:message];

    [self performBlock:^{
        [self hideErrorMessage];
        [self.container show];
    } afterDelay:2.5];
}

- (void)hideErrorMessage {
    [self.errorMessageGroup hide];
}

@end