//
//  PaymentBankSmsConfirmationInterfaceController.m
//  Baf2
//
//  Created by Askar Mustafin on 9/26/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import "PaymentBankSmsConfirmationInterfaceController.h"
#import "PaymentsApi.h"
#import "NewTransfersApi.h"
#import "NSObject+PWObject.h"

@interface PaymentBankSmsConfirmationInterfaceController ()
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceGroup *startInfoGroup;
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceLabel *startInfoLabel;
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceGroup *keyboardGroup;
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceGroup *errorMessageGroup;
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceGroup *loadingGroup; //mb animation works with several images
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceGroup *resendSmsGroup;
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceImage *loader;
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceLabel *loaderLabel;
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceGroup *successMessageGroup;
@end

@implementation PaymentBankSmsConfirmationInterfaceController {
    NSString *smsCode;
}

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    [self setTitle:@"Смс подтверждение"];
    
    // Configure interface objects here.
    self.serviceLogId = [context objectForKey:@"serviceLogId"];
    
    smsCode = @"";
    
    [self.keyboardGroup setHidden:YES];
    [self.errorMessageGroup setHidden:YES];
    [self.loadingGroup setHidden:YES];
    [self.resendSmsGroup setHidden:YES];
    [self.successMessageGroup setHidden:YES];
    
    
    [self performBlock:^{
        [self.startInfoGroup setHidden:YES];
        [self.keyboardGroup setHidden:NO];
    } afterDelay:3];
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

#pragma mark - ib actions
- (IBAction)tapGoMain {
    [self popController];
}

- (IBAction)tapPin0 {
    [self pinEntered:0];
}

- (IBAction)tapPin1 {
    [self pinEntered:1];
}

- (IBAction)tapPin2 {
    [self pinEntered:2];
}

- (IBAction)tapPin3 {
    [self pinEntered:3];
}

- (IBAction)tapPin4 {
    [self pinEntered:4];
}

- (IBAction)tapPin5 {
    [self pinEntered:5];
}

- (IBAction)tapPin6 {
    [self pinEntered:6];
}

- (IBAction)tapPin7 {
    [self pinEntered:7];
}

- (IBAction)tapPin8 {
    [self pinEntered:8];
}

- (IBAction)tapPin9 {
    [self pinEntered:9];
}

- (IBAction)deletePin {
    if(smsCode.length > 0) {
        smsCode = [smsCode substringToIndex:smsCode.length - 1];
        [self pinsChanged];
    }
}

- (IBAction)showResendSmsCodeGroup {
    [self.startInfoGroup setHidden:YES];
    [self.keyboardGroup setHidden:YES];
    [self.errorMessageGroup setHidden:YES];
    [self.loadingGroup setHidden:YES];
    [self.resendSmsGroup setHidden:NO];
}

- (IBAction)resendSmsCode {
    [self.resendSmsGroup setHidden:YES];
    
    [self.loaderLabel setText:@"Отправка смс"];
    [self showLoader];

    [NewTransfersApi sendSMS:self.serviceLogId
                     success:^(id response) {
                         [self.keyboardGroup setHidden:NO];
                         [self hideLoader];
                     } failure:^(NSString *code, NSString *message) {
                         [self hideLoader];
                     }];
}

- (IBAction)hideResendSmsCodeGroup {
    //todo?
    [self.startInfoGroup setHidden:YES];
    [self.keyboardGroup setHidden:NO];
    [self.errorMessageGroup setHidden:YES];
    [self.loadingGroup setHidden:YES];
    [self.resendSmsGroup setHidden:YES];
}

- (void)showLoader {
    [self.loadingGroup setHidden:NO];
    [self.loader setImageNamed:@"spinner"];
    [self.loader startAnimatingWithImagesInRange:NSMakeRange(1, 42) duration:0.9 repeatCount:0];
}

- (void)hideLoader {
    [self.loader stopAnimating];
    [self.loadingGroup setHidden:YES];
}

#pragma mark - config api requests

- (void)checkSms {
    //start loading
    [self.loaderLabel setText:@"Проверка смс"];
    [self showLoader];
    [self.keyboardGroup setHidden:YES];
    
    [PaymentsApi checkConfirmSMSPaymentOrderId:self.serviceLogId code:smsCode success:^(id response) {
        [self hideLoader];
        [self showSuccessMessage];
    } failure:^(NSString *code, NSString *message) {
        [self hideLoader];
        [self showWarning];
        //show failure
    }];
}

#pragma mark - config actions

- (void)pinEntered:(int)number
{
    if(smsCode.length < 4) {
        smsCode = [NSString stringWithFormat:@"%@%@", smsCode, @(number)];
        [self pinsChanged];
        
        if(smsCode.length == 4) {
            [self checkSms];
        }
    }
}

- (void)pinsChanged {
    NSString *titleString = @"";
    for (int i = 0; i<smsCode.length; ++i) {
        titleString = [NSString stringWithFormat:@"%@* ", smsCode];
    }
    for (int i = 0; i<(4 - smsCode.length); ++i) {
        titleString = [NSString stringWithFormat:@"%@.  ", smsCode];
    }
    [self setTitle:titleString];
}

- (void)initializeKeyboard {
    [self.keyboardGroup setHidden:NO];
    [self setTitle:@"Смс подтверждение"];
    smsCode = @"";
}

/**
 * show and hide warning message
 */

- (void)showWarning {
    [self.errorMessageGroup setHidden:NO];
    [self.keyboardGroup setHidden:YES];
    
    [self performBlock:^{
        // Hide error message, show keyboard.
        [self hideWarning];
        [self initializeKeyboard];
    } afterDelay:2];
}

- (void)hideWarning {
    [self.errorMessageGroup setHidden:YES];
}

/**
 * show success message and then pop to root
*/

- (void)showSuccessMessage {
    [self.keyboardGroup setHidden:YES];
    [self.successMessageGroup setHidden:NO];
}

#pragma mark -

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



