//
//  StartInterfaceController.m
//  BAF
//
//  Created by Almas Adilbek on 6/26/15.
//
//

#import "StartInterfaceController.h"
#import "AppUtils.h"
#import "PinHelper.h"
#import "WKInterfaceImage+Ext.h"

@interface StartInterfaceController ()


@end

@implementation StartInterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
}

- (void)willActivate {
    [super willActivate];
}

- (void)didDeactivate {
    [super didDeactivate];
}

@end



