//
//  PaymentBankSmsConfirmationInterfaceController.h
//  Baf2
//
//  Created by Askar Mustafin on 9/26/16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface PaymentBankSmsConfirmationInterfaceController : WKInterfaceController

@property (nonatomic, strong) NSNumber *serviceLogId;

@end
