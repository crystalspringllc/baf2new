//
//  LoginInterfaceController.m
//  Банк Астаны WatchKit Extension
//
//  Created by Almas Adilbek on 6/22/15.
//
//

#import "LoginInterfaceController.h"
#import "PinHelper.h"
#import "PinApi.h"
#import "User.h"
#import "NSString+Ext.h"
#import "DataCache.h"
#import "NSObject+PWObject.h"
#import "Constants.h"
#import "NotificationCenterHelper.h"
#import <WatchConnectivity/WatchConnectivity.h>
#import "AuthApi.h"

@interface LoginInterfaceController () <WCSessionDelegate>
@property (weak, nonatomic) IBOutlet WKInterfaceImage *loader;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *keyboardGroup;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *loaderGroup;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *iPhoneSyncLoader;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *iPhoneSyncGroup;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *errorMessageGroup;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *errorMessageLabel;

@property(nonatomic, strong) MMWormhole *wormhole;
@end


@implementation LoginInterfaceController {
    NSString *pincode;
}

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    pincode = @"";
    [self.errorMessageGroup setHidden:YES];
    [self.loaderGroup setHidden:YES];
    [self.iPhoneSyncGroup setHidden:YES];
}

- (void)willActivate {
    [super willActivate];

    // IF user tapped back (logout) then clear user.
    [User logout];

    // Clear cache data.
    [DataCache clear];

    // Watch connectivity
    [self configWatchConnectivity];
    
    // Observers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceivePinTokenMessage:) name:kNotificationSendPinToken object:nil];
}

- (void)didDeactivate {
    [super didDeactivate];

    self.wormhole = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)login {
    [self loginWithPin:pincode];
}

- (void)loginWithPin:(NSString *)pin
{
    if(pin.length == 4) {
        [self setTitle:@""];
        [self.keyboardGroup setHidden:YES];
        [self showLoader];

        NSString *pinToken = [DataCache instance].pinToken;
        [self openClientSessionByPin:pin pinToken:pinToken];
    }
}

- (void)openClientSessionByPin:(NSString *)pin pinToken:(NSString *)pinToken
{
    [PinApi openClientSessionByPin:pin pinToken:pinToken success:^(id response) {
    
        [User sharedInstance].sessionID = response[@"reference"];
        
        // Initialize user.
        [AuthApi getClientBySession:[User sharedInstance].sessionID
                            success:^(id response2) {
                                [self hideLoader];
                                
                                [[User sharedInstance] setUserProfileData:response2];
                                // Goto main controller.
                                [self pushControllerWithName:@"MainInterfaceController" context:nil];
                                
                                // Init keyboard, so that it is ready if user logouts..
                                [self initializeKeyboard];
                               
                            } failure:^(NSString *code, NSString *message) {
                               
                            }];

    } failure:^(NSString *code, NSString *message) {
        [self hideLoader];

        // If pin entered N times incorrectly
//        if(code == 930) {
//            // Delete pin token from local
//            [PinHelper deletePinToken];
//        }

        if(message && [message isNotEmpty]) {
            [self showWarning:message autoHide:YES];
        }
    }];
}

- (IBAction)tapPin0 {
    [self pinEntered:0];
}

- (IBAction)tapPin1 {
    [self pinEntered:1];
}

- (IBAction)tapPin2 {
    [self pinEntered:2];
}

- (IBAction)tapPin3 {
    [self pinEntered:3];
}

- (IBAction)tapPin4 {
    [self pinEntered:4];
}

- (IBAction)tapPin5 {
    [self pinEntered:5];
}

- (IBAction)tapPin6 {
    [self pinEntered:6];
}

- (IBAction)tapPin7 {
    [self pinEntered:7];
}

- (IBAction)tapPin8 {
    [self pinEntered:8];
}

- (IBAction)tapPin9 {
    [self pinEntered:9];
}

- (IBAction)deletePin {
    if(pincode.length > 0) {
        pincode = [pincode substringToIndex:pincode.length - 1];
        [self pinsChanged];
    }
}

- (void)pinEntered:(int)number
{
    if(pincode.length < 4) {
        pincode = [NSString stringWithFormat:@"%@%@", pincode, @(number)];
        [self pinsChanged];

        if(pincode.length == 4) {
            [self login];
        }
    }
}

- (void)pinsChanged {
    NSString *titleString = @"";
    for (int i = 0; i<pincode.length; ++i) {
        titleString = [NSString stringWithFormat:@"%@* ", titleString];
    }
    for (int i = 0; i<(4 - pincode.length); ++i) {
        titleString = [NSString stringWithFormat:@"%@.  ", titleString];
    }
    [self setTitle:titleString];
}

#pragma mark -
#pragma mark Keyboard

- (void)initializeKeyboard {
    [self.keyboardGroup setHidden:NO];
    [self setTitle:@"Вход"];
    pincode = @"";
}

#pragma mark -
#pragma mark Loader

- (void)showLoader {
    [self.loaderGroup setHidden:NO];
    [self.loader setImageNamed:@"spinner"];
    [self.loader startAnimatingWithImagesInRange:NSMakeRange(1, 42) duration:0.9 repeatCount:0];
}

- (void)hideLoader {
    [self.loader stopAnimating];
    [self.loaderGroup setHidden:YES];
}

- (void)showiPhoneSyncLoader {
    [self.iPhoneSyncGroup setHidden:NO];
    [self.iPhoneSyncLoader setImageNamed:@"spinner"];
    [self.iPhoneSyncLoader startAnimatingWithImagesInRange:NSMakeRange(1, 42) duration:0.9 repeatCount:0];
}

- (void)hideiPhoneSyncLoader {
    [self.iPhoneSyncLoader stopAnimating];
    [self.iPhoneSyncGroup setHidden:YES];
}

#pragma mark -
#pragma mark Error mess age

- (void)showWarning:(NSString *)message autoHide:(BOOL)autoHide {
    [self.errorMessageLabel setText:message];
    [self.errorMessageGroup setHidden:NO];

    if(autoHide) {
        [self performBlock:^{
            // Hide error message, show keyboard.
            [self hideWarning];
            [self initializeKeyboard];
        } afterDelay:2];
    }
}

- (void)hideWarning {
    [self.errorMessageGroup setHidden:YES];
}

#pragma mark -
#pragma mark Wormhole

- (void)configWatchConnectivity {
    if ([WCSession isSupported]) {
        WCSession *session = [WCSession defaultSession];
        session.delegate = self;
        [session activateSession];
        
        [self.keyboardGroup setHidden:true];
        [self showiPhoneSyncLoader];
        
        if (WCSession.defaultSession.reachable == true) {
            NSDictionary *requestPinToken = @{wcMessageType: wcMessageTypeGetPinToken, wcMessageObject: @{}};
            
            [[WCSession defaultSession] sendMessage:requestPinToken
                                       replyHandler:^(NSDictionary *reply) {
                                           NSLog(@"Sent message to iOS app with reply - %@", reply);
                                           
                                           NSDictionary *messageObject = reply[wcMessageObject];
                                           
                                           NSString *token = [messageObject objectForKey:@"pinToken"];
                                           NSString *deviceID = [messageObject objectForKey:@"deviceID"];
                                           
                                           if (token && token.length > 0) {
                                               [DataCache instance].pinToken = token;
                                               [DataCache instance].deviceID = deviceID;
                                               
                                               [self hideiPhoneSyncLoader];
                                               [self hideWarning];
                                               [self.keyboardGroup setHidden:NO];
                                           } else {
                                               [self hideiPhoneSyncLoader];
                                               [self.keyboardGroup setHidden:YES];
                                               [self showWarning:@"Чтобы войти, Вы должны установить Пин код в приложении Банк Астаны" autoHide:NO];
                                           }
                                       }
                                       errorHandler:^(NSError *error) {
                                           NSLog(@"Failed to send message to iOS app with error - %@", error);
                                       }
             ];
        } else {
            NSLog(@"iOS app not reachable");
        }
    }
}

#pragma mark - WCSessionDelegate

- (void)didReceivePinTokenMessage:(NSNotification *)notification {
    NSDictionary *message = notification.object;
    NSDictionary *messageObject = message[wcMessageObject];
    
    NSString *token = [messageObject objectForKey:@"pinToken"];
    NSString *deviceID = [messageObject objectForKey:@"deviceID"];
    
    if (token && token.length > 0) {
        [DataCache instance].pinToken = token;
        [DataCache instance].deviceID = deviceID;
        
        [self.loaderGroup setHidden:true];
        [self hideWarning];
        [self.keyboardGroup setHidden:NO];
    } else {
        [self.loaderGroup setHidden:true];
        [self.keyboardGroup setHidden:YES];
        [self showWarning:@"Чтобы войти, Вы должны установить Пин код в приложении Банк Астаны" autoHide:NO];
    }
}

@end



