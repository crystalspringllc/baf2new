//
//  OperationsHistoryInterfaceController.m
//  BAF
//
//  Created by Almas Adilbek on 9/7/15.
//
//

#import "OperationsHistoryInterfaceController.h"
#import "OperationsApi.h"
#import "WKInterfaceObject+Ext.h"
#import "Constants.h"
#import "OperationHistoryRowController.h"
#import "NSNumber+Ext.h"
#import "WKSettings.h"
//#import <MagicalRecord/MagicalRecord.h>
#import <DateTools/NSDate+DateTools.h>

@interface OperationsHistoryInterfaceController()
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *container;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *loader;
@property (weak, nonatomic) IBOutlet WKInterfaceTable *list;

@end

@implementation OperationsHistoryInterfaceController {
    NSUInteger visibleCount;
}

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    visibleCount = 0;

    // Database
//    [MagicalRecord setupCoreDataStackWithStoreNamed:kDBFile];
}

- (void)willActivate {
    [super willActivate];

    NSUInteger count = [WKSettings operationsHistoryVisibleOperationsCount];

    // Check if awakeWithContext or visible count value changed from settings, then load list.
    if(count != visibleCount) {
        visibleCount = count;
        [self loadOperationsHistory];
    }
}

- (void)didDeactivate {
    [super didDeactivate];
}

- (void)loadOperationsHistory
{
    [self.container hide];
    [self.loader show];

    OperationHistoryFilter *filter = [OperationHistoryFilter defaultFilter];
    filter.startDate = nil;
    filter.endDate = nil;
    filter.count = 50; // get last 50 operations
    [OperationsApi getLastOperationsWithFilter:filter success:^(NSArray *operationStatusList, NSArray *operationList, NSString *startDate, NSString *endDate) {
        [self.loader hide];
        [self.container show];
        
        [self operationsHistoryLoaded:operationList];
    } failure:^(NSString *code, NSString *message) {
        [self.loader setText:message];
    }];
}

- (void)operationsHistoryLoaded:(NSArray *)response
{
    [self.list setNumberOfRows:response.count withRowType:@"listRow"];

    NSUInteger index = 0;
    for(OperationHistory *operation in response) {
        OperationHistoryRowController *row = [self.list rowControllerAtIndex:index];

        NSDate *date = [[OperationHistory dateFormatterFromServer] dateFromString:operation.date];
        [row.dateLabel setText:[NSString stringWithFormat:@"%@", [date timeAgoSinceNow]]];
        [row.descriptionLabel setText:operation.text];

        [row.amountLabel setText:[NSString stringWithFormat:@"%@ %@", [operation.amount decimalFormatString], operation.currency]];
        [row.comissionLabel setText:[NSString stringWithFormat:@"%@ %@", [operation.amountWithCommission decimalFormatString], operation.currency]];

        StatusType status = [OperationHistory statusFromString:operation.status];
        if (status == StatusTypeComplete) {
            [row.statusImageView setImageNamed:@"icon-success"];
        } else if (status == StatusTypeError) {
            [row.statusImageView setImageNamed:@"icon-error"];
        } else if (status== StatusTypeInProgress) {
            [row.statusImageView setImageNamed:@"icon-inprogress"];
        }

        ++index;
    }
}

#pragma mark -
#pragma mark Actions

- (IBAction)tapRefresh {
    [self loadOperationsHistory];
}

- (IBAction)tapSettings {
    [self pushControllerWithName:@"OperationsHistorySettingsInterfaceController" context:nil];
}

@end
