//
//  OperationsHistorySettingsInterfaceController.m
//  BAF
//
//  Created by Almas Adilbek on 9/11/15.
//
//

#import "OperationsHistorySettingsInterfaceController.h"
#import "OptionTitleRowController.h"
#import "WKSettings.h"

@interface OperationsHistorySettingsInterfaceController()
@property (weak, nonatomic) IBOutlet WKInterfaceTable *list;
@end

@implementation OperationsHistorySettingsInterfaceController {
    OptionTitleRowController *selectedRow;
}

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    NSArray *options = [WKSettings operationsHistoryVisibleCounts];

    [self.list setNumberOfRows:options.count withRowType:@"listRow"];

    NSUInteger selectedOption = [WKSettings operationsHistoryVisibleOperationsCount];

    NSUInteger index = 0;
    for(NSString *option in options) {
        OptionTitleRowController *row = [self.list rowControllerAtIndex:index];
        [row.titleLabel setText:option];

        if([option integerValue] == selectedOption) {
            selectedRow = row;
            [row checked];
        } else {
            [row unchecked];
        }

        ++index;
    }
}

- (void)table:(WKInterfaceTable *)table didSelectRowAtIndex:(NSInteger)rowIndex
{
    if(selectedRow) {
        [selectedRow unchecked];
    }
    
    OptionTitleRowController *row = [table rowControllerAtIndex:rowIndex];
    [row checked];

    selectedRow = row;

    NSUInteger count = [[WKSettings operationsHistoryVisibleCounts][rowIndex] integerValue];
    [WKSettings setOperationsHistoryVisibleOperationsCount:count];

    // Close settings.
    [self popController];
}

- (void)willActivate {
    [super willActivate];
}

- (void)didDeactivate {
    [super didDeactivate];
}


@end
