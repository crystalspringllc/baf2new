//
//  MainInterfaceController.m
//  BAF
//
//  Created by Almas Adilbek on 6/30/15.
//
//

#import "MainInterfaceController.h"
#import "User.h"

@interface MainInterfaceController ()
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *welcomeLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceButton *operationsHistoryButton;

@end

@implementation MainInterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    // Hide for temp time operations history button.
//    [self.operationsHistoryButton setHidden:YES];

    NSString *welcomeText = [NSString stringWithFormat:@"Добро пожаловать,\n%@", [User sharedInstance].fio];
    [self.welcomeLabel setText:welcomeText];
}

- (void)willActivate {
    [super willActivate];
}

- (void)didDeactivate {
    [super didDeactivate];
}

@end



