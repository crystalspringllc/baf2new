//
//  AccountsInterfaceController.m
//  BAF
//
//  Created by Almas Adilbek on 6/30/15.
//
//

#import "NSObject+PWObject.h"
#import "AccountsInterfaceController.h"
#import "AccountsApi.h"
#import "TitleSubtitleRowController.h"
#import "AccountsResponse.h"
#import "Card.h"
#import "Current.h"
#import "Deposit.h"
#import "Loan.h"
#import "DataCache.h"
#import "User.h"
#import "CardAccount.h"
#import "DepositGroup.h"

@interface AccountsInterfaceController ()
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *container;
@property (weak, nonatomic) IBOutlet WKInterfaceTable *cardAccountsTable;
@property (weak, nonatomic) IBOutlet WKInterfaceTable *currentAccountsTable;
@property (weak, nonatomic) IBOutlet WKInterfaceTable *depositsTable;
@property (weak, nonatomic) IBOutlet WKInterfaceTable *loansTable;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *loaderText;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *messageLabel1;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *messageLabel2;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *messageLabel3;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *messageLabel4;

@end

@implementation AccountsInterfaceController {

    BOOL loading;
    NSMutableArray *bankCardAccounts;
    NSMutableArray *allDeposits;

    AccountsResponse *dataResponse;
}

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    [self.container setHidden:YES];

    // Load / Get data
    dataResponse = [[DataCache instance] accountsResponse];
    if(!dataResponse) {
        [self loadAccounts];
    } else {
        [self accountsLoaded];
    }
}

- (void)willActivate
{
    [super willActivate];
}

- (void)didDeactivate {
    [super didDeactivate];
}

- (IBAction)tapRefresh {
    [self loadAccounts];
}

#pragma mark -
#pragma mark Loading Accounts

- (void)loadAccounts
{
    if(!loading) {
        loading = YES;

        [self.loaderText setHidden:NO];
        [self.container setHidden:YES];
        
        [AccountsApi getAccountsSuccess:^(id response) {
            loading = NO;
            
            AccountsResponse *accountsReponse = [AccountsResponse instanceFromDictionary:response];
            
            // Cache
            [[DataCache instance] setAccountsResponse:accountsReponse];
            
            dataResponse = accountsReponse;
            [self accountsLoaded];
        } failure:^(NSString *code, NSString *message) {
            NSLog(@"error: %@", message);
            loading = NO;
            [self hideLoader];
        }];
    }
}

- (void)accountsLoaded
{
    [self hideLoader];

    NSInteger index = 0;

    NSArray *accounts = dataResponse.currents;
    NSArray *deposits = dataResponse.depositGroups;
    NSArray *loans = dataResponse.loans;

    // Get bank cards responses
    bankCardAccounts = [NSMutableArray arrayWithArray:dataResponse.cardAccounts];

    // Card accounts.
    [self.cardAccountsTable setNumberOfRows:bankCardAccounts.count withRowType:@"cardAccountRow"];
    for(CardAccount *account in bankCardAccounts)
    {
        TitleSubtitleRowController *row = [self.cardAccountsTable rowControllerAtIndex:index];
        [row.titleLabel setText:account.alias];
        [row.subtitleLabel setText:[account getBalanceWithCurrency]];
        [row hideIcon];
        ++index;
    }

    // Current accounts.
    index = 0;
    [self.currentAccountsTable setNumberOfRows:accounts.count withRowType:@"currentAccountRow"];
    for(Current *account in accounts) {
        TitleSubtitleRowController *row = [self.currentAccountsTable rowControllerAtIndex:index];
        [row.titleLabel setText:account.alias];
        [row.subtitleLabel setText:[account balanceWithCurrency]];
        [row.iconImage setImageNamed:account.isFavorite ? @"icon-favorite-on" : @"icon-favorite-off"];
        ++index;
    }

    // Deposits
    allDeposits = [NSMutableArray arrayWithArray:deposits];

    index = 0;
    [self.depositsTable setNumberOfRows:allDeposits.count withRowType:@"depositRow"];
    for(DepositGroup *theDeposit in allDeposits) {
        TitleSubtitleRowController *row = [self.depositsTable rowControllerAtIndex:index];
        [row.titleLabel setText:theDeposit.alias];
        ++index;
    }

    // Loans
    NSMutableArray * allLoans = [NSMutableArray array];
    for(Loan *loan in loans) {
        [allLoans addObject:loan];
    }

    index = 0;
    [self.loansTable setNumberOfRows:allLoans.count withRowType:@"loanRow"];
    for(Loan *loan in allLoans) {
        TitleSubtitleRowController *row = [self.loansTable rowControllerAtIndex:index];
        [row.titleLabel setText:[loan productAliasWithPrincipal]];
        [row.subtitleLabel setText:[loan repaymentAmountWithCurrency]];
        [row.iconImage setImageNamed:loan.isFavorite ? @"icon-favorite-on" : @"icon-favorite-off"];
        ++index;
    }

    // Check
    [self.messageLabel1 setHidden:bankCardAccounts.count != 0];
    [self.messageLabel2 setHidden:accounts.count != 0];
    [self.messageLabel3 setHidden:allDeposits.count != 0];
    [self.messageLabel4 setHidden:allLoans.count != 0];

    // Show container.
    [self.container setHidden:NO];
}

- (void)table:(WKInterfaceTable *)table didSelectRowAtIndex:(NSInteger)rowIndex
{
    if([table isEqual:self.cardAccountsTable])
    {
        [self pushControllerWithName:@"BankCardsInterfaceController" context:bankCardAccounts[rowIndex]];
    }
    else if([table isEqual:self.depositsTable])
    {
        [self pushControllerWithName:@"DepositsInterfaceController" context:allDeposits[rowIndex]];
    }
}

#pragma mark -
#pragma mark Helper

- (void)hideLoader {
    [self.loaderText setHidden:YES];
}

@end



