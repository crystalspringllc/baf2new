//
//  UserContractsInterfaceController.m
//  BAF
//
//  Created by Almas Adilbek on 7/2/15.
//
//

#import "UserContractsInterfaceController.h"
#import "ProfileApiOLD.h"
#import "Constants.h"
#import "Contract.h"
#import "ContractsApi.h"
#import "ContractRowController.h"
#import "WKInterfaceImage+Ext.h"
#import "DataCache.h"
//#import <MagicalRecord/MagicalRecord.h>

@interface UserContractsInterfaceController ()
@property (weak, nonatomic) IBOutlet WKInterfaceTable *table;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *loaderLabel;

@end

@implementation UserContractsInterfaceController {

}

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    // Database
//    [MagicalRecord setupCoreDataStackWithStoreNamed:kDBFile];

    [self loadContracts];
}

- (void)willActivate {
    [super willActivate];

    [DataCache instance].paymentAmount = nil;
}

- (void)didDeactivate {
    [super didDeactivate];
}

#pragma mark -
#pragma mark Load

- (void)loadContracts
{
    [self.loaderLabel setHidden:NO];
    [self.table setHidden:YES];

    NSArray *contracts = [[DataCache instance] contracts];
    NSMutableArray *filteredContractsForWatch = [[NSMutableArray alloc] init];

    if(!contracts) {
        [ContractsApi getContractsWithSuccess:^(NSArray *contracts) {
            
            for (Contract *contract in contracts) {
                if ([contract.provider.infoRequestType intValue] != 2) {
                    [filteredContractsForWatch addObject:contract];
                }
            }
            
            [self onContractsResponse:filteredContractsForWatch];
        } failure:^(NSString *code, NSString *message) {
            [self.loaderLabel setHidden:YES];
        }];
    } else {
        [self onContractsResponse:contracts];
    }
}

- (void)onContractsResponse:(NSArray *)response
{
    [self.loaderLabel setHidden:YES];

    [[DataCache instance] setContracts:response];
    [self.table setNumberOfRows:response.count withRowType:@"contractRow"];

    NSUInteger index = 0;
    for(Contract *contract in response) {
        ContractRowController *row = [self.table rowControllerAtIndex:index];
        [row.aliasLabel setText:contract.alias];
        [row.providerLabel setText:contract.provider.name];
        [row.identifierLabel setText:contract.contract];
        [row.iconImage setImageWithUrl:contract.provider.logo];
        ++index;
    }

    [self.table setHidden:NO];
}

- (void)table:(WKInterfaceTable *)table didSelectRowAtIndex:(NSInteger)rowIndex
{
    if([DataCache instance].contracts) {
        Contract *contract = [DataCache instance].contracts[rowIndex];

        [self pushControllerWithName:@"PaymentInterfaceController" context:contract];
    }
}

@end



