//
//  DepositsInterfaceController.m
//  BAF
//
//  Created by Almas Adilbek on 7/1/15.
//
//

#import "DepositsInterfaceController.h"
#import "TitleSubtitleRowController.h"
#import "CurrentAccount.h"
#import "Deposit.h"
#import "DepositGroup.h"

@interface DepositsInterfaceController ()
@property (weak, nonatomic) IBOutlet WKInterfaceTable *table;

@end

@implementation DepositsInterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    DepositGroup *deposit = (DepositGroup *)context;
    [self.table setNumberOfRows:deposit.deposits.count withRowType:@"listRow"];

    NSUInteger index = 0;
    for(Deposit *account in deposit.deposits) {
        TitleSubtitleRowController *row = [self.table rowControllerAtIndex:index];
        [row.titleLabel setText:account.alias];
        [row.subtitleLabel2 setText:account.iban];
        [row.subtitleLabel setText:[account balanceWithCurrency]];
        [row.iconImage setImageNamed:[NSString stringWithFormat:@"icon-rect-%@", [account.currency lowercaseString]]];
        ++index;
    }
}

- (void)willActivate {
    [super willActivate];
}

- (void)didDeactivate {
    [super didDeactivate];
}

@end



