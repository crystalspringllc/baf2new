//
//  PaymentInterfaceController.h
//  BAF
//
//  Created by Almas Adilbek on 7/2/15.
//
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@class BankCard;

@interface PaymentInterfaceController : WKInterfaceController

@end
