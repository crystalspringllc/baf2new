//
//  OptionTitleRowController.h
//  BAF
//
//  Created by Almas Adilbek on 7/5/15.
//
//

#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>

@interface OptionTitleRowController : NSObject
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *titleLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *disclosureImage;


- (void)checked;
- (void)unchecked;

@end
