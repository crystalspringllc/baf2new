//
//  OptionTitleRowController.m
//  BAF
//
//  Created by Almas Adilbek on 7/5/15.
//
//

#import "OptionTitleRowController.h"
#import "WKInterfaceObject+Ext.h"

@implementation OptionTitleRowController

- (void)checked {
    [self.disclosureImage show];
}

- (void)unchecked {
    [self.disclosureImage hide];
}


@end
