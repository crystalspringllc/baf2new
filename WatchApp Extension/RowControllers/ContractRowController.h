//
//  ContractRowController.h
//  BAF
//
//  Created by Almas Adilbek on 7/2/15.
//
//

#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>

@interface ContractRowController : NSObject
@property (weak, nonatomic) IBOutlet WKInterfaceImage *iconImage;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *aliasLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *providerLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *identifierLabel;

@end
