//
//  OperationHistoryRowController.h
//  BAF
//
//  Created by Almas Adilbek on 9/7/15.
//
//

#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>

@interface OperationHistoryRowController : NSObject

@property (weak, nonatomic) IBOutlet WKInterfaceImage *statusImageView;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *amountLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *comissionLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *dateLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *descriptionLabel;


@end
