//
//  TitleSubtitleRowController.h
//  BAF
//
//  Created by Almas Adilbek on 6/30/15.
//
//

#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>

@interface TitleSubtitleRowController : NSObject
@property (weak, nonatomic) IBOutlet WKInterfaceImage *iconImage;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *titleLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *subtitleLabel2;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *disclosureImage;

- (void)hideIcon;

@end
