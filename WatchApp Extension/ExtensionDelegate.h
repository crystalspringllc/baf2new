//
//  ExtensionDelegate.h
//  WatchApp Extension
//
//  Created by Shyngys Kassymov on 23.09.16.
//  Copyright © 2016 Crystal Spring. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <WatchConnectivity/WatchConnectivity.h>

@interface ExtensionDelegate : NSObject <WKExtensionDelegate, WCSessionDelegate>

@end
