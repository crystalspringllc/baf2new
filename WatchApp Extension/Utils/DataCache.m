//
//  DataCache.m
//  BAF
//
//  Created by Almas Adilbek on 7/1/15.
//
//

#import "DataCache.h"

static DataCache *_instance = nil;

@implementation DataCache

+ (DataCache *)instance {
    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }
    return _instance;
}

+ (void)clear {
    _instance = nil;
}


@end
