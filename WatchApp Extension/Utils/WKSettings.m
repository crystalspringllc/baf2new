//
//  WKSettings.m
//  BAF
//
//  Created by Almas Adilbek on 9/11/15.
//
//

#import "WKSettings.h"
#import "NSUserDefaults+sharedDefaults.h"

#define kOperationsHistoryVisibleOperationsCountKey @"wk_operationsHistoryVisibleOperationsCount"

@implementation WKSettings

+ (NSArray *)operationsHistoryVisibleCounts {
    return @[@"5", @"10", @"20", @"30"];
}

+ (void)setOperationsHistoryVisibleOperationsCount:(NSUInteger)count
{
    [[NSUserDefaults shared] setInteger:count forKey:kOperationsHistoryVisibleOperationsCountKey];
}

+ (NSUInteger)operationsHistoryVisibleOperationsCount
{
    NSUInteger count = [[NSUserDefaults shared] integerForKey:kOperationsHistoryVisibleOperationsCountKey];
    if(count == 0) {
        // Set the default value.
        count = [[self operationsHistoryVisibleCounts][1] integerValue];

        [self setOperationsHistoryVisibleOperationsCount:count];
    }
    return count;
}


@end
