//
//  AmountHelper.h
//  BAF
//
//  Created by Almas Adilbek on 7/5/15.
//
//

#import <Foundation/Foundation.h>

@interface AmountHelper : NSObject

+ (NSString *)formattedAmountWithKZT:(NSNumber *)amount;

+ (NSArray *)paymentAmounts;

@end
