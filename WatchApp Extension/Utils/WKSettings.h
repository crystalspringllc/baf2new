//
//  WKSettings.h
//  BAF
//
//  Created by Almas Adilbek on 9/11/15.
//
//

#import <Foundation/Foundation.h>

@interface WKSettings : NSObject

+ (NSArray *)operationsHistoryVisibleCounts;
+ (void)setOperationsHistoryVisibleOperationsCount:(NSUInteger)count;
+ (NSUInteger)operationsHistoryVisibleOperationsCount;

@end