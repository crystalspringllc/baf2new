//
//  AmountHelper.m
//  BAF
//
//  Created by Almas Adilbek on 7/5/15.
//
//

#import "AmountHelper.h"
#import "NSNumber+Ext.h"

@implementation AmountHelper

+ (NSString *)formattedAmountWithKZT:(NSNumber *)amount {
    NSString *amountText = [amount decimalFormatString];
    amountText = [amountText stringByReplacingOccurrencesOfString:@".00" withString:@""];
    amountText = [amountText stringByReplacingOccurrencesOfString:@",00" withString:@""];
    return [NSString stringWithFormat:@"%@ KZT", amountText];
}

+ (NSArray *)paymentAmounts {
    return @[@(200), @(500), @(1000), @(1500), @(2000), @(5000), @(10000)];
}


@end
