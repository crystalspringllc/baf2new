//
//  DataCache.h
//  BAF
//
//  Created by Almas Adilbek on 7/1/15.
//
//

#import <Foundation/Foundation.h>

@class AccountsResponse, Card, OperationAccounts;

@interface DataCache : NSObject

@property (nonatomic, strong) AccountsResponse *accountsResponse;
@property (nonatomic, strong) OperationAccounts *operationAccounts;
@property (nonatomic, strong) NSArray *contracts;

@property (nonatomic, copy) NSNumber *paymentAmount;
@property (nonatomic, strong) Card *selectedBankCard;

@property (nonatomic, strong) NSString *pinToken;
@property (nonatomic, strong) NSString *deviceID;

+ (DataCache *)instance;
+ (void)clear;

@end